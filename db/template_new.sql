-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2017 at 03:30 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `template_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `button`
--

CREATE TABLE `button` (
  `id_button` int(11) NOT NULL,
  `id_icon` int(11) DEFAULT NULL,
  `button_name` varchar(20) DEFAULT NULL,
  `button_title` varchar(20) DEFAULT NULL,
  `button_text` varchar(20) DEFAULT NULL,
  `button_class` varchar(100) DEFAULT NULL,
  `button_attribute` varchar(100) DEFAULT NULL,
  `button_type` int(11) DEFAULT NULL,
  `button_status` tinyint(1) NOT NULL DEFAULT '1',
  `is_privilege` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `button`
--

INSERT INTO `button` (`id_button`, `id_icon`, `button_name`, `button_title`, `button_text`, `button_class`, `button_attribute`, `button_type`, `button_status`, `is_privilege`) VALUES
(1, 1, 'add', 'Tambah', 'Tambah Baru', 'btn btn-primary btn-sm', NULL, 1, 1, 1),
(2, 23, 'view', 'Lihat', NULL, 'btn btn-primary btn-icon-only btn-circle', NULL, 1, 1, 1),
(3, 3, 'edit', 'Ubah', NULL, 'btn btn-warning btn-icon-only btn-circle', NULL, 1, 1, 1),
(4, 4, 'delete', 'Hapus', NULL, 'btn btn-danger btn-icon-only btn-circle', '', 1, 1, 1),
(5, NULL, 'save', 'Simpan', 'Simpan', 'btn blue btn-sm', NULL, 2, 1, 0),
(6, NULL, 'update', 'Simpan', 'Simpan', 'btn blue btn-sm', NULL, 2, 1, 0),
(7, NULL, 'cancel', 'Batal', 'Batal', 'btn default btn-sm', NULL, 3, 1, 0),
(8, NULL, 'closes', 'Tutup', 'Tutup', 'btn default btn-sm', NULL, 3, 1, 0),
(9, 9, 'add_item', 'Tambah', 'Tambah', 'btn blue-chambray', '', 1, 1, 0),
(10, 4, 'delete_item', 'Hapus', '', '', NULL, 3, 1, 0),
(11, 15, 'accept', 'Terima', '', ' ', '', 3, 1, 0),
(12, 16, 'decline', 'Tolak', '', '', '', 3, 1, 0),
(13, 5, 'process', 'Proses', '', '', '', 3, 1, 0),
(14, 6, 'search', 'Cari', 'Cari', 'btn blue-chambray', '', 1, 1, 0),
(15, NULL, 'close_modal', 'Tutup', 'Tutup', 'btn dark btn-outline', 'data-dismiss=\"modal\"', 1, 1, 0),
(16, NULL, 'process_modal', 'Proses', 'Proses', 'btn blue-chambray', 'data-dismiss=\"modal\"', 1, 1, 0),
(17, NULL, 'save_modal', 'Save', 'Save', 'btn blue-chambray', '', 1, 1, 0),
(18, 1, 'add_data', 'Tambah', 'Tambah', 'btn blue-chambray', NULL, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `icon`
--

CREATE TABLE `icon` (
  `id_icon` int(11) NOT NULL,
  `icon_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `icon`
--

INSERT INTO `icon` (`id_icon`, `icon_name`) VALUES
(1, 'fa fa-plus'),
(2, 'fa fa-eye'),
(3, 'fa fa-edit'),
(4, 'fa fa-trash'),
(5, 'fa fa-database'),
(6, 'fa fa-dashboard'),
(7, 'fa fa-heartbeat'),
(8, 'fa fa-print'),
(9, 'fa fa-opencart'),
(10, 'icon-notebook'),
(11, 'icon-users font-blue-chambray'),
(12, 'icon-target'),
(13, 'fa fa-book'),
(14, 'icon-paper-clip font-purple-sharp'),
(15, 'fa fa-check-square font-purple-sharp'),
(16, 'fa fa-calendar'),
(17, 'icon-wrench font-purple-sharp\r\n\r\n'),
(18, 'fa fa-clock-o\r\n\r\n'),
(19, 'icon-settings'),
(20, 'icon-docs font-purple-sharp\r\n\r\n'),
(21, 'fa fa-circle-o'),
(22, 'fa fa-line-chart'),
(23, 'fa fa-list');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `id_icon` int(11) DEFAULT NULL,
  `menu_name` varchar(255) DEFAULT NULL,
  `menu_class` varchar(255) DEFAULT NULL,
  `menu_folder` varchar(255) DEFAULT NULL,
  `menu_parent` int(11) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `menu_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_button`
--

CREATE TABLE `menu_button` (
  `id_menu_button` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_button` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_access`
--

CREATE TABLE `role_access` (
  `id_role_access` int(11) NOT NULL,
  `id_group` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_button`
--

CREATE TABLE `role_button` (
  `id_role_button` int(11) NOT NULL,
  `id_role_access` int(11) NOT NULL,
  `id_menu_button` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, 'oTGQETJE3vmF3HATHQoMiO', 1268889823, 1514341428, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `button`
--
ALTER TABLE `button`
  ADD PRIMARY KEY (`id_button`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `icon`
--
ALTER TABLE `icon`
  ADD PRIMARY KEY (`id_icon`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `menu_button`
--
ALTER TABLE `menu_button`
  ADD PRIMARY KEY (`id_menu_button`);

--
-- Indexes for table `role_access`
--
ALTER TABLE `role_access`
  ADD PRIMARY KEY (`id_role_access`);

--
-- Indexes for table `role_button`
--
ALTER TABLE `role_button`
  ADD PRIMARY KEY (`id_role_button`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
