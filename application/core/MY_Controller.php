<?php
 
if (!defined('BASEPATH')) exit('No direct script access allowed');
 
/* load the MX_Router class */
require APPPATH . "third_party/MX/Controller.php";
 
/**
 * Description of my_controller
 *
 * @author http://roytuts.com
 */
class MY_Controller extends MX_Controller {
 
    function __construct() {
        parent::__construct();
        
        if (version_compare(CI_VERSION, '2.1.0', '<')) {
            $this->load->library('security');
        }

        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->library('myprivilege');
        $this->load->library('activity_log');
        
    }

    public function load_view($modules="",$folder_parent="", $folder="", $file_name="", $data="")
    {
        $data['sess_user']      = $this->ion_auth->user()->row();
        $data['csrf_name']      = $this->security->get_csrf_token_name();
        $data['csrf_value']     = $this->security->get_csrf_hash();
        /* Check privilege */
        $data['mn']             = $this->myprivilege->cekPrivilegePage();
        $data['btn']            = $this->myprivilege->cekPrivilegeButton(); 
        $data['redirect_index'] = site_url() . $this->site;
      
        /* handling folder parent empty */
        if(!empty($folder_parent)){
            $folder_parent = $folder_parent;
        }else{
            $folder_parent = redirect('dashboard');
        }

        $this->load->blade($modules .'.'. $folder_parent.'.views.'. $folder .'.'. $file_name, $data);
    }

    /* 
        FRONT-END
    */
    public function load_view_frontend($modules="",$folder_parent="", $folder="", $file_name="", $data="")
    {
        $data['sess_user']  = $this->ion_auth->user()->row();
        $data['csrf_name']  = $this->security->get_csrf_token_name();
        $data['csrf_value'] = $this->security->get_csrf_hash();
        
        // Menu
        $data['active_menu'] = $this->uri->segment(1);
        $data['menu_bebek']  = ms_produk::where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.kode_kategori_motor', '2')->get();
        $data['menu_matic']  = ms_produk::where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.kode_kategori_motor', '1')->get();
        $data['menu_sport']  = ms_produk::where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.kode_kategori_motor', '3')->get();
        //
        $this->load->blade($modules .'.'. $folder_parent.'.views.'. $folder .'.'. $file_name, $data);
    }
}
 
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */