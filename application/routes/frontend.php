	<?php
	
	$frontend_route                                    = array();
	
	/* Home */
	$frontend_route['home']                            = 'home/index';
	$frontend_route['ajax_news_filter']                = 'home/ajax_get_news_filter';	// Function For Search
	$frontend_route['ajax_total_page_news']            = 'home/ajax_count_news_page';	// Function For Search
	
	/* Wanda */
	$frontend_route['wanda']                           = 'wanda/index';
	
	/* Aksesoris */
	$frontend_route['aksesoris']                       = 'aksesoris/index';
	
	/* Calculator */
	$frontend_route['calculator/(:any)']               = 'calculator/index/$1';
	
	/* Event */
	// $frontend_route['event']                        = 'event/index';
	// $frontend_route['event-detail/(:any)']          = 'event/view/$1';
	
	/* Routes Event Terbaru */
	$frontend_route['event']                           = 'event/index';
	$frontend_route['event/(:any)']                    = 'event/view/$1';
	
	/* Gso */
	$frontend_route['gso']                             = 'gso/index';
	
	/* Jaringan */
	$frontend_route['jaringan']                        = 'jaringan/index';
	
	/* News */
	$frontend_route['news/(:any)']                     = 'news/index/$1';
	$frontend_route['ajax_news_all']                   = 'news/ajax_get_all_news';
	$frontend_route['ajax_news_all/(:any)']            = 'news/ajax_get_all_news/$1';
	$frontend_route['ajax_news_news']                  = 'news/ajax_get_news_news';
	$frontend_route['ajax_news_news/(:any)']           = 'news/ajax_get_news_news/$1';
	$frontend_route['ajax_news_tips']                  = 'news/ajax_get_tips_news';
	$frontend_route['ajax_news_tips/(:any)']           = 'news/ajax_get_tips_news/$1';
	$frontend_route['ajax_news_event']                 = 'news/ajax_get_event_news';
	$frontend_route['ajax_news_event/(:any)']          = 'news/ajax_get_event_news/$1';
	$frontend_route['ajax_news_press_release']         = 'news/ajax_get_press_release_news';
	$frontend_route['ajax_news_press_release/(:any)']  = 'news/ajax_get_press_release_news/$1';
	$frontend_route['ajax_news_total_page']            = 'news/ajax_count_pages';
	$frontend_route['news-detail/(:any)']              = 'news/view/$1';
	$frontend_route['news-komentar/save']              = 'news/save';
	
	/* Route News Terbaru */
	$frontend_route['blog']                            = 'news/index';
	$frontend_route['blog/(:any)']                     = 'news/index/$1';
	$frontend_route['blog/artikel-terkait']            = 'news/artikel_terkait';
	$frontend_route['blog/artikel-terkait/(:any)']     = 'news/artikel_terkait/$1';
	
	/* Rss */
	$frontend_route['rss']                             = 'news/instant_article_facebook_rss';
	
	/* Produk */
	// $frontend_route['produk']                       = 'produk/index';
	$frontend_route['produk/(:any)']                   = 'produk/index/$1';
	
	/* Promo */
	// $frontend_route['promo-wahana']                 = 'promo_wahana/index';
	// $frontend_route['promo-wahana-detail/(:any)']   = 'promo_wahana/view/$1';
	
	/* Routes Promo Terbaru */
	$frontend_route['promo']                           = 'promo/index';
	$frontend_route['promo/(:any)']                    = 'promo/view/$1';
	
	
	/* Faq */
	$frontend_route['faq-wahana']                      = 'faq_wahana/index';
	
	/* Form Pemenang */
	$frontend_route['form-pemenang']                   = 'form_pemenang/index';
	
	$frontend_route['produk']                          = 'produk/list_all';
	$frontend_route['produk/motor-cub']                = 'produk/list_bebek';
	$frontend_route['produk/motor-matic']              = 'produk/list_matic';
	$frontend_route['produk/motor-sport']              = 'produk/list_sport';
	
	/* Routes Frontend Terbaru 2019 */
	
	/* Pemesanan Produk */
	$frontend_route['pemesanan-produk/(:any)']         = 'pemesanan_produk/index/$1';
	
	/* Daftar Test Ride */
	$frontend_route['testride/(:any)']                 = 'testride/index/$1';
	
	/* Daftar Promo */
	$frontend_route['daftar-promo/(:any)']             = 'daftar_promo/index/$1';
	
	/* Daftar Event  */
	$frontend_route['daftar-event/(:any)']             = 'daftar_event/index/$1';
	
	/* Aksesoris dan Pembelian Aksesoris */
	$frontend_route['aksesoris/(:any)']                = 'aksesoris_produk/index/$1';
	$frontend_route['aksesoris/beli-aksesoris/(:any)'] = 'aksesoris_produk/beli_aksesoris/$1';
	
	/* Apparel dan Pembelian */
	$frontend_route['apparel/(:any)']                  = 'apparel_produk/index/$1';
	$frontend_route['apparel/beli-apparel/(:any)']     = 'apparel_produk/beli_apparel/$1';
	
	/* Routes Frontend Terbaru 2019 */