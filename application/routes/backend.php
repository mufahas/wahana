<?php

$backend_route = array();

/* Dashboard */
$backend_route['dasboard'] = 'dashboard/dashboard';

/* Auth */
$backend_route['login']    = 'auth/auth';
$backend_route['logout']   = 'auth/auth/logout';

/* Role Application */
/* Menu */
$backend_route['role-application/menu']                  = 'role_application/menu';
$backend_route['role-application/menu/loadTable']        = 'role_application/menu/loadTable';
$backend_route['role-application/menu/add']              = 'role_application/menu/add';
$backend_route['role-application/menu/save']             = 'role_application/menu/save';
$backend_route['role-application/menu/edit/(:any)']      = 'role_application/menu/edit/$1';
$backend_route['role-application/menu/update']           = 'role_application/menu/update';
$backend_route['role-application/menu/delete']           = 'role_application/menu/delete';
$backend_route['role-application/menu/ajax_check_class'] = 'role_application/menu/ajax_check_class';

/* Role */
$backend_route['role-application/role']             = 'role_application/role';
$backend_route['role-application/role/loadTable']   = 'role_application/role/loadTable';
$backend_route['role-application/role/add']         = 'role_application/role/add';
$backend_route['role-application/role/save']        = 'role_application/role/save';
$backend_route['role-application/role/edit/(:any)'] = 'role_application/role/edit/$1';
$backend_route['role-application/role/update']      = 'role_application/role/update';
$backend_route['role-application/role/delete']      = 'role_application/role/delete';

/* Role Permission */
$backend_route['role-application/role-permission']                    = 'role_application/role_permission';
$backend_route['role-application/role-permission/loadTable']          = 'role_application/role_permission/loadTable';
$backend_route['role-application/role-permission/add']                = 'role_application/role_permission/add';
$backend_route['role-application/role-permission/save']               = 'role_application/role_permission/save';
$backend_route['role-application/role-permission/edit/(:any)']        = 'role_application/role_permission/edit/$1';
$backend_route['role-application/role-permission/update']             = 'role_application/role_permission/update';
$backend_route['role-application/role-permission/delete']             = 'role_application/role_permission/delete';
$backend_route['role-application/role-permission/ajax_getPermission'] = 'role_application/role_permission/ajax_getPermission';

/* User */
$backend_route['role-application/user']                               = 'role_application/user';
$backend_route['role-application/user/loadTable']                     = 'role_application/user/loadTable';
$backend_route['role-application/user/add']                           = 'role_application/user/add';
$backend_route['role-application/user/save']                          = 'role_application/user/save';
$backend_route['role-application/user/edit/(:any)']                   = 'role_application/user/edit/$1';
$backend_route['role-application/user/view/(:any)']                   = 'role_application/user/view/$1';
$backend_route['role-application/user/update']                   	  = 'role_application/user/update';
$backend_route['role-application/user/delete']                  	  = 'role_application/user/delete';
$backend_route['role-application/user/ajax_check_email']         	  = 'role_application/user/ajax_check_email';