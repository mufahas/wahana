<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Button
$lang['button_add_new']          = 'Add New';
$lang['button_insert']           = 'Save';
$lang['button_update']           = 'Update';
$lang['button_cancel']           = 'Cancel';
$lang['button_close']            = 'Close';
$lang['button_change_password']  = 'Change Password';
$lang['button_back']             = 'Back';
$lang['button_next']             = 'Next Column';
$lang['button_finish']           = 'Finish';
$lang['button_export']           = 'Export Excel';

// Messages
$lang['message_save_success']    = 'Data berhasil disimpan.';
$lang['message_save_failed']     = 'Data gagal disimpan.';
$lang['message_update_success']  = 'Data berhasil diubah.';
$lang['message_update_failed']   = 'Data gagal diubah.';
$lang['message_delete_success']  = 'Data berhasil dihapus.';
$lang['message_delete_failed']   = 'Data gagal dihapus.';
$lang['message_data_exist']      = 'Data sudah ada di dalam database.';
$lang['message_wrong_password']  = 'Konfirmasi kata sandi harus sama dengan kata sandinya.';
$lang['message_data_not_found']  = "Data tidak ditemukan.";
$lang['message_data_found']      = 'Data ditemukan.';