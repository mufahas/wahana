<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Button
$lang['button_add_new']          = 'Add New';
$lang['button_insert']           = 'Save';
$lang['button_cancel']           = 'Cancel';
$lang['button_close']            = 'Close';
$lang['button_change_password']  = 'Change Password';
$lang['button_back']             = 'Back';
$lang['button_next']             = 'Next Column';
$lang['button_finish']           = 'Finish';
$lang['button_export']           = 'Export Excel';

// Messages
$lang['message_save_success']    = 'Data successfuly saved.';
$lang['message_save_failed']     = 'Data failed to save.';
$lang['message_update_success']  = 'Data successfuly changed.';
$lang['message_update_failed']   = 'Data failed to change.';
$lang['message_delete_success']  = 'Data successfuly deleted.';
$lang['message_delete_failed']   = 'Data failed to delete.';
$lang['message_data_exist']      = 'The data is already registered.';
$lang['message_wrong_password']  = 'The password confirmation must be the same as the password.';
$lang['message_data_not_found']  = "Data doesn't exists.";
$lang['message_approve_success'] = 'Data successfuly approve.';
$lang['message_approve_failed']  = 'Data failed to approve.';
$lang['message_reject_success']  = 'Data successfuly reject.';
$lang['message_reject_failed']   = 'Data failed to reject.';
$lang['message_data_found']      = 'Data is found';