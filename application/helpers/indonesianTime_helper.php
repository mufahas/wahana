<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @abstract Function set date
 * @return Indonesian date format
 * @author Iskandar Zulkarnaen <lzulkarnaen111@gmail.com>
 */
function indonesian_date($date){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $date);
        
        // variabel index 0 = date
        // variabel index 1 = month
        // variabel index 2 = year
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 ?>