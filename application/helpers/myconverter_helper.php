<?php

/**
 * @abstract Function converter number to words
 * @return string
 * @author http://stackoverflow.com/questions/14314997/how-to-convert-amount-in-number-to-words
 * @author Muhammar Rafsanjani <amarafsanjani@gmail.com> edited
 * @param this function will round down every decimal, please convert to real integer before
 */
function NumbertoWord($number)
{
    $string      = null;
    $fraction    = array();
    $dash        = '-';
    $conjunction = ' and ';
    $separator   = ' ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => '',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety'
    );
    $fractions      = array(
        0                   => 'hundred',
        1                   => 'thousand',
        2                   => 'million',
        3                   => 'billion',
        4                   => 'trillion',
        5                   => 'quadrillion',
        6                   => 'quintillion'
    );

    /* Check number not higher than PHP MAX INTEGER */
    // if ($number > PHP_INT_MAX)
        // return false;

    /* Check where negative number */
    if ($number < 0)
        return $negative . NumbertoWord(abs($number));

    /* Convert to integer */
    $number  = number_format($number, 0, ',', '.');
    $arrnum  = explode('.', $number);

    /* Action */
    $arrlen  = count($arrnum);

    /* Set Fraction */
    for ($i=($arrlen-1); $i >= 0; $i--)
    { 
        $fraction[] = $fractions[$i];
    }

    for ($i=0; $i < $arrlen; $i++)
    { 
        $len = strlen($arrnum[$i]);
        $now = $i;
        switch ($len)
        {
            case 1:
                $value   = $dictionary[$arrnum[$i]];
                $string .= $value;
                break;

            case 2:
                if ($arrnum[$i] < 21)
                {
                    $value   = $dictionary[$arrnum[$i]];
                    $string .= $value;
                }
                else if ($arrnum[$i] < 100)
                {
                    $nominal    = number_format($arrnum[$i] / 10, 1, ',', '.');
                    $arrnominal = explode(',', $nominal);

                    /* Tens */
                    $value   = $dictionary[(int) current($arrnominal) * 10];
                    $string .= $value;

                    /* Units */
                    if (next($arrnominal) > 0)
                        $string .= $dash . $dictionary[(int) current($arrnominal)];
                }
                break;
            
            case 3:
                $nominal    = number_format($arrnum[$i] / 100, 2, ',', '.');
                $arrnominal = explode(',', $nominal);

                /* Hundreds */
                $value   = $dictionary[(int) current($arrnominal)];
                $string .= $value;

                /* Fraction */
                if ($value)
                    $string .= $separator . $fraction[$arrlen - 1];

                if (next($arrnominal) > 0)
                {
                    if (current($arrnominal) < 21)
                    {
                        $string .= $conjunction . $dictionary[(int) current($arrnominal)];
                    }
                    else if (current($arrnominal) < 100)
                    {
                        $nominal    = number_format(current($arrnominal) / 10, 1, ',', '.');
                        $arrnominal = explode(',', $nominal);

                        /* Tens */
                        $value   = $conjunction . $dictionary[(int) current($arrnominal) * 10];
                        $string .= $value;

                        /* Units */
                        if (next($arrnominal) > 0)
                            $string .= $dash . $dictionary[(int) current($arrnominal)];
                    }
                }
                break;

        }
        $now++;

        /* Fraction */
        if ($now > $i && $i != ($arrlen - 1))
            $string .= $separator . $fraction[$i] . $separator;
    }

    return $string;
}


/**
 * @abstract Function converter number to romawi
 * @return string
 * @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
 * @param number
 */
function NumbertoRomawi($number)
{
    $dictionary = array(
        1  => 'I',
        2  => 'II',
        3  => 'III',
        4  => 'IV',
        5  => 'V',
        6  => 'VI',
        7  => 'VII',
        8  => 'VIII',
        9  => 'IX',
        10 => 'X',
        11 => 'XI',
        12 => 'XII'
    );

    /* Convert to Integer */
    $number = (int) $number;

    return $dictionary[$number];
}


/**
 * @abstract Function get local format number
 * @param {string}
 * @return String
 */
function getLocalformatNumber()
{
    $locale_info   = localeconv();
    // $decimal_point = $locale_info['decimal_point'];

    // if ($decimal_point == '.')
    // {
    //     $separator = ',';
    // }
    // else
    // {
    //     $separator = '.';
    // }

    $decimal_point = '.';
    $separator     = ',';

    $locale['decimal']   = $decimal_point;
    $locale['separator'] = $separator;

    return $locale;
}


/**
 * @abstract Function number formated
 * @param {string}
 * @return String
 */
function formattedNumber($number, $currency="", $precision=false)
{
    if (!empty($number) || $number != '')
    {
        $n   = getLocalformatNumber();
        $num = clearformattedNumber($number);

        if (strpos($num, $n['decimal']) == 0)
        {
            if ($precision == true)
            {
                $number = number_format($num, 2, $n['decimal'], $n['separator']);
            }
            else
            {
                $number = number_format($num, 0, $n['decimal'], $n['separator']);
            }
        }
        else
        {
            $n2 = explode($n['decimal'], $num);
            if (count($n2) > 1)
            {
                if ($n2[1] == '')
                {
                    $number = $number;
                // }
                // else if ($n2[1] < 10)
                // {
                //     $number = number_format($num, 1, $n['decimal'], $n['separator']);
                }
                else
                {
                    if ($n2[1] == 0)
                    {
                        $number = number_format($num, 0, $n['decimal'], $n['separator']);
                    }
                    else
                    {
                        if ($precision == true)
                        {
                            $number = number_format($num, 2, $n['decimal'], $n['separator']);
                        }
                        else
                        {
                            $number = number_format($num, 0, $n['decimal'], $n['separator']);
                        }
                    }
                }
            }
        }

    }

    if (!empty($currency))
    {
        if ($currency == true)
        {
            $currency = 'Rp. ';
            $number = $currency . $number;
        }
        else
        {
            $number = $currency . $number;
        }
    }

    return $number;
}


/**
 * @abstract Function clear number formated
 * @param {string}
 * @return String
 */
function clearformattedNumber($number)
{
    if (!empty($number) || $number != '')
    {
        $n      = getLocalformatNumber();
        // $x      = strpos($number, ',');
        // $xx     = substr($number, -3, 1);

        $number = (string) $number;
        $number = explode(' ', $number);
        $number = end($number);

        // $val    = empty($x) ? $n['decimal'] : ($xx == ',' ? $n['decimal'] : $n['separator']);
        $val    = $n['separator'];

        $number = str_replace($val, '', $number);
    }

    return $number;
}

function round_up ( $value, $precision = 0 ) { 
    $pow    = pow ( 10, $precision ); 
    $result = ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow;
    return $result;
}

function ceiling($value, $precision = 0) {
    $result = ceil($value * pow(10, $precision)) / pow(10, $precision);
    return $result;
}
