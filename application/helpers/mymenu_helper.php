<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *  @abstract Get List Parent Menu
 *  @return Data
 *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
**/
function _get_list_menu(){
    $CI      =& get_instance();
    $user    = $CI->ion_auth->user()->row();
    $id_user = $user->id;
    
    $q = M_menu::selectRaw('menu.*,icon.icon_name')
                ->where('users_groups.user_id', $id_user)
                ->where('menu.menu_status','1')
                ->where('menu.menu_parent','0')
                ->where('groups.is_delete','f')
                ->join('role_access','role_access.id_menu','=','menu.id_menu')
                ->join('groups','groups.id','=','role_access.id_group')
                ->join('users_groups','users_groups.group_id','=','groups.id')
                ->leftjoin('icon','icon.id_icon','=','menu.id_icon')
                ->orderBy('menu.menu_order','ASC');
    
    if ($q && $q->count() > 0)
    {
        return $q->get();
    }
    else
    {
        return array();
    }
}


/**
 *  @abstract Get List Child Menu
 *  @return Data
 *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
**/
function _get_list_child($parent="")
{
    $CI      =& get_instance();
    $user    = $CI->ion_auth->user()->row();
    $id_user = $user->id;

    $q = M_menu::selectRaw('menu.*,icon.icon_name')
                ->where('users_groups.user_id', $id_user)
                ->where('menu.menu_status','1')
                ->where('menu.menu_parent',$parent)
                ->where('groups.is_delete','f')
                ->join('role_access','role_access.id_menu','=','menu.id_menu')
                ->join('groups','groups.id','=','role_access.id_group')
                ->join('users_groups','users_groups.group_id','=','groups.id')
                ->leftjoin('icon','icon.id_icon','=','menu.id_icon')
                ->orderBy('menu.menu_order','ASC');

    if ($q && $q->count() > 0)
    {
        return $q->get();
    }
    else
    {
        return array();
    }
}


/**
 *  @abstract Get id Parent of Child Menu
 *  @return Data
 *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
**/
function cek_uri_parent($current="")
{
    $CI =& get_instance();

    $thisparents = array();

    /* Set Field To Select */
    $r = M_menu::selectRaw('*');

    /* Set Condition */
    for ($i=1; $i <= count($current); $i++) {
        $r->orWhere('menu.menu_class',$current[$i]);
    }

    /* Get Data */
    $q = $r->get();

    if ($q && $q->count() > 0)
    {
        $res = $q->first();
        $thisparents[] = $res->id_menu;

        $parent_up = cek_uri_parent_by_id($res->menu_parent);
        if (count($parent_up) > 0)
        {
            foreach ($parent_up as $key => $value)
            {
                $thisparents[] = $value;
            }
        }

        return $thisparents;
    }
    else
    {
        return array();
    }
}


/**
 *  @abstract Get id Parent of Child Menu Looping
 *  @return Data
 *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
**/
function cek_uri_parent_by_id($id="")
{
    $CI =& get_instance();

    $thisparents = array();

    /* Set Field To Select */
    $r = M_menu::where('menu.id_menu',$id);

    /* Get Data */
    $q = $r->get();

    if ($q && $q->count() > 0)
    {
        $res = $q->first();
        $thisparents[] = $res->id_menu;

        $parent_up = cek_uri_parent_by_id($res->menu_parent);
        if (count($parent_up) > 0)
        {
            foreach ($parent_up as $key => $value)
            {
                $thisparents[] = $value;
            }
        }
        return $thisparents;
    }
    else
    {
        return array();
    }
}


/**
 *  @abstract Make List Menu
 *  @return Data
 *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
**/
function get_list_menu()
{
    $CI =& get_instance();
    
    $current   = $CI->uri->segment_array();
    $list_menu = _get_list_menu();

    foreach ($list_menu as $row)
    {
        $id             = $row->id_menu;
        $folder         = !empty($row->menu_folder) ? $row->menu_folder . '/' : '';
        $class          = $row->menu_class;
        $icon           = $row->icon_name;
        $menu           = ucwords($row->menu_name);
        $cek_uri_parent = !in_array('index', $current) ? cek_uri_parent($current) : array();
        
        $href          = !empty($class) ? site_url($folder . $class) : '#';
        $menu_name     = ($href == '#') ? substr(ucwords($row->menu_name), 0, 20) : ucwords($row->menu_name);
        $li_active     = ($href == '#') ? (in_array($id,$cek_uri_parent) ? 'm-menu__item--open m-menu__item--expanded m-menu__item--active' : '') : (in_array($id,$cek_uri_parent) ? 'm-menu__item--open m-menu__item--expanded m-menu__item--active' : '');

        $toggle        = ($href == '#') ? 'm-menu__toggle' : '';
        $arrow         = ($href == '#') ? '<i class="m-menu__ver-arrow la la-angle-right"></i>' : '';

        echo '<li class="m-menu__item  m-menu__item--submenu '.$li_active.'" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                <a href="'.$href.'" class="m-menu__link '.$toggle.'">
                    <i class="m-menu__link-icon '.$icon.'"></i>
                    <span class="m-menu__link-text">
                        '.$menu_name.'
                    </span>
                    '.$arrow.'
                </a>';

        if ($href == '#')
        {
            echo '<div class="m-menu__submenu">';
            echo '<span class="m-menu__arrow"></span>';
            echo '<ul class="m-menu__subnav">';

            get_list_menu_children($id,$current);
            
            echo '</ul>';
            echo '</div>';
        }
        
    }

    echo '
        </li>
    ';
}

/**
 *  @abstract Make List Child Menu
 *  @return Data
 *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
**/
function get_list_menu_children($id_menu="",$current="")
{
    $CI =& get_instance();

    $get_list_child = _get_list_child($id_menu);

    if (count($get_list_child) > 0)
    {

        foreach ($get_list_child as $row)
        {
            $id             = $row->id_menu;
            $folder         = !empty($row->menu_folder) ? $row->menu_folder . '/' : '';
            $class          = $row->menu_class;
            $icon           = $row->icon_name;
            $menu           = ucwords($row->menu_name);
            $cek_uri_parent = !in_array('index',$current) ? cek_uri_parent($current) : array();
            
            $href           = !empty($class) ? site_url($folder . $class) : '#';
            $menu_name      = ($href == '#') ? substr(ucwords($row->menu_name), 0, 20) : ucwords($row->menu_name);
            $li_active      = ($href == '#') ? (in_array($id,$cek_uri_parent) ? 'm-menu__item--expanded m-menu__item--active m-menu__item--open' : '') : (in_array($id,$cek_uri_parent) ? 'm-menu__item--expanded m-menu__item--active' : '');
            $toggle         = ($href == '#') ? 'm-menu__toggle' : '';
            $arrow         = ($href == '#') ? '<i class="m-menu__ver-arrow la la-angle-right"></i>' : '';

            echo '<li class="m-menu__item  m-menu__item--submenu '.$li_active.'" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a href="'.$href.'" class="m-menu__link '.$toggle.'">
                        <i class="m-menu__link-icon '.$icon.'"></i>
                            <span class="m-menu__link-text">
                                '.$menu_name.'
                            </span>
                        '.$arrow.'
                    </a>';

            if ($href == '#')
            {
                echo '<div class="m-menu__submenu">';
                echo '<span class="m-menu__arrow"></span>';
                echo '<ul class="m-menu__subnav">';

                get_list_menu_children($id,$current);
                
                echo '</ul>';
                echo '</div>';
            }

        }

        echo '
            </li>
        ';
    }
}


/**
 *  @abstract Make List Menu Role Application
 *  @return Data
 *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
**/
function get_list_menu_role_app()
{
    $CI =& get_instance();

    $current    = $CI->uri->segment_array();
    $user       = $CI->ion_auth->user()->row();
    $id_user    = $user->id;
 
    $q = M_user::join('users_groups','users_groups.user_id','=','users.id')
                ->where('users.id',$id_user)
                ->where('active','1')
                ->where('users_groups.group_id','1')//Superadmin
                ->first();
    
    // if (count($q) > 0) Jangan DIhapus
    // {
    //     echo 
    //     '<li class="m-menu__item  m-menu__item--submenu '.(in_array('role-application',$current) ? 'm-menu__item--open m-menu__item--expanded m-menu__item--active' : '').'" aria-haspopup="true"  data-menu-submenu-toggle="hover">
    //         <a  href="#" class="m-menu__link m-menu__toggle">
    //             <i class="m-menu__link-icon flaticon-interface-7"></i>
    //             <span class="m-menu__link-text">
    //                 Role Application
    //             </span>
    //             <i class="m-menu__ver-arrow la la-angle-right"></i>
    //         </a>
    //         <div class="m-menu__submenu">
    //             <span class="m-menu__arrow"></span>
    //             <ul class="m-menu__subnav">
    //                 <li class="m-menu__item m-menu__item--submenu '.(in_array('menu',$current) ? 'm-menu__item--open m-menu__item--expanded m-menu__item--active' : '').'" aria-haspopup="true"  data-menu-submenu-toggle="hover">
    //                     <a  href="'.site_url('role-application/menu').'" class="m-menu__link m-menu__toggle">
    //                         <span class="m-menu__link-text">
    //                             Menu
    //                         </span>
    //                     </a>
    //                 </li>
    //                 <li class="m-menu__item m-menu__item--submenu m-menu__item--open '.(in_array('role',$current) ? 'm-menu__item--open m-menu__item--expanded m-menu__item--active' : '').'" aria-haspopup="true"  data-menu-submenu-toggle="hover">
    //                     <a  href="'.site_url('role-application/role').'" class="m-menu__link m-menu__toggle">
    //                         <span class="m-menu__link-text">
    //                             Role
    //                         </span>
    //                     </a>
    //                 </li>
    //                 <li class="m-menu__item m-menu__item--submenu m-menu__item--open '.(in_array('role-permission',$current) ? 'm-menu__item--open m-menu__item--expanded m-menu__item--active' : '').'" aria-haspopup="true"  data-menu-submenu-toggle="hover">
    //                     <a  href="'.site_url('role-application/role-permission').'" class="m-menu__link m-menu__toggle">
    //                         <span class="m-menu__link-text">
    //                             Role Permission
    //                         </span>
    //                     </a>
    //                 </li>
    //                 <li class="m-menu__item m-menu__item--submenu '.(in_array('user',$current) ? 'm-menu__item--open m-menu__item--expanded m-menu__item--active' : '').'" aria-haspopup="true"  data-menu-submenu-toggle="hover">
    //                     <a  href="'.site_url('role-application/user').'" class="m-menu__link m-menu__toggle">
    //                         <span class="m-menu__link-text">
    //                             User Management
    //                         </span>
    //                     </a>
    //                 </li>
    //             </ul>
    //         </div>
    //     </li>';
    // }
    
    if (count($q) > 0)
    {
        echo 
        '<li class="m-menu__item  m-menu__item--submenu '.(in_array('role-application',$current) ? 'm-menu__item--open m-menu__item--expanded m-menu__item--active' : '').'" aria-haspopup="true"  data-menu-submenu-toggle="hover">
            <a  href="#" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-interface-7"></i>
                <span class="m-menu__link-text">
                    Role Application
                </span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item m-menu__item--submenu m-menu__item--open '.(in_array('role-permission',$current) ? 'm-menu__item--open m-menu__item--expanded m-menu__item--active' : '').'" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                        <a  href="'.site_url('role-application/role-permission').'" class="m-menu__link m-menu__toggle">
                            <span class="m-menu__link-text">
                                Role Permission
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item m-menu__item--submenu '.(in_array('user',$current) ? 'm-menu__item--open m-menu__item--expanded m-menu__item--active' : '').'" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                        <a  href="'.site_url('role-application/user').'" class="m-menu__link m-menu__toggle">
                            <span class="m-menu__link-text">
                                User Management
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>';
    }
}


/**
 *  @abstract Get Name of Menu by URI
 *  @return Data
 *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
**/
function get_breadcrumb()
{
    $CI            =& get_instance();
    $current       = $CI->uri->segment_array();

    echo '<li class="m-nav__item m-nav__item--home">
            <a href="'. site_url('dashboard') .'" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home"></i>
            </a>
        </li>';

    if (in_array('role-application',$current))
    {
        for ($i=1; $i <= 2; $i++)
        {

            $thismod = str_replace('-',' ',$current[$i]);

            echo '<li class="m-nav__separator">
                    -
                </li>';
                
            
            echo '<li class="m-nav__item">
                <a href="" class="m-nav__link">
                    <span class="m-nav__link-text">
                        '. ucwords($thismod) .'
                    </span>
                </a>
            </li>';
        }
    }
    else
    {
        $breadcrumb = cek_uri_parent($current);

        for ($i=(count($breadcrumb) - 1); $i >= 0; $i--)
        {
            $q = M_menu::where('id_menu',$breadcrumb[$i])
                        ->get();

            if ($q && $q->count() > 0)
            {   
                echo '<li class="m-nav__separator">
                    -
                </li>';

                $thismod = $q->first();
                echo '<li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                '. ucwords($thismod->menu_name) .'
                            </span>
                        </a>
                    </li>';
            }
        }
    }
}


/**
 *  @abstract Get Name of Menu by URI
 *  @return Data
 *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
**/
function get_menu_name()
{
    $CI =& get_instance();

    $current = $CI->uri->segment_array();

    if (in_array('role-application',$current))
    {

        $thismod = str_replace('-',' ',$current[2]);
        $menu_name =  ucwords($thismod);
        
    }else{
        /* Set Field To Select */
        $r = M_menu::selectRaw('menu_name');

        /* Set Condition */
        for ($i=1; $i <= count($current); $i++) {
            $r->orWhere('menu.menu_class',$current[$i]);
        }

        /* Get Data */
        $q = $r->first();

        if(!empty($q)){
            $menu_name = $q->menu_name;
        }else{
            $menu_name = '';
        }
    }

    return $menu_name;
}

/**
 *  @abstract Get ID of Menu by URI
 *  @return Data
 *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
**/
function get_menu_id()
{
    $CI =& get_instance();

    $current = $CI->uri->segment_array();

    if (in_array('role-application',$current))
    {
        for ($i=1; $i <= 2; $i++)
        {
            $thismod = str_replace('_','-',$current[$i]);
            if($thismod == 'menu'){
                $id_menu = 1;
            }else if ($thismod == 'role') {
                $id_menu = 2;
            }else if ($thismod == 'role-permission') {
                $id_menu = 3;
            }
        }
    }else{
        /* Set Field To Select */
        $r = M_menu::selectRaw('id_menu');

        /* Set Condition */
        for ($i=1; $i <= count($current); $i++) {
            $r->orWhere('menu.menu_class',$current[$i]);
        }

        /* Get Data */
        $q = $r->first();

        if(!empty($q)){
            $id_menu =  $q->id_menu;
        }
    }

    return $id_menu;
}