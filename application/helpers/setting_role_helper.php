<?php defined('BASEPATH') OR exit('No direct script access allowed');

function get_list_child($id_menu="",$sub="",$result2=array())
{
    $CI  =& get_instance();

    $button         = $CI->setting_menu_model->get_list_button();
    $get_list_child = $CI->setting_menu_model->get_list_child_($id_menu);
    $space          = "";
    for ($i=0; $i < $sub; $i++) { 
        $space .= "&nbsp; &nbsp; &nbsp; &nbsp;";
    }
    $sub            = $sub + 1;

    if (count($get_list_child) > 0):

        foreach ($get_list_child as $row):


            $name = empty($row->menu_class) ? '<b>'.$row->menu_name.'</b>' : $row->menu_name;

            echo '
                <tr>
                    <td>
                    </td>
                    <td>'.$space.'
                        <label class="m-checkbox">
                            <input type="checkbox" name="chk[]" class="chk chkchild'.$row->menu_parent.'" value="'.$row->id_menu.'" '.(array_key_exists($row->id_menu, $result2) ? 'checked' : '').' />
                            <span></span>
                        </label>
                        '.$name.'
                    </td>
            ';

            foreach ($button as $row2) {
                $q = $CI->setting_menu_model->get_list_menubutton($row->id_menu, $row2->id_button);
                if ($q) {
                    echo '
                        <td class="text-center">
                            <label class="m-checkbox">
                                <input type="checkbox" name="chkbutton['.$row->id_menu.'][]" class="chkbutton" value="'.$q->id_menu_button.'" '.(array_key_exists($row->id_menu, $result2) ? (in_array($q->id_menu_button, $result2[$row->id_menu]) ? 'checked' : '') : '').' />
                                <span></span>
                            </label>
                        </td>
                    ';
                } else {
                    echo '
                        <td class="text-center">
                        </td>
                    ';
                }
            }

            echo '
                </tr>
            ';

            if (empty($row->menu_class)):
                $sub_menu = get_list_child($row->id_menu, $sub, $result2);
            endif;

        endforeach;

    endif;
}
