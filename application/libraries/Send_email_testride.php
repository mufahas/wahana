<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Send_email_testride 
{
    protected $CI;

    function __construct() 
    {
        $this->CI =& get_instance();
    }

    public function email($email, $nama, $message)
    {
        require APPPATH . 'third_party/phpmailer/PHPMailerAutoload.php';

        //Create a new PHPMailer instance
        $mail = new PHPMailer;

        //Tell PHPMailer to use SMTP
        $mail->isSMTP();

        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        // $mail->SMTPDebug = 4;

        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';

        //Set the hostname of the mail server
        // $mail->Host = 'smtp.gmail.com';
        // use
        $mail->Host = gethostbyname('mail.wahanaartha.com');
        // if your network does not support SMTP over IPv6

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 587;

        //Set the encryption system to use - ssl (deprecated) or tls
        $mail->SMTPSecure = 'tls';
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;
        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = "promotion@wahanaartha.com";

        //Password to use for SMTP authentication
        $mail->Password = "prowms2019";

        //Set who the message is to be sent from
        $mail->setFrom('promotion@wahanaartha.com', 'Wanda Sales Station');

        //Set an alternative reply-to address
        // $mail->addReplyTo('tkazumi66@gmail.com', 'First Last');

        //Set who the message is to be sent to
        $mail->addAddress($email, $nama);
        $mail->AddBCC('promotion@wahanahonda.com', 'Wanda Sales Station');

        //Set the subject line
        $mail->Subject = 'Wahana Honda Sales Station';

        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->msgHTML($message);
                            // 1, 2, 3, 4, 5
        $mail->crlf = "\n";                     // "\r\n" or "\n" or "\r"
        $mail->newline = "\n";  

        //Replace the plain text body with one created manually
        $mail->AltBody = 'WahanaHonda';

        //To use HTML
        $mail->IsHTML(true);

        //Attach an image file
        // $mail->addAttachment($file.$namefile.'.pdf');
        // $mail->addAttachment('images/phpmailer_mini.png');

        // image cid
        // $mail->AddEmbeddedImage('img/2u_cs_mini.jpg', 'logo_2u');

        //send the message, check for errors
        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            // $this->session->set_flashdata('success', "Approve succesfully.");
            // redirect('back-armour/registers');
            //Section 2: IMAP
            //Uncomment these to save your message in the 'Sent Mail' folder.
            #if (save_mail($mail)) {
            #    echo "Message saved!";
            #}
        }
    }
}
