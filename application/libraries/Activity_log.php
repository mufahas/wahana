<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Activity_log 
{
    protected $CI;

    function __construct() 
    {
        $this->CI =& get_instance();
    }

    /**
     *  @abstract Log setting
     *  @return setting
     *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
    **/
    public function setup($menu_name)
    {
        $dir        = FCPATH . "application/logs/log_activity/";
        $file       = "log_" . str_replace(' ','_',strtolower($menu_name));
        $path_file  = $dir . $file . ".txt";

        $setup['delimiter_id']  = "|%|";
        $setup['delimiter_row'] = "|#|";
        $setup['delimiter']     = "|";
        $setup['path_file']     = $path_file;

        return $setup;
    }

    /**
     *  @abstract Insert Log file
     *  @return file .txt
     *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
    **/
    function create($data_new, $data_old, $data_change, $message, $activity) 
    {
        /* Get id user by login */
        $user      = $this->CI->ion_auth->user()->row();
        $role      = $this->CI->ion_auth->get_users_groups($user->id)->row()->name;
        $full_name = $user->first_name . ' ' . $user->last_name;

        /* Clean activity */
        $clean_activity = ucwords(str_replace('_',' ',$activity));

        /* Get menu name */
        $menu_name = get_menu_name();

        $data  = array(
                        'id'          => '',
                        'actor'       => $full_name,
                        'role'        => $role,
                        'menu'        => $menu_name,
                        'data_new'    => $data_new,
                        'data_old'    => $data_old,
                        'data_change' => $data_change,
                        'message'     => $message,
                        'activity'    => $clean_activity,
                        'created_on'  => date('Y-m-d H:i:s'), 
                        ); 

        $setup = $this->setup($menu_name);

        if (file_exists($setup['path_file']))
        {
            /* If file exist */
            $file_exists = file_get_contents($setup['path_file']);
            $file_exists = explode($setup['delimiter_id'],$file_exists);

            $log_ext    = explode($setup['delimiter_row'],$file_exists[1]);
            for ($i=0; $i < count($log_ext); $i++) { 
                $log_ext_real[$i] = $log_ext[$i];
            }

            $last_id   = explode($setup['delimiter'],end($log_ext_real));
            if(!empty($data['id'])){
                $id = $data['id'];
            }else{
                $id = current($last_id) + 1;                
            }

            $log      = $setup['delimiter_row'];
            $log     .= $id;
            foreach ($data as $key => $value)
            {
                if($key != 'id'){
                    $log   .= $setup['delimiter'] . $value;
                }
            }

            file_put_contents($setup['path_file'],$log,FILE_APPEND);
        }
        else
        {
            /* If file not exist, create new */
            if(!empty($data['id'])){
                $id = $data['id'];
            }else{
                $id = 1;
            }
            
            $header    = "id";
            $log      = $id;
            foreach ($data as $key => $value)
            {
                if($key != 'id'){
                    $header .= $setup['delimiter'] . $key;
                    $log   .= $setup['delimiter'] . $value;
                }
            }

            // $log   .= $setup['delimiter_row'];
            $header .= $setup['delimiter_id'];

            file_put_contents($setup['path_file'],$header,FILE_APPEND);
            file_put_contents($setup['path_file'],$log,FILE_APPEND);
        }
    }

    /**
     *  @abstract Get header Log file
     *  @return Array data
     *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
    **/
    public function header($menu_name)
    {
        $setup = $this->setup($menu_name);

        if (file_exists($setup['path_file']))
        {
            $file_exists = file_get_contents($setup['path_file']);
            $file_exists = explode($setup['delimiter_id'],$file_exists);

            $header_ext  = explode($setup['delimiter'],$file_exists[0]);

            return $header_ext;
        }
        else
        {
            return array();
        }

    }

    /**
     *  @abstract Get content Log file
     *  @return Array data
     *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
    **/
    public function contents($id="",$menu_name="")
    {
        $setup = $this->setup($menu_name);

        if (file_exists($setup['path_file']))
        {
            $file_exists = file_get_contents($setup['path_file']);
            $file_exists = explode($setup['delimiter_id'],$file_exists);

            $header_ext  = $this->header($menu_name);
            if (is_array($file_exists))
            {
                $log_ext    = explode($setup['delimiter_row'],$file_exists[1]);
                for ($i=0; $i < count($log_ext); $i++)
                {
                    $log_ext_real[$i] = array();
                    $detail[$i]        = explode($setup['delimiter'],$log_ext[$i]);

                    for ($x=0; $x < count($header_ext); $x++) { 
                        $log_add[$i][$header_ext[$x]] = $detail[$i][$x];
                    }

                    $log_ext_real[$i] = (object) $log_add[$i];
                }
            }
            else
            {
                $log_ext_real = array();
                $detail        = explode($setup['delimiter'],$file_exists[1]);
                for ($i=0; $i < count($header_ext); $i++) { 
                    $log_add[$header_ext[$i]] = $detail[$i];
                }

                $log_ext_real[] = (object) $log_add;
            }

            if (!empty($id))
            {
                for ($i=0; $i < count($log_ext_real); $i++) { 
                    if ($log_ext_real[$i]->id == $id) {
                        $log_ext_real = (object) $log_ext_real[$i];
                    }
                }
            }
            return $log_ext_real;
        }
        else
        {
            return array();
        }
    }
}
