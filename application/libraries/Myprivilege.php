<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Myprivilege {

    protected $CI;
    public $crud   = "";
    public $button = "";
    public $data   = "";
    public $site   = "";

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    /**
     * @abstract Check privilege page
     * @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
    **/
    public function cekPrivilegePage()
    {
        $current = $this->CI->uri->segment_array();
        $user    = $this->CI->ion_auth->user()->row();
        $id_user = $user->id;
    
        
        $q = M_menu::selectRaw('menu_class');

        for ($i=1; $i <= count($current); $i++)
        {
            $q->orWhere('menu.menu_class',$current[$i]);
        }

        $hasil = $q->first();

        if(!empty($hasil))
        {
            $page = $this->queryCekPrivilegePage($id_user, $hasil->menu_class);
        
            if($page != 1)
            {
                die("You can't access this page. Please contact administrator.");
            }
        }
        else
        {
            if (in_array('role-application',$current))
            {
                $model_user = M_user::join('users_groups','users_groups.user_id','=','users.id')
                                    ->where('users.id',$id_user)
                                    ->where('active','1')
                                    ->where('users_groups.group_id','1')//Superadmin
                                    ->first();

                if(empty($model_user))
                {
                    die("You can't access this page. Please contact administrator.");
                }
            }
        }
    }


    /**
     * @abstract Query for check privilege page
     * @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
    **/
    public function queryCekPrivilegePage($id_user,$class)
    {
        $check = M_user_role::where('menu.menu_class',$class)
                            ->where('users_groups.user_id',$id_user)
                            ->where('menu.menu_status','1')
                            ->where('groups.is_delete','f')
                            ->join('groups','groups.id','=','users_groups.group_id')
                            ->join('role_access','role_access.id_group','=','groups.id')
                            ->join('menu','menu.id_menu','=','role_access.id_menu')
                            ->get();

        if ($check->count() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @abstract Check privilege button
     * @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
    **/
    public function cekPrivilegeButton()
    {
        $current = $this->CI->uri->segment_array();
        $user    = $this->CI->ion_auth->user()->row();
        $id_user = $user->id;
    

        /* Get Status SuperAdmin */        
        $user = M_user::where('id',$id_user)->first();
        $sa   = false;

        if($user) $sa = true;

        /* Get Privilege Button */
        $q = M_menu::selectRaw('menu_class');

        for ($i=1; $i <= count($current); $i++)
        {
            $q->orWhere('menu.menu_class',$current[$i]);
        }

        $hasil = $q->first();

        $r = M_user_role::selectRaw('button.button_name')
                        ->where('users_groups.user_id',$id_user)
                        ->where('button.button_status','1')
                        ->where('button.is_privilege','1')
                        ->where('groups.is_delete','f')

                        ->join('groups','groups.id','=','users_groups.group_id')
                        ->join('role_access','role_access.id_group','=','groups.id')
                        ->join('menu','menu.id_menu','=','role_access.id_menu')
                        ->join('role_button','role_button.id_role_access','=','role_access.id_role_access')
                        ->join('menu_button','menu_button.id_menu_button','=','role_button.id_menu_button')
                        ->join('button','button.id_button','=','menu_button.id_button');

        if(!empty($hasil)){
            $r->where('menu.menu_class',$hasil->menu_class);
        }

        $check = $r->get();

        if ($check && $check->count() > 0)
        {
            foreach ($check as $value)
            {
                $this->crud[] = $value->button_name;
            }
        }

        /* Get All Button */
        $check = M_button::selectRaw('button.button_name,
                                    icon.icon_name,
                                    button.button_class,
                                    button.button_title,
                                    button.button_text,
                                    button.button_type,
                                    button.button_attribute,
                                    button.is_privilege')
                        ->leftjoin('icon','icon.id_icon','=','button.id_icon')
                        ->where('button.button_status','1')
                        ->get();

        if ($check->count() > 0)
        {
            foreach ($check as $value)
            {
                $configs = array(
                                'name'         => $value->button_name,
                                'icon'         => $value->icon_name,
                                'class'        => $value->button_class,
                                'title'        => $value->button_title,
                                'text'         => $value->button_text,
                                'type'         => $value->button_type,
                                'attribute'    => $value->button_attribute,
                                'is_privilege' => $value->is_privilege
                            );
                $this->button[$value->button_name]      = $configs;
                $this->data['btn_'.$value->button_name] = "";
            }
        }


        /* Create Button Available */
        if (!empty($this->button))
        {
            /*
                Type Button:
                1 = Button Default
                2 = Submit
                3 = Link
            */
           
            foreach ($this->button as $key => $value)
            {
                $thishref        = $value['type'] == "3" ? site_url() . $this->CI->uri->uri_string . "/" . $key : "";
                $thisclass       = $key . ' ' . $value['class'];
                $thistitle       = $value['title'];
                $thisdesc        = '<i class="' . $value['icon'] . '"></i> ' . $value['text'];
                $thisattr        = $value['attribute'];
                
                if ($value['type'] == "1")
                {
                    $thisbutton  = '<button type="button" class="' . $thisclass . '" title="' . $thistitle . '" '.$thisattr.'>' . $thisdesc . '</button>';
                }
                else if ($value['type'] == "2")
                {
                    $thisbutton  = '<button type="submit" class="' . $thisclass . '" title="' . $thistitle . '" '.$thisattr.'>' . $thisdesc . '</button>';
                }
                else if ($value['type'] == "3")
                {
                    $thisbutton  = '<a href="' . $thishref . '" class="' . $thisclass . '" title="' . $thistitle . '" '.$thisattr.'>' . $thisdesc . '</a>';
                }
                else
                {
                    $thisbutton  = "";
                }

                if ($value['is_privilege'] == "1")
                {
                    if ($sa === true && in_array('role_application',$current))
                    {
                        $data['btn_'.$key] = $thisbutton;
                    }
                    else if (!empty($this->crud))
                    {
                        $data['btn_'.$key] = in_array($key, $this->crud) ? $thisbutton : "";
                    }
                    else
                    {
                        $data['btn_'.$key] = "";
                    }
                }
                else
                {
                    $data['btn_'.$key]     = $thisbutton;
                }
            }
        }

        return $data;
    }
}