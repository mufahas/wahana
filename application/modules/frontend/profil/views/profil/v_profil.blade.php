@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Profil PT. Wahana Makmur Sejati | Wahana Honda</title>
<meta name="description" content="PT. Wahana Makmur Sejati merupakan salah satu usaha yang Tergantung dalam Wahana Artha Group yang bergerak di bidang distribusi sepeda motor Honda">
@endsection

@section('body')
    <style type="text/css">
        .headerAhay 
        {
            height: 350px; 
            background: url('{{base_url()}}assets/frontend/img/BG-3.jpg');  
            background-size: cover; 
            background-repeat: no-repeat;
        }
        .litleWord {
            padding-top: 10px;
            font-size: 14px;
            color: #6d6e71;
        }
    </style>
    
    <div id="for_container">
        <div class="headerAhay pt-5">
            <div class="container-fluid container pt-5">
                <h1 class="text-white pt-5">Profile WMS</h1>
            </div>
        </div>
    
        <div class="container pt-5 pb-5">
    
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <img src="{{base_url()}}assets/frontend/img/profil/wahana logo.png" class="img-fluid" alt="Logo Wahana" title="Logo Wahana">
                    </div>
                    <div class="col-md-6 shitWord">
                        {!! $profile->deskripsi_tentang_wahana !!}
                        <!--<p align="justify">-->
                        <!--    PT Wahana Makmur Sejati merupakan salah satu anak perusahaan dari-->
                        <!--    Wahanaarta Group yang bergerak di bidang industri sepeda motor Honda.-->
                        <!--    Sejak didirikan pada 6 Agustus 1072, WMS ditetapkan oleh PT. Astra-->
                        <!--    Honda Motor (AHM) sebagai Main Dealer Sepeda Motor Honda untuk wilayah-->
                        <!--    Jakarta dan Tangerang.-->
                        <!--</p>-->
                        <!--<br> -->
                        <!--<p align="justify">-->
                        <!--    Sebagai Main Dealer sepeda motor Honda wilayah Jakarta dan Tangerang,-->
                        <!--    PT WMS membawahi 119 Dealer &amp; 394 AHASS.-->
                        <!--</p>-->
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
    
            <div class="col-lg-12 pb-3 pt-3" align="center">
                <h2 class="text-dark"><b>Layanan WMS</b></h2>
            </div>
    
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-3 pb-3">
                        <center>
                            <img src="{{base_url()}}assets/frontend/img/profil/indent.png" alt="Indent System" title="Indent System">
                            <h3 class="litleBlackWord pt-2">
                                INDENT SYSTEM
                            </h3>
                            <div class="litleWord">WMS adalah Main Dealer sepeda motor pertama yang mengaplikasikan indent system.</div>
                        </center>
                    </div>
                    <div class="col-md-3  pb-3">
                        <center>
                            <img src="{{base_url()}}assets/frontend/img/profil/asuransi.png" alt="Asuransi" title="Asuransi">
                            <h3 class="litleBlackWord pt-2">
                                ASURANSI
                            </h3>
                            <div class="litleWord">WMS juga memberikan perlindungan dan manfaat lebih kepada konsumen melalui asuransi.</div>
                        </center>
                    </div>
                    <div class="col-md-3  pb-3">
                        <center>
                            <img src="{{base_url()}}assets/frontend/img/profil/vip.png" alt="Vip Card" title="Vip card">
                            <h3 class="litleBlackWord pt-2">
                                VIP CARD
                            </h3>
                            <div class="litleWord">Program loyalitas yang dirancang untuk memberikan berbagai nilai lebih bagi para pelanggan setia perusahaan.</div>
                        </center>
                    </div>
                    <div class="col-md-3  pb-3">
                        <center>
                            <img src="{{base_url()}}assets/frontend/img/profil/help.png" alt="Wahana Help" title="Wahana Help">
                            <h3 class="litleBlackWord pt-2">
                                WAHANA HELP
                            </h3>
                            <div class="litleWord">Honda Emengency Layanan Pelanggan (HELP) bagi pengendara Honda sehingga tidak perlu kuatir apabila mogok di wilayah WMS.</div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')
<script type="text/javascript">
    // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="All News">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });
</script>
@stop