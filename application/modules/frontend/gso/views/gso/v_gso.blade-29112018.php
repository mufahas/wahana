@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Wahana Honda | GSO</title>
@endsection

@section('body')
    
    <div id="for_container">
        <div class="bg-white mb-5 mt-5 pb-5 pt-5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="gso-logo">
                            <img src="{{ base_url() }}assets/frontend/img/GSO.png" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 t-md">
                        <h3 class="text-23">Group Sales Operation</h3>
                        <p class="mt-3">
                            GSO – WAHANA adalah Divisi khusus Sales PT. Wahana Makmur Sejati (Main Dealer Sepada Motor Honda wilayah Jakarta-Tangerang) yang khusus melayani
                            pembelian sepeda motor Honda oleh Group Customer (non-retail) baik dari instansi pemerintah maupun non instansi seperti Perusahaan Swasta, BUMD / BUMN, Bank Pemerintah dan Koperasi Pemerintah / Swasta.
                        </p>
                        <a href="https://api.whatsapp.com/send?phone=6289601838126" target="_blank">
                            <div class="d-flex align-items-center icon-wa">
                                <img src="{{ base_url() }}assets/frontend/img/wa.png" class="p-1" width="30px" height="30px">
                                <b class="fs-14 text-wa">Hubungi Kami via WhatsApp</b>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-6 no-padding">
                    <a id="aLayananVip" href="#" class="gso-btn btn-block">Layanan VIP</a>
                </div>
                <div class="col-md-3 col-6 no-padding">
                    <a id="aFasilitasKelebihan" href="#" class="gso-btn btn-block">Fasilitas &amp; Kelebihan</a>
                </div>
                <div class="col-md-3 col-6 no-padding">
                    <a href="{{$path . $gso->presentasi_produk}}" class="gso-btn btn-block">Product Presentation</a>
                </div>
                <div class="col-md-3 col-6 no-padding">
                    <a href="{{$path . $gso->daftar_harga}}" class="gso-btn btn-block">Price List</a>
                </div>
            </div>
        </div>
    
        <div id="layananVIP" class="bg-red-custom pt-12 pb-12 display-none">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div>
                            <img src="{{ base_url() }}assets/frontend/img/gso-vip.png" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 t-md">
                        <h3 class="text-26 text-white-custom">Layanan VIP</h3>
                        <div class="text-15 text-white-custom">
                            {!! $gso->layanan_vip !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <div id="fasilitasKelebihan" class="bg-red-custom pt-12 pb-12 display-none">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div>
                            <img src="{{ base_url() }}assets/frontend/img/gso-vip.png" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 t-md">
                        <h3 class="text-26 text-white-custom">Fasilitas &amp; Kelebihan</h3>
                        <div class="text-15 text-white-custom">
                            {!! $gso->fasilitas_kelebihan !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    
        <div class="container-fluid bg-dark-grey pt-5 pb-5">
            <div class="form-order-ride">
            @if(!empty($message_success)) 
                    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show success" role="alert">
                        <div class="m-alert__icon">
                            <i class="la la-warning"></i>
                        </div>
                        <div class="m-alert__text">
                            <strong>
                                Pesan Terkirim !
                            </strong>
                            {{$message_success}}
                        </div>
                        <div class="m-alert__close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        </div>
                    </div>
                    @endif
                    @if(!empty($message_error))
                    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-error alert-dismissible fade show error" role="alert">
                        <div class="m-alert__icon">
                            <i class="la la-warning"></i>
                        </div>
                        <div class="m-alert__text">
                            <strong>
                                Opps!
                            </strong>
                            {{$message_error}}
                        </div>
                        <div class="m-alert__close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        </div>
                    </div>
                    @endif
                <div class="text-center bg-red pb-2 pt-2">
                    <h3 class="text-23 mb-0 text-white">Hubungi Kami</h3>
                </div>
                <div class="detail-right">
                    Silahkan mengisi form untuk Informasi atau Pemesanan Motor khusus Perusahaan / Group.
                </div>
                {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
                    <div class="form-order bg-white">
                        <div class="form-group">
                          <input type="text" name="nama_lengkap" class="form-control" id="" placeholder="Nama Lengkap">
                        </div>
                        <div class="form-group">
                          <input type="text" name="perusahaan" class="form-control" id="" placeholder="Perusahaan">
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                  <input type="email" name="email" class="form-control" id="" placeholder="Email" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                  <input type="text" name="nomor_hp" class="form-control numbers" id="" placeholder="08xxxxxxxxxx" onkeypress="return isNumber(event)" maxlength="13">
                                </div>
                            </div>

                            <div class="form-group" align="right">
                                <button type="button" class="btn btn-danger btn-round button-verifikasi fs-12">Verifikasi No. Hp</button>
                            </div>

                            <div class="form-group">
                              <input type="hidden" name="confirm_code" class="form-control">
                            </div>

                            <!-- Set Input Kode Verifikasi -->
                            <div class="form-group nomor_verifikasi" style="display: none;">
                                <label>Nomor Verifikasi</label>
                                <input type="text" class="form-control" name="nomor_verifikasi" onkeypress="return isNumber(event)" maxlength="6">
                            </div>
                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show sent-message-success" role="alert" style="display: none;">
                                <i class="fa fa-envelope-o"></i> Kode Verifikasi Dikirim 
                            </div>
                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show sent-message-fail" role="alert" style="display: none;">
                                <i class="fa fa-remove"></i> Kode Verifikasi Gagal Dikirim !
                            </div>
                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show status-message-wrong" role="alert" style="display: none;">
                                <i class="fa fa-remove"></i> Kode Verifikasi Salah ! 
                            </div>
                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show status-message-right" role="alert" style="display: none;">
                                <i class="fa fa-check"></i> Kode Verifikasi Benar 
                            </div>
                            <!-- Set Input Kode Verifikasi -->
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control"  name="lokasi" id="" placeholder="Lokasi">
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control"  name="subyek_pesan" id="" placeholder="Subyek Pesan">
                        </div>
                        <div class="form-group">
                          <textarea name="isi_pesan" class="form-control" rows="8" cols="80" placeholder="Isi Pesan"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-left"></label>
                                    <div class="g-recaptcha" data-sitekey="6Lcb8VcUAAAAAD94jt6KFVp-rgY_VXTx55lv0y2R"></div>
                                    <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" required>
                                </div>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="check[]" class="custom-control-input" id="customControlAutosizing">
                            <label class="custom-control-label text-grey" for="customControlAutosizing">Saya bersedia dihubungi oleh team marketing Wahana Honda</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn bg-red text-white btn-lg btn-block save">SAYA BERMINAT</button>
                    </div>
                {!! form_close() !!}
            </div>
        </div>
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <h2 class="text-white">Berita WMS</h2>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->
@stop

@section('scripts')
<script src="{{ base_url() }}assets/frontend/js/pesan_gso/js/pesan-gso.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/frontend/js/validate/jquery.validate.js" type="text/javascript"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">

    var getVerifikasiKode   = '{{$getVerifikasiKode}}';

    // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}news-detail/'+v.judul_berita_url+'">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });
</script>
@stop