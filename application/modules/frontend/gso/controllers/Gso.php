<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gso extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();

        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {   
        $data['path']            = site_url() . 'assets/upload/gso/';
        $data['gso']             = ms_gso::orderby('ms_gso.kode_gso_wahana','ASC')->first();
        
        $data['action']          = site_url() . $this->site . '/save';
        
        $data['message_success'] = $this->session->flashdata('message_success');
        $data['message_error']   = $this->session->flashdata('messages_error');

        /* Ajax Verification Phone Number */
        $data['getVerifikasiKode'] = site_url() . $this->site . '/ajax_get_verifikasi_kode';

        /* CSRF */
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf_value'] = $this->security->get_csrf_hash();
        
        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        
        $this->load_view_frontend("frontend","gso","gso","v_gso",$data);
    }

    function ajax_get_verifikasi_kode()
    {
        $no_hp           = $this->security->xss_clean($this->input->post('no_hp'));
        
        $string          = '0123456789';
        $string_shuffled = str_shuffle($string);
        $password        = substr($string_shuffled, 1, 6);
        $pesan           = 'Wahana Honda GSO: Angka verifikasi Anda adalah: '.$password;
        
        $curl            = curl_init();
      
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain?&output=json',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'user'     => 'wahanaartha_api',
                'password' => 'Wahana321+',
                'SMSText'  => $pesan,
                'GSM'      => $no_hp
            )
        ));

        $resp = curl_exec($curl);
        
        $data = array('resp' => $resp,'password' => $password);

        if (!$resp) 
        {
            die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        } 
        else 
        {
            echo json_encode($data);
        }
        
        curl_close($curl);
    }

    function save()
    {   
        
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $url                        = site_url() . $this->site;
            
            $nama_lengkap               = $this->security->xss_clean($this->input->post('nama_lengkap'));
            $perusahaan                 = $this->security->xss_clean($this->input->post('perusahaan'));
            $email                      = $this->security->xss_clean($this->input->post('email'));
            $nomor_handphone            = $this->security->xss_clean($this->input->post('nomor_hp'));
            $lokasi                     = $this->security->xss_clean($this->input->post('lokasi'));
            $subyek_pesan               = $this->security->xss_clean($this->input->post('subyek_pesan'));
            $isi_pesan                  = $this->security->xss_clean($this->input->post('isi_pesan'));

            // $recaptcha                  = trim($this->input->post('g-recaptcha-response'));


            $tbl_pesan                  = new tbl_pesan;
            
            $tbl_pesan->nama_lengkap    = ucfirst($nama_lengkap);
            $tbl_pesan->nama_perusahaan = ucfirst($perusahaan);
            $tbl_pesan->email           = strtolower($email);
            $tbl_pesan->nomor_telepon   = $nomor_handphone;
            $tbl_pesan->lokasi          = ucwords($lokasi);
            $tbl_pesan->subyek_pesan    = $subyek_pesan;
            $tbl_pesan->isi_pesan       = $isi_pesan;
            $tbl_pesan->tipe_pesan      = 'GSO';
            
            $save                       = $tbl_pesan->save();

            if($save) {       
                $this->session->set_flashdata('message_success', 'Pesan Berhasil Dikirim ');
                redirect(site_url() . $this->site);
            } else {
                $this->session->set_flashdata('message_error', 'Pesan Gagal Dikirim');
                redirect(site_url() . $this->site);
            }

        } else {
            $this->session->set_flashdata('message_error', 'Pesan Gagal Dikirim');
            redirect(site_url() . $this->site);
        }
    }
}