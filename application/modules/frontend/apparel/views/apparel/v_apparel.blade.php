@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Spare Part dan Aksesoris Motor Honda | Wahana Honda</title>
<meta name="description" content="Daftar spare parts & aksesoris sepeda motor honda dengan pilihan terlengkap. Tersedia beragam jenis onderdil resmi motor honda untuk kebutuhan bengkel Anda">
@endsection

@section('body')
    <div id="for_container">

        <nav aria-label="breadcrumb" class="breadcrumb-custom">
          <div class="container-fluid">
              <div class="container">
                    <ol class="breadcrumb mb-0 pb-1 pt-1">
                      <li class="breadcrumb-item"><a href="{{base_url()}}" title="Wahana">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Aksesoris</li>
                    </ol>
              </div>
          </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="owl-carousel owl-theme slide-aksesoris">
                    <?php 
                        foreach ($banner as $key) { ?>
                        <div class="item">
                            <img src="{{ $path_foto_banner . $key->nama_gambar }}" class="img-fluid" alt="{{ $key->nama_gambar }}" title="{{ $key->nama_gambar }}">
                        </div>
                    <?php    } ?>
                    
                </div>
            </div>
        </div>
    
        <div class="container-fluid honda-genue"></div>
    
        <div class="container mt-4">
            <div class="text-center">
                <h2 class="small-title">Aksesoris &amp; Apparel Honda</h2>
                <hr class="separate-line" style="margin:auto;">
            </div>
            <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link menu-list news-link active" id="kemeja-tab" data-toggle="tab" href="#kemeja" role="tab" aria-controls="kemeja" aria-selected="false" title="Kemeja">Kemeja</a>
              </li>
              <li class="nav-item">
                <a class="nav-link menu-list news-link" id="topi-tab" data-toggle="tab" href="#topi" role="tab" aria-controls="topi" aria-selected="false" title="Topi">Topi</a>
              </li>
              <li class="nav-item">
                <a class="nav-link menu-list news-link" id="kaos-tab" data-toggle="tab" href="#kaos" role="tab" aria-controls="kaos" aria-selected="false" title="Kaos">Kaos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link menu-list news-link" id="helm-tab" data-toggle="tab" href="#helm" role="tab" aria-controls="helm" aria-selected="false" title="Helm">Helm</a>
              </li>
              <li class="nav-item">
                <a class="nav-link menu-list news-link" id="jacket-tab" data-toggle="tab" href="#jacket" role="tab" aria-controls="jacket" aria-selected="false" title="Jacket">Jacket</a>
              </li>
              <li class="nav-item">
                <a class="nav-link menu-list news-link" id="sarungtangan-tab" data-toggle="tab" href="#sarungtangan" role="tab" aria-controls="sarungtangan" aria-selected="false" title="Sarung Tangan">Sarung Tangan</a>
              </li>
              <li class="nav-item">
                <a class="nav-link menu-list news-link" id="protector-tab" data-toggle="tab" href="#protector" role="tab" aria-controls="protector" aria-selected="false" title="Protector">Protector</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="kemeja" role="tabpanel" aria-labelledby="kemeja">
                    <div class="row">
                        <?php 
                            if(!empty($kemeja)){
                                foreach ($kemeja as $key1) { ?>
                                    <?php
                                        if(!empty($key1->gambar_apparel)) { ?>
                                           <div class="col-md-4 mb-5" align="center">
                                                <img src="{{ $path_apparel . $key1->gambar_apparel}}" class="img-fluid" alt="{{$key1->nama_apparel}}" title="{{$key1->nama_apparel}}">
                                                <p class="mb-0">{{$key1->nama_apparel}}</p>
                                                <p class="text-red"> {{$key1->harga_apparel != 0 ? 'Rp. '.number_format($key1->harga_apparel,'0',',','.') : 'Rp. -'}}</p>
                                                <a class="btn btn-danger btn-round btn-block" target="_blank" href="{{ base_url() }}apparel/beli-apparel/{{ $key1->nama_apparel_url }}" title="Beli Produk"><i class="la-shopping-cart"></i>Beli Produk</a>
                                            </div>                              
                                    <?php } ?>
                                <?php  } ?>
                        <?php } else {  ?>
                            <div class="col-md-12 mr-auto ml-auto mt-3 mb-3" align="center">
                                <p class="text-red">Apparel Belum Tersedia</p>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="tab-pane fade" id="topi" role="tabpanel" aria-labelledby="topi">
                    <div class="row">
                        <?php 
                            if(!empty($topi)){
                                foreach ($topi as $key2) { ?>
                                    <?php 
                                        if(!empty($key2->gambar_apparel)) { ?>
                                            <div class="col-md-4 mb-5" align="center">
                                                <img src="{{ $path_apparel . $key2->gambar_apparel}}" class="img-fluid" alt="{{$key2->nama_apparel}}" title="{{$key2->nama_apparel}}">
                                                <p class="mb-0">{{$key2->nama_apparel}}</p>
                                                <p class="text-red"> {{$key2->harga_apparel != 0 ? 'Rp. '.number_format($key2->harga_apparel,'0',',','.') : 'Rp. -'}}</p>
                                                <a class="btn btn-danger btn-round btn-block" target="_blank" href="{{ base_url() }}apparel/beli-apparel/{{ $key2->nama_apparel_url }}" title="Beli Produk"><i class="la-shopping-cart"></i>Beli Produk</a>
                                            </div>     
                                    <?php } ?>
                                <?php  } ?>
                        <?php } else { ?>
                            <div class="col-md-12 mr-auto ml-auto mt-3 mb-3" align="center">
                                <p class="text-red">Apparel Belum Tersedia</p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
    
                <div class="tab-pane fade" id="kaos" role="tabpanel" aria-labelledby="kaos">
                   <div class="row">
                       <?php 
                            if(!empty($kaos)){
                                foreach ($kaos as $key3) { ?>
                                <?php 
                                    if(!empty($key3->gambar_apparel)) { ?>
                                        <div class="col-md-4 mb-5" align="center">
                                            <img src="{{ $path_apparel . $key3->gambar_apparel}}" class="img-fluid" alt="{{$key3->nama_apparel}}" title="{{$key3->nama_apparel}}">
                                            <p class="mb-0">{{$key3->nama_apparel}}</p>
                                            <p class="text-red"> {{$key3->harga_apparel != 0 ? 'Rp. '.number_format($key3->harga_apparel,'0',',','.') : 'Rp. -'}}</p>
                                            <a class="btn btn-danger btn-round btn-block" target="_blank" href="{{ base_url() }}apparel/beli-apparel/{{ $key3->nama_apparel_url }}" title="Beli Produk"><i class="la-shopping-cart"></i>Beli Produk</a>
                                        </div> 
                                    <?php    } ?>
                                <?php  } ?>
                        <?php } else { ?>
                            <div class="col-md-12 mr-auto ml-auto mt-3 mb-3" align="center">
                                <p class="text-red">Apparel Belum Tersedia</p>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="tab-pane fade" id="helm" role="tabpanel" aria-labelledby="helm">
                   <div class="row">
                       <?php 
                            if(!empty($helm)){
                                foreach ($helm as $key4) { ?>
                                <?php 
                                    if(!empty($key4->gambar_apparel)) { ?>
                                        <div class="col-md-4 mb-5" align="center">
                                            <img src="{{ $path_apparel . $key4->gambar_apparel}}" class="img-fluid" alt="{{$key4->nama_apparel}}" title="{{$key4->nama_apparel}}">
                                            <p class="mb-0">{{$key4->nama_apparel}}</p>
                                            <p class="text-red"> {{$key4->harga_apparel != 0 ? 'Rp. '.number_format($key4->harga_apparel,'0',',','.') : 'Rp. -'}}</p>
                                            <a class="btn btn-danger btn-round btn-block" target="_blank" href="{{ base_url() }}apparel/beli-apparel/{{ $key4->nama_apparel_url }}" title="Beli Produk"><i class="la-shopping-cart"></i>Beli Produk</a>
                                        </div> 
                                    <?php    } ?>
                                <?php  } ?>
                        <?php } else {  ?>
                            <div class="col-md-12 mr-auto ml-auto mt-3 mb-3" align="center">
                                <p class="text-red">Apparel Belum Tersedia</p>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="tab-pane fade" id="jacket" role="tabpanel" aria-labelledby="jacket">
                   <div class="row">
                       <?php 
                            if(!empty($jacket)){
                                foreach ($jacket as $key5) { ?>
                                <?php 
                                    if(!empty($key5->gambar_apparel)) { ?>
                                        <div class="col-md-4 mb-5" align="center">
                                            <img src="{{ $path_apparel . $key5->gambar_apparel}}" class="img-fluid" alt="{{$key5->nama_apparel}}" title="{{$key5->nama_apparel}}">
                                            <p class="mb-0">{{$key5->nama_apparel}}</p>
                                            <p class="text-red"> {{$key5->harga_apparel != 0 ? 'Rp. '.number_format($key5->harga_apparel,'0',',','.') : 'Rp. -'}}</p>
                                            <a class="btn btn-danger btn-round btn-block" target="_blank" href="{{ base_url() }}apparel/beli-apparel/{{ $key5->nama_apparel_url }}" title="Beli Produk"><i class="la-shopping-cart"></i>Beli Produk</a>
                                        </div> 
                                    <?php    } ?>
                                <?php  } ?>
                        <?php } else { ?>
                            <div class="col-md-12 mr-auto ml-auto mt-3 mb-3" align="center">
                                <p class="text-red">Apparel Belum Tersedia</p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="sarungtangan" role="tabpanel" aria-labelledby="sarungtangan">
                   <div class="row">
                       <?php 
                            if(!empty($sarungtangan)){
                                foreach ($sarungtangan as $key6) { ?>
                                <?php 
                                    if(!empty($key6->gambar_apparel)) { ?>
                                        <div class="col-md-4 mb-5" align="center">
                                            <img src="{{ $path_apparel . $key6->gambar_apparel}}" class="img-fluid" alt="{{$key6->nama_apparel}}" title="{{$key6->nama_apparel}}">
                                            <p class="mb-0">{{$key6->nama_apparel}}</p>
                                            <p class="text-red"> {{$key6->harga_apparel != 0 ? 'Rp. '.number_format($key6->harga_apparel,'0',',','.') : 'Rp. -'}}</p>
                                            <a class="btn btn-danger btn-round btn-block" target="_blank" href="{{ base_url() }}apparel/beli-apparel/{{ $key6->nama_apparel_url }}" title="Beli Produk"><i class="la-shopping-cart"></i>Beli Produk</a>
                                        </div> 
                                    <?php    } ?>
                                <?php  } ?>
                        <?php } else { ?>
                            <div class="col-md-12 mr-auto ml-auto mt-3 mb-3" align="center">
                                <p class="text-red">Apparel Belum Tersedia</p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="protector" role="tabpanel" aria-labelledby="protector">
                   <div class="row">
                       <?php 
                            if(!empty($protector)){
                                foreach ($protector as $key7) { ?>
                                <?php 
                                    if(!empty($key7->gambar_apparel)) { ?>
                                        <div class="col-md-4 mb-5" align="center">
                                            <img src="{{ $path_apparel . $key7->gambar_apparel}}" class="img-fluid" alt="{{$key7->nama_apparel}}" title="{{$key7->nama_apparel}}">
                                            <p class="mb-0">{{$key7->nama_apparel}}</p>
                                            <p class="text-red"> {{$key7->harga_apparel != 0 ? 'Rp. '.number_format($key7->harga_apparel,'0',',','.') : 'Rp. -'}}</p>
                                            <a class="btn btn-danger btn-round btn-block" target="_blank" href="{{ base_url() }}apparel/beli-apparel/{{ $key7->nama_apparel_url }}" title="Beli Produk"><i class="la-shopping-cart"></i>Beli Produk</a>
                                        </div> 
                                    <?php    } ?>
                                <?php  } ?>
                        <?php } else { ?>
                            <div class="col-md-12 mr-auto ml-auto mt-3 mb-3" align="center">
                                <p class="text-red">Apparel Belum Tersedia</p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <h2 class="text-white">Berita WMS</h2>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')
    <script type="text/javascript">

        $('.slide-aksesoris').owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            items: 1,

        })
        
        // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="All News">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });

    </script>
@stop