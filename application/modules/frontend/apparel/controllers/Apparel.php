<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Apparel extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();
    }

    function index() 
    {
        /* Get Banner Pict */
        $data['banner']           = ms_gambar_banner_home::where('dihapus','F')->where('publish_status','T')->where('jenis','A')->get();
        
        /* Get Path Foto For Banner */
        $data['path_foto_banner'] = "assets/upload/banner/";
        
        /* Jenis Apparel */
        $data['jenis_apparel']    = ms_apparel::where('publikasi','T')->where('dihapus','F')->groupBy('jenis_apparel')->get();
        
        /* Kemeja */
        $data['kemeja']           = ms_apparel::where('jenis_apparel','K')->where('publikasi','T')->where('dihapus','F')->get();
        
        /* Topi */
        $data['topi']             = ms_apparel::where('jenis_apparel','T')->where('publikasi','T')->where('dihapus','F')->get();
        
        /* Kaos */
        $data['kaos']             = ms_apparel::where('jenis_apparel','KS')->where('publikasi','T')->where('dihapus','F')->get();
        
        /* Helm */
        $data['helm']             = ms_apparel::where('jenis_apparel','H')->where('publikasi','T')->where('dihapus','F')->get();
        
        /* Jacket */
        $data['jacket']           = ms_apparel::where('jenis_apparel','J')->where('publikasi','T')->where('dihapus','F')->get();
        
        /* Sarung Tangan */
        $data['sarungtangan']     = ms_apparel::where('jenis_apparel','ST')->where('publikasi','T')->where('dihapus','F')->get();
        
        /* Protector */
        $data['protector']        = ms_apparel::where('jenis_apparel','P')->where('publikasi','T')->where('dihapus','F')->get();
        
        /* Path Foto Apparel*/
        $data['path_apparel']     = "assets/upload/part_aksesoris/apparel/";
        
        /* Get Path Foto For Produk */
        $data['path_foto_produk'] = "assets/upload/produk/gambar/";
        
        /* Get Product Matic */ 
        $data['matic']            = ms_produk::join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')->where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_kategori_motor.kode_kategori_motor','1')->get();
        
        /* Get Product Bebek */ 
        $data['bebek']            = ms_produk::join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')->where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_kategori_motor.kode_kategori_motor','2')->get();
        
        /* Get Product Sport */ 
        $data['sport']            = ms_produk::join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')->where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_kategori_motor.kode_kategori_motor','3')->get();
        

        

        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        
        $this->load_view_frontend("frontend","apparel","apparel","v_apparel",$data);
    }

}