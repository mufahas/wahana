@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Wanda | Wahana Honda</title>
<meta name="description" content="Wahanahonda.com, PT Wahana Makmur Sejati merupakan salah satu anak perusahaan dari Wahanaartha Group. PT Wahana Makmur Sejati bergerak di bidang distribusi sepeda motor Honda. Sejak didirikan pada tanggal 6 Agustus 1972, WMS ditetapkan oleh PT">
@endsection

<style>
    .header-layanan {
        font-size: 26px;
        font-weight: 700;
        letter-spacing: 1px;
    }
    .post-title-layanan {
        font-size: 22px;
        font-weight: 700;
        letter-spacing: .5px;
    }
    .post-desc-layanan {
        font-size: 16px;
    }
    .post-img-layanan {
        width: 150px !important;
        margin-left: auto;
        margin-right: auto;
    }
    .post-btn-layanan {
        padding: .25em 1.5em;
        background: #4d4d4f;
        color: #ffffff;
        display: inline-block;
        font-weight: 400;
        border: 1px solid transparent;
        font-size: .8rem;
        letter-spacing: .5px;
        line-height: 1.5;
        transition: all .25s;
    }
    .post-btn-layanan:hover {
        color: #ffffff;
        background: #dc3545;
    }
    .post-close-layanan {
        width: 100%;
        height: 60px;
    }
    .header-download {
        font-size: 26px;
        font-weight: 700;
        letter-spacing: 1px;
    }
    .btn-download {
        padding: .25em 1.5em;
        background: #ffffff;
        color: #dc3545;
        display: inline-block;
        font-weight: 700;
        border: 1px solid transparent;
        font-size: 1rem;
        letter-spacing: .5px;
        line-height: 1.5;
        transition: all .25s;
    }
    .btn-download:hover {
        color: #ffffff;
        background: #dc3545;
    }
    .owl-custom-dots .owl-dots {
        width: 100%;
        position: absolute;
        margin-left: auto;
        margin-right: auto;
        margin-top: -30px !important;
    }
</style>

@section('body')
    
    <div id="for_container">
        
        <div class="section-header-wanda" style="background:url('{{ base_url() }}assets/frontend/img/wanda/Banner.jpg');background-position: center;background-size: 100% auto;">
            <div class="container">
                <div class="row m-0">
                    <div class="col-md-5 d-flex justify-content-center align-items-center ml-md-auto text-center">
                        <div class="post-box-header-wanda pt-5 py-md-4">
                            <p class="h6 text-white m-0 pb-1" style="letter-spacing: 1px"><i><b>Hai Indonesia Perkenalkan saya</b></i></p>
                            <img src="{{ base_url() }}assets/frontend/img/wanda/Logo Wanda.png" class="img-fluid px-5" alt="Wanda" title="Wanda">
                            <p class="h6 text-white pt-5">Mari kita mulai perjalanan kita!</p>
                            <a href="https://play.google.com/store/apps/details?id=com.mokita.wahana" target="blank" title="Download Apk Wanda">
                                <img src="{{ base_url() }}assets/frontend/img/wanda/Icon google play-800.png" class="img-fluid" width="160" alt="Download Apk Wanda" title="Download Apk Wanda">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-5 mr-md-auto">
                        <img src="{{ base_url() }}assets/frontend/img/wanda/Icon 1.png" class="img-fluid" alt="Wanda" title="Wanda">
                    </div>
                </div>
            </div>
        </div>

        <div class="section-layanan-wanda py-5">
            <div class="container text-center">
                <p class="header-layanan text-danger my-0 mx-auto col-md-10 col-lg-8">Berbagai layanan sepeda motor Honda bisa anda dapatkan di mana pun dan kapan pun</p>
                <div class="owl-carousel owl-theme layanan-wanda pt-4 px-lg-5">
                    <div class="item">
                        <div class="post-box-layanan text-center">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Fitur_Info.png" class="img-fluid post-img-layanan p-3" alt="Informasi dan Rekomendasi" title="Informasi dan Rekomendasi">
                            <p class="post-title-layanan text-danger">Informasi & Rekomendasi</p>
                            <p class="post-desc-layanan px-5">berbagai informasi terkini untuk produk sepeda motor Honda bisa anda dapatkan setiap saat.</p>
                            <button class="post-btn-layanan rounded" title="Detail Informasi" data-toggle="modal" data-target="#informasiRekomendasi">Lihat detail</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-layanan text-center">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Fitur_Sparepart.png" class="img-fluid post-img-layanan p-3" alt="Spare Part Asli" title="Spare Part Asli">
                            <p class="post-title-layanan text-danger">Spare Part Asli</p>
                            <p class="post-desc-layanan px-5">Tak perlu khawatir lagi untuk membeli spare part yang asli sesuai dengan saran wanda.</p>
                            <button class="post-btn-layanan rounded" title="Detail Informasi" data-toggle="modal" data-target="#sparePart">Lihat detail</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-layanan text-center">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Wanda Aksesoris.png" class="img-fluid post-img-layanan p-3" alt="Aksesoris Orisinil" title="Aksesoris Orisinil">
                            <p class="post-title-layanan text-danger">Aksesoris Orisinil</p>
                            <p class="post-desc-layanan px-5">Aksesoris orisinil bikin sepeda motor kesayangan anda semakin keren.</p>
                            <button class="post-btn-layanan rounded" title="Detail Informasi" data-toggle="modal" data-target="#aksesorisOrisinil">Lihat detail</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-layanan text-center">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Wanda booking.png" class="img-fluid post-img-layanan p-3" alt="Booking Servis" title="Booking Servis">
                            <p class="post-title-layanan text-danger">Booking Servis</p>
                            <p class="post-desc-layanan px-5">Bebas mengantri untuk servis sepeda motor di Bengkel Resmi Honda.</p>
                            <button class="post-btn-layanan rounded" title="Detail Informasi" data-toggle="modal" data-target="#bookingServis">Lihat detail</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-layanan text-center">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Wanda calculator.png" class="img-fluid post-img-layanan p-3" alt="Calculator Cicilan" title="Calculator Cicilan">
                            <p class="post-title-layanan text-danger">Calculator Cicilan</p>
                            <p class="post-desc-layanan px-5">Butuh Informasi mengenai cicilan motor anda? Wanda akan membantu Anda.</p>
                            <button class="post-btn-layanan rounded" title="Detail Informasi" data-toggle="modal" data-target="#calculatorCicilan">Lihat detail</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-layanan text-center">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Wanda KPB.png" class="img-fluid post-img-layanan p-3" alt="KPB Digital" title="KPB Digital">
                            <p class="post-title-layanan text-danger">KPB Digital</p>
                            <p class="post-desc-layanan px-5">Lupa bawa buku service? Tenang saja, Anda dapat klaim service gratis berkala Anda.</p>
                            <button class="post-btn-layanan rounded" title="Detail Informasi" data-toggle="modal" data-target="#kpbDigital">Lihat detail</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-layanan text-center">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Wanda emergency.png" class="img-fluid post-img-layanan p-3" alt="Emergency Call" title="Emergency Call">
                            <p class="post-title-layanan text-danger">Emergency Call</p>
                            <p class="post-desc-layanan px-5">Motor Anda mogok saat di perjalanan atau di rumah? Wanda siap membantu Anda.</p>
                            <button class="post-btn-layanan rounded" title="Detail Informasi" data-toggle="modal" data-target="#emergencyCall">Lihat detail</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-layanan text-center">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Wanda Racing.png" class="img-fluid post-img-layanan p-3" alt="Racing Games" title="Racing Games">
                            <p class="post-title-layanan text-danger">Racing Games</p>
                            <p class="post-desc-layanan px-5">Mainkan games balapan seru sambil menservis sepeda motor Anda.</p>
                            <button class="post-btn-layanan rounded" title="Detail Informasi" data-toggle="modal" data-target="#racingGames">Lihat detail</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-layanan text-center">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Fitur_Poin.png" class="img-fluid post-img-layanan p-3" alt="Hepigo Poin" title="Hepigo Poin">
                            <p class="post-title-layanan text-danger">Hepigo Poin</p>
                            <p class="post-desc-layanan px-5">Anda bisa mengumpulkan point rewards untuk ditukarkan di merchant favorit Anda.</p>
                            <button class="post-btn-layanan rounded" title="Detail Informasi" data-toggle="modal" data-target="#hepigoPoin">Lihat detail</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="section-about-wanda px-lg-5">
            <div class="text-center">
                <div class="owl-carousel owl-theme about-wanda owl-custom-dots">
                    <div class="item">
                        <div class="post-box-about text-center mx-auto">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Slider 1.png" class="img-fluid post-img-about" alt="Siapakah saya" title="Siapakah saya">
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-about text-center mx-auto">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Slider 2.png" class="img-fluid post-img-about" alt="Apa Tugas Saya" title="Apa Tugas Saya">
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-about text-center mx-auto">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Slider 3.png" class="img-fluid post-img-about" alt="Apa Yang Bisa Saya Lakukan" title="Apa Yang Bisa Saya Lakukan">
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-about text-center mx-auto">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Slider 4.png" class="img-fluid post-img-about" alt="Seperti Siapakah Saya" title="Seperti Siapakah Saya">
                        </div>
                    </div>
                    <div class="item">
                        <div class="post-box-about text-center mx-auto">
                            <img src="{{base_url()}}assets/frontend/img/wanda/Slider 5.png" class="img-fluid post-img-about" alt="Bagaimana Sifat Saya" title="Bagaimana Sifat Saya">
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="section-download-wanda p-5" style="background:url('{{ base_url() }}assets/frontend/img/wanda/Banner.jpg');background-position: center;background-size: 100% auto;">
            <div class="container text-center">
                <p class="header-download text-white">Download Aplikasinya<br> Wahana Honda<br> Sekarang!</p>
                <a href="https://play.google.com/store/apps/details?id=com.mokita.wahana" target="blank" class="btn-download rounded" title="Download Wahana honda">Download Sekarang</a>
            </div>
        </div>

    </div>


    <!-- Modal Layanan Wanda -->

    <!-- Informasi dan Rekomendasi -->
    <div class="modal fade" id="informasiRekomendasi" tabindex="-1" role="dialog" aria-labelledby="informasiRekomendasiTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="post-close-layanan">
                <button type="button" class="close py-3 px-4" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="post-box-layanan text-center">
                    <img src="{{base_url()}}assets/frontend/img/wanda/Detail_Fitur_Info.png" class="img-fluid" alt="Informasi dan Rekomendasi" title="Informasi dan Rekomendasi">
                    <p class="post-title-layanan text-danger" id="informasiRekomendasiTitle">Informasi & Rekomendasi Produk Sepeda Motor</p>
                    <p class="post-desc-layanan px-5">berbagai informasi terkini untuk produk sepeda motor Honda bisa anda dapatkan setiap saat. Jadi, Anda bisa mendapatkan sepeda motor Honda yang sesuai dengan kebutuhan Anda.</p>
                </div>
            </div>
        </div>
      </div>
    </div>

    <!-- Spare Part Asli -->
    <div class="modal fade" id="sparePart" tabindex="-1" role="dialog" aria-labelledby="sparePartTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="post-close-layanan">
                <button type="button" class="close py-3 px-4" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="post-box-layanan text-center">
                    <img src="{{base_url()}}assets/frontend/img/wanda/Detail_Fitur_Sparepart.png" class="img-fluid" alt="Spare Part Asli" title="Spare Part Asli">
                    <p class="post-title-layanan text-danger" id="sparePartTitle">Spare Part Asli Bergaransi</p>
                    <p class="post-desc-layanan px-5">Tak perlu khawatir lagi untuk membeli spare part yang asli sesuai dengan saran wanda. Karena Wanda adalah perwakilan resmi yang memberikan informasi mengenai spare part asli bergaransi dari Wahana Honda.</p>
                </div>
            </div>
        </div>
      </div>
    </div>

    <!-- Aksesoris Orisinil -->
    <div class="modal fade" id="aksesorisOrisinil" tabindex="-1" role="dialog" aria-labelledby="aksesorisOrisinilTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="post-close-layanan">
                <button type="button" class="close py-3 px-4" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="post-box-layanan text-center">
                    <img src="{{base_url()}}assets/frontend/img/wanda/Detail_Wanda Aksesoris.png" class="img-fluid" alt="Aksesoris Orisinil" title="Aksesoris Orisinil">
                    <p class="post-title-layanan text-danger" id="aksesorisOrisinilTitle">Aksesoris Orisinil</p>
                    <p class="post-desc-layanan px-5">Aksesoris orisinil bikin sepeda motor kesayangan anda semakin keren. Wanda akan memberikan informasi lengkap dan jujur seperti layaknya teman bagi anda.</p>
                </div>
            </div>
        </div>
      </div>
    </div>

    <!-- Booking Servis -->
    <div class="modal fade" id="bookingServis" tabindex="-1" role="dialog" aria-labelledby="bookingServisTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="post-close-layanan">
                <button type="button" class="close py-3 px-4" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="post-box-layanan text-center">
                    <img src="{{base_url()}}assets/frontend/img/wanda/Detail_Wanda booking.png" class="img-fluid" alt="Booking Servis" title="Booking Servis">
                    <p class="post-title-layanan text-danger" id="bookingServisTitle">Booking Servis Sepeda Motor di AHAAS dan di rumah</p>
                    <p class="post-desc-layanan px-5">Bebas mengantri untuk servis sepeda motor di Bengkel Resmi Honda. Karena Wanda akan membantu mengaturkan waktu servis sesuai dengan yang Anda inginkan dan tidak perlu khawatir lagi karena Wanda akan mengingatkan anda mengenai jadwal booking sevice yang telah anda buat.</p>
                </div>
            </div>
        </div>
      </div>
    </div>

    <!-- Calculator Cicilan -->
    <div class="modal fade" id="calculatorCicilan" tabindex="-1" role="dialog" aria-labelledby="calculatorCicilanTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="post-close-layanan">
                <button type="button" class="close py-3 px-4" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="post-box-layanan text-center">
                    <img src="{{base_url()}}assets/frontend/img/wanda/Detail_Wanda calculator.png" class="img-fluid" alt="Calculator Cicilan" title="Calculator Cicilan">
                    <p class="post-title-layanan text-danger" id="calculatorCicilanTitle">Calculator Cicilan</p>
                    <p class="post-desc-layanan px-5">(*Saat ini fitur ini baru tersedia di website www.wahanahonda.com).<br> Butuh Informasi mengenai cicilan motor anda? Wanda akan membantu menghitungkannya untuk Anda. Dapatkan juga berbagai program menguntungkan lainnya.</p>
                </div>
            </div>
        </div>
      </div>
    </div>

    <!-- KPB Digital -->
    <div class="modal fade" id="kpbDigital" tabindex="-1" role="dialog" aria-labelledby="kpbDigitalTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="post-close-layanan">
                <button type="button" class="close py-3 px-4" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="post-box-layanan text-center">
                    <img src="{{base_url()}}assets/frontend/img/wanda/Detail_Wanda KPB.png" class="img-fluid" alt="KPB Digital" title="KPB Digital">
                    <p class="post-title-layanan text-danger" id="kpbDigitalTitle">KPB (Kartu Perawatan Berkala) Digital</p>
                    <p class="post-desc-layanan px-5">Lupa bawa buku service? Tenang saja, Anda dapat klaim service gratis berkala Anda melalui aplikasi Wahana Honda yang tentunya sangat praktis.</p>
                </div>
            </div>
        </div>
      </div>
    </div>

    <!-- Emergency Call -->
    <div class="modal fade" id="emergencyCall" tabindex="-1" role="dialog" aria-labelledby="emergencyCallTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="post-close-layanan">
                <button type="button" class="close py-3 px-4" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="post-box-layanan text-center">
                    <img src="{{base_url()}}assets/frontend/img/wanda/Detail_Wanda emergency.png" class="img-fluid" alt="Emergency Call" title="Emergency Call">
                    <p class="post-title-layanan text-danger" id="emergencyCallTitle">Emergency Call</p>
                    <p class="post-desc-layanan px-5">Motor Anda mogok saat di perjalanan atau di rumah? Wanda siap membantu Anda untuk memberikan solusi perbaikan sepeda motor ke lokasi Anda.</p>
                </div>
            </div>
        </div>
      </div>
    </div>

    <!-- Racing Games -->
    <div class="modal fade" id="racingGames" tabindex="-1" role="dialog" aria-labelledby="racingGamesTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="post-close-layanan">
                <button type="button" class="close py-3 px-4" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="post-box-layanan text-center">
                    <img src="{{base_url()}}assets/frontend/img/wanda/Detail_Wanda Racing.png" class="img-fluid" alt="Racing Games" title="Racing Games">
                    <p class="post-title-layanan text-danger" id="racingGamesTitle">Racing Games</p>
                    <p class="post-desc-layanan px-5">Mainkan games balapan seru sambil menservis motor Anda.</p>
                </div>
            </div>
        </div>
      </div>
    </div>

    <!-- Hepigo Poin -->
    <div class="modal fade" id="hepigoPoin" tabindex="-1" role="dialog" aria-labelledby="hepigoPoinTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="post-close-layanan">
                <button type="button" class="close py-3 px-4" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="post-box-layanan text-center">
                    <img src="{{base_url()}}assets/frontend/img/wanda/Detail_Fitur_Poin.png" class="img-fluid" alt="Hepigo Poin" title="Hepigo Poin">
                    <p class="post-title-layanan text-danger" id="hepigoPoinTitle">Hepigo Poin</p>
                    <p class="post-desc-layanan px-5">Anda bisa mengumpulkan point rewards untuk ditukarkan di merchant favorit sambil menservis motor, bermain dan banyak lagi dengan menggunakan aplikasi Wahana Honda.</p>
                </div>
            </div>
        </div>
      </div>
    </div>



    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script type="text/javascript">
        $('.layanan-wanda').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            dots:true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:2
                },
                1024:{
                    items:3
                }
            }
        });
        $('.about-wanda').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            dots:true,
            autoplay:true,
            autoplayHoverPause:true,
            items:1
        });
    </script>
    <script type="text/javascript">
        
        // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });

    </script>
@stop