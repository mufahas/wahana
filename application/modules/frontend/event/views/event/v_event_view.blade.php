@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>{{$detail->judul_event}} | Wahana Honda</title>
<meta name="description" content="{{ $detail->short_desc }}">
@endsection

@section('body')
    
    <div id="for_container">
        @if(!empty($detail))
        <nav aria-label="breadcrumb" class="breadcrumb-custom">
            <div class="container">
              <ol class="breadcrumb mb-0 pb-2 pt-2">
                <li class="breadcrumb-item default"><a href="{{base_url()}}" title="Wahana">Home</a></li>
                <li class="breadcrumb-item default"><a href="{{base_url()}}event" title="Event">Event</a></li>
                <li class="breadcrumb-item default active" aria-current="page">{{$detail->judul_event}}</li>
              </ol>
            </div>
        </nav>
    
        <div class="container-fluid mb-5 pt-5">
            <div class="container">
                
                <img src="{{ base_url() }}assets/upload/kegiatan/{{$detail->gambar_event}}" class="img-fluid" alt="{{$detail->judul_event}}" title="{{$detail->judul_event}}">
                <div class="content-promo pb-3 pt-3 pl-3 pr-3">
                    <h1 class="header1-content font-weight-bold text-secondary">{{$detail->judul_event}}</h1><br>
                    <!-- <b>Tanggal {{$detail->tanggal_mulai_event}}</b><br> -->
                    <!-- <b> {{$detail->lokasi_event}} </b></br> -->
                    <!-- <b>Jam {{$detail->waktu_mulai_event}} - {{$detail->waktu_akhir_event}}</b> -->
                    <?php // if ($detail->dibuka_untuk_umum = "1") {
                        // echo "<br><b>Free entry *Limited seat</b>";
                     // } ?>
                    {!!$detail->deskripsi_event!!}
                </div>
                
                <div class="row">
                    <div class="ml-auto mr-auto">
                        <!-- <button type="button" class="btn btn-danger btn-round btn-block ikut_promo">Ikuti Promo</button> -->
                        <a class="btn btn-danger btn-round btn-block" target="_blank" href="{{ base_url()}}daftar-event/{{$detail->judul_event_url}}" title="Daftar Event">Daftar Event</a>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script type="text/javascript">
        
        $('.varian').owlCarousel({
            loop:true,
            margin:10,
            dots: true,
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:1
                }
            }
        });
        
        // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="All News">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });

    </script>
@stop