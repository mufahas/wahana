@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Daftar Event Terbaru Motor Honda | Wahana Honda</title>
<meta name="description" content="Jangan lewatkan event - event terbaru mengenai produk motor Honda dan dapatkan door prize serta merchandise yang menarik selama persediaan masih ada">
@endsection

@section('body')
    <style type="text/css">
        .event-banner {
            padding: 0;
        }
    </style>
    
    <nav aria-label="breadcrumb" class="breadcrumb-custom">
      <div class="container">
        <div class="container-fluid">
          <ol class="breadcrumb pl-0 mb-0 pb-1 pt-1">
            <li class="breadcrumb-item"><a href="{{base_url()}}" title="Wahana">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Event</li>
          </ol>
        </div>
      </div>
    </nav>
    
    <div id="for_container">
        <div class="container-fluid">
            <div class="row">
                <div class="owl-carousel owl-theme slide-aksesoris">
                    @if($banner_event != "[]")
                    @foreach($banner_event as $vbanner_event)
                    <div class="item">
                        <img src="{{ base_url() }}assets/upload/kegiatan/{{$vbanner_event->gambar_event}}" class="img-fluid" alt="{{$vbanner_event->judul_event}}" title="{{$vbanner_event->judul_event}}">
                    </div>
                    @endforeach
                    @endif
              
                </div>
            </div>
        </div>
        <!-- <div class="container-fluid event">
            <div class="container">
                <h2 class="text-white">Event Wahana Honda</h2>
            </div>
        </div> -->
    
    
            <div class="container mt-5">
                
                <div class="text-center">
                    <h1 class="header1-content mb-2">Event Wahana Honda</h1>
                    <hr class="separate-line m-auto">
                </div>
                
                <div class="row mt-4">
    
                    @if(!empty($event))
                    @foreach($event as $vevent)
                    <div class="col-md-6 mb-5 loadmore">
                        <a href="{{base_url()}}event/{{$vevent->judul_event_url}}" class="event-banner btn-block" title="Event Details">
                            <img src="{{ base_url() }}assets/upload/kegiatan/{{$vevent->gambar_event}}" class="img-fluid" style="height: 100%;width: 100%;" alt="{{$vevent->judul_event}}" title="{{$vevent->judul_event}}">
                            <h2 class="header2-content bg-red text-center pt-2 pb-2 text-white">
                                {{$vevent->judul_event}}
                            </h2>
                        </a>
                    </div>
                    @endforeach
                    <div class="col-md-12 mb-5" align="center">
                        <a href="#" id="loadMore" class="btn btn-danger" title="Load More">Load More</a>
                    </div>
                    @endif
    
                </div>
            </div>
        </div>
    </div>
    <!-- ADDON -->
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->
@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script type="text/javascript">

        $('.slide-aksesoris').owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            items: 1,
            dotsClass: 'owl-dots dots-container',
        })

    </script>
    
    <script type="text/javascript">
        $(function () {
            if ($(".loadmore").length <= 6) {
                $("#loadMore").hide();
            }

            $(".loadmore").slice(0, 6).show();
            $("#loadMore").on('click', function (e) {
                e.preventDefault();
                $(".loadmore:hidden").slice(0, 6).slideDown();
                if ($(".loadmore:hidden").length == 0) {
                    $("#load").fadeOut('slow');
                }
                if ($(".loadmore:hidden").length <= 0) {
                    $("#loadMore").hide();
                }
            });
        });
    </script>
    
    
    <!---->
    
    
    <script type="text/javascript">
    
        $("#img-thumn a").on('click', function(e) 
        {
            e.preventDefault();

            $('#preview img').attr('src', $(this).attr('href'));
        });

        $('.aksesoris').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            stageOuterClass: 'owl-stage-outer outer-stage',
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'nav-container',
            navClass: [ 'prev', 'next' ],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:6
                }
            }
        });
        // $.ajax({
        //     type: 'POST',
        //     url: '{{base_url ()}}event/get_all_event',
        //     data: '',
        //     dataType: 'json',
        //     success: function (data) 
        //     {
        //         var kancut = new Array(2);
        //         kancut[0]  = '<br>';
        //         kancut[1]  = "Free entry *Limited seat";
        //         $.each(data.data, function(i, item)
        //         {
        //             var date = new Date(item.tanggal_mulai_event);
        //             $('#thisOfThis').append("<div class='col-md-6 mb-5'>"+
        //                 "<a href='{{base_url()}}event-detail/"+item.judul_event_url+"' class='event-banner btn-block'>"+
        //                     "<p class='text-30 mb-0'>"+indonesian_date(date)+"</p>"+
        //                     "<p class='mb-0'>"+item.judul_event+" - "+item.lokasi_event+"</p>"+
        //                     "<p class='mb-0'>Jam "+item.waktu_mulai_event+" - "+item.waktu_akhir_event+"</p>"+
        //                     "<p>"+kancut[item.dibuka_untuk_umum]+"</p>"+
        //                     "<div class='desc-event'>"+
        //                         item.judul_event+"<i class='fa fa-angle-right btn-arrow' aria-hidden='true'></i>"+
        //                     "</div>"+
        //                 "</a>"+
        //             "</div>");
        //         });
        //     }
        // }); 

        var filter = $('select[name="bulan"]').val();
        var filter_ = new Array(2);
        filter_[0]  = '<br>';
        filter_[1]  = "Free entry *Limited seat";
        $.ajax({
            type: 'POST',
            url: '{{base_url () }}event/ajax_get_filtered_event',
            data: 'filter=' + filter,
            dataType: 'json',
            success: function (data) {
                $('#thisOfThis').html('');
                $.each(data.data, function(i, item)
                {
                    var date = new Date(item.tanggal_mulai_event);
                    $('#thisOfThis').append("<div class='col-md-6 mb-5'>"+
                        "<a href='{{base_url()}}event-detail/"+item.judul_event_url+"' class='event-banner btn-block' title='Event Details'>"+
                            "<p class='text-30 mb-0'>"+indonesian_date(date)+"</p>"+
                            "<p class='mb-0'>"+item.judul_event+" - "+item.lokasi_event+"</p>"+
                            "<p class='mb-0'>Jam "+item.waktu_mulai_event+" - "+item.waktu_akhir_event+"</p>"+
                            "<p>"+filter_[item.dibuka_untuk_umum]+"</p>"+
                            "<div class='desc-event'>"+
                                item.judul_event+"<i class='fa fa-angle-right btn-arrow' aria-hidden='true'></i>"+
                            "</div>"+
                        "</a>"+
                    "</div>");
                });
            }
        }); 

        $('select[name="bulan"]').change(function(){
            var filter = $(this).val();
            var kancut = new Array(2);
            kancut[0]  = '<br>';
            kancut[1]  = "Free entry *Limited seat";
            $.ajax({
                type: 'POST',
                url: '{{base_url () }}event/ajax_get_filtered_event',
                data: 'filter=' + filter,
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $('#thisOfThis').html('');
                    $.each(data.data, function(i, item)
                    {
                        var date = new Date(item.tanggal_mulai_event);
                        $('#thisOfThis').append("<div class='col-md-6 mb-5'>"+
                            "<a href='{{base_url()}}event-detail/"+item.judul_event_url+"' class='event-banner btn-block' title='Event Details'>"+
                                "<p class='text-30 mb-0'>"+indonesian_date(date)+"</p>"+
                                "<p class='mb-0'>"+item.judul_event+" - "+item.lokasi_event+"</p>"+
                                "<p class='mb-0'>Jam "+item.waktu_mulai_event+" - "+item.waktu_akhir_event+"</p>"+
                                "<p>"+kancut[item.dibuka_untuk_umum]+"</p>"+
                                "<div class='desc-event'>"+
                                    item.judul_event+"<i class='fa fa-angle-right btn-arrow' aria-hidden='true'></i>"+
                                "</div>"+
                            "</a>"+
                        "</div>");
                    });
                }
            }); 
        });

        function indonesian_date(date) 
        {
            var monthArray =  new Array(12);
            monthArray[0]  = 'Januari';
            monthArray[1]  = 'Februari';
            monthArray[2]  = 'Maret';
            monthArray[3]  = 'April';
            monthArray[4]  = 'Mei';
            monthArray[5]  = 'Juni';
            monthArray[6]  = 'Juli';
            monthArray[7]  = 'Agustus';
            monthArray[8]  = 'September';
            monthArray[9]  = 'Oktober';
            monthArray[10] = 'November';
            monthArray[11] = 'Desember';
            var day        = date.getDay();
            var month      = date.getMonth();
            var year       = date.getFullYear();

            return day + ' ' + monthArray[month] + ' ' + year;
        }
        
        // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="All News">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });
        
    </script>
@stop