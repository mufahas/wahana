<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();
         
        $this->load->model('Select_global_model');
    }

    function index() 
    {
        $today                 = date('Y-m-d');
        $data['current_month'] = date('n');
        $data['bulan']         = $this->Select_global_model->selectBulan();
        $data['event']         = tbl_event::where('tbl_event.status_event', 'A')->get();
        $data['banner_event']  = tbl_event::where('tbl_event.status_event', 'A')->orderBy('tbl_event.tanggal_mulai_event','DESC')->limit(3)->get();
        
        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        
        $this->load_view_frontend("frontend","event","event","v_event",$data);
    }

    function view($url) 
    {

        $today          = date('Y-m-d');
        $data['detail'] = tbl_event::where('tbl_event.status_event', 'A')->where('tbl_event.judul_event_url', $url)->first();
        
        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        
        $this->load_view_frontend("frontend","event","event","v_event_view",$data);
    }

    //From This Code Z
    
    /**
    * Get all data from : tbl_event
    * @return ajax
    **/
    function ajax_get_all_event()
    {
    	if ($this->input->is_ajax_request()) 
    	{
            $events = tbl_event::where('status_event','A')->get();
            if (empty($events)) 
            {
                $status = array('status' => 'error');
            }
        	else
        	{
                $status = array('status' => 'success','data'=>$events);
            }
            $data = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    /**
    * Get filtered data from : tbl_event
    * @return ajax
    **/
    function ajax_get_filtered_event()
    {
        if ($this->input->is_ajax_request()) 
        {
            $filter = $this->input->post('filter');
            if ($filter == 'all') 
            {
                $getByFiltered = tbl_event::where('status_event','A')->get();
            }
            else
            {
                $getByFiltered = tbl_event::whereRaw('MONTH(tanggal_mulai_event) = "'.$filter.'" AND status_event = "A"')->get();
            }
            if (empty($getByFiltered)) 
            {
                $status = array('status' => 'error');
            }
            else
            {
                $status = array('status' => 'success','data'=>$getByFiltered);
            }
            $data = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
}