<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_event extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index($url="") 
    {
        /* For Search */
            $where                          = "tbl_berita.status_publikasi = 'T'";
            $count_all                      = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                                ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                                ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                                ->whereRaw($where)
                                                ->groupBy('tbl_berita.kode_berita')
                                                ->get();
            $all = 0;
            foreach ($count_all as $value) {
                $all++;
            }
            $data['total_page_all'] = ceil($all / 12);
        /* For Search */

        /* Button Action */
        $data['event']                      = tbl_event::select('tbl_event.kode_event','tbl_event.judul_event','tbl_event.judul_event_url','tbl_event.event_code','tbl_event.short_desc')->where('tbl_event.status_event', 'A')->where('judul_event_url',$url)->orderBy('tbl_event.kode_event','DESC')->first();
        $data['action']                     = site_url() . $this->class . '/save';
        $data['getVerifikasiKode']          = site_url() . $this->class .  '/ajax_get_verifikasi_kode';
        $data['message_success']            = $this->session->flashdata('success');
        $data['message_error']              = $this->session->flashdata('error');

        $this->load_view_frontend("frontend","daftar_event","daftar_event","v_daftar_event",$data);
    }

    function save()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            
            /* Post Pendaftar event */
            $id_event        = decryptID($this->security->xss_clean($this->input->post('id_event')));
            $judul_event_url = $this->security->xss_clean($this->input->post('judul_event_url'));
            $no_ktp          = $this->security->xss_clean($this->input->post('no_ktp'));
            $nama_pendaftar  = $this->security->xss_clean($this->input->post('nama_pendaftar'));
            $email_pendaftar = $this->security->xss_clean($this->input->post('email'));
            $no_hp           = $this->security->xss_clean($this->input->post('nomor_hp'));
            $domisili        = $this->security->xss_clean($this->input->post('domisili'));

            /* URL */
            $url        = site_url() . 'daftar-event/' . $judul_event_url;
   
            /* Cek Peserta */
            $cek_peserta_event = tbl_peserta_event::where('email_peserta_event', strtolower($email_pendaftar))->where('kode_event',$id_event)->first();

            if(empty($cek_peserta_event))
            {
                /* Initialize Data Peserta event */
                $tbl_peserta_event                         = new tbl_peserta_event;
                
                $tbl_peserta_event->kode_event             = $id_event;
                $tbl_peserta_event->no_ktp_peserta_event   = $no_ktp;
                $tbl_peserta_event->nama_peserta_event     = ucfirst($nama_pendaftar);
                $tbl_peserta_event->email_peserta_event    = strtolower($email_pendaftar);
                $tbl_peserta_event->no_hp_peserta_event    = $no_hp;
                $tbl_peserta_event->domisili_peserta_event = $domisili;
                $tbl_peserta_event->date_event_submit      = date('Y-m-d');

                /* Save Peserta event */
                $save = $tbl_peserta_event->save();
                
                if($save)
                {
                    $this->session->set_flashdata('success', 'Pendaftaran Event Berhasil !');
                    redirect($url);
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Anda Sudah Pernah Mendaftar Untuk Event Ini !');
                redirect($url);
            }
        }
    }
    
    function ajax_get_verifikasi_kode()
    {
        $no_hp           = $this->security->xss_clean($this->input->post('no_hp'));
      
        $string          = '0123456789';
        $string_shuffled = str_shuffle($string);
        $password        = substr($string_shuffled, 1, 6);
        $pesan           = 'Wahana Honda Testride: Angka verifikasi Anda adalah: '.$password;

        $curl = curl_init();
      
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_URL             => 'http://api.nusasms.com/api/v3/sendsms/plain?&output=json',
            CURLOPT_POST            => true,
            CURLOPT_POSTFIELDS      => array(
                                                'user'     => 'wahanaartha_api',
                                                'password' => 'Wahana321+',
                                                'SMSText'  => $pesan,
                                                'GSM'      => $no_hp
                                            )
        ));

        $resp = curl_exec($curl);
        
        $data = array('resp' => $resp,'password' => $password);

        if (!$resp) 
        {
            die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        } 
        else 
        {
            echo json_encode($data);
        }
    }

}