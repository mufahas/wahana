<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_pendaftar_testride extends Eloquent {

	public $table      = 'tbl_pendaftar_testride';
	public $primaryKey = 'id_pendaftar_testride';
	public $timestamps = false;

}