<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- If you delete this meta tag, Half Life 3 will never be released. -->
    <meta name="viewport" content="width=device-width" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <title>Wanda Sales Station</title>

    <style>
    	/* Begin: Universal */
        body {
            margin: 0;
            padding: 0;
            font-family: arial;
        }
        a {
            text-decoration: none;
    	}

    	.mt-auto {
            margin-top: auto;
        }
        .mb-auto {
            margin-left: auto;
        }
        .ml-auto {
            margin-left: auto;
        }
        .mr-auto {
            margin-right: auto;
        }

        .primary-text {
            color: #c52822;
        }
        .red-text {
            color: #b90a29;
        }
        .green-text {
            color: #11a88a;
        }
        .blue-text {
            color: #0000ff;
        }
        .white-text {
            color: #fafafa;
        }
        .dark-text{
            color: #575962;
        }
        .grey-text {
            color: #6a6a6a;
        }
        
        .primary {
            background: #fff;
            border-bottom: solid 2px #2f88b4;
        }
        .red {
            background: #C52822;
        }
        .green {
            background: #11a88a;
        }
        .blue {
            background: #0000ff;
        }
        .white {
            background: #ffffff;
        }
        .dark{
            background: #575962;
        }
        .grey {
            background: #9e9e9e;
        }
    	/* End: Universal */

        /* Begin: Layout*/
        .body {
            padding-top: 50px;
            padding-bottom: 50px;
            font-family: roboto;
            background-color: #2f88b417;
        }
        .portlet {
            flex: 0 0 60%;
            max-width: 60%;
        }
        .portlet .portlet-header {
            padding: 1rem 2rem;
        }
        .portlet .portlet-content {
            padding: 1rem 2rem;
        }
        .portlet .portlet-footer {
            padding: 1rem 2rem;
        }
        .portlet-border-top {
        	border-top: 10px solid #df031dc2;
        }
        /*End: Layout*/
    
        /* Begin: Mobile */
        @media only screen and (max-width: 768px) {
            /*Begin: Layout*/
            .portlet {
                flex: 0 0 90%;
                max-width: 90%;
            }
            /*End: Layout*/
        }
        /* End: Mobile */
    </style>
</head>
<body>

<!-- BEGIN: Body -->
<div class="body">

    <div class="portlet ml-auto mr-auto white dark-text portlet-border-top">
        
        <div class="portlet-content">
            <h4>Hello <b><?= ucwords($nama) ?></b>,</h4>
            <p>Selamat kepada <b><?= ucwords($nama) ?></b>. Permintaan order Anda untuk test ride Honda <b><?= $kendaraan ?></b> di <b><?= $lokasi ?></b> telah kami terima. Pihak kami akan menghubungi Anda kembali untuk konfirmasi tanggal untuk test ride</p>
			<br>
            <p>Terima Kasih</p>
            <p>Wanda Sales Station.</p>
        </div>
            
    </div>
</div>
<!-- END: Body -->

</body>
</html>