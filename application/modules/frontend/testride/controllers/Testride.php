<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Testride extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        /* Load Model */
        $this->load->model('select_global_model');

        /* Load Library */
        $this->load->library('send_email_testride');
    }

    function index($url) 
    {
        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        /* End For Search */
        
        /* Set Data */
         $data['detail']            = ms_produk::join('ms_harga_otr','ms_harga_otr.kode_produk','=','ms_produk.kode_produk')->join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')->where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.nama_produk_url', $url)->first();
         
         /* Button Action */
         $data['action']            = site_url() . $this->site . '/save';
         $data['getAjaxTestRide']   = site_url() . $this->site . '/ajax_get_testride';
         $data['getVerifikasiKode'] = site_url() . $this->site .  '/ajax_get_verifikasi_kode';
         $data['testride']          = '- Pilih Tipe Motor -';
         $data['kategori_motor']    = $this->select_global_model->selectKategoriMotor();
         $data['lokasi_testride']   = $this->select_global_model->selectLokasiTestride();
         $data['message_success']   = $this->session->flashdata('success');
         $data['message_error']     = $this->session->flashdata('error');

        $this->load_view_frontend("frontend","testride","testride","v_testride",$data);
    }

    function ajax_get_testride()
    {
        $lokasi_testride = $this->security->xss_clean($this->input->get('lokasi'));
        $lokasi          = decryptID($lokasi_testride);

        $testride        = $this->select_global_model->selectKendaraanTestride($lokasi);
        $result          = form_dropdown('kendaraan_testride', $testride, '', 'class="form-control"');

        echo json_encode($result);
    }

    function ajax_get_verifikasi_kode()
    {
        $no_hp           = $this->security->xss_clean($this->input->post('no_hp'));
      
        $string          = '0123456789';
        $string_shuffled = str_shuffle($string);
        $password        = substr($string_shuffled, 1, 6);
        $pesan           = 'Wahana Honda Testride: Angka verifikasi Anda adalah: '.$password;

        $curl = curl_init();
      
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_URL             => 'http://api.nusasms.com/api/v3/sendsms/plain?&output=json',
            CURLOPT_POST            => true,
            CURLOPT_POSTFIELDS      => array(
                                                'user'     => 'wahanaartha_api',
                                                'password' => 'Wahana321+',
                                                'SMSText'  => $pesan,
                                                'GSM'      => $no_hp
                                            )
        ));

        $resp = curl_exec($curl);
        
        $data = array('resp' => $resp,'password' => $password);

        if (!$resp) 
        {
            die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        } 
        else 
        {
            echo json_encode($data);
        }
    }

    function save()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") 
        {
            /* Set Post Data */
            $lokasi_testride    = decryptID($this->security->xss_clean($this->input->post('lokasi_testride')));
            $kendaraan_testride = decryptID($this->security->xss_clean($this->input->post('kendaraan_testride')));
            $nama_testrider     = $this->security->xss_clean($this->input->post('nama_testrider'));
            $email_testrider    = $this->security->xss_clean($this->input->post('email_testrider'));
            $nomor_hp           = $this->security->xss_clean($this->input->post('nomor_hp'));
            $nama_produk_url    = decryptID($this->security->xss_clean($this->input->post('nama_produk_url')));

            /* URL */
            $url                = site_url() . 'testride/' . $nama_produk_url;

            /* Check Into Database If Email and Phone Number are Exist */
            // $cek_tbl            = tbl_pendaftar_testride::where('email_testrider', $email_testrider)->where('kendaraan_testride',$kendaraan_testride)->first(); 
 
            /* Begin Save Data Into Table tbl_pendaftar_testride */
            $tbl_pendaftar_testride                       = new tbl_pendaftar_testride;
            
            $tbl_pendaftar_testride->lokasi_testride      = $lokasi_testride;
            $tbl_pendaftar_testride->kendaraan_testride   = $kendaraan_testride;
            $tbl_pendaftar_testride->nama_testrider       = $nama_testrider;
            $tbl_pendaftar_testride->email_testrider      = $email_testrider;
            $tbl_pendaftar_testride->no_telepon_testrider = $nomor_hp;
            $tbl_pendaftar_testride->tgl_pendaftaran      = date('Y-m-d');

            $save = $tbl_pendaftar_testride->save();

            if($save)
            {   
                /* Send Email */
                $this->sendEmail($email_testrider,$nama_testrider,$kendaraan_testride,$lokasi_testride);

                $this->session->set_flashdata('success', 'Pendaftaran Test Ride Berhasil');
                redirect($url);
            }
            else
            {
                $this->session->set_flashdata('error', 'Gagal Mendaftar Testride');
                redirect($url);
            }
        }
    }

    public function sendEmail($email,$nama,$kendaraan,$lokasi)
    {
        $set['email']     = $email;
        $set['nama']      = $nama;
        $set['kendaraan'] = $kendaraan;
        $set['lokasi']    = $lokasi == 'JKT' ? 'Wahana Makmur Sejati - Jakarta' : 'Wahana Jetake - Tangerang';
        
        $message          = $this->load->view('../modules/frontend/testride/views/testride/v_send_email_testrider', $set , true, true); 
        
        $this->send_email_testride->email($email, $nama, $message);
    }
}