<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Aksesoris extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();
    }

    function index() 
    {
		/* Get Banner Pict */
		$data['banner']    			= ms_gambar_banner_home::where('dihapus','F')->where('publish_status','T')->where('jenis','A')->get();
		
		/* Get Path Foto For Banner */
		$data['path_foto_banner'] 	= "assets/upload/banner/";

		/* Get Path Foto For Produk */
		$data['path_foto_produk'] 	= "assets/upload/produk/gambar/";
		
		/* Get Product Matic */	
		$data['matic']	 	  		= ms_produk::join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')->where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_kategori_motor.kode_kategori_motor','1')->get();
		
		/* Get Product Bebek */	
		$data['bebek']	   			= ms_produk::join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')->where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_kategori_motor.kode_kategori_motor','2')->get();

		/* Get Product Sport */	
		$data['sport']	   			= ms_produk::join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')->where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_kategori_motor.kode_kategori_motor','3')->get();
        
        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        
        $this->load_view_frontend("frontend","aksesoris","aksesoris","v_aksesoris",$data);
    }

}