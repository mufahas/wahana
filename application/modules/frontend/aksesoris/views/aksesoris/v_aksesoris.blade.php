@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Spare Part dan Aksesoris Motor Honda | Wahana Honda</title>
<meta name="description" content="Daftar spare parts & aksesoris sepeda motor honda dengan pilihan terlengkap. Tersedia beragam jenis onderdil resmi motor honda untuk kebutuhan bengkel Anda">
@endsection

@section('body')
    <div id="for_container">

        <nav aria-label="breadcrumb" class="breadcrumb-custom">
          <div class="container-fluid">
              <div class="container">
                    <ol class="breadcrumb mb-0 pb-1 pt-1">
                      <li class="breadcrumb-item"><a href="{{base_url()}}" title="Wahana">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Aksesoris</li>
                    </ol>
              </div>
          </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="owl-carousel owl-theme slide-aksesoris">
                    <?php foreach ($banner as $key) { ?>
                        <?php
                            $find = array(".jpg",".png",".jpeg",".gif",".JPG",".PNG",".JPEG",".GIF");
                            $replace = array("");
                            $title_banner = str_replace($find, $replace, $key->nama_gambar);
                        ?>
                        <div class="item">
                            <?php if (!empty($key->url_banner)){ ?>
                                <?php if ($key->new_page == 'T') { ?>
                                    <a href="{{$key->url_banner}}" title="{{$title_banner}}" target="blank">
                                        <img src="{{ base_url() }}assets/upload/banner/{{$key->nama_gambar}}" class="img-fluid" alt="{{$title_banner}}" title="{{$title_banner}}">
                                    </a>
                                <?php } else { ?>
                                    <a href="{{$key->url_banner}}" title="{{$title_banner}}">
                                        <img src="{{ base_url() }}assets/upload/banner/{{$key->nama_gambar}}" class="img-fluid" alt="{{$title_banner}}" title="{{$title_banner}}">
                                    </a>
                                <?php } ?>
                            <?php }else{ ?>
                                <img src="{{ $path_foto_banner . $key->nama_gambar }}" class="img-fluid" alt="{{$title_banner}}" title="{{$title_banner}}">
                            <?php } ?>
                        </div>
                    <?php } ?>
                    
                </div>
            </div>
        </div>
    
        <div class="container-fluid honda-genue"></div>
    
        <div class="container mt-4">
            <div class="text-center">
                <h1 class="header1-content mb-2">Aksesoris Motor</h1>
                <hr class="separate-line m-auto">
            </div>
            <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link menu-list news-link active" id="bebek-tab" data-toggle="tab" href="#motor-bebek" role="tab" aria-controls="motor-bebek" aria-selected="false" title="Cub">CUB</a>
              </li>
              <li class="nav-item">
                <a class="nav-link menu-list news-link" id="matic-tab" data-toggle="tab" href="#motor-matic" role="tab" aria-controls="motor-matic" aria-selected="false" title="Matic">MATIC</a>
              </li>
              <li class="nav-item">
                <a class="nav-link menu-list news-link" id="sport-tab" data-toggle="tab" href="#motor-sport" role="tab" aria-controls="motor-sport" aria-selected="false" title="Sport">SPORT</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="motor-bebek" role="tabpanel" aria-labelledby="bebek-tab">
                    <div class="row">
                        <?php 
                            if(!empty($bebek)){
                                foreach ($bebek as $key1) { ?>
                                    <?php
                                        if(!empty($key1->gambar_produk)) { ?>
                                            <div class="col-md-4 mb-5" align="center">
                                                <a href="{{base_url()}}aksesoris/{{$key1->nama_produk_url}}" class="promo-banner btn-block" title="Cub">
                                                    <img src="{{ $path_foto_produk . $key1->gambar_produk}}" class="img-fluid" alt="{{ $key1->nama_produk }}" title="{{ $key1->nama_produk }}">
                                                    <h2 class="header2-content bg-red text-center pt-2 pb-2 text-white">
                                                        {{ $key1->nama_produk }}
                                                    </h2>
                                                </a>
                                            </div>                                
                                    <?php } ?>
                                <?php  } ?>
                        <?php } ?>
                    </div>
                </div>
    
                <div class="tab-pane fade" id="motor-matic" role="tabpanel" aria-labelledby="matic-tab">
                    <div class="row">
                        <?php 
                            if(!empty($matic)){
                                foreach ($matic as $key2) { ?>
                                    <?php 
                                        if(!empty($key2->gambar_produk)) { ?>
                                            <div class="col-md-4 mb-5" align="center">
                                                <a href="{{base_url()}}aksesoris/{{$key2->nama_produk_url}}" class="promo-banner btn-block" title="Matic">
                                                    <img src="{{ $path_foto_produk . $key2->gambar_produk}}" class="img-fluid" alt="{{ $key2->nama_produk }}" title="{{ $key2->nama_produk }}">
                                                    <h2 class="header2-content bg-red text-center pt-2 pb-2 text-white">
                                                        {{ $key2->nama_produk }}
                                                    </h2>
                                                </a>
                                            </div>
                                    <?php } ?>
                                <?php  } ?>
                        <?php } ?>
                    </div>
                </div>
    
                <div class="tab-pane fade" id="motor-sport" role="tabpanel" aria-labelledby="sport-tab">
                   <div class="row">
                       <?php 
                            if(!empty($sport)){
                                foreach ($sport as $key3) { ?>
                                <?php 
                                    if(!empty($key3->gambar_produk)) { ?>
                                        <div class="col-md-4 mb-5" align="center">
                                            <a href="{{base_url()}}aksesoris/{{$key3->nama_produk_url}}" class="promo-banner btn-block" title="Sport">
                                                <img src="{{ $path_foto_produk . $key3->gambar_produk}}" class="img-fluid" alt="{{ $key3->nama_produk }}" title="{{ $key3->nama_produk }}">
                                                <h2 class="header2-content bg-red text-center pt-2 pb-2 text-white">
                                                    {{ $key3->nama_produk }}
                                                </h2>
                                            </a>
                                        </div>
                                    <?php    } ?>
                                <?php  } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')
    <script type="text/javascript">

        $('.slide-aksesoris').owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            items: 1,

        })
        
        // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}ajax_total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="All News">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });

    </script>
@stop