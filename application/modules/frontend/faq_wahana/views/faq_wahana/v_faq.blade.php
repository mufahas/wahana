@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Frequently Asked Question (FAQ) | Wahana Honda</title>
<meta name="description" content="Daftar pertanyaan yang sering diajukan pengguna baru. Periksalah terlebih dahulu apakah pertanyaan yang akan Anda ajukan telah terjawab atau belum">
@endsection

<style>
    .card-header::before {
        top: 0px !important;
    }
</style>

@section('body')
    
    <div id="for_container">
        @if(!empty($header))
        <div class="banner-home">
            <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="...">
        </div>
        @endif
    
        <div class="container-fluid event">
            <div class="container">
                <h1 class="text-white">FAQ</h1>
            </div>
        </div>
    
        <div class="container faq mt-5 mb-5">
            <div class="accordion" id="accordionExample">
              <?php 
                $count = 1;
               ?>
              @foreach($faq as $faq)
                <div class="card mb-3">
                  <div class="card-header" id="headingOne" type="button" data-toggle="collapse" data-target="#collapse{{$count}}" aria-expanded="false" aria-controls="collapse{{$count}}">
                    <h2 class="header2-content m-auto text-white">
                        {{$faq->pertanyaan}}
                    </h2>
                  </div>
                  <div id="collapse{{$count}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                      {{$faq->jawaban}}
                    </div>
                  </div>
                </div>
                <?php 
                $count++;
               ?>
              @endforeach
            </div>
        </div>
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script type="text/javascript">
      var currentURL = window.location.href;
      var result = currentURL.split('#');
      if(result[1] == "aksesoris"){
        $('#recommend').removeClass("show active");
        $('#aparel').removeClass("show active");
        $('#aksesoris-relate').addClass("show active");
      }else{
        $('#aksesoris-relate').removeClass("show active");
        $('#aparel').removeClass("show active");
        $('#recommend').addClass("show active");
      }
    </script>
    <script type="text/javascript">

        $('.varian').owlCarousel({
            loop:true,
            margin:10,
            dots: true,
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:1
                }
            }
        });

        $('.feature-res').owlCarousel({
            loop:true,
            margin:10,
            dots: true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        })

        $('.aksesoris-terkait').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'owl-nav nav-container-news',
            navClass: [ 'owl-prev prev-news', 'owl-next next-news' ],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        })
        $('.aparel-terkait').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'owl-nav nav-container-news',
            navClass: [ 'owl-prev prev-news', 'owl-next next-news' ],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        })

        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })

        function editfitur($id, $coorX, $coorY, $nama, $deskripsi, $gambar){
          // # berhenti submit
          event.preventDefault();
          var gambar = '{{ base_url() }}assets/upload/fitur/'+$gambar;
          $('input[name="cX"]').val($coorX);
          $('input[name="cY"]').val($coorY);
          $('input[name="nama_fitur"]').val($nama);
          $('textarea[name="deskripsi_fitur"]').val($deskripsi);
          $('input[name="id"]').val($id);
          $("#tampilGambarFitur").attr("src", gambar);
          $('.delete').show();
          $('.gambar-edit').show();
          $('.dropzone-edit').hide();
          $('#m_modal').modal('show');
          $('.save').attr("disabled",false);
        }
        
        // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });

    </script>
@stop