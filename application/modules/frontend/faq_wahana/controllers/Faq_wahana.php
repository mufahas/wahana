<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_wahana extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();
    }

    function index() 
    {
        $faq         = tbl_pertanyaan::where('dihapus','F')->get();
        $data['faq'] = $faq;
        
        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        
        $this->load_view_frontend("frontend","faq_wahana","faq_wahana","v_faq",$data);
    }

}