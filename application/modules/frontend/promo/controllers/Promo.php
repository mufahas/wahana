<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();
    }

    function index() 
    {
        // Where tanggal publish
        // $today = date('Y-m-d');
        // whereRaw('tbl_promo.mulai_promo <="'.$today.'" AND tbl_promo.akhir_promo >="'.$today.'"')
        // 
        $data['banner_promo'] = tbl_promo::where('tbl_promo.status_promo', 'A')->orderBy('tbl_promo.kode_promo','DESC')->limit(5)->get();
        $data['promo']        = tbl_promo::where('tbl_promo.status_promo', 'A')->orderBy('tbl_promo.kode_promo','DESC')->get();
        
        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        
        $this->load_view_frontend("frontend","promo","promo","v_promo",$data);
    }

    function view($url) 
    {
        // Where tanggal publish
        // $today = date('Y-m-d');
        // whereRaw('tbl_promo.mulai_promo <="'.$today.'" AND tbl_promo.akhir_promo >="'.$today.'"')
        
        $data['detail'] = tbl_promo::where('tbl_promo.status_promo', 'A')->where('tbl_promo.judul_promo_url', $url)->first();
        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        
        $this->load_view_frontend("frontend","promo","promo","v_promo_view",$data);
    }

}