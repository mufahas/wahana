@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Daftar Promo Terbaru Motor Honda | Wahana Honda</title>
<meta name="description" content="Jangan lewatkan promo dan diskon terbaru mengenai produk motor Honda dan dapatkan door prize serta merchandise yang menarik selama persediaan masih ada">
@endsection

@section('body')
    
    <nav aria-label="breadcrumb" class="breadcrumb-custom">
        <div class="container">
            <div class="container-fluid">
              <ol class="breadcrumb mb-0 pb-1 pt-1">
                <li class="breadcrumb-item"><a href="{{base_url()}}" title="Wahana">Beranda</a></li>
                <li class="breadcrumb-item active" aria-current="page">Promo</li>
              </ol>
            </div>
        </div>
    </nav>
    
    <div id="for_container">
        <div class="container-fluid">
            <div class="row">
                <div class="owl-carousel owl-theme slide-aksesoris">
                    @if(!empty($banner_promo))
                    @foreach($banner_promo as $vbanner_promo)
                    <div class="item">
                        <img src="{{ base_url() }}assets/upload/promo/{{$vbanner_promo->banner_gambar}}" class="img-fluid" alt="{{$vbanner_promo->judul_promo}}" title="{{$vbanner_promo->judul_promo}}">
                    </div>
                    @endforeach
                    @endif
                    <!-- <div class="item">
                        <img src="{{ base_url() }}assets/frontend/img/promo/1900x750-7.jpg" class="img-fluid" alt="...">
                    </div> -->
                </div>
            </div>
        </div>
    
        <div class="container mt-5">
            
            <div class="text-center">
                <h1 class="header1-content mb-2">Promo Wahana Honda</h1>
                <hr class="separate-line m-auto">
            </div>
            
            <div class="row mt-4">
    
                @if(!empty($promo))
                @foreach($promo as $vpromo)
                <div class="col-md-6 mb-5 loadmore">
                    <a href="{{base_url()}}promo/{{$vpromo->judul_promo_url}}" class="promo-banner btn-block" title="Promo">
                        <img src="{{ base_url() }}assets/upload/promo/{{$vpromo->banner_gambar}}" class="img-fluid" alt="{{$vpromo->judul_promo}}" title="{{$vpromo->judul_promo}}">
                        <h2 class="header2-content bg-red text-center pt-2 pb-2 text-white">
                            {{$vpromo->judul_promo}}
                        </h2>
                    </a>
                </div>
                @endforeach
                <div class="col-md-12 mb-5" align="center">
                    <a href="#" id="loadMore" class="btn btn-danger" title="Load More">Load More</a>
                </div>
                @endif
            </div>
        </div>
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->
@stop

@section('scripts')
    <script type="text/javascript">

        $('.slide-aksesoris').owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            items: 1,
            dotsClass: 'owl-dots dots-container',
        })

    </script>
    
    <script type="text/javascript">
        $(function () {
            if ($(".loadmore").length <= 6) {
                $("#loadMore").hide();
            }

            $(".loadmore").slice(0, 6).show();
            $("#loadMore").on('click', function (e) {
                e.preventDefault();
                $(".loadmore:hidden").slice(0, 6).slideDown();
                if ($(".loadmore:hidden").length == 0) {
                    $("#load").fadeOut('slow');
                }
                if ($(".loadmore:hidden").length <= 0) {
                    $("#loadMore").hide();
                }
            });
        });
        
         // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="All News">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });
    </script>
@stop