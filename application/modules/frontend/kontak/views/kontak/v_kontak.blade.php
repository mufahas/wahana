@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Hubungi Kami | Wahana Honda</title>
<meta name="description" content="Hubungi kami sekarang untuk kritik, saran, keluhan ataupun informasi serta pemesanan sepeda motor Honda di wilayah DKI Jakarta dan Tangerang">
@endsection

@section('body')
    <style type="text/css">
        .anotherWord
        {
            color:#6d6e71;
        }
        .anotherLitleWord
        {
            padding-top: -10px;
            font-weight: 450;
            line-height: 120%;
            font-size: 16px;
            color: #a4a4a4;
        }
        .secondAnotherWord
        {
            color:#6d6e71;
            font-weight: 800;
            font-size: 18px;
        }
        .text-28 {
            font-size: 28px;
        }
        #map
        {
            height: 300px;
            width: 100%;
        }
    </style>
    
    <div id="for_container">
        <div class="container-fluid bg-feature">
            <div class="container pt-5 pb-5">
                <h1 class="text-white">Kontak Kami</h1>
                <p class="text-white">Sampaikan kepada kami agar kami lebih baik lagi</p>
            </div>
        </div>
        <div class="container-fluid  pt-4 mb-1">
            <div class="row">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-6">
                @if(!empty($message_success)) 
                        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show success" role="alert">
                            <div class="m-alert__icon">
                                <i class="la la-warning"></i>
                            </div>
                            <div class="m-alert__text">
                                <strong>
                                    Pesan Terkirim !
                                </strong>
                                {{$message_success}}
                            </div>
                            <div class="m-alert__close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                            </div>
                        </div>
                        @endif
                        @if(!empty($message_error))
                        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-error alert-dismissible fade show error" role="alert">
                            <div class="m-alert__icon">
                                <i class="la la-warning"></i>
                            </div>
                            <div class="m-alert__text">
                                <strong>
                                    Opps!
                                </strong>
                                {{$message_error}}
                            </div>
                            <div class="m-alert__close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                            </div>
                        </div>
                        @endif
                    <div class="pb-2 pt-2">
                        <p class="text-28 font-weight-bold anotherWord">Kontak Kami</p>
                    </div>
                    {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
                        <div class="bg-white">
                            <div class="form-group">
                                <label class="">Nama<i style="color: red;">*</i></label>
                              <input type="text" name="nama_lengkap" class="form-control" id="" placeholder="Nama Lengkap">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="">Alamat Email<i style="color: red;">*</i></label>
                                        <input type="email" name="email" class="form-control" id="" placeholder="Email" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            * No Handphone:
                                        </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control no-hp-otp" name="nomor_hp" maxlength="14" placeholder="08xxxxxxxxxx" onkeypress="return isNumber(event)" autocomplete="off">
                                            <span class="input-group-btn">
                                                <button class="btn btn-secondary button-verifikasi" type="button" id="button-verifikasi" disabled>
                                                    Verifikasi No. HP
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="confirm_code" class="form-control">
                                    </div>

                                    <div class="form-group nomor_verifikasi" style="display:none;">
                                        <label>
                                            Angka Verifikasi
                                        </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="nomor_verifikasi" maxlength="6" placeholder="123456" onkeypress="return isNumber(event)" autocomplete="off">
                                            <span class="input-group-btn">
                                                <button class="btn btn-secondary resend" type="button" id="resend" disabled>
                                                    Resend
                                                </button>
                                                &nbsp;<span id="timer"></span>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show sent-message-success" role="alert" style="display: none;">
                                        <i class="fa fa-envelope-o"></i> Kode Verifikasi Dikirim
                                    </div>
                                    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show sent-message-fail" role="alert" style="display: none;">
                                        <i class="fa fa-remove"></i> Kode Verifikasi Gagal Dikirim !
                                    </div>
                                    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show status-message-wrong" role="alert" style="display: none;">
                                        <i class="fa fa-remove"></i> Kode Verifikasi Salah !
                                    </div>
                                    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show status-message-right" role="alert" style="display: none;">
                                        <i class="fa fa-check"></i> Kode Verifikasi Benar
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="">Subjek<i style="color: red;">*</i></label>
                              <input type="text" class="form-control"  name="subyek_pesan" id="" placeholder="Subyek Pesan">
                            </div>
                            <div class="form-group">
                                <label class="">Pesan<i style="color: red;">*</i></label>
                              <textarea name="isi_pesan" class="form-control" rows="8" cols="80" placeholder="Isi Pesan"></textarea>
                            </div>
                            <div class="form-group" align="center">
                                <div class="g-recaptcha" data-sitekey="6Lcb8VcUAAAAAD94jt6KFVp-rgY_VXTx55lv0y2R"></div>
                                <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" required>
                            </div> 
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-flat-red btn-lg save">Submit</button>
                        </div>
                    {!! form_close() !!}
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-3">
                    <p class="text-28 font-weight-bold anotherWord">Kantor Pusat</p>
                    <div class="pt-4 anotherLitleWord">
                        <p class="text-grey">Gedung Wahana Artha, Jalan. Raya Gunung Sahari <br>
                        No.32, Jakarta 10720, indonesia <br>
                        Phone : <a href="tel:021 601 2070" class="anotherWord" title="No Telp">+6221 601 2070</a> 
                        </p>
                    </div>
                    <div class="pt-3 secondAnotherWord">WHC3</div>
                    <div class="anotherLitleWord">
                        <p class="text-grey">Wahana Honda Customer Care Center <br>
                        Email : customer.care@wahanaarta.com <br>
                        Phone : <a href="tel:0800 188 1700" class="anotherWord" title="No Telp">0800 188 1700</a> (Bebas Pulsa) atau <br>
                        <a href="tel:021 601 2044" class="anotherWord" title="No Telp">+6221 601 2044</a>
                        </p>
                    </div>
                    <div class="pt-3 secondAnotherWord">Sosial Media</div>
                    <div class="anotherLitleWord">
                        <a href="https://www.facebook.com/WahanaHondaJakarta/" class="text-grey fs-18" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true" title="Facebook"></i> @WahanaHondaJakarta</a><br>
                        <a href="https://twitter.com/WahanaHonda" class="text-grey fs-18" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true" title="twitter"></i> @WahanaHonda</a><br>
                        <a href="https://www.instagram.com/wahanahonda/" class="text-grey fs-18" target="_blank"><i class="fa fa-instagram" aria-hidden="true" title="Instagram"></i> @wahanahonda</a><br>
                        <a href="https://www.youtube.com/channel/UCHiyft68meQgEjzaDwofSUw" class="text-grey fs-18" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true" title="Youtube"></i> Wahana Motor Jakarta</a>
                    </div>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-md-12 pt-3">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->
@stop

@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOv9JAHiAWZ34Of1vlN5g1enIb0FWr2qU&callback=initMap"></script>
<script type="text/javascript">
    var xx = '{{ site_url() }}assets/frontend/img/profil/mapswahana.png';
</script>
<script src="{{ base_url() }}assets/frontend/js/validate/jquery.validate.min.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/frontend/js/kontak/kontak.js" type="text/javascript"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
    // Set For Search Form
        var getVerifikasiKode = "{{ $getVerifikasiKode }}";
        var get_news          = "{{base_url()}}ajax_news_filter";
        var count_news_page   = "{{base_url()}}total_page_news";
        var totalPageAll      = "{{$total_page_all}}";

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="All News">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });
</script>
@stop