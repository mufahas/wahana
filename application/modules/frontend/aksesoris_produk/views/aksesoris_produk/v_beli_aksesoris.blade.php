@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Pembelian Aksesoris | Wahana Honda</title>
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('body')

    <div id="for_container">
        @if(!empty($aksesoris))
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-4">
                    @if($message_success)
                    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show success" role="alert">
                        <div class="m-alert__icon">
                            <i class="la la-warning"></i>
                        </div>
                        <div class="m-alert__text">
                            <strong>
                                {{ $message_success }}
                            </strong>
                        </div>
                        <div class="m-alert__close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        </div>
                    </div>
                    @endif

                    @if($message_error)
                    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show success" role="alert">
                        <div class="m-alert__icon">
                            <i class="la la-warning"></i>
                        </div>
                        <div class="m-alert__text">
                            <strong>
                                {{ $message_error }}
                            </strong>
                        </div>
                        <div class="m-alert__close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        </div>
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 mb-3 identitas">
                    <div class="bg-white pl-4 pr-4 pb-4 pt-4 mb-3">
                    <h3 class="text-18"><strong>Form Pemesanan Aksesoris dan Apparel</strong></h3>
                    
                    {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form', 'enctype' => 'multipart/form-data')) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> Nama Aksesoris </label>
                                <input type="text" class="form-control" name="" value="{{ $aksesoris->nama_part_aksesoris }}" readonly>
                            </div>

                            <div class="form-group">
                                <label>* No KTP: </label>
                                <input type="text" class="form-control" name="no_ktp" value="" placeholder="317xxxxxxxxxxx" maxlength="18" onkeypress="return isNumber(event)" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label>* Domisili: </label>
                                <textarea name="domisili" class="form-control" autocomplete="off"></textarea>  
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>* Nama: </label>
                                <input type="text" class="form-control" name="nama_pembeli" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label>* Alamat Email: </label>
                                <input type="text" class="form-control" name="email" value="" placeholder="email@email.com" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label>
                                    * No Handphone:
                                </label>
                                <div class="input-group">
                                    <input type="tel" class="form-control" name="nomor_hp" maxlength="14" placeholder="08xxxxxxxxxx" onkeypress="return isNumber(event)" autocomplete="off">
                                    <span class="input-group-btn">
                                      <button class="btn btn-secondary button-verifikasi" type="button" id="button-verifikasi" disabled>
                                        Verifikasi No. HP
                                      </button>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="hidden" name="confirm_code" class="form-control">
                            </div>

                            <div class="form-group nomor_verifikasi" style="display:none;">
                                <label>
                                    Angka Verifikasi
                                </label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="nomor_verifikasi" maxlength="6" placeholder="123456" onkeypress="return isNumber(event)" autocomplete="off">
                                    <span class="input-group-btn">
                                      <button class="btn btn-secondary resend" type="button" id="resend" disabled>
                                        Resend
                                      </button>
                                      &nbsp;<span id="timer"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show sent-message-success" role="alert" style="display: none;">
                                <i class="fa fa-envelope-o"></i> Kode Verifikasi Dikirim 
                            </div>
                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show sent-message-fail" role="alert" style="display: none;">
                                <i class="fa fa-remove"></i> Kode Verifikasi Gagal Dikirim !
                            </div>
                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show status-message-wrong" role="alert" style="display: none;">
                                <i class="fa fa-remove"></i> Kode Verifikasi Salah ! 
                            </div>
                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show status-message-right" role="alert" style="display: none;">
                                <i class="fa fa-check"></i> Kode Verifikasi Benar 
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div align="center">
                          <div class="g-recaptcha pb-2" data-sitekey="6Lcb8VcUAAAAAD94jt6KFVp-rgY_VXTx55lv0y2R" align="center">
                          </div>
                          <input type="text" class="hiddenRecaptcha test" name="hiddenRecaptcha" id="hiddenRecaptcha" align="center" style="display: none;" required>
                        </div>
                    </div>

                    <div class="submit">
                        <div class="row">
                            <div class="col-sm-3 mb-2 ml-auto mr-auto ">
                                <button type="submit" class="btn btn-danger btn-round btn-block save">Beli Aksesoris</button>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="id_part_aksesoris" value="{{ encryptID($aksesoris->id_part_aksesoris) }}">
                    <input type="hidden" name="nama_part_aksesoris_url" value="{{ $aksesoris->nama_part_aksesoris_url }}">
                    {!! form_close() !!}     
                </div>
            </div>
        </div>
        @endif
    </div>

    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <h2 class="text-white">Berita WMS</h2>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab">
                <div id="contentAll" class="row row-eq-height"></div>
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/pembeli_aksesoris/pembeli-aksesoris.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/validate/jquery.validate.js" type="text/javascript"></script>
    <script type="text/javascript">

        $('.varian').owlCarousel({
            loop:true,
            margin:10,
            dots: true,
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:1
                }
            }
        });

    // Set For Search Form
        var get_news          = "{{base_url()}}ajax_news_filter";
        var count_news_page   = "{{base_url()}}total_page_news";
        var totalPageAll      = "{{$total_page_all}}";
        var getVerifikasiKode = "{{ $getVerifikasiKode }}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);

                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="All News">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' +
                                                                            'Kategori Berita: '+ kategori_berita +
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {

            listNewsAjax.init();

            /* Search For Dekstop */
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';

                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */

            /* Search For Mobile */
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';

                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */
        });

    </script>
@stop
