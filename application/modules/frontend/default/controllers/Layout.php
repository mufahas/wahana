<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Layout extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();
    }

    function index() 
    {
        $this->load_view_frontend("frontend","default","layout","v_layout");
    }

}