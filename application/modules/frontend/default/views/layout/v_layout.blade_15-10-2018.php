<?php 
    $url_wahana = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="google-site-verification" content="FxkycXOOZRH4XslN58jn9ygdL9lfkkXvd2rr7DqbLLY" />
    <meta property="fb:pages" content="135338199812332" />
    <meta name="description" content="Wahanahonda.com, PT Wahana Makmur Sejati merupakan salah satu anak perusahaan dari Wahanaartha Group. PT Wahana Makmur Sejati bergerak di bidang distribusi sepeda motor Honda. Sejak didirikan pada tanggal 6 Agustus 1972, WMS ditetapkan oleh PT.">

    <!-- Canonical Tag -->
    <meta name="robots" content="index, follow">
    <link rel="canonical" href="<?= $url_wahana ?>">

    <meta name="author" content="Wahana Honda Tech Team">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ base_url() }}assets/frontend/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/frontend/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/frontend/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/frontend/css/owl.theme.default.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/frontend/css/lightbox.min.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/frontend/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/frontend/css/styles.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/frontend/css/responsive.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/frontend/css/jquery-ui.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/frontend/css/custom.css"> 
    <link rel="stylesheet" href="{{ base_url() }}assets/frontend/css/custom-tooltip.css"> 
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto" type="text/css">
    <link rel="shortcut icon" href="{{ base_url() }}assets/default/media/img/logo/favicon.ico" />

    @section('head') @show
</head>
<body>
    <nav class="container-fluid header bg-grad-red">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <a href="{{base_url()}}" title="Wahana">
                        <img src="{{ base_url() }}assets/frontend/img/logo@3x.png" class="img-fluid" alt="Logo wahana" title="Logo wahana">
                    </a>
                </div>
            </div>
        </div>

    </nav>

    <!-- Desktop navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-red d-nav" id="navhead">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown full-dropdown active">
                        <a class="nav-link dropdown-toggle {{$active_menu == 'produk' ? 'active' : ''}}" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Product">
                            PRODUK
                        </a>
                        <ul class="dropdown-menu menu-mega" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item dropdown-toggle togle-sub" href="#" id="motorbebek" title="Cub">CUB</a>
                                <ul class="dropdown-menu submenu hovered pl-3 pr-3">
                                    <div class="row">
                                        @if(!empty($menu_bebek))
                                        @foreach($menu_bebek as $vbebek)
                                        <div class="col-sm-2">
                                            <a href="{{base_url()}}produk/{{$vbebek->nama_produk_url}}" title="product details">
                                                <img src="{{ base_url() }}assets/upload/produk/gambar/{{$vbebek->gambar_produk}}" class="img-fluid" alt="Image Product" title="Image Product">
                                                <p class="text-center text-12">{{$vbebek->nama_produk}}</p>
                                            </a>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </ul>
                            </li>
                            <li><a class="dropdown-item dropdown-toggle togle-sub" href="#" id="motormatic" title="Matic">MATIC</a>
                                <ul class="dropdown-menu submenu pl-3 pr-3">
                                    <div class="row">
                                        @if(!empty($menu_matic))
                                        @foreach($menu_matic as $vmatic)
                                        <div class="col-sm-2">
                                            <a href="{{base_url()}}produk/{{$vmatic->nama_produk_url}}" title="product details">
                                                <img src="{{ base_url() }}assets/upload/produk/gambar/{{$vmatic->gambar_produk}}" class="img-fluid" alt="Product Image" title="Product Image">
                                                <p class="text-center text-12">{{$vmatic->nama_produk}}</p>
                                            </a>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </ul>
                            </li>
                            <li><a class="dropdown-item dropdown-toggle togle-sub" href="#" id="motorsport" title="Sport">SPORT</a>
                                <ul class="dropdown-menu submenu pl-3 pr-3">
                                    <div class="row">
                                        @if(!empty($menu_sport))
                                        @foreach($menu_sport as $vsport)
                                        <div class="col-sm-2">
                                            <a href="{{base_url()}}produk/{{$vsport->nama_produk_url}}" title="product details">
                                                <img src="{{ base_url() }}assets/upload/produk/gambar/{{$vsport->gambar_produk}}" class="img-fluid" alt="Product Image" title="Product Image">
                                                <p class="text-center text-12">{{$vsport->nama_produk}}</p>
                                            </a>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </ul>
                            </li>
                            <li><a class="dropdown-item dropdown-toggle togle-sub" href="#" title="Big Bike">BIG BIKE</a>
                                <ul class="dropdown-menu submenu" style="background-color: #000!important">
                                    <a href="http://www.hondabigbike.id/" target="_blank" title="Big Bike Details">
                                        <img src="{{base_url()}}assets/frontend/img/bg-menu-bigbikes.png" class="img-fluid" alt="Product Image" title="Product Image">
                                    </a>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link {{$active_menu == 'aksesoris' ? 'active' : ''}}" href="{{base_url()}}aksesoris" title="Parts dan Aksesoris">PARTS &amp; AKSESORIS</a>
                    </li>
                    
                    <li class="nav-item ">
                        <a class="nav-link {{$active_menu == 'promo' || $active_menu == 'promo' ? 'active' : ''}}" href="{{base_url()}}promo" title="Promo">PROMO</a>
                    </li>
                    
                    <li class="nav-item ">
                        <a class="nav-link {{$active_menu == 'event' || $active_menu == 'event' ? 'active' : ''}}" href="{{base_url()}}event" title="Event">EVENT</a>
                    </li>
                    
                    <!-- LINK DROPDOWN BERITA TERBARU -->
                    <li class="nav-item dropdown full-dropdown">
                        <a class="nav-link dropdown-toggle {{$active_menu == 'news' || $active_menu == 'news' ? 'active' : ''}}" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Blog">
                            BERITA
                        </a>
                        <ul class="dropdown-menu bg-grey-custom" aria-labelledby="navbarDropdownMenuLink">
                            <li>
                                <a class="dropdown-item dropdown-item-custom " href="{{ base_url() }}blog" id="" title="All News">SEMUA BERITA</a>
                            </li>
                            <li>
                                <a class="dropdown-item dropdown-item-custom" href="{{ base_url() }}blog/news" id="" title="Blog News">NEWS</a>
                            </li>
                            <li>
                                <a class="dropdown-item dropdown-item-custom" href="{{ base_url() }}blog/tips" id="" title="Blog Tips">TIPS</a>
                            </li>
                            <li>
                                <a class="dropdown-item dropdown-item-custom" href="{{ base_url() }}blog/event" id="" title="Blog Event">EVENT</a>
                            </li>
                            <li>
                                <a class="dropdown-item dropdown-item-custom" href="{{ base_url() }}blog/press-release" title="Blog Press Release">PRESS RELEASE</a>
                            </li>
                        </ul>
                    </li>
                    <!-- LINK DROPDOWN BERITA TERBARU -->
                    
                    <li class="nav-item">
                        <a class="nav-link {{$active_menu == 'jaringan' ? 'active' : ''}}" href="{{base_url()}}jaringan" title="Dealers">DEALERS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{$active_menu == 'gso' ? 'active' : ''}}" href="{{base_url()}}gso" title="Group Sales">GROUP SALES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{$active_menu == 'profil' ? 'active' : ''}}" href="{{base_url()}}profil" title="Profil">PROFIL</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{$active_menu == 'kontak' ? 'active' : ''}}" href="{{base_url()}}kontak" title="Kontak">KONTAK</a>
                    </li>

                </ul>
                <ul class="navbar-nav my-2 my-lg-0">
                    <li class="nav-item">
                        <a class="nav-link pl-1 pr-1" href="https://www.facebook.com/WahanaHondaJakarta/" target="_blank" title="Facebook"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pl-1 pr-1" href="https://twitter.com/WahanaHonda" target="_blank" title="Twitter"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pl-1 pr-1" href="https://www.instagram.com/wahanahonda/" target="_blank" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pl-1 pr-1" href="https://www.youtube.com/channel/UCHiyft68meQgEjzaDwofSUw" target="_blank" title="Youtube"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#" role="button" id="navSearch" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Search">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right search-nav" aria-labelledby="lang">
                            <form class="form-inline">
                                <div class="input-group">
                                  <input type="text" id="input_search" name="input_search" class="form-control" placeholder="Search" aria-label="search" aria-describedby="basic-addon1">
                                  <button class="input-group-prepend btn btn-danger" type="button" id="button_search">
                                      <i class="fa fa-search" aria-hidden="true"></i>
                                  </button>
                                </div>
                              </form>
                        </div>
                    </li>

              </ul>
            </div>
        </div>
    </nav>

    <!-- Mobile Navbar -->
    <nav class="navbar navbar-light bg-white m-nav" id="navhead_mobile">
        <div class="col-md-12 navbar-header">
            <div class="row pl-2 pr-2 pt-1 pb-1">
                <div class="col-pc-60">
                    <button class="navbar-toggler collapsed menu-bar-button" type="button" data-toggle="collapse" data-target="#navbarNavDropdownM" aria-controls="navbarNavDropdownM" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="col-pc-10 d-flex align-items-center justify-content-center">
                    <a href="https://www.facebook.com/WahanaHondaJakarta/" class="text-white" target="_blank" title="Facebook"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                </div>
                <div class="col-pc-10 d-flex align-items-center justify-content-center">
                    <a href="https://twitter.com/WahanaHonda" class="text-white" target="_blank" title="Twitter"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                </div>
                <div class="col-pc-10 d-flex align-items-center justify-content-center">
                    <a href="https://www.instagram.com/wahanahonda/" class="text-white" target="_blank" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </div>
                <div class="col-pc-10 d-flex align-items-center justify-content-center">
                    <a href="https://www.youtube.com/channel/UCHiyft68meQgEjzaDwofSUw" class="text-white" target="_blank" title="Youtube"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <div class="navbar-collapse collapse" id="navbarNavDropdownM">
            <div class="list-group panel">
                <div class="input-group nav-search">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></div>
                    </div>
                    <input type="text" name="search_nav" class="form-control" id="search_nav" placeholder="SEARCH">
                    <button class="input-group-prepend btn btn-danger" type="button" id="button_search_nav">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </div>
                <a href="#product" class="list-group-item {{$active_menu == 'produk' ? 'active' : ''}}" data-toggle="collapse" data-parent="#MainMenu" title="Product">PRODUCT <i class="fa fa-caret-down right-carret"></i></a>
                <div class="collapse" id="product">
                    <a href="#bebek" class="list-group-item sub-list text-dark-grey pl-1 pr-1" data-toggle="collapse" data-parent="#MainMenu" title="Cub">CUB <i class="fa fa-caret-down right-carret"></i></a>
                    <div class="collapse" id="bebek">
                        @if(!empty($menu_bebek))
                        @foreach($menu_bebek as $vbebek)
                        <a href="{{base_url()}}produk/{{$vbebek->nama_produk_url}}" class="list-group-item sub-sub-list fs-14 pl-2 pr-2 text-dark-grey" title="Product Details">{{$vbebek->nama_produk}}</a>
                        @endforeach
                        @endif
                    </div>
                    <a href="#matic" class="list-group-item sub-list text-dark-grey pl-1 pr-1" data-toggle="collapse" data-parent="#MainMenu" title="Matic">MATIC <i class="fa fa-caret-down right-carret"></i></a>
                    <div class="collapse" id="matic">
                        @if(!empty($menu_matic))
                        @foreach($menu_matic as $vmatic)
                        <a href="{{base_url()}}produk/{{$vmatic->nama_produk_url}}" class="list-group-item sub-sub-list fs-14 pl-2 pr-2 text-dark-grey" title="Product Details">{{$vmatic->nama_produk}}</a>
                        @endforeach
                        @endif
                    </div>
                    <a href="#sport" class="list-group-item sub-list text-dark-grey pl-1 pr-1" data-toggle="collapse" data-parent="#MainMenu" title="Sport">SPORT <i class="fa fa-caret-down right-carret"></i></a>
                    <div class="collapse" id="sport">
                        @if(!empty($menu_sport))
                        @foreach($menu_sport as $vsport)
                        <a href="{{base_url()}}produk/{{$vsport->nama_produk_url}}" class="list-group-item sub-sub-list fs-14 pl-2 pr-2 text-dark-grey" title="Product Details">{{$vsport->nama_produk}}</a>
                        @endforeach
                        @endif
                    </div>
                    <a href="http://www.hondabigbike.id" target="_blank" class="list-group-item sub-list text-dark-grey pl-1 pr-1" title="Big Bike">BIG BIKE </a>
                </div>
                <a href="{{base_url()}}aksesoris" class="list-group-item {{$active_menu == 'aksesoris' ? 'active' : ''}}" title="Parts & Aksesoris">PART &amp; AKSESORIS</a>
                <a href="{{base_url()}}promo" class="list-group-item {{$active_menu == 'promo' || $active_menu == 'promo' ? 'active' : ''}}" title="Promo">PROMO</a>
                <a href="{{base_url()}}event" class="list-group-item {{$active_menu == 'event' || $active_menu == 'event' ? 'active' : ''}}" title="Event">EVENT</a>
                <!--<a href="{{base_url()}}news" class="list-group-item {{$active_menu == 'news' || $active_menu == 'news-detail' ? 'active' : ''}}">BERITA</a>-->
                <a href="#berita" class="list-group-item {{$active_menu == 'blog' || $active_menu == 'blog' ? 'active' : ''}}" data-toggle="collapse" data-parent="#MainMenu" id="dropdownmobileberita" title="Blog">BERITA <i class="fa fa-caret-down right-carret"></i></a>
                <div class="collapse" id="berita">
                    <a href="{{base_url()}}blog" class="list-group-item sub-sub-list fs-14 pl-2 pr-2 text-dark-grey" title="All news">Semua Berita</a>
                    <a href="{{base_url()}}blog/news" class="list-group-item sub-sub-list fs-14 pl-2 pr-2 text-dark-grey" title="Blog News">Berita</a>
                    <a href="{{base_url()}}blog/tips" class="list-group-item sub-sub-list fs-14 pl-2 pr-2 text-dark-grey" title="Blog Tips">Tips</a>
                    <a href="{{base_url()}}blog/event" class="list-group-item sub-sub-list fs-14 pl-2 pr-2 text-dark-grey" title="Blog Event">Event</a>
                    <a href="{{base_url()}}blog/press-release" class="list-group-item sub-sub-list fs-14 pl-2 pr-2 text-dark-grey" title="Blog Press Release">Press Release</a>
                </div>
                <a href="{{base_url()}}jaringan" class="list-group-item {{$active_menu == 'jaringan' ? 'active' : ''}}" title="Dealers">DEALERS</a>
                <a href="{{base_url()}}gso" class="list-group-item {{$active_menu == 'gso' ? 'active' : ''}}" title="Group Sales">GROUP SALES</a>
                <a href="{{base_url()}}profil" class="list-group-item {{$active_menu == 'profil' ? 'active' : ''}}" title="Profil">PROFIL</a>
                <a href="{{base_url()}}kontak" class="list-group-item {{$active_menu == 'kontak' ? 'active' : ''}}" title="Kontak">KONTAK</a>
            </div>
            <div class="d-flex align-items-center p-0">
                <hr style="background-color: #e3bac5; height: 1px; width: 10%;">
            </div>
            
        </div>
    </nav>

    <!-- body -->
    <h1 class="display-none">Wahana Honda</h1>
    @section('body') @show
    <!-- end body -->
    
    <footer>
        <div class="container-fluid">
            <div class="row">
                <!-- <div class="col-lg-2 col-md-4 col-4 no-padding">
                    <img src="{{ base_url() }}assets/frontend/img/17.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-4 no-padding">
                    <img src="{{ base_url() }}assets/frontend/img/27.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-4 no-padding">
                    <img src="{{ base_url() }}assets/frontend/img/29.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-4 m-hide no-padding">
                    <img src="{{ base_url() }}assets/frontend/img/2.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-4 m-hide no-padding">
                    <img src="{{ base_url() }}assets/frontend/img/3.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-4 m-hide no-padding">
                    <img src="{{ base_url() }}assets/frontend/img/14.jpg" class="img-fluid" alt="">
                </div> -->
                <!-- LightWidget WIDGET -->
                <script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/2ffd26c3069b53b9b1cc86b98293515b.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
                <!-- ================ -->
            </div>
        </div>
        <div class="footer bg-dark">
            <div class="footer-container">
                <!-- Footer Desktop -->
                <div class="d-hide">
                    <div class="row">
                        <div class="col-sm-7">
                            <a href="#" title="Help Banner"><img src="{{ base_url() }}assets/frontend/img/help-banner.png" class="img-fluid" alt="Help Banner" title="Help Banner"></a>
                        </div>
                        <div class="col-sm-5">
                            <h3 class="text-red">Wahana Makmur Sejati</h3>
                            <p class="text-light">Gn. Sahari Utara, Sawah Besar,
                                Kota Jakarta Pusat, Daerah Khusus
                                Ibukota Jakarta 10720
                                (021) 6012070</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-6 mb-3">
                            <h3 class="text-red">Tentang Kami</h3>
                            <ul class="nav flex-column">
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}" title="Wahana">Wahana Honda</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}blog" title="Blog">Berita</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="http://www.wahanaartha.com/wahanaartha/Careers/index/ind" target="_blank" title="Karir">Karir</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}kontak" title="Kontak">Kontak</a>
                              </li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-6 mb-3">
                            <h3 class="text-red">Produk</h3>
                            <ul class="nav flex-column">
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}produk/motor-bebek" title="Cub">Motor Bebek</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}produk/motor-matic" title="Matic">Motor Matic</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}produk/motor-sport" title="Sport">Motor Sport</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="http://www.hondabigbike.id/" target="_blank" title="Big Bike">Big Bike</a>
                              </li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-6 mb-3">
                            <h3 class="text-red">Jaringan</h3>
                            <ul class="nav flex-column">
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}gso" title="Customer Group">Customer Group</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="http://assosiasihondajakarta.com/" target="_blank" title="Community Club">Community Club</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}jaringan" title="Dealers">Dealers Wahana</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="https://safetyridingwms.blogspot.co.id/" target="_blank" title="Safety Riding">Safety Riding</a>
                              </li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-6 mb-3">
                            <h3 class="text-red">Highlights</h3>
                            <ul class="nav flex-column">
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}promo" title="Promotion">Promotion</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}aksesoris" title="Parts dan Aksesoris">Parts dan Aksesoris</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}faq-wahana" title="Faq">FAQ</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}kontak" title="Social Media">Social Media</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="{{base_url()}}form-pemenang" title="Form Pemenang">Form Pemenang</a>
                              </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Footer Mobile -->
                <div class="row m-hide footer-link">
                    <div class="col-lg-7 col-md-6">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 t-xs">
                                <h3 class="text-red">Tentang Kami</h3>
                                <ul class="nav flex-column">
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}" title="Wahana">Wahana Honda</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}blog" title="Blog">Berita</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="http://www.wahanaartha.com/wahanaartha/Careers/index/ind" target="_blank" title="Karir">Karir</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}kontak" title="Kontak">Kontak</a>
                                  </li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-6 t-xs">
                                <h3 class="text-red">Produk</h3>
                                <ul class="nav flex-column">
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}produk/motor-bebek" title="Cub">Motor Bebek</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}produk/motor-matic" title="Matic">Motor Matic</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}produk/motor-sport" title="Sport">Motor Sport</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="http://www.hondabigbike.id/" target="_blank" title="Big Bike">Big Bike</a>
                                  </li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-6 t-md">
                                <h3 class="text-red">Jaringan</h3>
                                <ul class="nav flex-column">
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}gso" title="Customer Group">Customer Group</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="http://assosiasihondajakarta.com/" target="_blank" title="Community Club">Community Club</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}jaringan" title="Dealers">Dealers Wahana</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="https://safetyridingwms.blogspot.co.id/" target="_blank" title="Safety Riding">Safety Riding</a>
                                  </li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-6 t-md">
                                <h3 class="text-red">Highlights</h3>
                                <ul class="nav flex-column">
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}promo" title="Promotion">Promotion</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}aksesoris" title="Parts dan Aksesoris">Parts dan Aksesoris</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}faq-wahana" title="Faq">FAQ</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}kontak" title="Social Media">Social Media</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{base_url()}}form-pemenang" title="Form Pemenang">Form Pemenang</a>
                                  </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6 t-xs">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3 class="text-red">Wahana Makmur Sejati</h3>
                                <p class="text-light">Gn. Sahari Utara, Sawah Besar,
                                    Kota Jakarta Pusat, Daerah Khusus
                                    Ibukota Jakarta 10720
                                    (021) 6012070</p>

                            </div>
                            <div class="col-lg-6">
                                <a href="#" title="Form Banner"><img src="{{ base_url() }}assets/frontend/img/help-banner.png" class="img-fluid" alt="Help Banner" title="Help Banner"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <p>
                    Wahanahonda.com, PT Wahana Makmur Sejati merupakan salah satu anak perusahaan dari Wahanaartha Group. PT Wahana Makmur Sejati bergerak di bidang distribusi sepeda motor Honda. Sejak didirikan pada tanggal 6 Agustus 1972, WMS ditetapkan oleh PT. Astra Honda Motor (AHM) sebagai Main Dealer Sepeda Motor Honda untuk wilayah Jakarta dan Tangerang. Sebagai Main Dealer sepeda motor Honda wilayah Jakarta dan Tangerang, PT WMS membawahi 119 Dealer dan 349 AHASS.
                </p>
            </div>
        </div>
        <div class="bg-red">
            <div class="sub-foo">
                2018 © WAHANA MAKMUR SEJATI - ALL RIGHTS RESERVED
            </div>
        </div>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <script src="{{ base_url() }}assets/frontend/js/jquery-3.2.1.min.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/lightbox-plus-jquery.min.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/jquery.dataTables.min.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/jquery.twbsPagination.min.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/popper.min.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/bootstrap.min.js"></script>
    
    <script src="{{ base_url() }}assets/frontend/js/owl.carousel.min.js"></script>
    
    <!-- analytics plugins -->
    <script src="{{ base_url() }}assets/frontend/js/jquery.scrolldepth.min.js"></script>
    <script>
        jQuery(function() {
          jQuery.scrollDepth();
        });
    </script>
    <script src="{{ base_url() }}assets/frontend/js/screentime.js"></script>
    <script>
        $.screentime({
          fields: [
            { selector: '#top',
              name: 'Top'
            },
            { selector: '#middle',
              name: 'Middle'
            },
            { selector: '#bottom',
              name: 'Bottom'
            }
          ],
          callback: function(data) {
            console.log(data);
            // Logs: { Top: 5, Middle: 3 }
          }
        });
    </script>
    <!-- =================== -->
   
    @section('scripts') @show
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-60453044-1"></script>-->
    <!--<script>-->
    <!--  window.dataLayer = window.dataLayer || [];-->
    <!--  function gtag(){dataLayer.push(arguments);}-->
    <!--  gtag('js', new Date());-->

    <!--  gtag('config', 'UA-60453044-1');-->
    <!--</script>-->
    <!-- End Google Analytics --> 
    
    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        
        ga('create', 'UA-60453044-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
    
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '1788118027920633'); 
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=1788118027920633&ev=PageView&noscript=1" alt="Image" title="Image" />
    </noscript>
    <!-- End Facebook Pixel Code -->
    
    <script type="text/javascript">

    $( '.dropdown-menu a.dropdown-toggle' ).on( 'click', function ( e ) {
        var $el = $( this );
        var $parent = $( this ).offsetParent( ".dropdown-menu" );
        if ( !$( this ).next().hasClass( 'show' ) ) {
            $( this ).parents( '.dropdown-menu' ).first().find( '.show' ).removeClass( "show" );
        }
        var $subMenu = $( this ).next( ".dropdown-menu" );
        $subMenu.toggleClass( 'show' );

        $( this ).parent( "li" ).toggleClass( 'show' );

        $( this ).parents( 'li.nav-item.dropdown.show' ).on( 'hidden.bs.dropdown', function ( e ) {
            $( '.dropdown-menu .show' ).removeClass( "show" );
        } );

         if ( !$parent.parent().hasClass( 'navbar-nav' ) ) {
            $el.next().css( { "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 } );
        }

        return false;
    });
    (function($) {
        "use strict";

        var $navbar = $("#navhead"),
            y_pos = $navbar.offset().top,
            height = $navbar.height();

        $(document).scroll(function() {
            var scrollTop = $(this).scrollTop();

            if (scrollTop > y_pos + height) {
                $navbar.addClass("sticky").animate({
                    top: 0
                });
            } else if (scrollTop <= y_pos) {
                $navbar.removeClass("sticky").stop().animate({
                    top: "0"
                }, 0);
            }
        });

    })(jQuery, undefined);


    $( '.dropdown-menu a.dropdown-toggle' ).on( 'click', function ( e ) {
        var $el = $( this );
        var $parent = $( this ).offsetParent( ".dropdown-menu" );
        if ( !$( this ).next().hasClass( 'show' ) ) {
            $( this ).parents( '.dropdown-menu' ).first().find( '.show' ).removeClass( "show" );
        }
        var $subMenu = $( this ).next( ".dropdown-menu" );
        $subMenu.toggleClass( 'show' );

        $( this ).parent( "li" ).toggleClass( 'show' );

        $( this ).parents( 'li.nav-item.dropdown.show' ).on( 'hidden.bs.dropdown', function ( e ) {
            $( '.dropdown-menu .show' ).removeClass( "show" );
        } );

         if ( !$parent.parent().hasClass( 'navbar-nav' ) ) {
            $el.next().css( { "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 } );
        }

        return false;
    } );

    $("[data-toggle='toggle']").click(function() {
        var selector = $(this).data("target");
        $(selector).toggleClass('in');
    });
    $('#close').click(function() {
        $('.sidebar').removeClass('in');
    });

    $('.slide').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        items: 1,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:false,
        stageOuterClass: 'owl-stage-outer outer-stage',
        dotsClass: 'owl-dots dots-container',
        navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
        navContainerClass: 'nav-container',
        navClass: [ 'prev-slide', 'next-slide' ],

    });

    window.onscroll = function() {myFunction()};

    var navbar = document.getElementById("navhead_mobile");
    var sticky = navbar.offsetTop;

    function myFunction() {
      if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }
    
    
    // Desktop
	$('#navSearch').mouseenter(function() {
		if ($('.search-nav').hasClass('show') == false) 
		{
			$('.search-nav').css({
				display: 'none'
			});
		}
	});
	$('#navSearch').click(function() {
		if ($('.search-nav').hasClass('show') == true) 
		{
			$('.search-nav').css({
				display: 'none'
			});
		}
		else
		{
			$('.search-nav').css({
				display: 'block'
			});
		}
	});
	$('#input_search').keypress(function(e) {
		if(e.which == 13) 
		{
			e.preventDefault();
	        $('#button_search').trigger('click');
    	}
    });
    // Mobile
    $('#search_nav').keypress(function(e) {
		if(e.which == 13) 
		{
			e.preventDefault();
	        $('#button_search_nav').trigger('click');
			$('#navbarNavDropdownM').removeClass('show');
    	}
    });
    
    $('#navbarDropdownMenuLink').click(function() {
        location.href = '{{ base_url() }}produk';
    }); 

    $('#motorbebek').click(function() {
        location.href = '{{base_url()}}produk/motor-bebek';
    });

    $('#motormatic').click(function() {
        location.href = '{{base_url()}}produk/motor-matic';
    });

    $('#motorsport').click(function() {
        location.href = '{{base_url()}}produk/motor-sport';
    });

    </script>
    <script>
        $(document).ready(function() {
            $('.page-link').attr('title','Page Link');
            $('.lb-prev').attr('title','Page Link');
            $('.lb-next').attr('title','Page Link');
            $('.lb-cancel').attr('title','Page Link');
            $('.lb-close').attr('title','Page Link');
            $('.lb-image').attr('alt','Image');
            $('.lb-image').attr('title','Image');
        });
    </script>
</body>
</html>
