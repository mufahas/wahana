<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_pemenang extends Eloquent {

	public $table      = 'tbl_pemenang';
	public $primaryKey = 'id_pemenang';
	public $timestamps = false;

}