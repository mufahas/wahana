<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_pemenang extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();

        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {   
        
        $data['action']             = site_url() . $this->site . '/save';
    
        $data['message_success']    = $this->session->flashdata('message_success');
        $data['message_error']      = $this->session->flashdata('messages_error');
        $data['message_exist']      = $this->session->flashdata('message_exist');

        /* For Search */
        $where                      = "tbl_berita.status_publikasi = 'T'";
        $count_all                  = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        
        $this->load_view_frontend("frontend","form_pemenang","form_pemenang","v_form_pemenang",$data);
    }

    function save()
    {   
        
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $url               = site_url() . $this->site;
            
            /* Post From Field Input */
            $nama_lengkap      = $this->input->post('nama_lengkap');
            $no_ktp            = $this->input->post('no_ktp'); 
            $no_hp             = $this->input->post('nomor_hp'); 
            $email             = $this->input->post('email'); 
            $social_media      = $this->input->post('social_media'); 
            
            /* Set Post File */  
            $userfile          = $_FILES["userfile"]["name"];
            $userfile_size     = $_FILES["userfile"]["size"];
            $userfile_tmp_name = $_FILES["userfile"]["tmp_name"]; 
            
            if(!empty($userfile))
            {
                // Set File Name, Extension File, File size, and Allowed Type
                $filename            = $userfile;
                $file_basename       = substr($filename, 0, strripos($filename, '.')); // get file extention
                $file_ext            = substr($filename, strripos($filename, '.')); // get file name extension
                $filesize            = $userfile_size; //get size
                $allowed_file_types  = array('.png','.jpg','.jpeg');

                if(in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 2000000)
                {
                    // Set File Name
                    $file_foto       = 'WIN_' . $no_ktp . '_' . $no_hp . $file_ext;  

                    if(file_exists("assets/upload/pemenang/" . $file_foto))
                    {
                        $this->session->set_flashdata('message_exist', 'Foto Sudah Ada !!!');
                        redirect($url);
                    }
                    else
                    {
                        if(move_uploaded_file($userfile_tmp_name,  "assets/upload/pemenang/" . $file_foto))
                        {
                            /* Begin Save Table: tbl_pemenang */
                            $tbl_pemenang                          = new tbl_pemenang;
                            
                            $tbl_pemenang->nama_lengkap_pemenang   = $nama_lengkap;
                            $tbl_pemenang->no_ktp_pemenang         = $no_ktp;
                            $tbl_pemenang->no_hp_pemenang          = $no_hp;
                            $tbl_pemenang->email_pemenang          = $email;
                            $tbl_pemenang->social_media            = $social_media;
                            $tbl_pemenang->foto_identitas_pemenang = $file_foto;
                            $tbl_pemenang->submit_date             = date('Y-m-d');

                            $save = $tbl_pemenang->save();

                            if($save) 
                            {       
                                $this->session->set_flashdata('message_success', 'Form Berhasil Dikirim ');
                                redirect(site_url() . $this->site);
                            } else {
                                $this->session->set_flashdata('message_error', 'Form Gagal Dikirim');
                                redirect(site_url() . $this->site);
                            }
                        }
                    }
                }
            }
        } else {
            $this->session->set_flashdata('message_error', 'Form Gagal Dikirim');
            redirect(site_url() . $this->site);
        }
    }
}