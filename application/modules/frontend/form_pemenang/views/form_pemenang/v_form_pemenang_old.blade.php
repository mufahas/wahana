@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Wahana Honda | Form Pemenang</title>
@endsection

@section('body')
    <div id="for_container">
        <div class="bg-grey">
            <nav aria-label="breadcrumb" class="bg-grey fs-12">
                <div class="container">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{base_url()}}home">Beranda</a></li>
                    <li class="breadcrumb-item"><a href="{{base_url()}}form-pemenang">Form Pemenang</a></li>
                  </ol>
                </div>
            </nav>
            <div class="container col-md-12">
                <div class="row align-items-center">
                    <div class="form-order-ride">
                    @if(!empty($message_success)) 
              <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show success" role="alert">
                  <div class="m-alert__icon">
                      <i class="la la-warning"></i>
                  </div>
                  <div class="m-alert__text">
                      <strong> 
                      {{$message_success}}
                      </strong>
                  </div>
                  <div class="m-alert__close">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                  </div>
              </div>
            @endif
            @if(!empty($message_error))
            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-error alert-dismissible fade show error" role="alert">
                <div class="m-alert__icon">
                    <i class="la la-warning"></i>
                </div>
                <div class="m-alert__text">
                    <strong>
                    {{$message_error}}
                    </strong>
                </div>
                <div class="m-alert__close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            @endif
            @if(!empty($message_exist))
            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-warning alert-dismissible fade show exist" role="alert">
                <div class="m-alert__icon">
                    <i class="la la-warning"></i>
                </div>
                <div class="m-alert__text">
                    <strong>
                    {{$message_exist}}
                    </strong>
                </div>
                <div class="m-alert__close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            @endif
                {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form', 'enctype' => 'multipart/form-data')) !!}
                    <div class="form-order bg-white">
                        <h3 class="text-18"><strong>Identitas diri</strong></h3>
                        
                        <div class="form-group">
                          <label>* Nama Lengkap</label>
                          <input type="text" class="form-control" name="nama_lengkap" value="">
                        </div>

                        <div class="form-group">
                            <label>* No KTP</label>
                            <input type="text" class="form-control" name="no_ktp" value="" placeholder="31xxxxxxxxxxxxxxxx" maxlength="17">
                        </div>
                  
                        <div class="form-group">
                            <label>* No. Handphone</label>
                            <input type="text" class="form-control" name="nomor_hp" value="" maxlength="14" placeholder="08xxxxxxxxxx">
                        </div>
                      
                        <div class="form-group">
                            <label>* Alamat Email</label>
                            <input type="text" class="form-control" name="email" value="" placeholder="email@email.com">
                        </div>

                        <div class="form-group">
                            <label>* Social Medial (Facebook, Instagram, Etc) </label>
                            <input type="text" class="form-control" name="social_media" value="" placeholder="Instagram/Twitter: @akuninstagram/@akuntwitter, Facebook: akunFacebook">
                        </div>

                        <div class="form-group">
                            <label>* Upload Foto Identitas Diri</label>
                            <div class="upload">
                                <input type="file" name="userfile" class="form-control"> 
                            </div>
                        </div>


                        <div class="g-recaptcha pb-2" data-sitekey="6Lcb8VcUAAAAAD94jt6KFVp-rgY_VXTx55lv0y2R" align="center"></div>
                        
                        <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" align="center" required>
                        
                    </div>
                    <br>
                    <div class="submit">
                      <div class="row">
                          <div class="col-sm-3 mb-2 mr-auto ml-auto ">
                              <button type="submit" class="btn btn-danger btn-round btn-block save">Submit</button>
                          </div>
                      </div>
                    </div>
                {!! form_close() !!}
            </div>
                </div>
            </div>
        </div>
    </div>

    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <h2 class="text-white">Berita WMS</h2>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->
      

    

@stop

@section('scripts')
<script src="{{ base_url() }}assets/frontend/js/form-pemenang/form-pemenang.js"></script>
<script src="{{ base_url() }}assets/frontend/js/validate/jquery.validate.js" type="text/javascript"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
    // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}news-detail/'+v.judul_berita_url+'">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });
</script>
@stop