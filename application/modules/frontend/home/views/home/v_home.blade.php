@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Dealer dan Distributor Resmi Motor Honda | Wahana honda</title>
<meta name="description" content="Dealer dan distributor resmi sepeda motor Honda di Jakarta serta Tangerang. Services prima, mekanik berpengalaman & spare parts ori | Wahana honda">
@endsection

@section('body')
    
    <!-- Modal Banner Wanda -->
    <div class="modal fade show" id="bannerWanda" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content bg-transparent border-0">
            <!-- <div class="post-close-layanan">
                <button type="button" class="close py-3 px-4" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
            </div> -->
            <div class="modal-body p-0">
                <img src="{{ base_url() }}assets/frontend/img/wanda/Pop banner.jpg" class="img-fluid" alt="Wanda" title="Wanda">
                <div class="box-banner-wanda text-center">
                    <a href="{{ base_url() }}wanda" class="btn btn-sm btn-danger w-100 rounded" title="Wanda">Tentang Wanda</a>
                    <a href="https://play.google.com/store/apps/details?id=com.mokita.wahana" target="blank" title="Download Apk Wanda">
                        <img src="{{ base_url() }}assets/frontend/img/wanda/Icon google play-800.png" class="img-fluid img-play" alt="Download Apk Wanda" title="Download Apk Wanda">
                    </a>
                </div>
            </div>
        </div>
      </div>
    </div>
    <button id="btnBannerWanda" class="post-btn-layanan rounded display-none" title="banner wanda" data-toggle="modal" data-target="#bannerWanda">submit</button>
    
    <div id="for_container">
        <div class="container-fluid">
            <div class="row">
                <div class="owl-carousel owl-theme slide">
                    @if(!empty($banner))
                    @foreach($banner as $vbanner)
                    <!-- <a href="https://wahanahonda.com" target="_blank"> -->
                        <?php
                            $find = array(".jpg",".png",".jpeg",".gif",".JPG",".PNG",".JPEG",".GIF");
                            $replace = array("");
                            $title_banner = str_replace($find, $replace, $vbanner->nama_gambar);
                        ?>
                        <div class="item">
                            <?php if (!empty($vbanner->url_banner)){ ?>
                                <?php if ($vbanner->new_page == 'T') { ?>
                                    <a href="{{$vbanner->url_banner}}" title="{{$title_banner}}" target="blank">
                                        <img src="{{ base_url() }}assets/upload/banner/{{$vbanner->nama_gambar}}" class="img-fluid" alt="{{$title_banner}}" title="{{$title_banner}}">
                                    </a>
                                <?php } else { ?>
                                    <a href="{{$vbanner->url_banner}}" title="{{$title_banner}}">
                                        <img src="{{ base_url() }}assets/upload/banner/{{$vbanner->nama_gambar}}" class="img-fluid" alt="{{$title_banner}}" title="{{$title_banner}}">
                                    </a>
                                <?php } ?>
                            <?php }else{ ?>
                                <img src="{{ base_url() }}assets/upload/banner/{{$vbanner->nama_gambar}}" class="img-fluid" alt="{{$title_banner}}" title="{{$title_banner}}">
                            <?php } ?>
                        </div>
                    <!-- </a> -->
                    @endforeach
                    @endif
                </div>
            </div>
        </div>

        <div class="container mt-4">
            <h2 class="small-title">BERITA UTAMA</h2>
            <hr class="separate-line">
            <div class="owl-carousel owl-theme berita-utama">
                @if(!empty($berita_utama))
                @foreach($berita_utama as $vberita_utama)
                <div class="item">
                    <a href="{{base_url()}}blog/{{$vberita_utama->judul_berita_url}}" title="{{$vberita_utama->judul_berita}}">
                        <div class="news">
                            <img src="{{ base_url() }}assets/upload/berita/{{$vberita_utama->gambar_berita}}" alt="{{$vberita_utama->judul_berita}}" title="{{$vberita_utama->judul_berita}}">
                            <div class="overlay">
                                <div class="content-news">
                                    {{$vberita_utama->judul_berita}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
                @endif
            </div>
        </div>

        <div class="container-fluid mt-3">

            <div class="row mb-1">
                @if(!empty($berita_terbaru))
                @foreach($berita_terbaru as $vberita_terbaru)
                <div class="col-sm-4 bt-spc col-home-berita">
                    <a href="{{base_url()}}blog/{{$vberita_terbaru->judul_berita_url}}" title="{{$vberita_terbaru->judul_berita}}">
                        <div class="row align-items-center news-mid-over">
                            <div class="col-sm-12 col-5 pr-0 pl-0 img-over">
                                <img src="{{ base_url() }}assets/upload/berita/{{$vberita_terbaru->gambar_berita}}" class="img-fluid" alt="{{$vberita_terbaru->judul_berita}}" title="{{$vberita_terbaru->judul_berita}}">
                            </div>
                            <div class="col-sm-12 col-7 news-over">
                                <div class="content-news content-news-over">
                                    {{$vberita_terbaru->judul_berita}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
                @endif
            </div>

            <!--<div class="row mb-3 m-hide">-->
            <!--    @if(!empty($sub_berita_terbaru))-->
            <!--    @foreach($sub_berita_terbaru as $vsub_berita_terbaru)-->
            <!--    <div class="col-sm-2 bt-spc col-pc-20">-->
            <!--        <a href="{{base_url()}}news-detail/{{$vsub_berita_terbaru->judul_berita_url}}">-->
            <!--            <div class="row align-items-center news-mid-over">-->
            <!--                <div class="col-sm-12 col-5 pr-0 pl-0 img-over">-->
            <!--                    <img src="{{ base_url() }}assets/upload/berita/{{$vsub_berita_terbaru->gambar_berita}}" class="img-fluid">-->
            <!--                </div>-->
            <!--                <div class="col-sm-12 col-7 news-over">-->
            <!--                    <div class="content-news content-news-over" >-->
            <!--                        {{$vsub_berita_terbaru->judul_berita}}-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--            </div>-->
            <!--        </a>-->
            <!--    </div>-->
            <!--    @endforeach-->
            <!--    @endif-->
            <!--</div>-->

        </div>
    
        <div class="container-fluid mt-3 col-md-3">
            <a href="{{base_url()}}blog" class="btn btn-danger btn-block" title="Blog">Lihat Semua Berita</a>
        </div>


        <div class="container mt-5 mb-5">
        <h2 class="small-title">PRODUK TERBARU</h2>
        <hr class="separate-line">
            <div class="owl-carousel owl-theme produk-terbaik">
                @if(!empty($produk_terbaik))
                @foreach($produk_terbaik as $vproduk_terbaik)
                <div class="item">
                    <a href="{{base_url()}}produk/{{$vproduk_terbaik->nama_produk_url}}" title="{{$vproduk_terbaik->nama_produk}}">
                        <img src="{{ base_url() }}assets/upload/produk/gambar/{{$vproduk_terbaik->gambar_produk}}" class="img-fluid" alt="{{$vproduk_terbaik->nama_produk}}" title="{{$vproduk_terbaik->nama_produk}}">
                        <div class="mt-3">
                            <a href="{{base_url()}}produk/{{$vproduk_terbaik->nama_produk_url}}" class="btn btn-danger btn-block" title="{{$vproduk_terbaik->nama_produk}}">{{$vproduk_terbaik->nama_produk}}</a>
                        </div>
                    </a>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="myTabContent" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')
    <script type="text/javascript">
    
        $('#btnBannerWanda').trigger('click');
        
        $('.berita-utama').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'owl-nav nav-container-news',
            navClass: [ 'owl-prev prev-news', 'owl-next next-news' ],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    slideBy: 2,
                    items:2
                },
                1000:{
                    slideBy: 3,
                    items:3
                }
            }
        });

        $('.produk-terbaik').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'owl-nav nav-container-news',
            navClass: [ 'owl-prev prev-news', 'owl-next next-news' ],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });
        
        /* BEGIN SET FOR SEARCH FORM */
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}ajax_total_page_news";
        var totalPageAll    = "{{$total_page_all}}";
        
        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="All News">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#myTabContent').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#myTabContent').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });
       
        /* END SET FOR SEARCH FORM */

    </script>
@stop