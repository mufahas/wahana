<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();
    }

    function index($judul_berita="") 
    {
        $data['banner']                = ms_gambar_banner_home::where('ms_gambar_banner_home.dihapus', 'F')->where('ms_gambar_banner_home.publish_status', 'T')->whereRaw('ms_gambar_banner_home.jenis = "H" ')->orderby('ms_gambar_banner_home.kode_gambar_banner','DESC')->get();
        $data['berita_utama']          = tbl_berita::join('tbl_gambar_berita','tbl_gambar_berita.kode_berita','=','tbl_berita.kode_berita')->where('tbl_berita.status_publikasi', 'T')->whereRaw('tbl_berita.urutan_berita_utama != "" ')->groupBy('tbl_gambar_berita.kode_berita')->orderby('tbl_berita.urutan_berita_utama','ASC')->orderby('tbl_berita.tanggal_publikasi','DESC')->get();
        $data['berita_terbaru']        = tbl_berita::join('tbl_gambar_berita','tbl_gambar_berita.kode_berita','=','tbl_berita.kode_berita')->where('tbl_berita.status_publikasi', 'T')->WhereRaw('tbl_berita.tanggal_publikasi <= "'.date('Y-m-d H:i:s').'"')->groupBy('tbl_gambar_berita.kode_berita')->orderby('tbl_berita.tanggal_publikasi','DESC')->limit(5)->get();
        // $data['sub_berita_terbaru'] = tbl_berita::join('tbl_gambar_berita','tbl_gambar_berita.kode_berita','=','tbl_berita.kode_berita')->where('tbl_berita.status_publikasi', 'T')->groupBy('tbl_gambar_berita.kode_berita')->orderby('tbl_berita.tanggal_publikasi','DESC')->offset(3)->limit(5)->get();
        // $data['produk_terbaru']     = ms_produk::where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.produk_terbaik', 'T')->orderby('ms_produk.kode_produk','DESC')->get();
        $data['produk_terbaik']        = ms_produk::where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.produk_terbaik', 'T')->orderby('ms_produk.urutan_produk_terbaik','ASC')->get();
        
        /* Begin Initialize For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        /* End Initialize For Search */
        
        $this->load_view_frontend("frontend","home","home","v_home",$data);
    
        /* For Search */
    }
    
    /* For Filtered Search */
    function ajax_get_news_filter()
    {   
        if($this->input->is_ajax_request())
        {   
            $page    = $this->input->post('page');
            $search  = $this->input->post('search');
            $halaman = 12;
            $offset  = ($page * $halaman) - $halaman;
            
            // Set Condition For Where
            $where   = "tbl_berita.status_publikasi = 'T'";
            $where   .= 'AND tbl_berita.judul_berita LIKE "%'.$search.'%"';

            $getAllNews =   tbl_berita::select(
                                'tbl_berita.kategori_berita',
                                'tbl_berita.judul_berita',
                                'tbl_berita.judul_berita_url',
                                'tbl_berita.isi_berita',
                                'tbl_berita.berita_utama',
                                'tbl_berita.status_publikasi',
                                'ms_label.nama_label',
                                'tbl_gambar_berita.gambar_berita'
                            )
                            ->leftJoin('tbl_berita_label', 'tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                            ->leftJoin('ms_label', 'ms_label.kode_label','=','tbl_berita_label.kode_label')
                            ->leftJoin('tbl_gambar_berita', 'tbl_gambar_berita.kode_berita','=','tbl_berita.kode_berita')
                            ->whereRaw($where)
                            ->groupBy('tbl_berita_label.kode_berita')
                            ->orderBy('tbl_berita.tanggal_publikasi','DESC')
                            ->offset($offset)
                            ->limit($halaman)
                            ->get();

            if ($getAllNews != "[]") {
                $status = array('status' => 'success','data' => $getAllNews);
            } else {
                $status = array('status' => 'error');
            }

            $data = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    function ajax_count_news_page() // Counting Searh Pages
    {
        if ($this->input->is_ajax_request()) 
        {
            $search   = $this->input->post('search');
            
            $where = "tbl_berita.status_publikasi = 'T'";
            $where .= " AND tbl_berita.judul_berita LIKE '%".$search."%'";
            
            $count_all = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                ->whereRaw($where)
                                ->groupBy('tbl_berita.kode_berita')
                                ->get();
            $all = 0;
            foreach ($count_all as $value) {
                $all++;
            }
            $total_page_all = ceil($all / 12);

            $result = array('total_page_all' => $total_page_all);

            echo json_encode($result);
        }
    }
    /* End For Inputed Search */

}