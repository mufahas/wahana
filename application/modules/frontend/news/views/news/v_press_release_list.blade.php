@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Press Release | Wahana Honda</title>
<meta name="description" content="Wahanahonda.com, PT Wahana Makmur Sejati merupakan salah satu anak perusahaan dari Wahanaartha Group. PT Wahana Makmur Sejati bergerak di bidang distribusi sepeda motor Honda. Sejak didirikan pada tanggal 6 Agustus 1972, WMS ditetapkan oleh PT.">
@endsection

@section('body')

    <div class="container-fluid bg-feature">
        <div class="container text-center pt-5 pb-5">
            <h1 class="header1-content text-white">PRESS RELEASE WMS</h1>
        </div>
    </div>

    <nav aria-label="breadcrumb" class="bg-grey fs-12">
      <div class="container">
        <div class="container-fluid">
          <ol class="breadcrumb pl-0 mb-0 pb-1 pt-1">
            <li class="breadcrumb-item"><a href="{{base_url()}}">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{base_url()}}blog">Blog</a></li>
            <li class="breadcrumb-item active" aria-current="page">Press Release</li>
          </ol>
        </div>
      </div>
    </nav>

    <div class="container mt-4 mb-5">
        <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="all-news-tab" data-toggle="tab" href="#all-news" role="tab" aria-controls="all-news" aria-selected="false">SEMUA BERITA</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="news-tab" data-toggle="tab" href="#news" role="tab" aria-controls="news" aria-selected="false">BERITA</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="tips-tab" data-toggle="tab" href="#tips" role="tab" aria-controls="tips" aria-selected="false">TIPS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="event-tab" data-toggle="tab" href="#event" role="tab" aria-controls="event" aria-selected="false">EVENT</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link active" id="press-release-tab" data-toggle="tab" href="#pressrelease" role="tab" aria-controls="pressrelease" aria-selected="false">PRESS RELEASE</a>
          </li>
        </ul>
        <div class="tab-content bg-grey mt-4" id="myTabContent">
            <div class="tab-pane fade show active" id="pressrelease" role="tabpanel" aria-labelledby="press-release-tab">    
               <div id="contentPressRelease" class="row row-eq-height"></div>    
                <ul id="paginationPressRelease" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    
    <script type="text/javascript">
       
        function indonesian_date(date){
            var monthArray =  new Array(12);
            monthArray[0]  = 'Januari';
            monthArray[1]  = 'Februari';
            monthArray[2]  = 'Maret';
            monthArray[3]  = 'April';
            monthArray[4]  = 'Mei';
            monthArray[5]  = 'Juni';
            monthArray[6]  = 'Juli';
            monthArray[7]  = 'Agustus';
            monthArray[8]  = 'September';
            monthArray[9]  = 'Oktober';
            monthArray[10] = 'November';
            monthArray[11] = 'Desember';
            var day        = date.getDay();
            var month      = date.getMonth();
            var year       = date.getFullYear();

            return day + ' ' + monthArray[month] + ' ' + year;
        };
    </script>
    <script type="text/javascript">
        
        $("#img-thumn a").on('click', function(e) {
            e.preventDefault();

            $('#preview img').attr('src', $(this).attr('href'));
        });

        $('.aksesoris').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            stageOuterClass: 'owl-stage-outer outer-stage',
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'nav-container',
            navClass: [ 'prev', 'next' ],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:6
                }
            }
        });
    </script>

    <script type="text/javascript">

        $('#all-news-tab').click(function() {
            location.href = '{{ base_url() }}blog';
        });

        $('#news-tab').click(function() {
            location.href = '{{ base_url() }}blog/news';
        });

        $('#tips-tab').click(function() {
            location.href = '{{ base_url() }}blog/tips';
        });

        $('#event-tab').click(function() {
            location.href = '{{ base_url() }}blog/event';
        });

        $('#press-release-tab').click(function() {
            location.href = '{{ base_url() }}blog/press-release';
        });

        var totalPagePressRelease   = "{{$total_page_press_release}}";
        var getPressReleaseNews     = "{{ base_url() }}news-press-release";
        var count_news_page         = "{{ base_url() }}news-total-page";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesPressRelease="") {

                if (totalPagesPressRelease != "") {
                    var totalPressReleasePage = totalPagesPressRelease;
                }else{
                    var totalPressReleasePage = totalPagePressRelease;
                }

                $('#paginationPressRelease').twbsPagination({
                    totalPages: totalPagePressRelease,
                    onPageClick: function (evt, page) {
                        $.ajax({
                            type: 'POST',
                            url: getPressReleaseNews,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function (data) {                     
                                $('#contentPressRelease').html('');
                                $.each(data.data, function(i, item){
                                    var date = new Date(item.tanggal_publikasi);
                                    if ((item.judul_berita).length > 60) {
                                        var str   = item.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = item.judul_berita;
                                    }
                                    $('#contentPressRelease').append('<div class="col-sm-4 bt-spc list-news">'+
                                        '<a href="{{base_url()}}blog/'+item.judul_berita_url+'">'+
                                            '<div class="row align-items-center news-mid-over p-2">'+
                                                '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                    '<img src="{{ base_url() }}assets/upload/berita/'+item.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                '</div>'+
                                                '<div class="col-sm-12 col-7 news-over">'+
                                                    '<h2 class="header2-content content-news content-news-over">'
                                                        +judul+
                                                    '</h2>'+
                                                '</div>'+
                                            '</div>'+
                                        '</a>'+
                                    '</div>');
                                });
                            }
                        }); 
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesPressRelease="") {
                    demo(searchData, totalPagesPressRelease);
                },
            };
        }();

        jQuery(document).ready(function() {
            listNewsAjax.init();
            $(document).on('click', '#button_search', function() {
                var searchData     = $('input[name="input_search"]').val();
                $('#paginationPressRelease').twbsPagination('destroy');
                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function (data) {
                        var totalPagesPressRelease = (data.total_page_press_release != '0') ? data.total_page_press_release : '1';
                        listNewsAjax.init(searchData, totalPagesPressRelease);
                    }
                });
            });
        });

    </script>
@stop