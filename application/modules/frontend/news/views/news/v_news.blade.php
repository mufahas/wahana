@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Informasi Terbaru Tentang Motor | Wahana Honda</title>
<meta name="description" content="Wahanahonda.com, PT Wahana Makmur Sejati merupakan salah satu anak perusahaan dari Wahanaartha Group. PT Wahana Makmur Sejati bergerak di bidang distribusi sepeda motor Honda. Sejak didirikan pada tanggal 6 Agustus 1972, WMS ditetapkan oleh PT.">
@endsection

@section('body')
    
    <div class="container-fluid bg-feature">
        <div class="container text-center pt-5 pb-5">
            <h1 class="header1-content text-white">SEMUA BERITA WMS</h1>
        </div>
    </div>

    <nav aria-label="breadcrumb" class="bg-grey fs-12">
      <div class="container">
        <div class="container-fluid">
          <ol class="breadcrumb pl-0 mb-0 pb-1 pt-1">
            <li class="breadcrumb-item"><a href="{{base_url()}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Blog</li>
          </ol>
        </div>
      </div>
    </nav>


    <div class="container mt-4 mb-5">
        @if(!empty($filter_produk))
        <div><h6>Filter berita berdasarkan label "{{$filter_produk}}"</h6></div>
        @endif
        <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link menu-list news-link active" id="all-news-tab" data-toggle="tab" href="#all-news" role="tab" aria-controls="all-news" aria-selected="false">SEMUA BERITA</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="news-tab" data-toggle="tab" href="#news" role="tab" aria-controls="news" aria-selected="false">BERITA</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="tips-tab" data-toggle="tab" href="#tips" role="tab" aria-controls="tips" aria-selected="false">TIPS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="event-tab" data-toggle="tab" href="#event" role="tab" aria-controls="event" aria-selected="false">EVENT</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="press-release-tab" data-toggle="tab" href="#pressrelease" role="tab" aria-controls="pressrelease" aria-selected="false">PRESS RELEASE</a>
          </li>
        </ul>
        <div class="tab-content bg-grey mt-4" id="myTabContent">
            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab">    <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
                <!-- <nav aria-label="Page navigation example">
                  <ul class="pagination justify-content-end">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item page">Halaman</li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item page">Dari 65</li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav> -->
            </div>

            <div class="tab-pane fade" id="news" role="tabpanel" aria-labelledby="news-tab">
                <div id="contentNews" class="row row-eq-height"></div>
                <ul id="paginationNews" class="pagination-sm justify-content-end"></ul>
            </div>

            <div class="tab-pane fade" id="tips" role="tabpanel" aria-labelledby="tips-tab">
                <div id="contentTips" class="row row-eq-height"></div>    
                <ul id="paginationTips" class="pagination-sm justify-content-end"></ul>
            </div>

            <div class="tab-pane fade" id="event" role="tabpanel" aria-labelledby="event-tab">
                <div id="contentEvent" class="row row-eq-height"></div>
                <ul id="paginationEvent" class="pagination-sm justify-content-end"></ul>
            </div>

            <div class="tab-pane fade" id="pressrelease" role="tabpanel" aria-labelledby="press-release-tab">
                <div id="contentPressRelease" class="row row-eq-height"></div>
                <ul id="paginationPressRelease" class="pagination-sm justify-content-end"></ul>
            </div>
            
        </div>
    </div>

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script>
        $('#all-news-tab').click(function() {
            location.href = '{{ base_url() }}blog';
        });

        $('#news-tab').click(function() {
            location.href = '{{ base_url() }}blog/news';
        });

        $('#tips-tab').click(function() {
            location.href = '{{ base_url() }}blog/tips';
        });

        $('#event-tab').click(function() {
            location.href = '{{ base_url() }}blog/event';
        });

        $('#press-release-tab').click(function() {
            location.href = '{{ base_url() }}blog/press-release';
        });

        var urlProduk      = "{{$url_produk}}";
        var totalPageAll   = "{{$total_page_all}}";
        var totalPageNews  = "{{$total_page_news}}";
        var totalPageTips  = "{{$total_page_tips}}";
        var totalPageEvent = "{{$total_page_event}}";
        var urlNews        = "{{base_url()}}news";
        var urlNewsAll     = "{{base_url()}}ajax_news_all/"+urlProduk;
        var urlNewsNews    = "{{base_url()}}ajax_news_news/"+urlProduk;
        var urlNewsTips    = "{{base_url()}}ajax_news_tips/"+urlProduk;
        var urlNewsEvent   = "{{base_url()}}ajax_news_event/"+urlProduk;
        var urlCountPage   = "{{base_url()}}ajax_count_pages";
        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="", totalPagesNews="", totalPagesTips="", totalPagesEvent="") {
                
                if (totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                }else{
                    var totalAllPage = totalPageAll;
                }
                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function (evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: urlNewsAll,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function (data) {                     
                                $('#contentAll').html('');
                                $.each(data.data, function(i, item){
                                    var date = new Date(item.tanggal_publikasi);
                                    if ((item.judul_berita).length > 60) {
                                        var str   = item.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = item.judul_berita;
                                    }
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                        '<a href="{{base_url()}}blog/'+item.judul_berita_url+'">'+
                                            '<div class="row align-items-center news-mid-over p-2">'+
                                                '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                    '<img src="{{ base_url() }}assets/upload/berita/'+item.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                '</div>'+
                                                '<div class="col-sm-12 col-7 news-over">'+
                                                    '<h2 class="header2-content content-news content-news-over">'
                                                        +judul+
                                                    '</h2>'+
                                                '</div>'+
                                            '</div>'+
                                        '</a>'+
                                    '</div>');
                                });
                            }
                        }); 
                    }
                });              


                if (totalPagesNews != "") {
                    var totalNewsPage = totalPagesNews;
                }else{
                    var totalNewsPage = totalPageNews;
                }
                $('#paginationNews').twbsPagination({
                    totalPages: totalNewsPage,
                    onPageClick: function (evt, page) {
                        $.ajax({
                            type: 'POST',
                            url: urlNewsNews,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function (data) {                     
                                $('#contentNews').html('');
                                $.each(data.data, function(i, item){
                                    var date = new Date(item.tanggal_publikasi);
                                    if ((item.judul_berita).length > 60) {
                                        var str   = item.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = item.judul_berita;
                                    }
                                    $('#contentNews').append('<div class="col-sm-4 bt-spc list-news">'+
                                        '<a href="{{base_url()}}blog/'+item.judul_berita_url+'">'+
                                            '<div class="row align-items-center news-mid-over p-2">'+
                                                '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                    '<img src="{{ base_url() }}assets/upload/berita/'+item.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                '</div>'+
                                                '<div class="col-sm-12 col-7 news-over">'+
                                                    '<h2 class="header2-content content-news content-news-over">'
                                                        +judul+
                                                    '</h2>'+
                                                '</div>'+
                                            '</div>'+
                                        '</a>'+
                                    '</div>');
                                });
                            }
                        }); 
                    }
                });


                if (totalPagesTips != "") {
                    var totalTipsPage = totalPagesTips;
                }else{
                    var totalTipsPage = totalPageTips;
                }
                $('#paginationTips').twbsPagination({
                    totalPages: totalTipsPage,
                    onPageClick: function (evt, page) {
                        $.ajax({
                            type: 'POST',
                            url: urlNewsTips,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function (data) {                     
                                $('#contentTips').html('');
                                $.each(data.data, function(i, item){
                                    var date = new Date(item.tanggal_publikasi);
                                    if ((item.judul_berita).length > 60) {
                                        var str   = item.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = item.judul_berita;
                                    }
                                    $('#contentTips').append('<div class="col-sm-4 bt-spc list-news">'+
                                        '<a href="{{base_url()}}blog/'+item.judul_berita_url+'">'+
                                            '<div class="row align-items-center news-mid-over p-2">'+
                                                '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                    '<img src="{{ base_url() }}assets/upload/berita/'+item.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                '</div>'+
                                                '<div class="col-sm-12 col-7 news-over">'+
                                                    '<h2 class="header2-content content-news content-news-over">'
                                                        +judul+
                                                    '</h2>'+
                                                '</div>'+
                                            '</div>'+
                                        '</a>'+
                                    '</div>');
                                });
                            }
                        }); 
                    }
                });


                if (totalPagesEvent != "") {
                    var totalEventPage = totalPagesEvent;
                }else{
                    var totalEventPage = totalPageEvent;
                }
                $('#paginationEvent').twbsPagination({
                    totalPages: totalEventPage,
                    onPageClick: function (evt, page) {
                        $.ajax({
                            type: 'POST',
                            url: urlNewsEvent,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function (data) {                     
                                $('#contentEvent').html('');
                                $.each(data.data, function(i, item){
                                    var date = new Date(item.tanggal_publikasi);
                                    if ((item.judul_berita).length > 60) {
                                        var str   = item.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = item.judul_berita;
                                    }
                                    $('#contentEvent').append('<div class="col-sm-4 bt-spc list-news">'+
                                        '<a href="{{base_url()}}blog/'+item.judul_berita_url+'">'+
                                            '<div class="row align-items-center news-mid-over p-2">'+
                                                '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                    '<img src="{{ base_url() }}assets/upload/berita/'+item.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                '</div>'+
                                                '<div class="col-sm-12 col-7 news-over">'+
                                                    '<h2 class="header2-content content-news content-news-over">'
                                                        +judul+
                                                    '</h2>'+
                                                '</div>'+
                                            '</div>'+
                                        '</a>'+
                                    '</div>');
                                });
                            }
                        }); 
                    }
                });



            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="", totalPagesNews="", totalPagesTips="", totalPagesEvent="") {
                    demo(searchData, totalPagesAll, totalPagesNews, totalPagesTips, totalPagesEvent);
                },
            };
        }();

        jQuery(document).ready(function() {
            listNewsAjax.init();
            $(document).on('click', '#button_search', function() {
                var searchData     = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');
                $('#paginationNews').twbsPagination('destroy');
                $('#paginationTips').twbsPagination('destroy');
                $('#paginationEvent').twbsPagination('destroy');
                $.ajax({
                    type: 'POST',
                    url: urlCountPage,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function (data) {
                        var totalPagesAll = (data.total_page_all != '0') ? data.total_page_all : '1';
                        var totalPagesNews = (data.total_page_news != '0') ? data.total_page_news : '1';
                        var totalPagesTips = (data.total_page_tips != '0') ? data.total_page_tips : '1';
                        var totalPagesEvent = (data.total_page_event != '0') ? data.total_page_event : '1';
                        listNewsAjax.init(searchData, totalPagesAll, totalPagesNews, totalPagesTips, totalPagesEvent);
                    }
                });
            });
            $(document).on('click', '#button_search_nav', function() {
                $('#navbarNavDropdownM').removeClass('show');
                var searchData     = $('input[name="search_nav"]').val(); 
                $('#paginationAll').twbsPagination('destroy');
                $('#paginationNews').twbsPagination('destroy');
                $('#paginationTips').twbsPagination('destroy');
                $('#paginationEvent').twbsPagination('destroy');
                $.ajax({
                    type: 'POST',
                    url: urlCountPage,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function (data) {     
                        var totalPagesAll = (data.total_page_all != '0') ? data.total_page_all : '1';
                        var totalPagesNews = (data.total_page_news != '0') ? data.total_page_news : '1';
                        var totalPagesTips = (data.total_page_tips != '0') ? data.total_page_tips : '1';
                        var totalPagesEvent = (data.total_page_event != '0') ? data.total_page_event : '1';
                        listNewsAjax.init(searchData, totalPagesAll, totalPagesNews, totalPagesTips, totalPagesEvent);
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
       
        function indonesian_date(date){
            var monthArray =  new Array(12);
            monthArray[0]  = 'Januari';
            monthArray[1]  = 'Februari';
            monthArray[2]  = 'Maret';
            monthArray[3]  = 'April';
            monthArray[4]  = 'Mei';
            monthArray[5]  = 'Juni';
            monthArray[6]  = 'Juli';
            monthArray[7]  = 'Agustus';
            monthArray[8]  = 'September';
            monthArray[9]  = 'Oktober';
            monthArray[10] = 'November';
            monthArray[11] = 'Desember';
            var day        = date.getDay();
            var month      = date.getMonth();
            var year       = date.getFullYear();

            return day + ' ' + monthArray[month] + ' ' + year;
        };
    </script>
    <script type="text/javascript">
        
        $("#img-thumn a").on('click', function(e) {
            e.preventDefault();

            $('#preview img').attr('src', $(this).attr('href'));
        });

        $('.aksesoris').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            stageOuterClass: 'owl-stage-outer outer-stage',
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'nav-container',
            navClass: [ 'prev', 'next' ],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:6
                }
            }
        });

    </script>
@stop