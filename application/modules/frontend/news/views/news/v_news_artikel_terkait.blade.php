@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Wahana Honda | News Wahana Honda</title>
@endsection

@section('body')

    <div class="container-fluid bg-feature">
        <div class="container text-center pt-5 pb-5">
            <h2 class="text-white">Berita WMS</h2>
        </div>
    </div>

    <nav aria-label="breadcrumb" class="bg-grey fs-12">
      <div class="container">
        <div class="container-fluid">
          <ol class="breadcrumb pl-0 mb-0 pb-1 pt-1">
            <li class="breadcrumb-item"><a href="{{base_url()}}news">Beranda</a></li>
            <li class="breadcrumb-item">Artikel Terkait</li>
            <li class="breadcrumb-item active">{{$detail->nama_produk}}</li>
          </ol>
        </div>
      </div>
    </nav>

    <div class="container mt-4 mb-5">
        @if(!empty($detail->nama_produk))
        <div><h6>Filter berita berdasarkan label "{{$detail->nama_produk}}"</h6></div>
        @endif
        <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link menu-list news-link active" id="all-news-tab" data-toggle="tab" href="#all-news" role="tab" aria-controls="all-news" aria-selected="false">SEMUA BERITA</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="news-tab" data-toggle="tab" href="#news" role="tab" aria-controls="news" aria-selected="false">BERITA</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="tips-tab" data-toggle="tab" href="#tips" role="tab" aria-controls="tips" aria-selected="false">TIPS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="event-tab" data-toggle="tab" href="#event" role="tab" aria-controls="event" aria-selected="false">EVENT</a>
          </li>
          <li class="nav-item">
            <a class="nav-link menu-list news-link" id="press-release-tab" data-toggle="tab" href="#pressrelease" role="tab" aria-controls="pressrelease" aria-selected="false">PRESS RELEASE</a>
          </li>
        </ul>

        <div class="tab-content bg-grey mt-4" id="myTabContent">
            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab">    
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
            <div class="tab-pane fade" id="news" role="tabpanel" aria-labelledby="news-tab">
                <div id="contentNews" class="row row-eq-height"></div>
                <ul id="paginationNews" class="pagination-sm justify-content-end"></ul>
            </div>

            <div class="tab-pane fade" id="tips" role="tabpanel" aria-labelledby="tips-tab">
                <div id="contentTips" class="row row-eq-height"></div>    
                <ul id="paginationTips" class="pagination-sm justify-content-end"></ul>
            </div>

            <div class="tab-pane fade" id="event" role="tabpanel" aria-labelledby="event-tab">
                <div id="contentEvent" class="row row-eq-height"></div>
                <ul id="paginationEvent" class="pagination-sm justify-content-end"></ul>
            </div>

            <div class="tab-pane fade" id="pressrelease" role="tabpanel" aria-labelledby="press-release-tab">
                <div id="contentPressRelease" class="row row-eq-height"></div>
                <ul id="paginationPressRelease" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>

    </div>

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script type="text/javascript">

        var urlProduk             = "{{$url_produk}}";
        var totalPageAll          = "{{$total_page_all}}";
        var totalPageNews         = "{{$total_page_news}}";
        var totalPageTips         = "{{$total_page_tips}}";
        var totalPageEvent        = "{{$total_page_event}}";
        var totalPagePressRelease = "{{$total_page_press_release}}";
        var urlNews               = "{{base_url()}}news";
        var urlNewsAll            = "{{base_url()}}ajax_news_all/"+urlProduk;
        var urlNewsNews           = "{{base_url()}}news-news/"+urlProduk;
        var urlNewsTips           = "{{base_url()}}news-tips/"+urlProduk;
        var urlNewsEvent          = "{{base_url()}}news-event/"+urlProduk;
        var urlNewsPressRelase    = "{{base_url()}}news-press-release/"+urlProduk;
        var urlCountPage          = "{{base_url()}}news-total-page";
        
        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="", totalPagesNews="", totalPagesTips="", totalPagesEvent="", totalPagesPressRelease="") {
                
                /* Pagination All News */
                    if (totalPagesAll != "") {
                        var totalAllPage = totalPagesAll;
                    }else{
                        var totalAllPage = totalPageAll;
                    }

                    if(totalPageAll != 0)
                    {
                        $('#paginationAll').twbsPagination({
                            totalPages: totalAllPage,
                            onPageClick: function (evt, page) {

                                $.ajax({
                                    type: 'POST',
                                    url: urlNewsAll,
                                    data: {page: page, search: searchData},
                                    dataType: 'json',
                                    success: function (data) {                     
                                        $('#contentAll').html('');
                                        $.each(data.data, function(i, item){
                                            var date = new Date(item.tanggal_publikasi);
                                            if ((item.judul_berita).length > 60) {
                                                var str   = item.judul_berita;
                                                var judul = str.substring(0,60)+"...";                          
                                            }else{
                                                var judul = item.judul_berita;
                                            }
                                            $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                '<a href="{{base_url()}}blog/'+item.judul_berita_url+'">'+
                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                            '<img src="{{ base_url() }}assets/upload/berita/'+item.gambar_berita+'" class="img-fluid">'+
                                                        '</div>'+
                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                            '<div class="content-news content-news-over">'
                                                                +judul+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</a>'+
                                            '</div>');
                                        });
                                    }
                                }); 
                            }
                        });              
                    }
                /* Pagination All News */

                /* Pagination News */
                    if (totalPagesNews != "") {
                        var totalNewsPage = totalPagesNews;
                    }else{
                        var totalNewsPage = totalPageNews;
                    }

                    if(totalPageNews != 0)
                    {
                        $('#paginationNews').twbsPagination({
                            totalPages: totalNewsPage,
                            onPageClick: function (evt, page) {
                                $.ajax({
                                    type: 'POST',
                                    url: urlNewsNews,
                                    data: {page: page, search: searchData},
                                    dataType: 'json',
                                    success: function (data) {                     
                                        $('#contentNews').html('');
                                        $.each(data.data, function(i, item){
                                            var date = new Date(item.tanggal_publikasi);
                                            if ((item.judul_berita).length > 60) {
                                                var str   = item.judul_berita;
                                                var judul = str.substring(0,60)+"...";                          
                                            }else{
                                                var judul = item.judul_berita;
                                            }
                                            $('#contentNews').append('<div class="col-sm-4 bt-spc list-news">'+
                                                '<a href="{{base_url()}}blog/'+item.judul_berita_url+'">'+
                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                            '<img src="{{ base_url() }}assets/upload/berita/'+item.gambar_berita+'" class="img-fluid">'+
                                                        '</div>'+
                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                            '<div class="content-news content-news-over">'
                                                                +judul+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</a>'+
                                            '</div>');
                                        });
                                    }
                                }); 
                            }
                        });
                    }
                /* Pagination News */

                /* Pagination Tips */
                    if (totalPagesTips != "") {
                        var totalTipsPage = totalPagesTips;
                    }else{
                        var totalTipsPage = totalPageTips;
                    }

                    if(totalPageTips != 0)
                    {
                        $('#paginationTips').twbsPagination({
                            totalPages: totalTipsPage,
                            onPageClick: function (evt, page) {
                                $.ajax({
                                    type: 'POST',
                                    url: urlNewsTips,
                                    data: {page: page, search: searchData},
                                    dataType: 'json',
                                    success: function (data) {                     
                                        $('#contentTips').html('');

                                        if(data != undefined)
                                        {
                                            $.each(data.data, function(i, item){
                                                var date = new Date(item.tanggal_publikasi);
                                                if ((item.judul_berita).length > 60) {
                                                    var str   = item.judul_berita;
                                                    var judul = str.substring(0,60)+"...";                          
                                                }else{
                                                    var judul = item.judul_berita;
                                                }
                                                $('#contentTips').append('<div class="col-sm-4 bt-spc list-news">'+
                                                    '<a href="{{base_url()}}blog/'+item.judul_berita_url+'">'+
                                                        '<div class="row align-items-center news-mid-over p-2">'+
                                                            '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                '<img src="{{ base_url() }}assets/upload/berita/'+item.gambar_berita+'" class="img-fluid">'+
                                                            '</div>'+
                                                            '<div class="col-sm-12 col-7 news-over">'+
                                                                '<div class="content-news content-news-over">'
                                                                    +judul+
                                                                '</div>'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</a>'+
                                                '</div>');
                                            });
                                        }
                                    }
                                }); 
                            }
                        });
                    }
                /* Pagination Tips */

                /* Pagination Event */
                    if (totalPagesEvent != "") {
                        var totalEventPage = totalPagesEvent;
                    }else{
                        var totalEventPage = totalPageEvent;
                    }

                    if(totalPageEvent != 0)
                    {
                        $('#paginationEvent').twbsPagination({
                            totalPages: totalEventPage,
                            onPageClick: function (evt, page) {
                                $.ajax({
                                    type: 'POST',
                                    url: urlNewsEvent,
                                    data: {page: page, search: searchData},
                                    dataType: 'json',
                                    success: function (data) {                     
                                        $('#contentEvent').html('');
                                        $.each(data.data, function(i, item){
                                            var date = new Date(item.tanggal_publikasi);
                                            if ((item.judul_berita).length > 60) {
                                                var str   = item.judul_berita;
                                                var judul = str.substring(0,60)+"...";                          
                                            }else{
                                                var judul = item.judul_berita;
                                            }
                                            $('#contentEvent').append('<div class="col-sm-4 bt-spc list-news">'+
                                                '<a href="{{base_url()}}blog/'+item.judul_berita_url+'">'+
                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                            '<img src="{{ base_url() }}assets/upload/berita/'+item.gambar_berita+'" class="img-fluid">'+
                                                        '</div>'+
                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                            '<div class="content-news content-news-over">'
                                                                +judul+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</a>'+
                                            '</div>');
                                        });
                                    }
                                }); 
                            }
                        });
                    }
                /* Pagination Event */
                
                /* Pagination Press Release */
                    if (totalPagesPressRelease != "") {
                        var totalPressReleasePage = totalPagesPressRelease;
                    }else{
                        var totalPressReleasePage = totalPagePressRelease;
                    }

                    if(totalPagePressRelease != 0)
                    {
                        $('#paginationPressRelease').twbsPagination({
                            totalPages: totalPressReleasePage,
                            onPageClick: function (evt, page) {
                                $.ajax({
                                    type: 'POST',
                                    url: urlNewsPressRelase,
                                    data: {page: page, search: searchData},
                                    dataType: 'json',
                                    success: function (data) {               
                                        $('#contentPressRelease').html('');
                                        $.each(data.data, function(i, item){
                                            var date = new Date(item.tanggal_publikasi);
                                            if ((item.judul_berita).length > 60) {
                                                var str   = item.judul_berita;
                                                var judul = str.substring(0,60)+"...";                          
                                            }else{
                                                var judul = item.judul_berita;
                                            }
                                            $('#contentPressRelease').append('<div class="col-sm-4 bt-spc list-news">'+
                                                '<a href="{{base_url()}}blog/'+item.judul_berita_url+'">'+
                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                            '<img src="{{ base_url() }}assets/upload/berita/'+item.gambar_berita+'" class="img-fluid">'+
                                                        '</div>'+
                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                            '<div class="content-news content-news-over">'
                                                                +judul+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</a>'+
                                            '</div>');
                                        });
                                    }
                                }); 
                            }
                        });
                    }
                /* Pagination Press Release */
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="", totalPagesNews="", totalPagesTips="", totalPagesEvent="", totalPagesPressRelease="") {
                    demo(searchData, totalPagesAll, totalPagesNews, totalPagesTips, totalPagesEvent, totalPagesPressRelease);
                },
            };
        }();

         jQuery(document).ready(function() {
            listNewsAjax.init();
            $(document).on('click', '#button_search', function() {
                var searchData     = $('input[name="input_search"]').val();
               
                $('#paginationAll').twbsPagination('destroy');
                $('#paginationNews').twbsPagination('destroy');
                $('#paginationTips').twbsPagination('destroy');
                $('#paginationEvent').twbsPagination('destroy');
                $('#paginationPressRelease').twbsPagination('destroy');
                
                $.ajax({
                    type: 'POST',
                    url: urlCountPage,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function (data) {
                        var totalPagesAll           = (data.total_page_all != '0') ? data.total_page_all : '1';
                        var totalPagesNews          = (data.total_page_news != '0') ? data.total_page_news : '1';
                        var totalPagesTips          = (data.total_page_tips != '0') ? data.total_page_tips : '1';
                        var totalPagesEvent         = (data.total_page_event != '0') ? data.total_page_event : '1';
                        var totalPagesPressRelease  = (data.total_page_press_release != '0') ? data.total_page_press_release : '1';
                        listNewsAjax.init(searchData, totalPagesAll, totalPagesNews, totalPagesTips, totalPagesEvent, totalPagesPressRelease);
                    }
                });
            });
            
        });
    </script>
@stop