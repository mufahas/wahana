@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Wahana Honda | Detail News</title>
@endsection

@section('body')
    
    <nav aria-label="breadcrumb" class="bg-grey fs-12">
      <div class="container">
        <div class="container-fluid">
          <ol class="breadcrumb pl-0 mb-0 pb-2 pt-2">
            <li class="breadcrumb-item"><a href="{{base_url()}}news">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Berita</li>
          </ol>
        </div>
      </div>
    </nav>
    <div class="container pb-5">
        <div class="row row-eq-height">
            <div class="col-md-9">
                <h1>{{$detail->judul_berita}}</h1>
                <p class="date-news">{{$selisih_waktu_publikasi}}</p>
                <div class="row">
                    <div class="col-fix-6">
                        @if(!empty($berita_sebelumnya))
                        <div class="nav justify-content-start">
                            <a href="{{$berita_sebelumnya->judul_berita_url}}" class="btn-trans-red"><i class="fa fa-chevron-left" aria-hidden="true"></i> Berita sebelumnya</a>
                        </div>    
                        @endif
                    </div>
                    <div class="col-fix-6">
                        @if(!empty($berita_selanjutnya))
                        <div class="nav justify-content-end">
                            <a href="{{$berita_selanjutnya->judul_berita_url}}" class="btn-trans-red">Berita selanjutnya <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
        <div class="row">
            <div class="col-md-9">

                <div class="owl-carousel owl-theme slide">
                    @if(!empty($banner))
                    @foreach($banner as $vbanner)
                    <div class="item">
                        <img src="{{ base_url() }}assets/upload/berita/{{$vbanner->gambar_berita}}" class="img-fluid mb-3" alt="">
                    </div>
                    @endforeach
                    @endif
                </div>

                <p>{!! $detail->isi_berita !!}</p>

                <p>
                    Label : 
                    @if(!empty($berita_label))
                    @foreach($berita_label as $vberita_label)
                        <span class="label bg-red text-white">{{$vberita_label->nama_label}}</span>
                    @endforeach
                    @endif
                </p>

                <div class="comment mt-5">
                    <h3 class="text-16 mb-3"><strong>Comment</strong></h3>
                    <div class="media mb-4">
                      <img class="mr-3 img-comment" src="{{ base_url() }}assets/frontend/img/grey-circle-.png" alt="">
                      <div class="media-body">
                        <h5 class="mt-0 text-16">Your comment</h5>
                        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
                        <div class="modal fade pt-5" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            Silahkan Masukkan Nama dan Alamat Email Anda
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">
                                                &times;
                                            </span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Anda"> <br>
                                        <input type="text" name="email" class="form-control" placeholder="Masukkan Email Anda">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                                        <button type="submit" class="btn btn-danger save">
                                            Kirim
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            @if(!empty($messages)) 
                                <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show success" role="alert">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        <strong>
                                            {{ $messages }} ! 
                                        </strong>
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group m-form__group">
                                <input type="text" class="form-control m-input" name="komentar" placeholder="Add a comment... " value="">
                            </div>   
                             
                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                <div class="m-form__actions m-form__actions--solid">
                                    <div class="row">
                                        <div class="col-lg-12 m--align-right">
                                            <button type="button" class="btn btn-danger kirim">Kirim</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <input type="hidden" name="kode_berita" value="<?= encryptID($detail->kode_berita) ?>">
                         
                        {!! form_close() !!}
                      </div>
                    </div>
                    @if(!empty($komentar))
                        @foreach($komentar as $kom)
                        <div class="loadmore">
                            <div class="media mb-4">
                              <img class="mr-3 img-comment" src="{{ base_url() }}assets/frontend/img/grey-circle-.png" alt="">
                                <div class="media-body">
                                    <h5 class="mt-0 text-16">
                                        {{ $kom->nama }}
                                    </h5>
                                        {{ $kom->isi_komentar }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div align="center">
                            <a href="#" id="loadMore" class="btn btn-danger">Load More</a>
                        </div>
                        <!--  <p class="totop">
                            <a href="#top">Back to top</a> 
                        </p> -->
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="bg-red text-center text-white pt-2 pb-2">
                    Berita lainnya
                </div>
                @if(!empty($berita_lainnya))
                @foreach($berita_lainnya as $vberita_lainnya)
                <a href="{{base_url()}}news-detail/{{$vberita_lainnya->judul_berita_url}}">
                    <div class="other-news-list">
                        <p class="date-news mb-0">{{date('d F y'), strtotime($vberita_lainnya->tanggal_publikasi)}}</p>
                        @if (strlen($vberita_lainnya->judul_berita) > 45 )
                        <h3 class="title-news">{{substr($vberita_lainnya->judul_berita, 0,45).'...'}}</h3>
                        @else
                        <h3 class="title-news">{{$vberita_lainnya->judul_berita}}</h3>
                        @endif
                    </div>
                </a>
                @endforeach
                @else
                <div class="other-news-list">
                    <h3 class="title-news text-center">Belum ada berita terkait</h3>
                </div>
                @endif
            </div>
        </div>

    </div>

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/validate/jquery.validate.js" type="text/javascript"></script>
    <script src="{{ base_url() }}assets/frontend/js/news/news.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        $(function () {
            if ($(".loadmore").length <= 3) {
                $("#loadMore").hide();
            }

            $(".loadmore").slice(0, 3).show();
            $("#loadMore").on('click', function (e) {
                e.preventDefault();
                $(".loadmore:hidden").slice(0, 5).slideDown();
                if ($(".loadmore:hidden").length == 0) {
                    $("#load").fadeOut('slow');
                }
                if ($(".loadmore:hidden").length <= 0) {
                    $("#loadMore").hide();
                }
                // $('html,body').animate({
                //     scrollTop: $(this).offset().top
                // }, 1500);
            });
        });

        // $('a[href=#top]').click(function () {
        //     $('body,html').animate({
        //         scrollTop: 0
        //     }, 600);
        //     return false;
        // });

        // $(window).scroll(function () {
        //     if ($(this).scrollTop() > 50) {
        //         $('.totop a').fadeIn();
        //     } else {
        //         $('.totop a').fadeOut();
        //     }
        // });
    </script>
    
@stop