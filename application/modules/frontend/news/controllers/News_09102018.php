<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class News extends MY_Controller
{
    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index($produk_url = "") 
    {
        $data['filter_produk'] = "";
        $data['url_produk'] = "";
        $where = "tbl_berita.status_publikasi = 'T'";
        if (!empty($produk_url)) {
            $produk = ms_produk::where('ms_produk.nama_produk_url', $produk_url)->first();
            $label  = tbl_berita_label::select('ms_label.nama_label')->join('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')->where('ms_label.dihapus', 'F')->get();
            if (!empty($produk)) {
                foreach ($label as $value) {
                    if ($value->nama_label == $produk->nama_produk ) {
                        $data['filter_produk'] = $produk->nama_produk;
                        $data['url_produk']    = $produk->nama_produk_url;
                        $where .= " AND ms_label.nama_label = '".$produk->nama_produk."'";
                    }
                }
            }
        }

        $count_all = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                ->whereRaw($where)
                                ->groupBy('tbl_berita.kode_berita')
                                ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);

        $count_news = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                ->whereRaw($where)
                                ->where('tbl_berita.kategori_berita','B')
                                ->groupBy('tbl_berita.kode_berita')
                                ->get();
        $news = 0;
        foreach ($count_news as $value) {
            $news++;
        }
        $data['total_page_news'] = ceil($news / 12);

        $count_tips = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                ->whereRaw($where)
                                ->where('tbl_berita.kategori_berita','T')
                                ->groupBy('tbl_berita.kode_berita')
                                ->get();
        $tips = 0;
        foreach ($count_tips as $value) {
            $tips++;
        }
        $data['total_page_tips'] = ceil($tips / 12);

        $count_event = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                ->whereRaw($where)
                                ->where('tbl_berita.kategori_berita','E')
                                ->groupBy('tbl_berita.kode_berita')
                                ->get();
        $event = 0;
        foreach ($count_event as $value) {
            $event++;
        }
        $data['total_page_event'] = ceil($event / 12);

        $this->load_view_frontend("frontend","news","news","v_news",$data);
    }

    function get_all_news($produk_url = ""){
        if ($this->input->is_ajax_request()) 
        {
            $page     = $this->input->post('page');
            $search   = $this->input->post('search');
            $halaman  = "12";
            $offset   = ($page * $halaman) - $halaman;
            
            $where = "tbl_berita.status_publikasi = 'T'";
            $where .= " AND tbl_berita.judul_berita LIKE '%".$search."%'";

            if (!empty($produk_url)) {
                $produk = ms_produk::where('ms_produk.nama_produk_url', $produk_url)->first();
                $label  = tbl_berita_label::select('ms_label.nama_label')->join('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')->where('ms_label.dihapus', 'F')->get();
                if (!empty($produk)) {
                    foreach ($label as $value) {
                        if ($value->nama_label == $produk->nama_produk ) {
                            $where .= " AND ms_label.nama_label = '".$produk->nama_produk."'";
                        }
                    }
                }
            }

            $getByPage = tbl_berita::leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                    ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                    ->leftJoin('tbl_gambar_berita','tbl_gambar_berita.kode_berita','=','tbl_berita.kode_berita')
                                    ->whereRaw($where)
                                    ->groupBy('tbl_gambar_berita.kode_berita')
                                    ->orderby('tbl_berita.tanggal_publikasi','DESC')
                                    ->offset($offset)
                                    ->limit($halaman)
                                    ->get();

            if ($getByPage != "[]") {
                $status = array('status' => 'success','data' => $getByPage);
            } else {
                $status = array('status' => 'error');
            }

            $data = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    function get_news_news($produk_url = ""){
        if ($this->input->is_ajax_request()) 
        {
            $page     = $this->input->post('page');
            $search   = $this->input->post('search');
            $halaman  = "12";
            $offset   = ($page * $halaman) - $halaman;
            
            $where = "tbl_berita.status_publikasi = 'T' AND tbl_berita.kategori_berita = 'B'";
            $where .= " AND tbl_berita.judul_berita LIKE '%".$search."%'";
            if (!empty($produk_url)) {
                $produk = ms_produk::where('ms_produk.nama_produk_url', $produk_url)->first();
                $label  = tbl_berita_label::select('ms_label.nama_label')->join('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')->where('ms_label.dihapus', 'F')->get();
                if (!empty($produk)) {
                    foreach ($label as $value) {
                        if ($value->nama_label == $produk->nama_produk ) {
                            $where .= " AND ms_label.nama_label = '".$produk->nama_produk."'";
                        }
                    }
                }
            }

            $getByPage = tbl_berita::leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                    ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                    ->leftJoin('tbl_gambar_berita','tbl_gambar_berita.kode_berita','=','tbl_berita.kode_berita')
                                    ->whereRaw($where)
                                    ->groupBy('tbl_gambar_berita.kode_berita')
                                    ->orderby('tbl_berita.tanggal_publikasi','DESC')
                                    ->offset($offset)
                                    ->limit($halaman)
                                    ->get();
            if ($getByPage != "[]") {
                $status = array('status' => 'success','data'=>$getByPage);
            } else {
                $status = array('status' => 'error');
            }
            $data = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    function get_tips_news($produk_url = ""){
        if ($this->input->is_ajax_request()) 
        {
            $page     = $this->input->post('page');
            $search   = $this->input->post('search');
            $halaman  = "12";
            $offset   = ($page * $halaman) - $halaman;
            
            $where = "tbl_berita.status_publikasi = 'T' AND tbl_berita.kategori_berita = 'T'";
            $where .= " AND tbl_berita.judul_berita LIKE '%".$search."%'";
            if (!empty($produk_url)) {
                $produk = ms_produk::where('ms_produk.nama_produk_url', $produk_url)->first();
                $label  = tbl_berita_label::select('ms_label.nama_label')->join('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')->where('ms_label.dihapus', 'F')->get();
                if (!empty($produk)) {
                    foreach ($label as $value) {
                        if ($value->nama_label == $produk->nama_produk ) {
                            $where .= " AND ms_label.nama_label = '".$produk->nama_produk."'";
                        }
                    }
                }
            }

            $getByPage = tbl_berita::leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                    ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                    ->leftJoin('tbl_gambar_berita','tbl_gambar_berita.kode_berita','=','tbl_berita.kode_berita')
                                    ->whereRaw($where)
                                    ->groupBy('tbl_gambar_berita.kode_berita')
                                    ->orderby('tbl_berita.tanggal_publikasi','DESC')
                                    ->offset($offset)
                                    ->limit($halaman)
                                    ->get();
            if ($getByPage != "[]") {
                $status = array('status' => 'success','data'=>$getByPage);
            } else {
                $status = array('status' => 'error');
            }
            $data = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    function get_event_news($produk_url = ""){
        if ($this->input->is_ajax_request()) 
        {
            $page     = $this->input->post('page');
            $search   = $this->input->post('search');
            $halaman  = "12";
            $offset   = ($page * $halaman) - $halaman;
            
            $where = "tbl_berita.status_publikasi = 'T' AND tbl_berita.kategori_berita = 'E'";
            $where .= " AND tbl_berita.judul_berita LIKE '%".$search."%'";
            if (!empty($produk_url)) {
                $produk = ms_produk::where('ms_produk.nama_produk_url', $produk_url)->first();
                $label  = tbl_berita_label::select('ms_label.nama_label')->join('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')->where('ms_label.dihapus', 'F')->get();
                if (!empty($produk)) {
                    foreach ($label as $value) {
                        if ($value->nama_label == $produk->nama_produk ) {
                            $where .= " AND ms_label.nama_label = '".$produk->nama_produk."'";
                        }
                    }
                }
            }

            $getByPage = tbl_berita::leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                    ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                    ->leftJoin('tbl_gambar_berita','tbl_gambar_berita.kode_berita','=','tbl_berita.kode_berita')
                                    ->whereRaw($where)
                                    ->groupBy('tbl_gambar_berita.kode_berita')
                                    ->orderby('tbl_berita.tanggal_publikasi','DESC')
                                    ->offset($offset)
                                    ->limit($halaman)
                                    ->get();
            if ($getByPage != "[]") {
                $status = array('status' => 'success','data'=>$getByPage);
            } else {
                $status = array('status' => 'error');
            }
            $data = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    function count_pages(){
        if ($this->input->is_ajax_request()) 
        {
            $search   = $this->input->post('search');
            
            $where = "tbl_berita.status_publikasi = 'T'";
            $where .= " AND tbl_berita.judul_berita LIKE '%".$search."%'";
            
            $count_all = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                ->whereRaw($where)
                                ->groupBy('tbl_berita.kode_berita')
                                ->get();
            $all = 0;
            foreach ($count_all as $value) {
                $all++;
            }
            $total_page_all = ceil($all / 12);

            $count_news = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                    ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                    ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                    ->whereRaw($where)
                                    ->where('tbl_berita.kategori_berita','B')
                                    ->groupBy('tbl_berita.kode_berita')
                                    ->get();
            $news = 0;
            foreach ($count_news as $value) {
                $news++;
            }
            $total_page_news = ceil($news / 12);

            $count_tips = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                    ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                    ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                    ->whereRaw($where)
                                    ->where('tbl_berita.kategori_berita','T')
                                    ->groupBy('tbl_berita.kode_berita')
                                    ->get();
            $tips = 0;
            foreach ($count_tips as $value) {
                $tips++;
            }
            $total_page_tips = ceil($tips / 12);

            $count_event = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                    ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                    ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                    ->whereRaw($where)
                                    ->where('tbl_berita.kategori_berita','E')
                                    ->groupBy('tbl_berita.kode_berita')
                                    ->get();
            $event = 0;
            foreach ($count_event as $value) {
                $event++;
            }
            $total_page_event = ceil($event / 12);

            $data = array('total_page_all'=>$total_page_all, 'total_page_news'=>$total_page_news, 'total_page_tips'=>$total_page_tips, 'total_page_event'=>$total_page_event);

            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    function view($url) 
    {
        $data['action']             = base_url().'news-komentar/save';

        $data['detail']             = tbl_berita::where('tbl_berita.status_publikasi','T')->where('tbl_berita.judul_berita_url', $url)->first();

        $data['banner']             = tbl_gambar_berita::where('tbl_gambar_berita.kode_berita', $data['detail']->kode_berita)->get();
        
        $data['komentar']           = tbl_komentar::leftJoin('tbl_berita','tbl_berita.kode_berita','=','tbl_komentar.kode_berita')->where('tbl_berita.judul_berita_url',$url)->where('status_komentar','T')->get();
        
        // ke tanggal berita Baru
        $data['berita_sebelumnya']  = tbl_berita::where('tbl_berita.status_publikasi','T')->whereRaw('tbl_berita.tanggal_publikasi > "'.$data['detail']->tanggal_publikasi.'"')->orderBy('tbl_berita.tanggal_publikasi','ASC')->first();
        
        // ke tanggal berita lama
        $data['berita_selanjutnya'] = tbl_berita::where('tbl_berita.status_publikasi','T')->whereRaw('tbl_berita.tanggal_publikasi < "'.$data['detail']->tanggal_publikasi.'"')->orderBy('tbl_berita.tanggal_publikasi','DESC')->first();
        
        $data['berita_label']       = tbl_berita_label::join('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')->where('tbl_berita_label.kode_berita',$data['detail']->kode_berita)->get();
        
        foreach ($data['berita_label'] as $row) {
            $list_label[] = $row->kode_label;
        }
        if(!empty($list_label)){
            $data['berita_lainnya'] = tbl_berita::join('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')->where('tbl_berita.status_publikasi','T')->whereRaw('tbl_berita.kode_berita != '.$data['detail']->kode_berita)->whereIn('tbl_berita_label.kode_label', $list_label)->groupBy('tbl_berita_label.kode_berita')->orderBy('tbl_berita.tanggal_publikasi', 'DESC')->limit(4)->get();
        }

        $awal  = date_create($data['detail']->tanggal_publikasi);
        $akhir = date_create(); // waktu sekarang
        $diff  = date_diff( $awal, $akhir );
        $date  = "";
        
        if ($diff->y != "0") {
            $date .= $diff->y . ' tahun ';
        }
        if ($diff->m != "0") {
            $date .= $diff->m . ' bulan ';
        }
        if ($diff->d != "0") {
            $date .= $diff->d . ' hari ';
        }
        if ($diff->h != "0") {
            $date .= $diff->h . ' jam ';
        }
        if ($diff->i != "0") {
            $date .= $diff->i . ' menit ';
        }
        // if ($diff->s != "0") {
        //     $date .= $diff->s . ' detik, ';
        // }  
        $data['selisih_waktu_publikasi'] = 'Publikasi: '. $date .' yang lalu ';

        $data['messages'] = $this->session->flashdata('messages');

        $this->load_view_frontend("frontend","news","news","v_news_view",$data);
    }

    function save()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
           
            $id           = $this->input->post('kode_berita');
            $kode_berita  = decryptID($id);
            
            /*  Set URL */
            $berita_url   = tbl_berita::where('kode_berita',$kode_berita)->first();
            
            $url          = 'news-detail/' . $berita_url->judul_berita_url;
            
            /* Post Data */
            $komentar     = ucfirst($this->input->post('komentar'));
            $nama         = ucfirst($this->input->post('nama'));
            $email        = $this->input->post('email');

            $tbl_komentar = tbl_komentar::where('isi_komentar',$komentar)->where('nama',$nama)->where('email',$email)->first();
           
            if(empty($tbl_komentar))
            {
                $model                   = new tbl_komentar;
                
                $model->kode_berita      = $kode_berita;
                $model->isi_komentar     = $komentar;
                $model->nama             = $nama;
                $model->email            = $email;
                $model->tanggal_komentar = date('Y-m-d H:i:s');

                $save = $model->save();

                if($save)
                {
                    $this->session->set_flashdata('messages', 'Komentar Berhasil Dikirim');
                    redirect(site_url() . $url);
                } else {
                    $this->session->set_flashdata('messages', 'Komentar Gagal Dikirim');
                    redirect(site_url() . $url);
                }
            } else {
                $this->session->set_flashdata('messages', 'Anda Sudah Memberi Komentar Untuk Berita Ini');
                redirect(site_url() . $url);
            }
        }
    }
    
    function instant_article_facebook_rss(){
        header("Content-Type: application/xml; charset=utf-8");

        $berita = tbl_berita::leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                            ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                            ->leftJoin('tbl_gambar_berita','tbl_gambar_berita.kode_berita','=','tbl_berita.kode_berita')
                            ->where('tbl_berita.status_publikasi', 'T')
                            ->groupBy('tbl_gambar_berita.kode_berita')
                            ->orderby('tbl_berita.tanggal_publikasi','DESC')
                            ->get();

        $list_berita = "";
        foreach($berita as $vberita) 
        { 
            $list_berita .= '

            <!-- Another Article (Each Article must be represented as an <item>)  -->
            <item>
              <title>'. htmlspecialchars($vberita->judul_berita, ENT_QUOTES, "UTF-8") .'</title>
              <link>'. base_url().'news-detail/'. htmlspecialchars($vberita->judul_berita_url, ENT_QUOTES, "UTF-8") .'</link>
              <pubDate>'.date("c", strtotime($vberita->tanggal_publikasi)).'</pubDate>
              <content:encoded>
                <![CDATA[
                <!doctype html>
                <html>
                <head>
                    <link rel="canonical" href="'. base_url().'news-detail/'. htmlspecialchars($vberita->judul_berita_url, ENT_QUOTES, "UTF-8") .'"/>
                    <meta charset="utf-8"/>
                    <meta property="op:generator" content="facebook-instant-articles-sdk-php"/>
                    <meta property="op:generator:version" content="1.5.0"/>
                    <meta property="op:generator:application" content="facebook-instant-articles-wp"/>
                    <meta property="op:generator:application:version" content="3.2.2"/>
                    <meta property="op:generator:transformer" content="facebook-instant-articles-sdk-php"/>
                    <meta property="op:generator:transformer:version" content="1.5.0"/>
                    <meta property="op:markup_version" content="v1.0"/>
                    <meta property="fb:article_style" content="WahanaHonda"/>
                </head>
                <body>
                    <article>
                        <header>
                            <figure>
                                <img src="'. base_url().'assets/upload/berita/'. htmlspecialchars($vberita->gambar_berita, ENT_QUOTES, "UTF-8") .'"/>
                            </figure>
                            <h1>'. htmlspecialchars($vberita->judul_berita, ENT_QUOTES, "UTF-8") .'</h1>
                            <time class="op-published" datetime="'.date("c", strtotime($vberita->tanggal_publikasi)).'">'.date("c", strtotime($vberita->tanggal_publikasi)).'</time>
                            <time class="op-modified" datetime="'.date("c", strtotime($vberita->tanggal_publikasi)).'">'.date("c", strtotime($vberita->tanggal_publikasi)).'</time>
                            <address><a>Wahana Honda</a></address>
                            <h3 class="op-kicker">Berita</h3>
                        </header>
                        <figure>
                            <img src="'. base_url().'assets/upload/berita/'. htmlspecialchars($vberita->gambar_berita, ENT_QUOTES, "UTF-8") .'"/>
                        </figure>
                        '. $vberita->isi_berita .'
                    </article>
                </body>
                </html>
                ]]>
              </content:encoded>
            </item> ';
        }
        
        $data = '
            <rss version="2.0"
            xmlns:content="http://purl.org/rss/1.0/modules/content/">
              <channel>
                <title>News Wahana Honda</title>
                <link>https://wahanahonda.com/news</link>
                <description>
                  Read our awesome news, every day.
                </description>
                <language>en-us</language>
                <lastBuildDate>'.date("c").'</lastBuildDate>

                '. $list_berita .'
                
              </channel>
            </rss>
        ';
        
        echo $data;
    }
    
    // function instant_article_facebook_rss(){
    //     header("Content-Type: application/xml; charset=utf-8");
    //     echo '
    //         <rss version="2.0"
    //         xmlns:content="http://purl.org/rss/1.0/modules/content/">
    //           <channel>
    //             <title>News Publisher</title>
    //             <link>https://wahanahonda.com/rss</link>
    //             <description>
    //               Read our awesome news, every day.
    //             </description>
    //             <language>en-us</language>
    //             <lastBuildDate>'.date("c").'</lastBuildDate>
                
    //             <!-- Another Article (Each Article must be represented as an <item>)  -->
    //             <item>
    //               <title>Instant Article</title>
    //               <link>https://wahanahonda.com/rss</link>
    //               <pubDate>'.date("c").'</pubDate>
    //               <content:encoded>
    //                 <![CDATA[
    //                 <!doctype html><html><head><link rel="canonical" href="https://wahanahonda.com/news-detail/rayakan-idul-adha-wahana-tebar-hewan-kurban"/><meta charset="utf-8"/><meta property="op:generator" content="facebook-instant-articles-sdk-php"/><meta property="op:generator:version" content="1.5.0"/><meta property="op:generator:application" content="facebook-instant-articles-wp"/><meta property="op:generator:application:version" content="3.2.2"/><meta property="op:generator:transformer" content="facebook-instant-articles-sdk-php"/><meta property="op:generator:transformer:version" content="1.5.0"/><meta property="op:markup_version" content="v1.0"/><meta property="fb:article_style" content="WahanaHonda"/></head><body><article><header><figure><img src="https://wahanahonda.com/assets/upload/berita/BERITA_1535363491_3ad8aab353be90be23d4cb5d5470b77c.jpg"/></figure><h1>Rayakan Idul Adha, Wahana ‘Tebar’ Hewan Kurban</h1><time class="op-published" datetime="2016-12-08T13:50:46+07:00">December 8th, 1:50pm</time><time class="op-modified" datetime="2016-12-08T13:50:46+07:00">December 8th, 1:50pm</time><address><a>Randy Nurfadly</a></address><h3 class="op-kicker">Berita</h3></header><figure><img src="https://wahanahonda.com/assets/upload/berita/BERITA_1535363491_c0762b064315fcc234a8fbe714a45072.jpg"/></figure><p>Jakarta, 24 Agustus 2018 – Sambut perayaan hari Idul Adha 1439 H, Wahana Artha Group lewat Yayasan Wahana Artha (YWA) Grup berikan hewan kurban diberbagai wilayah Jakarta dan Tangerang. Tidak hanya tahun ini, Setiap tahun agenda “Wahana Tebar Hewan Kurban” ini rutin berjalan di berbagai cabang ritel penjualan Wahanaartha Ritelindo (WARI).</p><p>Puluhan hewan kurban yang terdiri dari 29 kambing dan 1 ekor sapi di salurkan ke 25 lokasi di Jakarta dan Tangerang. Penyaluran hewan kurban dilakukan sehari sebelum perayaan Idul Adha pada Rabu (22/08) lalu. Pemberian sengaja dilakukan pada masyarakat yang dekat dengan lokasi cabang Wahana termasuk dua lokasi gudang yaitu Distribution Center Jatake Tangerang dan Distribution Wahana Cimanggis Depok.</p><p>“ Kami ingin ikut merayakan salah satu perayaan besar kaum muslim khususnya masyarakat yang hidup dekat dengan Wahana. Pemberian juga jadi satu bentuk apresiasi terhadap masyarakat yang selama ini dukung perkembangan Wahana, “ Papar Ronnal Prawira, Ketua Yayasan Wahana Artha Group.</p><p>Yayasan Wahana Artha Group merupakan sebuah yayasan yang berdiri pada tepat perayaan HUT Wahan Artha Group pada 2011 silam fokus di tiga bidang, pendidikan, kesehatan dan lingkungan. Tidak hanya berbisnis, YWA menjadi bagian penting yang aktif berikan manfaat untuk masyarakat yang membutuhkan. Yayasan Wahana Artha kini menaungi 53 SMK di Jakarta Tangerang sebagai SMK Binaan Honda dan miliki sarana kesehatan yang untuk masayarakat yang kurang mampu dalam dapatkan fasilitas kesehatan yang layak yaitu Rumah Sehat yang berada di Jatake Tangerang.</p><p>“ Yayasan ini digunakan sebagai sarana yang dapat menolong dan berikan manfaat untuk masyarakat sekitar khususnya yang memiliki keterbatasan secara ekonomi,” tutup Ronnal.&nbsp;</p></article></body></html>
    //                 ]]>
    //               </content:encoded>
    //             </item>
                
    //             <item>
    //               <title>This is another Instant Article</title>
    //               <link>https://wahanahonda.com/rss</link>
    //               <pubDate>'.date("c").'</pubDate>
    //               <content:encoded>
    //                 <![CDATA[
    //                 <!doctype html><html><head><link rel="canonical" href="https://wahanahonda.com/news-detail/lampu-karya-smk-binaan-wahana-terangi-pengungsi-lombok"/><meta charset="utf-8"/><meta property="op:generator" content="facebook-instant-articles-sdk-php"/><meta property="op:generator:version" content="1.5.0"/><meta property="op:generator:application" content="facebook-instant-articles-wp"/><meta property="op:generator:application:version" content="3.2.2"/><meta property="op:generator:transformer" content="facebook-instant-articles-sdk-php"/><meta property="op:generator:transformer:version" content="1.5.0"/><meta property="op:markup_version" content="v1.0"/><meta property="fb:article_style" content="WahanaHonda"/></head><body><article><header><figure><img src="https://wahanahonda.com/assets/upload/berita/BERITA_1535363832_c743cff25e63640d123cbcdcb9abc9c1.jpg"/></figure><h1>Lampu Karya SMK Binaan Wahana ‘Terangi’ Pengungsi Lombok</h1><time class="op-published" datetime="2016-12-08T13:50:46+07:00">December 8th, 1:50pm</time><time class="op-modified" datetime="2016-12-08T13:50:46+07:00">December 8th, 1:50pm</time><address><a>Randy Nurfadly</a></address><h3 class="op-kicker">Berita</h3></header><figure><img src="https://wahanahonda.com/assets/upload/berita/BERITA_1535363832_8488e418d8eae52856c31cf78bdd909d.jpg"/></figure><p>Jakarta, 24 Agustus 2018 – Duka Lombok belum juga berakhir, beberapa gempa susulan masih terus terjadi hingga kini dan jumlah pengungsi yang terus bertambah memerlukan banyak bantuan. Yayasan Wahana Artha Group (YWA) berikan langsung bantuan 500 lampu penerangan hasil karya salah satu SMK binaan Wahana ke tiga lokasi bencana di kecamatan Gunung Sari, Lombok Barat, Kamis (23/08).</p><p>Distribusi yang berjalan sejak 23 hingga 24 Agustus dilakukan langsung oleh Yayasan Wahana Artha kepada pengungsi. Desa yang mendapatkan bantuan tersebut merupakan lokasi yang terdampak besar gempa seperti desa Lendang Bajur, Balekuwu, dan desa Medas. Selain pangan dan pakaian, sarana penerangan juga jadi masalah para pengungsi di malam hari, maka bantuan lampu yang dibuat dari hasil olah limbah bengkel Astra Honda Authorized Service Station (AHASS) Jaringan Main Dealer sepeda motor Honda Jakarta Tangerang, PT. Wahana Makmur Sejati (WMS), Buana Smart Light dipilih sebagai bantuan yang tepat.</p><p>“ Gempa susulan yang kerap terjadi di Lombok ini rusak beragam fasilitas penting masyarakat salah satunya suplai listrik. Untuk berikan kemudahan beraktivitas pengungsi dimalam hari kami distibusikan 500 unit lampu sollar sell,” papar Ketua Umum Yayasan Wahana Artha, Ronnal Prawira. Dia menambahkan sollar sel ini merupakan hasil karya kreatif SMK Binaan Wahana, SMK Jaya Buana yang berhasil ciptakan lampu dengan bahan limbah AHASS. Karya mereka juga berhasil dapatkan juara 2 dalam ajang Kontes Astra Honda Best Student 2018 (AHMBS) yang digelar awal Agustus ini.</p><p>YWA juga libatkan tim komunitas Honda CRF Mataram&nbsp; untuk bantu pendistribusian bantuan wilayah yang sulit di tempuh akibat dampak gempa. Tidak hanya lampu, Wahana juga berikan bantuan tunai melalui&nbsp; kantor perwakilan Honda Sales Operation (HSO) Mataram senilai Rp. 250 Juta rupiah. Bantuan yang di terima langsung oleh Region Head HSO Mataram, Wahyudi Saputra diharapakan dapat tersalurkan untuk korban bencana secara tepat&nbsp; karena mengetahui beragam kondisi korban di lokasi.</p><p>Ketua Yayasan Wahana Artha yang ikut langsung berikan bantuan kepada&nbsp; masyarakat setempat dapatkan respon poitif terhadap bantuan ini. “ Karena listrik sulit, bantuan lampu sollar sell ini juga dapat digunakan sebagai sarana untuk charge HP, dan bisa dijadikan alat hiburan untuk pengungsi,” jelas Ronnal. Dia juga berhap duka Lombok dapat segera selesai dan masyarakat dapat&nbsp; melanjutkan kehidupan normal.</p></article></body></html>
    //                 ]]>
    //               </content:encoded>
    //             </item>
                
    //           </channel>
    //         </rss>
    //     ';
    // }
}