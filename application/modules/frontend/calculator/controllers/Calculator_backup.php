<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Calculator extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        $this->load->model('select_global_model');
    }

    function index($url) 
    {
        $data['detail']          = ms_produk::join('ms_harga_otr','ms_harga_otr.kode_produk','=','ms_produk.kode_produk')->join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')->where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.nama_produk_url', $url)->first();
        
        // Select Dropdown
        $data['kategori_motor']  = $this->select_global_model->selectKategoriMotor();
        $data['produk']          = '- Pilih Produk -';
        $data['dp']              = '- Pilih DP -';
        $data['tenor']           = $this->select_global_model->selectTenor();
        
        $data['getDP']           = site_url() . $this->site . '/ajaxGetDP';
        $data['getVarianProduk'] = site_url() . $this->site . '/ajaxGetVarianProduk';
        $data['getHargaOtr']     = site_url() . $this->site . '/ajaxGetHargaOtr';
        $data['getHargaCicilan'] = site_url() . $this->site . '/ajaxGetHargaCicilan';
        
        $data['action']          = site_url() . $this->site . '/save';
        $data['name']            = "";
        $data['message_success']   = $this->session->flashdata('message_success');
        $data['message_error']     = $this->session->flashdata('messages_error');
        
        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);

        $this->load_view_frontend("frontend","calculator","calculator","v_calculator",$data);
    }

    function ajaxGetVarianProduk()
    {
        $kategori_motor = $this->input->get('kategori_motor');
        $produk         = $this->select_global_model->selectVarianProduk($kategori_motor);
        
        
        $result         = form_dropdown('produk', $produk, '', 'class="form-control m-input"');
        echo json_encode($result);
    }

    function ajaxGetHargaOtr()
    {
        $id             = $this->input->get('kode_harga_otr');
        $kode_harga_otr = decryptID($id);
        
        $harga_otr      = ms_harga_otr::where('kode_harga_otr',$kode_harga_otr)->first();

        $result         = 'Rp. ' . number_format($harga_otr->harga_produk_otr,2,',','.'); 
        echo json_encode($result);
    }

    function ajaxGetDP()
    {
        $id             = $this->input->get('kode_harga_otr');
        $kode_harga_otr = decryptID($id);
        
        $dp             = $this->select_global_model->selectDPProduk($kode_harga_otr);
        
        $result         = form_dropdown('dp', $dp, '', 'class="form-control m-input"');
        echo json_encode($result);
       
    }

    function ajaxGetHargaCicilan()
    {
        $dp    = $this->input->get('dp');
        $tenor = $this->input->get('tenor');

        if($tenor == '11') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_11')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_11,2,',','.');
        } elseif($tenor == '17') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_17')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_17,2,',','.');
        } elseif($tenor == '23') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_23')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_23,2,',','.');
        } elseif($tenor == '27') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_27')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_27,2,',','.');
        } elseif($tenor == '29') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_29')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_29,2,',','.');  
        } elseif($tenor == '33') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_33')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_33,2,',','.');  
        } else {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_35')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_35,2,',','.');
        }

        echo json_encode($result);
    }

    function save()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            
            /* Post Pemesanan */
            $kategori_motor    = $this->input->post('kategori_motor');
            $produk            = decryptID($this->input->post('produk')); //produk dari table ms_harga_otr
            $uang_muka         = $this->input->post('dp');
            $tenor             = $this->input->post('tenor');
            $estimasi_cicilan  = $this->input->post('angsuran');

            /* Remove String From View */
            $find              = array('Rp',',00','.');
            $cicilan           = str_replace($find,'',$estimasi_cicilan);
          
            /* Post Pelanggan */
            $nama              = $this->input->post('nama_lengkap');
            $email             = $this->input->post('email');
            $nomor_hp          = $this->input->post('nomor_hp');

            /* Get URL Produk */
            $ms_produk  = ms_produk::selectRaw('nama_produk_url')
                                    ->join('ms_harga_otr','ms_harga_otr.kode_produk','=','ms_produk.kode_produk')
                                    ->where('ms_harga_otr.kode_harga_otr',$produk)
                                    ->first();

            $url        = site_url() . 'calculator/' . $ms_produk->nama_produk_url;

            
            /* Begin Save Table tbl_pelanggan */
            $tbl_pelanggan                          = new tbl_pelanggan;
            
            $tbl_pelanggan->nama_pelanggan          = ucwords($nama);
            $tbl_pelanggan->email_pelanggan         = strtolower($email);
            $tbl_pelanggan->nomor_telepon_pelanggan = $nomor_hp;
            
            $save_pelanggan                         = $tbl_pelanggan->save();
            /* End Save Table tbl_pelanggan */

            /*  Begin Save Table tbl_pemesanan */
            $tbl_pemesanan                            = new tbl_pemesanan;
            
            $date                                     = date('Ym');
            $prefix                                   = array('length' => 4, 'prefix' => 'ORD' . $date , 'position' => 'right');
            $kode                                     = $this->generate_id_model->code_prefix('tbl_pemesanan', 'id_pemesanan', $prefix);
            
            $tbl_pemesanan->kode_pemesanan            = $kode;
            $tbl_pemesanan->kode_harga_otr            = $produk;
            $tbl_pemesanan->kode_pelanggan            = tbl_pelanggan::max('kode_pelanggan');
            $tbl_pemesanan->jangka_pinjaman           = $tenor;
            $tbl_pemesanan->uang_muka                 = $uang_muka;
            $tbl_pemesanan->estimasi_cicilan_perbulan = $cicilan;
            $tbl_pemesanan->tanggal_pemesanan         = date('Y-m-d H:i:S');

            $save_pemesanan                           = $tbl_pemesanan->save();
            /*  End Save Table tbl_pemesanan */

            if($save_pelanggan && $save_pemesanan)
            {
                $this->session->set_flashdata('message_success', 'Pemesanan Berhasil Dikirim');
                redirect($url);
            } else 
            {
                $this->session->set_flashdata('message_error', 'Pemesanan Gagal Dikirim');
                redirect($url);
            }  
        }
    }
}