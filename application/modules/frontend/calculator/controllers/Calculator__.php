<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calculator extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        $this->load->model('select_global_model');
    }

    function index($url) 
    {
        $data['detail']            = ms_produk::join('ms_harga_otr','ms_harga_otr.kode_produk','=','ms_produk.kode_produk')->join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')->where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.nama_produk_url', $url)->first();
        
        // Select Dropdown
        $data['kategori_motor']    = $this->select_global_model->selectKategoriMotor();
        $data['lokasi_testride']   = $this->select_global_model->selectLokasiTestride();
        $data['tenor']             = $this->select_global_model->selectTenor();
        $data['produk']            = '- Pilih Produk -';
        $data['dp']                = '- Pilih DP -';
        $data['testride']          = '- Pilih Tipe Motor -';
        
        $data['getDP']             = site_url() . $this->site . '/ajaxGetDP';
        $data['getVarianProduk']   = site_url() . $this->site . '/ajaxGetVarianProduk';
        $data['getHargaOtr']       = site_url() . $this->site . '/ajaxGetHargaOtr';
        $data['getHargaCicilan']   = site_url() . $this->site . '/ajaxGetHargaCicilan';
        $data['getAjaxTestRide']   = site_url() . $this->site . '/ajaxGetTestRide';
        $data['getVerifikasiKode'] = site_url() . $this->site . '/ajaxGetVerifikasiKode';
        
        $data['action']            = site_url() . $this->site . '/save';
        $data['action_modal']      = site_url() . $this->site . '/save_modal';

        $data['name']              = "";
        $data['message_success']   = $this->session->flashdata('message_success');
        $data['message_error']     = $this->session->flashdata('messages_error');
        
        /* For Search */
        $where                          = "tbl_berita.status_publikasi = 'T'";
        $count_all                      = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);

        $this->load_view_frontend("frontend","calculator","calculator","v_calculator",$data);
    }

    function ajaxGetVerifikasiKode()
    {
        $no_hp           = $this->input->post('no_hp');
      
        $string          = '0123456789';
        $string_shuffled = str_shuffle($string);
        $password        = substr($string_shuffled, 1, 6);
        $pesan           = 'Wahana Honda Testride: Angka verifikasi Anda adalah: '.$password;

        $curl = curl_init();
      
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain?&output=json',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'user'     => 'wahanaartha_api',
                'password' => 'Wahana321+',
                'SMSText'  => $pesan,
                'GSM'      => $no_hp
            )
        ));

        $resp = curl_exec($curl);
        
        $data = array('resp' => $resp,'password' => $password);

        if (!$resp) 
        {
            die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        } 
        else 
        {
            echo json_encode($data);
        }
    }
    // 
    
    // function ajaxGetVerifikasiKode() 
    // {   

    //     $no_hp = $this->security->xss_clean($this->input->post('no_hp'));

    //     $string          = '0123456789';
    //     $string_shuffled = str_shuffle($string);
    //     $password        = substr($string_shuffled, 1, 6);
    //     $pesan           = 'Nomor Verifikasi "'.$password.'"';

    //     if(substr(trim($no_hp), 0, 1) == '0')
    //     {
    //         $nohp = '62' . substr(trim($no_hp),1);
    //     }
    
    //     $curl = curl_init();
    //     curl_setopt_array($curl, array(
    //         CURLOPT_RETURNTRANSFER => 1,
    //         CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
    //         CURLOPT_POST => true,
    //         CURLOPT_POSTFIELDS => array(
    //             'user' => 'wahanaartha_api',
    //             'password' => 'Wahana321+',
    //             'SMSText' => $pesan,
    //             'GSM' => $nohp
    //         )
    //     ));

    //     $resp = curl_exec($curl);
    //     if (!$resp) {
    //         die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
    //     } else {
    //         header('Content-type: text/xml'); /*if you want to output to be an xml*/
    //         echo $resp;
    //     }
    //     curl_close($curl);
    // }

    function ajaxGetTestRide()
    {
        $lokasi_testride = $this->input->get('lokasi');
        $lokasi          = decryptID($lokasi_testride);

        $testride        = $this->select_global_model->selectKendaraanTestride($lokasi);
        $result          = form_dropdown('kendaraan_testride', $testride, '', 'class="form-control"');

        echo json_encode($result);
    }

    function ajaxGetVarianProduk()
    {
        $kategori_motor = $this->input->get('kategori_motor');
        $produk         = $this->select_global_model->selectVarianProduk($kategori_motor);
        
        
        $result         = form_dropdown('produk', $produk, '', 'class="form-control m-input"');
        echo json_encode($result);
    }

    function ajaxGetHargaOtr()
    {
        $id             = $this->input->get('kode_harga_otr');
        $kode_harga_otr = decryptID($id);
        
        $harga_otr      = ms_harga_otr::where('kode_harga_otr',$kode_harga_otr)->first();

        $result         = 'Rp. ' . number_format($harga_otr->harga_produk_otr,2,',','.'); 
        echo json_encode($result);
    }

    function ajaxGetDP()
    {
        $id             = $this->input->get('kode_harga_otr');
        $kode_harga_otr = decryptID($id);
        
        $dp             = $this->select_global_model->selectDPProduk($kode_harga_otr);
        
        $result         = form_dropdown('dp', $dp, '', 'class="form-control m-input"');
        echo json_encode($result);
       
    }

    function ajaxGetHargaCicilan()
    {
        $dp    = $this->input->get('dp');
        $tenor = $this->input->get('tenor');

        if($tenor == '11') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_11')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_11,2,',','.');
        } elseif($tenor == '17') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_17')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_17,2,',','.');
        } elseif($tenor == '23') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_23')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_23,2,',','.');
        } elseif($tenor == '27') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_27')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_27,2,',','.');
        } elseif($tenor == '29') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_29')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_29,2,',','.');  
        } elseif($tenor == '33') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_33')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_33,2,',','.');  
        } else {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_35')->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_35,2,',','.');
        }

        echo json_encode($result);
    }

    /*
    *  Function Save to table: tbl_pelanggan & tbl_pemesanan
    * 
    */
    function save()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            
            /* Post Pemesanan */
            $kategori_motor    = $this->input->post('kategori_motor');
            $produk            = decryptID($this->input->post('produk')); //produk dari table ms_harga_otr
            $uang_muka         = $this->input->post('dp');
            $tenor             = $this->input->post('tenor');
            $estimasi_cicilan  = $this->input->post('angsuran');

            /* Remove String From View */
            $find              = array('Rp',',00','.');
            $cicilan           = str_replace($find,'',$estimasi_cicilan);
          
            /* Post Pelanggan */
            $nama              = $this->input->post('nama_lengkap');
            $no_ktp            = $this->input->post('no_ktp');
            $nomor_hp          = $this->input->post('nomor_hp');
            $email             = $this->input->post('email');
            $facebook_acc      = $this->input->post('facebook_acc');
            $instagram_acc     = $this->input->post('instagram_acc');
            $twitter_acc       = $this->input->post('twitter_acc');

            /* Set Post File */  
            $userfile           = $_FILES["userfile"]["name"];
            $userfile_size      = $_FILES["userfile"]["size"];
            $userfile_tmp_name  = $_FILES["userfile"]["tmp_name"];

            /* Get URL Produk */
            $ms_produk  = ms_produk::selectRaw('nama_produk_url')
                                    ->join('ms_harga_otr','ms_harga_otr.kode_produk','=','ms_produk.kode_produk')
                                    ->where('ms_harga_otr.kode_harga_otr',$produk)
                                    ->first();

            $url        = site_url() . 'calculator/' . $ms_produk->nama_produk_url;

            if(!empty($userfile))
            {
                // Set File Name, Extension File, File size, and Allowed Type
                $filename            = $userfile;
                $file_basename       = substr($filename, 0, strripos($filename, '.')); // get file extention
                $file_ext            = substr($filename, strripos($filename, '.')); // get file name extension
                $filesize            = $userfile_size; //get size
                $allowed_file_types  = array('.png','.jpg','.jpeg');
                
                if(in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 2000000)
                {
                    $file_foto       = 'CUST_' . $no_ktp . '_' . $nomor_hp . $file_ext;

                    if(file_exists("assets/upload/pelanggan/" . $file_foto))
                    {
                        $this->session->set_flashdata('message_exist', 'Foto Sudah Ada !!!');
                        redirect($url);
                    }
                    else
                    {
                        if(move_uploaded_file($userfile_tmp_name,  "assets/upload/pelanggan/" . $file_foto))
                        {
                            /* Begin Save Table tbl_pelanggan */
                            $tbl_pelanggan                          = new tbl_pelanggan;
                            
                            $tbl_pelanggan->no_ktp_pelanggan        = $no_ktp;
                            $tbl_pelanggan->nama_pelanggan          = ucwords($nama);
                            $tbl_pelanggan->no_hp_pelanggan         = $nomor_hp;
                            $tbl_pelanggan->email_pelanggan         = strtolower($email);
                            $tbl_pelanggan->instagram_acc           = strtolower($instagram_acc);
                            $tbl_pelanggan->twitter_acc             = strtolower($twitter_acc);
                            $tbl_pelanggan->facebook_acc            = $facebook_acc;
                            $tbl_pelanggan->foto_identitas          = $file_foto;
                            
                            $save_pelanggan                         = $tbl_pelanggan->save();
                            /* End Save Table tbl_pelanggan */

                            /*  Begin Save Table tbl_pemesanan */
                            $tbl_pemesanan                            = new tbl_pemesanan;

                            $date                                     = date('Ym');
                            $prefix                                   = array('length' => 4, 'prefix' => 'ORD' . $date , 'position' => 'right');
                            $kode                                     = $this->generate_id_model->code_prefix('tbl_pemesanan', 'kode_pemesanan', $prefix);
                            
                            $tbl_pemesanan->kode_pemesanan            = $kode;
                            $tbl_pemesanan->kode_harga_otr            = $produk;
                            $tbl_pemesanan->kode_pelanggan            = tbl_pelanggan::max('kode_pelanggan');
                            $tbl_pemesanan->jangka_pinjaman           = $tenor;
                            $tbl_pemesanan->uang_muka                 = $uang_muka;
                            $tbl_pemesanan->estimasi_cicilan_perbulan = $cicilan;
                            $tbl_pemesanan->tanggal_pemesanan         = date('Y-m-d H:i:S');

                            $save_pemesanan                           = $tbl_pemesanan->save();
                            /*  End Save Table tbl_pemesanan */

                            if($save_pelanggan && $save_pemesanan)
                            {
                                $this->session->set_flashdata('message_success', 'Pemesanan Berhasil Dikirim');
                                // $this->session->unset_userdata('confirm_Code');
                                redirect($url);
                            }
                            else
                            {
                                $this->session->set_flashdata('message_error', 'Pemesanan Gagal Dikirim');
                                redirect($url);
                            }
                        }
                    }
                }
            }
            else
            {
                $this->session->set_flashdata('message_error', 'Pemesanan Gagal Dikirim');
                redirect($url); 
            }
        }
    }

   /**
    * Function Save To Table tbl_pendaftar_testride
    *
    **/
    function save_modal()
    {   
        if ($_SERVER['REQUEST_METHOD'] == "POST") 
        {
            /* Set Post Data */
            $lokasi_testride    = decryptID($this->input->post('lokasi_testride'));
            $kendaraan_testride = decryptID($this->input->post('kendaraan_testride'));
            $nama_testrider     = $this->input->post('nama_testrider');
            $email_testrider    = $this->input->post('email_testrider');
            $no_telepon         = $this->input->post('no_telepon');
            $nama_produk_url    = decryptID($this->input->post('nama_produk_url'));

            /* URL */
            $url                = site_url() . 'calculator/' . $nama_produk_url;

            /* Check Into Database If Email and Phone Number are Exist */
            $cek_tbl            = tbl_pendaftar_testride::where('email_testrider', $email_testrider)->where('kendaraan_testride',$kendaraan_testride)->first(); 
 
            if(empty($cek_tbl))
            {
                /* Begin Save Data Into Table tbl_pendaftar_testride */
                $tbl_pendaftar_testride                       = new tbl_pendaftar_testride;
                
                $tbl_pendaftar_testride->lokasi_testride      = $lokasi_testride;
                $tbl_pendaftar_testride->kendaraan_testride   = $kendaraan_testride;
                $tbl_pendaftar_testride->nama_testrider       = $nama_testrider;
                $tbl_pendaftar_testride->email_testrider      = $email_testrider;
                $tbl_pendaftar_testride->no_telepon_testrider = $no_telepon;
                $tbl_pendaftar_testride->tgl_pendaftaran      = date('Y-m-d H:i:s');

                $save = $tbl_pendaftar_testride->save();

                if($save)
                {
                    $this->session->set_flashdata('message_success', 'Pendaftaran Test Ride Berhasil');
                    redirect($url);
                }
                else
                {
                    $this->session->set_flashdata('message_error', 'Gagal Mendaftar Testride');
                    redirect($url);
                }
            } 
            else
            {
                $this->session->set_flashdata('message_exist', 'Email dan Nomor Telepon Sudah Digunakan !!!');
                redirect($url);
            }
        }
    }
}