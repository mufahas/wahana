@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Wahana Honda | Calculator</title>
<script src='https://www.google.com/recaptcha/api.js'></script>
<style type="text/css"> body{ background: #DEDFE0 !important; } </style>
@endsection

@section('body')
    <div id="for_container">
        <div class="container-fluid bg-grey-2">
            <div class="container">
                <nav aria-label="breadcrumb" class="fs-12">
                  <ol class="breadcrumb pb-2 pt-2">
                    <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Calculator Cicilan</li>
                  </ol>
                </nav>
            </div>
        </div>

        <div class="container">
          {!! form_open($action_modal, array('id' => 'form-modal', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="">
                    <h5 class="modal-title" id="exampleModalLabel">
                      Wahana Honda Test Ride
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">
                        &times;
                      </span>
                    </button>
                  </div>
                  <div class="modal-body">
                      <div class="form-group">
                        <label>
                          * Lokasi Test Ride
                        </label>
                        {!! form_dropdown('lokasi_testride', $lokasi_testride, '', 'class="form-control m-input" id="lokasi_testride" style="width: 100%"') !!}
                      </div>

                      <div class="form-group">
                        <label>
                          * Tipe Kendaraan:
                        </label>
                        {!! form_dropdown('kendaraan_testride', $testride, '', 'class="form-control m-input" id="lokasi_testride" style="width: 100%"') !!}
                      </div>

                      <div class="form-group">
                        <label>* Nama </label>
                        {!! form_input(array('type' => 'text','name' => 'nama_testrider', 'class' => 'form-control m-input', 'placeholder' => 'Enter Your Name')) !!}
                      </div>

                      <div class="form-group">
                        <label>* Email </label>
                        {!! form_input(array('type' => 'text','name' => 'email_testrider', 'class' => 'form-control m-input', 'placeholder' => 'Enter Your Email Address')) !!}
                      </div>

                      <div class="form-group">
                        <label>* No Telepon </label>
                        {!! form_input(array('type' => 'text','name' => 'no_telepon', 'class' => 'form-control m-input numbers', 'placeholder' => 'Enter Your Phone Number', 'onkeypress' => 'return isNumber(event)', 'maxlength' => '13')) !!}
                      </div>
                  </div>
                  {!! form_input(array('type' => 'hidden','name' => 'nama_produk_url', 'class' => 'form-control m-input numbers', 'value' => encryptID($detail->nama_produk_url))) !!}
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-round" data-dismiss="modal">
                      Close
                    </button>
                    <button type="submit" class="btn btn-danger btn-round save_modal">
                      Send message
                    </button>
                  </div>
                </div>
              </div>
              </div>
              {!! form_close() !!}

              <div class="row">
                <div class="col-md-12">
                  @if(!empty($message_success))
                    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show success" role="alert">
                        <div class="m-alert__icon">
                            <i class="la la-warning"></i>
                        </div>
                        <div class="m-alert__text">
                            <strong>
                            {{$message_success}}
                            </strong>
                        </div>
                        <div class="m-alert__close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        </div>
                    </div>
                  @endif
                  @if(!empty($message_error))
                  <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-error alert-dismissible fade show error" role="alert">
                      <div class="m-alert__icon">
                          <i class="la la-warning"></i>
                      </div>
                      <div class="m-alert__text">
                          <strong>
                          {{$message_error}}
                          </strong>
                      </div>
                      <div class="m-alert__close">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                      </div>
                  </div>
                  @endif
                  @if(!empty($message_exist))
                  <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-warning alert-dismissible fade show exist" role="alert">
                      <div class="m-alert__icon">
                          <i class="la la-warning"></i>
                      </div>
                      <div class="m-alert__text">
                          <strong>
                          {{$message_exist}}
                          </strong>
                      </div>
                      <div class="m-alert__close">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                      </div>
                  </div>
                  @endif
                </div>
              </div>

              {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form', 'enctype' => 'multipart/form-data')) !!}
              <div class="row">
                  <div class="col-md-6 mb-3">
                    <div class="bg-white pl-4 pr-4 pb-4 pt-4 mb-3">
                      <h3 class="text-18"><strong>CALCULATOR CICILAN</strong></h3>
                      <p>
                          Nikmati fitur calculator cicilan untuk mendapatkan simulasi cicilan motor kesayangan Brosis, lengkapi data diri agar team kami dapat menghubungi anda
                      </p>
                      <h3 h3 class="text-18"><strong>Simulasi calculator cicilan</strong></h3>

                      <div class="form-group">
                          <label>Pilih type motor</label>
                          {!! form_dropdown('kategori_motor', $kategori_motor, '', 'class="form-control m-input" id="m_kategori_motor" style="width: 100%"') !!}
                      </div>

                      <div class="form-group">
                          <label>Pilih motor</label>
                          {!! form_dropdown('produk', $produk, '', 'class="form-control m-input" id="produk" style="width: 100%"') !!}
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 mb-3">
                    <div class="bg-dark-grey text-white pr-4 pl-4 pt-2 pb-2">
                      <div class="row">
                          <div class="col-sm-6">
                              Harga OTR
                          </div>
                          <div class="col-sm-6 text-right">
                              {!! form_input(array('type' => 'text','name' => 'harga_otr', 'class' => 'form-control m-input text-right', 'placeholder' => '', 'disabled' => 'true')) !!}
                          </div>
                      </div>
                    </div>

                    <div class="bg-grey-2 pr-4 pl-4 pt-2 pb-2">
                      Update terakhir 01 Maret 2019
                    </div>

                    <div class="bg-white pl-4 pr-4 pb-4 pt-4 mb-3">
                      <div class="row">
                          <div class="col-md-8">
                              <div class="form-group">
                                  <label>Pilih DP *Down Payment</label>
                                    {!! form_dropdown('dp', $dp, '', 'class="form-control m-input text-right" id="dp" style="width: 100%"') !!}
                              </div>
                          </div>
                          <div class="col-md-4">
                              <label>Jangka Waktu</label>
                              <div class="row">
                                  <div class="col-6 pr-0">
                                    {!! form_dropdown('tenor', $tenor, '', 'class="form-control m-input text-right" id="tenor" style="width: 100%"') !!}
                                  </div>
                                  <div class="col-6 align-self-center">
                                      Bulan
                                  </div>
                              </div>
                          </div>
                      </div>

                      Total angsuran per bulan
                      <p class="text-red text-18">{!! form_input(array('type' => 'text','name' => 'angsuran', 'class' => 'form-control m-input text-right', 'placeholder' => '', 'readonly' => 'true')) !!}</p>
                      <p class="mb-0">
                          *Simulasi diatas hanya untuk gambaran jumlah angsuran, hubungi team marketing kami untuk mendapatkan harga fix.
                      </p>
                    </div>
                  </div>

                  <div class="ml-auto mr-auto">
                      <div class="row">
                          <div class="col-sm-6 mb-3">
                              <button type="button" class="btn btn-danger btn-round btn-block pesan_motor">Lakukan Pemesanan</button>
                          </div>
                          <div class="col-sm-6 mb-3">
                            <!-- <a href="http://wahanahondatestride.com/" target="_blank"> -->
                              <button type="button" class="btn btn-danger btn-round btn-block" href="" data-toggle="modal" data-target="#m_modal_4">Saya mau Test Ride</button>
                            <!-- </a> -->
                          </div>
                      </div>
                  </div>

                  <div class="col-md-12 mb-3 identitas" style="display: none;">
                    <div class="bg-white pl-4 pr-4 pb-4 pt-4 mb-3">
                      <h3 class="text-18"><strong>Identitas diri</strong></h3>
                      <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>* Nama: </label>
                                <input type="text" class="form-control" name="nama_lengkap" value="">
                            </div>
                            
                            <div class="form-group">
                              <label>
                                * No Telephone:
                              </label>
                              <div class="input-group">
                                <input type="text" class="form-control" name="nomor_hp" maxlength="14" placeholder="08xxxxxxxxxx" onkeypress="return isNumber(event)">
                                <span class="input-group-btn">
                                  <button class="btn btn-secondary button-verifikasi" type="button" id="button-verifikasi" disabled>
                                    Verifikasi No. HP
                                  </button>
                                </span>
                              </div>
                            </div>
                            
                            <div class="form-group">
                              <input type="hidden" name="confirm_code" class="form-control">
                            </div>

                            <div class="form-group nomor_verifikasi" style="display:none;">
                              <label>
                                Angka Verifikasi
                              </label>
                              <div class="input-group">
                                <input type="text" class="form-control" name="nomor_verifikasi" maxlength="6" placeholder="123456" onkeypress="return isNumber(event)" autocomplete="off">
                                <span class="input-group-btn">
                                  <button class="btn btn-secondary resend" type="button" id="resend" disabled>
                                    Resend
                                  </button>
                                  &nbsp;<span id="timer"></span>
                                </span>
                              </div>
                            </div>

                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show sent-message-success" role="alert" style="display: none;">
                                <i class="fa fa-envelope-o"></i> Kode Verifikasi Dikirim 
                            </div>
                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show sent-message-fail" role="alert" style="display: none;">
                                <i class="fa fa-remove"></i> Kode Verifikasi Gagal Dikirim !
                            </div>
                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show status-message-wrong" role="alert" style="display: none;">
                                <i class="fa fa-remove"></i> Kode Verifikasi Salah ! 
                            </div>
                            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show status-message-right" role="alert" style="display: none;">
                                <i class="fa fa-check"></i> Kode Verifikasi Benar 
                            </div>
                            <!-- Set Input Kode Verifikasi -->
                            
                          <!--<div class="form-group">-->
                          <!--    <label>* Upload Foto Identitas Diri</label>-->
                          <!--    <div class="upload">-->
                          <!--      <input type="file" name="userfile" class="form-control"> -->
                          <!--    </div>-->
                          <!--</div>-->
                        </div>

                        <div class="col-md-6">
                            <!--<div class="form-group">-->
                            <!--    <label>Facebook: </label>-->
                            <!--    <input type="text" class="form-control" name="facebook_acc" value="" placeholder="">-->
                            <!--</div>-->

                            <!--<div class="form-group">-->
                            <!--    <label>Instagram: </label>-->
                            <!--    <input type="text" class="form-control" name="instagram_acc" value="" placeholder="">-->
                            <!--</div>-->

                            <!--<div class="form-group">-->
                            <!--    <label>Twitter: </label>-->
                            <!--    <input type="text" class="form-control" name="twitter_acc" value="" placeholder="">-->
                            <!--</div>-->
                            
                            <!--<div class="form-group">-->
                            <!--  <label>* No KTP: </label>-->
                            <!--  <input type="text" class="form-control" name="no_ktp" value="" placeholder="317xxxxxxxxxxxxx" maxlength="16" onkeypress="return isNumber(event)">-->
                            <!--</div>-->
                            <div class="form-group">
                                <label>* Alamat Email: </label>
                                <input type="text" class="form-control" name="email" value="" placeholder="email@email.com">
                            </div>
                            
                            <div class="form-group">
                                <label>* Domisili: </label>
                                <textarea name="domisili" class="form-control"></textarea>  
                            </div>


                            
                        </div>
                      </div>


                        <div class="g-recaptcha pb-2" data-sitekey="6Lcb8VcUAAAAAD94jt6KFVp-rgY_VXTx55lv0y2R" align="center">
                        </div>
                          
                          <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" align="center" required>
                          
                          <div class="form-group">
                            </div>

                          <p class="text-14 text-center">
                          Dengan pesan motor sekarang anda setuju dengan <a href="#" class="btn-trans-red">Syarat </a> dan <a href="#" class="btn-trans-red">ketentuan </a> yang berlaku
                          </p>

                          <div class="submit">
                            <div class="row">
                                <div class="col-sm-3 mb-2 ml-auto mr-auto ">
                                    <button type="submit" class="btn btn-danger btn-round btn-block save">Pesan motor sekarang</button>
                                </div>
                            </div>
                          </div>
                    </div>
                  </div>


              </div>
            {!! form_close() !!}
        </div>


        <div class="container-fluid bg-grey pt-4 pb-4">
            <div class="container head-related bg-white">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link nav-related-link active" id="recommend-tab" data-toggle="tab" href="#recommend" role="tab" aria-controls="recommend" aria-selected="false">REKOMENDASI PRODUK</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link nav-related-link" id="aksesoris-relate-tab" data-toggle="tab" href="#aksesoris-relate" role="tab" aria-controls="aksesoris-relate" aria-selected="false">AKSESORIS TERKAIT</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link nav-related-link" id="aparel-tab" data-toggle="tab" href="#aparel" role="tab" aria-controls="aparel" aria-selected="false">APAREL TERKAIT</a>
                  </li>
                </ul>
            </div>
            <div class="container bg-white body-related">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="recommend" role="tabpanel" aria-labelledby="recommend-tab">
                       <div class="row">
                           <div class="col-md-3 col-sm-6 mb-3">
                               <img src="{{ base_url() }}assets/frontend/img/menu-prod-sh-150i-2-384x242.png" class="img-fluid" alt="">
                               <div class="text-center">
                                   <p class="mb-0">Honda Sh 150i</p>
                                   <p class="text-red">Rp. 39.900.000</p>
                                   <a href="#" class="btn btn-flat-red btn-block">Lihat Produk</a>
                               </div>
                           </div>
                           <div class="col-md-3 col-sm-6 mb-3">
                               <img src="{{ base_url() }}assets/frontend/img/ico-produkvario-150-matte-black-384x242.png" class="img-fluid" alt="">
                               <div class="text-center">
                                   <p class="mb-0">Honda Vario 150</p>
                                   <p class="text-red">Rp. 21.175.000</p>
                                   <a href="#" class="btn btn-flat-red btn-block">Lihat Produk</a>
                               </div>
                           </div>
                           <div class="col-md-3 col-sm-6 mb-3">
                               <img src="{{ base_url() }}assets/frontend/img/ico-produk-beat-street-white-384x242.png" class="img-fluid" alt="">
                               <div class="text-center">
                                   <p class="mb-0">Honda BeAT Street</p>
                                   <p class="text-red">Rp. 15.175.000</p>
                                   <a href="#" class="btn btn-flat-red btn-block">Lihat Produk</a>
                               </div>
                           </div>
                           <div class="col-md-3 col-sm-6 mb-3">
                               <img src="{{ base_url() }}assets/frontend/img/motor-vario-esp-menu-baru-2017-384x242.png" class="img-fluid" alt="">
                               <div class="text-center">
                                   <p class="mb-0">Honda Vario Esp</p>
                                   <p class="text-red">Rp. 16.675.000</p>
                                   <a href="#" class="btn btn-flat-red btn-block">Lihat Produk</a>
                               </div>
                           </div>
                       </div>
                    </div>
                    <div class="tab-pane fade" id="aksesoris-relate" role="tabpanel" aria-labelledby="aksesoris-relate-tab">
                       Recommend
                    </div>
                    <div class="tab-pane fade" id="aparel" role="tabpanel" aria-labelledby="aparel-tab">
                       aparel
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <h2 class="text-white">Berita WMS</h2>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab">
                <div id="contentAll" class="row row-eq-height"></div>
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')

    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/kalkulator-cicilan/kalkulator-cicilan.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/validate/jquery.validate.js" type="text/javascript"></script>
    <script type="text/javascript">

      document.getElementById("timer").innerHTML="60"; 


      var getVarianProduk   = "{{$getVarianProduk}}";
      var getOtr            = "{{$getHargaOtr}}";
      var getHargaCicilan   = "{{$getHargaCicilan}}";
      var getDP             = "{{$getDP}}";
      var getAjaxTestRide   = "{{$getAjaxTestRide}}";
      var getVerifikasiKode = "{{$getVerifikasiKode}}";
      // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);

                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}news-detail/'+v.judul_berita_url+'">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' +
                                                                            'Kategori Berita: '+ kategori_berita +
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {

            listNewsAjax.init();

            /* Search For Dekstop */
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';

                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */

            /* Search For Mobile */
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';

                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */
        });
    </script>
@stop