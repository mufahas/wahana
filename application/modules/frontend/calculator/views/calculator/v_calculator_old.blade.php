@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Wahana Honda | Calculator</title>
<script src='https://www.google.com/recaptcha/api.js'></script>
<style type="text/css"> body{ background: #DEDFE0 !important; } </style>
@endsection

@section('body')
    <div id="for_container">
        <nav aria-label="breadcrumb" class="breadcrumb-custom">
          <div class="container-fluid">
              <div class="container">
                    <ol class="breadcrumb mb-0 pb-1 pt-1">
                      <li class="breadcrumb-item"><a href="{{ base_url() }}">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Calculator Cicilan</li>
                    </ol>
              </div>
          </div>
        </nav>
    
        <div class="container pt-5">
            {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form', 'enctype' => 'multipart/form-data')) !!}
            <div class="row">
                <div class="col-md-6 mb-3">
                    <div class="bg-white pl-4 pr-4 pb-4 pt-4 mb-3">
                        <h3 class="text-18"><strong>CALCULATOR CICILAN</strong></h3>
                        <p>
                            Nikmati fitur calculator cicilan untuk mendapatkan simulasi cicilan motor kesayangan Brosis, lengkapi data diri agar team kami dapat menghubungi anda
                        </p>
                        <h3 h3 class="text-18"><strong>Simulasi calculator cicilan</strong></h3>
    
                            <div class="form-group">
                                <label>Pilih type motor</label>
                                {!! form_dropdown('kategori_motor', $kategori_motor, '', 'class="form-control m-input" id="m_kategori_motor" style="width: 100%"') !!}
                            </div>
    
                            <div class="form-group">
                                <label>Pilih motor</label>
                                {!! form_dropdown('produk', $produk, '', 'class="form-control m-input" id="produk" style="width: 100%"') !!}
                            </div>
    
                    </div>
                    <div class="bg-dark-grey text-white pr-4 pl-4 pt-2 pb-2">
                        <div class="row">
                            <div class="col-sm-6">
                                Harga OTR
                            </div>
                            <div class="col-sm-6 text-right">
                                {!! form_input(array('type' => 'text','name' => 'harga_otr', 'class' => 'form-control m-input text-right', 'placeholder' => '', 'disabled' => 'true')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="bg-grey-2 pr-4 pl-4 pt-2 pb-2">
                        Update terakhir 16 Juli 2018
                    </div>
                    <div class="bg-white pl-4 pr-4 pb-4 pt-4 mb-3">
                        <form class="" action="index.html" method="post">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Pilih DP *Down Payment</label>
                                          {!! form_dropdown('dp', $dp, '', 'class="form-control m-input text-right" id="dp" style="width: 100%"') !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Jangka Waktu</label>
                                    <div class="row">
                                        <div class="col-6 pr-0">
                                          {!! form_dropdown('tenor', $tenor, '', 'class="form-control m-input text-right" id="tenor" style="width: 100%"') !!}
                                        </div>
                                        <div class="col-6 align-self-center">
                                            Bulan
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            Total angsuran per bulan
                            <p class="text-red text-18">{!! form_input(array('type' => 'text','name' => 'angsuran', 'class' => 'form-control m-input text-right', 'placeholder' => '', 'readonly' => 'true')) !!}</p>
                            <p class="mb-0">
                                *Simulasi diatas hanya untuk gambaran jumlah angsuran, hubungi team marketing kami untuk mendapatkan harga fix.
                            </p>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="bg-white pl-4 pr-4 pb-4 pt-4 mb-3">
                        <h3 class="text-18"><strong>Identitas diri</strong></h3>
                        <div class="form-group">
                            <label>Nama lengkap sesuai KTP</label>
                            <input type="text" class="form-control" name="nama_lengkap" value="">
                        </div>
                        <div class="form-group">
                            <label>Alamat Email</label>
                            <input type="text" class="form-control" name="email" value="">
                        </div>
    
                        <div class="form-group">
                            <label>No. Handphone</label>
                            <input type="tel" class="form-control" name="nomor_hp" value="" maxlength="13" onkeypress="return isNumber(event)" pattern="[0-9]*">
                        </div>
                    </div>
                    <div class="g-recaptcha" data-sitekey="6Lcb8VcUAAAAAD94jt6KFVp-rgY_VXTx55lv0y2R"></div>
                                    <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" required>
                    <p class="text-14">
                        Dengan pesan motor sekarang anda setuju dengan <a href="#" class="btn-trans-red">Syarat </a> dan <a href="#" class="btn-trans-red">ketentuan </a> yang berlaku
                    </p>
    
                   @if(!empty($message_success)) 
                        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show success" role="alert">
                            <div class="m-alert__icon">
                                <i class="la la-warning"></i>
                            </div>
                            <div class="m-alert__text">
                                <strong> 
                                {{$message_success}}
                                </strong>
                            </div>
                            <div class="m-alert__close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                            </div>
                        </div>
                        @endif
                        @if(!empty($message_error))
                        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-error alert-dismissible fade show error" role="alert">
                            <div class="m-alert__icon">
                                <i class="la la-warning"></i>
                            </div>
                            <div class="m-alert__text">
                                <strong>
                                {{$message_error}}
                                </strong>
                            </div>
                            <div class="m-alert__close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                            </div>
                        </div>
                        @endif
                    <div class="submit">
                        <div class="row">
                            <div class="col-sm-6 mb-3">
                                <button type="submit" class="btn btn-danger btn-round btn-block save">Pesan motor sekarang</button>
                            </div>
                            <div class="col-sm-6 mb-3">
                              <a href="http://wahanahondatestride.com/" target="_blank">
                                  <button type="button" class="btn btn-danger btn-round btn-block" href="http://wahanahonda.com">Saya mau Test Ride</button>
                              </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! form_close() !!}
        </div>
    
    
        <div class="container-fluid bg-grey pt-4 pb-4">
            <div class="container head-related bg-white">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link nav-related-link active" id="recommend-tab" data-toggle="tab" href="#recommend" role="tab" aria-controls="recommend" aria-selected="false">REKOMENDASI PRODUK</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link nav-related-link" id="aksesoris-relate-tab" data-toggle="tab" href="#aksesoris-relate" role="tab" aria-controls="aksesoris-relate" aria-selected="false">AKSESORIS TERKAIT</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link nav-related-link" id="aparel-tab" data-toggle="tab" href="#aparel" role="tab" aria-controls="aparel" aria-selected="false">APAREL TERKAIT</a>
                  </li>
                </ul>
            </div>
            <div class="container bg-white body-related">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="recommend" role="tabpanel" aria-labelledby="recommend-tab">
                       <div class="row">
                           <div class="col-md-3 col-sm-6 mb-3">
                               <img src="{{ base_url() }}assets/frontend/img/menu-prod-sh-150i-2-384x242.png" class="img-fluid" alt="">
                               <div class="text-center">
                                   <p class="mb-0">Honda Sh 150i</p>
                                   <p class="text-red">Rp. 39.900.000</p>
                                   <a href="#" class="btn btn-flat-red btn-block">Lihat Produk</a>
                               </div>
                           </div>
                           <div class="col-md-3 col-sm-6 mb-3">
                               <img src="{{ base_url() }}assets/frontend/img/ico-produkvario-150-matte-black-384x242.png" class="img-fluid" alt="">
                               <div class="text-center">
                                   <p class="mb-0">Honda Vario 150</p>
                                   <p class="text-red">Rp. 21.175.000</p>
                                   <a href="#" class="btn btn-flat-red btn-block">Lihat Produk</a>
                               </div>
                           </div>
                           <div class="col-md-3 col-sm-6 mb-3">
                               <img src="{{ base_url() }}assets/frontend/img/ico-produk-beat-street-white-384x242.png" class="img-fluid" alt="">
                               <div class="text-center">
                                   <p class="mb-0">Honda BeAT Street</p>
                                   <p class="text-red">Rp. 15.175.000</p>
                                   <a href="#" class="btn btn-flat-red btn-block">Lihat Produk</a>
                               </div>
                           </div>
                           <div class="col-md-3 col-sm-6 mb-3">
                               <img src="{{ base_url() }}assets/frontend/img/motor-vario-esp-menu-baru-2017-384x242.png" class="img-fluid" alt="">
                               <div class="text-center">
                                   <p class="mb-0">Honda Vario Esp</p>
                                   <p class="text-red">Rp. 16.675.000</p>
                                   <a href="#" class="btn btn-flat-red btn-block">Lihat Produk</a>
                               </div>
                           </div>
                       </div>
                    </div>
                    <div class="tab-pane fade" id="aksesoris-relate" role="tabpanel" aria-labelledby="aksesoris-relate-tab">
                       Recommend
                    </div>
                    <div class="tab-pane fade" id="aparel" role="tabpanel" aria-labelledby="aparel-tab">
                       aparel
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <h2 class="text-white">Berita WMS</h2>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->
    
@stop

@section('scripts')

    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/kalkulator-cicilan/kalkulator-cicilan.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/validate/jquery.validate.js" type="text/javascript"></script>
    <script type="text/javascript">
      var getVarianProduk = "{{$getVarianProduk}}";
      var getOtr          = "{{$getHargaOtr}}";
      var getHargaCicilan = "{{$getHargaCicilan}}";
      var getDP           = "{{$getDP}}";
      
      // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        } 
                
        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });
    </script>
@stop