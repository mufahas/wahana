<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Apparel_produk extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    public function index($url = "")
    {   
        if($url == 'beli-apparel/'.$url) // Kondisi Jika URL yang dituju adalah pembelian part aksesoris
        { 
          $this->beli_apparel($url="");
        }
        else 
        {
          /* For Search */
          $where     = "tbl_berita.status_publikasi = 'T'";
          $count_all = tbl_berita::selectRaw("tbl_berita.kode_berita")
              ->leftJoin('tbl_berita_label', 'tbl_berita_label.kode_berita', '=', 'tbl_berita.kode_berita')
              ->leftJoin('ms_label', 'ms_label.kode_label', '=', 'tbl_berita_label.kode_label')
              ->whereRaw($where)
              ->groupBy('tbl_berita.kode_berita')
              ->get();
          $all = 0;
          foreach ($count_all as $value) {
              $all++;
          }
          $data['total_page_all'] = ceil($all / 12);
          /* For Search */

          /* Data Apparel */
          $select = 'ms_apparel.kode_apparel,
                     ms_apparel.nama_apparel,
                     ms_apparel.nama_apparel_url,
                     ms_apparel.jenis_apparel,
                     ms_apparel.gambar_apparel,
                     ms_apparel.harga_apparel 
                    ';

          $data['result'] = ms_apparel::selectRaw($select)
                            ->where('ms_apparel.nama_apparel_url',$url)
                            ->where('ms_apparel.dihapus','F')
                            ->where('ms_apparel.publikasi','T')
                            ->where('ms_apparel.harga_apparel','!=','')
                            ->get();
          /* Data Apparel */

          /* Get Produk Name */
          $data['produk'] = ms_produk::select('nama_produk')->where('nama_produk_url', $url)->first();
          /* Get Produk Name */

          $this->load_view_frontend("frontend", "apparel_produk", "apparel_produk", "v_apparel_produk", $data);
        }

    }

    public function beli_apparel($url="")
    {

      /* For Search */
          $where                          = "tbl_berita.status_publikasi = 'T'";
          $count_all                      = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                              ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                              ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                              ->whereRaw($where)
                                              ->groupBy('tbl_berita.kode_berita')
                                              ->get();
          $all = 0;
          foreach ($count_all as $value) {
              $all++;
          }
          $data['total_page_all'] = ceil($all / 12);
      /* For Search */

      /* Get Data Part Apparel Based On URL */
      $data['result']            = ms_apparel::where('nama_apparel_url',$url)->first();
      
      /* Button Action */
      $data['action']            = site_url() . $this->module . '/save';
      $data['getVerifikasiKode'] = site_url() . $this->class .  '/ajax_get_verifikasi_kode';
     
      $data['message_success']   = $this->session->flashdata('success');
      $data['message_error']     = $this->session->flashdata('error');

      $this->load_view_frontend("frontend", "apparel_produk", "apparel_produk", "v_beli_apparel", $data);
    }

    /* Save */
    function save()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            /* Post Pembelian Apparel */
            $kode_apparel               = decryptID($this->input->post('kode_apparel'));
            $no_ktp_pembeli             = $this->input->post('no_ktp');
            $nama_apparel_url           = $this->input->post('nama_apparel_url');
            $nama_pembeli               = $this->input->post('nama_pembeli');
            $no_hp                      = $this->input->post('nomor_hp');
            $email_pembeli              = $this->input->post('email');
            $domisili                   = $this->input->post('domisili');

            /* URL */
            $url                        = site_url() . 'apparel/beli-apparel/' . $nama_apparel_url;
           

            /* Initialize Data */            
            $pembeli                    = new tbl_pembeli_apparel;
            
            $pembeli->kode_apparel      = $kode_apparel;
            $pembeli->no_ktp_pembeli    = $no_ktp_pembeli;
            $pembeli->nama_pembeli      = ucfirst($nama_pembeli);
            $pembeli->no_hp_pembeli     = $no_hp;
            $pembeli->email_pembeli     = strtolower($email_pembeli);
            $pembeli->domisili          = $domisili;
            $pembeli->tgl_beli          = date('Y-m-d H:i:s');

            $save = $pembeli->save();

            if($save)
            {
                $this->session->set_flashdata('success', 'Pembelian Apparel Berhasil. Kami Akan Segera Menghubungi Anda. Terima Kasih !');
                redirect($url);
            }
            else
            {
                $this->session->set_flashdata('error', 'Pembelian Gagal !');
                redirect($url);
            }

        }
    }
    
    function ajax_get_verifikasi_kode()
    {
        $no_hp           = $this->input->post('no_hp');
      
        $string          = '0123456789';
        $string_shuffled = str_shuffle($string);
        $password        = substr($string_shuffled, 1, 6);
        $pesan           = 'Wahana Honda Testride: Angka verifikasi Anda adalah: '.$password;

        $curl = curl_init();
      
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_URL             => 'http://api.nusasms.com/api/v3/sendsms/plain?&output=json',
            CURLOPT_POST            => true,
            CURLOPT_POSTFIELDS      => array(
                                                'user'     => 'wahanaartha_api',
                                                'password' => 'Wahana321+',
                                                'SMSText'  => $pesan,
                                                'GSM'      => $no_hp
                                            )
        ));

        $resp = curl_exec($curl);
        
        $data = array('resp' => $resp,'password' => $password);

        if (!$resp) 
        {
            die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        } 
        else 
        {
            echo json_encode($data);
        }
    }
}
