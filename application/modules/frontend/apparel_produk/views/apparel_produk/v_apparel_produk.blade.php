@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Spare Part dan Aksesoris Motor Honda | Wahana Honda</title>
<meta name="description" content="Daftar spare parts & aksesoris sepeda motor honda dengan pilihan terlengkap. Tersedia beragam jenis onderdil resmi motor honda untuk kebutuhan bengkel Anda">
@endsection

@section('body')
    <div id="for_container">

        <nav aria-label="breadcrumb" class="breadcrumb-custom">
          <div class="container-fluid">
              <div class="container">
                    <ol class="breadcrumb mb-0 pb-1 pt-1">
                      <li class="breadcrumb-item"><a href="{{base_url()}}" title="Wahana">Beranda</a></li>
                      <li class="breadcrumb-item ">Aksesoris</li>
                      <li class="breadcrumb-item active" aria-current="page">{{ $produk->nama_produk }}</li>
                    </ol>
              </div>
          </div>
        </nav>
        <br>
        <div class="container-fluid honda-genue"></div>
    
        <div class="container mt-4">
            <div class="text-center">
                <h2 class="small-title">AKSESORIS &amp; APPAREL HONDA</h2>
                <hr class="separate-line" style="margin:auto;">
            </div>
            
            <div class="row">
                <?php 
                    if(!empty($result)){
                        foreach ($result as $key1) { ?>
                            <div class="col-md-4 mb-5" align="center">
                                <img src="{{ base_url() }}assets/upload/part_aksesoris/apparel/{{$key1->gambar_apparel}}" class="img-fluid" alt="{{$key1->nama_apparel}}" title="{{$key1->nama_apparel}}">
                                    <p class="mb-0">{{$key1->nama_apparel}}</p>
                                    <p class="text-red"> {{$key1->harga_apparel != 0 ? 'Rp. '.number_format($key1->harga_apparel,'0',',','.') : 'Rp. -'}}</p>
                                    <a class="btn btn-danger btn-round btn-block" target="_blank" href="{{ base_url() }}apparel/beli-apparel/{{ $key1->nama_apparel_url }}" title="Beli Produk"><i class="la-shopping-cart"></i>Beli Produk</a>
                            </div>  
                        <?php  } ?>
                <?php } ?>
            </div>
        </div>

        
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <h2 class="text-white">Berita WMS</h2>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')
    <script type="text/javascript">

        $('.slide-aksesoris').owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            items: 1,

        })
        
        // Set For Search Form
        var get_news          = "{{base_url()}}ajax_news_filter";
        var count_news_page   = "{{base_url()}}total_page_news";
        var totalPageAll      = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="All News">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });

    </script>
@stop