<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();
    }

    function index($url) 
    {   
         if($url == 'motor-cub') 
        {
            $this->list_bebek();
        } 
        else if($url == 'motor-matic')
        {

            $this->list_matic();
        }
        else if($url == 'motor-sport')
        {
            $this->list_sport();
        }
        else
        {
            $data['detail']            = ms_produk::where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.nama_produk_url', $url)->first();
            $data['part_file']         = ms_part_file::orderBy('ms_part_file.kode_part_file', 'ASC')->first();
    
            $data['header']            = tbl_konten_produk::join('ms_konten','ms_konten.kode_konten','=','tbl_konten_produk.kode_konten')->whereRaw('tbl_konten_produk.kode_produk ="'.$data['detail']->kode_produk.'" AND ms_konten.nama_konten = "Header"')->where('ms_konten.dihapus','F')->first();
            
            $data['varian_warna']      = tbl_konten_produk::join('ms_konten','ms_konten.kode_konten','=','tbl_konten_produk.kode_konten')->whereRaw('tbl_konten_produk.kode_produk ="'.$data['detail']->kode_produk.'" AND ms_konten.nama_konten = "Varian Warna"')->where('ms_konten.dihapus','F')->groupBy('tbl_konten_produk.kode_produk')->first();
            $data['varian_warna_list'] = tbl_konten_produk::join('ms_konten','ms_konten.kode_konten','=','tbl_konten_produk.kode_konten')->whereRaw('tbl_konten_produk.kode_produk ="'.$data['detail']->kode_produk.'" AND ms_konten.nama_konten = "Varian Warna"')->where('ms_konten.dihapus','F')->get();
            
            $data['fitur']             = tbl_konten_produk::join('ms_konten','ms_konten.kode_konten','=','tbl_konten_produk.kode_konten')->whereRaw('tbl_konten_produk.kode_produk ="'.$data['detail']->kode_produk.'" AND ms_konten.nama_konten = "Fitur Unggulan"')->where('ms_konten.dihapus','F')->first();
            $data['titik_fitur']       = ms_produk_fitur::where('ms_produk_fitur.kode_produk',$data['detail']->kode_produk)->get();
    
            $data['video']             = tbl_konten_produk::join('ms_konten','ms_konten.kode_konten','=','tbl_konten_produk.kode_konten')->whereRaw('tbl_konten_produk.kode_produk ="'.$data['detail']->kode_produk.'" AND ms_konten.nama_konten = "Video"')->where('ms_konten.dihapus','F')->first();
    
            $data['performa']          = tbl_konten_produk::join('ms_konten','ms_konten.kode_konten','=','tbl_konten_produk.kode_konten')->whereRaw('tbl_konten_produk.kode_produk ="'.$data['detail']->kode_produk.'" AND ms_konten.nama_konten = "Performa Andalan"')->where('ms_konten.dihapus','F')->first();
    
            $data['highlight']         = tbl_konten_produk::join('ms_konten','ms_konten.kode_konten','=','tbl_konten_produk.kode_konten')->whereRaw('tbl_konten_produk.kode_produk ="'.$data['detail']->kode_produk.'" AND ms_konten.nama_konten = "Produk Highlight"')->where('ms_konten.dihapus','F')->first();
    
            $data['harga_otr']         = ms_harga_otr::where('ms_harga_otr.kode_produk',$data['detail']->kode_produk)->get();
    
            $data['rekomendasi1']      = ms_rekomendasi_produk::selectRaw('ms_produk.gambar_produk,ms_produk.nama_produk, ms_produk.nama_produk_url, ms_produk.harga_produk')->join('ms_produk','ms_produk.kode_produk','=','ms_rekomendasi_produk.kode_rekomendasi_1')->where('ms_rekomendasi_produk.dihapus','F')->where('ms_rekomendasi_produk.kode_produk',$data['detail']->kode_produk)->first();
            $data['rekomendasi2']      = ms_rekomendasi_produk::selectRaw('ms_produk.gambar_produk,ms_produk.nama_produk, ms_produk.nama_produk_url, ms_produk.harga_produk')->join('ms_produk','ms_produk.kode_produk','=','ms_rekomendasi_produk.kode_rekomendasi_2')->where('ms_rekomendasi_produk.dihapus','F')->where('ms_rekomendasi_produk.kode_produk',$data['detail']->kode_produk)->first();
            $data['rekomendasi3']      = ms_rekomendasi_produk::selectRaw('ms_produk.gambar_produk,ms_produk.nama_produk, ms_produk.nama_produk_url, ms_produk.harga_produk')->join('ms_produk','ms_produk.kode_produk','=','ms_rekomendasi_produk.kode_rekomendasi_3')->where('ms_rekomendasi_produk.dihapus','F')->where('ms_rekomendasi_produk.kode_produk',$data['detail']->kode_produk)->first();
            $data['rekomendasi4']      = ms_rekomendasi_produk::selectRaw('ms_produk.gambar_produk,ms_produk.nama_produk, ms_produk.nama_produk_url, ms_produk.harga_produk')->join('ms_produk','ms_produk.kode_produk','=','ms_rekomendasi_produk.kode_rekomendasi_4')->where('ms_rekomendasi_produk.dihapus','F')->where('ms_rekomendasi_produk.kode_produk',$data['detail']->kode_produk)->first();
    
            $data['aksesoris']         = ms_part_aksesoris::selectRaw('ms_gambar_part_aksesoris.gambar_part_aksesoris, ms_part_aksesoris.nama_part_aksesoris, ms_part_aksesoris.nama_part_aksesoris_url, ms_part_aksesoris.harga_part_aksesoris')->join('ms_gambar_part_aksesoris','ms_gambar_part_aksesoris.id_part_aksesoris','=','ms_part_aksesoris.id_part_aksesoris')->leftJoin('ms_part_aksesoris_detail','ms_part_aksesoris_detail.id_part_aksesoris','=','ms_part_aksesoris.id_part_aksesoris')->where('ms_part_aksesoris.dihapus','F')->where('ms_part_aksesoris_detail.kode_produk',$data['detail']->kode_produk)->groupBy('ms_gambar_part_aksesoris.id_part_aksesoris')->get();
    
            $data['aparel']            = ms_apparel::leftJoin('ms_apparel_detail','ms_apparel_detail.kode_apparel','=','ms_apparel.kode_apparel')->where('ms_apparel.dihapus','F')->where('ms_apparel.publikasi','T')->where('ms_apparel_detail.kode_produk',$data['detail']->kode_produk)->get();
            
            /* Search Filter */
            $where                         = "tbl_berita.status_publikasi = 'T'";
            $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                             ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                             ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                             ->whereRaw($where)
                                             ->groupBy('tbl_berita.kode_berita')
                                             ->get();
            $all = 0;
            foreach ($count_all as $value) {
                $all++;
            }
            $data['total_page_all'] = ceil($all / 12);
            /* End Search Filter */
            
            $this->load_view_frontend("frontend","produk","produk","v_produk",$data);    
        }
    }
    
    /* ALL PRODUCT */
    function list_all()
    {   
        /* Search Filter */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        /* End Search Filter */
        
        $data['total_page_all'] = ceil($all / 12);
        $data['all_product']    = ms_produk::where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->get();

        $this->load_view_frontend("frontend","produk","produk","v_produk_all",$data);
    }

    /* Motor Matic */
    function list_matic()
    {   
        /* Search Filter */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        /* End Search Filter */
        
        $data['total_page_all'] = ceil($all / 12);
        $data['matic_product']    = ms_produk::where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.kode_kategori_motor','1')->get();
        
        $this->load_view_frontend("frontend","produk","produk","v_produk_matic",$data);
    }

    /* Motor Bebek */   
    function list_bebek()
    {   
        /* Search Filter */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        /* End Search Filter */
        
        $data['total_page_all'] = ceil($all / 12);
        $data['bebek_product']    = ms_produk::where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.kode_kategori_motor','2')->get();
        
        $this->load_view_frontend("frontend","produk","produk","v_produk_bebek",$data);
    }

    /* Motor Sport */
    function list_sport()
    {   
        /* Search Filter */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        /* End Search Filter */
        
        $data['total_page_all'] = ceil($all / 12);
        $data['sport_product']    = ms_produk::where('ms_produk.dihapus', 'F')->where('ms_produk.status_tampil', 'T')->where('ms_produk.kode_kategori_motor','3')->get();
        
        $this->load_view_frontend("frontend","produk","produk","v_produk_sport",$data);
    }

}