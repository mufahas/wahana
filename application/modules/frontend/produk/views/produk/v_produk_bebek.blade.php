@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Jenis dan Varian Motor Cub Honda | Wahana Honda</title>
<meta name="description" content="Cari sepeda motor cub merek Honda? Dapatkan informasi lengkap jenis dan varian sepeda motor bebek Honda terbaru yang sesuai dengan minat Anda disini!">
@endsection

@section('body')
    <nav aria-label="breadcrumb" class="breadcrumb-custom">
      <div class="container">
        <div class="container-fluid">
          <ol class="breadcrumb pl-0 mb-0 pb-1 pt-1">
            <li class="breadcrumb-item"><a href="{{base_url()}}" title="Wahana">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{base_url()}}produk" title="All Product">Motor</a></li>
            <li class="breadcrumb-item active" aria-current="page">Motor Cub</li>
          </ol>
        </div>
      </div>
    </nav>

    <div id="for_container" class="container pt-4">
        <div class="container mt-4 mb-5">
            
            <div class="text-center">
                <h1 class="header1-content mb-2">Produk Motor Cub Honda</h1>
                <hr class="separate-line m-auto">
            </div>

            <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link menu-list news-link" id="all-product-tab" data-toggle="tab" href="#all-product" role="tab" aria-controls="all-product" aria-selected="false" title="All Product">ALL</a>
              </li>
              <li class="nav-item">
                <a class="nav-link menu-list news-link active" id="cub-tab" data-toggle="tab" href="#cub" role="tab" aria-controls="cub" aria-selected="false" title="Cub">CUB</a>
              </li>
              <li class="nav-item">
                <a class="nav-link menu-list news-link" id="matik-tab" data-toggle="tab" href="#matik" role="tab" aria-controls="matik" aria-selected="false" title="Matic">MATIC</a>
              </li>
              <li class="nav-item">
                <a class="nav-link menu-list news-link" id="sport-tab" data-toggle="tab" href="#tab-sport" role="tab" aria-controls="tab-sport" aria-selected="false" title="Sport">SPORT</a>
              </li>
            </ul>
        </div>

        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="all-product" role="tabpanel" aria-labelledby="all-product-tab"> 
                <div id="contentAllProduct" class="row row-eq-height">                        
                    @if(!empty($bebek_product))
                        @foreach($bebek_product as $bebek_pro)
                        <div class="col-sm-4">
                            <a href="{{base_url()}}produk/{{$bebek_pro->nama_produk_url}}" title="{{$bebek_pro->nama_produk}}">
                                <img src="{{ base_url() }}assets/upload/produk/gambar/{{$bebek_pro->gambar_produk}}" class="img-fluid" alt="{{$bebek_pro->nama_produk}}" title="{{$bebek_pro->nama_produk}}">
                                <h2 class="header2-content bg-red text-center pt-2 pb-2 text-white">
                                    {{$bebek_pro->nama_produk}}
                                </h2>
                            </a>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->
@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script type="text/javascript">

        $('#all-product-tab').click(function() {
            location.href = '{{base_url()}}produk';
        }); 

        $('#cub-tab').click(function() {
            location.href = '{{base_url()}}produk/motor-cub';
        });

        $('#matik-tab').click(function() {
            location.href = '{{base_url()}}produk/motor-matic';
        });

        $('#sport-tab').click(function() {
            location.href = '{{base_url()}}produk/motor-sport';
        });

        
        $("#img-thumn a").on('click', function(e) {
            e.preventDefault();

            $('#preview img').attr('src', $(this).attr('href'));
        });

        $('.aksesoris').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            stageOuterClass: 'owl-stage-outer outer-stage',
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'nav-container',
            navClass: [ 'prev', 'next' ],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:6
                }
            }
        });

        /* Set For Search */
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

                var demo = function(searchData="", totalPagesAll="") {

                    if(totalPagesAll != "") {
                        var totalAllPage = totalPagesAll;
                    } else {
                        var totalAllPage = totalPageAll;
                    }

                    $('#paginationAll').twbsPagination({
                        totalPages: totalAllPage,
                        onPageClick: function(evt, page) {

                            $.ajax({
                                type: 'POST',
                                url: get_news,
                                data: {page: page, search: searchData},
                                dataType: 'json',
                                success: function(data) {

                                    $('#contentAll').html('');

                                    $.each(data.data, function(i, v) {

                                        // Set Tanggal Publikasi
                                        var date = new Date(v.tanggal_publikasi);
                                        
                                        // Set Judul Berita
                                        if ((v.judul_berita).length > 60) {
                                            var str   = v.judul_berita;
                                            var judul = str.substring(0,60)+"...";                          
                                        }else{
                                            var judul = v.judul_berita;
                                        }

                                        // Set Definisi Kategori Berita
                                        if(v.kategori_berita == 'B')
                                        {
                                            var kategori_berita = 'Berita';
                                        }
                                        else if(v.kategori_berita == 'E')
                                        {
                                            var kategori_berita = 'Event';
                                        }
                                        else
                                        {
                                            var kategori_berita = 'Tips';
                                        }

                                        /* Set Display Content */
                                        $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                    '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="'+judul+'">'+
                                                                        '<div class="row align-items-center news-mid-over p-2">'+
                                                                            '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                                '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                            '</div>'+
                                                                            '<div class="col-sm-12 col-7 news-over">'+
                                                                                '<div class="content-news content-news-over">'
                                                                                    +judul+ '</br>' + 
                                                                                'Kategori Berita: '+ kategori_berita + 
                                                                                '</div>'+
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</a>'+
                                                                '</div>');   
                                    });
                                }
                            });
                        }
                    });
                };

                return {
                    // public functions
                    init: function(searchData="", totalPagesAll="") {
                        demo(searchData, totalPagesAll);
                    },
                };
            }();

            jQuery(document).ready(function() {
                
                listNewsAjax.init();
                
                /* Search For Dekstop */ 
                $(document).on('click', '#button_search', function() {
                    var searchData = $('input[name="input_search"]').val();
                    $('#paginationAll').twbsPagination('destroy');

                    $('#for_container').hide();
                    $('#contentSearch').show();

                    $.ajax({
                        type: 'POST',
                        url: count_news_page,
                        data: {search: searchData},
                        dataType: 'json',
                        success: function(result) {
                            var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                            
                            listNewsAjax.init(searchData, totalPagesAll);
                        }
                    });
                });
                /* End Search For Dekstop */ 

                /* Search For Mobile */ 
                $(document).on('click', '#button_search_nav', function() {
                    var searchData = $('input[name="search_nav"]').val();
                    $('#paginationAll').twbsPagination('destroy');

                    $('#for_container').hide();
                    $('#contentSearch').show();

                    $.ajax({
                        type: 'POST',
                        url: count_news_page,
                        data: {search: searchData},
                        dataType: 'json',
                        success: function(result) {
                            var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                            
                            listNewsAjax.init(searchData, totalPagesAll);
                        }
                    });
                });
                /* End Search For Mobile */ 
            });
        /* End Set For Search */

    </script>
@stop