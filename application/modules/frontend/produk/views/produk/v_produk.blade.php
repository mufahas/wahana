@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Spesifikasi Motor {{ $detail->nama_produk }} | Wahana Honda</title>
<meta name="description" content="Informasi lengkap Motor {{ $detail->nama_produk }}. Cari tahu varian warna, fitur unggulan, spesifikasi dan harga cicilan motor {{ $detail->nama_produk }} hanya di sini">
@endsection

@section('body')
    <nav aria-label="breadcrumb" class="breadcrumb-custom">
      <div class="container">
        <div class="container-fluid">
          <ol class="breadcrumb pl-0 mb-0 pb-1 pt-1">
            <li class="breadcrumb-item"><a href="{{base_url()}}" title="Wahana">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{base_url()}}produk" title="Product">Motor</a></li>
            @if($detail->kode_kategori_motor == '1')
            <li class="breadcrumb-item"><a href="{{base_url()}}produk/motor-matic" title="Matic">Motor Matic</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $detail->nama_produk }}</li>
            @elseif($detail->kode_kategori_motor == '2')
            <li class="breadcrumb-item"><a href="{{base_url()}}produk/motor-cub" title="Cub">Motor Cub</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $detail->nama_produk }}</li>
            @elseif($detail->kode_kategori_motor == '3')
            <li class="breadcrumb-item"><a href="{{base_url()}}produk/motor-sport" title="Sport">Motor Sport</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $detail->nama_produk }}</li>
            @endif
          </ol>
        </div>
      </div>
    </nav>

    <div id = "for_container">
        <!-- Section 1 -->
        @if(!empty($header) AND $header->urutan_konten == "1")
           @if($detail->nama_produk_url == "honda-super-cub-c125")
                <div class="banner-home" align="center">
                    <a href="https://wahanahonda.com/calculator/honda-super-cub-c125" target="_blank" title="Product Details">
                        <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="Banner Product" title="Banner Product">
                    </a>
                </div>
            @else
                <div class="banner-home" align="center">
                    <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="Banner Product" title="Banner Product">
                </div>
            @endif
        @endif
    
        @if(!empty($varian_warna) AND $varian_warna->urutan_konten == "1")
        <div class="container mt-5 mb-5">
            <div class="text-center">
                <h2 class="header2-content font-weight-bold">VARIAN WARNA</h2>
                <hr class="separate-line m-auto">
            </div>
    
            <div class="row justify-content-md-center">
                <div class="col-sm-5">
                    <div class="owl-carousel owl-theme varian">
                        @foreach($varian_warna_list as $vvarian_warna_list)
                        <div class="item">
                            <img src="{{ base_url() }}assets/upload/produk/varian_warna/{{$vvarian_warna_list->kode_produk}}/{{$vvarian_warna_list->isi_konten}}" alt="{{ $detail->nama_produk }}" title="{{ $detail->nama_produk }}">
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($fitur) AND $fitur->urutan_konten == "1")
        <div class="container-fluid bg-red2 pt-5 pb-5">
            <div class="text-center pb-2">
                <h2 class="header2-content font-weight-bold text-white">FITUR UNGGULAN</h2>
                <hr class="separate-line line-white m-auto">
            </div>
    
            <!-- Display on Desktop -->
            <div class="row justify-content-md-center text-center">
                <div class="col-sm-12 image-map">
                    @foreach($titik_fitur as $vtitik_fitur)
                    <?php 
                        $vtitik_fitur_explode = explode(",", $vtitik_fitur->posisi); 
                        $titikX   = $vtitik_fitur_explode[1] * 1.5 / (870.56 * 1.545) * 100;
                        $titikY   = $vtitik_fitur_explode[0] * 1.5 / 1343 * 100;
                    ?>
                    <div class="animate-point" style="margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;">
                    </div>
                     <a href="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" data-lightbox="unggulan" data-title="<!-- <a href='#'><span class='share-ico fb'></span></a><a href='#'><span class='share-ico gp'></span></a><a href='#'><span class='share-ico tw'></span></a> -->" title="Fitur Product">
                        <div class="point" style="border-color: #f7f14c;background-color: #45453f; margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;" data-toggle="tooltip" data-placement="top" title="{{$vtitik_fitur->nama_fitur}}"></div>
                    </a>
                    @endforeach
                    <img src="{{ base_url() }}assets/upload/produk/fitur/{{$fitur->isi_konten}}" class="img-fluid" alt="Fitur Product" title="Fitur Product">
                </div>
            </div>
    
            <!-- Display on Mobile -->
              <div class="image-feature-res">
                  <div class="owl-carousel owl-theme feature-res">
                    @foreach($titik_fitur as $vtitik_fitur)
                      <div class="item">
                          <img src="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" alt="{{$vtitik_fitur->nama_fitur}}" title="{{$vtitik_fitur->nama_fitur}}">
                      </div>
                    @endforeach
                  </div>
              </div>
        </div>
        @endif
    
        @if(!empty($video) AND $video->urutan_konten == "1")
        <div class="mt-3 mb-3">
            <div class="justify-content-md-center">
                <div class="col-sm-12">
                    <?php $explode = explode('/watch?v=', $video->isi_konten); $kode = $explode[1]; ?>
                    <iframe id="myVideo" width="100%" height="738" src="{!!str_replace('/watch?v=', '/embed/', $video->isi_konten)!!}?vq=hd720&mute=1&autoplay=0&loop=1&playlist={{$kode}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($performa) AND $performa->urutan_konten == "1")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/performa/{{$performa->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
        </div>
        @endif
    
        @if(!empty($highlight) AND $highlight->urutan_konten == "1")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/highlight/{{$highlight->isi_konten}}" class="img-fluid" alt="Product Highlight" title="Product Highlight">
        </div>
        @endif
        <!-- END section 1 -->
    
    
    
        <nav class="navbar navbar-expand-md navbar-dark bg-red nav-line">
            <ul class="navbar-nav mx-auto nav-prod">
              <li class="nav-item">
                <a class="nav-link nav-broch-act" target="_blank" href="{{base_url()}}assets/upload/produk/brosur/{{$detail->brosur_produk}}" title="Download Brochure">DOWNLOAD BROCHURE</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-broch-act" target="_blank" href="{{ base_url()}}pemesanan-produk/{{$detail->nama_produk_url}}" title="Lakukan Pemesanan">LAKUKAN PEMESANAN</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-broch-act" href="{{ base_url() }}testride/{{$detail->nama_produk_url}}" title="Test Ride">TEST RIDE</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-broch-act " href="{{base_url()}}calculator/{{ $detail->nama_produk_url }}" title="Calculator Cicilan">CALCULATOR CICILAN</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-broch-act" target="_blank" href="{{base_url()}}assets/upload/part_aksesoris/part_file/{{$part_file->nama_part_file}}" title="Download Part">DOWNLOAD PART</a>
              </li>
            </ul>
        </nav>  
    
    
    
        <!-- Section 2 -->
        @if(!empty($header) AND $header->urutan_konten == "2")
            @if($detail->nama_produk_url == "honda-super-cub-c125")
                <div class="banner-home" align="center">
                    <a href="https://wahanahonda.com/calculator/honda-super-cub-c125" target="_blank" title="Banner">
                        <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
                    </a>
                </div>
            @else
                <div class="banner-home" align="center">
                    <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
                </div>
            @endif
        @endif
    
        @if(!empty($varian_warna) AND $varian_warna->urutan_konten == "2")
        <div class="container mt-5 mb-5">
            <div class="text-center">
                <h2 class="header2-content font-weight-bold">VARIAN WARNA</h2>
                <hr class="separate-line m-auto">
            </div>
    
            <div class="row justify-content-md-center">
                <div class="col-sm-5">
                    <div class="owl-carousel owl-theme varian">
                        @foreach($varian_warna_list as $vvarian_warna_list)
                        <div class="item">
                            <img src="{{ base_url() }}assets/upload/produk/varian_warna/{{$vvarian_warna_list->kode_produk}}/{{$vvarian_warna_list->isi_konten}}" alt="{{ $detail->nama_produk }}" title="{{ $detail->nama_produk }}">
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($fitur) AND $fitur->urutan_konten == "2")
        <div class="container-fluid bg-red2 pt-5 pb-5">
            <div class="text-center pb-2">
                <h2 class="header2-content font-weight-bold text-white">FITUR UNGGULAN</h2>
                <hr class="separate-line line-white m-auto">
            </div>
    
            <!-- Display on Desktop -->
            <div class="row justify-content-md-center text-center">
                <div class="col-sm-12 image-map">
                    @foreach($titik_fitur as $vtitik_fitur)
                    <?php 
                        $vtitik_fitur_explode = explode(",", $vtitik_fitur->posisi); 
                        $titikX   = $vtitik_fitur_explode[1] * 1.5 / (870.56 * 1.545) * 100;
                        $titikY   = $vtitik_fitur_explode[0] * 1.5 / 1343 * 100;
                    ?>
                    <div class="animate-point" style="margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;">
                    </div>
                     <a href="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" data-lightbox="unggulan" data-title="<!-- <a href='#'><span class='share-ico fb'></span></a><a href='#'><span class='share-ico gp'></span></a><a href='#'><span class='share-ico tw'></span></a> -->" title="Fitur Product">
                        <div class="point" style="border-color: #f7f14c;background-color: #45453f; margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;" data-toggle="tooltip" data-placement="top" title="{{$vtitik_fitur->nama_fitur}}"></div>
                    </a>
                    @endforeach
                    <img src="{{ base_url() }}assets/upload/produk/fitur/{{$fitur->isi_konten}}" class="img-fluid" alt="Fitur Product" title="Fitur Product">
                </div>
            </div>
    
            <!-- Display on Mobile -->
              <div class="image-feature-res">
                  <div class="owl-carousel owl-theme feature-res">
                    @foreach($titik_fitur as $vtitik_fitur)
                      <div class="item">
                          <img src="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" alt="{{$vtitik_fitur->nama_fitur}}" title="{{$vtitik_fitur->nama_fitur}}">
                      </div>
                    @endforeach
                  </div>
              </div>
        </div>
        @endif
    
        @if(!empty($video) AND $video->urutan_konten == "2")
        <div class="mt-3 mb-3">
            <div class="justify-content-md-center">
                <div class="col-sm-12">
                    <?php $explode = explode('/watch?v=', $video->isi_konten); $kode = $explode[1]; ?>
                    <iframe id="myVideo" width="100%" height="738" src="{!!str_replace('/watch?v=', '/embed/', $video->isi_konten)!!}?vq=hd720&mute=1&autoplay=0&loop=1&playlist={{$kode}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($performa) AND $performa->urutan_konten == "2")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/performa/{{$performa->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
        </div>
        @endif
    
        @if(!empty($highlight) AND $highlight->urutan_konten == "2")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/highlight/{{$highlight->isi_konten}}" class="img-fluid" alt="Product Highlight" title="Product Highlight">
        </div>
        @endif
        <!-- END section 2 -->
    
    
    
        <!-- Section 3 -->
        @if(!empty($header) AND $header->urutan_konten == "3")
            @if($detail->nama_produk_url == "honda-super-cub-c125")
                <div class="banner-home" align="center">
                    <a href="https://wahanahonda.com/calculator/honda-super-cub-c125" target="_blank" title="Banner">
                        <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
                    </a>
                </div>
            @else
                <div class="banner-home" align="center">
                    <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="banner" title="Banner">
                </div>
            @endif
        @endif
    
        @if(!empty($varian_warna) AND $varian_warna->urutan_konten == "3")
        <div class="container mt-5 mb-5">
            <div class="text-center">
                <h2 class="header2-content font-weight-bold">VARIAN WARNA</h2>
                <hr class="separate-line m-auto">
            </div>
    
            <div class="row justify-content-md-center">
                <div class="col-sm-5">
                    <div class="owl-carousel owl-theme varian">
                        @foreach($varian_warna_list as $vvarian_warna_list)
                        <div class="item">
                            <img src="{{ base_url() }}assets/upload/produk/varian_warna/{{$vvarian_warna_list->kode_produk}}/{{$vvarian_warna_list->isi_konten}}" alt="{{ $detail->nama_produk }}" title="{{ $detail->nama_produk }}">
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($fitur) AND $fitur->urutan_konten == "3")
        <div class="container-fluid bg-red2 pt-5 pb-5">
            <div class="text-center pb-2">
                <h2 class="header2-content font-weight-bold text-white">FITUR UNGGULAN</h2>
                <hr class="separate-line line-white m-auto">
            </div>
    
            <!-- Display on Desktop -->
            <div class="row justify-content-md-center text-center">
                <div class="col-sm-12 image-map">
                    @foreach($titik_fitur as $vtitik_fitur)
                    <?php 
                        $vtitik_fitur_explode = explode(",", $vtitik_fitur->posisi); 
                        $titikX   = $vtitik_fitur_explode[1] * 1.5 / (870.56 * 1.545) * 100;
                        $titikY   = $vtitik_fitur_explode[0] * 1.5 / 1343 * 100;
                    ?>
                    <div class="animate-point" style="margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;">
                    </div>
                     <a href="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" data-lightbox="unggulan" data-title="<!-- <a href='#'><span class='share-ico fb'></span></a><a href='#'><span class='share-ico gp'></span></a><a href='#'><span class='share-ico tw'></span></a> -->" title="Fitur Product">
                        <div class="point" style="border-color: #f7f14c;background-color: #45453f; margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;" data-toggle="tooltip" data-placement="top" title="{{$vtitik_fitur->nama_fitur}}"></div>
                    </a>
                    @endforeach
                    <img src="{{ base_url() }}assets/upload/produk/fitur/{{$fitur->isi_konten}}" class="img-fluid" alt="Fitur Product" title="Fitur Product">
                </div>
            </div>
    
            <!-- Display on Mobile -->
              <div class="image-feature-res">
                  <div class="owl-carousel owl-theme feature-res">
                    @foreach($titik_fitur as $vtitik_fitur)
                      <div class="item">
                          <img src="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" alt="{{$vtitik_fitur->nama_fitur}}" title="{{$vtitik_fitur->nama_fitur}}">
                      </div>
                    @endforeach
                  </div>
              </div>
        </div>
        @endif
    
        @if(!empty($video) AND $video->urutan_konten == "3")
        <div class="mt-3 mb-3">
            <div class="justify-content-md-center">
                <div class="col-sm-12">
                    <?php $explode = explode('/watch?v=', $video->isi_konten); $kode = $explode[1]; ?>
                    <iframe id="myVideo" width="100%" height="738" src="{!!str_replace('/watch?v=', '/embed/', $video->isi_konten)!!}?vq=hd720&mute=1&autoplay=0&loop=1&playlist={{$kode}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($performa) AND $performa->urutan_konten == "3")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/performa/{{$performa->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
        </div>
        @endif
    
        @if(!empty($highlight) AND $highlight->urutan_konten == "3")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/highlight/{{$highlight->isi_konten}}" class="img-fluid" alt="Product Highlight" title="Product Highlight">
        </div>
        @endif
        <!-- END section 3 -->
    
    
    
        <!-- Section 4 -->
        @if(!empty($header) AND $header->urutan_konten == "4")
            @if($detail->nama_produk_url == "honda-super-cub-c125")
                <div class="banner-home" align="center">
                    <a href="https://wahanahonda.com/calculator/honda-super-cub-c125" target="_blank" title="Banner">
                        <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
                    </a>
                </div>
            @else
                <div class="banner-home" align="center">
                    <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
                </div>
            @endif
        @endif
    
        @if(!empty($varian_warna) AND $varian_warna->urutan_konten == "4")
        <div class="container mt-5 mb-5">
            <div class="text-center">
                <h2 class="header2-content font-weight-bold">VARIAN WARNA</h2>
                <hr class="separate-line m-auto">
            </div>
    
            <div class="row justify-content-md-center">
                <div class="col-sm-5">
                    <div class="owl-carousel owl-theme varian">
                        @foreach($varian_warna_list as $vvarian_warna_list)
                        <div class="item">
                            <img src="{{ base_url() }}assets/upload/produk/varian_warna/{{$vvarian_warna_list->kode_produk}}/{{$vvarian_warna_list->isi_konten}}" alt="{{ $detail->nama_produk }}" title="{{ $detail->nama_produk }}">
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($fitur) AND $fitur->urutan_konten == "4")
        <div class="container-fluid bg-red2 pt-5 pb-5">
            <div class="text-center pb-2">
                <h2 class="header2-content font-weight-bold text-white">FITUR UNGGULAN</h2>
                <hr class="separate-line line-white m-auto">
            </div>
    
            <!-- Display on Desktop -->
            <div class="row justify-content-md-center text-center">
                <div class="col-sm-12 image-map">
                    @foreach($titik_fitur as $vtitik_fitur)
                    <?php 
                        $vtitik_fitur_explode = explode(",", $vtitik_fitur->posisi); 
                        $titikX   = $vtitik_fitur_explode[1] * 1.5 / (870.56 * 1.545) * 100;
                        $titikY   = $vtitik_fitur_explode[0] * 1.5 / 1343 * 100;
                    ?>
                    <div class="animate-point" style="margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;">
                    </div>
                     <a href="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" data-lightbox="unggulan" data-title="<!-- <a href='#'><span class='share-ico fb'></span></a><a href='#'><span class='share-ico gp'></span></a><a href='#'><span class='share-ico tw'></span></a> -->" title="Fitur Product">
                        <div class="point" style="border-color: #f7f14c;background-color: #45453f; margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;" data-toggle="tooltip" data-placement="top" title="{{$vtitik_fitur->nama_fitur}}"></div>
                    </a>
                    @endforeach
                    <img src="{{ base_url() }}assets/upload/produk/fitur/{{$fitur->isi_konten}}" class="img-fluid" alt="Fitur Product" title="Fitur Product">
                </div>
            </div>
    
            <!-- Display on Mobile -->
              <div class="image-feature-res">
                  <div class="owl-carousel owl-theme feature-res">
                    @foreach($titik_fitur as $vtitik_fitur)
                      <div class="item">
                          <img src="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" alt="{{$vtitik_fitur->nama_fitur}}" title="{{$vtitik_fitur->nama_fitur}}">
                      </div>
                    @endforeach
                  </div>
              </div>
        </div>
        @endif
    
        @if(!empty($video) AND $video->urutan_konten == "4")
        <div class="mt-3 mb-3">
            <div class="justify-content-md-center">
                <div class="col-sm-12">
                    <?php $explode = explode('/watch?v=', $video->isi_konten); $kode = $explode[1]; ?>
                    <iframe id="myVideo" width="100%" height="738" src="{!!str_replace('/watch?v=', '/embed/', $video->isi_konten)!!}?vq=hd720&mute=1&autoplay=0&loop=1&playlist={{$kode}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($performa) AND $performa->urutan_konten == "4")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/performa/{{$performa->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
        </div>
        @endif
    
        @if(!empty($highlight) AND $highlight->urutan_konten == "4")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/highlight/{{$highlight->isi_konten}}" class="img-fluid" alt="Product Highlight" title="Product Highlight">
        </div>
        @endif
        <!-- END section 4 -->
    
    
    
        <!-- Section 5 -->
        @if(!empty($header) AND $header->urutan_konten == "5")
            @if($detail->nama_produk_url == "honda-super-cub-c125")
                <div class="banner-home" align="center">
                    <a href="https://wahanahonda.com/calculator/honda-super-cub-c125" target="_blank" title="Banner">
                        <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
                    </a>
                </div>
            @else
                <div class="banner-home" align="center">
                    <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
                </div>
            @endif
        @endif
    
        @if(!empty($varian_warna) AND $varian_warna->urutan_konten == "5")
        <div class="container mt-5 mb-5">
            <div class="text-center">
                <h2 class="header2-content font-weight-bold">VARIAN WARNA</h2>
                <hr class="separate-line m-auto">
            </div>
    
            <div class="row justify-content-md-center">
                <div class="col-sm-5">
                    <div class="owl-carousel owl-theme varian">
                        @foreach($varian_warna_list as $vvarian_warna_list)
                        <div class="item">
                            <img src="{{ base_url() }}assets/upload/produk/varian_warna/{{$vvarian_warna_list->kode_produk}}/{{$vvarian_warna_list->isi_konten}}" alt="{{ $detail->nama_produk }}" title="{{ $detail->nama_produk }}">
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($fitur) AND $fitur->urutan_konten == "5")
        <div class="container-fluid bg-red2 pt-5 pb-5">
            <div class="text-center pb-2">
                <h2 class="header2-content font-weight-bold text-white">FITUR UNGGULAN</h2>
                <hr class="separate-line line-white m-auto">
            </div>
    
            <!-- Display on Desktop -->
            <div class="row justify-content-md-center text-center">
                <div class="col-sm-12 image-map">
                    @foreach($titik_fitur as $vtitik_fitur)
                    <?php 
                        $vtitik_fitur_explode = explode(",", $vtitik_fitur->posisi); 
                        $titikX   = $vtitik_fitur_explode[1] * 1.5 / (870.56 * 1.545) * 100;
                        $titikY   = $vtitik_fitur_explode[0] * 1.5 / 1343 * 100;
                    ?>
                    <div class="animate-point" style="margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;">
                    </div>
                     <a href="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" data-lightbox="unggulan" data-title="<!-- <a href='#'><span class='share-ico fb'></span></a><a href='#'><span class='share-ico gp'></span></a><a href='#'><span class='share-ico tw'></span></a> -->" title="Fitur Product">
                        <div class="point" style="border-color: #f7f14c;background-color: #45453f; margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;" data-toggle="tooltip" data-placement="top" title="{{$vtitik_fitur->nama_fitur}}"></div>
                    </a>
                    @endforeach
                    <img src="{{ base_url() }}assets/upload/produk/fitur/{{$fitur->isi_konten}}" class="img-fluid" alt="Fitur Product" title="Fitur Product">
                </div>
            </div>
    
            <!-- Display on Mobile -->
              <div class="image-feature-res">
                  <div class="owl-carousel owl-theme feature-res">
                    @foreach($titik_fitur as $vtitik_fitur)
                      <div class="item">
                          <img src="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" alt="{{$vtitik_fitur->nama_fitur}}" title="{{$vtitik_fitur->nama_fitur}}">
                      </div>
                    @endforeach
                  </div>
              </div>
        </div>
        @endif
    
        @if(!empty($video) AND $video->urutan_konten == "5")
        <div class="mt-3 mb-3">
            <div class="justify-content-md-center">
                <div class="col-sm-12">
                    <?php $explode = explode('/watch?v=', $video->isi_konten); $kode = $explode[1]; ?>
                    <iframe id="myVideo" width="100%" height="738" src="{!!str_replace('/watch?v=', '/embed/', $video->isi_konten)!!}?vq=hd720&mute=1&autoplay=0&loop=1&playlist={{$kode}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($performa) AND $performa->urutan_konten == "5")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/performa/{{$performa->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
        </div>
        @endif
    
        @if(!empty($highlight) AND $highlight->urutan_konten == "5")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/highlight/{{$highlight->isi_konten}}" class="img-fluid" alt="Product Highlight" title="Product Highlight">
        </div>
        @endif
        <!-- END section 5 -->
    
    
    
        <!-- Section 6 -->
        @if(!empty($header) AND $header->urutan_konten == "6")
            @if($detail->nama_produk_url == "honda-super-cub-c125")
                <div class="banner-home" align="center">
                    <a href="https://wahanahonda.com/calculator/honda-super-cub-c125" target="_blank" title="Banner">
                        <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
                    </a>
                </div>
            @else
                <div class="banner-home" align="center">
                    <img src="{{ base_url() }}assets/upload/produk/header/{{$header->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
                </div>
            @endif
        @endif
    
        @if(!empty($varian_warna) AND $varian_warna->urutan_konten == "6")
        <div class="container mt-5 mb-5">
            <div class="text-center">
                <h2 class="header2-content font-weight-bold">VARIAN WARNA</h2>
                <hr class="separate-line m-auto">
            </div>
    
            <div class="row justify-content-md-center">
                <div class="col-sm-5">
                    <div class="owl-carousel owl-theme varian">
                        @foreach($varian_warna_list as $vvarian_warna_list)
                        <div class="item">
                            <img src="{{ base_url() }}assets/upload/produk/varian_warna/{{$vvarian_warna_list->kode_produk}}/{{$vvarian_warna_list->isi_konten}}" alt="{{ $detail->nama_produk }}" title="{{ $detail->nama_produk }}">
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($fitur) AND $fitur->urutan_konten == "6")
        <div class="container-fluid bg-red2 pt-5 pb-5">
            <div class="text-center pb-2">
                <h2 class="header2-content font-weight-bold text-white">FITUR UNGGULAN</h2>
                <hr class="separate-line line-white m-auto">
            </div>
    
            <!-- Display on Desktop -->
            <div class="row justify-content-md-center text-center">
                <div class="col-sm-12 image-map">
                    @foreach($titik_fitur as $vtitik_fitur)
                    <?php 
                        $vtitik_fitur_explode = explode(",", $vtitik_fitur->posisi); 
                        $titikX   = $vtitik_fitur_explode[1] * 1.5 / (870.56 * 1.545) * 100;
                        $titikY   = $vtitik_fitur_explode[0] * 1.5 / 1343 * 100;
                    ?>
                    <div class="animate-point" style="margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;">
                    </div>
                     <a href="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" data-lightbox="unggulan" data-title="<!-- <a href='#'><span class='share-ico fb'></span></a><a href='#'><span class='share-ico gp'></span></a><a href='#'><span class='share-ico tw'></span></a> -->" title="Fitur Product">
                        <div class="point" style="border-color: #f7f14c;background-color: #45453f; margin-top: {{$titikX}}%;margin-left: {{$titikY}}%;" data-toggle="tooltip" data-placement="top" title="{{$vtitik_fitur->nama_fitur}}"></div>
                    </a>
                    @endforeach
                    <img src="{{ base_url() }}assets/upload/produk/fitur/{{$fitur->isi_konten}}" class="img-fluid" alt="Fitur Product" title="Fitur Product">
                </div>
            </div>
    
            <!-- Display on Mobile -->
              <div class="image-feature-res">
                  <div class="owl-carousel owl-theme feature-res">
                    @foreach($titik_fitur as $vtitik_fitur)
                      <div class="item">
                          <img src="{{base_url()}}assets/upload/fitur/{{$vtitik_fitur->gambar_fitur}}" alt="{{$vtitik_fitur->nama_fitur}}" title="{{$vtitik_fitur->nama_fitur}}">
                      </div>
                    @endforeach
                  </div>
              </div>
        </div>
        @endif
    
        @if(!empty($video) AND $video->urutan_konten == "6")
        <div class="mt-3 mb-3">
            <div class="justify-content-md-center">
                <div class="col-sm-12">
                    <?php $explode = explode('/watch?v=', $video->isi_konten); $kode = $explode[1]; ?>
                    <iframe id="myVideo" width="100%" height="738" src="{!!str_replace('/watch?v=', '/embed/', $video->isi_konten)!!}?vq=hd720&mute=1&autoplay=0&loop=1&playlist={{$kode}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        @endif
    
        @if(!empty($performa) AND $performa->urutan_konten == "6")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/performa/{{$performa->isi_konten}}" class="img-fluid" alt="Banner" title="Banner">
        </div>
        @endif
    
        @if(!empty($highlight) AND $highlight->urutan_konten == "6")
        <div class="banner-home" align="center">
            <img src="{{ base_url() }}assets/upload/produk/highlight/{{$highlight->isi_konten}}" class="img-fluid" alt="Product Highlight" title="Product Highlight">
        </div>
        @endif
        <!-- END section 6 -->
    
          
    
        <!-- Section 7 -->
        <div class="container spesikasi mt-5 mb-5">
            <div class="text-center mb-4">
                <h2 class="header2-content font-weight-bold">SPESIFIKASI MOTOR</h2>
                <hr class="separate-line m-auto">
            </div>
            <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
              <li class="nav-item item-non-justify">
                <a class="nav-link nav-tab-link menu-list active" id="mesin-tab" data-toggle="tab" href="#mesin" role="tab" aria-controls="mesin" aria-selected="false" title="Mesin"><h3 class="header3-content font-weight-bold">MESIN</h3></a>
              </li>
              <li class="nav-item item-non-justify">
                <a class="nav-link nav-tab-link menu-list" id="kelistrikan-tab" data-toggle="tab" href="#kelistrikan" role="tab" aria-controls="kelistrikan" aria-selected="false" title="Kelistrikan"><h3 class="header3-content font-weight-bold">KELISTRIKAN</h3></a>
              </li>
              <li class="nav-item item-non-justify">
                <a class="nav-link nav-tab-link menu-list" id="dimensi-tab" data-toggle="tab" href="#dimensi" role="tab" aria-controls="dimensi" aria-selected="false" title="Dimensi dan Berat"><h3 class="header3-content font-weight-bold">DIMENSI DAN BERAT</h3></a>
              </li>
              <li class="nav-item item-non-justify">
                <a class="nav-link nav-tab-link menu-list" id="rangka-tab" data-toggle="tab" href="#rangka" role="tab" aria-controls="rangka" aria-selected="false" title="Rangka dan Kaki"><h3 class="header3-content font-weight-bold">RANGKA DAN KAKI KAKI</h3></a>
              </li>
              <li class="nav-item item-non-justify">
                <a class="nav-link nav-tab-link menu-list" id="harga-tab" data-toggle="tab" href="#harga" role="tab" aria-controls="harga" aria-selected="false" title="Harga OTR"><h3 class="header3-content font-weight-bold">HARGA OTR</h3></a>
              </li>
            </ul>
        </div>
        <div class="container content-spesifikasi mt-4">
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="mesin" role="tabpanel" aria-labelledby="mesin-tab">
                  @if($detail->tipe_mesin != "" AND $detail->tipe_mesin != "-" AND $detail->tipe_mesin != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Tipe Mesin </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->tipe_mesin}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->tipe_transmisi != "" AND $detail->tipe_transmisi != "-" AND $detail->tipe_transmisi != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Tipe Transmisi</p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->tipe_transmisi}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->pola_pengoperan_gigi != "" AND $detail->pola_pengoperan_gigi != "-" AND $detail->pola_pengoperan_gigi != "0")
                  <div class="row">
                      <div class="col-pc-35">
                         <p>Pola Pengoperan Gigi</p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                         <p>  {{$detail->pola_pengoperan_gigi}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->rasio_reduksi_gigi != "" AND $detail->rasio_reduksi_gigi != "-" AND $detail->rasio_reduksi_gigi != "0")
                  <div class="row">
                      <div class="col-pc-35">
                         <p>Rasio Reduksi Gigi</p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                         <p>  {{$detail->rasio_reduksi_gigi}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->tipe_kopling != "" AND $detail->tipe_kopling != "-" AND $detail->tipe_kopling != "0")
                  <div class="row">
                      <div class="col-pc-35">
                         <p>Tipe Kopling</p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                         <p>  {{$detail->tipe_kopling}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->tipe_starter != "" AND $detail->tipe_starter != "-" AND $detail->tipe_starter != "0")
                  <div class="row">
                      <div class="col-pc-35">
                         <p>Tipe Starter</p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                         <p>  {{$detail->tipe_starter}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->tipe_busi != "" AND $detail->tipe_busi != "-" AND $detail->tipe_busi != "0")
                  <div class="row">
                      <div class="col-pc-35">
                         <p>Tipe Busi</p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                         <p>  {{$detail->tipe_busi}}</p>
                      </div>
                  </div>
                  @endif
                  <!-- @if($detail->diameter != "" AND $detail->diameter != "-" AND $detail->diameter != "0") -->
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Diameter x Langkah </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->diameter}} x {{$detail->langkah}} mm</p>
                      </div>
                  </div>
                  <!-- @endif -->
                  @if($detail->volume_langkah != "" AND $detail->volume_langkah != "-" AND $detail->volume_langkah != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Volume Langkah </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->volume_langkah}} cc</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->sistem_pendingin_mesin != "" AND $detail->sistem_pendingin_mesin != "-" AND $detail->sistem_pendingin_mesin != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Sistem Pendingin Mesin </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->sistem_pendingin_mesin}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->sistem_suplai_bb != "" AND $detail->sistem_suplai_bb != "-" AND $detail->sistem_suplai_bb != "0")
                  <div class="row">
                      <div class="col-pc-35">
                         <p>Sistem Suplai Bahan Bakar </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                         <p>  {{$detail->sistem_suplai_bb}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->perbandingan_kompresi != "" AND $detail->perbandingan_kompresi != "-" AND $detail->perbandingan_kompresi != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Perbandingan Kompresi </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->perbandingan_kompresi}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->daya_maksimum != "" AND $detail->daya_maksimum != "-" AND $detail->daya_maksimum != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Daya Maksimum </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->daya_maksimum}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->torsi_maksimum != "" AND $detail->torsi_maksimum != "-" AND $detail->torsi_maksimum != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Torsi Maksimum </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->torsi_maksimum}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->torsi_maksimum != "" AND $detail->torsi_maksimum != "-" AND $detail->torsi_maksimum != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Torsi Maksimum </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->torsi_maksimum}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->sistem_pelumasan != "" AND $detail->sistem_pelumasan != "-" AND $detail->sistem_pelumasan != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Sistem Pelumasan </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->sistem_pelumasan}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->sistem_pengereman != "" AND $detail->sistem_pengereman != "-" AND $detail->sistem_pengereman != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Sistem Pengereman </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->sistem_pengereman}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->sistem_idling_stop != "" AND $detail->sistem_idling_stop != "-" AND $detail->sistem_idling_stop != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Sistem Idling Stop </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->sistem_idling_stop}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->kapasitas_tangki_bb != "" AND $detail->kapasitas_tangki_bb != "-" AND $detail->kapasitas_tangki_bb != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Kapasitas Tangki Bahan Bakar </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->kapasitas_tangki_bb}} L</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->kapasitas_minyak_pelumas != "" AND $detail->kapasitas_minyak_pelumas != "-" AND $detail->kapasitas_minyak_pelumas != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Kapasitas Minyak Pelumas </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->kapasitas_minyak_pelumas}} L</p>
                      </div>
                  </div>
                  @endif
              </div>
              <div class="tab-pane fade" id="kelistrikan" role="tabpanel" aria-labelledby="kelistrikan-tab">
                  @if($detail->tipe_baterai != "" AND $detail->tipe_baterai != "-" AND $detail->tipe_baterai != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Tipe Baterai </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->tipe_baterai}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->sistem_pengapian != "" AND $detail->sistem_pengapian != "-" AND $detail->sistem_pengapian != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Sistem Pengapaian </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->sistem_pengapian}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->lampu_depan != "" AND $detail->lampu_depan != "-" AND $detail->lampu_depan != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Lampu Depan </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->lampu_depan}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->lampu_senja != "" AND $detail->lampu_senja != "-" AND $detail->lampu_senja != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Lampu Senja </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->lampu_senja}}</p>
                      </div>
                  </div>
                  @endif
              </div>
              <div class="tab-pane fade" id="dimensi" role="tabpanel" aria-labelledby="dimensi-tab">
                  <!-- @if($detail->panjang != "" AND $detail->panjang != "-" AND $detail->panjang != "0") -->
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Panjang x Lebar x tinggi </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->panjang}} x {{$detail->lebar}} x {{$detail->tinggi}} mm </p>
                      </div>
                  </div>
                  <!-- @endif -->
                  @if($detail->jarak_sumbu_roda != "" AND $detail->jarak_sumbu_roda != "-" AND $detail->jarak_sumbu_roda != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Jarak Sumbu Roda </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->jarak_sumbu_roda}} mm</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->jarak_terendah_ke_tanah != "" AND $detail->jarak_terendah_ke_tanah != "-" AND $detail->jarak_terendah_ke_tanah != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Jarak Terendah Ke Tanah </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->jarak_terendah_ke_tanah}} mm</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->ketinggian_tempat_duduk != "" AND $detail->ketinggian_tempat_duduk != "-" AND $detail->ketinggian_tempat_duduk != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Ketinggian Tempat Duduk </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->ketinggian_tempat_duduk}} mm</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->radius_putar != "" AND $detail->radius_putar != "-" AND $detail->radius_putar != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Radius Putar </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->radius_putar}} mm</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->berat_kosong != "" AND $detail->berat_kosong != "-" AND $detail->berat_kosong != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Berat Kosong </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->berat_kosong}} kg</p>
                      </div>
                  </div>
                  @endif
              </div>
              <div class="tab-pane fade" id="rangka" role="tabpanel" aria-labelledby="rangka-tab">
                  @if($detail->tipe_rangka != "" AND $detail->tipe_rangka != "-" AND $detail->tipe_rangka != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Tipe Rangka </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  
                            {{$detail->tipe_rangka}}
                          </p>
                      </div>
                  </div>
                  @endif
                  @if($detail->tipe_suspensi_depan != "" AND $detail->tipe_suspensi_depan != "-" AND $detail->tipe_suspensi_depan != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Tipe Suspensi Depan </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->tipe_suspensi_depan}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->tipe_suspensi_belakang != "" AND $detail->tipe_suspensi_belakang != "-" AND $detail->tipe_suspensi_belakang != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Tipe Suspensi Belakang </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->tipe_suspensi_belakang}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->ukuran_ban_depan != "" AND $detail->ukuran_ban_depan != "-" AND $detail->ukuran_ban_depan != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Ukuran Ban Depan </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->ukuran_ban_depan}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->ukuran_ban_belakang != "" AND $detail->ukuran_ban_belakang != "-" AND $detail->ukuran_ban_belakang != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Ukuran Ban Belakang </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->ukuran_ban_belakang}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->rem_depan != "" AND $detail->rem_depan != "-" AND $detail->rem_depan != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Rem Depan </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->rem_depan}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->rem_belakang != "" AND $detail->rem_belakang != "-" AND $detail->rem_belakang != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Rem Belakang </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->rem_belakang}}</p>
                      </div>
                  </div>
                  @endif
                  @if($detail->jenis_velg != "" AND $detail->jenis_velg != "-" AND $detail->jenis_velg != "0")
                  <div class="row">
                      <div class="col-pc-35">
                          <p>Jenis Velg </p>
                      </div>
                      <div class="col-pc-5">
                          :
                      </div>
                      <div class="col-pc-60">
                          <p>  {{$detail->jenis_velg}}</p>
                      </div>
                  </div>
                  @endif
              </div>
              <div class="tab-pane fade" id="harga" role="tabpanel" aria-labelledby="harga-tab">
                  @if($harga_otr != "[]")
                  @foreach($harga_otr as $vharga_otr)
                  <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-10">
                                    <p>{{$vharga_otr->nama_produk_otr}} </p>
                                </div>
                                <div class="col-2">
                                    :
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-1"></div>
                                <div class="col-5">
                                    <p>  Rp. {{number_format($vharga_otr->harga_produk_otr,'0',',','.')}}</p>
                                </div>
                                <div class="col-5">
                                    <p> {{ $vharga_otr->lokasi_otr  }}</p>
                                </div>
                                <div class="col-1"></div>
                            </div>
                        </div>
                  </div>
                  @endforeach
                  @endif
              </div>
            </div>
            <div id="aksesoris" class="pb-5"></div>
        </div>
    
        <div class="container-fluid bg-grey pt-4 pb-4">
            <div class="container head-related bg-white">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link nav-related-link active" id="recommend-tab" data-toggle="tab" href="#recommend" role="tab" aria-controls="recommend" aria-selected="false" title="Produk Rekomendasi">PRODUK REKOMENDASI</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link nav-related-link" id="aksesoris-relate-tab" data-toggle="tab" href="#aksesoris-relate" role="tab" aria-controls="aksesoris-relate" aria-selected="false" title="Aksesoris">AKSESORIS</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link nav-related-link" id="aparel-tab" data-toggle="tab" href="#aparel" role="tab" aria-controls="aparel" aria-selected="false" title="Apparel">APPAREL</a>
                  </li>
                </ul>
            </div>
            <div class="container bg-white body-related">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="recommend" role="tabpanel" aria-labelledby="recommend-tab">
                        <div class="owl-carousel owl-theme rekomendasi-produk mt-5">
                            @if(!empty($rekomendasi1))
                            <div class="item" align="center">
                               <img src="{{ base_url() }}assets/upload/produk/gambar/{{$rekomendasi1->gambar_produk}}" class="img-fluid" alt="{{$rekomendasi1->nama_produk}}" title="{{$rekomendasi1->nama_produk}}">
                                   <p class="mb-0 judul-produk-res">{{$rekomendasi1->nama_produk}}</p>
                                   <p class="text-red harga-produk-res">{{$rekomendasi1->harga_produk}}</p>
                                   <a href="{{base_url()}}produk/{{$rekomendasi1->nama_produk_url}}" class="btn btn-flat-red btn-block" title="{{$rekomendasi1->nama_produk}}">Lihat Produk</a>
                            </div>
                            @endif
                            @if(!empty($rekomendasi2))
                            <div class="item" align="center">
                               <img src="{{ base_url() }}assets/upload/produk/gambar/{{$rekomendasi2->gambar_produk}}" class="img-fluid" alt="{{$rekomendasi2->nama_produk}}" title="{{$rekomendasi2->nama_produk}}">
                                   <p class="mb-0 judul-produk-res">{{$rekomendasi2->nama_produk}}</p>
                                   <p class="text-red harga-produk-res">{{$rekomendasi2->harga_produk}}</p>
                                   <a href="{{base_url()}}produk/{{$rekomendasi2->nama_produk_url}}" class="btn btn-flat-red btn-block" title="{{$rekomendasi2->nama_produk}}">Lihat Produk</a>
                            </div>
                            @endif
                            @if(!empty($rekomendasi3))
                            <div class="item" align="center">
                               <img src="{{ base_url() }}assets/upload/produk/gambar/{{$rekomendasi3->gambar_produk}}" class="img-fluid" alt="{{$rekomendasi3->nama_produk}}" title="{{$rekomendasi3->nama_produk}}">
                                   <p class="mb-0 judul-produk-res">{{$rekomendasi3->nama_produk}}</p>
                                   <p class="text-red harga-produk-res">{{$rekomendasi3->harga_produk}}</p>
                                   <a href="{{base_url()}}produk/{{$rekomendasi3->nama_produk_url}}" class="btn btn-flat-red btn-block" title="{{$rekomendasi3->nama_produk}}">Lihat Produk</a>
                            </div>
                            @endif
                            @if(!empty($rekomendasi4))
                            <div class="item" align="center">
                               <img src="{{ base_url() }}assets/upload/produk/gambar/{{$rekomendasi4->gambar_produk}}" class="img-fluid" alt="{{$rekomendasi4->nama_produk}}" title="{{$rekomendasi4->nama_produk}}">
                                   <p class="mb-0 judul-produk-res">{{$rekomendasi4->nama_produk}}</p>
                                   <p class="text-red harga-produk-res">{{$rekomendasi4->harga_produk}}</p>
                                   <a href="{{base_url()}}produk/{{$rekomendasi4->nama_produk_url}}" class="btn btn-flat-red btn-block" title="{{$rekomendasi4->nama_produk}}">Lihat Produk</a>
                            </div>
                            @endif
                      </div>
                    </div>
                    <div class="tab-pane fade" id="aksesoris-relate" role="tabpanel" aria-labelledby="aksesoris-relate-tab">
                      <div class="owl-carousel owl-theme aksesoris-terkait mt-5">
                          @if(!empty($aksesoris))
                          @foreach($aksesoris as $vaksesoris)
                          <div class="item" align="center">
                              <!-- <a href="#"> -->
                                  <img src="{{ base_url() }}assets/upload/part_aksesoris/aksesoris/{{$vaksesoris->gambar_part_aksesoris}}" class="img-fluid" alt="{{$vaksesoris->nama_part_aksesoris}}" title="{{$vaksesoris->nama_part_aksesoris}}">
                                  <p class="mb-0">{{$vaksesoris->nama_part_aksesoris}}</p>
                                  @if($vaksesoris->harga_part_aksesoris != "0" && $vaksesoris->harga_part_aksesoris != "")
                                  <p class="text-red">Rp. {{number_format($vaksesoris->harga_part_aksesoris,'0',',','.')}}</p>
                                  @endif
                              <!-- </a> -->
                          </div>
                          @endforeach
                          @endif
                      </div>
                    </div>
                    <div class="tab-pane fade" id="aparel" role="tabpanel" aria-labelledby="aparel-tab">
                       <div class="owl-carousel owl-theme aparel-terkait mt-5">
                          @if(!empty($aparel))
                          @foreach($aparel as $vaparel)
                          <div class="item" align="center">
                              <!-- <a href="#"> -->
                                  <img src="{{ base_url() }}assets/upload/part_aksesoris/apparel/{{$vaparel->gambar_apparel}}" class="img-fluid" alt="{{$vaparel->nama_apparel}}" title="{{$vaparel->nama_apparel}}">
                                  <p class="mb-0">{{$vaparel->nama_apparel}}</p>
                                  @if($vaparel->harga_apparel != "0" && $vaparel->harga_apparel != "")
                                  <p class="text-red">Rp. {{number_format($vaparel->harga_apparel,'0',',','.')}}</p>
                                  @endif
                              <!-- </a> -->
                          </div>
                          @endforeach
                          @endif
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END section 7 -->  
    </div>
    
    <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script type="text/javascript">
      var currentURL = window.location.href;
      var result = currentURL.split('#');
      if(result[1] == "aksesoris"){
        $('#recommend-tab').removeClass("active");
        $('#aparel-tab').removeClass("active");
        $('#aksesoris-relate-tab').addClass("active");

        $('#recommend').removeClass("show active");
        $('#aparel').removeClass("show active");
        $('#aksesoris-relate').addClass("show active");
      }else{
        $('#aksesoris-relate-tab').removeClass("active");
        $('#aparel-tab').removeClass("active");
        $('#recommend-tab').addClass("active");
        
        $('#aksesoris-relate').removeClass("show active");
        $('#aparel').removeClass("show active");
        $('#recommend').addClass("show active");
      }
    </script>
    <script type="text/javascript">

        $('.varian').owlCarousel({
            loop:true,
            margin:10,
            dots: true,
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:1
                }
            }
        });

        $('.feature-res').owlCarousel({
            loop:true,
            margin:10,
            dots: true,
            stageOuterClass: 'owl-stage-outer outer-stage',
            dotsClass: 'owl-dots dots-container',
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'nav-container',
            navClass: [ 'prev-slide', 'next-slide' ],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        })

        $('.rekomendasi-produk').owlCarousel({
            loop:false,
            margin:10,
            nav:true,
            dots: false,
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'owl-nav nav-container-news',
            navClass: [ 'owl-prev prev-news', 'owl-next next-news' ],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:2
                },
                1000:{
                    items:4
                }
            }
        })
        $('.aksesoris-terkait').owlCarousel({
            loop:false,
            margin:10,
            nav:true,
            dots: false,
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'owl-nav nav-container-news',
            navClass: [ 'owl-prev prev-news', 'owl-next next-news' ],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:2
                },
                1000:{
                    items:4
                }
            }
        })
        $('.aparel-terkait').owlCarousel({
            loop:false,
            margin:10,
            nav:true,
            dots: false,
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'owl-nav nav-container-news',
            navClass: [ 'owl-prev prev-news', 'owl-next next-news' ],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:2
                },
                1000:{
                    items:4
                }
            }
        })

        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })

        function editfitur($id, $coorX, $coorY, $nama, $deskripsi, $gambar){
          // # berhenti submit
          event.preventDefault();
          var gambar = '{{ base_url() }}assets/upload/fitur/'+$gambar;
          $('input[name="cX"]').val($coorX);
          $('input[name="cY"]').val($coorY);
          $('input[name="nama_fitur"]').val($nama);
          $('textarea[name="deskripsi_fitur"]').val($deskripsi);
          $('input[name="id"]').val($id);
          $("#tampilGambarFitur").attr("src", gambar);
          $('.delete').show();
          $('.gambar-edit').show();
          $('.dropzone-edit').hide();
          $('#m_modal').modal('show');
          $('.save').attr("disabled",false);
        }
        
        /* Set For Search */
    var get_news        = "{{base_url()}}ajax_news_filter";
    var count_news_page = "{{base_url()}}total_page_news";
    var totalPageAll    = "{{$total_page_all}}";

    var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="'+judul+'">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });
    /* End Set For Search */
    </script>
@stop