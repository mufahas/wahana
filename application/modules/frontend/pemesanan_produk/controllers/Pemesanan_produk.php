<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan_produk extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        $this->load->model('select_global_model');
    }

    function index($url) 
    {
        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        /* End For Search */
        
        /* Set Data */
        $detail                       = ms_produk::select(
                                                        'ms_produk.kode_produk',
                                                        'ms_produk.nama_produk',
                                                        'ms_produk.harga_produk',
                                                        'ms_harga_otr.kode_harga_otr',
                                                        'ms_harga_otr.nama_produk_otr',
                                                        'ms_harga_otr.harga_produk_otr',
                                                        'ms_kategori_motor.nama_kategori_motor'
                                                    )
                                                ->join('ms_harga_otr','ms_harga_otr.kode_produk','=','ms_produk.kode_produk')
                                                ->join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')
                                                ->where('ms_produk.dihapus', 'F')
                                                ->where('ms_produk.status_tampil', 'T')
                                                ->where('ms_produk.nama_produk_url', $url)
                                                ->first();

        $data['detail']               = $detail;
        
        /* Button Action */
        $data['dp']                   = '- Pilih DP -';
        $data['tenor']                = $this->select_global_model->selectTenor();
        $data['produk_otr']           = $this->select_global_model->selectProdukOtr($detail->kode_produk);
        
        $data['getVarianProduk']      = site_url() . $this->class . '/ajax_get_varian_produk';
        $data['getHargaVarianProduk'] = site_url() . $this->class . '/ajax_get_harga_varian_produk';
        $data['getDPVarianProduk']    = site_url() . $this->class . '/ajax_get_dp_varian_produk';
        $data['getHargaCicilan']      = site_url() . $this->class . '/ajax_get_harga_cicilan';
        $data['getVerifikasiKode']    = site_url() . $this->class .  '/ajax_get_verifikasi_kode';

        $data['message_success']   = $this->session->flashdata('success');
        $data['message_error']     = $this->session->flashdata('error');

        $data['action']               = site_url() . $this->class . '/save';

        $this->load_view_frontend("frontend","pemesanan_produk","pemesanan_produk","v_pemesanan_produk",$data);
    }


    function save()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
          
            /* Post Pemesanan */
            $produk            = decryptID($this->security->xss_clean($this->input->post('produk_otr'))); //produk dari table ms_harga_otr
            $uang_muka         = $this->security->xss_clean($this->input->post('dp'));
            $tenor             = $this->security->xss_clean($this->input->post('tenor'));
            $estimasi_cicilan  = $this->security->xss_clean($this->input->post('angsuran'));

            /* Remove String From View */
            $find              = array('Rp',',00','.');
            $cicilan           = str_replace($find,'',$estimasi_cicilan);
          
            /* Post Pelanggan */
            $nama              = $this->security->xss_clean($this->input->post('nama_lengkap'));
            $email             = $this->security->xss_clean($this->input->post('email'));
            $nomor_hp          = $this->security->xss_clean($this->input->post('nomor_hp'));
            $domisili          = $this->security->xss_clean($this->input->post('domisili'));

            /* Get URL Produk */
            $ms_produk  = ms_produk::selectRaw('nama_produk_url')
                                    ->join('ms_harga_otr','ms_harga_otr.kode_produk','=','ms_produk.kode_produk')
                                    ->where('ms_harga_otr.kode_harga_otr',$produk)
                                    ->first();

            $url        = site_url() . 'pemesanan-produk/' . $ms_produk->nama_produk_url;

            
            /* Begin Save Table tbl_pelanggan */
            $tbl_pelanggan                          = new tbl_pelanggan;
            
            $tbl_pelanggan->nama_pelanggan          = ucwords($nama);
            $tbl_pelanggan->email_pelanggan         = strtolower($email);
            $tbl_pelanggan->nomor_telepon_pelanggan = $nomor_hp;
            $tbl_pelanggan->domisili                = ucwords($domisili);
            
            $save_pelanggan                         = $tbl_pelanggan->save();
            /* End Save Table tbl_pelanggan */

            /*  Begin Save Table tbl_pemesanan */
            $tbl_pemesanan                            = new tbl_pemesanan;
            
            $date                                     = date('Ym');
            $prefix                                   = array('length' => 5, 'prefix' => 'ORD' . $date , 'position' => 'right');
            $kode                                     = $this->generate_id_model->code_prefix('tbl_pemesanan', 'kode_pemesanan', $prefix);
            
            $tbl_pemesanan->kode_pemesanan            = $kode;
            $tbl_pemesanan->kode_harga_otr            = $produk;
            $tbl_pemesanan->kode_pelanggan            = tbl_pelanggan::max('kode_pelanggan');
            $tbl_pemesanan->jangka_pinjaman           = $tenor;
            $tbl_pemesanan->uang_muka                 = $uang_muka;
            $tbl_pemesanan->estimasi_cicilan_perbulan = $cicilan;
            $tbl_pemesanan->tanggal_pemesanan         = date('Y-m-d');

            $save_pemesanan                           = $tbl_pemesanan->save();
            /*  End Save Table tbl_pemesanan */

            if($save_pelanggan && $save_pemesanan)
            {
                $this->session->set_flashdata('success', 'Pemesanan Berhasil Dikirim');
                redirect($url);
            } else 
            {
                $this->session->set_flashdata('error', 'Pemesanan Gagal Dikirim');
                redirect($url);
            }  
        }
    }

    function ajax_get_harga_varian_produk()
    {
        $kode_harga_otr = decryptID($this->security->xss_clean($this->input->post('kode_harga_otr')));

        $harga_otr      = ms_harga_otr::where('kode_harga_otr',$kode_harga_otr)->first();

        $result         = 'Rp. ' . number_format($harga_otr->harga_produk_otr,2,',','.'); 
        echo json_encode($result);
    }

    function ajax_get_dp_varian_produk()
    {
        $kode_harga_otr = decryptID($this->security->xss_clean($this->input->post('kode_harga_otr')));

        $dp             = $this->select_global_model->selectDPProduk($kode_harga_otr);
        
        $result         = form_dropdown('dp', $dp, '', 'class="form-control m-input"');
        echo json_encode($result);
    }

    function ajax_get_harga_cicilan()
    {
        $dp             = $this->security->xss_clean($this->input->get('dp'));
        $tenor          = $this->security->xss_clean($this->input->get('tenor'));
        $kode_harga_otr = decryptID($this->security->xss_clean($this->input->get('kode_harga_otr')));
        

        if($tenor == '11') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_11')->where('kode_harga_otr',$kode_harga_otr)->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_11,2,',','.');
        } elseif($tenor == '17') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_17')->where('kode_harga_otr',$kode_harga_otr)->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_17,2,',','.');
        } elseif($tenor == '23') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_23')->where('kode_harga_otr',$kode_harga_otr)->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_23,2,',','.');
        } elseif($tenor == '27') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_27')->where('kode_harga_otr',$kode_harga_otr)->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_27,2,',','.');
        } elseif($tenor == '29') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_29')->where('kode_harga_otr',$kode_harga_otr)->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_29,2,',','.');  
        } elseif($tenor == '33') {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_33')->where('kode_harga_otr',$kode_harga_otr)->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_33,2,',','.');  
        } else {
            $harga_cicilan = ms_kalkulator_cicilan::selectRaw('tenor_35')->where('kode_harga_otr',$kode_harga_otr)->where('dp',$dp)->first();
            $result        = 'Rp. ' . number_format($harga_cicilan->tenor_35,2,',','.');
        }

        echo json_encode($result);
    }

    function ajax_get_verifikasi_kode()
    {
        $no_hp           = $this->security->xss_clean($this->input->post('no_hp'));
      
        $string          = '0123456789';
        $string_shuffled = str_shuffle($string);
        $password        = substr($string_shuffled, 1, 6);
        $pesan           = 'Wahana Honda Testride: Angka verifikasi Anda adalah: '.$password;

        $curl = curl_init();
      
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_URL             => 'http://api.nusasms.com/api/v3/sendsms/plain?&output=json',
            CURLOPT_POST            => true,
            CURLOPT_POSTFIELDS      => array(
                                                'user'     => 'wahanaartha_api',
                                                'password' => 'Wahana321+',
                                                'SMSText'  => $pesan,
                                                'GSM'      => $no_hp
                                            )
        ));

        $resp = curl_exec($curl);
        
        $data = array('resp' => $resp,'password' => $password);

        if (!$resp) 
        {
            die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        } 
        else 
        {
            echo json_encode($data);
        }
    }
}