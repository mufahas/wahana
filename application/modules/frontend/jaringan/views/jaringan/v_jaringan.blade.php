@extends('frontend.default.views.layout.v_layout')
@section('head')
<style type="text/css">
    #jaringan-list img {
      max-width: none;
    }
</style>
<title>Alamat Dealer dan Distributor Motor Honda | Wahana Honda</title>
<meta name="description" content="Alamat/lokasi dan nomer telepon dealer serta distributor sepeda motor honda wilayah Jakarta dan Tangerang. Silahkan pilih area yang terdekat dengan Anda">
@endsection

@section('body')
  
  <div id="for_container">
    <div class="container-fluid bg-feature">
        <div class="container text-center">
            <h1 class="header1-content m-auto text-white">Dealers Wahana Honda</h1>
        </div>
    </div>
    <div class="row">
      <div class="col-md-3">  
        <div class="container mt-4">
          <div class="row justify-content-md-center">
              <div class="col-md-12 text-center">
                  <img src="{{ base_url() }}assets/frontend/img/Jaringan-1.png" alt="Banner" title="Banner">
              </div>
          </div>
        </div>
        <div class="container mt-4 mb-5">
          <form class="" action="index.html" method="post">
              <div class="row justify-content-md-center">
                  <div class="col-md-10">
                      <div class="form-group">
                          <label>Silahkan pilih area</label>
                          <select class="custom-select filter-jaringan" name="area" id="inputGroupSelect01">
                              <option value="">Semua area</option>
                              @if(!empty($select_area))
                              @foreach($select_area as $vselect_area)
                              <option value="{{$vselect_area->kode_kota_kabupaten}}">{{$vselect_area->nama_kota_kabupaten}}</option>
                              @endforeach
                              @endif
                          </select>
                      </div>
                  </div>
              </div>
              <div class="row display-none" id="filterPelayanan" style="padding-top: 10px;">
                <div class="col-md-8 checkbox ml-auto mr-auto">
                  <label>
                    <input type="checkbox" name="h1" class="h1" value="H1"> H1 - PENJUALAN
                  </label>
                </div>

                <div class="col-md-8 checkbox ml-auto mr-auto">
                  <label>
                    <input type="checkbox" name="h2" class="h2" value="H2"> H2 - PEMELIHARAAN
                  </label>
                </div>

                <div class="col-md-8 checkbox ml-auto mr-auto">
                  <label>
                    <input type="checkbox" name="h3" class="h3" value="H3"> H3 - SUKU CADANG
                  </label>
                </div>
              </div>
              <br>
              
              <div class="row justify-content-md-center" style="overflow: auto; height:400px;" id="jaringan-list">
              
              </div>
          </form>
        </div>
          <!-- <div class="row col-md-12 p-5" id="listDealer"> -->
            <!-- @if(!empty($jaringan))
              @foreach($jaringan as $vjaringan)
                <div class="col-md-3 pb-5 d-flex flex-column" align="center">
                  <div>
                    <h5><b>{{$vjaringan->nama_dealer}}</b></h5>
                    <p class="fs-13">
                      {{$vjaringan->alamat_jaringan}}<br>
                      <a href="tel:{{$vjaringan->nomor_telepon}}" target="_blank">{{$vjaringan->nomor_telepon}}</a><br>
                      {{$vjaringan->nama_kota_kabupaten}}
                    </p>
                  </div>
                </div>
              @endforeach
            @endif -->
          <!-- </div> -->

          
      </div>

      <div class="col-md-9">
        <div id="map-canvas" class="col-sm-12" style="height:800px;"></div>
      </div>
    </div>
  </div>

  <!--  Display For Search Result -->
    <div class="container mt-4 mb-5" id="contentSearch" style="display: none;">  
        <div class="tab-content bg-grey mt-4">
            <div class="container-fluid bg-feature">
                <div class="container text-center pt-5 pb-5">
                    <p class="header-search">Berita WMS</p>
                </div>
            </div>

            <div class="tab-pane fade show active" id="all-news" role="tabpanel" aria-labelledby="all-news-tab"> 
                <div id="contentAll" class="row row-eq-height"></div> 
                <ul id="paginationAll" class="pagination-sm justify-content-end"></ul>
            </div>
        </div>
    </div>
    <!-- End Display For Search Result -->

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/dealers/markerclusterer.js"></script>
    <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?&libraries=places&callback=initialize"></script> -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOv9JAHiAWZ34Of1vlN5g1enIb0FWr2qU&libraries=places&callback=initialize" type="text/javascript"></script>
    <!-- BEGIN JS MARKER MAPPING -->
    <script type="text/javascript">
       document.addEventListener('contextmenu', event => event.preventDefault());
        $(document).keydown(function (event) {
           if (event.keyCode == 123) { 
                return false;
            } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {      
               return false;
            }
        });
        
      var marker;
      var markers      = [];
      var markerCluster;
      var infowindow   = null;
      var map          = null;
      var icon         = '';

      function initialize()
      {
        
        /*--------------------- Begin Set Variable Untuk Setting Marker Google Maps -------------------- */
          var infoWindow      = new google.maps.InfoWindow;                           
          var myLatlng        = {lat: -6.229728, lng: 106.6894294};                   
          var mapOptions      = {mapTypeId: google.maps.MapTypeId.ROADMAP,
          zoom: 10,
          center: new google.maps.LatLng(myLatlng)
          };
          var map             = new google.maps.Map(document.getElementById('map-canvas'), mapOptions); 
          var bounds          = new google.maps.LatLngBounds(); 
      
        /*--------------------- End Set Variable Untuk Setting Marker Google Maps -------------------- */
        
        
        /*--------------------- Begin Set Variable Untuk Ambil Data List Jaringan -------------------- */
          var getListJaringan = "{{base_url()}}jaringan/jaringan/ajax_get_jaringan"; 
        
          var data_pelayanan = {area:'', penjualan: '', pemeliharan: '', sukucadang: ''};

          $.ajax({
            type: 'POST',
            url: getListJaringan,
            data: data_pelayanan,
            dataType: 'json',
            cache: false,
            success: function(result) {
              clearMarkers();
              var html         = '';
              $.each(result, function(i,v) {

                // Definisi Kategori Jaringan
                var katjar = v.kategori_jaringan;

                if(katjar == 'H1')
                {
                  var kategori_jaringan = 'PENJUALAN';
                } 
                else if(katjar == 'H2')
                {
                  var kategori_jaringan = 'PEMELIHARAN';
                } 
                else 
                {
                  var kategori_jaringan = 'SUKU CADANG';
                }
                // Definisi Kategori Jaringan

                var info = v.nama_dealer + '<br>' + v.alamat_dealer + '<br>' + katjar;
                var lat  = parseFloat(v.latitude);
                var lng  = parseFloat(v.longitude);

                if(lat != '' && lat != undefined && lng != '' && lng != undefined)
                {
                  addMarker(lat, lng, info);
                }

                html += '<div class="col-md-10" align="left" style="border-top: 1px solid grey; padding-top: 10px;">' +
                        '<span class="latitude" style="display: none;">'+ lat +'</span>'+
                        '<span class="longitude" style="display: none;">'+ lng +'</span>'+
                        '<span class="info_" style="display: none;">'+ v.nama_dealer +'</span>'+
                        '<h2 class="header2-content cobadah"><b><a href="#" class="" title="'+v.nama_dealer+'">'+v.nama_dealer+'</a></b></h2>'+
                        '<p class="fs-13">'+
                        ''+ v.alamat_dealer +'<br>'+
                        '<a href="tel:'+ v.no_telepon +'" target="_blank" title="No.Telepon">'+ v.no_telepon +'</a><br>'+
                        ''+ v.kota_kabupaten +''+
                        '</p>'+
                        '<p class="fs-13"><a href="'+ v.link_peta +'" target="_blank"><b>Open In Map</b></a></p>'+
                        '</div>';
              });
              $('#jaringan-list').html(html);
            } 
          });

        /* BEGIN SET ON CHANGE AREA  */
        $('select[name="area"]').change(function(){
          clearMarkers();
          $('#filterPelayanan').removeClass('display-none');

          var area_pelayanan_change = $(this).val();
          var data_area_change      = {area: area_pelayanan_change, penjualan: '', pemeliharan: '', sukucadang: ''};

           $.ajax({
            type: 'POST',
            url: getListJaringan,
            data: data_area_change,
            dataType: 'json',
            cache: false,
            success: function(result) {
            clearMarkers();
              var html         = '';
              $.each(result, function(i,v) {

                // Begin Defini Kategori Jaringan
                var katJar = v.kategori_jaringan;

                if(katJar == 'H1')
                { 
                  var kategori_jaringan = 'PENJUALAN';
                }
                else if(katJar == 'H2')
                {
                  var kategori_jaringan = 'PEMELIHARAN';
                } 
                else 
                {
                  var kategori_jaringan = 'SUKU CADANG';
                }
                // End Defini Kategori Jaringan

                var info = v.nama_dealer + '<br>' + v.alamat_dealer + '<br>' + kategori_jaringan;
                var lat  = parseFloat(v.latitude);
                var lng  = parseFloat(v.longitude);

                if(lat != '' && lat != undefined && lng != '' && lng != undefined)
                {
                  addMarker(lat, lng, info);
                }

                html += '<div class="col-md-10" align="left" style="border-top: 1px solid grey; padding-top: 10px;">' +
                        '<span class="latitude" style="display: none;">'+ lat +'</span>'+
                        '<span class="longitude" style="display: none;">'+ lng +'</span>'+
                        '<span class="info_" style="display: none;">'+ v.nama_dealer +'</span>'+
                        '<h2 class="header2-content cobadah"><b><a href="#" class="" title="'+v.nama_dealer+'">'+v.nama_dealer+'</a></b></h2>'+
                        '<p class="fs-13">'+
                        ''+ v.alamat_dealer +'<br>'+
                        '<a href="tel:'+ v.no_telepon +'" target="_blank" title="No.Telepon">'+ v.no_telepon +'</a><br>'+
                        ''+ v.kota_kabupaten +''+
                        '</p>'+
                        '<p class="fs-13"><a href="'+ v.link_peta +'" target="_blank"><b>Open In Map</b></a></p>'+
                        '</div>';

              });
              $('#jaringan-list').html(html);
            }
          });
        });
        /* END SET ON CHANGE AREA  */

        /* On Change Checkbox For Area Pelayanan */
        $('.h1, .h2, .h3').change(function() {
          clearMarkers();
          var get_area = $('select[name="area"]').val();
          var h1       = $('.h1').is(':checked');
          var h2       = $('.h2').is(':checked');
          var h3       = $('.h3').is(':checked');

          // All Filter Empty
          if(h1 == false && h2 == false && h3 == false)
          {
            var data_jaringan = {area: get_area, penjualan: '', pemeliharan: '', sukucadang: ''};
          }  

          // Area and H1 Penjualan Selected
          if(h1 == true && h2 == false && h3 == false)
          {
            var penjualan     = $('input[name="h1"]').val();
            var data_jaringan = {area: get_area, penjualan: penjualan, pemeliharan: '', sukucadang: ''};
          } 

          // Area and H2 Pemeliharaan Selected
          if(h1 == false && h2 == true && h3 == false)
          {
            var pemeliharan   = $('input[name="h2"]').val();
            var data_jaringan = {area: get_area, penjualan: '', pemeliharan: pemeliharan, sukucadang: ''};
          }

          // Area and H3 Suku Cadang Selected
          if(h1 == false && h2 == false && h3 == true)
          {
            var sukucadang    = $('input[name="h3"]').val();
            var data_jaringan = {area: get_area, penjualan: '', pemeliharan: '', sukucadang: sukucadang};
          }

          // Area, H1, and H2 Selected
          if(h1 == true && h2 == true && h3 == false)
          {
            var penjualan     = $('input[name="h1"]').val();
            var pemeliharan   = $('input[name="h2"]').val();
            
            var data_jaringan = {area: get_area, penjualan: penjualan, pemeliharan: pemeliharan, sukucadang: ''};
          }

          // Area, H1, and H3 Selected
          if(h1 == true && h2 == false && h3 == true)
          {
            var penjualan     = $('input[name="h1"]').val();
            var sukucadang    = $('input[name="h3"]').val();
            
            var data_jaringan = {area: get_area, penjualan: penjualan, pemeliharan: '', sukucadang: sukucadang};
          }

          // Area, H2, and H3 Selected
          if(h1 == false && h2 == true && h3 == true )
          { 
            var pemeliharan   = $('input[name="h2"]').val();
            var sukucadang    = $('input[name="h3"]').val();
            
            var data_jaringan = {area: get_area, penjualan: '', pemeliharan: pemeliharan, sukucadang: sukucadang};
          } 

          // All Filter Selected
          if(h1 == true && h2 == true && h3 == true)
          {
            var penjualan   = $('input[name="h1"]').val();
            var pemeliharan = $('input[name="h2"]').val();
            var sukucadang  = $('input[name="h3"]').val();

            var data_jaringan = {area: get_area, penjualan: penjualan, pemeliharan: pemeliharan, sukucadang: sukucadang};
          }    

          $.ajax({
            type: 'POST',
              url: getListJaringan,
              data: data_jaringan,
              dataType: 'json',
              cache: false,
              success: function(result) {
                var html         = '';
                $.each(result, function(i,v) {
                  var katJar = v.kategori_jaringan;

                if(katJar == 'H1')
                { 
                  var kategori_jaringan = 'PENJUALAN';
                }
                else if(katJar == 'H2')
                {
                  var kategori_jaringan = 'PEMELIHARAN';
                } 
                else 
                {
                  var kategori_jaringan = 'SUKU CADANG';
                }
                // End Defini Kategori Jaringan

                var info = v.nama_dealer + '<br>' + v.alamat_dealer + '<br>' + kategori_jaringan;
                var lat  = parseFloat(v.latitude);
                var lng  = parseFloat(v.longitude);

                if(lat != '' && lat != undefined && lng != '' && lng != undefined)
                {
                  addMarker(lat, lng, info);
                }

                html += '<div class="col-md-10" align="left" style="border-top: 1px solid grey; padding-top: 10px;">' +
                        '<span class="latitude" style="display: none;">'+ lat +'</span>'+
                        '<span class="longitude" style="display: none;">'+ lng +'</span>'+
                        '<span class="info_" style="display: none;">'+ v.nama_dealer +'</span>'+
                        '<h2 class="header2-content cobadah"><b><a href="#" class="" title="'+v.nama_dealer+'">'+v.nama_dealer+'</a></b></h2>'+
                        '<p class="fs-13">'+
                        ''+ v.alamat_dealer +'<br>'+
                        '<a href="tel:'+ v.no_telepon +'" target="_blank" title="No.Telepon">'+ v.no_telepon +'</a><br>'+
                        ''+ v.kota_kabupaten +''+
                        '</p>'+
                        '<p class="fs-13"><a href="'+ v.link_peta +'" target="_blank"><b>Open In Map</b></a></p>'+
                        '</div>';

                });
                $('#jaringan-list').html(html);
              }
          });

        });

      /*--------------------- End Set Variable Untuk Ambil Data List Jaringan -------------------- */    



      /*--------------------- Begin Set Marker -------------------- */
        var icon = {
              url: "<?= base_url() ?>assets/frontend/img/icon_marker/icon_marker.png?>", // url
              scaledSize: new google.maps.Size(40, 40), // scaled size
              origin: new google.maps.Point(0,0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
          };

        function addMarker(lat, lng,info) {
          
          var lokasi        = new google.maps.LatLng(lat,lng);
          bounds.extend(lokasi);
          var marker = new google.maps.Marker({
            map: map,
            position: lokasi,
            icon: icon
          });
          markers.push(marker);

          markerCluster.addMarker(marker);

          // map.setMap(null);
          map.fitBounds(bounds);
          map.setZoom(10);
          bindInfoWindow(marker, map, infoWindow, info);
         }
  

        function bindInfoWindow(marker, map, infoWindow, html) {
            google.maps.event.addListener(marker, 'click', function(event) {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
            map.setZoom(17);
            map.setCenter(marker.getPosition());
            map.panTo(marker.getPosition());
          });
        }

        var clusterStyles = [
          {
            textColor: 'white',
            url: '{{base_url()}}assets/frontend/img/cluster/icon-cluster.png',
            height:65,
            width:65,
            textSize: 14
          },
          {
            textColor: 'white',
            url: '{{base_url()}}assets/frontend/img/cluster/icon-cluster.png',
            height:65,
            width:65,
            textSize: 14
          },
          {
            textColor: 'white',
            url: '{{base_url()}}assets/frontend/img/cluster/icon-cluster.png',
            height:65,
            width:65,
            textSize: 14
          },
          {
            textColor: 'white',
            url: '{{base_url()}}assets/frontend/img/cluster/icon-cluster.png',
            height:65,
            width:65,
            textSize: 14
          },
          {
            textColor: 'white',
            url: '{{base_url()}}assets/frontend/img/cluster/icon-cluster.png',
            height:65,
            width:65,
            textSize: 14
          },
        ];

        map.addListener('click', function(event) {
            addMarker(event.marker);
        });

        markerCluster = new MarkerClusterer(map, markers, {
          maxZoom: 11,
          styles: clusterStyles
        });

        var lokasi;

        $(document).on('click', '.cobadah', function(event) {

          clearMarkers();
          event.preventDefault();
          var h6     = $(this);
          var info_  = String($(this).prev().text());
          var longi  = parseFloat($(this).prev().prev().text());
          var lati   = parseFloat($(this).prev().prev().prev().text());
          var lokasi_ = new google.maps.LatLng(lati,longi);

          bounds.extend(lokasi_);
          var marker_ = new google.maps.Marker({
            map: map,
            position: lokasi_,
            icon: icon
          });
          markers.push(marker_);
          map.fitBounds(bounds);
          map.setZoom(10);

          bindInfoWindow(marker_, map, infoWindow, info_);
        });
      }
    /*--------------------- End Set Marker -------------------- */

     // Sets the map on all markers in the array.
      function setMapOnAll(map) {
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
          }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
          setMapOnAll(null);
          markerCluster.clearMarkers();
      }

      // Shows any markers currently in the array.
      function showMarkers() {
          setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
          clearMarkers();
          markers = [];
      }

    </script>
    <!-- END JS MARKER MAPPING -->

    <script type="text/javascript">

        function peta($peta){
          event.preventDefault();
          $('#peta').html($peta);
          $('#m_modal_1').modal('show');
        }

        $("#img-thumn a").on('click', function(e) {
            e.preventDefault();

            $('#preview img').attr('src', $(this).attr('href'));
        });

        $('.aksesoris').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            stageOuterClass: 'owl-stage-outer outer-stage',
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'nav-container',
            navClass: [ 'prev', 'next' ],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:6
                }
            }
        });
        
        // Set For Search Form
        var get_news        = "{{base_url()}}ajax_news_filter";
        var count_news_page = "{{base_url()}}total_page_news";
        var totalPageAll    = "{{$total_page_all}}";

        var listNewsAjax = function() {

            var demo = function(searchData="", totalPagesAll="") {

                if(totalPagesAll != "") {
                    var totalAllPage = totalPagesAll;
                } else {
                    var totalAllPage = totalPageAll;
                }

                $('#paginationAll').twbsPagination({
                    totalPages: totalAllPage,
                    onPageClick: function(evt, page) {

                        $.ajax({
                            type: 'POST',
                            url: get_news,
                            data: {page: page, search: searchData},
                            dataType: 'json',
                            success: function(data) {

                                $('#contentAll').html('');

                                $.each(data.data, function(i, v) {

                                    // Set Tanggal Publikasi
                                    var date = new Date(v.tanggal_publikasi);
                                    
                                    // Set Judul Berita
                                    if ((v.judul_berita).length > 60) {
                                        var str   = v.judul_berita;
                                        var judul = str.substring(0,60)+"...";                          
                                    }else{
                                        var judul = v.judul_berita;
                                    }

                                    // Set Definisi Kategori Berita
                                    if(v.kategori_berita == 'B')
                                    {
                                        var kategori_berita = 'Berita';
                                    }
                                    else if(v.kategori_berita == 'E')
                                    {
                                        var kategori_berita = 'Event';
                                    }
                                    else
                                    {
                                        var kategori_berita = 'Tips';
                                    }

                                    /* Set Display Content */
                                    $('#contentAll').append('<div class="col-sm-4 bt-spc list-news">'+
                                                                '<a href="{{base_url()}}blog/'+v.judul_berita_url+'" title="All News">'+
                                                                    '<div class="row align-items-center news-mid-over p-2">'+
                                                                        '<div class="col-sm-12 col-5 pr-0 pl-0 img-over">'+
                                                                            '<img src="{{ base_url() }}assets/upload/berita/'+v.gambar_berita+'" class="img-fluid" alt="'+judul+'" title="'+judul+'">'+
                                                                        '</div>'+
                                                                        '<div class="col-sm-12 col-7 news-over">'+
                                                                            '<div class="content-news content-news-over">'
                                                                                +judul+ '</br>' + 
                                                                            'Kategori Berita: '+ kategori_berita + 
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</a>'+
                                                            '</div>');   
                                });
                            }
                        });
                    }
                });
            };

            return {
                // public functions
                init: function(searchData="", totalPagesAll="") {
                    demo(searchData, totalPagesAll);
                },
            };
        }();

        jQuery(document).ready(function() {
            
            listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */ 
        });
    </script>
@stop