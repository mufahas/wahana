@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Wahana Honda | Dealer</title>
@endsection

@section('body')

    <div class="container-fluid bg-feature">
        <div class="container text-center">
            <h2 class="text-white">Dealers Wahana Honda</h2>
        </div>
    </div>
    <div class="row">
      <div class="col-md-3">  
        <div class="container mt-4">
          <div class="row justify-content-md-center">
              <div class="col-md-12 text-center">
                  <img src="{{ base_url() }}assets/frontend/img/Jaringan-1.png" alt="">
              </div>
          </div>
        </div>
        <div class="container mt-4 mb-5">
          <form class="" action="index.html" method="post">
              <div class="row justify-content-md-center">
                  <div class="col-md-10">
                      <div class="form-group">
                          <label>Silahkan pilih area</label>
                          <select class="custom-select filter-jaringan" name="area" id="inputGroupSelect01">
                              <option value="">Semua area</option>
                              @if(!empty($select_area))
                              @foreach($select_area as $vselect_area)
                              <option value="{{$vselect_area->kode_kota_kabupaten}}">{{$vselect_area->nama_kota_kabupaten}}</option>
                              @endforeach
                              @endif
                          </select>
                      </div>
                  </div>
              </div>
              <div class="row justify-content-md-center display-none" id="filterPelayanan" style="padding-top: 10px;">
                <div class="col-md-10">
                  <button class="btn btn-danger btn-block fs-13 active" id="h1">H1 - PENJUALAN</button>
                </div>
                <div class="col-md-10 pt-1">
                  <button class="btn btn-danger btn-block fs-13" id="h2">H2 - PEMELIHARAAN</button>
                </div>
                <div class="col-md-10 pt-1">
                  <button class="btn btn-danger btn-block fs-13" id="h3">H3 - SUKU CADANG</button>
                </div>
              </div>
              <br>
              
              <div class="row justify-content-md-center" style="overflow: auto; height:400px;" id="jaringan-list">
              
              </div>
          </form>
        </div>
          <!-- <div class="row col-md-12 p-5" id="listDealer"> -->
            <!-- @if(!empty($jaringan))
              @foreach($jaringan as $vjaringan)
                <div class="col-md-3 pb-5 d-flex flex-column" align="center">
                  <div>
                    <h5><b>{{$vjaringan->nama_dealer}}</b></h5>
                    <p class="fs-13">
                      {{$vjaringan->alamat_jaringan}}<br>
                      <a href="tel:{{$vjaringan->nomor_telepon}}" target="_blank">{{$vjaringan->nomor_telepon}}</a><br>
                      {{$vjaringan->nama_kota_kabupaten}}
                    </p>
                  </div>
                </div>
              @endforeach
            @endif -->
          <!-- </div> -->

          
      </div>

      <div class="col-md-9">
        <div id="map-canvas" class="col-sm-12" style="height:800px;"></div>
      </div>
    </div>
    <!-- <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog modal-custom" role="document">
        <div class="modal-content transparant-custom no-border-custom">
          <div class="modal-header transparant-custom no-border-custom">
            <h5 class="modal-title" id="exampleModalLabel">
              Peta
            </h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">
                ×
              </span>
            </button>
          </div>
          <div class="modal-body" align="center">
            <div id="peta"></div>
          </div>
          <div class="modal-footer transparant-custom no-border-custom">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
              Close
            </button>
            <button type="button" class="btn btn-primary">
              Save changes
            </button>
          </div>
        </div>
      </div>
    </div> -->

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    <script src="{{ base_url() }}assets/frontend/js/dealers/markerclusterer.js"></script>
    <!--<script async defer src="https://maps.googleapis.com/maps/api/js?&libraries=places&callback=initialize"></script>-->
     <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOv9JAHiAWZ34Of1vlN5g1enIb0FWr2qU&libraries=places&callback=initialize" type="text/javascript"></script> 
    <!-- BEGIN JS MARKER MAPPING -->
    <script type="text/javascript">
      // document.addEventListener('contextmenu', event => event.preventDefault());
      //  $(document).keydown(function (event) {
      //     if (event.keyCode == 123) { 
      //          return false;
      //      } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {      
      //         return false;
      //      }
      //  });
      var marker;
      var markers= [];
      var markerCluster;
      var infowindow = null;
      var map = null;
      // var addMarker='';
      var icon = '';

      function initialize()
      {
        
        /*--------------------- Begin Set Variable Untuk Setting Marker Google Maps -------------------- */
          var infoWindow      = new google.maps.InfoWindow;                           
          var myLatlng        = {lat: -6.229728, lng: 106.6894294};                   
          var mapOptions      = {mapTypeId: google.maps.MapTypeId.ROADMAP,
          zoom: 10,
          center: new google.maps.LatLng(myLatlng)
          };
          var map             = new google.maps.Map(document.getElementById('map-canvas'), mapOptions); 
          var bounds          = new google.maps.LatLngBounds(); 
      
        /*--------------------- End Set Variable Untuk Setting Marker Google Maps -------------------- */
        
        
        /*--------------------- Begin Set Variable Untuk Ambil Data List Jaringan -------------------- */
        var getListJaringan = "{{base_url()}}jaringan/jaringan/ajax_get_jaringan"; 
        var area_           = $('select[name="area"]').val(); 
        var h1              = $('#h1').val();
        var h2              = $('#h2').val();
        var h3              = $('#h3').val();
        var data            = '';

        if(h1 != '')
        {
          var data  = {area: area_, pelayanan: h1};
        }

        if(h2 != '')
        {
          var data  = {area: area_, pelayanan: h1};
        }

        if(h3 != '')
        {
          var data  = {area: area_, pelayanan: h3};
        }

        $.ajax({
          type: 'POST',
          url: getListJaringan,
          data: data,
          dataType: 'json',
          cache: false,
          success: function(result) {
            clearMarkers();
            var html='';
            $.each(result, function(i,v){

                  var kategori_jaringan = v.kategori_jaringan;

                  if(kategori_jaringan == 'H1')
                  {
                    var katJar = 'PENJUALAN'; 
                  } 
                  else if(kategori_jaringan == 'H2')
                  {
                    var katJar = 'PEMELIHARAAN'; 
                  } 
                  else
                  {
                    var katJar = 'SUKU CADANG';
                  }

                  var info = v.nama_dealer + '<br>' + v.alamat_dealer + '<br>' + katJar;
                  var lat  = parseFloat(v.latitude);
                  var lng  = parseFloat(v.longitude);

                  if(lat != '' && lat != undefined && lng != '' && lng != undefined)
                  {
                    addMarker(lat, lng, info);
                  }

                  html += '<div class="col-md-10" align="left" style="border-top: 1px solid grey; padding-top: 10px;">' +
                          '<span class="latitude" style="display: none;">'+ lat +'</span>'+
                          '<span class="longitude" style="display: none;">'+ lng +'</span>'+
                          '<span class="info_" style="display: none;">'+ v.nama_dealer +'</span>'+
                          '<h6 class="cobadah"><b><a href="#" class="">'+v.nama_dealer+'</a></b></h6>'+
                          '<p class="fs-13">'+
                          ''+ v.alamat_dealer +'<br>'+
                          '<a href="tel:'+ v.no_telepon +'" target="_blank">'+ v.no_telepon +'</a><br>'+
                          ''+ v.kota_kabupaten +''+
                          '</p>'+
                          '</div>';
            });
            $('#jaringan-list').html(html);
          }
        });

        /* BEGIN SET ON CHANGE AREA  */
        $('select[name="area"]').change(function(){
          clearMarkers();
          $('#filterPelayanan').removeClass('display-none');
            var area_ = $(this).val();
            var h1_   = $('#h1').val();
            var h2_   = $('#h2').val();
            var h3_   = $('#h3').val();
            var data_ = '';

            if(area_ != '')
            {
              var data_ = {area: area_, pelayanan: h1};
            }

            $.ajax({
              type: 'POST',
              url: getListJaringan,
              data: data_,
              dataType: 'json',
              cache: false,
              success: function(result) {
                var html  = '';
                $.each(result, function(i,v) {

                  var kategori_jaringan = v.kategori_jaringan;

                  if(kategori_jaringan == 'H1')
                  {
                    var katJar = 'PENJUALAN'; 
                  } 
                  else if(kategori_jaringan == 'H2')
                  {
                    var katJar = 'PEMELIHARAAN'; 
                  } 
                  else
                  {
                    var katJar = 'SUKU CADANG';
                  }

                  var info = v.nama_dealer + '<br>' + v.alamat_dealer + '<br>' + katJar;
                  var lat  = parseFloat(v.latitude);
                  var lng  = parseFloat(v.longitude);

                  if(lat != '' && lat != undefined && lng != '' && lng != undefined)
                  {
                    addMarker(lat, lng, info);
                  }

                  html += '<div class="col-md-10" align="left" style="border-top: 1px solid grey; padding-top: 10px;">' +
                          '<span class="latitude" style="display: none;">'+ lat +'</span>'+
                          '<span class="longitude" style="display: none;">'+ lng +'</span>'+
                          '<span class="info_" style="display: none;">'+ v.nama_dealer +'</span>'+
                          '<h6 class="cobadah"><b><a href="#" class="">'+v.nama_dealer+'</a></b></h6>'+
                          '<p class="fs-13">'+
                          ''+ v.alamat_dealer +'<br>'+
                          '<a href="tel:'+ v.no_telepon +'" target="_blank">'+ v.no_telepon +'</a><br>'+
                          ''+ v.kota_kabupaten +''+
                          '</p>'+
                          '</div>';
                });
                $('#jaringan-list').html(html);
              }
            });
            /* END SET ON CHANGE AREA  */

            /* BEGIN SET ON CHANGE PELAYANAN PENJUALAN */
            $('#h1').click(function() {
              event.preventDefault();
              clearMarkers();
              $('#h1').addClass('active');
              $('#h2').removeClass('active');
              $('#h3').removeClass('active');

              var areah1      = $('select[name="area"]').val(); 
              var pelayananh1 = "H1";
              var datah1      = {area: areah1, pelayanan: pelayananh1};

              $.ajax({
                type: 'POST',
                url: getListJaringan,
                data: datah1,
                dataType: 'json',
                cache: false,
                success: function(result) {
                  var html        ='';
                  $.each(result, function(i,v) {
                    var kategori_jaringan = v.kategori_jaringan;

                    if(kategori_jaringan == 'H1')
                    {
                      var katJar = 'PENJUALAN'; 
                    } 
                    else if(kategori_jaringan == 'H2')
                    {
                      var katJar = 'PEMELIHARAAN'; 
                    } 
                    else
                    {
                      var katJar = 'SUKU CADANG';
                    }

                    var info = v.nama_dealer + '<br>' + v.alamat_dealer + '<br>' + katJar;
                    var lat  = parseFloat(v.latitude);
                    var lng  = parseFloat(v.longitude);

                    if(lat != '' && lat != undefined && lng != '' && lng != undefined)
                    {
                      addMarker(lat, lng, info);
                    }

                   html += '<div class="col-md-10" align="left" style="border-top: 1px solid grey; padding-top: 10px;">' +
                          '<span class="latitude" style="display: none;">'+ lat +'</span>'+
                          '<span class="longitude" style="display: none;">'+ lng +'</span>'+
                          '<span class="info_" style="display: none;">'+ v.nama_dealer +'</span>'+
                          '<h6 class="cobadah"><b><a href="#" class="">'+v.nama_dealer+'</a></b></h6>'+
                          '<p class="fs-13">'+
                          ''+ v.alamat_dealer +'<br>'+
                          '<a href="tel:'+ v.no_telepon +'" target="_blank">'+ v.no_telepon +'</a><br>'+
                          ''+ v.kota_kabupaten +''+
                          '</p>'+
                          '</div>';
                });
                $('#jaringan-list').html(html);
                }
              });
            });
            /* END SET ON CHANGE PELAYANAN PENJUALAN */

            /* BEGIN SET ON CHANGE PELAYANAN PEMELIHARAN */
            $('#h2').click(function(){
              event.preventDefault();
              clearMarkers();
              $('#h1').removeClass('active');
              $('#h2').addClass('active');
              $('#h3').removeClass('active');

              var areah2      = $('select[name="area"]').val(); 
              var pelayananh2 = "H2";
              var datah2      = {area: areah2, pelayanan: pelayananh2};

              $.ajax({
                type: 'POST',
                url: getListJaringan,
                data: datah2,
                dataType: 'json',
                cache: false,
                success: function(result) {
                  var html        = '';
                  $.each(result, function(i,v) {
                    var kategori_jaringan = v.kategori_jaringan;

                    if(kategori_jaringan == 'H1')
                    {
                      var katJar = 'PENJUALAN'; 
                    } 
                    else if(kategori_jaringan == 'H2')
                    {
                      var katJar = 'PEMELIHARAAN'; 
                    } 
                    else
                    {
                      var katJar = 'SUKU CADANG';
                    }

                    var info = v.nama_dealer + '<br>' + v.alamat_dealer + '<br>' + katJar;
                    var lat  = parseFloat(v.latitude);
                    var lng  = parseFloat(v.longitude);

                    if(lat != '' && lat != undefined && lng != '' && lng != undefined)
                    {
                      addMarker(lat, lng, info);
                    }

                    html += '<div class="col-md-10" align="left" style="border-top: 1px solid grey; padding-top: 10px;">' +
                          '<span class="latitude" style="display: none;">'+ lat +'</span>'+
                          '<span class="longitude" style="display: none;">'+ lng +'</span>'+
                          '<span class="info_" style="display: none;">'+ v.nama_dealer +'</span>'+
                          '<h6 class="cobadah"><b><a href="#" class="">'+v.nama_dealer+'</a></b></h6>'+
                          '<p class="fs-13">'+
                          ''+ v.alamat_dealer +'<br>'+
                          '<a href="tel:'+ v.no_telepon +'" target="_blank">'+ v.no_telepon +'</a><br>'+
                          ''+ v.kota_kabupaten +''+
                          '</p>'+
                          '</div>';
                });
                $('#jaringan-list').html(html);
                }
              });

            });
            /* END SET ON CHANGE PELAYANAN PEMELIHARAN */

            /* BEGIN SET ON CHANGE PELAYANAN SUKU CADANG */
            $('#h3').click(function() {
              event.preventDefault();
              clearMarkers();
              $('#h1').removeClass('active');
              $('#h2').removeClass('active');
              $('#h3').addClass('active');

              var areah3      = $('select[name="area"]').val(); 
              var pelayananh3 = "H3";
              var datah3      = {area: areah3, pelayanan: pelayananh3};

              $.ajax({
                type: 'POST',
                url: getListJaringan,
                data: datah3,
                dataType: 'json',
                cache: false,
                success: function(result) {
                  var html        = '';
                  $.each(result, function(i,v) {
                  var kategori_jaringan = v.kategori_jaringan;

                    if(kategori_jaringan == 'H1')
                    {
                      var katJar = 'PENJUALAN'; 
                    } 
                    else if(kategori_jaringan == 'H2')
                    {
                      var katJar = 'PEMELIHARAAN'; 
                    } 
                    else
                    {
                      var katJar = 'SUKU CADANG';
                    }

                    var info = v.nama_dealer + '<br>' + v.alamat_dealer + '<br>' + katJar;
                    var lat  = parseFloat(v.latitude);
                    var lng  = parseFloat(v.longitude);

                    if(lat != '' && lat != undefined && lng != '' && lng != undefined)
                    { 
                      addMarker(lat, lng, info);
                    }
                    html += '<div class="col-md-10" align="left" style="border-top: 1px solid grey; padding-top: 10px;">' +
                          '<span class="latitude" style="display: none;">'+ lat +'</span>'+
                          '<span class="longitude" style="display: none;">'+ lng +'</span>'+
                          '<span class="info_" style="display: none;">'+ v.nama_dealer +'</span>'+
                          '<h6 class="cobadah"><b><a href="#" class="">'+v.nama_dealer+'</a></b></h6>'+
                          '<p class="fs-13">'+
                          ''+ v.alamat_dealer +'<br>'+
                          '<a href="tel:'+ v.no_telepon +'" target="_blank">'+ v.no_telepon +'</a><br>'+
                          ''+ v.kota_kabupaten +''+
                          '</p>'+
                          '</div>';
                });
                $('#jaringan-list').html(html);
                }
              });
            });
            /* END SET ON CHANGE PELAYANAN SUKU CADANG */
        });
      /*--------------------- End Set Variable Untuk Ambil Data List Jaringan -------------------- */    



      /*--------------------- Begin Set Marker -------------------- */
        var icon = {
              url: "<?= base_url() ?>assets/frontend/img/icon_marker/icon_marker.png?>", // url
              scaledSize: new google.maps.Size(40, 40), // scaled size
              origin: new google.maps.Point(0,0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
          };

        function addMarker(lat, lng,info) {
          
          var lokasi        = new google.maps.LatLng(lat,lng);
          bounds.extend(lokasi);
          var marker = new google.maps.Marker({
            map: map,
            position: lokasi,
            icon: icon
          });
          markers.push(marker);

          markerCluster.addMarker(marker);

          // map.setMap(null);
          map.fitBounds(bounds);
          map.setZoom(10);
          bindInfoWindow(marker, map, infoWindow, info);
         }
  

        function bindInfoWindow(marker, map, infoWindow, html) {
            google.maps.event.addListener(marker, 'click', function(event) {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
            map.setZoom(17);
            map.setCenter(marker.getPosition());
            map.panTo(marker.getPosition());
          });
        }

        var clusterStyles = [
          {
            textColor: 'white',
            url: '{{base_url()}}assets/frontend/img/cluster/icon-cluster.png',
            height:65,
            width:65
          },
          {
            textColor: 'white',
            url: '{{base_url()}}assets/frontend/img/cluster/icon-cluster.png',
            height:65,
            width:65
          },
          {
            textColor: 'white',
            url: '{{base_url()}}assets/frontend/img/cluster/icon-cluster.png',
            height:65,
            width:65
          },
          {
            textColor: 'white',
            url: '{{base_url()}}assets/frontend/img/cluster/icon-cluster.png',
            height:65,
            width:65
          },
          {
            textColor: 'white',
            url: '{{base_url()}}assets/frontend/img/cluster/icon-cluster.png',
            height:65,
            width:65
          },
        ];

        map.addListener('click', function(event) {
            addMarker(event.marker);
        });

        markerCluster = new MarkerClusterer(map, markers, {
          maxZoom: 11,
          styles: clusterStyles
        });

        var lokasi;

        $(document).on('click', '.cobadah', function(event) {

          clearMarkers();
          event.preventDefault();
          var h6     = $(this);
          var info_  = String($(this).prev().text());
          var longi  = parseFloat($(this).prev().prev().text());
          var lati   = parseFloat($(this).prev().prev().prev().text());
          var lokasi_ = new google.maps.LatLng(lati,longi);

          bounds.extend(lokasi_);
          var marker_ = new google.maps.Marker({
            map: map,
            position: lokasi_,
            icon: icon
          });
          markers.push(marker_);
          map.fitBounds(bounds);
          map.setZoom(10);

          bindInfoWindow(marker_, map, infoWindow, info_);
        });
      }
    /*--------------------- End Set Marker -------------------- */

     // Sets the map on all markers in the array.
      function setMapOnAll(map) {
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
          }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
          setMapOnAll(null);
          markerCluster.clearMarkers();
      }

      // Shows any markers currently in the array.
      function showMarkers() {
          setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
          clearMarkers();
          markers = [];
      }

    </script>
    <!-- END JS MARKER MAPPING -->

    <script type="text/javascript">

        function peta($peta){
          event.preventDefault();
          $('#peta').html($peta);
          $('#m_modal_1').modal('show');
        }

        $("#img-thumn a").on('click', function(e) {
            e.preventDefault();

            $('#preview img').attr('src', $(this).attr('href'));
        });

        $('.aksesoris').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            stageOuterClass: 'owl-stage-outer outer-stage',
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'nav-container',
            navClass: [ 'prev', 'next' ],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:6
                }
            }
        })
    </script>
@stop