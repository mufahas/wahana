@extends('frontend.default.views.layout.v_layout')

@section('head')
<title>Wahana Honda | Dealer</title>
@endsection

@section('body')

    <div class="container-fluid bg-feature">
        <div class="container text-center pt-5 pb-5">
            <h2 class="text-white">Dealers Wahana Honda</h2>
        </div>
    </div>
    <div class="container mt-4">
        <div class="row justify-content-md-center">
            <div class="col-md-4 text-center">
                <img src="{{ base_url() }}assets/frontend/img/Jaringan-1.png" alt="">
            </div>
        </div>
    </div>
    <div class="container mt-4 mb-5">
        <form class="" action="index.html" method="post">
            <div class="row justify-content-md-center">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Silahkan pilih area</label>
                        <select class="custom-select filter-jaringan" name="area" id="inputGroupSelect01">
                            <option value="">Semua area</option>
                            @if(!empty($select_area))
                            @foreach($select_area as $vselect_area)
                            <option value="{{$vselect_area->kode_kota_kabupaten}}">{{$vselect_area->nama_kota_kabupaten}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="row justify-content-md-center display-none" id="filterPelayanan">
              <div class="col-md-2">
                <button class="btn btn-danger btn-block fs-13 active" id="h1">H1 - PENJUALAN</button>
              </div>
              <div class="col-md-2">
                <button class="btn btn-danger btn-block fs-13" id="h2">H2 - PEMELIHARAAN</button>
              </div>
              <div class="col-md-2">
                <button class="btn btn-danger btn-block fs-13" id="h3">H3 - SUKU CADANG</button>
              </div>
            </div>
            <!-- <div class="row justify-content-md-center">
                <div class="col-md-3">
                  <div class="form-group">
                      <label>Pilih pelayanan anda</label>
                      <select class="custom-select filter-jaringan" name="pelayanan" id="inputGroupSelect01">
                          <option value="">Semua pelayanan</option>
                          <option value="H1">H1 - Penjualan</option>
                          <option value="H2">H2 - Pemeliharaan</option>
                          <option value="H3">H3 - Suku Cadang</option>
                      </select>
                  </div>
                </div>
              </div> -->
        </form>
        
        <div class="row col-md-12 p-5" id="listDealer">
          <!-- @if(!empty($jaringan))
            @foreach($jaringan as $vjaringan)
              <div class="col-md-3 pb-5 d-flex flex-column" align="center">
                <div>
                  <h5><b>{{$vjaringan->nama_dealer}}</b></h5>
                  <p class="fs-13">
                    {{$vjaringan->alamat_jaringan}}<br>
                    <a href="tel:{{$vjaringan->nomor_telepon}}" target="_blank">{{$vjaringan->nomor_telepon}}</a><br>
                    {{$vjaringan->nama_kota_kabupaten}}
                  </p>
                </div>
                <div class="mt-auto">
                  <a href="{{$vjaringan->link_peta}}" class="btn btn-danger btn-block fs-13" target="_blank">View Map</a>
                </div>
              </div>
            @endforeach
          @endif -->
        </div>
        
    </div>

    <!-- <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog modal-custom" role="document">
        <div class="modal-content transparant-custom no-border-custom">
          <div class="modal-header transparant-custom no-border-custom">
            <h5 class="modal-title" id="exampleModalLabel">
              Peta
            </h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">
                ×
              </span>
            </button>
          </div>
          <div class="modal-body" align="center">
            <div id="peta"></div>
          </div>
          <div class="modal-footer transparant-custom no-border-custom">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
              Close
            </button>
            <button type="button" class="btn btn-primary">
              Save changes
            </button>
          </div>
        </div>
      </div>
    </div> -->

@stop

@section('scripts')
    <script src="{{ base_url() }}assets/frontend/js/thumb.js"></script>
    
    <script type="text/javascript">

      var filterJaringanUrl      = "{{base_url()}}jaringan/jaringan/filterJaringan";

      $('.filter-jaringan').change(function(){
        $('#filterPelayanan').removeClass('display-none');
        var area      = $('select[name="area"]').val();
        var pelayanan = 'h1';
        // var pelayanan = $('select[name="pelayanan"]').val();
        $.ajax({
            type    : 'POST',
            url     : filterJaringanUrl,
            data    : {area : area, pelayanan : pelayanan},
            dataType: "html",
            cache   : false,
            success : function(response){
                $("#listDealer").html(response);
            }
        });
      });

      $('#h1').click(function(){
        event.preventDefault();
        $('#h1').addClass('active');
        $('#h2').removeClass('active');
        $('#h3').removeClass('active');

        var area      = $('select[name="area"]').val();
        var pelayanan = 'H1';
        $.ajax({
            type    : 'POST',
            url     : filterJaringanUrl,
            data    : {area : area, pelayanan : pelayanan},
            dataType: "html",
            cache   : false,
            success : function(response){
                $("#listDealer").html(response);
            }
        });
      });

      $('#h2').click(function(){
        event.preventDefault();
        $('#h1').removeClass('active');
        $('#h2').addClass('active');
        $('#h3').removeClass('active');

        var area      = $('select[name="area"]').val();
        var pelayanan = 'H2';
        $.ajax({
            type    : 'POST',
            url     : filterJaringanUrl,
            data    : {area : area, pelayanan : pelayanan},
            dataType: "html",
            cache   : false,
            success : function(response){
                $("#listDealer").html(response);
            }
        });
      });

      $('#h3').click(function(){
        event.preventDefault();
        $('#h1').removeClass('active');
        $('#h2').removeClass('active');
        $('#h3').addClass('active');
        var area      = $('select[name="area"]').val();
        var pelayanan = 'H3';
        $.ajax({
            type    : 'POST',
            url     : filterJaringanUrl,
            data    : {area : area, pelayanan : pelayanan},
            dataType: "html",
            cache   : false,
            success : function(response){
                $("#listDealer").html(response);
            }
        });
      });
    </script>

    <script type="text/javascript">

        function peta($peta){
          event.preventDefault();
          $('#peta').html($peta);
          $('#m_modal_1').modal('show');
        }

        $("#img-thumn a").on('click', function(e) {
            e.preventDefault();

            $('#preview img').attr('src', $(this).attr('href'));
        });

        $('.aksesoris').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            stageOuterClass: 'owl-stage-outer outer-stage',
            navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
            navContainerClass: 'nav-container',
            navClass: [ 'prev', 'next' ],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:6
                }
            }
        })

    </script>
@stop