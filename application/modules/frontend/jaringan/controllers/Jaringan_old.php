<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jaringan extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();
    }

    function index() 
    {
        $data['select_area'] = ms_jaringan_wahana::select('ms_kota_kabupaten.kode_kota_kabupaten','ms_kota_kabupaten.nama_kota_kabupaten')->join('ms_kota_kabupaten','ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')->where('ms_jaringan_wahana.dihapus','F')->groupBy('ms_kota_kabupaten.kode_kota_kabupaten')->orderBy('ms_kota_kabupaten.nama_kota_kabupaten','ASC')->get();

        $data['jaringan'] = ms_jaringan_wahana::join('ms_kota_kabupaten','ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')->where('ms_jaringan_wahana.dihapus','F')->orderBy('ms_kota_kabupaten.nama_kota_kabupaten','ASC')->get();

        $this->load_view_frontend("frontend","jaringan","jaringan","v_jaringan",$data);
    }

    function filterJaringan()
    {

        $area      = $this->input->post('area');
        $pelayanan = $this->input->post('pelayanan');
        $where     = "ms_jaringan_wahana.dihapus = 'F'";
        if (!empty($area) OR !empty($pelayanan)) {
            $where .= ' AND ';
        }
        if (!empty($area)) {
            $where .= 'ms_jaringan_wahana.kode_kota_kabupaten = "'.$area.'"';
        }
        if (!empty($area) AND !empty($pelayanan)) {
            $where .= ' AND ';
        }
        if (!empty($pelayanan)) {
            $where .= 'tbl_jaringan_wahana_detail.kategori_jaringan = "'.$pelayanan.'"';
        }

        $jaringan = ms_jaringan_wahana::selectRaw("ms_jaringan_wahana.kode_jaringan, ms_kota_kabupaten.nama_kota_kabupaten, tbl_jaringan_wahana_detail.kategori_jaringan, ms_jaringan_wahana.nama_dealer, ms_jaringan_wahana.nomor_telepon, ms_jaringan_wahana.alamat_jaringan, ms_jaringan_wahana.link_peta")->join('tbl_jaringan_wahana_detail','tbl_jaringan_wahana_detail.kode_jaringan','=','ms_jaringan_wahana.kode_jaringan')->join('ms_kota_kabupaten','ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')->whereRaw($where)->groupBy('ms_jaringan_wahana.kode_jaringan')->orderBy('ms_kota_kabupaten.nama_kota_kabupaten','ASC')->get();

        $list_dealer = "";
        if ($jaringan != "[]") {
            foreach ($jaringan as $key => $value) {
                $list_dealer .= '<div class="col-md-3 pb-5 d-flex flex-column" align="center">
                                    <div>
                                      <h5><b>'.$value->nama_dealer.'</b></h5>
                                      <p class="fs-13">
                                        '.$value->alamat_jaringan.'<br>
                                        <a href="tel:'.$value->nomor_telepon.'" target="_blank">'.$value->nomor_telepon.'</a><br>
                                        '.$value->nama_kota_kabupaten.'
                                      </p>
                                    </div>
                                    <div class="mt-auto">
                                      <a href="'.$value->link_peta.'" class="btn btn-danger btn-block fs-13" target="_blank">View Map</a>
                                    </div>
                                </div>';
            }
        }

        echo $list_dealer;
    }

}