<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jaringan extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();

        /* Load Model */
        $this->load->model('select_global_model');
    }

    function index() 
    {
        $data['select_area']        = ms_jaringan_wahana::select('ms_kota_kabupaten.kode_kota_kabupaten','ms_kota_kabupaten.nama_kota_kabupaten')->join('ms_kota_kabupaten','ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')->where('ms_jaringan_wahana.dihapus','F')->groupBy('ms_kota_kabupaten.kode_kota_kabupaten')->orderBy('ms_kota_kabupaten.nama_kota_kabupaten','ASC')->get();

        $data['jaringan']           = ms_jaringan_wahana::join('ms_kota_kabupaten','ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')->where('ms_jaringan_wahana.dihapus','F')->orderBy('ms_kota_kabupaten.nama_kota_kabupaten','ASC')->get();

        /* For Search */
        $where                         = "tbl_berita.status_publikasi = 'T'";
        $count_all                     = tbl_berita::selectRaw("tbl_berita.kode_berita")
                                         ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                                         ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                         ->whereRaw($where)
                                         ->groupBy('tbl_berita.kode_berita')
                                         ->get();
        $all = 0;
        foreach ($count_all as $value) {
            $all++;
        }
        $data['total_page_all'] = ceil($all / 12);
        
        $this->load_view_frontend("frontend","jaringan","jaringan","v_jaringan",$data);
    }

    function filterJaringan()
    {

        $area      = $this->input->post('area');
        $pelayanan = $this->input->post('pelayanan');
        $where     = "ms_jaringan_wahana.dihapus = 'F'";
        if (!empty($area) OR !empty($pelayanan)) {
            $where .= ' AND ';
        }
        if (!empty($area)) {
            $where .= 'ms_jaringan_wahana.kode_kota_kabupaten = "'.$area.'"';
        }
        if (!empty($area) AND !empty($pelayanan)) {
            $where .= ' AND ';
        }
        if (!empty($pelayanan)) {
            $where .= 'tbl_jaringan_wahana_detail.kategori_jaringan = "'.$pelayanan.'"';
        }

        $jaringan = ms_jaringan_wahana::selectRaw("ms_jaringan_wahana.kode_jaringan, ms_kota_kabupaten.nama_kota_kabupaten, tbl_jaringan_wahana_detail.kategori_jaringan, ms_jaringan_wahana.nama_dealer, ms_jaringan_wahana.nomor_telepon, ms_jaringan_wahana.alamat_jaringan, ms_jaringan_wahana.link_peta")->join('tbl_jaringan_wahana_detail','tbl_jaringan_wahana_detail.kode_jaringan','=','ms_jaringan_wahana.kode_jaringan')->join('ms_kota_kabupaten','ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')->whereRaw($where)->groupBy('ms_jaringan_wahana.kode_jaringan')->orderBy('ms_kota_kabupaten.nama_kota_kabupaten','ASC')->get();

        $list_dealer = "";
        if ($jaringan != "[]") {
            foreach ($jaringan as $key => $value) {
                $list_dealer .= '<div class="col-md-3 pb-5 d-flex flex-column" align="center">
                                    <div>
                                      <h5><b>'.$value->nama_dealer.'</b></h5>
                                      <p class="fs-13">
                                        '.$value->alamat_jaringan.'<br>
                                        <a href="tel:'.$value->nomor_telepon.'" target="_blank">'.$value->nomor_telepon.'</a><br>
                                        '.$value->nama_kota_kabupaten.'
                                      </p>
                                    </div>
                                    <div class="mt-auto">
                                      <a href="'.$value->link_peta.'" class="btn btn-danger btn-block fs-13" target="_blank">View Map</a>
                                    </div>
                                </div>';
            }
        }

        echo $list_dealer;
    }

    // function ajax_get_jaringan()
    // {
    //     $area           = $this->input->post('area');
    //     $pelayanan      = $this->input->post('pelayanan');

    //     $list_jaringan  = $this->select_global_model->getJaringanBasedOnFilter($area,$pelayanan);

    //     if(!empty($list_jaringan))
    //     {

    //         $param = array();
    //         foreach ($list_jaringan as $jaringan) 
    //         {
    //             $linkPeta          = $jaringan->link_peta;
    //             $nama_dealer       = $jaringan->nama_dealer;
    //             $alamat_jaringan   = $jaringan->alamat_jaringan;
    //             $kategori_jaringan = $jaringan->kategori_jaringan;

    //             // Prepare a request for the given URL
    //             $curl = curl_init($linkPeta);

    //             // Set the needed options:
    //             curl_setopt_array($curl, array(
    //                 CURLOPT_NOBODY => TRUE,            // Don't ask for a body, we only need the headers
    //                 CURLOPT_FOLLOWLOCATION => FALSE,   // Don't follow the 'Location:' header, if any
    //             ));

    //             // Send the request (you should check the returned value for errors)
    //             curl_exec($curl);

    //             // Get information about the 'Location:' header (if any)
    //             $location = curl_getinfo($curl,CURLINFO_REDIRECT_URL);
    //             $param[]    = array('location' => $location,'nama_dealer' => $nama_dealer, 'alamat_dealer' => $alamat_jaringan, 'kategori_jaringan' => $kategori_jaringan);
    //         }
            
    //         echo json_encode($param);
    //     }
    // }
    
    function ajax_get_jaringan()
    {
        $area        = $this->input->post('area');
        $penjualan   = $this->input->post('penjualan');
        $pemeliharan = $this->input->post('pemeliharan');
        $sukucadang  = $this->input->post('sukucadang');
        $param       = array();

        $list_jaringan = $this->select_global_model->getJaringanBasedOnFilter($area,$penjualan,$pemeliharan,$sukucadang);

        if(!empty($list_jaringan))
        {
            foreach($list_jaringan as $jaringan)
            {
                $latitude          = $jaringan->latitude;
                $longitude         = $jaringan->longitude;
                $nama_dealer       = $jaringan->nama_dealer;
                $alamat_jaringan   = $jaringan->alamat_jaringan;
                $no_telepon        = $jaringan->nomor_telepon;
                $kota_kabupaten    = $jaringan->nama_kota_kabupaten;
                $kategori_jaringan = $jaringan->kategori_jaringan;
                $link_peta         = $jaringan->link_peta;

                $param[]           = array('latitude' => $latitude, 'longitude' => $longitude, 'nama_dealer' => $nama_dealer, 'alamat_dealer' => $alamat_jaringan, 'kategori_jaringan' => $kategori_jaringan, 'no_telepon' => $no_telepon, 'kota_kabupaten' => $kota_kabupaten, 'link_peta' => $link_peta);
            }

            echo json_encode($param);
        }
    }
}