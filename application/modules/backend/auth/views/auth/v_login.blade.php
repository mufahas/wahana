<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Version: 5.0.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en" >
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>
            WAHANA HONDA
        </title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!--end::Web font -->
        <!--begin::Base Styles -->
        <link href="{{ base_url() }}assets/backend/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/default/css/vendors.bundle.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/default/css/style.bundle.css" rel="stylesheet" type="text/css" />

        <!--end::Base Styles -->
        <link rel="shortcut icon" href="{{ base_url() }}assets/default/media/img/logo/wahana.ico" />
    </head>
    <style type="text/css">
        .m-login.m-login--2.m-login-2--skin-1 .m-login__container .m-login__form .form-control {
            color: #fff;
            background: rgba(126, 31, 34,.3);
        }
        .m-login.m-login--2.m-login-2--skin-1 .m-login__container .m-login__form {
            color: #fff;
        }
        .m-login.m-login--2.m-login-2--skin-1 .m-login__container .m-login__form .m-login__form-sub .m-checkbox {
            color: #fff;
        }
        .m-login.m-login--2.m-login-2--skin-1 .m-login__container .m-login__form .m-login__form-sub .m-link {
            color: #fff;
        }

        .bg-image {
            filter: gray;
            -webkit-filter: grayscale(1);
            -webkit-transition: all .8s ease-in-out;  
        }

        .bg-image:hover {
            filter: none;
            -webkit-filter: grayscale(0);
            -webkit-transform: scale(1.00);
        }


    </style>
    <!-- end::Head -->
    <!-- end::Body -->
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="bg-image m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url({{ base_url() }}assets/default/media/img/users/bg-1.jpg);">
                <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
                    <div class="m-login__container">
                        <div class="m-login__logo">
                            <a href="#">
                                <img src="{{ base_url() }}assets/default/media/img/users/logo-1.png">
                            </a>
                        </div>
                        <div class="m-login__signin">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                    Sign In To Admin
                                </h3>
                            </div>
                            {!! form_open(null, array('class' => 'm-login__form m-form', 'role' => 'form')) !!}
                                <div class="form-group m-form__group">
                                    {!! form_input(array('type' => 'text','name' => 'identity', 'class' => 'form-control m-input m-login__form-input--last', 'placeholder' => 'Email', 'autocomplete' => 'off' )) !!}
                                </div>
                                <div class="form-group m-form__group">
                                    {!! form_input(array('type' => 'password','name' => 'password', 'class' => 'form-control m-input m-login__form-input--last', 'placeholder' => 'Password', 'autocomplete' => 'off' )) !!}
                                </div>
                                <div class="row m-login__form-sub">
                                    <!--<div class="col m--align-left m-login__form-left">-->
                                    <!--    <label class="m-checkbox  m-checkbox--light">-->
                                    <!--        <input type="checkbox" name="remember">-->
                                    <!--        Remember me-->
                                    <!--        <span></span>-->
                                    <!--    </label>-->
                                    <!--</div>-->
                                    <div class="col m--align-right m-login__form-right">
                                        <a href="javascript:;" id="m_login_forget_password" class="m-link">
                                            Forget Password ?
                                        </a>
                                    </div>
                                </div>
                                <div class="m-login__form-action">
                                    <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">
                                        Sign In
                                    </button>
                                </div>
                            {!! form_close() !!}
                        </div>
                        <div class="m-login__forget-password">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                    Forgotten Password ?
                                </h3>
                                <div class="m-login__desc">
                                    Enter your email to reset your password:
                                </div>
                            </div>
                            <form class="m-login__form m-form" action="">
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
                                </div>
                                <div class="m-login__form-action">
                                    <button id="m_login_forget_password_submit" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                        Request
                                    </button>
                                    &nbsp;&nbsp;
                                    <button id="m_login_forget_password_cancel" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Page -->
        <!--begin::Base Scripts -->
        <script src="{{ base_url() }}assets/default/js/vendors.bundle.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/default/js/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Base Scripts --> 
        <!--begin::Page Snippets -->
        <script type="text/javascript">
            var processLogin    = '{{$processLogin}}';
            var dashboard       = '{{$dashboard}}';
            var forget_password = '{{$forget_password}}'
        </script>
        <script src="{{ base_url() }}assets/backend/js/auth/auth.js" type="text/javascript"></script>
        <!--end::Page Snippets -->
    </body>
    <!-- end::Body -->
</html>
