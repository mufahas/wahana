<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- If you delete this meta tag, Half Life 3 will never be released. -->
    <meta name="viewport" content="width=device-width" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <title>IMS | Forgot Password</title>

    <style>
        
        /* Begin: Universal */
        body {
            margin: 0;
            padding: 0;
            font-family: arial;
        }
        a {
            text-decoration: none;
        }

        .btn {
            display: inline-block;
            font-weight: normal;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            user-select: none;
            border: 1px solid transparent;
            padding: 0.65rem 1.25rem;
            font-size: 1rem;
            line-height: 1.25;
            border-radius: 0.25rem;
            transition: all 0.15s ease-in-out;
        }
        .btn-outline-primary {
            color: #716aca;
            background-color: transparent;
            background-image: none;
            border-color: #716aca;
        }
        .btn-outline-primary:hover {
            color: #fbf8fd;
            background-color: #716aca;
            background-image: none;
            border-color: #716aca;
        }
        .btn-outline-2x {
            border-width: 2px;
        }
        .btn-icon-only {
            display: inline-block;
            position: relative;
            padding: 0 !important;
            width: 33px;
            height: 33px;
        }
        .btn-pill {
            -webkit-border-radius: 60px;
            -moz-border-radius: 60px;
            -ms-border-radius: 60px;
            -o-border-radius: 60px;
            border-radius: 60px;
        }

        .d-flex {
            display: flex;
        }
        .flex-column {
            flex-direction: column;
        }
        .flex-column-reverse {
            flex-direction: column-reverse;
        }
        .justify-content-center {
            justify-content: center;
        }
        .align-content-center {
            align-content: center;
        }
        .align-item-center {
            align-items: center;
        }

        .col-100 {
            max-width: 100%;
            width: 100%;
        }
        .col-rs-1 {
            flex: 0 0 8.33%;
            max-width: 8.33%;
        }
        .col-rs-2 {
            flex: 0 0 16.66%;
            max-width: 16.66%;
        }
        .col-rs-3 {
            flex: 0 0 25%;
            max-width: 25%;
        }
        .col-rs-4 {
            flex: 0 0 33.33%;
            max-width: 33.33%;
        }
        .col-rs-5 {
            flex: 0 0 41.66%;
            max-width: 41.66%;
        }
        .col-rs-6 {
            flex: 0 0 50%;
            max-width: 50%;
        }
        .col-rs-7 {
            flex: 0 0 58.33%;
            max-width: 58.33%;
        }
        .col-rs-8 {
            flex: 0 0 66.66%;
            max-width: 66.66%;
        }
        .col-rs-9 {
            flex: 0 0 75%;
            max-width: 75%;
        }
        .col-rs-10 {
            flex: 0 0 83.33%;
            max-width: 83.33%;
        }
        .col-rs-11 {
            flex: 0 0 91.66%;
            max-width: 91.66%;
        }
        .col-rs-12 {
            flex: 0 0 100%;
            max-width: 100%;
        }

        .mt-auto {
            margin-top: auto;
        }
        .mb-auto {
            margin-left: auto;
        }
        .ml-auto {
            margin-left: auto;
        }
        .mr-auto {
            margin-right: auto;
        }

        .mt-10 {
            margin-top: 10px;
        }
        .mb-10 {
            margin-bottom: 10px;
        }
        .ml-10 {
            margin-left: 10px;
        }
        .mr-10 {
            margin-right: 10px;
        }

        .mt-20 {
            margin-top: 20px;
        }
        .mb-20 {
            margin-bottom: 20px;
        }
        .ml-20 {
            margin-left: 20px;
        }
        .mr-20 {
            margin-right: 20px;
        }

        .mt-30 {
            margin-top: 30px;
        }
        .mb-30 {
            margin-bottom: 30px;
        }
        .ml-30 {
            margin-left: 30px;
        }
        .mr-30 {
            margin-right: 30px;
        }

        .mt-40 {
            margin-top: 40px;
        }
        .mb-40 {
            margin-bottom: 40px;
        }
        .ml-40 {
            margin-left: 40px;
        }
        .mr-40 {
            margin-right: 40px;
        }

        .mt-50 {
            margin-top: 50px;
        }
        .mb-50 {
            margin-bottom: 50px;
        }
        .ml-50 {
            margin-left: 50px;
        }
        .mr-50 {
            margin-right: 50px;
        }

        .pt-auto {
            padding-top: auto;
        }
        .pb-auto {
            padding-left: auto;
        }
        .pl-auto {
            padding-left: auto;
        }
        .pr-auto {
            padding-right: auto;
        }

        .pt-10 {
            padding-top: 10px;
        }
        .pb-10 {
            padding-bottom: 10px;
        }
        .pl-10 {
            padding-left: 10px;
        }
        .pr-10 {
            padding-right: 10px;
        }

        .pt-20 {
            padding-top: 20px;
        }
        .pb-20 {
            padding-bottom: 20px;
        }
        .pl-20 {
            padding-left: 20px;
        }
        .pr-20 {
            padding-right: 20px;
        }

        .pt-30 {
            padding-top: 30px;
        }
        .pb-30 {
            padding-bottom: 30px;
        }
        .pl-30 {
            padding-left: 30px;
        }
        .pr-30 {
            padding-right: 30px;
        }

        .pt-40 {
            padding-top: 40px;
        }
        .pb-40 {
            padding-bottom: 40px;
        }
        .pl-40 {
            padding-left: 40px;
        }
        .pr-40 {
            padding-right: 40px;
        }

        .pt-50 {
            padding-top: 50px;
        }
        .pb-50 {
            padding-bottom: 50px;
        }
        .pl-50 {
            padding-left: 50px;
        }
        .pr-50 {
            padding-right: 50px;
        }

        .fs-12 {
            font-size: 12px;
        }
        .fs-14 {
            font-size: 14px;
        }
        .fs-16 {
            font-size: 16px;
        }
        .fs-18 {
            font-size: 18px;
        }
        .fs-20 {
            font-size: 20px;
        }
        .fs-27 {
            font-size: 27px;
        }

        .primary-text {
            color: #716aca;
        }
        .red-text {
            color: #b90a29;
        }
        .green-text {
            color: #11a88a;
        }
        .blue-text {
            color: #0000ff;
        }
        .white-text {
            color: #fafafa;
        }
        .dark-text{
            color: #575962;
        }
        .grey-text {
            color: #6a6a6a;
        }
        
        .primary {
            background: #716aca;
        }
        .red {
            background: #b90a29;
        }
        .green {
            background: #11a88a;
        }
        .blue {
            background: #0000ff;
        }
        .white {
            background: #ffffff;
        }
        .dark{
            background: #575962;
        }
        .grey {
            background: #9e9e9e;
        }
        /* End: Universal */

        /* Begin: Layout*/
        .body {
            padding-top: 50px;
            font-family: roboto;
            background-color: #f2f3f8;
        }
        .portlet {
            flex: 0 0 60%;
            max-width: 60%;
        }
        .portlet .portlet-header {
            padding: 1rem 2rem;
        }
        .portlet .portlet-content {
            padding: 1rem 2rem;
        }
        .portlet .portlet-footer {
            padding: 1rem 2rem;
        }
        .footer {
            padding: 10px;
            background-color: rgba(0,0,0,0.05);
        }
        /* End: Layout */

        /* Begin: Mobile */
        @media only screen and (max-width: 768px) {
            /* Begin: Universal */
            [class*="col-rs-"] {
                flex: 0 0 100%;
                max-width: 100%;
            }
            /*End: Universal */

            /*Begin: Layout*/
            .body {
                padding-top: 20px;
            }
            .portlet {
                flex: 0 0 90%;
                max-width: 90%;
            }
            /*End: Layout*/
        }
        /* End:; Mobile */

    </style>

</head>

<body>

<!-- BEGIN: Body -->
<div class="body">

    <div class="portlet ml-auto mr-auto white dark-text">
        
        <div class="portlet-header primary white-text">
            <h3>Wahana Honda</h3>
        </div>
        
        <div class="portlet-content">
            <h4>Hello <?= (!empty($user->first_name)) ? $user->first_name : '' ?> <?= (!empty($user->last_name)) ? $user->last_name : '' ?>,</h4>

            <center>
                <h1>Changing your password</h1>
                <p>Need to reset your password? No problem, Just click below to get started.</p>
                <p><a href="<?= base_url() ?>auth/reset_password/<?= $forgotten['forgotten_password_code'] ?>" class="btn btn-outline-primary btn-outline-2x" target="_blank"> Reset my password</a></p>
            </center>
        </div>
    </div>
</div>
<!-- END: Body -->

</body>
</html>