@extends('backend.default.views.layout.v_layout')

@section('body')
    
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form Role Permission
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form-role-permission', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            Role Name:
                        </label>
                        <b><h5>{{ $result->name }}</h5></b>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="500">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th width="50px" class="text-center">
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="chk_all" class="chk_all" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th>Menu Name</th>
                                    <?php
                                    foreach ($allbutton as $row) {
                                        echo '<th class="text-center">';
                                        echo ucwords($row->button_title);
                                        echo '</th>';
                                    }
                                    ?>
                                </tr>
                            </thead>

                            <tbody>
                            <?php
                                $sub = 0;
                                foreach ($allmenu as $row):

                                    echo '
                                        <tr>
                                            <td class="text-center">
                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="chk[]" class="chk chkchild'.$row->menu_parent.'" value="'.$row->id_menu.'" '.(array_key_exists($row->id_menu, $result2) ? 'checked' : '').' />
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td><b>'.$row->menu_name.'</b></td>
                                    ';

                                    foreach ($allbutton as $row2) {

                                        $q = M_menu_button::where('id_menu', $row->id_menu)->where('id_button',$row2->id_button)->first();
                                        
                                        if ($q) {
                                            echo '
                                                <td class="text-center">
                                                    <label class="m-checkbox">
                                                        <input type="checkbox" name="chkbutton['.$row->id_menu.'][]" class="chkbutton" value="'.$q->id_menu_button.'" '.(array_key_exists($row->id_menu, $result2) ? (in_array($q->id_menu_button, $result2[$row->id_menu]) ? 'checked' : '') : '').' />
                                                        <span></span>
                                                    </label>
                                                </td>
                                            ';
                                        } else {
                                            echo '
                                                <td class="text-center">
                                                </td>
                                            ';
                                        }
                                    }

                                    echo '</tr>';

                                    if ($row->menu_class == '' || $row->menu_class == null):
                                        $sub_menu = get_list_child($row->id_menu, $sub, $result2);
                                    endif;

                                endforeach;
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            <button type="button" class="btn btn-primary save">
                                {{ lang('button_insert') }}
                            </button>
                            <button type="reset" class="btn btn-secondary">
                                {{ lang('button_cancel') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($result->id) )) !!}
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/role_application/role_permission/js/role-permission.js" type="text/javascript"></script>
@stop