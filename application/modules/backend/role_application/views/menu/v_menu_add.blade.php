@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form Menu
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form-menu', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Menu Name:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'menu_name', 'class' => 'form-control m-input', 'placeholder' => 'Enter Menu Name' )) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="">
                            Alias Class:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'menu_class', 'class' => 'form-control m-input', 'placeholder' => "don't fill if dropdown menu" )) !!}
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label class="">
                            Folder:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'menu_folder', 'class' => 'form-control m-input', 'placeholder' => "don't fill if dropdown menu" )) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="">
                            *
                            Menu Parent:
                        </label>
                        {!! form_dropdown('menu_parent', $parent, '', 'class="form-control m-input"') !!}
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label class="">
                            Icon:
                        </label>
                        <div class="input-group"> 
                            {!! form_dropdown('menu_icon', $icon, '', 'class="form-control m-select2" id="m_select2_2"') !!}
                            <span class="input-group-addon icons">
                                            
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-6 form-group ">
                        <label class="">
                            Menu Order:
                        </label>
                        {!! form_input(array('type' => 'number','name' => 'menu_order', 'class' => 'form-control m-input', 'placeholder' => 'Enter Order' )) !!}
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label class="">
                            Button:
                        </label>
                        @foreach ($button as $row)
                            <div class="col-lg-6">
                                <label class="m-checkbox chkbox">
                                    {!! form_input(array('type' => 'checkbox','name' => 'menu_button[]', 'value' => $row->id_button )) !!}  {{ ucwords($row->button_title) }}
                                    <span></span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-lg-6 form-group ">
                        <label class="">
                            Status:
                        </label>
                        <br>
                        {!! form_input(array('type' => 'checkbox','name' => 'menu_status', 'data-switch' => 'true', 'id' => 'm_switch_1', 'checked' => 'checked', 'value' => '1')) !!}
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            <button type="button" class="btn btn-primary save">
                                {{ lang('button_insert') }}
                            </button>
                            <button type="reset" class="btn btn-secondary">
                                {{ lang('button_cancel') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var checkClass = '{{$checkClass}}';
</script>
<script src="{{ base_url() }}assets/backend/js/role_application/menu/js/menu.js" type="text/javascript"></script>
@stop