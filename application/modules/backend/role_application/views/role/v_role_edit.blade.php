@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form Role
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form-role', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            *
                            Name:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'name', 'class' => 'form-control m-input', 'placeholder' => 'Enter Name', 'value' => $result->name )) !!}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label class="">
                            *
                            Description:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'description', 'class' => 'form-control m-input', 'placeholder' => 'Enter Description', 'value' => $result->description )) !!}
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            <button type="button" class="btn btn-primary save">
                                {{ lang('button_update') }}
                            </button>
                            <button type="reset" class="btn btn-secondary">
                                {{ lang('button_cancel') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($result->id) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/role_application/role/js/role.js" type="text/javascript"></script>
@stop