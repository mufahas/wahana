@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form User
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form-menu', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Username:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'username', 'class' => 'form-control m-input')) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class>
                            *
                            Email:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'email', 'class' => 'form-control m-input')) !!}
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label class="">
                            First Name:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'first_name', 'class' => 'form-control m-input')) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="">
                            *
                            Last Name:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'last_name', 'class' => 'form-control m-input' )) !!}
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label class="">
                            *
                            Role User:
                        </label>
                        {!! form_dropdown('role_user', $role, '', 'class="form-control m-input m-select2" id="m_select2_2"') !!}
                    </div>
                    <div class="col-lg-6 form-group ">
                        <label class="">
                            Phone Number:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'phone_number', 'class' => 'form-control m-input')) !!}
                    </div>
                </div>
                <!-- <div class="m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label class="">
                            Status:
                        </label>
                        <br>
                        {!! form_input(array('type' => 'checkbox','name' => 'status_user', 'data-switch' => 'true', 'id' => 'm_switch_1', 'checked' => 'checked', 'value' => '1')) !!}
                    </div>
                </div> -->
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            <button type="button" class="btn btn-primary save">
                                {{ lang('button_insert') }}
                            </button>
                            <button type="reset" class="btn btn-secondary cancel">
                                {{ lang('button_cancel') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var checkEmail = '{{$checkEmail}}';
</script>
<script src="{{ base_url() }}assets/backend/js/role_application/user/js/user.js" type="text/javascript"></script>
@stop