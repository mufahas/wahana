@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        View
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{base_url()}}role-application/user" class="m-portlet__nav-link m-portlet__nav-link--icon">
                            <i class="la la-undo"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form-menu', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Username:
                        </label>
                        <h5>{{ $result->username }}</h5>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class>
                            *
                            Email:
                        </label>
                        <h5>{{ $result->email }}</h5>
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label class="">
                            First Name:
                        </label>
                        <h5>{{ $result->first_name }}</h5>    
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="">
                            *
                            Last Name:
                        </label>
                        <h5>{{ $result->last_name }}</h5>
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label class="">
                            *
                            Role User:
                        </label>
                        <h5>{{ $result->name }}</h5>
                    </div>
                    <div class="col-lg-6 form-group ">
                        <label class="">
                            Phone Number:
                        </label>
                        <h5>{{ $result->phone }}</h5>
                    </div>
                </div>
                <!-- <div class="m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label class="">
                            Status:
                        </label>
                        <br>
                        {!! form_input(array('type' => 'checkbox','name' => 'status_user', 'data-switch' => 'true', 'id' => 'm_switch_1', 'checked' => 'checked', 'value' => '1')) !!}
                    </div>
                </div> -->
            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/role_application/user/js/user-edit.js" type="text/javascript"></script>
@stop