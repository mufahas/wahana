<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_group extends Eloquent {

	public $table = 'groups';
	public $primaryKey = 'id';
	public $timestamps = false;

}