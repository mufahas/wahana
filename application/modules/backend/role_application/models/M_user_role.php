<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_user_role extends Eloquent {

	public $table      = 'users_groups';
	public $primaryKey = 'id';
	public $timestamps = false;

}