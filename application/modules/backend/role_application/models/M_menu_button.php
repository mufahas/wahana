<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_menu_button extends Eloquent {

	public $table      = 'menu_button';
	public $primaryKey = 'id_menu_button';
	public $timestamps = false;

}