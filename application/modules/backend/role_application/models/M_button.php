<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_button extends Eloquent {

	public $table      = 'button';
	public $primaryKey = 'id_button';
	public $timestamps = false;

}