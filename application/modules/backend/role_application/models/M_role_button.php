<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_role_button extends Eloquent {

	public $table      = 'role_button';
	public $primaryKey = 'id_role_button';
	public $timestamps = false;

}