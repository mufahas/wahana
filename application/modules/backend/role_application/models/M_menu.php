<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_menu extends Eloquent {

	public $table      = 'menu';
	public $primaryKey = 'id_menu';
	public $timestamps = false;

}