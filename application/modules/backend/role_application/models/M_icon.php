<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_icon extends Eloquent {

	public $table      = 'icon';
	public $primaryKey = 'id_icon';
	public $timestamps = false;

}