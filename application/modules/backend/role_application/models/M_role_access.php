<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_role_access extends Eloquent {

	public $table      = 'role_access';
	public $primaryKey = 'id_role_access';
	public $timestamps = false;

}