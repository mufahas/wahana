<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_user extends Eloquent {

	public $table      = 'users';
	public $primaryKey = 'id';
	public $timestamps = false;

}