<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller 
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = str_replace('_','-', $this->router->fetch_class());
        $this->site   = $this->folder . '/' . $this->class;

        $this->load->model('setting_menu_model');

        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend","role_application","menu","v_" . $this->class, $data);
    }

    /**
    * Serverside load table:menu
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "M_menu";
        $condition    = "";
        $row          = array('menu.id_menu','menu.menu_name', 'menu.menu_folder', 'menu.menu_class', 'menu.menu_status', 'menu.id_menu');
        $row_search   = array('menu.id_menu','menu.menu_name', 'menu.menu_folder', 'menu.menu_class', 'menu.menu_status', 'menu.id_menu');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {   
        $data['parent']     = $this->setting_menu_model->get_select_parent();
        $data['button']     = $this->setting_menu_model->get_list_button();
        $data['icon']       = $this->setting_menu_model->get_select_icon();

        /* Button Action */
        $data['action']     = site_url() . $this->site . '/save';
        $data['checkClass'] = site_url() . $this->site . '/ajax_check_class';

        $this->load_view("backend","role_application","menu","v_" . $this->class . "_add",$data);
    }

    /**
    * Save data to table:menu & menu_button
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/add';
            
            /* Get Data Post */
            $menu_name   = ucwords($this->input->post('menu_name'));
            $menu_class  = $this->input->post('menu_class');
            $menu_folder = $this->input->post('menu_folder');
            $menu_parent = $this->input->post('menu_parent');
            $menu_order  = $this->input->post('menu_order');
            $menu_icon   = !empty($this->input->post('menu_icon')) ? $this->input->post('menu_icon') : null;
            $menu_button = $this->input->post('menu_button');
            $menu_status = !empty($this->input->post('menu_status')) ? '1' : '0';

            /* Checking data in database */
            if(!empty($menu_class)){
                $menu = M_menu::where('menu_class',$menu_class)->first();
            }
            else
            {
                $menu = null;
            }

            if(empty($menu))
            {
                /* Initialize Data */
                $model              = new M_menu();
                $model->menu_name   = $menu_name;
                $model->menu_class  = $menu_class;
                $model->menu_folder = $menu_folder;
                $model->menu_parent = $menu_parent;
                $model->menu_order  = $menu_order;
                $model->id_icon     = $menu_icon;
                $model->menu_status = $menu_status;

                /* Save */
                $save = $model->save();

                if($save)
                {
                    /* ---------------------------------------------------------------------------------------------- */
                    if(!empty($menu_button))
                    {
                        for ($i=0; $i < count($menu_button); $i++) 
                        { 
                            
                            $model_menu_button                 = new M_menu_button();
                            $model_menu_button->id_menu        = M_menu::max('id_menu'); 
                            $model_menu_button->id_button      = $menu_button[$i];

                            /* Save Data menu_button */
                            $saveb = $model_menu_button->save();
                        }
                    }

                    /* Write Log */
                    $data_notif = array(
                                        "ID Menu"     => M_menu::max('id_menu'),
                                        "Menu Name"   => $menu_name,
                                        "Menu Class"  => $menu_class,
                                        "Menu Folder" => $menu_folder,
                                        "Menu Parent" => $menu_parent,
                                        "Menu Order"  => $menu_order,
                                        "ID Icon"     => $menu_icon,
                                        "Menu status" => $menu_status
                                        );

                    $message = "Managed to added that a menu " . $menu_name;
                    $this->activity_log->create(json_encode($data_notif), NULL, NULL, $message, $this->router->fetch_method());
                    /* End Write Log */

                    /* Create folder and file .php*/
                    $this->create_file($menu_class,$menu_folder);
                    /* End Create folder and file .php */

                    $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
                }

            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $id_menu    = decryptID($id);
        $model      = M_menu::where('id_menu',$id_menu)->first();

        if(!empty($model))
        {
            $data['parent']    = $this->setting_menu_model->get_select_parent();
            $data['button']    = $this->setting_menu_model->get_list_button();
            $data['icon']      = $this->setting_menu_model->get_select_icon();
            $data['result']    = $model;
            $data['result2']   = array();

            $getMenuButton     = M_menu_button::where('id_menu',$id_menu)->get();
            foreach ($getMenuButton as $key => $value) 
            {
                $data['result2'][] = $value->id_button;
            }

            /* Button Action */
            $data['checkClass'] = site_url() . $this->site . '/ajax_check_class';
            $data['action']     = site_url() . $this->site . '/update';

            $this->load_view("backend","role_application","menu","v_" . $this->class . "_edit",$data);
        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:groups
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($this->input->is_ajax_request()) 
        {   
            $id          = $this->input->post("id");
            $id_menu     = decryptID($id);
            
            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/edit/' . $id;
            
            /* Get Data Post */
            $menu_name   = ucwords($this->input->post('menu_name'));
            $menu_class  = $this->input->post('menu_class');
            $menu_folder = $this->input->post('menu_folder');
            $menu_parent = $this->input->post('menu_parent');
            $menu_order  = $this->input->post('menu_order');
            $menu_icon   = !empty($this->input->post('menu_icon')) ? $this->input->post('menu_icon') : null;
            $menu_button = $this->input->post('menu_button');
            $menu_status = !empty($this->input->post('menu_status')) ? '1' : '0';

            /* Checking data in database */
            if(!empty($menu_class))
            {
                $menu = M_menu::where('menu_class',$menu_class)->whereRaw('id_menu != '.$id_menu.'')->first();
            }else{
                $menu = null;
            }

            if(empty($menu))
            {
                $model              = M_menu::find($id_menu);  

                /* Array for write log */
                $data_old = array(
                                "ID Menu"     => $model->id_menu,
                                "Menu Name"   => $model->menu_name,
                                "Menu Class"  => $model->menu_class,
                                "Menu Folder" => $model->menu_folder,
                                "Menu Parent" => $model->menu_parent,
                                "Menu Order"  => $model->menu_order,
                                "ID Icon"     => $model->menu_icon,
                                "Menu status" => $model->menu_status
                            );

                $data_new = array(
                                "ID Menu"     => $id_menu,
                                "Menu Name"   => $menu_name,
                                "Menu Class"  => $menu_class,
                                "Menu Folder" => $menu_folder,
                                "Menu Parent" => $menu_parent,
                                "Menu Order"  => $menu_order,
                                "ID Icon"     => $menu_icon,
                                "Menu status" => $menu_status
                            );
                /* End array for write log */
                
                /* Initialize Data */
                $model->menu_name   = $menu_name;
                $model->menu_class  = $menu_class;
                $model->menu_folder = $menu_folder;
                $model->menu_parent = $menu_parent;
                $model->menu_order  = $menu_order;
                $model->id_icon     = $menu_icon;
                $model->menu_status = $menu_status;

                /* Update Data menu */
                $update             = $model->save();

                if($update)
                {
                    /* ---------------------------------------------------------------------------------------------- */
                    /* Delete Data menu_button & role_button not Available */
                    $button_available = array();
                    $q = M_menu_button::where('id_menu',$id_menu)->get();

       
                    foreach ($q as $row) {
                        if (!in_array($row->id_button, (array) $menu_button)) {
                            $m_menu_role_button = M_role_button::where('id_menu_button', $row->id_menu_button)->get();
                            if(!empty($m_menu_role_button)){
                                foreach ($m_menu_role_button as $role_button) {
                                    $delete1 = $role_button->delete();
                                }
                            }
                            
                            $m_menu_button = M_menu_button::where('id_menu',$id_menu)->where('id_button', $row->id_button)->get();
                            if(!empty($m_menu_button)){
                                foreach ($m_menu_button as $m_mnu_button) {
                                    $delete2 = $m_mnu_button->delete();
                                }
                            }
                            
                        } else {
                            $button_available[] = $row->id_button;
                        }
                    }

                    /* ---------------------------------------------------------------------------------------------- */
                    /* Add Data menu_button */
                    for ($i=0; $i < count($menu_button); $i++) { 
                        if (!in_array($menu_button[$i], (array) $button_available)) {
                            $model_menu_button                 = new M_menu_button();
                            $model_menu_button->id_menu        = $id_menu;
                            $model_menu_button->id_button      = $menu_button[$i];

                            /* Save Data menu_button */
                            $saveb = $model_menu_button->save();
                        }
                    }


                    /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message     = 'Managed to change the menu ' . $menu_name;
                    $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                    /* End Write Log*/

                    $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
                }

            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url      = site_url() . $this->site;
            $id       = $this->input->get("id");
            $id_menu  = decryptID($id);

            $menu     = M_menu::where("id_menu",$id_menu)->first();

            if(!empty($menu))
            {
                $data_notif = array(
                    "ID Menu"     => $menu->id_menu,
                    "Menu Name"   => $menu->menu_name,
                    "Menu Class"  => $menu->menu_class,
                    "Menu Folder" => $menu->menu_folder,
                    "Menu Parent" => $menu->menu_parent,
                    "Menu Order"  => $menu->menu_order,
                    "ID Icon"     => $menu->menu_icon,
                    "Menu status" => $menu->menu_status
                );

                $q = M_menu::leftjoin('role_access', 'role_access.id_menu', '=', 'menu.id_menu')
                            ->where('menu.id_menu',$id_menu)
                            ->get();

                if(!empty($q))
                {
                    foreach ($q as $value) 
                    {
                        $m_menu_role_button = M_role_button::where('id_role_access', $value->id_role_access)->get();
                        if(!empty($m_menu_role_button))
                        {
                            foreach ($m_menu_role_button as $menu_role_button) 
                            {
                                $delete1 = $menu_role_button->delete();
                            }
                        }
                    }
                }

                $q2 = M_role_access::where('id_menu',$id_menu)->get();

                if(!empty($q2))
                {
                    foreach ($q2 as $value2) 
                    {
                        $delete2 = $value2->delete();
                    }
                }

                $q3 = M_menu_button::where('id_menu',$id_menu)->get();

                if(!empty($q3))
                {
                    foreach ($q3 as $value3) 
                    {
                        $delete3 = $value3->delete();
                    }
                }

                $delete = $menu->delete();

                if($delete)
                {

                    /* Write log */
                    $message = "Managed to remove menu " . $menu->menu_name;
                    $this->activity_log->create(NULL, json_encode($data_notif), NULL, $message, $this->router->fetch_method());
                    /* End Write Log */
    
                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    function ajax_check_class()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $id_menu    = decryptID($this->input->post('id_menu'));
            $menu_class = $this->input->post('menu_class');
            $result     = M_menu::where('menu_class',$menu_class)->first();

            if ($result) 
            {
                if ($id_menu) 
                {
                    if ($id_menu == $result->id_menu) 
                    {
                        echo 'true';
                    } 
                    else 
                    {
                        echo 'false';
                    }
                } 
                else 
                {
                    echo 'false';
                }
            } 
            else 
            {
                echo 'true';
            }
        }
    }

    /* 
    this function create file controllers and file views
    */
    function create_file($menu_class,$menu_folder)
    {
        $class              = ucfirst(str_replace("-","_",$menu_class));
        $class_lower        = strtolower($class); 
        $folder             = $menu_folder;

        $modules            = './application/modules/backend/'.$folder;
        $controllers        = './application/modules/backend/'.$folder.'/controllers';
        $models             = './application/modules/backend/'.$folder.'/models';
        $views              = './application/modules/backend/'.$folder.'/views';
        $views_class        = './application/modules/backend/'.$folder.'/views/'.$class_lower;

        /* file controllers */
        $source_controllers = './assets/template/controllers/Class.php';
        $file_controllers   = './application/modules/backend/'.$folder.'/controllers/'.$class.'.php';

        /* file views (blade) */
        $source_views_index = './assets/template/views/page.blade.php';
        $source_views_add   = './assets/template/views/page_add.blade.php';
        $source_views_edit  = './assets/template/views/page_edit.blade.php';
        $file_views_index   = './application/modules/backend/'.$folder.'/views/'.$class_lower.'/v_'.$class_lower.'.blade.php';
        $file_views_add     = './application/modules/backend/'.$folder.'/views/'.$class_lower.'/v_'.$class_lower.'_add.blade.php';
        $file_views_edit    = './application/modules/backend/'.$folder.'/views/'.$class_lower.'/v_'.$class_lower.'_edit.blade.php';

        if (!file_exists($modules)) 
        {
            if(!empty($folder))
            {
                $modules = mkdir($modules, 0755);

                if($modules)
                {
                    if (!file_exists($controllers)) 
                    {
                        $dir_controllers =  mkdir($controllers, 0755);

                        if($dir_controllers)
                        {
                            if(!empty($class))
                            {
                                $copy_file_controllers = copy($source_controllers, $file_controllers);

                                if($copy_file_controllers)
                                {
                                    
                                    $filename           = $file_controllers; 
                                    $file_content       = file($filename);
                                    $count_file_content = count($file_content);

                                    $file_content[4]    = "class ".$class." extends MY_Controller\r\n"; 
                                    $fp                 = fopen($filename, "w+") or die("Couldn't create new file controller ".$class."");  

                                    for ($i= 0; $i < $count_file_content; $i++)
                                    {
                                        fwrite($fp, $file_content[$i]);
                                    }
                                    
                                    fclose($fp);
                                }
                            }
                        }
                    }

                    if (!file_exists($models)) 
                    {
                        mkdir($models, 0755);
                    }

                    if (!file_exists($views)) 
                    {
                        $dir_views = mkdir($views, 0755);

                        if($dir_views) {

                            if(!empty($class))
                            {
                                if(!file_exists($views_class))
                                {
                                    $dir_views_class = mkdir($views_class, 0755); 

                                    if($dir_views_class)
                                    {
                                        $copy_views_index = copy($source_views_index, $file_views_index);
                                        $copy_views_add   = copy($source_views_add, $file_views_add);
                                        $copy_views_edit  = copy($source_views_edit, $file_views_edit);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if(!empty($class))
            {
                /* create file controllers*/
                if(!file_exists($file_controllers))
                {
                    $copy_controllers = copy($source_controllers, $file_controllers);

                    if($copy_controllers)
                    {
                        $filename           = $file_controllers; 
                        $file_content       = file($filename);
                        $count_file_content = count($file_content);

                        $file_content[4]    = "class ".$class." extends MY_Controller\r\n"; 
                        $fp                 = fopen($filename, "w+") or die("Couldn't create new file controller ".$class."");  

                        for ($i= 0; $i < $count_file_content; $i++)
                        {
                            fwrite($fp, $file_content[$i]);
                        }
                        
                        fclose($fp);
                        
                    }
                }
                /* end create file controllers */

                /* create file views */
                if(!file_exists($views_class))
                {
                    $dir_views_class = mkdir($views_class, 0755); 

                    if($dir_views_class)
                    {   
                        if(!file_exists($file_views_index))
                        {
                            $copy_views_index = copy($source_views_index, $file_views_index);
                        }

                        if(!file_exists($file_views_add))
                        {
                            $copy_views_add   = copy($source_views_add, $file_views_add);
                        }

                        if(!file_exists($file_views_edit))
                        {
                            $copy_views_edit  = copy($source_views_edit, $file_views_edit);
                        }
                    }
                }
                /* end crete file views */
            }
        }
    }
}