<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";

    public function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = str_replace('_','-', $this->router->fetch_class());
        $this->site   = $this->folder . '/' . $this->class;

        /* Load Model */
        $this->load->model('select_global_model');
        $this->load->library('send_email');

    }

    function index()
    {
        $data['loadTable']  = site_url() . $this->site . '/loadTable';
        $data['add']        = site_url() . $this->site . '/add';
        $data['edit']       = site_url() . $this->site . '/edit';
        $data['action']     = site_url() . $this->site . '/update';
        $data['delete']     = site_url() . $this->site . '/delete';
        $data['checkEmail'] = site_url() . $this->site . '/ajax_check_email';
  
        /* Get Data User Based On Login */
        $data['user']       = $this->ion_auth->user()->row();
            
        $this->load_view("backend","role_application","profile","v_" . $this->class, $data);
    }


    function ajax_check_email()
    {
        if ($this->input->is_ajax_request())
        {
            $id_user = decryptID($this->input->post('id_user'));
            $email   = $this->input->post('email');
            $result  = M_user::where('email',$email)->first();

            if ($result)
            {
                if ($id_user)
                {
                    if ($id_user == $result->id)
                    {
                        echo 'true';
                    }
                    else
                    {
                        echo 'false';
                    }
                }
                else
                {
                    echo 'false';
                }
            }
            else
            {
                echo 'true';
            }
        }
    }

    public function sendEmail($id,$email,$password,$firstname,$lastname,$role_name)
    {
        $set['id']          = $id;
        $set['email']       = $email;
        $set['password']    = $password;
        $set['first_name']  = $firstname;
        $set['last_name']   = $lastname;
        $set['role_name']   = $role_name;

        $message            = $this->load->view('../modules/backend/role_application/views/user/v_user_email', $set , true, true);

        $this->send_email->email($email, $message);

    }
}