<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Role_permission extends MY_Controller 
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = str_replace('_','-', $this->router->fetch_class());
        $this->site   = $this->folder . '/' . $this->class;
        
        $this->load->model("select_global_model");
        $this->load->model("setting_menu_model");
        $this->load->helper('setting_role');
    }

    function index() 
    {
        $data['loadTable']     = site_url() . $this->site . '/loadTable';
        $data['add']           = site_url() . $this->site . '/add';
        $data['edit']          = site_url() . $this->site . '/edit';
        $data['delete']        = site_url() . $this->site . '/delete';
        $data['getPermission'] = site_url() . $this->site . '/ajax_getPermission';

        $this->load_view("backend","role_application","role_permission","v_" . $this->class, $data);
    }

    /**
    * Serverside load table:role
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "M_group";
        $condition    = "groups.is_delete = 'f'";
        $row          = array('groups.id','groups.description', 'groups.id');
        $row_search   = array('groups.id','groups.description', 'groups.id');
        $join         = array('role_access'  => 'role_access.id_group = groups.id');
        $order        = "";
        $groupby      = "groups.description";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        $data['role']      = $this->select_global_model->selectAddRole();
        $data['allmenu']   = $this->setting_menu_model->get_list_menu();
        $data['allbutton'] = $this->setting_menu_model->get_list_button();

        /* Button Action */
        $data['action'] = site_url() . $this->site . '/save';

        $this->load_view("backend","role_application","role_permission","v_" . $this->class . "_add",$data);
    }

    /**
    * Save data to table:groups
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/add';

            /* Get Data Post */
            $role      = $this->input->post('role');
            $chk       = $this->input->post('chk');
            $chkbutton = $this->input->post('chkbutton');

            if(!empty($chk))
            {
                foreach ($chk as $key => $value) 
                {
                    $model_role_access = new M_role_access;
                    /* Initialize Data role_access */
                    $model_role_access->id_group = $role;
                    $model_role_access->id_menu  = $value;

                    /* Save Data role_access */
                    $save = $model_role_access->save();

                    if($save)
                    {
                        if(!empty($chkbutton))
                        {
                            foreach ($chkbutton as $key2 => $value2) 
                            {
                                if ($key2 == $value) 
                                {
                                    foreach ($value2 as $key3 => $value3) 
                                    {
                                        $model_role_button = new M_role_button;
                                        /* Initialize Data role_button */
                                        $model_role_button->id_role_access = M_role_access::max('id_role_access');
                                        $model_role_button->id_menu_button = $value3;

                                        /* Save Data role_button */
                                        $save2 = $model_role_button->save();
                                    }
                                }
                            }
                        }
                    }
                }

                if($save)
                {
                    /* Write Log */
                    $q = M_group::where('id', $role)->first();
                    $resultMenuButton = array();
                    $resultMenu       = array();
                    if(!empty($chk)){
                        foreach ($chk as $key => $value4) {

                            $menu = M_menu::where('id_menu',$value4)->first();
                            
                            $resultMenuButton[] = '<b>' . $menu->menu_name . '</b> => '; 
                            $resultMenu[]  = $menu->menu_name;

                            if(!empty($chkbutton)){
                                foreach ($chkbutton as $key3 => $value5) {
                                    if ($key3 == $value4) {
                                        foreach ($value5 as $key4 => $value5) {

                                            $menu_button = M_role_button::leftjoin('menu_button','menu_button.id_menu_button','=','role_button.id_menu_button')
                                                                        ->leftjoin('button','button.id_button','=','menu_button.id_button')
                                                                        ->where('menu_button.id_menu_button',$value5)->first();

                                            $resultMenuButton[] = $menu_button->button_name . ',';
                                        }
                                    }
                                }
                            }

                            $resultMenuButton[] = '<br>';
                        }
                    }

                    $menuAll    = implode(",", $resultMenu);
                    $menuButton = implode("", $resultMenuButton);

                    $data_notif = array(
                                        "ID Role"     => $q->id,
                                        "Role Name"   => $q->name,
                                        "Menu"        => $menuAll,
                                        "Menu Button" => $menuButton,
                                        );

                    $message = "Managed to added that a role permission " . $q->name;
                    $this->activity_log->create(json_encode($data_notif), NULL, NULL, $message,  $this->router->fetch_method());
                    /* End write log*/

                    $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
                }

            }
            else
            {
                $status = array('status' => 'error', 'message' => 'You have not picked at all.', 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $id_group       = decryptID($id);
        $model          = M_group::where('id',$id_group)->first();

        if(!empty($model))
        {
            
            $data['result']    = $model;
            $data['allmenu']   = $this->setting_menu_model->get_list_menu();
            $data['allbutton'] = $this->setting_menu_model->get_list_button();

            $getAvailableMenu  = M_role_access::where('id_group', $id_group)->get();
            foreach ($getAvailableMenu as $key => $value) {
                $data['result2'][$value->id_menu] = array();

                $getAvailableButton = M_role_button::where('id_role_access',$value->id_role_access)->get();
                foreach ($getAvailableButton as $key2 => $value2) {
                    $data['result2'][$value->id_menu][] = $value2->id_menu_button;
                }
            }

            /* Button Action */
            $data['action'] = site_url() . $this->site . '/update';
            
            $this->load_view("backend","role_application","role_permission","v_" . $this->class . "_edit",$data);
        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:groups
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($this->input->is_ajax_request()) 
        {   
            $id          = $this->input->post("id");
            $id_group    = decryptID($id);

            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/edit/' . $id;

            /* Get Data Post */
            $role_name              = $this->input->post('role_name');
            $chk                    = $this->input->post('chk');
            $chkbutton              = $this->input->post('chkbutton');

            /* Checking data in database */
            $group = M_group::where('id',$id_group)->first();

            if(!empty($group))
            {
                /* Array for write log */
                /* Data Old */
                $resultMenuButton_old = array();
                $resultMenu_old       = array();

                $m_role_access = M_role_access::where('id_group',$id_group)->get();
                foreach ($m_role_access as $key => $val) {

                    $menu                   = M_menu::where('id_menu',$val->id_menu)->first();
                    $resultMenuButton_old[] = '<b>' . $menu->menu_name . '</b> => ';
                    $resultMenu_old[]       = $menu->menu_name;

                    $m_role_button = M_role_button::leftjoin('menu_button','menu_button.id_menu_button','=','role_button.id_menu_button')
                                                    ->leftjoin('button','button.id_button','=','menu_button.id_button')
                                                    ->where('role_button.id_role_access',$val->id_role_access)->get(); 

                    foreach ($m_role_button as $key => $val2) {

                        $resultMenuButton_old[] = $val2->button_name . ',';

                    }

                    $resultMenuButton_old[] = '<br>';
                }

                $menuAll_old    = implode(",", $resultMenu_old);
                $menuButton_old = implode("", $resultMenuButton_old);

                $data_old = array(
                                "ID Role"     => $group->id,
                                "Role Name"   => $group->name,
                                "Menu"        => $menuAll_old,
                                "Menu Button" => $menuButton_old,
                                );
                /* End data old */
                /* End array for write log */

                /* ---------------------------------------------------------------------------------------------- */
                $q                = M_role_access::where('id_group',$id_group)->get();
                $menu_available   = array();
                $button_available = array();

                /* Delete Data role_access & role_button Not Available */
                foreach ($q as $row) {
                    if (!in_array($row->id_menu, (array) $chk)) {
                        $model_role_button = M_role_button::where('id_role_access', $row->id_role_access)->get();
                        if(!empty($model_role_button)){
                            foreach ($model_role_button as $mod_role_button) {
                                $delete = $mod_role_button->delete();
                            }
                        }

                        $model_role_access = M_role_access::where('id_role_access', $row->id_role_access)->get();
                        if(!empty($model_role_access)){
                            foreach ($model_role_access as $mod_role_access) {
                                $delete1 = $mod_role_access->delete();
                            }
                        }
                    } else {
                        $menu_available[$row->id_role_access] = $row->id_menu; // id_menu available just in table role_access
                    }

                    $r = M_role_button::where('id_role_access', $row->id_role_access)->get();
                    foreach ($r as $row2) {
                        if ($row->id_role_access == $row2->id_role_access) {

                            if (!in_array($row2->id_menu_button, (array) $chkbutton)) { /*$chkbutton[$row->id_menu]*/

                                $model_role_button2 = M_role_button::where('id_role_button', $row2->id_role_button)->get();
                                if(!empty($model_role_button2)){
                                    foreach ($model_role_button2 as $mod_role_button2) {
                                        $delete2 = $mod_role_button2->delete();
                                    }
                                }

                            } else {
                                $button_available[$row->id_menu][] = $row2->id_menu_button; // id_menu_button available just in table role_button
                            }
                        }
                    }
                }

                /* ---------------------------------------------------------------------------------------------- */

                if(!empty($chk)){
                    /* Add Data role_access & role_button */
                    foreach ($chk as $key => $value) {
                        if (!in_array($value, (array) $menu_available)) {
                            $model           = new M_role_access;
                            $model->id_group = $id_group;
                            $model->id_menu  = $value;
                            
                            $save            = $model->save();

                            $data['id_role_access'] = M_role_access::max('id_role_access');
                            $menu_available[$data['id_role_access']] = $value; // id_menu all available
                        }
                    }
                }

                foreach ($menu_available as $key => $value) {
                    if (array_key_exists($value, (array) $chkbutton)) {
                        foreach ($chkbutton[$value] as $key2 => $value2) {
                            if (array_key_exists($value, (array) $button_available)) {
                                if (!in_array($value2, (array) $button_available[$value])) {

                                    $model1 = new M_role_button;
                                    $model1->id_role_access = $key;
                                    $model1->id_menu_button = $value2;

                                    $save1 = $model1->save();
                                }

                            } else {

                                $model2 = new M_role_button;
                                $model2->id_role_access = $key;
                                $model2->id_menu_button = $value2;

                                $save2 = $model2->save();
                            }
                        }
                    }
                }

                /* Write Log */
                /* Data New */
                $resultMenuButton = array();
                $resultMenu       = array();
                if(!empty($chk)){
                    foreach ($chk as $key => $value4) {

                        $menu = M_menu::where('id_menu',$value4)->first();
                        
                        $resultMenuButton[] = '<b>' . $menu->menu_name . '</b> => '; 
                        $resultMenu[]  = $menu->menu_name;

                        if(!empty($chkbutton)){
                            foreach ($chkbutton as $key3 => $value5) {
                                if ($key3 == $value4) {
                                    foreach ($value5 as $key4 => $value5) {

                                        $menu_button = M_role_button::leftjoin('menu_button','menu_button.id_menu_button','=','role_button.id_menu_button')
                                                                    ->leftjoin('button','button.id_button','=','menu_button.id_button')
                                                                    ->where('menu_button.id_menu_button',$value5)->first();

                                        $resultMenuButton[] = $menu_button->button_name . ',';
                                    }
                                }
                            }
                        }

                        $resultMenuButton[] = '<br>';
                    }
                }

                $menuAll    = implode(",", $resultMenu);
                $menuButton = implode("", $resultMenuButton);

                $data_new = array(
                                    "ID Role"     => $group->id,
                                    "Role Name"   => $group->name,
                                    "Menu"        => $menuAll,
                                    "Menu Button" => $menuButton,
                                    );
                /* End data new */
                $data_change = array_diff_assoc($data_new, $data_old);
                $message     = 'Managed to change the role permission' . $group->name;
                    $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                /* End write log */

                $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url_succees);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url      = site_url() . $this->site;
            $id       = $this->input->get("id");
            $id_group = decryptID($id);

            $group = M_group::where('groups.id',$id_group)
                            ->leftjoin('role_access', 'role_access.id_group', '=', 'groups.id')
                            ->get();
                            
            $model = M_group::where('id',$id_group)->first();

            if(!empty($group))
            {
                /* Array for write log */
                $resultMenuButton_old = array();
                $resultMenu_old       = array();

                $m_role_access = M_role_access::where('id_group',$id_group)->get();
                foreach ($m_role_access as $key => $val) {

                    $menu                   = M_menu::where('id_menu',$val->id_menu)->first();
                    $resultMenuButton_old[] = '<b>' . $menu->menu_name . '</b> => ';
                    $resultMenu_old[]       = $menu->menu_name;

                    $m_role_button = M_role_button::leftjoin('menu_button','menu_button.id_menu_button','=','role_button.id_menu_button')
                                                    ->leftjoin('button','button.id_button','=','menu_button.id_button')
                                                    ->where('role_button.id_role_access',$val->id_role_access)->get(); 

                    foreach ($m_role_button as $key => $val2) {

                        $resultMenuButton_old[] = $val2->button_name . ',';

                    }

                    $resultMenuButton_old[] = '<br>';
                }

                $menuAll_old    = implode(",", $resultMenu_old);
                $menuButton_old = implode("", $resultMenuButton_old);

                $data_notif = array(
                                "ID Role"     => $model->id,
                                "Role Name"   => $model->name,
                                "Menu"        => $menuAll_old,
                                "Menu Button" => $menuButton_old,
                                );
                /* End array for write log */

                foreach ($group as $value) 
                {
                    $model_role_button = M_role_button::where('id_role_access',$value->id_role_access)->get();
                    if(!empty($model_role_button)){
                        foreach ($model_role_button as $mod_role_button) {
                            $mod_role_button->delete();
                        }
                    }

                }

                $model_role_access = M_role_access::where('id_group',$id_group)->get();

                if(!empty($model_role_access ))
                {
                    foreach ($model_role_access as $m_role_access) 
                    {
                        $m_role_access->delete();
                    }
                }

                /* Write log */
                $message = "Managed to remove role permission " . $model->name;
                $this->activity_log->create(NULL, json_encode($data_notif), NULL, $message, $this->router->fetch_method());
                /* End write log*/

                $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);                
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    function ajax_getPermission()
    {
        if ($this->input->is_ajax_request()) 
        {
            $id = decryptID($this->input->post('id'));
            $q  = M_role_access::selectRaw('GROUP_CONCAT(menu.menu_name) AS permission')
                          ->join('groups', 'groups.id', '=', 'role_access.id_group')
                          ->join('menu', 'menu.id_menu', '=', 'role_access.id_menu')
                          ->where('groups.id',$id)
                          ->get();

            echo json_encode($q);
        } 
    }
}