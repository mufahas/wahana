<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Activation_email extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['activation'] = site_url() . $this->site . '/activation';

        $this->load->view($this->folder ."/". $this->class ."/v_" . $this->class, $data);
    }

    function activation($id="")
    {
        $id_user             = decryptID($id);
        
        /* Url */
        $url_succees         = site_url() . $this->site;
        $url_error           = site_url();

        // $user = $this->model_general_main->getRowDataFromTable('users',array('id' => $id_user, 'active' => '0 '));
        $user = M_user::where('id', $id_user)->where('active','=','0')->first();

        if(!empty($user))
        {

            $model = M_user::find($id_user);

            /* Initialize Data */
            $model->active      = '1';

            /* Update Data menu */
            $update             = $model->save();

            if($update)
            {
                redirect($url_succees);
            }
        }
        else
        {
            redirect($url_error);
        }
    }

}