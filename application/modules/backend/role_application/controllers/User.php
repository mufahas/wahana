<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";

    public function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = str_replace('_','-', $this->router->fetch_class());
        $this->site   = $this->folder . '/' . $this->class;

        /* Load Model */
        $this->load->model('select_global_model');
        $this->load->library('send_email');

    }

    function index()
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['view']      = site_url() . $this->site . '/view';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend","role_application","user","v_" . $this->class, $data);
    }

    /**
    * Serverside load table:role
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "M_user";
        $condition    = "users.is_delete = 'f'";
        $row          = array('users.id','username','email', 'first_name', 'last_name', 'groups.name','active','users.id');
        $row_search   = array('users.id','username','email', 'first_name', 'last_name', 'groups.name','active','users.id');
        $join         = array(
                                'users_groups' => 'users_groups.user_id = users.id',
                                'groups'       => 'groups.id = users_groups.group_id'
                            );
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add()
    {
        /* Button Action */
        $data['action']     = site_url() . $this->site . '/save';
        $data['checkEmail'] = site_url() . $this->site . '/ajax_check_email';

        $data['role']       = $this->select_global_model->selectRoleUser();

        $this->load_view("backend","role_application","user","v_" . $this->class . "_add",$data);
    }

    /**
    * Save data to table:groups
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($this->input->is_ajax_request())
        {
            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/add';

            /* Get Data User */
            $user        = $this->ion_auth->user()->row();

            /* Get Data Post */
            $username    = $this->input->post('username');
            $email       = $this->input->post('email');
            $first_name  = $this->input->post('first_name');
            $last_name   = $this->input->post('last_name');
            $group       = decryptID($this->input->post('role_user'));
            $phone       = $this->input->post('phone_number');
            $password    = $this->rand_string( 8 );
            // $password    = 'password';

            /* Check Email Into Table */
            $check_email = M_user::where('email', $email)->first();

            if(empty($checkEmail))
            {
                $additional_data    = array(
                                                'first_name'    => $first_name,
                                                'last_name'     => $last_name,
                                                'phone'         => $phone,
                                                'active'        => '0'
                                            );

                $groups             = array($group);

                $this->ion_auth->register($username, $password, $email, $additional_data, $groups);

                /* Get Group Name  */
                $list_group         = M_group::where('id', $group)->first();

                /* Get Data User For Sending Mail */
                $data_user = M_user::select('users.id','users.email','users.first_name','users.last_name')->orderBy('users.id','desc')->limit(1)->first();

                if(!empty($data_user))
                {

                    $user_id         = $data_user->id;
                    $email_user      = $data_user->email;
                    $user_first_name = $data_user->first_name;
                    $user_last_name  = $data_user->last_name;

                    $this->sendEmail($user_id, $email_user, $password, $user_first_name, $user_last_name, $list_group->name);

                }


                /* Write Log */
                $data_notif = array(
                                    "First Name"       => $first_name,
                                    "Last Name"        => $last_name,
                                    "Email"            => $email,
                                    "Phone"            => $phone,
                                    "Role"             => $list_group->name,
                                    );

                $message = "Success to add user data " . $first_name . ' ' . $last_name;
                $this->activity_log->create(json_encode($data_notif), NULL, NULL, $message,  $this->router->fetch_method());
                /* End Write Log */

                $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);

            }
            else
            {
                $status = array("status" => "error", "message" => 'Email already exist.', 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function view($id)
    {
        $id_user        = decryptID($id);
        $model          = M_user::join('users_groups', 'users_groups.user_id','=','users.id')
                                  ->join('groups', 'groups.id','=','users_groups.group_id')
                                  ->where('users.id',$id_user)->first();

        if(!empty($model))
        {
            $data['result']     = $model;
            $data['role']       = $this->select_global_model->selectRoleUser();

            /* Button Action */
            $data['action']     = site_url() . $this->site . '/update';

            $this->load_view("backend","role_application","user","v_" . $this->class . "_view",$data);
        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }


    function delete()
    {
        if ($this->input->is_ajax_request())
        {
            $url      = site_url() . $this->site;
            $id       = $this->input->get("id");
            $id_user  = decryptID($id);

            $user     = M_user::where("id",$id_user)->first();

            if(!empty($user))
            {   
                /* Update Data User */
                $user->is_delete   = 't';
                $user->active      = '0';
                $user->delete_date = date('Y-m-d H:i:s');

                $delete            = $user->save();

                if($delete)
                {
                    $users_groups   = M_user_role::where('user_id', $id_user)->first();

                    if(!empty($users_groups))
                    {
                        $users_groups->delete();
                    }

                    /* Write log */
                    $data_notif = array(
                        "ID User"       => $user->id,
                        "First Name"    => $user->first_name,
                        "Last Name"     => $user->last_name,
                        "Email"         => $user->email,
                    );

                    $message = "Managed to remove user " . $user->first_name . ' ' . $user->last_name;
                    $this->activity_log->create(NULL, json_encode($data_notif), NULL, $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    function rand_string( $length )
    {
        $str = "";
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz@#$&*";
        $size = strlen( $chars );
        for( $i = 0; $i < $length; $i++ )
        {
              $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str;

    }

    function ajax_check_email()
    {
        if ($this->input->is_ajax_request())
        {
            $id_user = decryptID($this->input->post('id_user'));
            $email   = $this->input->post('email');
            $result  = M_user::where('email',$email)->first();

            if ($result)
            {
                if ($id_user)
                {
                    if ($id_user == $result->id)
                    {
                        echo 'true';
                    }
                    else
                    {
                        echo 'false';
                    }
                }
                else
                {
                    echo 'false';
                }
            }
            else
            {
                echo 'true';
            }
        }
    }

    public function sendEmail($id,$email,$password,$firstname,$lastname,$role_name)
    {
        $set['id']          = $id;
        $set['email']       = $email;
        $set['password']    = $password;
        $set['first_name']  = $firstname;
        $set['last_name']   = $lastname;
        $set['role_name']   = $role_name;

        $message            = $this->load->view('../modules/backend/role_application/views/user/v_user_email', $set , true, true);

        $this->send_email->email($email, $message);

    }
}