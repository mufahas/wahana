<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends MY_Controller 
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = str_replace('_','-', $this->router->fetch_class());
        $this->site   = $this->folder . '/' . $this->class;
 
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend","role_application","role","v_" . $this->class, $data);
    }

    /**
    * Serverside load table:role
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "M_group";
        $condition    = "is_delete = 'f'";
        $row          = array('id','name','description', 'id');
        $row_search   = array('id','name','description');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend","role_application","role","v_" . $this->class . "_add",$data);
    }

    /**
    * Save data to table:groups
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/add';

            /* Get Data Post */
            $name        = ucwords($this->input->post("name"));
            $description = ucfirst($this->input->post("description"));

            /* Checking data in database */
            $group = M_group::where('name',$name)->first();

            if(empty($group))
            {
                /* Initialize Data */
                $model              = new M_group;
                $model->name        = $name;
                $model->description = $description;

                /* Save */
                $save = $model->save();

                if($save)
                {
                    /* Write Log */
                    $data_notif = array(
                                        "ID Role"     => M_group::max('id'),
                                        "Role Name"   => $name,
                                        "Description" => $description,
                                        );

                    $message = "Managed to added that a role " . $name;
                    $this->activity_log->create(json_encode($data_notif), NULL, NULL, $message,  $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
                }

            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $id_role        = decryptID($id);
        $model          = M_group::where('id',$id_role)->first();

        if(!empty($model))
        {
            $data['result'] = $model;
            /* Button Action */
            $data['action'] = site_url() . $this->site . '/update';
            
            $this->load_view("backend","role_application","role","v_" . $this->class . "_edit",$data);
        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:groups
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($this->input->is_ajax_request()) 
        {   
            $id          = $this->input->post("id");
            $id_group    = decryptID($id);

            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/edit/' . $id;

            /* Get Data Post */
            $name        = ucwords($this->input->post("name"));
            $description = ucfirst($this->input->post("description"));

            /* Checking data in database */
            $group = M_group::where('name',$name)->whereRaw('id != '.$id_group.'')->first();

            if(empty($group))
            {
                $model              = M_group::where('id',$id_group)->first();

                /* Array for write log */
                $data_old = array(
                            "ID Role"     => $model->id,
                            "Role Name"   => $model->name,
                            "Description" => $model->description
                            );

                $data_new = array(
                            "ID Role"     => $id_group,
                            "Role Name"   => $name,
                            "Description" => $description,
                            );
                /* End array for write log */

                /* Initialize Data */
                $model->name        = $name;
                $model->description = $description;

                /* Save */
                $save = $model->save();

                if($save)
                {   
                    /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message     = 'Managed to change the role ' . $name;
                    $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                    /* End Write Log*/

                    $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
                }

            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url      = site_url() . $this->site;
            $id       = $this->input->get("id");
            $id_group = decryptID($id);

            $group = M_group::where("id",$id_group)->first();

            if(!empty($group))
            {
                $group->is_delete   = 't';
                $group->delete_date = date('Y-m-d H:i:s');
                $delete             = $group->save();

                if($delete)
                {
                    /* Write log */
                    $data_notif = array(
                        "ID Role"     => $group->id,
                        "Role Name"   => $group->name,
                        "Description" => $group->description,
                    );

                    $message = "Managed to remove role " . $group->name;
                    $this->activity_log->create(NULL, json_encode($data_notif), NULL, $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
}