@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        View Peserta {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Nama Promo
                        </label>
                        <h5>{{ $result->judul_promo }}</h5>
                    </div>
                    <div class="col-lg-6 ">
                        <label>
                            Nama Peserta:
                        </label>
                        <h5>{{ $result->nama_peserta_promo }}</h5>
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Nomor HP:
                        </label>
                        <h5>{{ $result->no_hp_peserta_promo }}</h5>
                    </div>

                     <div class="col-lg-6 form-group">
                        <label>
                            Email:
                        </label>
                        <h5>{{ $result->email_peserta_promo }}</h5>
                    </div>
                   
                </div>

                <div class="form-group m-form__group row">
                     <div class="col-lg-6 form-group">
                        <label>
                            Tanggal Daftar:
                        </label>
                        <h5>{{ date('d F Y',strtotime($result->date_promo_submit)) }}</h5>
                    </div>

                    <div class="col-lg-6 form-group">
                        <label>
                            Domisili:
                        </label>
                        <h5>{{ $result->domisili_peserta_promo }}</h5>
                    </div>
                </div>
        </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop