@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Detail {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-6 form-group">
                    <label>
                        Nama Peserta:
                    </label>
                    <h5>{{ $result->nama_testrider }}</h5>
                </div>
                <div class="col-lg-6 form-group">
                    <label>
                        Email Peserta:
                    </label>
                    <h5>{{ $result->email_testrider }}</h5>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <div class="col-lg-6 form-group">
                    <label>
                        No Handphone 
                    </label>    
                    <h5> {{ $result->no_telepon_testrider }} </h5>
                </div>
                <div class="col-lg-6 form-group">
                    <label>
                        Tanggal Pendaftaran
                    </label>
                    <h5>{{ date('d F Y', strtotime($result->tgl_pendaftaran)) }}</h5>
                </div>
            </div>


            <div class="form-group m-form__group row">
                <div class="col-lg-6 form-group">
                    <label>
                        Lokasi Testride
                    </label>
                    <h5>{{ $result->lokasi_testride == 'JKT' ? 'Jakarta' : 'Tangerang' }}</h5>
                </div>
                <div class="col-lg-6 form-group">
                    <label>
                        Kendaraan Testride
                    </label>
                    <h5>{{ $result->kendaraan_testride }}</h5>
                </div>
            </div>
        </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/example/example/js/example.js" type="text/javascript"></script>
@stop