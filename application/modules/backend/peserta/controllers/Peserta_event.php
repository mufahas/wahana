<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta_event extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';
        $data['view']      = site_url() . $this->site . '/view';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {   
        $date                 = $this->input->post("start_date");
        $explode              = explode(" / ", $date);
        $start_date           = !empty($explode['0']) ? date('Y-m-d', strtotime($explode['0'])) : date('Y-m-d');
        $end_date             = !empty($explode['1']) ? date('Y-m-d', strtotime($explode['1'])) : date('Y-m-d');
        $condition            = "";
    
        if(!empty($start_date) && !empty($end_date))
        {
            $condition .= "tbl_peserta_event.date_event_submit >= '".$start_date."' AND tbl_peserta_event.date_event_submit <= '".$end_date."'";
        }

        $model        = "tbl_peserta_event";
        $condition    = $condition;
        $row          = array('id_peserta_event','judul_event','nama_peserta_event','no_hp_peserta_event','email_peserta_event','date_event_submit');
        $row_search   = array('id_peserta_event','judul_event','nama_peserta_event','no_hp_peserta_event','email_peserta_event','date_event_submit');
        $join         = array('tbl_event' => 'tbl_event.kode_event = tbl_peserta_event.kode_event');
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {

    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/update';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        
    }

    /**
    * View data from table:
    * @param Id
    * @return page index
    **/
    function view($id)
    {
        $id_peserta_event = decryptID($id);

        $select = 'tbl_peserta_event.kode_event,
                   tbl_peserta_event.no_ktp_peserta_event,
                   tbl_peserta_event.nama_peserta_event,
                   tbl_peserta_event.email_peserta_event,
                   tbl_peserta_event.no_hp_peserta_event,
                   tbl_peserta_event.domisili_peserta_event,
                   tbl_peserta_event.date_event_submit,
                   tbl_event.judul_event';

        $peserta = tbl_peserta_event::selectRaw($select)
                   ->join('tbl_event','tbl_event.kode_event','=','tbl_peserta_event.kode_event')
                   ->where('id_peserta_event',$id_peserta_event)
                   ->first();
        
        if(!empty($peserta))
        {
            $data['result'] = $peserta;
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }
}