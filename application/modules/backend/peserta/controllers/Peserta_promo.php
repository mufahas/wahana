<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta_promo extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';
        $data['view']      = site_url() . $this->site . '/view';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $date                 = $this->input->post("start_date");
        $explode              = explode(" / ", $date);
        $start_date           = !empty($explode['0']) ? date('Y-m-d', strtotime($explode['0'])) : date('Y-m-d');
        $end_date             = !empty($explode['1']) ? date('Y-m-d', strtotime($explode['1'])) : date('Y-m-d');
        $condition            = "";
    
        if(!empty($start_date) && !empty($end_date))
        {
            $condition .= "tbl_peserta_promo.date_promo_submit >= '".$start_date."' AND tbl_peserta_promo.date_promo_submit <= '".$end_date."'";
        }

        $model        = "tbl_peserta_promo";
        $condition    = $condition;
        $row          = array('id_peserta_promo','judul_promo','nama_peserta_promo','no_hp_peserta_promo','email_peserta_promo','date_promo_submit');
        $row_search   = array('id_peserta_promo','judul_promo','nama_peserta_promo','no_hp_peserta_promo','email_peserta_promo','date_promo_submit');
        $join         = array('tbl_promo' => 'tbl_promo.kode_promo = tbl_peserta_promo.kode_promo');
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {

    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/update';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        
    }

    /**
    * View data from table:
    * @param Id
    * @return page index
    **/
    function view($id)
    {
        $id_peserta_promo = decryptID($id);

        $select = 'tbl_peserta_promo.kode_promo,
                   tbl_peserta_promo.no_ktp_peserta_promo,
                   tbl_peserta_promo.nama_peserta_promo,
                   tbl_peserta_promo.email_peserta_promo,
                   tbl_peserta_promo.no_hp_peserta_promo,
                   tbl_peserta_promo.domisili_peserta_promo,
                   tbl_peserta_promo.date_promo_submit,
                   tbl_promo.judul_promo';

        $peserta = tbl_peserta_promo::selectRaw($select)
                   ->join('tbl_promo','tbl_promo.kode_promo','=','tbl_peserta_promo.kode_promo')
                   ->where('id_peserta_promo',$id_peserta_promo)
                   ->first();
        
        if(!empty($peserta))
        {
            $data['result'] = $peserta;
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }
}