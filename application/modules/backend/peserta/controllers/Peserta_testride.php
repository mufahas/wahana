<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta_testride extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['view']      = site_url() . $this->site . '/view';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {   
        $date                 = $this->input->post("start_date");
        $explode              = explode(" / ", $date);
        $start_date           = !empty($explode['0']) ? date('Y-m-d', strtotime($explode['0'])) : date('Y-m-d');
        $end_date             = !empty($explode['1']) ? date('Y-m-d', strtotime($explode['1'])) : date('Y-m-d');
        $condition            = "";
    
        if(!empty($start_date) && !empty($end_date))
        {
            $condition .= "tbl_pendaftar_testride.tgl_pendaftaran >= '".$start_date."' AND tbl_pendaftar_testride.tgl_pendaftaran <= '".$end_date."' AND tbl_pendaftar_testride.is_delete = 'F'";
        }

        $model        = "tbl_pendaftar_testride";
        $condition    = $condition;
        $row          = array('id_pendaftar_testride','nama_testrider','email_testrider','no_telepon_testrider','lokasi_testride','kendaraan_testride');
        $row_search   = array('id_pendaftar_testride','nama_testrider','email_testrider','no_telepon_testrider','lokasi_testride','kendaraan_testride');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page update data
    * @return page
    **/
    function view($id)
    {   
        /* Parameter */
        $id_pendaftar_testride = decryptID($id);

        /* Detail Data */
        $detail = tbl_pendaftar_testride::where('id_pendaftar_testride', $id_pendaftar_testride)->first();
        
        if(!empty($detail))
        {
            $data['result'] = $detail;
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        }
        else
        {
            redirect(site_url() . $this->site);
        }

    }
}