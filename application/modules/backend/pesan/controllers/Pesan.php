<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTableKontak'] = site_url() . $this->site . '/loadTableKontak';
        $data['loadTableGso']  = site_url() . $this->site . '/loadTableGso';
        $data['delete']          = site_url() . $this->site . '/view';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTableKontak()
    {
        $model        = "tbl_pesan";
        $condition    = "tbl_pesan.tipe_pesan = 'K'";
        $row          = array('tbl_pesan.kode_pesan','tbl_pesan.nama_lengkap','tbl_pesan.email','tbl_pesan.nomor_telepon','tbl_pesan.lokasi');
        $row_search   = array('tbl_pesan.kode_pesan','tbl_pesan.nama_lengkap','tbl_pesan.email','tbl_pesan.nomor_telepon','tbl_pesan.lokasi');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTableGso()
    {
        $model        = "tbl_pesan";
        $condition    = "tbl_pesan.tipe_pesan = 'GSO'";
        $row          = array('tbl_pesan.kode_pesan','tbl_pesan.nama_lengkap','tbl_pesan.email','tbl_pesan.nomor_telepon','tbl_pesan.lokasi');
        $row_search   = array('tbl_pesan.kode_pesan','tbl_pesan.nama_lengkap','tbl_pesan.email','tbl_pesan.nomor_telepon','tbl_pesan.lokasi');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";


        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    
    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function view($id)
    {
       $kode_pesan = decryptID($id);
       $pesan      = tbl_pesan::where('kode_pesan',$kode_pesan)->first();

       if(!empty($pesan))
       {    
            /* Get Data */
            $data['pesan'] = $pesan;
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data); 
       } else {
            redirect(site_url() . $this->site);
       }
       
       
    }
}