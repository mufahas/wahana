<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_pesan extends Eloquent {

	public $table      = 'tbl_pesan';
	public $primaryKey = 'kode_pesan';
	public $timestamps = false;

}