@extends('backend.default.views.layout.v_layout')

@section('body')

<link href="{{ base_url() }}assets/default/css/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ base_url() }}assets/default/css/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    {{ get_menu_name() }}
                </h3>
            </div>
        </div>
    </div>

    <div class="m-portlet__body">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#m_tabs_1_1" id="tab_1" aria-expanded="true">
                    Kontak
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#m_tabs_1_2" id="tab_2" aria-expanded="false">
                    GSO
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="m_tabs_1_1" role="tabpanel" aria-expanded="true">
                <table class="table table-striped table-bordered table-hover" id="table_kontak" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>No. Telepon</th>
                            <th>Lokasi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="tab-pane" id="m_tabs_1_2" role="tabpanel" aria-expanded="false">
                <table class="table table-striped table-bordered table-hover" id="table_gso" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>No. Telepon</th>
                            <th>Lokasi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    var loadTableKontak = '{{$loadTableKontak}}';
    var loadTableGso    = '{{$loadTableGso}}';
    var view_           = '{!! $btn["btn_view"] !!}';

    $('#tab_1').click(function(){
        $('#m_tabs_1_1').show();
        $('#m_tabs_1_2').hide();
    });
    $('#tab_2').click(function(){
        $('#m_tabs_1_1').hide();
        $('#m_tabs_1_2').show();
    });
</script>

<script src="{{ base_url() }}assets/default/js/datatables.min.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/default/js/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/backend/js/handler-datatable.js" type="text/javascript"></script>

<script src="{{ base_url() }}assets/backend/js/pesan/table/table-pesan.js" type="text/javascript"></script>
@stop