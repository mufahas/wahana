<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:tbl_pertanyaan
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "tbl_pertanyaan";
        $condition    = "tbl_pertanyaan.dihapus = 'F'";
        $row          = array('tbl_pertanyaan.kode_pertanyaan', 'tbl_pertanyaan.pertanyaan','tbl_pertanyaan.jawaban');
        $row_search   = array('tbl_pertanyaan.kode_pertanyaan', 'tbl_pertanyaan.pertanyaan','tbl_pertanyaan.jawaban');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table: tbl_pertanyaan
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/add';
            
            /* Get Data Post */
            $pertanyaan  = $this->input->post('pertanyaan');
            $jawaban     = $this->input->post('jawaban');

            /* check in table available or not */
            $tbl_pertanyaan   = tbl_pertanyaan::where('pertanyaan',$pertanyaan)->first();

            if(empty($tbl_pertanyaan))
            {
                $model = new tbl_pertanyaan;

                $model->pertanyaan = $pertanyaan;
                $model->jawaban    = $jawaban;

                $save = $model->save();

                if($save)
                {
                    /* Write Log */
                    $data_notif = array(
                                        "Kode Pertanyaan" => tbl_pertanyaan::max('kode_pertanyaan'),
                                        "Pertanyaan"      => $pertanyaan,
                                        "Jawaban"         => $jawaban,
                                        );

                    $message = "Berhasil menambahkan faq " . $pertanyaan;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);

                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
                }          
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $kode_pertanyaan = decryptID($id);
        $tbl_pertanyaan  = tbl_pertanyaan::where('kode_pertanyaan',$kode_pertanyaan)->first();

        if(!empty($tbl_pertanyaan))
        {
            /* Button Action */
            $data['action']         = site_url() . $this->site . '/update';
            
            $data['tbl_pertanyaan'] = $tbl_pertanyaan;

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:tbl_pertanyaan
    * @param Post Data
    * @return page index
    **/
    function update()
    {   
        if ($this->input->is_ajax_request()) 
        {   
            $id              = $this->input->post("id");
            $kode_pertanyaan = decryptID($id);

            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/edit/' . $id;

            /* Get Data Post */
            $pertanyaan = $this->input->post("pertanyaan");
            $jawaban    = $this->input->post("jawaban");

            /* check in table available or not */
            $tbl_pertanyaan = tbl_pertanyaan::where('pertanyaan',$pertanyaan)->whereRaw('kode_pertanyaan != '.$kode_pertanyaan.'')->first();

            if(empty($tbl_pertanyaan))
            {
                $model = tbl_pertanyaan::where('kode_pertanyaan',$kode_pertanyaan)->first();

                /* Array for write log */
                $data_old = array(
                            "Kode Pertanyaan" => $model->kode_pertanyaan,
                            "Pertanyaan"      => $model->pertanyaan,
                            "Jawaban"         => $model->jawaban
                            );

                $data_new = array(
                            "Kode Pertanyaan" => $kode_pertanyaan,
                            "Pertanyaan"      => $pertanyaan,
                            "Jawaban"         => $jawaban,
                            );
                /* End array for write log */

                /* Initialize Data */
                $model->pertanyaan = $pertanyaan;
                $model->jawaban    = $jawaban;

                /* Save */
                $save = $model->save();

                if($save)
                {
                    /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message     = 'Memperbarui faq ' . $pertanyaan;
                    $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                    /* End Write Log*/

                    $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * Delete data from table:tbl_pertanyaan
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url                 = site_url() . $this->site;
            $id                  = $this->input->get("id");
            $kode_pertanyaan = decryptID($id);

            $tbl_pertanyaan = tbl_pertanyaan::where("kode_pertanyaan",$kode_pertanyaan)->first();

            if(!empty($tbl_pertanyaan))
            {
                $user      = $this->ion_auth->user()->row();

                $tbl_pertanyaan->dihapus       = 'T';
                $tbl_pertanyaan->tanggal_hapus = date('Y-m-d H:i:s');
                $tbl_pertanyaan->dihapus_oleh  = $user->first_name . ' ' . $user->last_name;
                $delete                        = $tbl_pertanyaan->save();

                if($delete)
                {
                    /* Write log */
                    $data_notif = array(
                        "Kode Pertanyaan" => $kode_pertanyaan,
                        "Pertanyaan"      => $tbl_pertanyaan->pertanyaan,
                        "Jawaban"         => $tbl_pertanyaan->jawaban,
                    );

                    $message = "Menghapus faq " . $tbl_pertanyaan->pertanyaan;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
}