<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_pertanyaan extends Eloquent {

	public $table      = 'tbl_pertanyaan';
	public $primaryKey = 'kode_pertanyaan';
	public $timestamps = false;

}