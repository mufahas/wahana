<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        $this->load->helper('time2str');
    }

    function index() 
    {
        $data['view']       = site_url() . $this->site . '/view/';

        /* get file name in logs folder */
        $directory         = FCPATH . "application/logs/log_activity/";
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        
        $remove_log_       = str_replace('log_','',$scanned_directory);
        $remove_txt        = str_replace('.txt','',$remove_log_);
        $menu_name         = str_replace('_',' ',$remove_txt);
        
        $data['logs']      = $menu_name;

        /* untuk fungsi ambil */

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Direct to page view data
    * @return page
    **/
    function view($menu_name)
    {
        $data['detail'] = site_url() . $this->site . '/detail/';
        $data['title']  = ucwords(str_replace('_',' ',$menu_name));
        $data['views']  = $this->activity_log->contents('',$menu_name);

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Direct to page view data
    * @return page
    **/
    function detail($id, $menu_name) 
    {
        $data['title'] = ucwords(str_replace('_',' ',$menu_name));
        $data['log']   = $this->activity_log->contents($id,$menu_name);

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }
}