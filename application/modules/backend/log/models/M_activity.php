<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_activity extends Eloquent {

	public $table      = 'logs';
	public $primaryKey = 'id_logs';
	public $timestamps = false;

}