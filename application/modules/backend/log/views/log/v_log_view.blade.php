@extends('backend.default.views.layout.v_layout')

@section('body')

<link href="{{ base_url() }}assets/default/css/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ base_url() }}assets/default/css/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    {{ get_menu_name() }} - {{ $title }}
                </h3>
            </div>
        </div>
    </div>

    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <table class="table table-striped table-bordered table-hover" id="table">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Actor</th>
                    <th>Role</th>
                    <th>Activities</th>
                    <th>Description</th>
                    <th>Time</th>
                    <th width="10">Action</th>
                </tr>
            </thead>
            <tbody>

                @php 
                    $no = 1 
                @endphp

                @foreach(array_reverse($views) as $log)

                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $log->actor }}</td>
                        <td>{{ $log->role }}</td>
                        <td>{{ $log->activity }}</td>
                        <td>{{ $log->message }}</td>
                        <td>{{ time2str($log->created_on) }}</td>
                        <td align="center"><a href="{{ $detail }}{{ $log->id }}/{{ str_replace(' ','_',strtolower($log->menu)) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Detail "><i class="la la-eye"></i></a></td>
                    </tr>

                    @php 
                        $no++ 
                    @endphp

                @endforeach
            </tbody>
        </table>
        <!--end: Datatable -->
    </div>
</div>

@stop

@section('scripts')
<script src="{{ base_url() }}assets/default/js/datatables.min.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/default/js/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/backend/js/handler-datatable.js" type="text/javascript"></script>

<script src="{{ base_url() }}assets/backend/js/log/table/table-log.js" type="text/javascript"></script>
@stop