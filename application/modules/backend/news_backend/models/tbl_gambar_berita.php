<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_gambar_berita extends Eloquent {

	public $table      = 'tbl_gambar_berita';
	public $primaryKey = 'kode_gambar_berita';
	public $timestamps = false;

}