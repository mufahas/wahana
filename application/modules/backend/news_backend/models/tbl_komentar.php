<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_komentar extends Eloquent {

	public $table      = 'tbl_komentar';
	public $primaryKey = 'kode_komentar';
	public $timestamps = false;

}