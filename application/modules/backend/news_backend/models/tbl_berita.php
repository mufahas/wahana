<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_berita extends Eloquent {

	public $table      = 'tbl_berita';
	public $primaryKey = 'kode_berita';
	public $timestamps = false;

}