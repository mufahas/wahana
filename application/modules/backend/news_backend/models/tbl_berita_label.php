<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_berita_label extends Eloquent {

	public $table      = 'tbl_berita_label';
	public $primaryKey = 'kode_berita_label';
	public $timestamps = false;

}