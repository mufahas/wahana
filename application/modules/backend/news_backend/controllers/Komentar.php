<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Komentar extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "tbl_komentar";
        $condition    = "";
        $row          = array('tbl_komentar.kode_komentar','tbl_komentar.nama','tbl_komentar.email');
        $row_search   = array('tbl_komentar.kode_komentar','tbl_komentar.nama','tbl_komentar.email');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {

    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {   
        $kode_komentar = decryptID($id);
        $tbl_komentar  = tbl_komentar::
                         leftJoin('tbl_berita','tbl_berita.kode_berita','=','tbl_komentar.kode_berita')
                        ->where('kode_komentar',$kode_komentar)->first();

        if(!empty($tbl_komentar)) {
            /* Button Action */
            $data['action']         = site_url() . $this->site . '/update';
            $data['url_succees']    = site_url() . $this->site;

            $data['komentar']       = $tbl_komentar; 
            $data['label']          = tbl_berita_label::leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                      ->where('tbl_berita_label.kode_berita',$tbl_komentar->kode_berita)->get(); 
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        } else {
            redirect(site_url() . $this->site);
        }

    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        $id            = $this->input->post('id');
        $kode_komentar = decryptID($id);
        
        /* Get User */
        $user          = $this->ion_auth->user()->row();

        /* Url */
        $url_succees   = site_url() . $this->site;
        $url_error     = site_url() . $this->site . '/edit/' . $id; 

        /* Get Data Post */
        $status_komentar = $this->input->post('status_komentar');
        
        $komentar = tbl_komentar::where('kode_komentar',$kode_komentar)->first();
        
        if(!empty($komentar))
        {
            $model = tbl_komentar::where('kode_komentar',$kode_komentar)->first();

            /* Begin Array Write Log */
            $data_old = array(
                                "Nama"              => $model->nama,
                                "Email"             => $model->email,
                                "Status Komentar"   => empty($model->status_komentar) ? 'Tolak' : 'Terima',
                                "Disetujui Oleh"    => $model->disetujui_oleh,
                                "Tanggal Disetujui" => $model->tgl_disetujui,
                            );

            $data_new = array(
                                "Nama"              => $model->nama,
                                "Email"             => $model->email,
                                "Status Komentar"   => empty($model->status_komentar) ? 'Tolak' : 'Terima',
                                "Disetujui Oleh"    => $user->username,
                                "Tanggal Disetujui" => date('Y-m-d H:i:s'),
                            );
            /* End Array Write Log */

            $model->status_komentar  = empty($status_komentar) ? 'F' : 'T';
            $model->tgl_disetujui    = date('Y-m-d H:i:s');
            $model->disetujui_oleh   = $user->username;
            
            /* Update  */
            $save                    = $model->save();

            if($save)
            {
                /* Write Log */
                $data_change = array_diff_assoc($data_new, $data_old);
                $message     = 'Menyetujui Komentar ' . $model->komentar;
                $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                /* End Write Log*/

                $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
            } else {
                $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
            }
        } else{
            $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        
    }
}