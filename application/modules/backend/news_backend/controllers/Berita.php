<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        //Model
        $this->load->model('select_global_model');
    }

    function index() 
    {
      
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';
        $data['index']     = site_url() . $this->site . '/index';
        $data['default']   = site_url() . $this->site;

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    // function loadTable($tgl_mulai = "", $tgl_akhir = "")
    // {
    //     $where        = "";
    //     if (!empty($tgl_mulai) AND !empty($tgl_akhir)) {
    //         $where .= " tbl_berita.tanggal_publikasi >= '".$tgl_mulai." 00:00:00' AND tbl_berita.tanggal_publikasi <= '".$tgl_akhir." 23:59:59'";
    //     }
    //     $model        = "tbl_berita";
    //     $condition    = $where;
    //     $row          = array('tbl_berita.kode_berita','tbl_berita.judul_berita','tbl_berita.berita_utama','tbl_berita.tanggal_publikasi');
    //     $row_search   = array('tbl_berita.kode_berita','tbl_berita.judul_berita','tbl_berita.berita_utama', 'tbl_berita.tanggal_publikasi');
    //     $join         = "";
    //     $order        = "";
    //     $groupby      = "";
    //     $limit        = "";
    //     $offset       = "";
    //     $distinct     = "";

    //     /* Get Data */
    //     $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
    //     return $q;
    // }

    function loadTable()
    {
        $date                 = $this->input->post("start_date");
        $explode              = explode(" / ", $date);
        $start_date           = !empty($explode['0']) ? date('Y-m-d', strtotime($explode['0'])) : array();
        $end_date             = !empty($explode['1']) ? date('Y-m-d', strtotime($explode['1'])) : array();
        $condition            = "";

        if(!empty($start_date) && !empty($end_date))
        {
            $condition .= "tbl_berita.tanggal_publikasi >= '".$start_date."' AND tbl_berita.tanggal_publikasi <= '".$end_date."'";
        }
      
       
        $model        = "tbl_berita";
        $condition    = $condition;
        $row          = array('tbl_berita.kode_berita','tbl_berita.judul_berita','tbl_berita.berita_utama','tbl_berita.tanggal_publikasi');
        $row_search   = array('tbl_berita.kode_berita','tbl_berita.judul_berita','tbl_berita.berita_utama', 'tbl_berita.tanggal_publikasi');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['url_succees']     = site_url() . $this->site;
        $data['action']          = site_url() . $this->site . '/save';

        $data['checkTitle']      = site_url() . $this->site . '/ajax_check_title';
        $data['checkUrutan']     = site_url() . $this->site . '/ajax_check_urutan';
        
        /* Get Select Dropdown For Kategori Berita */
        $data['kategori_berita'] = $this->select_global_model->selectKategoriBerita();
        $data['berita_utama']    = $this->select_global_model->selectBeritaUtama();
        $data['label']           = $this->select_global_model->selectLabel();
        $data['publikasi']       = $this->select_global_model->selectPublish();

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    { 
        
        /* Url */
        $url_succees         = site_url() . $this->site;
        $url_error           = site_url() . $this->site . '/add';
        
        /* Get Data Post */
        $user                = $this->ion_auth->user()->row();
        $kategori_berita     = $this->input->post('kategori_berita');
        $label_berita        = $this->input->post('label_berita');
        $kode_label_berita   = explode(",",$label_berita);
        $title               = ucwords($this->input->post('title'));
        $content             = $this->input->post('content');
        $deskripsi           = $this->input->post('deskripsi');
        $url                 = clean_url(strtolower($title));
        // $date                = date('Y-m-d H:i:s');
        
        $berita_utama        = $this->input->post('berita_utama');
        $urutan_berita_utama = $this->input->post('urutan_berita');
        $status_publikasi    = $this->input->post('status_publikasi');
        if(!empty($this->input->post('datePub')))
        {
            $date = date('Y-m-d H:i:s',strtotime($this->input->post('datePub')));
        }
        else
        {
            $date = null;
        }

        /* Post Upload File */
        $userfile                 = $_FILES["file"]["name"];
        $userfile_size            = $_FILES["file"]["size"];
        $userfile_tmp_name        = $_FILES["file"]["tmp_name"];

        /* check in table available or not */
        $tbl_berita   = tbl_berita::where('judul_berita',$title)->first();

        if(empty($tbl_berita))
        {
            $model = new tbl_berita;

            $model->kategori_berita     = $kategori_berita;
            $model->judul_berita        = $title;
            $model->judul_berita_url    = $url;
            $model->isi_berita          = $content;
            $model->deskripsi_berita    = $deskripsi;
            $model->berita_utama        = empty($berita_utama) ? null : $berita_utama;
            $model->urutan_berita_utama = $urutan_berita_utama;
            $model->status_publikasi    = $status_publikasi;
            $model->tanggal_publikasi   = $date;
            $model->dipublikasi_oleh    = $user->username;

            $save = $model->save();

            if($save)
            {   
                /* Begin Save Data Label Berita ke Table tbl_label_berita */
                foreach ($kode_label_berita as $label) {
                    $model_label_berita              = new tbl_berita_label;
                    
                    $model_label_berita->kode_berita = tbl_berita::max('kode_berita');
                    $model_label_berita->kode_label  = decryptID($label);
                    
                    $save_model_label_berita         = $model_label_berita->save();
                }
                /* End Save Data Label Berita ke Table tbl_label_berita */


                /* Begin Save Data Foto Ke Table tbl_gambar_berita */
                foreach ($userfile as $gambar => $file) {
                    $filename                 = $file;
                    $file_basename            = substr($filename, 0, strripos($filename, '.')); // get file extention
                    $file_ext                 = substr($filename, strripos($filename, '.')); // get file name
                    $filesize                 = $userfile_size[$gambar];
                    $allowed_file_types       = array('.jpg','.jpeg','.png');

                    if (in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 2000000) {

                        // Rename file
                        $newfilename = 'BERITA_'. time() . '_'. md5($filename) . $file_ext;

                        if(file_exists("assets/upload/berita/" . $newfilename)) {
                            
                            // file already exists error
                            // echo "You have already uploaded this file.";
                            $status = array('status' => 'error', 'message' => lang('message_save_failed') . ' ' . $filename);
                        } else {
                            if(move_uploaded_file($userfile_tmp_name[$gambar], "assets/upload/berita/" . $newfilename))
                            {
                                $model_gambar_berita                = new tbl_gambar_berita; 
                                
                                $model_gambar_berita->kode_berita   = tbl_berita::max('kode_berita');
                                $model_gambar_berita->gambar_berita = $newfilename;

                                $save_gambar_berita = $model_gambar_berita->save();

                                $status = array('status' => 'success','message' => lang('message_save_success') . ' ' . $filename);
                            }
                        }
                    } else if(empty($file_basename)) {
                        // file selection error
                        // echo "Please select a file to upload.";
                        $status = array('status' => 'error', 'message' => lang('message_save_failed'));
                    } else if($filesize >= 2000000) {
                        // file size error
                        // echo "The file you are trying to upload is too large.";
                        $status = array('status' => 'error', 'message' => lang('message_save_failed'));
                    }
                    else {
                        // file type error
                        //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
                        $status = array('status' => 'error', 'message' => lang('message_save_failed'));
                        unlink($userfile_tmp_name[$gambar]);
                    }
                }
                /* End Save Data Foto Ke Table tbl_gambar_berita */

                /* Write Log */
                $data_notif = array(
                                    "Judul Berita"        => $title,
                                    "Judul Berita Url"    => $url,
                                    "Isi Berita"          => $content,
                                    "Tanggal Publikasi"   => $date,
                                    "Dipublikasikan Oleh" => $user->first_name.' '.$user->last_name
                                    );

                $message = "Berhasil menambahkan berita " . $title;
                $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                /* End Write Log */
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
            }          
        }
        else
        {
            $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
        
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {   

        $news_code = decryptID($id);
        $news      = tbl_berita::selectRaw(
                        'tbl_berita.kode_berita,
                         tbl_berita.kategori_berita,
                         tbl_berita.judul_berita,
                         tbl_berita.judul_berita_url,
                         tbl_berita.isi_berita,
                         tbl_berita.deskripsi_berita,
                         tbl_berita.berita_utama,
                         tbl_berita.urutan_berita_utama,
                         tbl_berita.tanggal_publikasi,
                         tbl_berita.dipublikasi_oleh,
                         tbl_berita.tanggal_terakhir_diubah,
                         tbl_berita.status_publikasi
                        ')
                    // ->leftJoin('tbl_berita_label','tbl_berita_label.kode_berita','=','tbl_berita.kode_berita')
                    // ->leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                    // ->leftJoin('tbl_gambar_berita','tbl_gambar_berita.kode_berita','=','tbl_berita.kode_berita')
                    ->where('tbl_berita.kode_berita',$news_code)->first();

        if(!empty($news))
        {
            /* Button Action */
            $data['checkTitle']      = site_url() . $this->site . '/ajax_check_title';
            $data['checkTitleUrl']   = site_url() . $this->site . '/ajax_check_title_url';
            $data['checkUrutan']     = site_url() . $this->site . '/ajax_check_urutan';
            $data['getListLabel']     = site_url() . $this->site . '/ajax_list_label';
            $data['getImage']        = site_url() . $this->site . '/getImage/';
            $data['remove_image']    = site_url() . $this->site . '/remove_image/';
            $data['path_image']      = site_url() . 'assets/upload/berita/';
            $data['url_succees']     = site_url() . $this->site;

           
            /* Get Select Dropdown For Kategori Berita */
            $data['kategori_berita'] = $this->select_global_model->selectKategoriBerita();
            $data['berita_utama']    = $this->select_global_model->selectBeritaUtama();
            $data['label']           = ms_label::where('dihapus','F')->get();
            $data['publikasi']       = $this->select_global_model->selectPublish();
            
            $berita_label    =  tbl_berita_label::leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                        ->where('tbl_berita_label.kode_berita',$news->kode_berita)->get();
            $arr_berita_label = array();                            
            foreach ($berita_label as $key) 
            {
                array_push($arr_berita_label,$key->kode_label);
            }
           
            $data['array_berita_label'] = $arr_berita_label;
            $data['action']          = site_url() . $this->site . '/update';
            $data['news']            = $news;
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        $id               = $this->input->post("id");
        $news_code        = decryptID($id);
        
        /* Url */
        $url_succees      = site_url() . $this->site;
        $url_error        = site_url() . $this->site . '/edit/' . $id;
        
        /* Get Data Post */
        $user             = $this->ion_auth->user()->row();
        $kategori_berita  = $this->input->post('kategori_berita');
        $label            = $this->input->post('label[]');
        $title            = ucwords($this->input->post('title'));
        $url              = clean_url(strtolower($this->input->post('title_url')));
        $content          = $this->input->post('content');
        $deskripsi        = $this->input->post('deskripsi');
        // $url              = clean_url(strtolower($title));
        // $date             = date('Y-m-d');
        $berita_utama     = $this->input->post('berita_utama');
        $urutan_berita    = $this->input->post('urutan_berita');
        $status_publikasi = $this->input->post('status_publikasi');
        $check_file       = $_FILES;

        if(!empty($this->input->post('date')))
        {
            $date = date('Y-m-d H:i:s',strtotime($this->input->post('date')));
        }
        else
        {
            $date = null;
        }

        /* check in table available or not */
        // $news = tbl_berita::where('judul_berita',$title)->whereRaw('kode_berita != '.$news_code.'')->first();
        $news = tbl_berita::whereRaw('(judul_berita = "'.$title.'" OR judul_berita_url = "'.$url.'") AND kode_berita != '.$news_code)->first();

        if(empty($news))
        {
            $model = tbl_berita::where('kode_berita',$news_code)->first();

            /* Array for write log */
            $data_old = array(
                        "Judul Berita"        => $model->judul_berita,
                        "Kategori Berita"     => ($model->kategori_berita == 'B' ? 'Berita' : ($model->kategori_berita == 'T' ? 'Tips' : 'Event')),
                        "Judul Berita Url"    => $model->judul_berita_url,
                        "Isi Berita"          => $model->isi_berita,
                        "Tanggal Publikasi"   => $model->tanggal_publikasi,
                        "Dipublikasikan Oleh" => $model->dipublikasi_oleh
                    );

            $data_new = array(
                        "Judul Berita"        => $title,
                        "Kategori Berita"     => ($model->kategori_berita == 'B' ? 'Berita' : ($model->kategori_berita == 'T' ? 'Tips' : 'Event')),
                        "Judul Berita Url"    => $url,
                        "Isi Berita"          => $content,
                        "Tanggal Publikasi"   => $date,
                        "Dipublikasikan Oleh" => $user->first_name.' '.$user->last_name
                    );
            /* End array for write log */

            /* Write Log */
            $data_change = array_diff_assoc($data_new, $data_old);
            $message     = 'Memperbarui berita ' . $title;
            $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
            /* End Write Log*/

            /* Initialize Data */

            $model->kategori_berita         = $kategori_berita;
            $model->judul_berita            = $title;
            $model->judul_berita_url        = $url;
            $model->isi_berita              = $content;
            $model->deskripsi_berita        = $deskripsi;
            $model->berita_utama            = empty($berita_utama) ? null : $berita_utama;;
            $model->urutan_berita_utama     = $urutan_berita;
            $model->status_publikasi        = $status_publikasi;
            $model->tanggal_publikasi       = $date;
            $model->dipublikasi_oleh        = $status_publikasi == 'T' ? $user->username : null;
            $model->tanggal_terakhir_diubah = date('Y-m-d H:i:s');

            /* Save */
            $save = $model->save();

            if($save)
            {
                /* Begin Save Data Label Berita ke Table tbl_label_berita */
                $delete_label = tbl_berita_label::where('tbl_berita_label.kode_berita',$news_code)->delete();
                
                if(!empty($label)) { 
                    foreach ($label as $label_) {
                        $model_label_berita              = new tbl_berita_label;

                        $model_label_berita->kode_berita = $news_code;
                        $model_label_berita->kode_label  = decryptID($label_);
                        
                        $save_model_label_berita         = $model_label_berita->save();
                    }
                }
                
                 

                /* End Save Data Label Berita ke Table tbl_label_berita */

                if(!empty($check_file))
                {
                    $userfile                     = $_FILES["file"]["name"];
                    $userfile_size                = $_FILES["file"]["size"];
                    $userfile_tmp_name            = $_FILES["file"]["tmp_name"];

                    foreach ($userfile as $key => $file) {
                        $filename           = $file;
                        $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
                        $file_ext           = substr($filename, strripos($filename, '.')); // get file name
                        $filesize           = $userfile_size[$key];
                        $allowed_file_types = array('.jpg','.jpeg','.png');  

                        // Rename file
                        $newfilename = 'BERITA_'. time() . '_'. md5($filename) . $file_ext;

                        if (in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 2000000)
                        {
                            if (file_exists("assets/upload/berita/" . $newfilename)) {
                                // file already exists error
                                // echo "You have already uploaded this file.";
                                $status = array('status' => 'error', 'message' => lang('message_update_failed') . ' ' . $filename);
                            } else {

                                if(move_uploaded_file($userfile_tmp_name[$key], "assets/upload/berita/" . $newfilename)) {
                                    $model_gambar_berita                = new tbl_gambar_berita; 
                                    
                                    $model_gambar_berita->kode_berita   = $news_code;
                                    $model_gambar_berita->gambar_berita = $newfilename;

                                    $save_gambar_berita = $model_gambar_berita->save();

                                    //echo "File uploaded successfully.";     
                                    $status = array('status' => 'success','message' => lang('message_update_success') . ' ' . $filename);
                                }
                            }

                        } else if(empty($file_basename)) {

                            // file selection error
                            // echo "Please select a file to upload.";
                            $status = array('status' => 'error', 'message' => lang('message_update_failed'));
                        
                        } else if($filesize >= 2000000) {
                            // file size error
                            // echo "The file you are trying to upload is too large.";
                            $status = array('status' => 'error', 'message' => lang('message_update_failed'));
                        } else {
                            // file type error
                            //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
                            $status = array('status' => 'error', 'message' => lang('message_update_failed'));
                            unlink($userfile_tmp_name[$key]);
                        }
                    }
                } else {
                    $status = array('status' => 'success','message' => lang('message_update_success'));
                }

            } else {
               $status = array('status' => 'error', 'message' => lang('message_update_failed'));
            }
        }
        else {
            $status = array('status' => 'error', 'message' => 'URL berita atau Judul berita sudah digunakan.', 'url' => $url_error);
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    /**
    * Direct to page detail
    * @return page
    **/
    function view($id)
    {

        $news_code = decryptID($id);
        $news      = tbl_berita::where('kode_berita',$news_code)->first();

        if(!empty($news_code))
        {
            /* Button Action */
            $data['list_label']     = tbl_berita_label::leftJoin('ms_label','ms_label.kode_label','=','tbl_berita_label.kode_label')
                                        ->where('tbl_berita_label.kode_berita',$news->kode_berita)->get();
            $data['gambar_berita']  = tbl_gambar_berita::where('kode_berita',$news->kode_berita)->get();                      
            $data['path_foto']      = base_url() . "assets/upload/berita/";
        
            $data['news']           = $news;

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Validate title
    * @param title
    * @return boolean
    **/
    function ajax_check_title()
    {
        if ($this->input->is_ajax_request()) 
        { 
            $id     = decryptID($this->input->post('id'));
            $title  = $this->input->post('title');
            $result = tbl_berita::where('judul_berita',$title)->first();

            if($title)
            {
                if ($result) 
                {
                    if ($id) 
                    {
                        if ($id == $result->kode_berita) 
                        {
                            echo 'true';
                        } 
                        else 
                        {
                            echo 'false';
                        }
                    } 
                    else 
                    {
                        echo 'false';
                    }
                } 
                else 
                {
                    echo 'true';
                }
            }
            else
            {
                echo 'false';
            }
        }
    }
    
    /**
    * Validate title URL
    * @param title URL
    * @return boolean
    **/
    function ajax_check_title_url()
    {
        if ($this->input->is_ajax_request()) 
        { 
            $id        = decryptID($this->input->post('id'));
            $title_url = $this->input->post('title_url');
            $result    = tbl_berita::where('judul_berita_url',$title_url)->first();

            if($title_url)
            {
                if ($result) 
                {
                    if ($id) 
                    {
                        if ($id == $result->kode_berita) 
                        {
                            echo 'true';
                        } 
                        else 
                        {
                            echo 'false';
                        }
                    } 
                    else 
                    {
                        echo 'false';
                    }
                } 
                else 
                {
                    echo 'true';
                }
            }
            else
            {
                echo 'false';
            }
        }
    }

    /*
    * Validate urutan berita
    * @param urutan berita
    * @return boolean
    **/
    function ajax_check_urutan()
    {
        if ($this->input->is_ajax_request()) 
        { 
            $id            = decryptID($this->input->post('id'));
            $urutan_berita = $this->input->post('urutan_berita');
            $result        = tbl_berita::where('urutan_berita_utama',$urutan_berita)->first();

            if($urutan_berita)
            {
                if ($result) 
                {
                    if ($id) 
                    {
                        if ($id == $result->kode_berita) 
                        {
                            echo 'true';
                        } 
                        else 
                        {
                            echo 'false';
                        }
                    } 
                    else 
                    {
                        echo 'false';
                    }
                } 
                else 
                {
                    echo 'true';
                }
            }
            else
            {
                echo 'false';
            }
        }
    }

    function getImage($kode_berita)
    {
       $tbl_gambar_berita = tbl_gambar_berita::where('kode_berita', decryptID($kode_berita))->get();
       echo json_encode($tbl_gambar_berita);
    }

    function remove_image($kode_berita, $gambar_berita)
    {
        if (file_exists("assets/upload/berita/" . $gambar_berita))
        {
            if(unlink("assets/upload/berita/" . $gambar_berita))
            {
                $tbl_gambar_berita = tbl_gambar_berita::where('kode_berita',decryptID($kode_berita))
                                                                ->where('gambar_berita',$gambar_berita)
                                                                ->first();
                if(!empty($tbl_gambar_berita)){
                    $tbl_gambar_berita->delete();
                }

                $status = array('status' => 'success', 'message' => 'Gambar berhasil dihapus.');
            }else{
                $status = array('status' => 'error', 'message' => 'Gambar gagal dihapus.');
            }
        }
        else
        {
            $tbl_gambar_berita = tbl_gambar_berita::where('kode_berita',decryptID($kode_berita))
                                                                ->where('gambar_berita',$gambar_berita)
                                                                ->first();
            if(!empty($tbl_gambar_berita)){
                $tbl_gambar_berita->delete();
            }

            $status = array('status' => 'success', 'message' => 'Gambar berhasil dihapus.');
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
}