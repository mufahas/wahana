@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            Kategori Berita:
                        </label>
                        <h5>{{ ($komentar->kategori_berita == 'B' ? 'Berita' : ($komentar->kategori_berita == 'T' ? 'Tips' : 'Event')) }}</h5>
                    </div>

                    <div class="col-lg-6 ">
                        <label>
                            Label Berita:
                        </label>
                        <h5>
                            @foreach ($label as $key)
                            {{ $key->nama_label . ' ,' }}
                            @endforeach
                        </h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 ">
                        <label>
                            Judul Berita:
                        </label>
                        <h5>{{ $komentar->judul_berita }}</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 ">
                        <label>
                            Isi Berita:
                        </label>
                       <h5>{{ $komentar->isi_berita }}<h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label>
                            Nama
                        </label>
                        <h5>{{ $komentar->nama }}</h5>
                    </div>
                    <div class="col-lg-6">
                        <label>
                            Email
                        </label>
                        <h5>{{ $komentar->email }}</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-12">
                        <label>
                            Isi Komentar
                        </label>
                        <h5>{{ $komentar->isi_komentar }}</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-12">
                        <label>
                            Terima / Tolak Komentar
                        </label>
                    </div>
                    <div class="col-lg-3">
                        @if($komentar->status_komentar == 'T')
                            {!! form_input(array('type' => 'checkbox','name' => 'status_komentar','data-on-text'=> 'Setuju', 'data-switch' => 'true', 'id' => 'm_switch_1', 'checked' => 'checked', 'value' => 'F','data-off-text'=>'Tolak', 'data-on-color'=>'danger' )) !!}
                        @else
                            {!! form_input(array('type' => 'checkbox','name' => 'status_komentar','data-on-text'=> 'Setuju', 'data-switch' => 'true', 'id' => 'm_switch_1',  'value' => 'T','data-off-text'=>'Tolak', 'data-on-color'=>'danger' )) !!}
                        @endif
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_update"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($komentar->kode_komentar) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/berita/js/komentar.js" type="text/javascript"></script>
@stop