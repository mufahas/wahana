@extends('backend.default.views.layout.v_layout')

@section('body')

<style type="text/css">
    .m-portlet.m-portlet--creative .m-portlet__head .m-portlet__head-caption .m-portlet__head-title .m-portlet__head-text {
    font-size: 1.2rem;
    font-weight: 600
     }
     .m-portlet .m-portlet__head .m-portlet__head-tools .m-portlet__nav .m-portlet__nav-item .m-portlet__nav-link.m-portlet__nav-link--icon [class^="la-"], .m-portlet .m-portlet__head .m-portlet__head-tools .m-portlet__nav .m-portlet__nav-item .m-portlet__nav-link.m-portlet__nav-link--icon [class*=" la-"] {
        font-size: 2em;
    }
    .m-portlet .m-portlet__head .m-portlet__head-tools .m-portlet__nav .m-portlet__nav-item .m-portlet__nav-link.m-portlet__nav-link--icon i {
    color: #36a3f7;
}
</style>
    <!--begin::Portlet-->
    <div class="m-portlet">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="flaticon-statistics"></i>
                        </span>
                        <h1 class="m-portlet__head-text">
                            {{$news->judul_event }}
                        </h1>
                        <h2 class="m-portlet__head-label m-portlet__head-label--danger">
                            <span>
                                Detail {{ get_menu_name()}}
                            </span>
                        </h2>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ $url }}" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-undo"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <table class="table table-striped">
                    <tr>
                        <td>
                            <center>
                                <h2>{{$news->judul_berita}}</h1>
                            </center>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="width: 100%">
                                <div style="width: 50%;float: left;font-size: 15px">
                                    <b>Dipublikasikan Oleh: {{$news->dipublikasi_oleh}}</b>
                                </div>
                            </div>
                            <div style="width: 100%">
                                <div style="width: 50%; font-size: 15px">
                                    <b>Tanggal Publikasi: {{date('d F Y', strtotime($news->tanggal_publikasi))}}</b>
                                </div>  
                            </div>
                            <div style="width: 100%">
                                <div style="width: 50%; font-size: 15px">
                                    <b>Berita Utama: {{ $news->kategori_berita == 'B' ? 'Berita' : ($news->kategori_berita == 'E' ? 'Event' : 'Tips') }}</b>
                                </div>  
                            </div>
                            <div style="width: 100%">
                                <div style="width: 50%; font-size: 15px">
                                    <b>Berita Utama: {{ $news->berita_utama == 'T' ? 'Ya' : 'Tidak' }}</b>
                                </div>  
                            </div>
                            <br>
                            <?php
                                foreach ($gambar_berita as $key) { ?>
                                  <img src="{{ $path_foto .  $key->gambar_berita }}" style="width: 80%; display: block;margin-left: auto; margin-right: auto;">
                            <?php } ?>
                            <br>
                            {!!$news->isi_berita!!}
                            <br>
                            
                             <div style="width: 100%">
                                <div style="width: 50%; font-size: 15px">
                                    <b>Label: <?php foreach($list_label as $key_) { ?> {{ $key_->nama_label . ',' }} <?php } ?> </b>
                                </div>  
                            </div>
                            
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/kegiatan/js/kegiatan.js" type="text/javascript"></script>
@stop