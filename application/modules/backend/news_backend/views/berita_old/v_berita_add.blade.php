@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            {!! form_input(array('type' => 'hidden','name' => 'id')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group ">
                       <label>
                            *
                            Kategori Berita:
                        </label>
                        {!! form_dropdown('kategori_berita', $kategori_berita, '', 'class="form-control m-select2" id="m_select2_2"') !!}
                    </div>  
                    <div class="col-lg-12 form-group ">
                       <label>
                            Label Berita:
                        </label>
                        {!! form_dropdown('label_berita', $label, '', 'class="form-control m-select2" id="m_select2_4" multiple="multiple"') !!}
                    </div> 
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Judul Berita:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'title', 'class' => 'form-control m-input', 'placeholder' => 'Judul Berita' )) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-12 ">
                                    <label>
                                        *
                                        Photo:
                                    </label> 
                                    <div class="m-dropzone dropzone m-dropzone--primary dropzone-previews" id="my-awesome-dropzone">
                                        <div class="m-dropzone__msg dz-message needsclick">
                                            <h3 class="m-dropzone__msg-title">
                                                Jatuhkan berkas di sini atau klik untuk diunggah.
                                            </h3>
                                            <span class="m-dropzone__msg-desc">
                                                Maksimal upload hingga 10 berkas, Ukuran berkas 1201 x 703 px, Berkas maksimal 2 MB
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 form-group">
                        <label>
                            *
                            Isi Berita:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'content', 'class' => 'summernote form-control m-input', 'placeholder' => 'Isi Berita' )) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                       <label>
                            *
                            Berita Utama:
                        </label>
                        {!! form_dropdown('berita_utama', $berita_utama, '', 'class="form-control m-select2" id="m_select2_3"') !!}
                    </div> 
                    <div class="col-lg-12 form-group" style="display: none;" id="urutan_berita">
                        <label>
                            Urutan Berita:
                        </label>
                        {!! form_input(array('type' => 'number','name' => 'urutan_berita', 'class' => 'form-control m-input', 'placeholder' => 'Urutan Berita','min' => 0)) !!}
                    </div>
                    <div class="col-lg-12 form-group ">
                        <label class="">
                            *
                            Publish:
                        </label>
                        {!! form_dropdown('status_publikasi', $publikasi, 'T', 'class="form-control m-select2" id="m_select2_5"') !!}
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_save"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var checkTitle       = '{{$checkTitle}}';
    var checkUrutan      = '{{$checkUrutan}}';
    var url              = '{{$action}}';
    var url_succees      = "{{$url_succees}}";
    var uploadMultiple   = true; 
    var autoProcessQueue = false;
    var maxFilesize      = 2;
    var paramName        = "file";
    var addRemoveLinks   = true;
    var maxFiles         = 10;
    var parallelUploads  = 10;
    var acceptedFiles    = 'image/jpeg,image/png';

    $('.save').attr("disabled",true);
</script>
<script src="{{ base_url() }}assets/backend/js/berita/js/berita-add.js" type="text/javascript"></script>
@stop