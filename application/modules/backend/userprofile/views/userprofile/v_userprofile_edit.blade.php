@extends('backend.default.views.layout.v_layout')

@section('body')
    
    <!--begin::Portlet-->
    <div class="">
        
    </div>

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            *
                            Username:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'username', 'class' => 'form-control m-input', 'placeholder' => 'Enter Username', 'value' => $result->username )) !!}
                    </div>

                    <div class="col-lg-6 ">
                    <!--     <label>
                            *
                            Email:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'email', 'class' => 'form-control m-input', 'placeholder' => 'Enter Email', 'value' => $result->email )) !!}
                     -->
                        <label>
                            *
                            Email
                        </label>
                        <div class="input-group">
                            {!! form_input(array('type' => 'text','name' => 'email', 'class' => 'form-control m-input', 'placeholder' => 'Enter Email', 'value' => $result->email )) !!}
                            <span class="input-group-btn">
                                <button class="btn btn-primary change-password" data-toggle="m-tooltip" data-original-title="Ubah Password" title type="button"><i class="fa fa-key"></i></button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            *
                            First Name:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'first_name', 'class' => 'form-control m-input', 'placeholder' => 'Enter First Name', 'value' => $result->first_name )) !!}
                    </div>

                    <div class="col-lg-6 ">
                        <label>
                            *
                            Last Name:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'last_name', 'class' => 'form-control m-input', 'placeholder' => 'Enter Last Name', 'value' => $result->last_name )) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            *
                            Phone Number:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'phone_number', 'class' => 'form-control m-input', 'placeholder' => 'Enter Phone Number', 'onkeypress' => 'return isNumber(event)', 'value' => $result->phone )) !!}
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_update"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($result->id) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var change_password = "{{ $change_password }}";
</script>
<script src="{{ base_url() }}assets/backend/js/userprofile/js/userprofile-edit.js" type="text/javascript"></script>
@stop