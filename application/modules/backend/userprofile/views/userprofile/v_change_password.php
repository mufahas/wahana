<!DOCTYPE html>
<html lang="en" >
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>
            Metronic | Login Page - 1
        </title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!--end::Web font -->
        <!--begin::Base Styles -->
        <link href="<?= base_url() ?>assets/default/css/vendors.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/default/css/style.bundle.css" rel="stylesheet" type="text/css" />
        <!--end::Base Styles -->
        <!--begin::Custom Styles -->
        <link href="<?= base_url() ?>assets/backend/css/custom.css" rel="stylesheet" type="text/css" />
        <!--end::Custom Styles -->
        <link rel="shortcut icon" href="<?= base_url() ?>assets/default/media/img/logo/wahana.ico" />
    </head>
    <style type="text/css">
        .m-login.m-login--2.m-login-2--skin-1 .m-login__container .m-login__form .form-control {
            color: #fff;
            background: rgba(126, 31, 34,.3);
        }
        .m-login.m-login--2.m-login-2--skin-1 .m-login__container .m-login__form {
            color: #fff;
        }
        .m-login.m-login--2.m-login-2--skin-1 .m-login__container .m-login__form .m-login__form-sub .m-checkbox {
            color: #fff;
        }
        .m-login.m-login--2.m-login-2--skin-1 .m-login__container .m-login__form .m-login__form-sub .m-link {
            color: #fff;
        }

        .bg-image {
            filter: gray;
            -webkit-filter: grayscale(1);
            -webkit-transition: all .8s ease-in-out;  
        }

        .bg-image:hover {
            filter: none;
            -webkit-filter: grayscale(0);
            -webkit-transform: scale(1.00);
        }


    </style>
    <!-- end::Head -->
    <!-- end::Body -->
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin:: Page -->
       <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="bg-image m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url(<?= base_url() ?>assets/default/media/img/users/bg-1.jpg);">
                <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
                    <div class="m-login__container">
                        <div class="m-login__logo">
                            <a href="#">
                                <img src="<?= base_url() ?>assets/default/media/img/users/logo-1.png">
                            </a>
                        </div>
                        <div class="m-login__signin">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                    Reset Password
                                </h3>
                            </div>
                            <?= form_open(null, array('class' => 'm-login__form m-form', 'role' => 'form')) ?>
                                <div class="form-group m-form__group">
                                    <?= form_input(array('type' => 'password','id' => 'new_password','name' => 'new_password', 'class' => 'form-control m-input', 'placeholder' => 'New Password', 'autocomplete' => 'off' )) ?>
                                </div>
                                <div class="form-group m-form__group">
                                    <?= form_input(array('type' => 'password','name' => 'confirm_password', 'class' => 'form-control m-input m-login__form-input--last', 'placeholder' => 'Confirm Password', 'autocomplete' => 'off' )) ?>
                                </div>
                                <div class="m-login__form-action">
                                    <button id="m_reset_password_signin_submit" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                        Reset
                                    </button>
                                </div>
                                <?= form_input(array('type' => 'hidden','name' => 'forgotten_password_code', 'class' => 'form-control m-input m-login__form-input--last', 'value' => $code )) ?>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Page -->
        <!--begin::Base Scripts -->
        <script src="<?= base_url() ?>assets/default/js/vendors.bundle.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/default/js/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Base Scripts -->   
        <!--begin::Page Snippets -->
        <script type="text/javascript">
            var processResetPassword = '<?= $processResetPassword ?>';
            var auth                 = '<?= $auth ?>';
            var reset_password       = '<?= $reset_password ?>';
        </script>
        <script src="<?= base_url() ?>assets/backend/js/auth/reset-password.js" type="text/javascript"></script>
        <!--end::Page Snippets -->
    </body>
    <!-- end::Body -->
</html>
