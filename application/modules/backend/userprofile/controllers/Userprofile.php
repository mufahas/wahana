<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userprofile extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        $this->load->library("send_email");
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {   
        $user = $this->ion_auth->user()->row();


        $model        = "M_user";
        $condition    = "users.id = '$user->id' AND active = '1' AND is_delete = 'F'";
        $row          = array('users.id','first_name','last_name', 'email', 'active','users.id');
        $row_search   = array('users.id','first_name','last_name', 'email', 'active','users.id');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {   
        $id_user    = decryptID($id);
        $ms_user    = M_user::where('id',$id_user)->first();
        
        if(!empty($ms_user))
        {
            /* Result */
            $data['result']          = $ms_user;
            
            /* Button Action */
            $data['action']          = site_url() . $this->site . '/update';
            $data['change_password'] = site_url() . $this->site . '/ajax_change_password';


            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }

    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        $id          = $this->input->post('id');
        $id_user     = decryptID($id);
        
        /* Url */
        $url_succees = site_url() . $this->site;
        $url_error   = site_url() . $this->site . '/edit/' . $id;
        
        /* Data Post */
        $username    = strtolower($this->input->post('username'));
        $email       = strtolower($this->input->post('email'));   
        $first_name  = ucwords($this->input->post('first_name'));
        $last_name   = ucwords($this->input->post('last_name'));
        $phone       = $this->input->post('phone_number');

        /* Check Data Into Table  */
        $ms_user = M_user::where('email',$email)->whereRaw('id != '.$id_user.'')->first();
     
        if(empty($ms_user))
        {
            $model = M_user::where('id',$id_user)->first();
            
            /* BEGIN ARRAY WRITE LOG */
            $data_old   = array(
                                "Username" => $model->username,
                                "Email"    => $model->email,
                                "Name"     => $model->first_name .' '. $model->last_name,
                                "Phone"    => $model->phone 
                            );

            $data_new   = array(
                                "Username" => $username,
                                "Email"    => $email,
                                "Name"     => $first_name .' '. $last_name,
                                "Phone"    => $phone 
                            );
            /* END ARRAY WRITE LOG */

            /* Initialize Data */
            $model->username   = $username;
            $model->email      = $email;
            $model->first_name = $first_name;
            $model->last_name  = $last_name;
            $model->phone      = $phone;

            /* Update */
            $save = $model->save();

            if($save)
            {
                /* Write Log */
                $data_change = array_diff_assoc($data_new, $data_old);
                $message     = 'Memperbarui data user ' . $first_name.' '.$last_name;

                $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                /* End Write Log*/

                $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /*
     * Forget Password
     */
    public function ajax_change_password()
    {
        if ($this->input->is_ajax_request())
        {   
            /* Set URL */
            $url = site_url() . $this->site . '/add';

            /* Check User Login */
            $user      = $this->ion_auth->user()->row();
            
            /* Set Forgotten Password Based On Ion Auth Plugin */
            $forgotten = $this->ion_auth->forgotten_password($user->email);

            if($forgotten)
            {
                $this->sendEmail($user,$forgotten);

                $status = array('status' => 'success', 'message' => 'Please check your email for change password', 'url' => $url);
            }
            else
            {
                $status = array('status' => 'error', 'message' => 'Change Password Failed !', 'url' => $url);
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($status));
        }
    }

    public function sendEmail($user, $forgotten)
    {
        $data['user']      = $user;
        $data['forgotten'] = $forgotten;
        $message           = $this->load->view('../modules/backend/userprofile/views/userprofile/v_forgot_password', $data , true, true); 

        $this->send_email->email($user->email, $message);
    }

    public function reset_password($code = NULL) 
    {
        $user = $this->ion_auth->forgotten_password_check($code);
        if ($user) //ADA
        {
            $data['code']                 = $code;
            $data['auth']                 = site_url() . $this->site;
            $data['processResetPassword'] = site_url() . $this->site . '/processResetPassword';
            $data['reset_password']       = site_url() . $this->site . '/reset_password/' . $code;

            $this->load->view('userprofile/userprofile/v_change_password',$data);
        }
        else
        {
            $logout = $this->ion_auth->logout();
            redirect('auth', 'refresh');
        }
    }

    public function processResetPassword()
    {
        $new_password            = $this->input->post('new_password');
        $confirm_password        = $this->input->post('confirm_password');
        $forgotten_password_code = $this->input->post('forgotten_password_code');

        $user = $this->ion_auth->forgotten_password_check($forgotten_password_code);

        $change = $this->ion_auth->reset_password($user->email, $new_password);

        if($change)
        {
            $this->ion_auth->clear_forgotten_password_code($forgotten_password_code);
            $data = array("status" => "success", "message" => 'Successfully to reset password.');
        }
        else
        {
            $data = array("status" => "error", "message" => 'Failed to reset password.');
        }

        echo json_encode($data);
    }

}