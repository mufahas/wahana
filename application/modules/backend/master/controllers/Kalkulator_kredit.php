<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kalkulator_kredit extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        /* Load Model */
        $this->load->model('select_global_model');
    }

    function index()
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_kalkulator_kredit";
        $condition    = "";
        $row          = array('ms_kalkulator_kredit.kode_kalkulator_kredit', 'ms_kalkulator_kredit.persentase_bunga', 'ms_kalkulator_kredit.perhitungan_jenis_bunga');
        $row_search   = array('ms_kalkulator_kredit.kode_kalkulator_kredit', 'ms_kalkulator_kredit.persentase_bunga', 'ms_kalkulator_kredit.perhitungan_jenis_bunga');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add()
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {

    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $kode_kalkulator_kredit = decryptID($id);
        $ms_kalkulator_kredit   = ms_kalkulator_kredit::where('kode_kalkulator_kredit',$kode_kalkulator_kredit)->first();

        if(!empty($ms_kalkulator_kredit))
        {
            /* Button Action */
            $data['action']                         = site_url() . $this->site . '/update';

            $data['ms_kalkulator_kredit']           = $ms_kalkulator_kredit;
            $data['select_perhitungan_jenis_bunga'] = $this->select_global_model->selectPerhitunganJenisBunga();

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($this->input->is_ajax_request()) 
        {   
            $id                         = $this->input->post("id");
            $kode_kalkulator_kredit     = decryptID($id);

            /* Url */
            $url_succees                = site_url() . $this->site;
            $url_error                  = site_url() . $this->site . '/edit/' . $id;

            /* Get Data Post */
            $persentase_bunga           = ucwords($this->input->post("persentase_bunga"));
            $perhitungan_jenis_bunga    = ucwords($this->input->post("perhitungan_jenis_bunga"));

            /* Get User Login */
            $user      = $this->ion_auth->user()->row();

            /* Get Model ms_kalkulator_kredit */
            $model = ms_kalkulator_kredit::where('kode_kalkulator_kredit',$kode_kalkulator_kredit)->first();

            /* Array for write log */
            $data_old = array(
                        "Kode Kalkulator Kredit"  => $model->kode_kalkulator_kredit,
                        "Persentase Bunga"        => $model->persentase_bunga,
                        "Perhitungan Jenis Bunga" => $model->perhitungan_jenis_bunga
                        );

            $data_new = array(
                        "Kode Kalkulator Kredit"  => $kode_kalkulator_kredit,
                        "Persentase Bunga"        => $persentase_bunga,
                        "Perhitungan Jenis Bunga" => $perhitungan_jenis_bunga
                        );
            /* End array for write log */

            /* Initialize Data */
            $model->persentase_bunga        = $persentase_bunga;
            $model->perhitungan_jenis_bunga = $perhitungan_jenis_bunga;
            $model->diubah_oleh             = $user->first_name . ' ' . $user->last_name;
            $model->tanggal_terakhir_diubah = date('Y-m-d H:i:s');

            /* Save */
            $save = $model->save();

            if($save)
            {
                /* Write Log */
                $data_change = array_diff_assoc($data_new, $data_old);
                $message     = 'Memperbarui master kalkulator kredit ' . $persentase_bunga . $perhitungan_jenis_bunga;
                $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                /* End Write Log*/

                $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
            }
            
            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {

    }
}