<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jaringan_wahana extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        /* Load Model */
        $this->load->model('select_global_model');
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_jaringan_wahana";
        $condition    = "ms_jaringan_wahana.dihapus = 'F'";
        $row          = array('ms_jaringan_wahana.kode_jaringan', 'ms_kota_kabupaten.nama_kota_kabupaten', 'ms_jaringan_wahana.nama_dealer', 'ms_jaringan_wahana.nomor_telepon', 'ms_jaringan_wahana.alamat_jaringan', 'ms_jaringan_wahana.link_embed_peta');
        $row_search   = array('ms_jaringan_wahana.kode_jaringan', 'ms_kota_kabupaten.nama_kota_kabupaten', 'ms_jaringan_wahana.nama_dealer', 'ms_jaringan_wahana.nomor_telepon', 'ms_jaringan_wahana.alamat_jaringan', 'ms_jaringan_wahana.link_embed_peta');
        $join         = array('ms_kota_kabupaten' => 'ms_kota_kabupaten.kode_kota_kabupaten = ms_jaringan_wahana.kode_kota_kabupaten');
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * View data from table:
    * @param Id
    * @return page index
    **/
    function view($id)
    {
        $kode_jaringan = decryptID($id);
        $ms_jaringan_wahana  = ms_jaringan_wahana::join('ms_kota_kabupaten','ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')->where('ms_jaringan_wahana.kode_jaringan',$kode_jaringan)->first();

        if(!empty($ms_jaringan_wahana))
        {
            $data['back']       = site_url() . $this->site;
            $data['ms_jaringan_wahana']         = $ms_jaringan_wahana;
            $tbl_jaringan_wahana_detail   = tbl_jaringan_wahana_detail::where('tbl_jaringan_wahana_detail.kode_jaringan', $kode_jaringan)->orderBy('tbl_jaringan_wahana_detail.kategori_jaringan','ASC')->get();
            foreach ($tbl_jaringan_wahana_detail as $key => $value) {
                if ($value->kategori_jaringan == 'H1') {
                    $kategori_jaringan[] = "Penjualan";
                }elseif($value->kategori_jaringan == 'H2') {
                    $kategori_jaringan[] = "Pemeliharaan";
                }elseif($value->kategori_jaringan == 'H3'){
                    $kategori_jaringan[] = "Suku Cadang";
                }
            }
            $data['kategori_jaringan'] = implode( ", ", $kategori_jaringan );
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
        
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']                   = site_url() . $this->site . '/save';
        $data['select_kota_kabupaten']    = $this->select_global_model->selectKotaKabupaten();
        $data['select_kategori_jaringan'] = $this->select_global_model->selectKategoriJaringan();

        /* Ajax Get Latitude Langitude */
        $data['ajaxGetLatLng']            = site_url() . $this->site . '/ajax_get_lat_lng';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Url */
            $url_succees                = site_url() . $this->site;
            $url_error                  = site_url() . $this->site . '/add';
            
            /* Get Data Post */
            $kategori_jaringan          = $this->input->post('kategori_jaringan');
            $nama_dealer                = ucwords($this->input->post('nama_dealer'));
            $nomor_telepon_             = $this->input->post('nomor_telepon');
            $nomor_telepon              = str_replace("_","",$nomor_telepon_);
            $alamat_jaringan            = $this->input->post('alamat_jaringan');
            $link_peta                  = $this->input->post('link_peta');
            // $link_embed_peta         = $this->input->post('link_embed_peta');
            $kode_kota_kabupaten        = decryptID(($this->input->post('kode_kota_kabupaten')));
            $latitude                   = $this->input->post('latitude');
            $longitude                  = $this->input->post('longitude');
  
            $ms_kota_kabupaten          = ms_kota_kabupaten::where('kode_kota_kabupaten',$kode_kota_kabupaten)->first();
            /* check in table available or not */
            $ms_jaringan_wahana         = ms_jaringan_wahana::where('nama_dealer',$nama_dealer)->first();


            if(empty($ms_jaringan_wahana))
            {

                $model = new ms_jaringan_wahana;

                $model->nama_dealer         = $nama_dealer;
                $model->nomor_telepon       = $nomor_telepon;
                $model->alamat_jaringan     = $alamat_jaringan;
                $model->link_peta           = $link_peta;
                // $model->link_embed_peta     = $link_embed_peta;
                $model->kode_kota_kabupaten = $kode_kota_kabupaten;
                $model->latitude            = $latitude;
                $model->longitude           = $longitude;

                $save = $model->save();

                if($save)
                {
                    if(!empty($kategori_jaringan))
                    {
                        foreach ($kategori_jaringan as $key) 
                        {   
                            $model_detail = new tbl_jaringan_wahana_detail;

                            $model_detail->kode_jaringan     = ms_jaringan_wahana::max('kode_jaringan');
                            $model_detail->kategori_jaringan = $key;

                            $save_detail = $model_detail->save();
                        }
                    }

                    /* Write Log */
                    $data_notif = array(
                                        "Kode Jaringan"     => ms_jaringan_wahana::max('kode_jaringan'),
                                        "Lokasi"            => $ms_kota_kabupaten->nama_kota_kabupaten,
                                        // "Kategori Jaringan" => $kategori_jaringan,
                                        "Nama Dealer"       => $nama_dealer,
                                        "Nomor Jaringan"    => $nomor_telepon,
                                        "Alamat Jaringan"   => $alamat_jaringan,
                                        "Link Peta"         => $link_peta,
                                        // "Link Embed Peta"   => $link_embed_peta,
                                        );

                    $message = "Berhasil menambahkan master jaringan wahana " . $nama_dealer;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);

                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
                }          
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        
        $kode_jaringan = decryptID($id);
        $ms_jaringan_wahana   = ms_jaringan_wahana::where('kode_jaringan',$kode_jaringan)->first();

        if(!empty($ms_jaringan_wahana))
        {
            /* Button Action */
            $data['action']            = site_url() . $this->site . '/update';
            $data['select_kota_kabupaten']    = $this->select_global_model->selectKotaKabupaten();
            $data['select_kategori_jaringan'] = $this->select_global_model->selectKategoriJaringan();//tidak dipake

            $data['H1'] = tbl_jaringan_wahana_detail::where('kode_jaringan',$kode_jaringan)->where('kategori_jaringan','H1')->first();
            $data['H2'] = tbl_jaringan_wahana_detail::where('kode_jaringan',$kode_jaringan)->where('kategori_jaringan','H2')->first();
            $data['H3'] = tbl_jaringan_wahana_detail::where('kode_jaringan',$kode_jaringan)->where('kategori_jaringan','H3')->first();

            $data['ms_jaringan_wahana'] = $ms_jaringan_wahana;

            /* Ajax Get Latitude Langitude */
            $data['ajaxGetLatLng']            = site_url() . $this->site . '/ajax_get_lat_lng';

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($this->input->is_ajax_request()) 
        {   
            $id                  = $this->input->post("id");
            $kode_jaringan       = decryptID($id);
            
            /* Url */
            $url_succees         = site_url() . $this->site;
            $url_error           = site_url() . $this->site . '/edit/' . $id;
            
            /* Get Data Post */
            $kategori_jaringan   = $this->input->post('kategori_jaringan');
            $nama_dealer         = ucwords($this->input->post('nama_dealer'));
            $nomor_telepon_     = $this->input->post('nomor_telepon');
            $nomor_telepon       = str_replace("_","",$nomor_telepon_);
            $alamat_jaringan     = $this->input->post('alamat_jaringan');
            $link_peta           = $this->input->post('link_peta');
            // $link_embed_peta  = $this->input->post('link_embed_peta');
            $kode_kota_kabupaten = decryptID($this->input->post('kode_kota_kabupaten'));
            $latitude            = $this->input->post('latitude');
            $longitude           = $this->input->post('longitude');
            
            /* check in table available or not */
            $ms_jaringan_wahana  = ms_jaringan_wahana::where('nama_dealer',$nama_dealer)->whereRaw('kode_jaringan != '.$kode_jaringan.'')->first();

            if(empty($ms_jaringan_wahana))
            {
                $model = ms_jaringan_wahana::where('kode_jaringan',$kode_jaringan)->first();

                $ms_kota_kabupaten   = ms_kota_kabupaten::where('kode_kota_kabupaten',$model->kode_kota_kabupaten)->first();

                /* Array for write log */
                $data_old = array(
                            "Kode Jaringan"     => $model->kode_jaringan,
                            "Lokasi"            => $ms_kota_kabupaten->nama_kota_kabupaten,
                            // "Kategori Jaringan" => $model->kategori_jaringan,
                            "Nama Dealer"       => $model->nama_dealer,
                            "Nomor Jaringan"    => $model->nomor_telepon,
                            "Alamat Jaringan"   => $model->alamat_jaringan,
                            "Link Peta"         => $model->link_peta,
                            );

                $data_new = array(
                            "Kode Jaringan"     => $kode_jaringan,
                            "Lokasi"            => $kode_kota_kabupaten,
                            // "Kategori Jaringan" => $kategori_jaringan,
                            "Nama Dealer"       => $nama_dealer,
                            "Nomor Jaringan"    => $nomor_telepon,
                            "Alamat Jaringan"   => $alamat_jaringan,
                            "Link Peta"         => $link_peta,
                            );
                /* End array for write log */

                /* Initialize Data */
                // $model->kategori_jaringan   = $kategori_jaringan;
                $model->nama_dealer         = $nama_dealer;
                $model->nomor_telepon       = $nomor_telepon;
                $model->alamat_jaringan     = $alamat_jaringan;
                $model->link_peta           = $link_peta;
                // $model->link_embed_peta     = $link_embed_peta;
                $model->kode_kota_kabupaten = $kode_kota_kabupaten;
                $model->latitude            = $latitude;
                $model->longitude           = $longitude;

                /* Save */
                $save = $model->save();

                if($save)
                {
                    if(!empty($kategori_jaringan))
                    {
                        $list_delete = tbl_jaringan_wahana_detail::where('kode_jaringan',$kode_jaringan);
                        $delete = $list_delete->delete();

                        foreach ($kategori_jaringan as $key) 
                        {   
                            $model_detail = new tbl_jaringan_wahana_detail;

                            $model_detail->kode_jaringan     = $kode_jaringan;
                            $model_detail->kategori_jaringan = $key;

                            $save_detail = $model_detail->save();
                        }
                    }

                    /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message     = 'Memperbarui master jaringan wahana ' . $nama_dealer;
                    $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                    /* End Write Log*/

                    $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url               = site_url() . $this->site;
            $id                = $this->input->get("id");
            $kode_jaringan     = decryptID($id);
            
            $ms_jaringan_wahana   = ms_jaringan_wahana::where("kode_jaringan",$kode_jaringan)->first();

            $ms_kota_kabupaten   = ms_kota_kabupaten::where('kode_kota_kabupaten',$ms_jaringan_wahana->kode_kota_kabupaten)->first();

            if(!empty($ms_jaringan_wahana))
            {
                $delete                            = ms_jaringan_wahana::where("kode_jaringan",$kode_jaringan)->delete();

                if($delete)
                {
                    
                    $delete_detail = tbl_jaringan_wahana_detail::where('kode_jaringan',$kode_jaringan)->delete();

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /* Function Get Latitude and Longitude Based On Short URL Map */
    function ajax_get_lat_lng()
    {
        $link_peta = $this->input->post('link_peta');
        
        if(!empty($link_peta))
        {
            // URL Request
            $curl      = curl_init($link_peta); 

            curl_setopt_array($curl, array(
                CURLOPT_NOBODY => TRUE,            // Don't ask for a body, we only need the headers
                CURLOPT_FOLLOWLOCATION => FALSE,   // Don't follow the 'Location:' header, if any
            ));

            // Send the request (you should check the returned value for errors)
            curl_exec($curl);

             // Get information about the 'Location:' header (if any)
            $location = curl_getinfo($curl,CURLINFO_REDIRECT_URL);

            echo json_encode($location);
        }
    }
}