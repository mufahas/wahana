<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kalkulator_cicilan extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        $this->load->model('Select_global_model');

        require_once APPPATH.'third_party/PHPExcel.php';
        $this->excel = new PHPExcel(); 
    }

    function index() 
    {
        $data['loadTable']       = site_url() . $this->site . '/loadTable';
        $data['add']             = site_url() . $this->site . '/add';
        $data['edit']            = site_url() . $this->site . '/edit';
        $data['delete']          = site_url() . $this->site . '/delete';
        $data['messages']        = $this->session->flashdata('messages');

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_harga_otr";
        $condition    = "";
        $row          = array('ms_harga_otr.kode_harga_otr','ms_harga_otr.nama_produk_otr');
        $row_search   = array('ms_harga_otr.kode_harga_otr','ms_harga_otr.nama_produk_otr');
        $join         = array('ms_kalkulator_cicilan'  => 'ms_kalkulator_cicilan.kode_harga_otr = ms_harga_otr.kode_harga_otr',
                              'ms_produk'              => 'ms_produk.kode_produk = ms_harga_otr.kode_produk'
                          );
        $order        = "";
        $groupby      = "ms_kalkulator_cicilan.kode_harga_otr";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }
    
    // function loadTable()
    // {
    //     $model        = "ms_harga_otr";
    //     $condition    = "ms_produk.dihapus = 'F' AND ms_harga_otr.list_harga_otr != ''";
    //     $row          = array('ms_harga_otr.kode_harga_otr','ms_harga_otr.nama_produk_otr');
    //     $row_search   = array('ms_harga_otr.kode_harga_otr','ms_harga_otr.nama_produk_otr');
    //     $join         = array(
    //                             'ms_produk'              => 'ms_produk.kode_produk = ms_harga_otr.kode_produk'
    //                         );
    //     $order        = "";
    //     $groupby      = "ms_harga_otr.kode_produk";
    //     $limit        = "";
    //     $offset       = "";
    //     $distinct     = "";

    //     /* Get Data */
    //     $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
    //     return $q;
    // }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        $data['produk']    = $this->Select_global_model->selectProdukHargaOtr();

        /* Path File Upload Kalkulator Cicilan */
        $data['path_file'] = base_url() . "assets/upload/kalkulator_cicilan/Contoh_Bentuk_Excel_Upload_Kalkulator_Cicilan.xlsx"; 
        
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $id = $this->input->post('produk');
            $kode_harga_otr = decryptID($id);

            /* Set Post Upload For Presentasi Daftar Harga excel*/
            $userfile            = $_FILES["userfile"]["name"];
            $userfile_size       = $_FILES["userfile"]["size"];
            $userfile_tmp_name   = $_FILES["userfile"]["tmp_name"];
            $exist_harga_otr     = ms_kalkulator_cicilan::where('ms_kalkulator_cicilan.kode_harga_otr',$kode_harga_otr)->first();
            
            if(!empty($userfile) && empty($exist_harga_otr))
            {
                // Set File Name, Extension File, File size, and Allowed Type
                $filename            = $userfile;
                $file_basename       = substr($filename, 0, strripos($filename, '.')); // get file extention
                $file_ext            = substr($filename, strripos($filename, '.')); // get file name extension
                $filesize            = $userfile_size; //get size
                $allowed_file_types  = array('.xls','.xlsx');

                if(in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 5000000)
                {   
                    $nama_produk  = ms_harga_otr::join('ms_produk','ms_produk.kode_produk','=','ms_harga_otr.kode_produk')->where('ms_harga_otr.kode_harga_otr',$kode_harga_otr)->first();

                    $file_cicilan = $file_basename . '_' . date('d-m-Y') . $file_ext;

                    if(file_exists("assets/upload/produk/kalkulator_cicilan/" . $file_cicilan))
                    {
                        $this->session->set_flashdata('messages', 'Data Gagal Disimpan');
                        redirect(site_url() . $this->site);
                    }
                    else
                    {
                        if(move_uploaded_file($userfile_tmp_name,  "assets/upload/kalkulator_cicilan/" . $file_cicilan))
                        {
                            /* Update Field List Harga OTR */
                            $ms_harga_otr                 = ms_harga_otr::where('ms_harga_otr.kode_harga_otr',$kode_harga_otr)->first();
                            $ms_harga_otr->list_harga_otr = $file_cicilan;

                            $save_ms_harga_otr            = $ms_harga_otr->save();
                            
                            /* Read Field In Excel Based On Format Given */
                            PHPExcel_Settings::setZipClass(PHPExcel_Settings::ZIPARCHIVE);
                            $file_type   = PHPExcel_IOFactory::identify( "assets/upload/kalkulator_cicilan/" . $file_cicilan);
                            $objReader   = PHPExcel_IOFactory::createReader($file_type);
                            $objPHPExcel = $objReader->load("assets/upload/kalkulator_cicilan/" . $file_cicilan);
                            $sheet_data  = $objPHPExcel->getActiveSheet()->toArray(null,true,false,true);

                            foreach($sheet_data as $key)
                            {
                               
                                $ms_cicilan_kredit = new ms_kalkulator_cicilan;

                                $ms_cicilan_kredit->kode_harga_otr = $kode_harga_otr;
                                $ms_cicilan_kredit->dp             = $key['A'];
                                $ms_cicilan_kredit->tenor_11       = $key['B'];
                                $ms_cicilan_kredit->tenor_17       = $key['C'];
                                $ms_cicilan_kredit->tenor_23       = $key['D'];
                                $ms_cicilan_kredit->tenor_27       = $key['E'];
                                $ms_cicilan_kredit->tenor_29       = $key['F'];
                                $ms_cicilan_kredit->tenor_33       = $key['G'];
                                $ms_cicilan_kredit->tenor_35       = $key['H'];

                                $save = $ms_cicilan_kredit->save();

                                /* Write Log */
                                $data_notif = array(
                                     "Kode Harga OTR "       => $kode_harga_otr,
                                     "Nama Produk"           => $nama_produk
                                     );

                                $message = "Berhasil Menambah Master Kalkulator_cicilan" . $nama_produk;
                                $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                                /* End Write Log */
                                
                            }
                            
                            $this->session->set_flashdata('messages', 'Data Berhasil Disimpan');
                            redirect(site_url() . $this->site);
                        } 
                    }
                }
                elseif(empty($file_basename)) 
                {
                    $this->session->set_flashdata('messages', 'Data Gagal Disimpan !!');
                    redirect(site_url() . $this->site);
                } else 
                {
                    $this->session->set_flashdata('messages', 'Data Gagal Disimpan !!');
                    redirect(site_url() . $this->site); 
                }
            } 
            else
            {
                $this->session->set_flashdata('messages', 'Data Sudah Ada !!');
                redirect(site_url() . $this->site);

            }
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function view($id)
    {   
        $kode_harga_otr = decryptID($id);

        $ms_kalkulator_cicilan = ms_kalkulator_cicilan::where('ms_kalkulator_cicilan.kode_harga_otr',$kode_harga_otr)->get();

        if(!empty($ms_kalkulator_cicilan))
        {
            $data['cicilan']    = $ms_kalkulator_cicilan;
            
            $data['back']       = site_url() . $this->site;

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url                   = site_url() . $this->site;
            $id                    = $this->input->get("id");
            $kode_harga_otr        = decryptID($id);
            
            $ms_kalkulator_cicilan = ms_kalkulator_cicilan::where('ms_kalkulator_cicilan.kode_harga_otr',$kode_harga_otr)->first();
          
            if(!empty($ms_kalkulator_cicilan))
            {
                $user            = $this->ion_auth->user()->row();
                $ms_harga_otr    = ms_harga_otr::where('ms_harga_otr.kode_harga_otr',$kode_harga_otr)->first();

                if(file_exists("assets/upload/kalkulator_cicilan/" . $ms_harga_otr->list_harga_otr))
                {
                    $unlink = unlink("assets/upload/kalkulator_cicilan/" . $ms_harga_otr->list_harga_otr);

                    if($unlink)
                    {
                        $ms_harga_otr->list_harga_otr = "";
                        $ms_harga_otr->save();
                        
                        $delete = ms_kalkulator_cicilan::where('ms_kalkulator_cicilan.kode_harga_otr',$kode_harga_otr)->delete();
                        
                        if($delete)
                        {
                            /* Write log */
                            $data_notif = array(
                                "Kode Harga OTR"     => $kode_harga_otr
                            );

                            $message = "Menghapus master kalkulator_cicilan " . $ms_kalkulator_cicilan->kode_harga_otr;
                            $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                            /* End Write Log */

                            $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                        }
                        else
                        {
                            $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                        }


                    }
                } 
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
}