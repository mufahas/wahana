<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_part_aksesoris extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:ms_kategori_part_aksesoris
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_kategori_part_aksesoris";
        $condition    = "ms_kategori_part_aksesoris.dihapus = 'F'";
        $row          = array('ms_kategori_part_aksesoris.kode_kategori_part_aksesoris', 'ms_kategori_part_aksesoris.nama_kategori_part_aksesoris');
        $row_search   = array('ms_kategori_part_aksesoris.kode_kategori_part_aksesoris', 'ms_kategori_part_aksesoris.nama_kategori_part_aksesoris');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table: ms_kategori_part_aksesoris
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Url */
            $url_succees         = site_url() . $this->site;
            $url_error           = site_url() . $this->site . '/add';
            
            /* Get Data Post */
            $nama_kategori_part_aksesoris = ucwords($this->input->post('nama_kategori_part_aksesoris'));

            /* check in table available or not */
            $ms_kategori_part_aksesoris   = ms_kategori_part_aksesoris::where('nama_kategori_part_aksesoris',$nama_kategori_part_aksesoris)->first();

            if(empty($ms_kategori_part_aksesoris))
            {
                $model = new ms_kategori_part_aksesoris;

                $model->nama_kategori_part_aksesoris = $nama_kategori_part_aksesoris;

                $save = $model->save();

                if($save)
                {
                    /* Write Log */
                    $data_notif = array(
                                        "Kode Kategori Part Aksesoris" => ms_kategori_part_aksesoris::max('kode_kategori_part_aksesoris'),
                                        "Nama Kategori Part Aksesoris" => $nama_kategori_part_aksesoris,
                                        );

                    $message = "Berhasil menambahkan master kategori part aksesoris " . $nama_kategori_part_aksesoris;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);

                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
                }          
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $kode_kategori_part_aksesoris = decryptID($id);
        $ms_kategori_part_aksesoris   = ms_kategori_part_aksesoris::where('kode_kategori_part_aksesoris',$kode_kategori_part_aksesoris)->first();

        if(!empty($ms_kategori_part_aksesoris))
        {
            /* Button Action */
            $data['action']            = site_url() . $this->site . '/update';

            $data['ms_kategori_part_aksesoris'] = $ms_kategori_part_aksesoris;

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:ms_kategori_part_aksesoris
    * @param Post Data
    * @return page index
    **/
    function update()
    {   
        if ($this->input->is_ajax_request()) 
        {   
            $id                  = $this->input->post("id");
            $kode_kategori_part_aksesoris = decryptID($id);

            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/edit/' . $id;

            /* Get Data Post */
            $nama_kategori_part_aksesoris = ucwords($this->input->post("nama_kategori_part_aksesoris"));

            /* check in table available or not */
            $ms_kategori_part_aksesoris = ms_kategori_part_aksesoris::where('nama_kategori_part_aksesoris',$nama_kategori_part_aksesoris)->whereRaw('kode_kategori_part_aksesoris != '.$kode_kategori_part_aksesoris.'')->first();

            if(empty($ms_kategori_part_aksesoris))
            {
                $model = ms_kategori_part_aksesoris::where('kode_kategori_part_aksesoris',$kode_kategori_part_aksesoris)->first();

                /* Array for write log */
                $data_old = array(
                            "Kode Kategori Part Aksesoris" => $model->kode_kategori_part_aksesoris,
                            "Nama Kategori Part Aksesoris" => $model->nama_kategori_part_aksesoris
                            );

                $data_new = array(
                            "Kode Kategori Part Aksesoris" => $kode_kategori_part_aksesoris,
                            "Nama Kategori Part Aksesoris" => $nama_kategori_part_aksesoris
                            );
                /* End array for write log */

                /* Initialize Data */
                $model->nama_kategori_part_aksesoris = $nama_kategori_part_aksesoris;

                /* Save */
                $save = $model->save();

                if($save)
                {
                    /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message     = 'Memperbarui master kategori part aksesoris ' . $nama_kategori_part_aksesoris;
                    $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                    /* End Write Log*/

                    $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * Delete data from table:ms_kategori_part_aksesoris
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url               = site_url() . $this->site;
            $id                = $this->input->get("id");
            $kode_kategori_part_aksesoris = decryptID($id);
            
            $ms_kategori_part_aksesoris   = ms_kategori_part_aksesoris::where("kode_kategori_part_aksesoris",$kode_kategori_part_aksesoris)->first();

            if(!empty($ms_kategori_part_aksesoris))
            {
                $user      = $this->ion_auth->user()->row();

                $ms_kategori_part_aksesoris->dihapus       = 't';
                $ms_kategori_part_aksesoris->tanggal_hapus = date('Y-m-d H:i:s');
                $ms_kategori_part_aksesoris->dihapus_oleh  = $user->first_name . ' ' . $user->last_name;
                $delete                         = $ms_kategori_part_aksesoris->save();

                if($delete)
                {
                    /* Write log */
                    $data_notif = array(
                        "Kode Kategori Part Aksesoris" => $kode_kategori_part_aksesoris,
                        "Nama Kategori Part Aksesoris" => $ms_kategori_part_aksesoris->nama_kategori_part_aksesoris,
                    );

                    $message = "Menghapus master kategori part aksesoris " . $ms_kategori_part_aksesoris->nama_kategori_part_aksesoris;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
}