<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kota_kabupaten extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        //Model
        $this->load->model('select_global_model');
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_kota_kabupaten";
        $condition    = "ms_kota_kabupaten.dihapus = 'F'";
        $row          = array('ms_kota_kabupaten.kode_kota_kabupaten', 'ms_provinsi.nama_provinsi', 'ms_kota_kabupaten.nama_kota_kabupaten');
        $row_search   = array('ms_kota_kabupaten.kode_kota_kabupaten', 'ms_provinsi.nama_provinsi', 'ms_kota_kabupaten.nama_kota_kabupaten');
        $join         = array('ms_provinsi'  => 'ms_provinsi.kode_provinsi = ms_kota_kabupaten.kode_provinsi');
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']   = site_url() . $this->site . '/save';
        $data['provinsi'] = $this->select_global_model->selectProvinsi();

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Url */
            $url_succees         = site_url() . $this->site;
            $url_error           = site_url() . $this->site . '/add';
            
            /* Get Data Post */
            $city_name   = ucwords($this->input->post('koka'));
            $province_id = decryptID($this->input->post('kode_provinsi'));

            /* check in table available or not */
            $ms_koka   = ms_kota_kabupaten::where('nama_kota_kabupaten',$city_name)->first();

            if(empty($ms_koka))
            {
                $model = new ms_kota_kabupaten;

                $model->kode_provinsi       = $province_id;
                $model->nama_kota_kabupaten = $city_name;

                $save = $model->save();

                if($save)
                {
                    /*Get Provinse Detail*/
                    $province = ms_provinsi::find($province_id);
                    /* Write Log */
                    $data_notif = array(
                                        "Kode Kota/Kabupaten" => ms_kota_kabupaten::max('kode_kota_kabupaten'),
                                        "Nama Provinsi"       => $province->nama_provinsi,
                                        "Nama Kota/Kabupaten" => $city_name,
                                        );

                    $message = "Berhasil menambahkan master kota/Kabupaten " . $city_name;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);

                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
                }          
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $city_code = decryptID($id);
        $city      = ms_kota_kabupaten::where('kode_kota_kabupaten',$city_code)->first();

        if(!empty($city))
        {
            /* Button Action */
            $data['action']      = site_url() . $this->site . '/update';
            $data['province_id'] = encryptID($city->kode_provinsi);
            $data['city']        = $city;
            $data['provinsi']    = $this->select_global_model->selectProvinsi();

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($this->input->is_ajax_request()) 
        {   
            $id        = $this->input->post("id");
            $city_code = decryptID($id);

            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/edit/' . $id;

            /* Get Data Post */
            $city_name     = ucwords($this->input->post("koka"));
            $province_code = decryptID($this->input->post('kode_provinsi'));

            /* check in table available or not */
            $city = ms_kota_kabupaten::where('nama_kota_kabupaten',$city_name)->whereRaw('kode_kota_kabupaten != '.$city_code.'')->first();

            if(empty($city))
            {
                $model = ms_kota_kabupaten::where('kode_kota_kabupaten',$city_code)->first();


                /*Get Provinse Detail*/
                $old_province = ms_provinsi::find($model->kode_provinsi);
                $new_province = ms_provinsi::find($province_code);

                /* Array for write log */
                $data_old = array(
                            "Kode Kota/Kabupaten" => $model->kode_kota_kabupaten,
                            "Nama Provinsi"       => $old_province,
                            "Nama Kota/Kabupaten" => $model->nama_kota_kabupaten
                            );

                $data_new = array(
                            "Kode Kota/Kabupaten" => $city_code,
                            "Nama Provinsi"       => $new_province,
                            "Nama Kota/Kabupaten" => $city_name
                            );
                /* End array for write log */

                /* Initialize Data */
                $model->kode_provinsi       = $province_code;
                $model->nama_kota_kabupaten = $city_name;

                /* Save */
                $save = $model->save();

                if($save)
                {
                    /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message     = 'Memperbarui master kota/kabupaten ' . $city_name;
                    $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                    /* End Write Log*/

                    $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url       = site_url() . $this->site;
            $id        = $this->input->get("id");
            $city_code = decryptID($id);
            
            $city      = ms_kota_kabupaten::where("kode_kota_kabupaten",$city_code)->first();

            if(!empty($city))
            {
                $user      = $this->ion_auth->user()->row();

                $city->dihapus       = 't';
                $city->tanggal_hapus = date('Y-m-d H:i:s');
                $city->dihapus_oleh  = $user->first_name . ' ' . $user->last_name;
                $delete                     = $city->save();

                if($delete)
                {
                    /*Get Provinse Detail*/
                    $old_province = ms_provinsi::find($city->kode_provinsi);

                    /* Write log */
                    $data_notif = array(
                        "Kode Kota/Kabupaten" => $city->kode_kota_kabupaten,
                        "Nama Provinsi"       => $old_province,
                        "Nama Kota/Kabupaten" => $city->nama_kota_kabupaten
                    );

                    $message = "Menghapus master kota/kabupaten " . $city->nama_kota_kabupaten;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
}