<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Label extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_label";
        $condition    = "ms_label.dihapus = 'F'";
        $row          = array('ms_label.kode_label','ms_label.nama_label');
        $row_search   = array('ms_label.kode_label','ms_label.nama_label');;
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if($this->input->is_ajax_request())
        {
            /* Url */
            $url_succees        = site_url() . $this->site;
            $url_error          = site_url() . $this->site . '/add';

            /* Get Data Post */
            $nama_label         = ucwords($this->input->post('nama_label'));

            /* Check In Table Is Data Available or Not */
            $ms_label           = ms_label::where('nama_label',$nama_label)->first();

            if(empty($ms_label))
            {
                $model_label                = new ms_label;

                $model_label->nama_label    = $nama_label;

                $save = $model_label->save();

                if($save)
                {
                    /* Write Log */
                    $data_notif = array(
                                        "Kode Label"    => ms_label::max('kode_label'),
                                        "Nama Label"    => $nama_label,
                                        );
                    $message = "Berhasil menambahkan master Label " . $nama_label;
                     $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);
                } else {
                    $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
                }
            }        
        } else {
            $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {   
        $kode_label     = decryptID($id);
        $ms_label       = ms_label::where('kode_label', $kode_label)->first();

        if(!empty($ms_label))
        {
            /* Button Action */
            $data['action']     = site_url() . $this->site . '/update';

            $data['ms_label']   = $ms_label;


            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        } else {
            redirect(site_url() . $this->site);
        }


    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($this->input->is_ajax_request())
        {
            $id          = $this->input->post('id');
            $kode_label  = decryptID($id);
            
            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/edit/' . $id;
            
            /* Get Data Post */
            $nama_label  = ucwords($this->input->post('nama_label'));
            
            $ms_label    = ms_label::where('nama_label',$nama_label)->whereRaw('kode_label != '.$kode_label.'')->first(); 
            
            if(empty($ms_label)) {

                $model_label    = ms_label::where('kode_label', $kode_label)->first();

                /* Array for write log */
                $data_old = array(
                            "Kode Label" => $model_label->kode_label,
                            "Nama Label" => $model_label->nama_label
                            );

                $data_new = array(
                            "Kode Label" => $kode_label,
                            "Nama Label" => $nama_label
                            );
                /* End array for write log */

                /* Initialize Data */
                $model_label->nama_label    = $nama_label;

                /* Save */
                $save = $model_label->save();

                if($save) {

                     /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message     = 'Memperbarui master Label ' . $nama_label;
                    $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                    /* End Write Log*/

                    $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
                } else {
                    $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
                }
            } else {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
       if ($this->input->is_ajax_request()) 
        {  
            $url        = site_url() . $this->site;
            $id         = $this->input->get("id");
            $kode_label = decryptID($id);
            
            $ms_label   = ms_label::where("kode_label",$kode_label)->first();

            if(!empty($ms_label))
            {
                $user      = $this->ion_auth->user()->row();

                $ms_label->dihapus       = 't';
                $ms_label->tanggal_hapus = date('Y-m-d H:i:s');
                $ms_label->dihapus_oleh  = $user->first_name . ' ' . $user->last_name;
                $delete                  = $ms_label->save();

                if($delete)
                {
                    /* Write log */
                    $data_notif = array(
                        "Kode Label" => $kode_label,
                        "Nama Label" => $ms_label->nama_label,
                    );

                    $message = "Menghapus master Label " . $ms_label->nama_label;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } 
    }
}