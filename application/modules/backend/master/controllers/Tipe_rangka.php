<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tipe_rangka extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:ms_tipe_rangka
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_tipe_rangka";
        $condition    = "ms_tipe_rangka.dihapus = 'F'";
        $row          = array('ms_tipe_rangka.kode_tipe_rangka', 'ms_tipe_rangka.nama_tipe_rangka');
        $row_search   = array('ms_tipe_rangka.kode_tipe_rangka', 'ms_tipe_rangka.nama_tipe_rangka');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table: ms_tipe_rangka
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Url */
            $url_succees         = site_url() . $this->site;
            $url_error           = site_url() . $this->site . '/add';
            
            /* Get Data Post */
            $nama_tipe_rangka = ucwords($this->input->post('nama_tipe_rangka'));

            /* check in table available or not */
            $ms_tipe_rangka   = ms_tipe_rangka::where('nama_tipe_rangka',$nama_tipe_rangka)->first();

            if(empty($ms_tipe_rangka))
            {
                $model = new ms_tipe_rangka;

                $model->nama_tipe_rangka = $nama_tipe_rangka;

                $save = $model->save();

                if($save)
                {
                    /* Write Log */
                    $data_notif = array(
                                        "Kode Tipe Rangka"  => ms_tipe_rangka::max('kode_tipe_rangka'),
                                        "Nama tipe rangka" => $nama_tipe_rangka,
                                        );

                    $message = "Berhasil menambahkan master tipe rangka " . $nama_tipe_rangka;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);

                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
                }          
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $kode_tipe_rangka = decryptID($id);
        $ms_tipe_rangka   = ms_tipe_rangka::where('kode_tipe_rangka',$kode_tipe_rangka)->first();

        if(!empty($ms_tipe_rangka))
        {
            /* Button Action */
            $data['action']            = site_url() . $this->site . '/update';

            $data['ms_tipe_rangka'] = $ms_tipe_rangka;

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:ms_tipe_rangka
    * @param Post Data
    * @return page index
    **/
    function update()
    {   
        if ($this->input->is_ajax_request()) 
        {   
            $id                  = $this->input->post("id");
            $kode_tipe_rangka = decryptID($id);

            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/edit/' . $id;

            /* Get Data Post */
            $nama_tipe_rangka = ucwords($this->input->post("nama_tipe_rangka"));

            /* check in table available or not */
            $ms_tipe_rangka = ms_tipe_rangka::where('nama_tipe_rangka',$nama_tipe_rangka)->whereRaw('kode_tipe_rangka != '.$kode_tipe_rangka.'')->first();

            if(empty($ms_tipe_rangka))
            {
                $model = ms_tipe_rangka::where('kode_tipe_rangka',$kode_tipe_rangka)->first();

                /* Array for write log */
                $data_old = array(
                            "Kode Tipe Rangka" => $model->kode_tipe_rangka,
                            "Nama tipe rangka" => $model->nama_tipe_rangka
                            );

                $data_new = array(
                            "Kode Tipe Rangka" => $kode_tipe_rangka,
                            "Nama tipe rangka" => $nama_tipe_rangka
                            );
                /* End array for write log */

                /* Initialize Data */
                $model->nama_tipe_rangka = $nama_tipe_rangka;

                /* Save */
                $save = $model->save();

                if($save)
                {
                    /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message     = 'Memperbarui master tipe rangka ' . $nama_tipe_rangka;
                    $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                    /* End Write Log*/

                    $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * Delete data from table:ms_tipe_rangka
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url              = site_url() . $this->site;
            $id               = $this->input->get("id");
            $kode_tipe_rangka = decryptID($id);
            
            $ms_tipe_rangka   = ms_tipe_rangka::where("kode_tipe_rangka",$kode_tipe_rangka)->first();

            if(!empty($ms_tipe_rangka))
            {
                $user      = $this->ion_auth->user()->row();

                $ms_tipe_rangka->dihapus       = 't';
                $ms_tipe_rangka->tanggal_hapus = date('Y-m-d H:i:s');
                $ms_tipe_rangka->dihapus_oleh  = $user->first_name . ' ' . $user->last_name;
                $delete                         = $ms_tipe_rangka->save();

                if($delete)
                {
                    /* Write log */
                    $data_notif = array(
                        "Kode Tipe Rangka" => $kode_tipe_rangka,
                        "Nama tipe rangka" => $ms_tipe_rangka->nama_tipe_rangka,
                    );

                    $message = "Menghapus master tipe rangka " . $ms_tipe_rangka->nama_tipe_rangka;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
}