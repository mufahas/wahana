<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        $this->load->model('select_global_model');
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:ms_gambar_banner_home
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_gambar_banner_home";
        $condition    = "ms_gambar_banner_home.dihapus = 'F'";
        $row          = array('ms_gambar_banner_home.kode_gambar_banner', 'ms_gambar_banner_home.nama_gambar','ms_gambar_banner_home.jenis','ms_gambar_banner_home.publish_status');
        $row_search   = array('ms_gambar_banner_home.kode_gambar_banner', 'ms_gambar_banner_home.nama_gambar','ms_gambar_banner_home.jenis','ms_gambar_banner_home.publish_status');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['url_succees']  = site_url() . $this->site;
        $data['action']       = site_url() . $this->site . '/save';
        
        $data['jenis_banner'] = $this->select_global_model->selectJenisBanner();
 
        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        $jenis              = $this->input->post('jenis');
        $url_banner         = $this->input->post('url_banner');
        $publish            = $this->input->post('publish_status');
        $page               = $this->input->post('new_page');
        
        if ($publish == "undefined") 
        {
            $publish_status = 'F';
        }
        else
        {
            $publish_status = 'T';
        }
        
        if ($page == "") 
        {
            $new_page = 'F';
        }
        else
        {
            $new_page = 'T';
        }
        
        $filename           = $_FILES["file"]["name"];
        $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
        $file_ext           = substr($filename, strripos($filename, '.')); // get file name
        $filesize           = $_FILES["file"]["size"];
        $allowed_file_types = array('.jpg','.jpeg','.png');  

        if (in_array($file_ext,$allowed_file_types) && $filesize <= 2000000)
        {   
            // Rename file
            // $newfilename = 'BANNER_'. time() . '_'. md5($filename) . $file_ext;
            $newfilename = $filename;
            if (file_exists("assets/upload/banner/" . $newfilename))
            {
                // file already exists error
                // echo "You have already uploaded this file.";
                $status = array('status' => 'error', 'message' => lang('message_save_failed') . ' ' . $filename);
            }
            else
            {       
                if(move_uploaded_file($_FILES["file"]["tmp_name"], "assets/upload/banner/" . $newfilename)){
                    
                    $model = new ms_gambar_banner_home;

                    $model->jenis          = $jenis;
                    $model->url_banner     = $url_banner;
                    $model->new_page       = $new_page;
                    $model->publish_status = $publish_status;
                    $model->nama_gambar    = $newfilename;

                    $save = $model->save();

                    if($save){

                        /* Write Log */
                        $data_notif = array(
                                            "Kode Gambar Banner"  => ms_gambar_banner_home::max('kode_gambar_banner'),
                                            "Nama Gambar"         => $newfilename,
                                            "Url Gambar"          => $url_banner,
                                            "Halaman Baru"        => ($new_page == 'T') ? 'Ya' : 'Tidak',
                                            "Jenis"               => ($jenis == 'H') ? 'Home' : 'Aksesoris',
                                            "Status"              => ($jenis == 'T') ? 'Aktif' : 'Tidak Aktif',
                                            "Url Photo"           => site_url() . 'assets/upload/banner/' . $newfilename,
                                            );

                        $message = "Berhasil menambahkan master banner " . $newfilename;
                        $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                        /* End Write Log */

                        //echo "File uploaded successfully.";     
                        $status = array('status' => 'success','message' => lang('message_save_success') . ' ' . $filename);
                    }
                }
            }
        }
        elseif (empty($file_basename))
        {   
            // file selection error
            // echo "Please select a file to upload.";
            $status = array('status' => 'error', 'message' => lang('message_save_failed'));
        } 
        elseif ($filesize >= 2000000)
        {   
            // file size error
            // echo "The file you are trying to upload is too large.";
            $status = array('status' => 'error', 'message' => lang('message_save_failed'));
        }
        else
        {
            // file type error
            //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
            $status = array('status' => 'error', 'message' => lang('message_save_failed'));
            unlink($_FILES["file"]["tmp_name"]);
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $kode_gambar_banner    = decryptID($id);
        $ms_gambar_banner_home = ms_gambar_banner_home::where('kode_gambar_banner',$kode_gambar_banner)->first();

        if(!empty($ms_gambar_banner_home))
        {
            /* Button Action */
            $data['url_succees']           = site_url() . $this->site;
            $data['action']                = site_url() . $this->site . '/update';
            
            $data['ms_gambar_banner_home'] = $ms_gambar_banner_home;
            $data['jenis_banner']          = $this->select_global_model->selectJenisBanner();

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:ms_gambar_banner_home
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        $id                 = $this->input->post("id");
        $kode_gambar_banner = decryptID($id);

        /* Url */
        $url_succees = site_url() . $this->site;
        $url_error   = site_url() . $this->site . '/edit/' . $id;

        /* Get Data Post */
        $jenis      = $this->input->post('jenis');
        $url_banner = $this->input->post('url_banner');
        $publish    = $this->input->post('publish_status');
        $page       = $this->input->post('new_page');
        
        if (empty($publish)) 
        {
            $publish_status = 'F';
        }
        else
        {
            if ($publish == "undefined") {
                $publish_status = 'F';
            }else{ 
                $publish_status = 'T';   
            }
        }
        
        if ($page == "") 
        {
            $new_page = 'F';
        }
        else
        {
            $new_page = 'T';
        }
        
        $userfile = $_FILES;

        $model = ms_gambar_banner_home::where('kode_gambar_banner',$kode_gambar_banner)->first();

        /* Array for write log */
        $data_old = array(
                    "Kode Kategori Motor" => $model->kode_gambar_banner,
                    "Nama Gambar"         => $model->nama_gambar,
                    "Url Gambar"          => $model->url_banner,
                    "Pindah Halaman"      => ($model->new_page == 'T') ? 'Ya' : 'Tidak',
                    "Jenis"               => ($model->jenis == 'H') ? 'Home' : 'Aksesoris',
                    "Status"              => ($model->publish_status == 'T') ? 'Aktif' : 'Tidak Aktif',
                    "Url Photo"           => site_url() . 'assets/upload/banner/' . $model->nama_gambar,
                    );


        if(!empty($_FILES))
        {
            $filename           = $_FILES["file"]["name"];
            $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext           = substr($filename, strripos($filename, '.')); // get file name
            $filesize           = $_FILES["file"]["size"];
            $allowed_file_types = array('.jpg','.jpeg','.png');  

            if (in_array($file_ext,$allowed_file_types) && $filesize <= 2000000)
            {   
                // Rename file
                // $newfilename = 'BANNER_'. time() . '_'. md5($filename) . $file_ext;
                $newfilename = $filename;
                if (file_exists("assets/upload/banner/" . $newfilename))
                {
                    // file already exists error
                    // echo "You have already uploaded this file.";
                    $status = array('status' => 'error', 'message' => lang('message_update_failed') . ' ' . $filename);
                }
                else
                {       
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], "assets/upload/banner/" . $newfilename)){
                        
                        /* Delete file old */
                        if(file_exists("assets/upload/banner/" . $model->nama_gambar))
                        {
                            unlink("assets/upload/banner/" . $model->nama_gambar);
                        }

                        $model->jenis          = $jenis;
                        $model->url_banner     = $url_banner;
                        $model->new_page       = $new_page;
                        $model->publish_status = $publish_status;
                        $model->nama_gambar    = $newfilename;

                        $save = $model->save();

                        if($save){

                            /* Write Log */
                            $data_new = array(
                                                "Kode Kategori Motor" => $kode_gambar_banner,
                                                "Nama Gambar"         => $newfilename,
                                                "Url Gambar"          => $url_banner,
                                                "Pindah Halaman"      => ($new_page == 'T') ? 'Ya' : 'Tidak',
                                                "Jenis"               => ($jenis == 'H') ? 'Home' : 'Aksesoris',
                                                "Status"              => ($publish_status == 'T') ? 'Aktif' : 'Tidak Aktif',
                                                "Url Photo"           => site_url() . 'assets/upload/banner/' . $newfilename,
                                                );

                            /* Write Log */
                            $data_change = array_diff_assoc($data_new, $data_old);
                            $message     = 'Memperbarui master banner ' . $newfilename;
                            $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                            /* End Write Log*/

                            //echo "File uploaded successfully.";     
                            $status = array('status' => 'success','message' => lang('message_update_success') . ' ' . $filename);
                        }
                    }
                }
            }
            elseif (empty($file_basename))
            {   
                // file selection error
                // echo "Please select a file to upload.";
                $status = array('status' => 'error', 'message' => lang('message_update_failed'));
            } 
            elseif ($filesize >= 2000000)
            {   
                // file size error
                // echo "The file you are trying to upload is too large.";
                $status = array('status' => 'error', 'message' => lang('message_update_failed'));
            }
            else
            {
                // file type error
                //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
                $status = array('status' => 'error', 'message' => lang('message_update_failed'));
                unlink($_FILES["file"]["tmp_name"]);
            }
        }
        else
        {
            $model->jenis          = $jenis;
            $model->url_banner     = $url_banner;
            $model->new_page       = $new_page;
            $model->publish_status = $publish_status;

            $save = $model->save();

            if($save){

                /* Write Log */
                $data_new = array(
                                    "Kode Kategori Motor" => $kode_gambar_banner,
                                    "Nama Gambar"         => $model->nama_gambar,
                                    "Url Gambar"          => $url_banner,
                                    "Pindah Halaman"      => ($new_page == 'T') ? 'Ya' : 'Tidak',
                                    "Jenis"               => ($jenis == 'H') ? 'Home' : 'Aksesoris',
                                    "Status"              => ($publish_status == 'T') ? 'Aktif' : 'Tidak Aktif',
                                    "Url Photo"           => site_url() . 'assets/upload/banner/' . $model->nama_gambar,
                                    );

                /* Write Log */
                $data_change = array_diff_assoc($data_new, $data_old);
                $message     = 'Memperbarui master banner ' . $model->nama_gambar;
                $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                /* End Write Log*/

                $status = array('status' => 'success','message' => lang('message_update_success'));
            }
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url                = site_url() . $this->site;
            $id                 = $this->input->get("id");
            $kode_gambar_banner = decryptID($id);
            
            $ms_gambar_banner_home   = ms_gambar_banner_home::where("kode_gambar_banner",$kode_gambar_banner)->first();

            if(!empty($ms_gambar_banner_home))
            {
                $user = $this->ion_auth->user()->row();

                $ms_gambar_banner_home->dihapus       = 't';
                $ms_gambar_banner_home->tanggal_hapus = date('Y-m-d H:i:s');
                $ms_gambar_banner_home->dihapus_oleh  = $user->first_name . ' ' . $user->last_name;
                $delete = $ms_gambar_banner_home->save();

                if($delete)
                {
                    /* Write log */
                    $data_notif = array(
                        "Kode Kategori Motor" => $ms_gambar_banner_home->kode_gambar_banner,
                        "Nama Gambar"         => $ms_gambar_banner_home->nama_gambar,
                        "Jenis"               => ($ms_gambar_banner_home->jenis == 'H') ? 'Home' : 'Aksesoris',
                        "Status"               => ($ms_gambar_banner_home->publish_status == 'T') ? 'Aktif' : 'Tidak Aktif',
                        "Url Photo"           => site_url() . 'assets/upload/banner/' . $ms_gambar_banner_home->nama_gambar,
                    );

                    $message = "Menghapus master banner " . $ms_gambar_banner_home->nama_gambar;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
}