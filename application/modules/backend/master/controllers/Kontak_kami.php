<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak_kami extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        /* Load Model */
        $this->load->model('select_global_model');
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['view']      = site_url() . $this->site . '/view';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_kontak_wahana";
        $condition    = "";
        $row          = array('ms_kontak_wahana.kode_kontak_wahana','ms_kontak_wahana.nama_kontak','ms_kontak_wahana.jenis_kontak','ms_kontak_wahana.email','ms_kontak_wahana.nomor_telepon1');
        $row_search   = array('ms_kontak_wahana.kode_kontak_wahana','ms_kontak_wahana.nama_kontak','ms_kontak_wahana.jenis_kontak','ms_kontak_wahana.email','ms_kontak_wahana.nomor_telepon1');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        /* Get Dropdown */
        $data['jenis_kontak'] = $this->select_global_model->selectJenisKontak();

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Url */
            $url_succees           = site_url() . $this->site;
            $url_error             = site_url() . $this->site . '/add';
            
            /* Get Data Post */
            $nama_kontak           = ucwords($this->input->post('nama_kontak'));
            $jenis_kontak          = $this->input->post('jenis_kontak');
            $email                 = $this->input->post('email');
            $alamat_kontak         = ucwords($this->input->post('alamat_kontak'));
            $nomor_telepon1        = $this->input->post('nomor_telepon1');
            $nomor_telepon2        = $this->input->post('nomor_telepon2');
            
            /* Start Save Data */
            $model                 = new ms_kontak_wahana;
            
            $model->nama_kontak    = $nama_kontak;
            $model->jenis_kontak   = $jenis_kontak;
            $model->email          = $email;
            $model->alamat_kontak  = $alamat_kontak;
            $model->nomor_telepon1 = $nomor_telepon1;
            $model->nomor_telepon2 = $nomor_telepon2;
            
            $save                  = $model->save();

            if($save)
            {
                /* Write Log */
                $data_notif = array(
                                    "Kode Kontak Wahana" => ms_kontak_wahana::max('kode_kontak_wahana'),
                                    "Nama Kontak "       => $nama_kontak,
                                    "Jenis Kontak "      => $jenis_kontak,
                                    "Email"              => $email,
                                    "Alamat"             => $alamat_kontak,
                                    "Telepon 1"          => $nomor_telepon1,
                                    "Telepon 2"          => $nomor_telepon2
                                    );

                $message = "Berhasil menambahkan master kontak wahana" . $nama_kontak;
                $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                /* End Write Log */

                $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);

            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
            }          
            /* End Save Data */

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $kode_kontak_wahana = decryptID($id);
        $ms_kontak_wahana   = ms_kontak_wahana::where('kode_kontak_wahana',$kode_kontak_wahana)->first();

        if(!empty($ms_kontak_wahana))
        {
            /* Button Action */
            $data['action']             = site_url() . $this->site . '/update';

            $data['ms_kontak_wahana']   = $ms_kontak_wahana;
            $data['jenis_kontak']       = $this->select_global_model->selectJenisKontak();

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($this->input->is_ajax_request()) 
        {   
            $id                    = $this->input->post("id");
            $kode_kontak_wahana    = decryptID($id);

            /* Url */
            $url_succees           = site_url() . $this->site;
            $url_error             = site_url() . $this->site . '/edit/' . $id;

            /* Get Data Post */
            $nama_kontak           = ucwords($this->input->post('nama_kontak'));
            $jenis_kontak          = $this->input->post('jenis_kontak');
            $email                 = $this->input->post('email');
            $alamat_kontak         = ucwords($this->input->post('alamat_kontak'));
            $nomor_telepon1        = $this->input->post('nomor_telepon1');
            $nomor_telepon2        = $this->input->post('nomor_telepon2');

            /* Get User Login */
            $user                  = $this->ion_auth->user()->row();

            /* Get Model ms_kalkulator_kredit */
            $model                 = ms_kontak_wahana::where('kode_kontak_wahana',$kode_kontak_wahana)->first();

            /* Array for write log */
            $data_old = array(
                        "Kode Kontak Wahana" => $model->kode_kontak_wahana,
                        "Nama Kontak Wahana" => $model->nama_kontak,
                        "Jenis Kontak"       => $model->jenis_kontak,
                        "Email"              => $model->email,
                        "Alamat"             => $model->alamat_kontak,
                        "Nomor Telepon I"    => $model->nomor_telepon1,
                        "Nomor Telepon II"   => $model->nomor_telepon2
                        );

            $data_new = array(
                        "Kode Kontak Wahana" => $kode_kontak_wahana,
                        "Nama Kontak Wahana" => $nama_kontak,
                        "Jenis Kontak"       => $jenis_kontak,
                        "Email"              => $email,
                        "Alamat"             => $alamat_kontak,
                        "Nomor Telepon I"    => $nomor_telepon1,
                        "Nomor Telepon II"   => $nomor_telepon2
                        );
            /* End array for write log */

            /* Initialize Data */
            $model->nama_kontak             = $nama_kontak;
            $model->jenis_kontak            = $jenis_kontak;
            $model->email                   = $email;
            $model->nomor_telepon1          = $nomor_telepon1;
            $model->nomor_telepon2          = $nomor_telepon2;
            $model->alamat_kontak           = $alamat_kontak;
            $model->diubah_oleh             = $user->first_name . ' ' . $user->last_name;
            $model->tanggal_terakhir_diubah = date('Y-m-d H:i:s');

            /* Save */
            $save = $model->save();

            if($save)
            {
                /* Write Log */
                $data_change = array_diff_assoc($data_new, $data_old);
                $message     = 'Memperbarui master kontak wahana ' . $nama_kontak;
                $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                /* End Write Log*/

                $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
            }
            
            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        
    }

    /**
    * View data from table:
    * @param Id
    * @return page index
    **/
    function view($id)
    {
        $kode_kontak_wahana = decryptID($id);
        $ms_kontak_wahana   = ms_kontak_wahana::where('kode_kontak_wahana',$kode_kontak_wahana)->first();

        if(!empty($ms_kontak_wahana))
        {
            $data['ms_kontak_wahana']   = $ms_kontak_wahana;
            $data['jenis_kontak']       = $this->select_global_model->selectJenisKontak();

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
        
    }
}