<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gso extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable']       = site_url() . $this->site . '/loadTable';
        $data['add']             = site_url() . $this->site . '/add';
        $data['edit']            = site_url() . $this->site . '/edit';
        $data['delete']          = site_url() . $this->site . '/delete';
        $data['message_success'] = $this->session->flashdata('message_success');
        $data['message_error']   = $this->session->flashdata('messages_error');

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_gso";
        $condition    = "";
        $row          = array('ms_gso.kode_gso_wahana','ms_gso.layanan_vip','ms_gso.fasilitas_kelebihan');
        $row_search   = array('ms_gso.kode_gso_wahana','ms_gso.layanan_vip','ms_gso.fasilitas_kelebihan');;
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']          = site_url() . $this->site . '/save';

        $data['message_success'] = $this->session->flashdata('message_success');
        $data['message_error']   = $this->session->flashdata('messages_error');

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            
            $user                = $this->ion_auth->user()->row();

            /* Set Input Post */
            $layanan_vip         = $this->input->post('layanan_vip');
            $fasilitas_kelebihan = $this->input->post('fasilitas_kelebihan');
            
            /* Set Post Upload For Presentasi Produk Extension power point*/
            $userfile            = $_FILES["userfile"]["name"];
            $userfile_size       = $_FILES["userfile"]["size"];
            $userfile_tmp_name   = $_FILES["userfile"]["tmp_name"];
            
            /* Set Post Upload For Presentasi Daftar Harga excel*/
            $userfile_           = $_FILES["userfile_"]["name"];
            $userfile_size_      = $_FILES["userfile_"]["size"];
            $userfile_tmp_name_  = $_FILES["userfile_"]["tmp_name"];

            if(!empty($userfile) && !empty($userfile_))
            {
                /* PPT or PDF Presentasi Produk */
                $filename            = $userfile;
                $file_basename       = substr($filename, 0, strripos($filename, '.')); // get file extention
                $file_ext            = substr($filename, strripos($filename, '.')); // get file name extension
                $filesize            = $userfile_size; //get size
                $allowed_file_types  = array('.ppt','.pptx','.pdf');
                
                /* EXCEL or PDF Daftar Harga */
                $filename_           = $userfile_;
                $file_basename_      = substr($filename_, 0, strripos($filename_, '.')); // get file extention
                $file_ext_           = substr($filename_, strripos($filename_, '.')); // get file name extension
                $filesize_           = $userfile_size_; //get size
                $allowed_file_types_ = array('.xls','.xlsx','.pdf');

                if(in_array(strtolower($file_ext),$allowed_file_types) && in_array(strtolower($file_ext_),$allowed_file_types_) && $filesize <= 1000000000 && $filesize_ <= 5000000000) {

                    //Set new filename
                    $file_ppt   = 'GSO_PRESENTASI_'. date('d-m-Y') . $file_ext;
                    $file_excel = 'GSO_DAFTARHARGA_'. date('d-m-Y') . $file_ext_;


                    //Condition Error If File Exist
                    if(file_exists("assets/upload/gso/" . $file_ppt) && file_exists("assets/upload/gso/" . $file_excel) ) {
                        $this->session->set_flashdata('message_error', lang('message_save_failed'));
                        redirect(site_url() . $this->site . '/add');
                    } else {
                        if(move_uploaded_file($userfile_tmp_name,  "assets/upload/gso/" . $file_ppt) && move_uploaded_file($userfile_tmp_name_,  "assets/upload/gso/" . $file_excel)) {

                            $ms_gso = new ms_gso;
                            
                            $ms_gso->layanan_vip         = $layanan_vip;
                            $ms_gso->fasilitas_kelebihan = $fasilitas_kelebihan;
                            $ms_gso->presentasi_produk   = $file_ppt;
                            $ms_gso->daftar_harga        = $file_excel;

                            $save_ms_gso = $ms_gso->save();

                            /* Write Log */
                            $data_notif = array(
                                 "Layanan VIP "       => $layanan_vip
                                 );

                            $message = "Berhasil Menambah Master GSO" . $layanan_vip;
                            $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                            /* End Write Log */
                        }
                        $this->session->set_flashdata('message_success', lang('message_save_success'));
                        redirect(site_url() . $this->site);
                    } 
                } elseif(empty($file_basename) && empty($file_basename_)) {
                    $this->session->set_flashdata('message_error', lang('message_save_failed'));
                    redirect(site_url() . $this->site . '/add');
                } elseif ($filesize >= 1000000000 && $filesize_ >= 5000000000) {
                    $this->session->set_flashdata('message_error', lang('message_save_failed'));
                    redirect(site_url() . $this->site . '/add');
                } else {
                    $this->session->set_flashdata('message_error', lang('message_save_failed'));
                    unlink($userfile_tmp_name);
                    redirect(site_url() . $this->site . '/add');
                }
            }
        } else {
            $this->session->set_flashdata('message_error', lang('message_save_failed'));
            redirect(site_url() . $this->site . '/add');
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $kode_gso_wahana = decryptID($id);
       
        $ms_gso = ms_gso::where('kode_gso_wahana',$kode_gso_wahana)->first();

        if(!empty($ms_gso)) {

            /* Button Action */
            $data['action']    = site_url() . $this->site . '/update';
            
            /* Set Path Foto */
            $data['path_foto'] = site_url() . 'assets/upload/gso/';
            $data['gso']       = $ms_gso;
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        } else {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        // redirect(site_url() . $this->site);
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $user                = $this->ion_auth->user()->row();
            $id                  = $this->input->post('id');
            $kode_gso_wahana     = decryptID($id);
            
            /* Set Input Post */
            $layanan_vip         = $this->input->post('layanan_vip');
            $fasilitas_kelebihan = $this->input->post('fasilitas_kelebihan');

            /* Set Post Upload For Presentasi Produk Extension power point*/
            $userfile            = $_FILES["userfile"]["name"];
            $userfile_size       = $_FILES["userfile"]["size"];
            $userfile_tmp_name   = $_FILES["userfile"]["tmp_name"];
            
            /* Set Post Upload For Presentasi Daftar Harga excel*/
            $userfile_          = $_FILES["userfile_"]["name"];
            $userfile_size_     = $_FILES["userfile_"]["size"];
            $userfile_tmp_name_ = $_FILES["userfile_"]["tmp_name"];
            
            $ms_gso             = ms_gso::where('kode_gso_wahana',$kode_gso_wahana)->first();
            $old_foto_ppt       = 'assets/upload/gso/' . $ms_gso->presentasi_produk;
            $old_foto_excel     = 'assets/upload/gso/' . $ms_gso->daftar_harga;


            if(!empty($userfile) && empty($userfile_)) //Kondisi Jika File Excel Tidak Kosong dan File PPT Kosong
            {
                /* PPT or PDF Presentasi Produk */
                $filename            = $userfile;
                $file_basename       = substr($filename, 0, strripos($filename, '.')); // get file extention
                $file_ext            = substr($filename, strripos($filename, '.')); // get file name extension
                $filesize            = $userfile_size; //get size
                $allowed_file_types  = array('.ppt','.pptx','.pdf');

                if(in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 100000000) 
                {
                	$file_ppt   = 'GSO_PRESENTASI_'. date('d-m-Y') . $file_ext;

                	if(file_exists("assets/upload/gso/" . $file_ppt)) //Kondisi Jika File Sudah Ada
                	{
                		unlink("assets/upload/gso/" . $ms_gso->presentasi_produk);
                	}
                	
            		if(move_uploaded_file($userfile_tmp_name,  "assets/upload/gso/" . $file_ppt))
            		{
            			$ms_gso->layanan_vip             = $layanan_vip;
                        $ms_gso->fasilitas_kelebihan     = $fasilitas_kelebihan;
                        $ms_gso->presentasi_produk       = $file_ppt;
                        $ms_gso->tanggal_terakhir_diubah = date('Y-m-d H:i:s');
                        $ms_gso->diubah_oleh             = $user->username;

                        $save_ms_gso                     = $ms_gso->save();

                        /* Write Log */
                        $data_notif = array(
                             "Layanan VIP "       => $layanan_vip
                             );

                        $message = "Berhasil Ubah Data Master GSO" . $layanan_vip;
                        $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                        /* End Write Log */

                        $this->session->set_flashdata('message_success', lang('message_update_success'));
                        redirect(site_url() . $this->site);
            		}
                } else if(empty($file_basename)) { //Kondisi Berkas Kosong
                	$this->session->set_flashdata('message_error', 'File Kosong');
                    redirect(site_url() . $this->site);
                } else if($file_basename >= 100000000) { //Kondisi Ukuran Berkas Leboh dari 100 MB
                	$this->session->set_flashdata('message_error', 'Ukuran File Terlalu Besar');
                    redirect(site_url() . $this->site);
                } else {
                	$this->session->set_flashdata('message_error', lang('message_save_failed'));
                    unlink($userfile_tmp_name);
                    redirect(site_url() . $this->site);
                }
            } 
        	else if(empty($userfile) && !empty($userfile_)) //Kondisi Jika File Excel Kosong dan File PPT Tidak Kosong
            {	
                /* EXCEL or PDF Daftar Harga */
                $filename_           = $userfile_;
                $file_basename_      = substr($filename_, 0, strripos($filename_, '.')); // get file extention
                $file_ext_           = substr($filename_, strripos($filename_, '.')); // get file name extension
                $filesize_           = $userfile_size_; //get size
                $allowed_file_types_ = array('.xls','.xlsx','.pdf');

            	if(in_array(strtolower($file_ext_),$allowed_file_types_) && $filesize_ <= 50000000)
            	{
            	    $file_excel = 'GSO_DAFTARHARGA_'. date('d-m-Y') . $file_ext_;
            	    
            		if(file_exists("assets/upload/gso/" . $ms_gso->daftar_harga))
            		{
            		    unlink("assets/upload/gso/" . $ms_gso->daftar_harga);
            		}
            		
        			if(move_uploaded_file($userfile_tmp_name_,  "assets/upload/gso/" . $file_excel))
        			{
        				$ms_gso->layanan_vip             = $layanan_vip;
                        $ms_gso->fasilitas_kelebihan     = $fasilitas_kelebihan;
                        $ms_gso->daftar_harga            = $file_excel;
                        $ms_gso->tanggal_terakhir_diubah = date('Y-m-d H:i:s');
                        $ms_gso->diubah_oleh             = $user->username;

                        $save_ms_gso                     = $ms_gso->save();

                        /* Write Log */
                        $data_notif = array(
                             "Layanan VIP "       => $layanan_vip
                             );

                        $message = "Berhasil Ubah Data Master GSO" . $layanan_vip;
                        $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                        /* End Write Log */

                        $this->session->set_flashdata('message_success', lang('message_save_success'));
                        redirect(site_url() . $this->site);
        			}
            	} else if(empty($file_basename_)) { //Kondisi Jika Berkas Kosong
            		$this->session->set_flashdata('message_error', 'File Kosong');
                    redirect(site_url() . $this->site);
            	} else if($filesize_ >= 50000000) { //Kondisi Jika Ukuran Berkas Lebih dari 5 MB
					$this->session->set_flashdata('message_error', 'Ukuran File Terlalu Besar');
                    redirect(site_url() . $this->site);
            	} else {
            		$this->session->set_flashdata('message_error', lang('message_save_failed'));
                    unlink($userfile_tmp_name_);
                    redirect(site_url() . $this->site);
            	}
            } else if(!empty($userfile) && !empty($userfile_)) //Kondisi Jika Kedua Berkas (PPT dan Excel) Tidak Kosong
            {
            	$filename            = $userfile;
                $file_basename       = substr($filename, 0, strripos($filename, '.')); // get file extention
                $file_ext            = substr($filename, strripos($filename, '.')); // get file name extension
                $filesize            = $userfile_size; //get size
                $allowed_file_types  = array('.ppt','.pptx','.pdf');

				$filename_           = $userfile_;
                $file_basename_      = substr($filename_, 0, strripos($filename_, '.')); // get file extention
                $file_ext_           = substr($filename_, strripos($filename_, '.')); // get file name extension
                $filesize_           = $userfile_size_; //get size
                $allowed_file_types_ = array('.xls','.xlsx','.pdf');

				$file_ppt           = 'GSO_PRESENTASI_'. date('d-m-Y') . $file_ext;
				$file_excel         = 'GSO_DAFTARHARGA_'. date('d-m-Y') . $file_ext_;

                if(in_array(strtolower($file_ext),$allowed_file_types) && in_array(strtolower($file_ext_),$allowed_file_types_) && $filesize <= 100000000 && $filesize_ <= 50000000) 
                {

                    //Set new filename
                    $file_ppt   = 'GSO_PRESENTASI_'. date('d-m-Y') . $file_ext;
                    $file_excel = 'GSO_DAFTARHARGA_'. date('d-m-Y') . $file_ext_;


                    //Condition Error If File Exist
                    if(file_exists("assets/upload/gso/" . $file_ppt) && file_exists("assets/upload/gso/" . $file_excel)) //Kondisi Jika File Sudah Ada
                	{
                		unlink("assets/upload/gso/" . $ms_gso->presentasi_produk);
                		unlink("assets/upload/gso/" . $ms_gso->daftar_harga);
                	}
                	
                    if(move_uploaded_file($userfile_tmp_name,  "assets/upload/gso/" . $file_ppt) && move_uploaded_file($userfile_tmp_name_,  "assets/upload/gso/" . $file_excel))
                    {
                        
                        $ms_gso->layanan_vip         = $layanan_vip;
                        $ms_gso->fasilitas_kelebihan = $fasilitas_kelebihan;
                        $ms_gso->presentasi_produk   = $file_ppt;
                        $ms_gso->daftar_harga        = $file_excel;

                        $save_ms_gso = $ms_gso->save();

                        /* Write Log */
                        $data_notif = array(
                             "Layanan VIP "       => $layanan_vip
                             );

                        $message = "Berhasil Menambah Master GSO" . $layanan_vip;
                        $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                        /* End Write Log */
                    }
                    $this->session->set_flashdata('message_success', lang('message_save_success'));
                    redirect(site_url() . $this->site);
                } else if(empty($file_basename) && empty($file_basename_)) { //Kondisi Jika Kedua Berkas Kosong
                	$this->session->set_flashdata('message_error', 'File Kosong');
                    redirect(site_url() . $this->site);
                } else if($filesize >= 100000000 && $filesize_ >= 50000000) { //Kondisi Jika Ukuran Berkas Melebihi Batas Yang Sudah Ditentukan
                	$this->session->set_flashdata('message_error', 'Ukuran File Terlalu Besar');
                    redirect(site_url() . $this->site);
                } else {
                	$this->session->set_flashdata('message_error', lang('message_save_failed'));
                    unlink($userfile_tmp_name);
                    unlink($userfile_tmp_name_);
                    redirect(site_url() . $this->site);
                }
            } else {
            	$this->session->set_flashdata('message_error', lang('message_save_failed'));
            	redirect(site_url() . $this->site);
            }
        } else {
        	$this->session->set_flashdata('message_error', lang('message_save_failed'));
        	redirect(site_url() . $this->site);
        }
    }

    /**
    * Direct to page detail
    * @return page
    **/
    function view($id)
    {
		$kode_gso_wahana = decryptID($id);
		$ms_gso          = ms_gso::where('kode_gso_wahana',$kode_gso_wahana)->first();

		if(!empty($ms_gso))
		{
			$data['gso']       = $ms_gso;
			$data['path_file'] = base_url() . "assets/upload/gso/";

			$this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
		}  else {
			redirect(site_url() . $this->site);
		}
    }
}