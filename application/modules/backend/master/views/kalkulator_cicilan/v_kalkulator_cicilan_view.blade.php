@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Detail {!! get_menu_name() !!}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{$back}}" class="m-portlet__nav-link m-portlet__nav-link--icon">
                            <i class="la la-undo"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!--begin::Form-->
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            Simulasi Cicilan :
                        </label>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>DP</th>
                                    <th>Tenor 11</th>
                                    <th>Tenor 17</th>
                                    <th>Tenor 23</th>
                                    <th>Tenor 27</th>
                                    <th>Tenor 29</th>
                                    <th>Tenor 33</th>
                                    <th>Tenor 35</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($cicilan))
                                    @php 
                                        $no = 1 
                                    @endphp

                                    @foreach($cicilan as $cicil)
                                        <tr>
                                            <td align="center">{{ $no }}</td>
                                            <td align="center">{{ $cicil->dp }}</td>
                                            <td align="center">{{ $cicil->tenor_11 }}</td>
                                            <td align="center">{{ $cicil->tenor_17 }}</td>
                                            <td align="center">{{ $cicil->tenor_23 }}</td>
                                            <td align="center">{{ $cicil->tenor_27 }}</td>
                                            <td align="center">{{ $cicil->tenor_29 }}</td>
                                            <td align="center">{{ $cicil->tenor_33 }}</td>
                                            <td align="center">{{ $cicil->tenor_35 }}</td>
                                        </tr>
                                        @php 
                                            $no++ 
                                        @endphp
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" align="center" style="padding-left: 20%">There's no data</td>
                                    </tr>
                                @endif
                        </table>

                    </div>
                    
                </div>
            </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop