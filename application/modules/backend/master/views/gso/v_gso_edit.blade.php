@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form', 'enctype' => 'multipart/form-data')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            *
                            Layanan VIP:
                        </label>
                       {!! form_textarea('layanan_vip',$gso->layanan_vip,'id="" class="summernote form-control m-input" placeholder="Layanan VIP"') !!}
                    </div>
                    <div class="col-lg-6 ">
                        <label>
                            *
                            Fasilitas Kelebihan:
                        </label>
                       {!! form_textarea('fasilitas_kelebihan',$gso->fasilitas_kelebihan,'id="" class="summernote form-control m-input" placeholder="Fasilitas Kelebihan"') !!}
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label>
                            *
                            Presentasi Produk:
                        </label>  
                        @if(!empty($gso->presentasi_produk))
                        {!! form_input(array('type' => 'file','name' => 'userfile', 'id' => 'userfile' , 'value' => $gso->presentasi_produk ,'class' => 'form-control m-input', 'accept' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf', 'style' => 'display: none;')) !!}
                        <a href="{{ $path_foto . $gso->presentasi_produk }}">{{$gso->presentasi_produk}}</a>
                        @endif
                        <div>    
                            <button id="btn-image" type="button" class="btn blue btn-sm" onClick="$('#userfile').click()">
                                <i class="fa fa-upload"></i> Choose File
                            </button>
                        </div>
                        <br>
                        <span>
                            <ul>Keterangan:
                                <li>Jenis file: .pptx dan .pdf</li>
                                <li>Maksimal file 100mb</li>
                            </ul>
                        </span>
                    </div>
                    <div class="col-lg-6">
                        <label>
                            *
                            Daftar Harga:
                        </label> 
                        {!! form_input(array('type' => 'file','name' => 'userfile_', 'id' => 'userfile_' , 'value' => $gso->presentasi_produk, 'class' => 'form-control m-input', 'accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/pdf', 'style' => 'display: none;')) !!}
                        @if(!empty($gso->presentasi_produk))
                        <a href="{{ $path_foto . $gso->daftar_harga }}">{{$gso->daftar_harga}}</a>
                        @endif
                        <div>    
                            <button id="btn-image" type="button" class="btn blue btn-sm" onClick="$('#userfile_').click()">
                            <i class="fa fa-upload"></i> Choose File
                            </button>
                        </div>
                        <br>
                        <span>
                            <ul>Keterangan:
                                <li>Jenis file: .xlsx dan .pdf</li>
                                <li>Maksimal file 50mb</li>
                            </ul>
                        </span>
                    </div>
                </div>
                
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_update_"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($gso->kode_gso_wahana) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/master/gso/js/gso-edit.js" type="text/javascript"></script>
@stop