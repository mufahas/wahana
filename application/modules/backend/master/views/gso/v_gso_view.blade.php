@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open(null, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            Layanan VIP
                        </label>
                        <h5>{!! $gso->layanan_vip !!}</h5>
                    </div>
                    <div class="col-lg-6 ">
                        <label>
                            Fasilitas Kelebihan:
                        </label>
                        <h5>{!! $gso->fasilitas_kelebihan !!}</h5>
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label>
                            Presentasi Produk
                        </label>
                        <h5><a href="{{ $path_file . $gso->presentasi_produk }}">{{ $gso->presentasi_produk }}</a></h5>
                    </div>
                    <div class="col-lg-6">
                        <label>
                            Daftar Harga
                        </label>
                        <h5><a href="{{ $path_file . $gso->daftar_harga }}">{{ $gso->daftar_harga }}</a></h5>
                    </div>
                </div>
            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop