@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form', 'enctype' => 'multipart/form-data')) !!}
            <div class="m-portlet__body">
                @if(!empty($message_success)) 
                <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show success" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="m-alert__text">
                        {{$message_success}}
                    </div>
                    <div class="m-alert__close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
                @endif
                @if(!empty($message_error))
                <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-error alert-dismissible fade show error" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="m-alert__text">
                        {{$message_error}}
                    </div>
                    <div class="m-alert__close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
                @endif
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            *
                            Layanan VIP:
                        </label>
                       {!! form_textarea('layanan_vip','','id="" class="summernote form-control"') !!}
                    </div>
                     <div class="col-lg-6 ">
                        <label>
                            *
                            Fasilitas Kelebihan:
                        </label>
                       {!! form_textarea('fasilitas_kelebihan','','id="" class="summernote form-control"') !!}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label>
                            *
                            Presentasi Produk:
                        </label> 
                        {!! form_input(array('type' => 'file','name' => 'userfile', 'class' => 'form-control m-input', 'accept' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf')) !!}
                    </div>
                    <div class="col-lg-6">
                        <label>
                            *
                            Daftar Harga:
                        </label> 
                        {!! form_input(array('type' => 'file','name' => 'userfile_', 'class' => 'form-control m-input', 'accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/pdf')) !!}
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_save_"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/master/gso/js/gso.js" type="text/javascript"></script>
@stop