@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-12">
                        <label>
                            *
                            Jenis:
                        </label>
                        {!! form_dropdown('jenis', $jenis_banner, '', 'class="form-control m-select2" id="m_select2_2"') !!}
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 ">
                        <label>
                            *
                            Photo:
                        </label> 
                        <div class="m-dropzone dropzone m-dropzone--primary dropzone-previews" id="my-awesome-dropzone">
                            <div class="m-dropzone__msg dz-message needsclick">
                                <h3 class="m-dropzone__msg-title">
                                    Jatuhkan berkas di sini atau klik untuk diunggah.
                                </h3>
                                <span class="m-dropzone__msg-desc">
                                    Upload hanya 1 berkas. Berkas maksimal 2 MB
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-12">
                        <label>
                            Url Banner:
                        </label>
                        {!! form_input(array('type' => 'url','name' => 'url_banner', 'class' => 'form-control m-input', 'placeholder' => 'Url Banner Image', 'maxlength' => '250' )) !!}
                    </div>
                    <div class="col-lg-12 mt-2">
                        <label class="m-checkbox chkbox">
                            <input type="checkbox" name="new_page" value="T">
                            Pindah Halaman
                            <span style="margin-top:4px"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label class="col-form-label">
                            *Status:
                        </label>
                        {!! form_input(array('type' => 'checkbox','name' => 'publish_status','data-on-text'=> 'Aktif', 'data-switch' => 'true', 'id' => 'm_switch_1', 'checked' => 'checked', 'value' => 'T','data-off-text'=>'Nonaktif', 'data-on-color'=>'danger' )) !!}
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_save"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var url              = "{{$action}}";
    var url_succees      = "{{$url_succees}}";
    var uploadMultiple   = false; 
    var autoProcessQueue = false;
    var maxFilesize      = 2;
    var paramName        = "file";
    var addRemoveLinks   = true;
    var maxFiles         = 1;
    var parallelUploads  = 1;
    var acceptedFiles    = 'image/jpeg,image/png';

    $('.save').attr("disabled",true);
</script>
<script src="{{ base_url() }}assets/backend/js/master/banner/js/banner.js" type="text/javascript"></script>
@stop