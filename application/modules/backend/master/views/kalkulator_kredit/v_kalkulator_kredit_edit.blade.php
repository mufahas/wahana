@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Persentase Bunga:
                        </label>
                        {!! form_input(array('type' => 'number','name' => 'persentase_bunga', 'class' => 'form-control m-input', 'placeholder' => 'Persentase Bunga', 'value' => $ms_kalkulator_kredit->persentase_bunga, 'min' => 0)) !!}
                    </div>

                    <div class="col-lg-6 form-group ">
                        <label>
                            *
                            Perhitungan Jenis Bunga
                        </label>
                        <div class="input-group"> 
                            {!! form_dropdown('perhitungan_jenis_bunga', $select_perhitungan_jenis_bunga, $ms_kalkulator_kredit->perhitungan_jenis_bunga, 'class="form-control m-select2" id="m_select2_2"') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_update"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($ms_kalkulator_kredit->kode_kalkulator_kredit) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/master/kalkulator_kredit/js/kalkulator-kredit.js" type="text/javascript"></script>
@stop