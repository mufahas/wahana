@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Nama Kontak:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'nama_kontak', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Nama Kontak', 'value' => $ms_kontak_wahana->nama_kontak )) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Nomor Telepon I:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'nomor_telepon1', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Nomor Telepon I', 'value' => $ms_kontak_wahana->nomor_telepon1)) !!}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Jenis Kontak:
                        </label>
                        <div class="input-group"> 
                            {!! form_dropdown('jenis_kontak', $jenis_kontak, $ms_kontak_wahana->jenis_kontak, 'class="form-control m-select2" id="m_select2_2"') !!}
                        </div>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Nomor Telepon II:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'nomor_telepon2', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Nomor Telepon II', 'value' => $ms_kontak_wahana->nomor_telepon2)) !!}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Email:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'email', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Alamat Email', 'value' => $ms_kontak_wahana->email)) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Alamat:
                        </label>
                        {!! form_textarea('alamat_kontak', $ms_kontak_wahana->alamat_kontak,'id="" class="form-control" placeholder = "Masukkan Alamat Kontak" ') !!} 
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_update"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($ms_kontak_wahana->kode_kontak_wahana) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/master/kontak_wahana/js/kontak-wahana.js" type="text/javascript"></script>
@stop