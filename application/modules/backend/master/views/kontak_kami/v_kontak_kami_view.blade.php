@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Detail {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Nama Kontak:
                        </label>
                        <h5>{{ $ms_kontak_wahana->nama_kontak }}</h5>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            Nomor Telepon I:
                        </label>
                        <h5>{{ $ms_kontak_wahana->nomor_telepon1 }}</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Jenis Kontak:
                        </label>
                        @if ($ms_kontak_wahana->jenis_kontak == 'P')
                            <h5>Pusat</h5>
                        @elseif ($ms_kontak_wahana->jenis_kontak == 'C') 
                            <h5>Cabang</h5>
                        @elseif ($ms_kontak_wahana->jenis_kontak == 'LK')
                            <h5>Layanan Konsumen</h5>
                        @else 
                            <h5>Group Sales Operation</h5>
                        @endif
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            Nomor Telepon II:
                        </label>
                        <h5>{{ $ms_kontak_wahana->nomor_telepon2 }}</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Email:
                        </label>
                        <h5>{{ $ms_kontak_wahana->email }}</h5>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            Alamat:
                        </label>
                        <h5>{{ $ms_kontak_wahana->alamat_kontak }}</h5> 
                    </div>
                </div>
            </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop