@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Detail {!! get_menu_name() !!}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{$back}}" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-undo"></i>
                            </a>
                        </li>
                    </ul>
                </div>
        </div>
        <!--begin::Form-->
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Lokasi:
                        </label>
                        <h5>{!! $ms_jaringan_wahana->nama_kota_kabupaten !!}</h5>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            Kategori Jaringan:
                        </label>
                        @if(!empty($kategori_jaringan))
                        <h5>{!! $kategori_jaringan !!}</h5>
                        @endif
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Nama Dealer:
                        </label>
                        <h5>{!! $ms_jaringan_wahana->nama_dealer !!}</h5>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            Nomor Telepon:
                        </label>
                        <h5>{!! $ms_jaringan_wahana->nomor_telepon !!}</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            Alamat Jaringan:
                        </label>
                        <h5>{!! $ms_jaringan_wahana->alamat_jaringan !!}</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            Link Peta:
                        </label>
                        <div>     
                            <a href="{{ $ms_jaringan_wahana->link_peta }}" target="blank">{!! $ms_jaringan_wahana->link_peta !!}</a>
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            Link Peta Embed:
                        </label>
                        <div>
                            {!! $ms_jaringan_wahana->link_embed_peta !!}
                        </div>
                    </div>
                </div> -->
            </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop