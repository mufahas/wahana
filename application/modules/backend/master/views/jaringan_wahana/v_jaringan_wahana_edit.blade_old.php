@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label>
                            *
                            Lokasi:
                        </label>
                        {!! form_dropdown('kode_kota_kabupaten', $select_kota_kabupaten, encryptID($ms_jaringan_wahana->kode_kota_kabupaten), 'class="form-control m-select2" id="m_select2_1"') !!}
                    </div>
                    <div class="col-lg-6 form-group ">
                        <label>
                            *
                            Kategori Jaringan:
                        </label>
                        <!-- {!! form_dropdown('kategori_jaringan', $select_kategori_jaringan, $ms_jaringan_wahana->kategori_jaringan, 'class="form-control m-select2" id="m_select2_2" ') !!} -->
                        <div class="m-checkbox-list">
                            <label class="m-checkbox col-lg-12">
                                <input type="checkbox" name="kategori_jaringan[]" id="kategori_jaringan" value="H1" {{ !empty($H1) ? 'checked="checked"' : '' }}>H1 - Penjualan
                                <span></span>
                            </label>
                            <label class="m-checkbox col-lg-12">
                                <input type="checkbox" name="kategori_jaringan[]" id="kategori_jaringan" value="H2" {{ !empty($H2) ? 'checked="checked"' : '' }}>H2 - Pemeliharaan
                                <span></span>
                            </label>
                            <label class="m-checkbox col-lg-12">
                                <input type="checkbox" name="kategori_jaringan[]" id="kategori_jaringan" value="H3" {{ !empty($H3) ? 'checked="checked"' : '' }}>H3 - Suku Cadang
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label>
                            *
                            Nama Dealer:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'nama_dealer', 'class' => 'form-control m-input', 'placeholder' => 'Nama Dealer', 'value' => $ms_jaringan_wahana->nama_dealer )) !!}
                    </div>
                    <div class="col-lg-6 form-group ">
                        <label>
                            Nomor Telepon:
                        </label>
                        {!! form_input(array('type' => 'number','name' => 'nomor_telepon', 'class' => 'form-control m-input', 'placeholder' => '021XXXXXX', 'value' => $ms_jaringan_wahana->nomor_telepon )) !!}
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Alamat Jaringan:
                        </label>
                        <textarea name="alamat_jaringan" class="form-control m-input" placeholder="Alamat Jaringan">{!! $ms_jaringan_wahana->alamat_jaringan !!}</textarea>
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Link Peta:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'link_peta', 'class' => 'form-control m-input', 'placeholder' => 'https://goo.gl/maps/GcPdA4dv2jw', 'value' => $ms_jaringan_wahana->link_peta )) !!}
                    </div>
                </div>
                <!-- <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Link Embed Peta:
                        </label>
                        <textarea name="link_embed_peta" class="form-control m-input" placeholder="Alamat Jaringan">{!! $ms_jaringan_wahana->link_embed_peta !!}</textarea>
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Preview Peta:
                        </label>
                        <div id="previewPeta"></div>
                    </div>
                </div> -->
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_update"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($ms_jaringan_wahana->kode_jaringan) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/master/jaringan_wahana/js/jaringan-wahana.js" type="text/javascript"></script>
@stop