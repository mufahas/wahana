<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_tipe_mesin extends Eloquent {

	public $table      = 'ms_tipe_mesin';
	public $primaryKey = 'kode_tipe_mesin';
	public $timestamps = false;

}