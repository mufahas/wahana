<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_konten extends Eloquent {

	public $table      = 'ms_konten';
	public $primaryKey = 'kode_konten';
	public $timestamps = false;

}