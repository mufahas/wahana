<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_kategori_motor extends Eloquent {

	public $table      = 'ms_kategori_motor';
	public $primaryKey = 'kode_kategori_motor';
	public $timestamps = false;

}