<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_jaringan_wahana_detail extends Eloquent {

	public $table      = 'tbl_jaringan_wahana_detail';
	public $primaryKey = 'kode_jaringan_detail';
	public $timestamps = false;

}