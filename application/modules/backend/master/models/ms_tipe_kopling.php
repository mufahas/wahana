<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_tipe_kopling extends Eloquent {

	public $table      = 'ms_tipe_kopling';
	public $primaryKey = 'kode_tipe_kopling';
	public $timestamps = false;

}