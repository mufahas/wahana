<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_harga_otr extends Eloquent {

	public $table      = 'ms_harga_otr';
	public $primaryKey = 'kode_harga_otr';
	public $timestamps = false;

}