<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_gso extends Eloquent {

	public $table      = 'ms_gso';
	public $primaryKey = 'kode_gso_wahana';
	public $timestamps = false;

}