<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_kota_kabupaten extends Eloquent {

	public $table      = 'ms_kota_kabupaten';
	public $primaryKey = 'kode_kota_kabupaten';
	public $timestamps = false;

}