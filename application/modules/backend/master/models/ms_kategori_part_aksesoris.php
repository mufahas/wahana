<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_kategori_part_aksesoris extends Eloquent {

	public $table      = 'ms_kategori_part_aksesoris';
	public $primaryKey = 'kode_kategori_part_aksesoris';
	public $timestamps = false;

}