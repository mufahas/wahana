<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_gambar_banner_home extends Eloquent {

	public $table      = 'ms_gambar_banner_home';
	public $primaryKey = 'kode_gambar_banner';
	public $timestamps = false;

}