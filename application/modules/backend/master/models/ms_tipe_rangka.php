<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_tipe_rangka extends Eloquent {

	public $table      = 'ms_tipe_rangka';
	public $primaryKey = 'kode_tipe_rangka';
	public $timestamps = false;

}