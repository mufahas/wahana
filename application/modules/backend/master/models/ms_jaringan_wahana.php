<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_jaringan_wahana extends Eloquent {

	public $table      = 'ms_jaringan_wahana';
	public $primaryKey = 'kode_jaringan';
	public $timestamps = false;

}