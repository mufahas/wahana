<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_provinsi extends Eloquent {

	public $table      = 'ms_provinsi';
	public $primaryKey = 'kode_provinsi';
	public $timestamps = false;

}