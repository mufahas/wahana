<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_label extends Eloquent {

	public $table      = 'ms_label';
	public $primaryKey = 'kode_label';
	public $timestamps = false;

}