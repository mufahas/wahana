<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_kalkulator_kredit extends Eloquent {

	public $table      = 'ms_kalkulator_kredit';
	public $primaryKey = 'kode_kalkulator_kredit';
	public $timestamps = false;

}