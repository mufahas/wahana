<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_kontak_wahana extends Eloquent {

	public $table      = 'ms_kontak_wahana';
	public $primaryKey = 'kode_kontak_wahana';
	public $timestamps = false;

}