<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_kalkulator_cicilan extends Eloquent {

	public $table      = 'ms_kalkulator_cicilan';
	public $primaryKey = 'kode_harga_otr';
	public $timestamps = false;

}