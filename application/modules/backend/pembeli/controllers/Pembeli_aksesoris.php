<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pembeli_aksesoris extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['view']      = site_url() . $this->site . '/view';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "tbl_pembeli_aksesoris";
        $condition    = "";
        $row          = array('id_pembeli_aksesoris','nama_pembeli','no_hp_pembeli','email_pembeli','tgl_beli');
        $row_search   = array('id_pembeli_aksesoris','nama_pembeli','no_hp_pembeli','email_pembeli','tgl_beli');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * View data from table:
    * @param Id
    * @return page index
    **/
    function view($id)
    {
        $id_pembeli_aksesoris = decryptID($id);                 //Parameter

        /* Select Row */
        $select = 'tbl_pembeli_aksesoris.id_pembeli_aksesoris,
                   tbl_pembeli_aksesoris.no_ktp_pembeli,
                   tbl_pembeli_aksesoris.nama_pembeli,
                   tbl_pembeli_aksesoris.email_pembeli,
                   tbl_pembeli_aksesoris.no_hp_pembeli,
                   tbl_pembeli_aksesoris.tgl_beli,
                   tbl_pembeli_aksesoris.domisili,
                   ms_part_aksesoris.nama_part_aksesoris,
                   ms_part_aksesoris.status_ketersediaan_stok,
                   ms_part_aksesoris.harga_part_aksesoris';

        /* Cek Data */
        $pembeli = tbl_pembeli_aksesoris::selectRaw($select)->join('ms_part_aksesoris','ms_part_aksesoris.id_part_aksesoris','=','tbl_pembeli_aksesoris.id_part_aksesoris')->where('id_pembeli_aksesoris',$id_pembeli_aksesoris)->first();

        if(!empty($pembeli))
        {
            $data['result'] = $pembeli;
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);              
        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }
}