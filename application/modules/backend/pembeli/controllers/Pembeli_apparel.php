<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pembeli_apparel extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "tbl_pembeli_apparel";
        $condition    = "";
        $row          = array('id_pembeli_apparel','nama_pembeli','no_hp_pembeli','email_pembeli','tgl_beli');
        $row_search   = array('id_pembeli_apparel','nama_pembeli','no_hp_pembeli','email_pembeli','tgl_beli');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {

    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/update';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        
    }

    /**
    * View data from table:
    * @param Id
    * @return page index
    **/
    function view($id)
    {
        $id_pembeli_apparel = decryptID($id);                 //Parameter

        /* Select Row */
        $select = 'tbl_pembeli_apparel.id_pembeli_apparel,
                   tbl_pembeli_apparel.no_ktp_pembeli,
                   tbl_pembeli_apparel.nama_pembeli,
                   tbl_pembeli_apparel.email_pembeli,
                   tbl_pembeli_apparel.no_hp_pembeli,
                   tbl_pembeli_apparel.tgl_beli,
                   tbl_pembeli_apparel.domisili,
                   ms_apparel.nama_apparel,
                   ms_apparel.harga_apparel';

        /* Cek Data */
        $pembeli = tbl_pembeli_apparel::selectRaw($select)->join('ms_apparel','ms_apparel.kode_apparel','=','tbl_pembeli_apparel.kode_apparel')->where('id_pembeli_apparel',$id_pembeli_apparel)->first();

        if(!empty($pembeli))
        {
            $data['result'] = $pembeli;
           
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);              
        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }
}