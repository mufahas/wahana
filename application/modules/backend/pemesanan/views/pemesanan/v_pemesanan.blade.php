@extends('backend.default.views.layout.v_layout')

@section('body')

<link href="{{ base_url() }}assets/default/css/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ base_url() }}assets/default/css/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    {{ get_menu_name() }}
                </h3>
            </div>
        </div>
    </div>

    <div class="m-portlet__body">
        <div class="row">
            <div class="col-lg-6">
                <div class="col-lg-12 form-group date-public">
                    <label class="col-form-label">
                        Filter Tanggal Pemesanan
                    </label>
                    <div class='input-group date'>
                        {!! form_input(array('type' => 'text','name' => 'datePub', 'class' => 'form-control m-input', 'placeholder' => 'Pilih Tanggal Pemesanan','id'=>'m_daterangepicker_1')) !!}
                        <span class="input-group-addon">
                            <a href="{{$default}}">
                            <i class="la la-refresh"></i>
                            </a>
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <!--begin: button  -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-12 order-1 order-xl-2 m--align-right">
                            {!! $btn["btn_add"] !!}
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <!--end: button-->
            </div>  
        </div>
        

        <!--begin: Datatable -->
        <table class="table table-striped table-bordered table-hover" id="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Pelanggan</th>
                    <th>Telepon Pelanggan</th>
                    <th>Email Pelanggan</th>
                    <th>Nama Produk</th>
                    <th>Tanggal Pemesanan</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    var loadTable  = '{{$loadTable}}';
    var view_      = '{!! $btn["btn_view"] !!}';
    var edit_      = '{!! $btn["btn_edit"] !!}';
    var delete_    = '{!! $btn["btn_delete"] !!}';
    var urlFilter  = '{{$index}}';
    var tglMulai   = '{{$tgl_mulai}}';
    var tglAkhir   = '{{$tgl_akhir}}';

    $(document).ready(function() {
        if (tglMulai != "" && tglAkhir != "") {
            $('#m_daterangepicker_1').val(formattedDaterangepickerddMMyyyy(tglMulai +' / '+ tglAkhir));
        }else{
            $('#m_daterangepicker_1').val("");
        }
    });

    $('#m_daterangepicker_1').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        singleDatePicker: false,
        showDropdowns: true,
        locale: {
            format: 'DD MMM YYYY',
            separator: ' / ',
        }
        // todayHighlight: true,
        // autoclose: true,
        // format: 'yyyy-mm-dd',
        // format: 'dd MM yyyy'
    });

    $('#m_daterangepicker_1').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD MMM YYYY') + ' / ' + picker.endDate.format('DD MMM YYYY'));
        var datePub      = $('#m_daterangepicker_1').val();
        var aa           = formattedDaterangepickerYmd(datePub);
        window.location.href = urlFilter+'/'+ aa;
    });

    $('#m_daterangepicker_1').on('cancel.daterangepicker', function(ev, picker) {
        if (tglMulai == "" && tglAkhir == "") {
            $(this).val('');
        }else{
            window.location.reload()
        }
    });


    // $('#m_datetimepicker_1').datepicker({
    //     todayHighlight: true,
    //     autoclose: true,
    //     // format: 'yyyy-mm-dd',
    //     format: 'dd MM yyyy'
    // });

    // $('input[name="datePub"]').change( function(){
    //     var datePub      = $('#m_datetimepicker_1').val();
    //     window.location.href = urlFilter+'/'+formattedDateyyyymmddDash(datePub);
    // });
</script>

<script src="{{ base_url() }}assets/default/js/datatables.min.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/default/js/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/backend/js/handler-datatable.js" type="text/javascript"></script>

<script src="{{ base_url() }}assets/backend/js/pemesanan/table/table-pemesanan.js" type="text/javascript"></script>
@stop