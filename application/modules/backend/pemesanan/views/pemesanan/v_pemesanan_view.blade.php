@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Detail {!! get_menu_name() !!}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{base_url()}}pemesanan" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-undo"></i>
                            </a>
                        </li>
                    </ul>
                </div>
        </div>
        <!--begin::Form-->
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-4 form-group">
                        <label>
                            Nama Pelanggan:
                        </label>
                        <h5>{!! $tbl_pemesanan->nama_pelanggan !!}</h5>
                    </div>
                    <div class="col-lg-4 form-group">
                        <!--<label>-->
                        <!--    Kartu Identitas:-->
                        <!--</label>-->
                        <!--<img src="{!! base_url() !!}assets/upload/pelanggan/{!! $tbl_pemesanan->kartu_identitas !!}" height="200px" width="400px">-->
                        <label>
                            Email Pelanggan:
                        </label>
                        <h5>{!! $tbl_pemesanan->email_pelanggan !!}</h5>
                    </div>
                     <div class="col-lg-4 form-group">
                        <label>
                            Telepon Pelanggan:
                        </label>
                        <h5>{!! $tbl_pemesanan->nomor_telepon_pelanggan !!}</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Kategori Produk:
                        </label>
                        <h5>{!! $tbl_pemesanan->nama_kategori_motor !!}</h5>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            Nama Produk:
                        </label>
                        <h5>{!! $tbl_pemesanan->nama_produk_otr !!}</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Uang Muka:
                        </label>
                        <h5>Rp {!! number_format( $tbl_pemesanan->uang_muka , 2 , ',' , '.' ) !!}</h5>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            Jangka Pinjaman:
                        </label>
                        <h5>{!! $tbl_pemesanan->jangka_pinjaman !!} Bulan</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Estimasi Cicilan Perbulan:
                        </label>
                        <h5>Rp {!! number_format( $tbl_pemesanan->estimasi_cicilan_perbulan , 2 , ',' , '.' ) !!}</h5>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            Tanggal Pemesanan:
                        </label>
                        <h5>{!! date('d F Y', strtotime($tbl_pemesanan->tanggal_pemesanan)) !!}</h5>
                    </div>
                </div>
            </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop