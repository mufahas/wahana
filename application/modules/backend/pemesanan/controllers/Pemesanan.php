<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index($tgl_mulai = "", $tgl_akhir = "") 
    {
        $data['tgl_mulai'] = "";
        $data['tgl_akhir'] = "";
        if (!empty($tgl_mulai) AND !empty($tgl_akhir)) {
            $data['tgl_mulai'] = $tgl_mulai;
            $data['tgl_akhir'] = $tgl_akhir;
            $data['loadTable'] = site_url() . $this->site . '/loadTable/'.$tgl_mulai.'/'.$tgl_akhir;
        }else{
            $data['loadTable'] = site_url() . $this->site . '/loadTable';
        }
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';
        $data['index']     = site_url() . $this->site . '/index';
        $data['default']   = site_url() . $this->site;

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable($tgl_mulai = "", $tgl_akhir = "")
    {   
        $where        = "";
        if (!empty($tgl_mulai) AND !empty($tgl_akhir)) {
            $where .= " tbl_pemesanan.tanggal_pemesanan >= '".$tgl_mulai." 00:00:00' AND tbl_pemesanan.tanggal_pemesanan <= '".$tgl_akhir." 23:59:59'";
        }

        $model        = "tbl_pemesanan";
        $condition    = $where;
        $row          = array('tbl_pemesanan.id_pemesanan', 'tbl_pelanggan.nama_pelanggan', 'tbl_pelanggan.nomor_telepon_pelanggan', 'tbl_pelanggan.email_pelanggan', 'ms_harga_otr.nama_produk_otr', 'tbl_pemesanan.tanggal_pemesanan');
        $row_search   = array('tbl_pemesanan.id_pemesanan', 'tbl_pelanggan.nama_pelanggan', 'tbl_pelanggan.nomor_telepon_pelanggan', 'tbl_pelanggan.email_pelanggan', 'ms_harga_otr.nama_produk_otr', 'tbl_pemesanan.tanggal_pemesanan');
        $join         = array('tbl_pelanggan' => 'tbl_pelanggan.kode_pelanggan = tbl_pemesanan.kode_pelanggan', 
                               'ms_harga_otr' => 'ms_harga_otr.kode_harga_otr = tbl_pemesanan.kode_harga_otr');
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * View data from table:
    * @param Id
    * @return page index
    **/
    function view($id)
    {   
        $kode_pemesanan = decryptID($id);
        $tbl_pemesanan  = tbl_pemesanan::leftJoin('tbl_pelanggan','tbl_pelanggan.kode_pelanggan','=','tbl_pemesanan.kode_pelanggan')
                                        ->leftJoin('ms_harga_otr','ms_harga_otr.kode_harga_otr','=','tbl_pemesanan.kode_harga_otr')
                                        ->leftJoin('ms_produk','ms_produk.kode_produk','=','ms_harga_otr.kode_produk')
                                        ->leftJoin('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')
                                        ->where('tbl_pemesanan.id_pemesanan',$kode_pemesanan)
                                        ->first();

        if(!empty($tbl_pemesanan))
        {
            $data['tbl_pemesanan']    = $tbl_pemesanan;
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
        
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {

    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/update';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        
    }
}