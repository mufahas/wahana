<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_pelanggan extends Eloquent {

	public $table      = 'tbl_pelanggan';
	public $primaryKey = 'kode_pelanggan';
	public $timestamps = false;

}