<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_pemesanan extends Eloquent {

	public $table      = 'tbl_pemesanan';
	public $primaryKey = 'id_pemesanan';
	public $timestamps = false;

}