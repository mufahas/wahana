@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Detail {!! get_menu_name() !!}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ $url }}" class="m-portlet__nav-link m-portlet__nav-link--icon">
                            <i class="la la-undo"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!--begin::Form-->
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Judul Promo:
                        </label>
                        <h5>{!! $tbl_promo->judul_promo !!}</h5>
                        
                        <br>
                        
                        <label>
                            Status Promo:
                        </label>
                        @if ($tbl_promo->status_promo == 'A')
                            <h5>Aktif</h5>
                        @else 
                            <h5>Tidak Aktif</h5>
                        @endif
                        
                        <br>
                        
                        <label>
                            Tanggal Publikasi:
                        </label>
                        <h5>{!! $tbl_promo->tanggal_publikasi !!}</h5>

                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            Banner Gambar:
                        </label>
                        <img src="{!! base_url() !!}assets/upload/promo/{!! $tbl_promo->banner_gambar !!}" height="200px" width="400px">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Tanggal Promo:
                        </label>
                        <h5>{!! $tbl_promo->tgl_promo !!}</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            Deskripsi:
                        </label>
                        <h5>{!! $tbl_promo->deskripsi !!}</h5>
                    </div>
                </div>
            </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop