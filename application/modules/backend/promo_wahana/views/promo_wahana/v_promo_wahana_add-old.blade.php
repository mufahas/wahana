@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Judul:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'judul_promo', 'class' => 'form-control m-input', 'onkeyup' => 'check()', 'placeholder' => 'Judul Promo' )) !!}
                        <p class="ValidasiJudul" style="display: none;color: red;">Judul promo sudah digunakan</p>
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Tanggal Promo:
                        </label>
                        <div class="input-group" id="tanggal_promo">
                            <span class="input-group-addon">
                                <i class="la la-calendar"></i>
                            </span>
                            {!! form_input(array('type' => 'text','name' => 'tanggal_promo', 'class' => 'form-control m-input', 'placeholder' => 'Tanggal Promo')) !!}
                        </div>
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Banner Promo:
                        </label>
                        <div class="m-dropzone dropzone m-dropzone--primary dropzone-previews" id="my-awesome-dropzone">
                            <div class="m-dropzone__msg dz-message needsclick">
                                <h3 class="m-dropzone__msg-title">
                                    Jatuhkan berkas di sini atau klik untuk diunggah.
                                </h3>
                                <span class="m-dropzone__msg-desc">
                                    Upload hanya 1 berkas, Ukuran gambar 1900 x 750 px, Berkas maksimal 5 MB, Jenis gambar png, jpg, jpeg
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Deskripsi Promo:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'deskripsi', 'class' => 'form-control m-input summernote', 'placeholder' => 'Deskripsi Promo' )) !!}
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label class="">
                            *
                            Publish:
                        </label>
                        {!! form_dropdown('status_publikasi', $publikasi, 'T', 'class="form-control m-select2" id="m_select2_1"') !!}
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_save"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var checkJudul = '{{$checkJudul}}';
</script>
<script type="text/javascript">
    var url              = "{{$action}}";
    var url_succees      = "{{$url_succees}}";
    var uploadMultiple   = false; 
    var autoProcessQueue = false;
    var maxFilesize      = 5;
    var paramName        = "file";
    var addRemoveLinks   = true;
    var maxFiles         = 1;
    var parallelUploads  = 1;
    var acceptedFiles    = 'image/jpeg,image/png';

    $('.save').attr("disabled",true);
</script>
<script src="{{ base_url() }}assets/backend/js/promo/js/promo.js" type="text/javascript"></script>
@stop