@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label>
                            *
                            Judul:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'judul_promo', 'class' => 'form-control m-input', 'onkeyup' => 'check()', 'placeholder' => 'Judul Promo', 'value' => $tbl_promo->judul_promo )) !!}
                        <p class="ValidasiJudul" style="display: none;color: red;">Judul promo sudah digunakan</p>
                    </div>
                    <!-- <div class="col-lg-6 form-group ">
                        <label>
                            Kode Promo:
                        </label>
                        <div class="input-group">
                            {!! form_input(array('type' => 'text','name' => 'promo_code', 'class' => 'form-control m-input', 'placeholder' => 'Contoh: PROMOVARIO', 'value' => $tbl_promo->promo_code, 'maxlength' => '12')) !!}
                            <span class="input-group-btn">
                                <button class="btn btn-primary check_promo_code" type="button" data-toggle="m-tooltip" title data-original-title="Check Kode Promo" >
                                    <i class="la la-search"></i>
                                </button>
                            </span>
                        </div>
                    </div> -->
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label>
                            *
                            Tanggal Promo:
                        </label>
                        <div class="input-group" id="tanggal_promo">
                            <span class="input-group-addon">
                                <i class="la la-calendar"></i>
                            </span>
                            <input type="text" class="form-control m-input" name="tanggal_promo" value="{{ $tbl_promo->tgl_promo }}" placeholder="Tanggal Promo">
                        </div>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Short Description:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'short_desc', 'class' => 'form-control m-input', 'placeholder' => 'Short Desc', 'minlength' => '120', 'maxlength' => '320', 'value' => $tbl_promo->short_desc)) !!}
                    </div>
                </div>
                <div class="m-form__group banner-edit row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Banner Promo:
                        </label>
                        <div class="input-group date">
                            <img src="{{site_url()}}assets/upload/promo/{{$tbl_promo->banner_gambar}}" width="420" height="200">
                            <span class="input-group-addon">
                                <button type="button" class="remove m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Hapus"><i class="la la-trash"></i> </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="m-form__group dropzone-edit row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Banner Promo:
                        </label>
                        <div class="m-dropzone dropzone m-dropzone--primary dropzone-previews" id="my-awesome-dropzone">
                            <div class="m-dropzone__msg dz-message needsclick">
                                <h3 class="m-dropzone__msg-title">
                                    Jatuhkan berkas di sini atau klik untuk diunggah.
                                </h3>
                                <span class="m-dropzone__msg-desc">
                                    Upload hanya 1 berkas, Ukuran gambar 1900 x 750 px, Berkas maksimal 5 MB, Jenis gambar png, jpg, jpeg
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Deskripsi Promo:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'deskripsi', 'class' => 'form-control m-input summernote', 'placeholder' => 'Deskripsi Promo', 'value' => $tbl_promo->deskripsi )) !!}
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label class="col-form-label">
                            *Status:
                        </label>
                        <br>
                            <input type="checkbox" name="status_promo" data-switch="true" id="m_switch_1" data-on-text="Aktif" data-off-text="Nonaktif" data-on-color="danger" <?= $tbl_promo->status_promo == 'A' ? 'checked' : '' ?> >
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label class="">
                            *
                            Publish:
                        </label>
                        {!! form_dropdown('status_publikasi', $publikasi, $tbl_promo->status_publikasi, 'class="form-control m-select2" id="m_select2_1"') !!}
                    </div>
                </div>
            </div>

            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_update"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'value_banner', 'value' => encryptID($tbl_promo->banner_gambar) )) !!}
            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($tbl_promo->kode_promo) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var checkJudul     = '{{$checkJudul}}';
</script>
<script type="text/javascript">
    var url              = "{{$action}}";
    var url_succees      = "{{$url_succees}}";
    var uploadMultiple   = false; 
    var autoProcessQueue = false;
    var maxFilesize      = 5;
    var paramName        = "file";
    var addRemoveLinks   = true;
    var maxFiles         = 1;
    var parallelUploads  = 1;
    var acceptedFiles    = 'image/jpeg,image/png';
</script>
<script src="{{ base_url() }}assets/backend/js/promo/js/promo.js" type="text/javascript"></script>
@stop