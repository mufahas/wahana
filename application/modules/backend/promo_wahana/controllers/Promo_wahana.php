<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_wahana extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        //Model
        $this->load->model('select_global_model');
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "tbl_promo";
        $condition    = "";
        $row          = array('tbl_promo.kode_promo', 'tbl_promo.judul_promo', 'tbl_promo.tgl_promo', 'tbl_promo.banner_gambar', 'tbl_promo.status_promo', 'tbl_promo.tanggal_publikasi');
        $row_search   = array('tbl_promo.kode_promo', 'tbl_promo.judul_promo', 'tbl_promo.tgl_promo', 'tbl_promo.banner_gambar', 'tbl_promo.status_promo', 'tbl_promo.tanggal_publikasi');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * View data from table:
    * @param Id
    * @return page index
    **/
    function view($id)
    {
        $kode_promo = decryptID($id);
        $tbl_promo  = tbl_promo::where('kode_promo',$kode_promo)->first();

        if(!empty($tbl_promo))
        {
            $data['tbl_promo']    = $tbl_promo;
            $data['url']          = site_url() . $this->site;
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
        
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['url_succees']    = site_url() . $this->site;
        $data['publikasi']      = $this->select_global_model->selectPublish();
        $data['action']         = site_url() . $this->site . '/save';
        $data['checkJudul']     = site_url() . $this->site . '/ajax_check_class';
        // $data['checkPromoCode'] = site_url() . $this->site . '/ajax_promo_code';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {  
         /* Url */
        $url_succees        = site_url() . $this->site;
        $url_error          = site_url() . $this->site . '/add';

        /* Get Data Post */
        $judul_promo        = ucwords($this->input->post('judul_promo'));
        $tanggal_promo      = ucwords($this->input->post('tanggal_promo'));
        $deskripsi          = ucwords($this->input->post('deskripsi'));
        // $promo_code         = strtoupper($this->input->post('promo_code'));
        $short_desc         = $this->input->post('short_desc');
        $status_publikasi   = $this->input->post('status_publikasi');

        // $tgl_promo         = explode(' - ', $tanggal_promo);
        // $mulai_promo       = $tgl_promo[0];   
        // $akhir_promo       = $tgl_promo[1]; 

        $filename           = $_FILES["file"]["name"];
        $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
        $file_ext           = substr($filename, strripos($filename, '.')); // get file name
        $filesize           = $_FILES["file"]["size"];
        $allowed_file_types = array('.jpg','.jpeg','.png');

        // $check_promo        = tbl_promo::where('judul_promo',$judul_promo)->orWhere('promo_code',$promo_code)->first();
        $check_promo        = tbl_promo::where('judul_promo',$judul_promo)->first();
        
        if(empty($check_promo))
        {
            if (in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 2000000)
            {
                // Rename file
                $newfilename = 'PROMO_'. time() . '_'. md5($filename) . $file_ext;
                if (file_exists("assets/upload/promo/" . $newfilename))
                {
                    // file already exists error
                    // echo "You have already uploaded this file.";
                    $status = array('status' => 'error', 'message' => lang('message_save_failed') . ' ' . $newfilename);
                }
                else
                {       
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], "assets/upload/promo/" . $newfilename)){
                        
                        $model = new tbl_promo;

                        $model->judul_promo       = $judul_promo;
                        $model->judul_promo_url   = clean_url(strtolower($judul_promo));
                        $model->deskripsi         = $deskripsi;
                        $model->banner_gambar     = $newfilename;
                        $model->tgl_promo         = $tanggal_promo;
                        $model->status_promo      = "A";
                        $model->status_publikasi  = $status_publikasi;
                        $model->tanggal_publikasi = $status_publikasi == 'T' ? date('Y-m-d H:i:s') : null;
                        // $model->promo_code        = $promo_code;
                        $model->short_desc        = $short_desc;

                        $save = $model->save();

                        if($save){

                            /* Write Log */
                            $data_notif = array(
                                                "Kode Promo"        => tbl_promo::max('kode_promo'),
                                                "Judul Promo"       => $judul_promo,
                                                "Tanggal Promo"     => $tanggal_promo,
                                                "Deskripsi"         => $deskripsi,
                                                "Url Banner"        => site_url() . 'assets/upload/promo/' . $newfilename,
                                                );

                            $message = "Berhasil menambahkan promo " . $judul_promo;
                            $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                            /* End Write Log */

                            //echo "File uploaded successfully.";  
                            $status = array('status' => 'success','message' => lang('message_save_success') . ' ' . $filename);   
                        }
                    }
                }        
            }
            elseif (empty($file_basename))
            {   
                // file selection error
                // echo "Please select a file to upload.";
                $status = array('status' => 'error', 'message' => lang('message_save_failed'));
            } 
            elseif ($filesize >= 5000000)
            {   
                // file size error
                // echo "The file you are trying to upload is too large.";
                $status = array('status' => 'error', 'message' => lang('message_save_failed'));
            }
            else
            {
                // file type error
                //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
                $status = array('status' => 'error', 'message' => lang('message_save_failed'));
                unlink($_FILES["file"]["tmp_name"]);
            }
        }
        else
        {
            $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $kode_promo = decryptID($id);
        $tbl_promo   = tbl_promo::where('kode_promo',$kode_promo)->first();

        if(!empty($tbl_promo))
        {
            /* Button Action */
            $data['url_succees']    = site_url() . $this->site;
            $data['publikasi']      = $this->select_global_model->selectPublish();
            $data['action']         = site_url() . $this->site . '/update';
            $data['checkJudul']     = site_url() . $this->site . '/ajax_check_class';
            // $data['checkPromoCode'] = site_url() . $this->site . '/ajax_promo_code';
            
            $data['tbl_promo']      = $tbl_promo;

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table: tbl_promo
    * @param Post Data
    * @return page index
    **/
    function update()
    {
          
        $id         = $this->input->post("id");
        $kode_promo = decryptID($id);

        /* Url */
        $url_succees = site_url() . $this->site;
        $url_error   = site_url() . $this->site . '/edit/' . $id;

        /* Get Data Post */
        $judul_promo      = ucwords($this->input->post('judul_promo'));
        $tanggal_promo    = $this->input->post('tanggal_promo');
        $deskripsi        = $this->input->post('deskripsi');
        $banner_gambar    = $this->input->post('banner_gambar');
        $status           = $this->input->post('status_promo');
        $status_promo     = ($status == 'true' || $status == 'on') ? 'A' : 'TA';
        $status_publikasi = $this->input->post('status_publikasi');
        $userfile         = $_FILES;
        // $promo_code       = strtoupper($this->input->post('promo_code'));
        $short_desc       = $this->input->post('short_desc');

        // $tgl_promo        = explode(' - ', $tanggal_promo);
        // $mulai_promo      = $tgl_promo[0];   
        // $akhir_promo      = $tgl_promo[1];  

        $model = tbl_promo::where('kode_promo',$kode_promo)->first();

        /* Array for write log */
        $data_old = array(
                    "Kode Promo"        => $model->kode_promo,
                    "Judul Promo"       => $model->judul_promo,
                    "Tanggal Promo"     => $model->tgl_promo,
                    "Deskripsi"         => $model->deskripsi,
                    "Url Banner"        => site_url() . 'assets/upload/promo/'.$model->banner_gambar,
                    );

        // $check_promo = tbl_promo::where('judul_promo',$judul_promo)->where('promo_code',$promo_code)->whereRaw('kode_promo != "'.$kode_promo.'"')->first();
        $check_promo = tbl_promo::where('judul_promo',$judul_promo)->whereRaw('kode_promo != "'.$kode_promo.'"')->first();

        /* Kondisi Upload File dan Data Tidak Ada */
        if(!empty($_FILES) && empty($check_promo))
        {
            $filename           = $_FILES["file"]["name"];
            $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext           = substr($filename, strripos($filename, '.')); // get file name
            $filesize           = $_FILES["file"]["size"];
            $allowed_file_types = array('.jpg','.jpeg','.png');  

            if (in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 2000000)
            {   
                // Rename file
                $newfilename = 'PROMO_'. time() . '_'. md5($filename) . $file_ext;
                if (file_exists("assets/upload/promo/" . $newfilename))
                {
                    // file already exists error
                    // echo "You have already uploaded this file.";
                    $status = array('status' => 'error', 'message' => lang('message_update_failed') . ' ' . $filename);
                }
                else
                {       
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], "assets/upload/promo/" . $newfilename)){
                        
                        /* Delete file old */
                        if($model->banner_gambar != null){
                            if(file_exists("assets/upload/promo/" . $model->banner_gambar))
                            {
                                unlink("assets/upload/promo/" . $model->banner_gambar);
                            }
                        }

                        /* Initialize Data */
                        $model->judul_promo       = $judul_promo;
                        $model->judul_promo_url   = clean_url(strtolower($judul_promo));
                        $model->tgl_promo         = $tanggal_promo;
                        $model->deskripsi         = $deskripsi;
                        $model->banner_gambar     = $newfilename;
                        $model->status_promo      = $status_promo;
                        $model->status_publikasi  = $status_publikasi;
                        $model->tanggal_publikasi = $status_publikasi == 'T' ? date('Y-m-d H:i:s') : null;
                        // $model->promo_code        = strtoupper($promo_code);
                        $model->short_desc        = $short_desc;
                        
                        $save = $model->save();

                        if($save){

                            $data_new = array(
                                            "Kode Promo"        => $kode_promo,
                                            "Judul Promo"       => $judul_promo,
                                            "Tanggal Promo"     => $tanggal_promo,
                                            "Deskripsi"         => $deskripsi,
                                            "Url Banner"        => site_url() . 'assets/upload/promo/'. $newfilename,
                                            );

                                        /* Write Log */
                            
                            $data_change = array_diff_assoc($data_new, $data_old);
                            $message     = 'Memperbarui promo ' . $judul_promo;
                            $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                            /* End Write Log*/

                            $status = array('status' => 'success','message' => lang('message_update_success') . ' ' . $filename);
                        }
                    }
                }
            }
            elseif (empty($file_basename))
            {   
                // file selection error
                // echo "Please select a file to upload.";
                $status = array('status' => 'error', 'message' => lang('message_update_failed'));
            } 
            elseif ($filesize >= 5000000)
            {   
                // file size error
                // echo "The file you are trying to upload is too large.";
                $status = array('status' => 'error', 'message' => lang('message_update_failed'));
            }
            else
            {
                // file type error
                //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
                $status = array('status' => 'error', 'message' => lang('message_update_failed'));
                unlink($_FILES["file"]["tmp_name"]);
            }
        }
        
        /* Kondisi Tidak Upload File dan Data Tidak Ada */
        if(empty($_FILES) && empty($check_promo))
        {
            /* Initialize Data */
            $model->judul_promo       = $judul_promo;
            $model->judul_promo_url   = clean_url(strtolower($judul_promo));
            $model->tgl_promo         = $tanggal_promo;
            $model->deskripsi         = $deskripsi;
            $model->status_promo      = $status_promo;
            $model->status_publikasi  = $status_publikasi;
            $model->tanggal_publikasi = $status_publikasi == 'T' ? date('Y-m-d H:i:s') : null;
            // $model->promo_code        = strtoupper($promo_code);
            $model->short_desc        = $short_desc;

            $save = $model->save();

            if($save){

                /* Write Log */
                $data_new = array(
                                "Kode Promo"        => $kode_promo,
                                "Judul Promo"       => $model->judul_promo,
                                "Tanggal Promo"     => $model->tgl_promo,
                                "Deskripsi"         => $model->deskripsi,
                                "Url Banner"        => site_url() . 'assets/upload/promo/'. $model->banner_gambar,
                                );

                $data_change = array_diff_assoc($data_new, $data_old);
                $message     = 'Memperbarui promo ' . $judul_promo;
                $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                /* End Write Log*/

                $status = array('status' => 'success','message' => lang('message_update_success'));
            }
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    
    }

    /**
    * Delete data from table: tbl_promo
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request())
        {
            $url                = site_url() . $this->site;
            $id                 = $this->input->get("id");
            $kode_promo         = decryptID($id);
            
            $promo              = tbl_promo::where('tbl_promo.kode_promo', $kode_promo)->first();

            if(!empty($promo))
            {
                $user   = $this->ion_auth->user()->row();
                
                $delete = $promo->delete();

                if($delete)
                {   
                    unlink("assets/upload/promo/" . $promo->banner_gambar);
                    //Delete Success
                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

     /**
    * Validate judul promo
    * @param judul promo
    * @return boolean
    **/
    function ajax_check_class()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $id          = decryptID($this->input->post('id'));
            $judul_promo = $this->input->post('judul_promo');
            $result      = tbl_promo::where('judul_promo',$judul_promo)->first();
            if ($result) 
            {
                if ($id) 
                {
                    if ($id == $result->kode_promo) 
                    {
                        echo 'true';
                    } 
                    else 
                    {
                        echo 'false';
                    }
                } 
                else 
                {
                    echo 'false';
                }
            } 
            else 
            {
                echo 'true';
            }
        }
    }

    /**
    * Validate promo_code
    * @param promo_code
    * @return boolean
    **/
    // function ajax_promo_code()
    // {
    //     if ($this->input->is_ajax_request()) 
    //     { 
    //         $id         = decryptID($this->input->post('id'));
    //         $promo_code = strtoupper($this->input->post('promo_code'));
    //         // $result     = tbl_promo::whereRaw('promo_code LIKE "%'.$promo_code.'%"')->first();
            
    //         if(empty($id) && !empty($promo_code))
    //         {
    //             $result     = tbl_promo::where('promo_code',$promo_code)->first();
    //         }
    //         else if(!empty($id) && !empty($promo_code))
    //         {
    //             $result     = tbl_promo::where('promo_code','=',$promo_code)->whereRaw('kode_promo != "'.$id.'"')->first();
    //         }
    //         else
    //         {
    //             $result = '';
    //         }


    //         if(empty($result))
    //         {
    //             $status = array('status' => 'true', 'message' => 'Kode Promo Tersedia');
    //         }
    //         else
    //         {
    //             $status = array('status' => 'false', 'message' => 'Kode Promo Sudah Digunakan');
    //         }

    //         $data  = $status;
    //         $this->output->set_content_type('application/json')->set_output(json_encode($data));  
    //     }
    // }
}