<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_peserta_promo extends Eloquent {

	public $table      = 'tbl_peserta_promo';
	public $primaryKey = 'id_peserta_promo';
	public $timestamps = false;

}