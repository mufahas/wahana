<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_promo extends Eloquent {

	public $table      = 'tbl_promo';
	public $primaryKey = 'kode_promo';
	public $timestamps = false;

}