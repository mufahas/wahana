@extends('backend.default.views.layout.v_layout')

@section('body')

<link href="{{ base_url() }}assets/default/css/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ base_url() }}assets/default/css/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<link href="{{ base_url() }}assets/backend/css/custom.css" rel="stylesheet" type="text/css" />


<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    {{ get_menu_name() }}
                </h3>
            </div>
        </div>
    </div>

    <div class="m-portlet__body">
        <!--begin: fitur  -->
        <div class="form-group m-form__group row">
            <div class="col-lg-12 ">
                <label>
                    *
                    Tentukan posisi fitur: 
                </label>
                <div align="center">
                    @foreach($fitur_produk as $data)
                    <?php 
                        $data_explode = explode(",", $data->posisi); 
                        $animateX = $data_explode[1] - 13;
                        $animateY = $data_explode[0] - 13;
                    ?>
                    <a href="" class="btn btn-secondary tooltip-test animate-point animated zoomIn infinite" style="border-color: rgba(255, 235, 59, 0.7);background-color: transparent; margin-top: {{$animateX}}px;margin-left: {{$animateY}}px"></a>
                    <a href="" data-toggle="m-tooltip" class="btn btn-secondary tooltip-test point" title="" data-content="" data-original-title="{{$data->nama_fitur}}" style="border-color: #f7f14c;background-color: #45453f; margin-top: {{$data_explode[1]}}px;margin-left: {{$data_explode[0]}}px" onclick="editfitur('{{$data->kode_produk_fitur}}','{{$data_explode[0]}}','{{$data_explode[1]}}','{{$data->nama_fitur}}','{{$data->deskripsi_fitur}}','{{$data->gambar_fitur}}')"></a>
                    @endforeach
                    <a href="" data-toggle="modal" data-target="#m_modal">
                        <img src="{{ base_url() }}assets/upload/produk/fitur/PRODUK_FITUR_1_14-05-2018.png" style="width: 875.33px; height: 492.67px;cursor:url('{{base_url()}}assets/default/media/img/cursor.png'), auto;" onclick="showcoords();">
                    </a>
                </div>
            </div>
        </div>
        <!--end: fitur-->
    </div>
</div>

    <div class="modal fade show" id="m_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Fitur</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
          <div class="modal-body">
              <div class="form-group">
                <label class="form-control-label">Nama Fitur</label>
                <input type="text" class="form-control" name="nama_fitur" id="nama_fitur">
              </div>
              <div class="form-group">
                <label class="form-control-label">Deskripsi Fitur</label>
                <textarea class="form-control" name="deskripsi_fitur" id="deskripsi_fitur"></textarea>
              </div>
              <div class="form-group">
                <label class="form-control-label">Gambar Fitur</label>
                <div class="m-dropzone dropzone m-dropzone--primary dropzone-previews dropzone-edit" id="my-awesome-dropzone">
                    <div class="m-dropzone__msg dz-message needsclick">
                        <h3 class="m-dropzone__msg-title">
                            Jatuhkan berkas di sini atau klik untuk diunggah.
                        </h3>
                        <span class="m-dropzone__msg-desc">
                            Upload hanya 1 berkas. Berkas maksimal 2 MB
                        </span>
                    </div>
                </div>
                <div class="input-group gambar-edit" style="display: none;">
                    <img src="" id="tampilGambarFitur" width="200" height="200">
                    <span class="input-group-addon">
                        <button type="button" class="remove m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Hapus"><i class="la la-trash"></i> </button>
                    </span>
                </div>
              </div>
              <input type="hidden" class="form-control" name="cX" id="cX" readonly>
              <input type="hidden" class="form-control" name="cY" id="cY" readonly>
              <input type="hidden" class="form-control" name="id" id="id" readonly>
          </div>
          <div class="modal-footer">    
            <button type="button" class="btn btn-primary save">Simpan</button>        
            <button type="button" class="btn btn-danger delete" style="display: none;">Hapus</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>
        {!! form_close() !!}
        </div>
      </div>
    </div> 

@stop

@section('scripts')
<script type="text/javascript">
    var delete_          = "{{$delete}}";

    var url              = "{{$action}}";
    var url_succees      = "{{$url}}";
    var uploadMultiple   = false; 
    var autoProcessQueue = false;
    var maxFilesize      = 2;
    var paramName        = "file";
    var addRemoveLinks   = true;
    var maxFiles         = 1;
    var parallelUploads  = 1;
    var acceptedFiles    = 'image/jpeg,image/png';
    $('.save').attr("disabled",true);

    function showcoords() {
        var cX = event.offsetX;
        var cY = event.offsetY;
        $('#cX').val(cX);
        $('#cY').val(cY);
        $('input[name="nama_fitur"]').val('');
        $('textarea[name="deskripsi_fitur"]').val('');
        $('input[name="id"]').val('');
        $('.delete').hide();
        $('.gambar-edit').hide();
        $('.dropzone-edit').show();
    }

    function editfitur($id, $coorX, $coorY, $nama, $deskripsi, $gambar){
        // # berhenti submit
        event.preventDefault();
        var gambar = '{{ base_url() }}assets/upload/fitur/'+$gambar;
        $('input[name="cX"]').val($coorX);
        $('input[name="cY"]').val($coorY);
        $('input[name="nama_fitur"]').val($nama);
        $('textarea[name="deskripsi_fitur"]').val($deskripsi);
        $('input[name="id"]').val($id);
        $("#tampilGambarFitur").attr("src", gambar);
        $('.delete').show();
        $('.gambar-edit').show();
        $('.dropzone-edit').hide();
        $('#m_modal').modal('show');
        $('.save').attr("disabled",false);
    }

    $('.remove').click(function(){
        $('.save').attr("disabled",true);
        $('.gambar-edit').hide();
        $('.dropzone-edit').show();
    });

</script>

<script src="{{ base_url() }}assets/backend/js/produk_rekomendasi/produk/js/ms_produk_fitur.js" type="text/javascript"></script>

@stop