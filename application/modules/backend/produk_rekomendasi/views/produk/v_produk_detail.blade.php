@extends('backend.default.views.layout.v_layout')

@section('body')

<link href="{{ base_url() }}assets/default/css/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ base_url() }}assets/default/css/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<link href="{{ base_url() }}assets/backend/css/custom.css" rel="stylesheet" type="text/css" />


<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    {{ get_menu_name() }}
                </h3>
            </div>
        </div>
    </div>

    <div class="m-portlet__body">
        
        @if(!empty($messages) && $messages == TRUE) 
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show success" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                {{$messages}}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
        @endif

        <!--begin: fitur  -->
        <div class="form-group m-form__group row">
            <div class="col-lg-12 ">

                <!-- section header -->
                <div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_1">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-header"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Header
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Collapse">
                                        <i class="la la-angle-down"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div data-max-height="100%"><div id="mCSB_1" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" tabindex="0"><div id="mCSB_1_container" class="mCSB_container" dir="ltr">

                            <form id="headerForm" action="{{$header_url}}" method="post" enctype="multipart/form-data">
                                <label>Upload gambar header:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        {!! form_input(array('type' => 'file','name' => 'gambar_header', 'class' => 'form-control m-input')) !!}
                                        <h6 class="test-red">*Keterangan: 
                                            <ul>
                                                <li>Maksimal file 5mb</li>
                                                <li>jenis file: png, jpg, jpeg</li>
                                                <li>Ukuran gambar: full width</li>
                                            </ul>
                                        </h6>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>

                            <div class="m-separator m-separator--space m-separator--dashed"></div>
                            
                            @if(!empty($header_produk))
                            <img src="{{base_url()}}assets/upload/produk/header/{{$header_produk->isi_konten}}" width="100%" height="100%">
                            @else
                            Tidak ada gambar
                            @endif

                        </div></div></div>
                    </div>
                </div>

                <!-- section varian warna -->
                <div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_2">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-tint"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Varian Warna
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Collapse">
                                        <i class="la la-angle-down"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div data-max-height="100%"><div id="mCSB_2" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" tabindex="0"><div id="mCSB_2_container" class="mCSB_container" dir="ltr">

                            {!! form_open($varian_url, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form', 'enctype' => 'multipart/form-data' )) !!}
                            <label class="form-control-label">Gambar varian warna</label>
                            <div class="m-dropzone dropzone m-dropzone--primary dropzone-previews dropzone-edit" id="my-awesome-dropzone">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 class="m-dropzone__msg-title">
                                        Jatuhkan berkas di sini atau klik untuk diunggah.
                                    </h3>
                                    <span class="m-dropzone__msg-desc">
                                        Upload hanya 8 berkas, Ukuran gambar 515 x 504 px, Berkas maksimal 5 MB, Jenis gambar png, jpg, jpeg
                                    </span>
                                </div>
                            </div><br>
                            <button type="button" class="btn btn-primary save">Simpan</button>
                            {!! form_close() !!}
                            
                            <div class="m-separator m-separator--space m-separator--dashed"></div>

                            <div class="row">
                                @if($varian_produk != "[]")
                                    @foreach($varian_produk as $vvarian_produk)
                                    <div class="col-sm-3">
                                        <div class="item" align="center">
                                            <img src="{{ base_url() }}assets/upload/produk/varian_warna/{{$vvarian_produk->kode_produk}}/{{$vvarian_produk->isi_konten}}" alt="">
                                            <button type="button" class="btn btn-danger" onclick="deleteVarian('{{$vvarian_produk->kode_konten_produk}}')">Hapus</button>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                            </div>

                        </div></div></div>
                    </div>
                </div>

                <!-- section produk highlight -->
                <div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_3">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-lightbulb-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Produk Highlight
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Collapse">
                                        <i class="la la-angle-down"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div data-max-height="100%"><div id="mCSB_3" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" tabindex="0"><div id="mCSB_3_container" class="mCSB_container" dir="ltr">

                            <form id="highlightForm" action="{{$highlight_url}}" method="post" enctype="multipart/form-data">
                                <label>Upload gambar Highlight:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        {!! form_input(array('type' => 'file','name' => 'gambar_highlight', 'class' => 'form-control m-input')) !!}
                                        <h6 class="test-red">*Keterangan: 
                                            <ul>
                                                <li>Maksimal file 5mb</li>
                                                <li>jenis file: png, jpg, jpeg</li>
                                                <li>Ukuran gambar: full width</li>
                                            </ul>
                                        </h6>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>

                            <div class="m-separator m-separator--space m-separator--dashed"></div>
                            
                            <div align="center">
                                @if(!empty($highlight_produk))
                                <img src="{{base_url()}}assets/upload/produk/highlight/{{$highlight_produk->isi_konten}}" width="100%" height="100%">
                                <button type="button" class="btn btn-danger mt-2" onclick="deleteHighlight('{{$highlight_produk->kode_konten_produk}}')">Hapus</button>
                                @else
                                Tidak ada gambar
                                @endif
                            </div>

                        </div></div></div>
                    </div>
                </div>

                <!-- section fitur unggulan -->
                <div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_4">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-cubes"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Fitur Unggulan
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Collapse">
                                        <i class="la la-angle-down"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div data-max-height="100%"><div id="mCSB_4" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" tabindex="0"><div id="mCSB_4_container" class="mCSB_container" dir="ltr">

                            <form id="backgroundFiturForm" action="{{$back_fitur_url}}" method="post">
                                <label>Upload Background Fitur Unggulan:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        {!! form_input(array('type' => 'file','name' => 'back_fitur', 'class' => 'form-control m-input')) !!}
                                        <h6 class="test-red">*Keterangan: 
                                            <ul>
                                                <li>Maksimal file 5mb</li>
                                                <li>jenis file: png, jpg, jpeg</li>
                                                <li>Ukuran gambar: 1600 x 900 pixel</li>
                                            </ul>
                                        </h6>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                            
                            <div class="m-separator m-separator--space m-separator--dashed"></div>

                            <label>
                                *
                                Tentukan posisi fitur: 
                            </label>
                            <div align="center">
                                @foreach($fitur_produk as $data)
                                <?php 
                                    $data_explode = explode(",", $data->posisi); 
                                    $animateX = $data_explode[1] - 13;
                                    $animateY = $data_explode[0] - 13;
                                ?>
                                <a href="" class="btn btn-secondary tooltip-test animate-point animated zoomIn infinite" style="border-color: rgba(255, 235, 59, 0.7);background-color: transparent; margin-top: {{$animateX}}px;margin-left: {{$animateY}}px"></a>
                                <a href="" data-toggle="m-tooltip" class="btn btn-secondary tooltip-test point" title="" data-content="" data-original-title="{{$data->nama_fitur}}" style="border-color: #f7f14c;background-color: #45453f; margin-top: {{$data_explode[1]}}px;margin-left: {{$data_explode[0]}}px" onclick="editfitur('{{$data->kode_produk_fitur}}','{{$data_explode[0]}}','{{$data_explode[1]}}','{{$data->nama_fitur}}','{{$data->deskripsi_fitur}}','{{$data->gambar_fitur}}')"></a>
                                @endforeach
                                @if(!empty($back_fitur_produk))
                                <a href="" data-toggle="modal" data-target="#m_modal">
                                    <img id="imgBackFitur" src="{{ base_url() }}assets/upload/produk/fitur/{{$back_fitur_produk->isi_konten}}" style="width: 875.33px; height: 492.67px;cursor:url('{{base_url()}}assets/default/media/img/cursor.png'), auto;" onclick="showcoords();">
                                </a>
                                @endif
                            </div>

                        </div></div></div>
                    </div>
                </div>

                <!-- section video -->
                <div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_5">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-play"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Video
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Collapse">
                                        <i class="la la-angle-down"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div data-max-height="100%"><div id="mCSB_5" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" tabindex="0"><div id="mCSB_5_container" class="mCSB_container" dir="ltr">
                            
                            <form id="videoForm" action="{{$video_url}}" method="post">
                                <div class="modal-body">
                                  <div class="form-group">
                                    <label class="form-control-label">URL Video</label>
                                    <input type="text" class="form-control" name="video" id="video" placeholder="https://www.youtube.com/watch?v=vjrkQDP6bQU" required>
                                  </div>
                                  <button type="submit" class="btn btn-primary">Simpan</button>        
                                </div>
                            </form>

                            <div class="m-separator m-separator--space m-separator--dashed"></div>

                            <div align="center">
                                @if(!empty($video_produk))
                                <iframe width="100%" height="500" src="{!!str_replace('/watch?v=', '/embed/', $video_produk->isi_konten)!!}" frameborder="0" allowfullscreen></iframe>
                                <button type="button" class="btn btn-danger" onclick="deleteVideo('{{$video_produk->kode_konten_produk}}')">Hapus</button>
                                @endif
                            </div>

                        </div></div></div>
                    </div>
                </div>

                <!-- section performa andalan -->
                <div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_6">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-dashboard"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Performa Andalan
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Collapse">
                                        <i class="la la-angle-down"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div data-max-height="100%"><div id="mCSB_6" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" tabindex="0"><div id="mCSB_6_container" class="mCSB_container" dir="ltr">

                            <form id="performaForm" action="{{$performa_url}}" method="post" enctype="multipart/form-data">
                                <label>Upload gambar performa:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        {!! form_input(array('type' => 'file','name' => 'gambar_performa', 'class' => 'form-control m-input')) !!}
                                        <h6 class="test-red">*Keterangan: 
                                            <ul>
                                                <li>Maksimal file 5mb</li>
                                                <li>jenis file: png, jpg, jpeg</li>
                                                <li>Ukuran gambar: full width</li>
                                            </ul>
                                        </h6>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>

                            <div class="m-separator m-separator--space m-separator--dashed"></div>
                            
                            <div align="center">
                                @if(!empty($performa_produk))
                                <img src="{{base_url()}}assets/upload/produk/performa/{{$performa_produk->isi_konten}}" width="100%" height="100%">
                                <button type="button" class="btn btn-danger mt-2" onclick="deletePerforma('{{$performa_produk->kode_konten_produk}}')">Hapus</button>
                                @else
                                Tidak ada gambar
                                @endif
                            </div>

                        </div></div></div>
                    </div>
                </div>

                <!-- section spesifikasi -->
                <div class="m-portlet m-portlet--head-sm m-portlet--collapse" data-portlet="true" id="m_portlet_tools_7">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-gears"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Spesifikasi
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Collapse">
                                        <i class="la la-angle-down"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="display: none;">
                        <div data-max-height="100%"><div id="mCSB_7" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" tabindex="0"><div id="mCSB_7_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" dir="ltr">
                            
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#m_tabs_1_1" aria-expanded="false">
                                        Mesin
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#m_tabs_1_2" aria-expanded="true">
                                        Kelistrikan
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#m_tabs_1_3" aria-expanded="true">
                                        Dimensi &amp; Berat
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#m_tabs_1_4" aria-expanded="true">
                                        Rangka &amp; Kaki Kaki
                                    </a>
                                </li>
                                 <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#m_tabs_1_5" aria-expanded="true">
                                        Harga OTR
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="m_tabs_1_1" role="tabpanel" aria-expanded="false">
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Tipe Mesin </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:
                                            @if($detail->tipe_mesin != "")
                                            {{ $detail->tipe_mesin }}
                                            @endif  
                                          </p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Tipe Transmisi</p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->tipe_transmisi}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                         <p>Pola Pengoperan Gigi</p>
                                      </div>
                                      <div class="col-sm-7">
                                         <p>:   {{$detail->pola_pengoperan_gigi}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                         <p>Rasio Reduksi Gigi</p>
                                      </div>
                                      <div class="col-sm-7">
                                         <p>:   {{$detail->rasio_reduksi_gigi}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                         <p>Tipe Kopling</p>
                                      </div>
                                      <div class="col-sm-7">
                                         <p>:   
                                            @if($detail->tipe_kopling != "")
                                            {{$detail->tipe_kopling}}
                                            @endif
                                          </p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                         <p>Tipe Starter</p>
                                      </div>
                                      <div class="col-sm-7">
                                         <p>:   {{$detail->tipe_starter}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                         <p>Tipe Busi</p>
                                      </div>
                                      <div class="col-sm-7">
                                         <p>:   {{$detail->tipe_busi}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Diameter x Langkah </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->diameter}} x {{$detail->langkah}} mm</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Volume Langkah </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->volume_langkah}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Sistem Pendingin Mesin </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->sistem_pendingin_mesin}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                         <p>Sistem Suplai Bahan Bakar </p>
                                      </div>
                                      <div class="col-sm-7">
                                         <p>:   {{$detail->sistem_suplai_bb}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Perbandingan Kompresi </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->perbandingan_kompresi}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Daya Maksimum </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->daya_maksimum}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Torsi Maksimum </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->torsi_maksimum}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Torsi Maksimum </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->torsi_maksimum}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Sistem Pelumasan </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->sistem_pelumasan}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Sistem Pengereman </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->sistem_pengereman}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Sistem Idling Stop </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->sistem_idling_stop}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Kapasitas Tangki Bahan Bakar </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->kapasitas_tangki_bb}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Kapasitas Minyak Pelumas </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->kapasitas_minyak_pelumas}}</p>
                                      </div>
                                  </div>
                                </div>
                                <div class="tab-pane" id="m_tabs_1_2" role="tabpanel">
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Tipe Baterai </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->tipe_baterai}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Sistem Pengapaian </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->sistem_pengapian}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Lampu Depan </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->lampu_depan}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Lampu Senja </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->lampu_senja}}</p>
                                      </div>
                                  </div>
                                </div>
                                <div class="tab-pane" id="m_tabs_1_3" role="tabpanel">
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Panjang x Lebar x tinggi </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->panjang}} x {{$detail->lebar}} x {{$detail->tinggi}} mm </p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Jarak Sumbu Roda </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->jarak_sumbu_roda}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Jarak Terendah Ke Tanah </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->jarak_terendah_ke_tanah}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Ketinggian Tempat Duduk </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->ketinggian_tempat_duduk}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Radius Putar </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->radius_putar}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Berat Kosong </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->berat_kosong}}</p>
                                      </div>
                                  </div>
                                </div>
                                <div class="tab-pane" id="m_tabs_1_4" role="tabpanel">
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Tipe Rangka </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  
                                            @if($detail->tipe_rangka != "")
                                            {{$detail->tipe_rangka}}
                                            @endif
                                          </p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Berat Kosong </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->berat_kosong}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Tipe Suspensi Depan </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->tipe_suspensi_depan}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Tipe Suspensi Belakang </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->tipe_suspensi_belakang}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Ukuran Ban Depan </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->ukuran_ban_depan}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Ukuran Ban Belakang </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->ukuran_ban_belakang}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Rem Depan </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->rem_depan}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Rem Belakang </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->rem_belakang}}</p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>Jenis Velg </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  {{$detail->jenis_velg}}</p>
                                      </div>
                                  </div>
                                </div>
                                <div class="tab-pane" id="m_tabs_1_5" role="tabpanel">
                                  @if($harga_otr != "[]")
                                  @foreach($harga_otr as $vharga_otr)
                                  <div class="row">
                                      <div class="col-sm-5">
                                          <p>{{$vharga_otr->nama_produk_otr}} </p>
                                      </div>
                                      <div class="col-sm-7">
                                          <p>:  Rp {{number_format($vharga_otr->harga_produk_otr,'2',',','.')}}</p>
                                      </div>
                                  </div>
                                  @endforeach
                                  @endif
                                </div>
                            </div>

                        </div></div></div>
                    </div>
                </div>


                <!-- section posisi -->
                <div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_8">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-sort-numeric-asc"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Position
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Collapse">
                                        <i class="la la-angle-down"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div data-max-height="100%"><div id="mCSB_1" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" tabindex="0"><div id="mCSB_1_container" class="mCSB_container" dir="ltr">
                            
                            <form id="videoForm" action="{{$posisi_url}}" method="post">
                                <div class="modal-body">
                                    @if(!empty($header_urutan))
                                    <div class="form-group m-form__group row">
                                        <label class="col-2 col-form-label">
                                            Header
                                        </label>
                                        <div class="col-10">
                                            <select name="header_position" class="form-control m-input" required>
                                                <option value=""> -- pilih posisi -- </option>
                                                @for($i=1;$i<=6;$i++)
                                                <option value="{{$i}}" <?php if($header_urutan->urutan_konten == $i){ echo "selected"; }?> >{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    @if(!empty($varian_urutan))
                                    <div class="form-group m-form__group row">
                                        <label class="col-2 col-form-label">
                                            Varian Warna
                                        </label>
                                        <div class="col-10">
                                            <select name="varian_position" class="form-control m-input" required>
                                                <option value=""> -- pilih posisi -- </option>
                                                @for($i=1;$i<=6;$i++)
                                                <option value="{{$i}}" <?php if($varian_urutan->urutan_konten == $i){ echo "selected"; }?> >{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    @if(!empty($highlight_urutan))
                                    <div class="form-group m-form__group row">
                                        <label class="col-2 col-form-label">
                                            Highlight
                                        </label>
                                        <div class="col-10">
                                            <select name="highlight_position" class="form-control m-input" required>
                                                <option value=""> -- pilih posisi -- </option>
                                                @for($i=1;$i<=6;$i++)
                                                <option value="{{$i}}" <?php if($highlight_urutan->urutan_konten == $i){ echo "selected"; }?> >{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    @if(!empty($fitur_urutan))
                                    <div class="form-group m-form__group row">
                                        <label class="col-2 col-form-label">
                                            Fitur Unggulan
                                        </label>
                                        <div class="col-10">
                                            <select name="fitur_position" class="form-control m-input" required>
                                                <option value=""> -- pilih posisi -- </option>
                                                @for($i=1;$i<=6;$i++)
                                                <option value="{{$i}}" <?php if($fitur_urutan->urutan_konten == $i){ echo "selected"; }?> >{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    @if(!empty($video_urutan))
                                    <div class="form-group m-form__group row">
                                        <label class="col-2 col-form-label">
                                            Video
                                        </label>
                                        <div class="col-10">
                                            <select name="video_position" class="form-control m-input" required>
                                                <option value=""> -- pilih posisi -- </option>
                                                @for($i=1;$i<=6;$i++)
                                                <option value="{{$i}}" <?php if($video_urutan->urutan_konten == $i){ echo "selected"; }?> >{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    @if(!empty($performa_urutan))
                                    <div class="form-group m-form__group row">
                                        <label class="col-2 col-form-label">
                                            Performa
                                        </label>
                                        <div class="col-10">
                                            <select name="performa_position" class="form-control m-input" required>
                                                <option value=""> -- pilih posisi -- </option>
                                                @for($i=1;$i<=6;$i++)
                                                <option value="{{$i}}" <?php if($performa_urutan->urutan_konten == $i){ echo "selected"; }?> >{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="form-group m-form__group row">
                                        <label class="col-2 col-form-label">
                                            Spesifikasi
                                        </label>
                                        <div class="col-10">
                                            <select name="spesifikasi_position" class="form-control m-input" disabled="disabled" required>
                                                <option value="7">7</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary pull-right">Simpan</button>        
                                </div>
                            </form>

                        </div></div></div>
                    </div>
                </div>


            </div>
        </div>
        <div align="right">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#m_modal_2">Selesai</button>
        </div>
        <!--end: fitur-->
    </div>
</div>

    <div class="modal fade show" id="m_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Fitur</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          {!! form_open($fitur_url, array('id' => 'fiturForm', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form', 'enctype' => 'multipart/form-data' )) !!}
            <div class="modal-body">
              <div class="form-group">
                <label class="form-control-label">Nama Fitur</label>
                <input type="text" class="form-control" name="nama_fitur" id="nama_fitur" required>
              </div>
              <!-- <div class="form-group">
                <label class="form-control-label">Deskripsi Fitur</label>
                <textarea class="form-control" name="deskripsi_fitur" id="deskripsi_fitur"></textarea>
              </div> -->
              <div class="form-group">
                <label class="form-control-label">Gambar Fitur</label>
                <div class="gambar-add">
                    {!! form_input(array('type' => 'file','name' => 'gambar_fitur', 'class' => 'form-control m-input')) !!}
                    <h6 class="test-red">*Keterangan: 
                        <ul>
                            <li>Maksimal file 5mb</li>
                            <li>jenis file: png, jpg, jpeg</li>
                        </ul>
                    </h6>
                </div>
                <div class="input-group gambar-edit" style="display: none;">
                    <img src="" id="tampilGambarFitur" width="200" height="100%">
                    <span class="input-group-addon">
                        <button type="button" class="remove m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Hapus"><i class="la la-trash"></i> </button>
                    </span>
                </div>
              </div>
              <input type="hidden" class="form-control" name="cX" id="cX" readonly>
              <input type="hidden" class="form-control" name="cY" id="cY" readonly>
              <input type="hidden" class="form-control" name="id" id="id" readonly>
          </div>
          <div class="modal-footer">    
            <button type="submit" class="btn btn-primary save-fitur">Simpan</button>        
            <button type="button" class="btn btn-danger delete-fitur" style="display: none;">Hapus</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>
        {!! form_close() !!}
        </div>
      </div>
    </div> 


    <div class="modal fade show" id="m_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Form Status
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <form action="{{$status_url}}" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">
                                Status Tampil:
                            </label>
                            <select name="status_tampil" class="form-control" required>
                                <option value="T">True</option>
                                <option value="F">False</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            Simpan
                        </button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Tutup
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop

@section('scripts')
<script type="text/javascript">
    var delete_varian    = "{{$delete_varian}}";
    var delete_highlight = "{{$delete_highlight}}";
    var delete_fitur     = "{{$delete_fitur}}";
    var delete_video     = "{{$delete_video}}";
    var delete_performa  = "{{$delete_performa}}";

    var url_finish      =  "{{$url_finish}}";

    var url              = "{{$varian_url}}";
    var url_succees      = "{{$url}}";
    var uploadMultiple   = false; 
    var autoProcessQueue = false;
    var maxFilesize      = 8;
    var paramName        = "userFile";
    var addRemoveLinks   = true;
    var maxFiles         = 8;
    var parallelUploads  = 8;
    var acceptedFiles    = 'image/jpeg,image/png,image/gif';
    $('.save').attr("disabled",true);

    $(document).ready(function (e) {
        $("#headerForm").on('submit',(function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{$header_url}}",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success: function(data){
                    window.location.reload();
                    window.location.href = url_succees+'#m_portlet_tools_1';
                },
                error: function(){
                }           
           });
        }));

        $("#highlightForm").on('submit',(function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{$highlight_url}}",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success: function(data){
                    window.location.reload();
                    window.location.href = url_succees+'#m_portlet_tools_3';
                },
                error: function(){
                }           
           });
        }));

        $("#backgroundFiturForm").on('submit',(function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{$back_fitur_url}}",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success: function(data){
                    window.location.reload();
                    window.location.href = url_succees+'#m_portlet_tools_4';
                },
                error: function(){
                }           
           });
        }));

        $("#videoForm").on('submit',(function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{$video_url}}",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success: function(data){
                    // $("#previewVideo").html(data);
                    window.location.reload();
                    window.location.href = url_succees+'#m_portlet_tools_5';
                },
                error: function(){
                }           
           });
        }));

        $("#performaForm").on('submit',(function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{$performa_url}}",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success: function(data){
                    window.location.reload();
                    window.location.href = url_succees+'#m_portlet_tools_6';
                },
                error: function(){
                }           
           });
        }));
        
    });

</script>

<script src="{{ base_url() }}assets/backend/js/produk_rekomendasi/produk/js/detail.js" type="text/javascript"></script>

@stop