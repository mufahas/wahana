@extends('backend.default.views.layout.v_layout')

<!-- <link rel="stylesheet" href="{{ base_url() }}assets/backend/css/bootzard/style.css"> -->
<!-- <link rel="stylesheet" href="{{ base_url() }}assets/backend/css/bootzard/form-elements.css">  -->

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state f1', 'role' => 'form', 'enctype' => 'multipart/form-data')) !!}

            <h3>Tambah Produk</h3>
            <p>tahap tambah data produk</p>
            <div class="f1-steps" align="center">
                <div class="f1-progress">
                    <div class="f1-progress-line" data-now-value="8.33" data-number-of-steps="6" style="width: 8.33%;"></div>
                </div>
                <div class="f1-step active">
                    <div class="f1-step-icon"><i class="fa fa-motorcycle"></i></div>
                    <p>Produk</p>
                </div>
                <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-wrench"></i></div>
                    <p>Mesin</p>
                </div>
                <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-flash"></i></div>
                    <p>Kelistrikan</p>
                </div>
                <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-balance-scale"></i></div>
                    <p>Dimensi &amp; Berat</p>
                </div>
                <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-gears"></i></div>
                    <p>Rangka &amp; Kaki kaki</p>
                </div>
                <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-check"></i></div>
                    <p>Tahap Akhir</p>
                </div>
            </div>
            
           <!--  Begin Field Set Produk -->
            <fieldset>
                <h4>Data produk:</h4>
                
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Kategori Motor
                        </label>
                        {!! form_dropdown('kategori_motor', $kategori_motor, $produk->kode_kategori_motor, 'class="form-control m-select2" id="m_kategori_motor" style="width: 100%"') !!}
                    </div>

                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Nama Produk:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'nama_produk', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Nama Produk', 'value' => $produk->nama_produk)) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Harga Produk:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'harga_produk', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Harga Produk', 'min' => 0, 'value' => $produk->harga_produk)) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Deskripsi Produk
                        </label>
                        {!! form_textarea('deskripsi_produk', $produk->deskripsi_produk,'id="" class="form-control" style="height:55px" placeholder = "Masukkan Deskripsi Produk"') !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            *
                            Gambar Produk:
                        </label> 
                        <div class="row">
                            <div class="col-lg-6">
                                {!! form_input(array('type' => 'file','name' => 'userfile', 'class' => 'form-control m-input', 'id' => 'input_gambar_produk')) !!}
                                <h6 class="test-red">*Keterangan: 
                                    <ul>
                                        <li>Maksimal file 5mb</li>
                                        <li>jenis file: png, jpg, jpeg</li>
                                        <li>Ukuran gambar: 800 x 600 pixel</li>
                                    </ul>
                                </h6>
                            </div>
                            <div class="col-lg-6">
                                <?php 
                                if(!empty($produk->gambar_produk)) { ?> 
                                    <img src="{{ $path_gambar_produk . $produk->gambar_produk}}" style="" id="gambar_produk">
                                    <br>
                                <?php } else { ?>
                                    <img src="{{base_url()}}assets/default/media/img/users/no-image.jpg" style="" id="gambar_produk">
                                    <br>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            *
                            Produk Varian:
                        </label>
                    </div>
                    <input type="hidden" id="row_otr" value="1"/>
                    <div class="col-lg-12" id="formOtr">
                        <div class="col-lg-12" id="listOtr_1">
                            <?php 
                                $num = '1';
                                foreach($harga_otr_ as $harga_otr) { ?>
                                    <div class="row">
                                        <div class="col-lg-4 form-group">
                                            <input type="text" name="nama_varian_edit[]" class="form-control m-input" placeholder="Nama Varian" value="<?= $harga_otr->nama_produk_otr; ?>">
                                        </div>
                                        
                                        <div class="col-lg-3 form-group">
                                            <input type="text" name="harga_otr_edit[]" class="form-control m-input" placeholder="Harga Otr" value="<?= $harga_otr->harga_produk_otr; ?>">
                                        </div>

                                        <div class="col-lg-3 form-group">
                                            <input type="text" name="lokasi_otr_edit[]" class="form-control m-input" placeholder="Jakarta/Tangerang" value="<?= $harga_otr->lokasi_otr; ?>">
                                        </div>
                                        
                                        <div class="col-lg-2 form-group">
                                            <?php 
                                            if($num >= '2'){
                                            ?>
                                            <a href="" id="{{$harga_otr->kode_harga_otr}}" onclick="deleteOtr('{{$harga_otr->kode_harga_otr}}')" class="btn btn-sm btn-danger"><i class="fa fa-close "></i> &nbsp; Delete</a>
                                            <?php } $num++ ?>
                                        </div>
                                        <input type="hidden" name="kode_harga_otr_edit[]" value="{{ encryptID($harga_otr->kode_harga_otr) }}">
                                    </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <button type="button" class="btn btn-sm btn-success addmore_otr" title="Add More"><i class="fa fa-plus "></i> &nbsp; Add More</button>
                    </div>
                </div>

                <div class="f1-buttons">
                    <button type="button" class="btn btn-next btn-primary">Next</button>
                </div>

            </fieldset>
            <!--  End Field Set Produk -->

            <!-- Begin Field Set Mesin -->
            <fieldset>
                <h4>Data Mesin:</h4>

                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            *
                            Tipe Mesin
                        </label>
                        {!! form_textarea('tipe_mesin', $produk->tipe_mesin,'id="" class="form-control m-input" style="height:55px" placeholder="Masukkan Tipe Mesin"') !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-4 form-group">
                        <label>
                            *
                            Tipe Transmisi
                        </label>
                         <!-- {!! form_dropdown('tipe_transmisi', $tipe_transmisi, $produk->tipe_transmisi, 'class="form-control m-select2" id="m_tipe_transmisi" style="width: 100%"') !!} -->
                         {!! form_input(array('type' => 'text','name' => 'tipe_transmisi', 'class' => 'form-control m-input', 'value' => $produk->tipe_transmisi, 'placeholder' => 'Masukkan Tipe Transmisi')) !!}
                    </div>
                    <div class="col-lg-4 form-group" id="oper_gigi">
                        <label>
                            Pola Pengoperan Gigi
                        </label>       
                        <!-- {!! form_dropdown('pengoperan_gigi', $pengoperan_gigi, $produk->pola_pengoperan_gigi, 'class="form-control m-select2" id="m_pengoperan_gigi" style="width:100%;"') !!} -->
                        {!! form_input(array('type' => 'text','name' => 'pengoperan_gigi', 'class' => 'form-control m-input notvalidate', 'value' => $produk->pola_pengoperan_gigi, 'placeholder' => 'Masukkan Pola Pengoperan Gigi')) !!}
                        <h6 class="test-red">
                          <p>Perhatian ! Field Ini Tidak Perlu Diisi Jika Tipe Transmisi Matik</p>
                        </h6>
                    </div>
                    <div class="col-lg-4 form-group" id="reduksi_gigi">
                        <label>
                            Rasio Reduksi Gigi
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'rasio_reduksi_gigi', 'class' => 'form-control m-input notvalidate', 'placeholder' => 'Masukkan Rasio Reduksi Gigi', 'value' => $produk->rasio_reduksi_gigi)) !!}
                        <h6 class="test-red">
                          <p>Perhatian ! Field Ini Tidak Perlu Diisi Jika Tipe Transmisi Matik</p>
                        </h6>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            *
                            Tipe Kopling
                        </label>
                        {!! form_textarea('tipe_kopling',$produk->tipe_kopling,'id="" class="form-control m-input" style="height:55px" placeholder="Masukkan Tipe Kopling"') !!}
                    </div>
                </div>  

                <div class="form-group m-form__group row">
                    <div class="col-lg-4 form-group ">
                         <label>
                            *
                            Tipe Starter
                        </label>
                        {!! form_dropdown('tipe_starter', $tipe_starter, $produk->tipe_starter, 'class="form-control m-select2" id="m_tipe_starter" style="width:100%;"') !!}
                    </div>
                    
                    <div class="col-lg-4 form-group ">
                        <label>
                            *
                            Tipe Busi
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'tipe_busi', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Tipe Busi', 'value' => $produk->tipe_busi)) !!}
                    </div>

                    <div class="col-lg-4 form-group">
                        <label>
                            *
                            Diameter
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'diameter', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Diameter', 'value' => $produk->diameter,'min' => 0)) !!}
                        	<span class="m-input-icon__icon m-input-icon__icon--right">
                        	    <p style="padding-top: 2px; font-weight: bold">mm</p>
                        	</span>
                        </div>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-4 form-group">
                        <label>
                            *
                            Langkah
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'langkah', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Langkah', 'value' => $produk->langkah, 'min' => 0)) !!}
                        	<span class="m-input-icon__icon m-input-icon__icon--right">
                        	    <p style="padding-top: 2px; font-weight: bold">mm</p>
                        	</span>
                        </div>
                    </div>
                    <div class="col-lg-4 form-group ">
                        <label>
                            *
                            Volume Langkah
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                            {!! form_input(array('type' => 'text','name' => 'volume_langkah', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Volume Langkah', 'value' => $produk->volume_langkah)) !!}
                        	<span class="m-input-icon__icon m-input-icon__icon--right">
                        	    <p style="padding-top: 2px; font-weight: bold">CC</p>
                        	</span>
                        </div>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>
                            *
                            Sistem Pendingin Mesin
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'sistem_pendingin_mesin', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Sistem Pendingin Mesin', 'value' => $produk->sistem_pendingin_mesin)) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-4 form-group ">
                        <label>
                            *
                            Sistem Bahan Bakar
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'sistem_bahan_bakar', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Sistem Bahan Bakar', 'value' => $produk->sistem_suplai_bb)) !!}
                    </div>
                    <div class="col-lg-4 form-group ">
                        <label>
                            Perbandingan Kompresi
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'perbandingan_kompresi', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Perbandingan Kompresi', 'value' => $produk->perbandingan_kompresi)) !!}
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>
                            *
                            Daya Maksimum
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'daya_maksimum', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Daya Maksimum', 'value' => $produk->daya_maksimum)) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-4 form-group ">
                        <label>
                            *
                            Torsi Maksimum
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'torsi_maksimum', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Torsi Maksimum', 'value' => $produk->torsi_maksimum)) !!}
                        </div>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>
                            *
                            Sistem Pelumasan
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'sistem_pelumasan', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Sistem Pelumasan', 'value' => $produk->sistem_pelumasan)) !!}
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>
                            *
                            Sistem Pengereman
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'sistem_pengereman', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Sistem Pengereman', 'value' => $produk->sistem_pengereman)) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-4 form-group ">
                        <label>
                            *
                            Sistem Idling Stop
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'sistem_idling_stop', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Sistem Idling Stop', 'value' => $produk->sistem_idling_stop)) !!}
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>
                            *
                            Kapasitas Tangki Bahan Bakar 
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'kapasitas_tangki_bb', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Kapasitas Tangki Bahan Bakar', 'value' => $produk->kapasitas_tangki_bb, 'min' => 0)) !!}
                        	<span class="m-input-icon__icon m-input-icon__icon--right">
                        	    <p style="padding-top: 2px; font-weight: bold">L</p>
                        	</span>
                        </div>
                    </div>
                    <div class="col-lg-4 form-group ">
                        <label>
                            *
                            Kapasitas Minyak Pelumas
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'kapasitas_minyak_pelumas', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Kapasitas Minyak Pelumas', 'value' => $produk->kapasitas_minyak_pelumas)) !!}
                        	<span class="m-input-icon__icon m-input-icon__icon--right">
                        	    <p style="padding-top: 2px; font-weight: bold">L</p>
                        	</span>
                        </div>
                    </div>
                </div>

                <div class="f1-buttons">
                    <button type="button" class="btn btn-previous">Previous</button>
                    <button type="button" class="btn btn-next btn-primary">Next</button>
                </div>
            </fieldset>
            <!-- End Field Set Mesin -->
            
            <!-- Begin Field Set Kelistrikan -->
            <fieldset>
                <h4>Data Kelistrikan:</h4><br>
                
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Tipe Baterai
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'tipe_baterai', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Tipe Baterai', 'value' => $produk->tipe_baterai)) !!}
                    </div>

                    <div class="col-lg-6 form-group ">
                        <label>
                            *
                            Sistem Pengapian
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'sistem_pengapian', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Sistem Pengapian', 'value' => $produk->sistem_pengapian)) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Lampu Depan
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'lampu_depan', 'class' => 'form-control m-input', 'placeholder' => 'Masukan Lampu Depan', 'value' => $produk->lampu_depan)) !!}
                    </div>

                    <div class="col-lg-6 form-group ">
                        <label>
                            *
                            Lampu Senja
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'lampu_senja', 'class' => 'form-control m-input', 'placeholder' => 'Masukan Lampu Senja', 'value' => $produk->lampu_senja)) !!}
                    </div>
                </div>
                
                <div class="f1-buttons">
                    <button type="button" class="btn btn-previous">Previous</button>
                    <button type="button" class="btn btn-next btn-primary">Next</button>
                </div>
            </fieldset>
            <!-- End Field Set Kelistrikan -->

            <!-- Begin Field Set Dimensi dan Berat -->
            <fieldset>
                <h4>Data Dimensi &amp; Berat:</h4><br>

                <div class="form-group m-form__group row">
                    <div class="col-lg-4 form-group">
                        <label>
                            *
                            Panjang
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'panjang', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Panjang', 'value' => $produk->panjang, 'min' => 0)) !!}
                        	<span class="m-input-icon__icon m-input-icon__icon--right">
                        	    <p style="padding-top: 2px; font-weight: bold">mm</p>
                        	</span>
                        </div>
                    </div>

                    <div class="col-lg-4 form-group ">
                        <label>
                            *
                            Lebar
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'lebar', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Lebar', 'value' => $produk->lebar, 'min' => 0)) !!}
                    </div>

                    <div class="col-lg-4 form-group ">
                        <label>
                            *
                            Tinggi
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'tinggi', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Tinggi', 'value' => $produk->tinggi, 'min' => 0)) !!}
                        	<span class="m-input-icon__icon m-input-icon__icon--right">
                        	    <p style="padding-top: 2px; font-weight: bold">mm</p>
                        	</span>
                        </div>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-4 form-group">
                        <label>
                            *
                            Jarak Sumbu Roda
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'jarak_sumbu_roda', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Jarak Sumbu Roda', 'value' => $produk->jarak_sumbu_roda, 'min' => 0)) !!}
                        	<span class="m-input-icon__icon m-input-icon__icon--right">
                        	    <p style="padding-top: 2px; font-weight: bold">mm</p>
                        	</span>
                        </div>
                    </div>

                    <div class="col-lg-4 form-group ">
                        <label>
                            *
                            Jarak Terendah Ke Tanah
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'jarak_terendah_ke_tanah', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Jarak Terendah Ketanah', 'value' => $produk->jarak_terendah_ke_tanah, 'min' => 0)) !!}
                    </div>

                    <div class="col-lg-4 form-group">
                        <label>
                            *
                            Ketinggian Tempat Duduk
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'ketinggian_tempat_duduk', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Ketinggian Tempat Duduk', 'value' => $produk->ketinggian_tempat_duduk, 'min' => 0)) !!}
                        	<span class="m-input-icon__icon m-input-icon__icon--right">
                        	    <p style="padding-top: 2px; font-weight: bold">mm</p>
                        	</span>
                        </div>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-4 form-group ">
                        <label>
                            *
                            Radius Putar
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'radius_putar', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Radius Putar', 'value' => $produk->radius_putar)) !!}
                        	<span class="m-input-icon__icon m-input-icon__icon--right">
                        	    <p style="padding-top: 2px; font-weight: bold">mm</p>
                        	</span>
                        </div>
                    </div>

                    <div class="col-lg-4 form-group ">
                        <label>
                            Berat Kosong
                        </label>
                        <div class="m-input-icon m-input-icon--right" >
                        	{!! form_input(array('type' => 'text','name' => 'berat_kosong', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Berat Kosong', 'value' => $produk->berat_kosong,'min' => 0)) !!}
                        	<span class="m-input-icon__icon m-input-icon__icon--right">
                        	    <p style="padding-top: 2px; font-weight: bold">Kg</p>
                        	</span>
                        </div>
                    </div>
                </div>
            
                <div class="f1-buttons">
                    <button type="button" class="btn btn-previous">Previous</button>
                    <button type="button" class="btn btn-next btn-primary">Next</button>
                </div>
            </fieldset>
            <!-- End Field Set Dimensi dan Berat -->
            
            <!-- Begin Field Set Rangka dan Kaki -->
            <fieldset>
                <h4>Data Rangka &amp; Kaki kaki:</h4><br>
                
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            *
                            Tipe Rangka
                        </label>
                        {!! form_textarea('tipe_rangka',$produk->tipe_rangka,'id="" class="form-control m-input" style="height:55px" placeholder="Masukkan Tipe Rangka"') !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Tipe Suspensi Depan
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'tipe_suspensi_depan', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Tipe Suspensi Depan', 'value' => $produk->tipe_suspensi_depan)) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Tipe Suspensi Belakang
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'tipe_suspensi_belakang', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Tipe Suspensi Belakang', 'value' => $produk->tipe_suspensi_belakang)) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                   <div class="col-lg-6 form-group">
                        <label>
                            *
                            Ukuran Ban Depan
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'ukuran_ban_depan', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Ukuran Ban Depan', 'value' => $produk->ukuran_ban_depan)) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Ukuran Ban Belakang
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'ukuran_ban_belakang', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Ukuran Ban Belakang', 'value' => $produk->ukuran_ban_belakang)) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                   <div class="col-lg-6 form-group">
                        <label>
                            *
                            Rem Depan
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'rem_depan', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Rem Depan','value' => $produk->rem_depan)) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Rem Belakang
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'rem_belakang', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Rem Belakang', 'value' => $produk->rem_belakang)) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                   <div class="col-lg-6 form-group">
                        <label>
                            *
                            Jenis Velg
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'jenis_velg', 'class' => 'form-control m-input', 'placeholder' => 'Masukkan Jenis Velg', 'value' => $produk->jenis_velg)) !!}
                    </div>
                </div>


                <div class="f1-buttons">
                    <button type="button" class="btn btn-previous">Previous</button>
                    <button type="button" class="btn btn-next btn-primary">Next</button>
                </div>
            </fieldset>
            <!-- End Field Set Rangka dan Kaki -->

            <fieldset>
                <h4>Tahap Akhir:</h4><br>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Produk Terbaru
                        </label>
                        {!! form_dropdown('produk_terbaik', $produk_terbaik, $produk->produk_terbaik, 'class="form-control m-select2" id="m_produk_terbaik" style="width:100%"') !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            Urutan Produk Terbaru:
                        </label>
                        {!! form_dropdown('urutan_produk_terbaru', $urutan_produk_terbaru, $produk->produk_terbaik == 'T' ? $produk->urutan_produk_terbaik : '', 'class="form-control m-select2" id="m_urutan_produk_terbaik" style="width: 100%"') !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Brosur Produk:
                        </label>

                       <?php 
                        if(!empty($produk->brosur_produk)) { ?> 
                            <a href="{{ $path_brosur_produk . $produk->brosur_produk}}">{{ $produk->brosur_produk }}</a>
                            <br>
                        <?php } else { ?>
                            <a href=""></a>
                            <br>
                        <?php } ?>
                        
                        {!! form_input(array('type' => 'file','name' => 'brosur_produk', 'class' => 'form-control m-input', 'accept' => 'application/pdf')) !!}
                    
                        <h6 class="test-red">*Keterangan: 
                            <ul>
                                <li>Maksimal file 15mb</li>
                                <li>jenis file: pdf</li>
                            </ul>
                        </h6>
                    </div>
                </div>

                    <!-- <div class="col-lg-6 form-group">
                        <label>
                            *
                            Status Tampil:
                        </label>
                        <br>
                            <input type="checkbox" name="status_promo" data-switch="true" id="m_switch_1" data-on-text="Aktif" data-off-text="Nonaktif" data-on-color="danger">
                    </div> -->
         

                <div class="f1-buttons">
                    <button type="button" class="btn btn-previous">Previous</button>
                    <button type="submit" class="btn btn-submit btn-primary save">Submit</button>
                </div>
            </fieldset>
        
        {!! form_input(array('type' => 'hidden','name' => 'id_', 'class' => 'form-control m-input', 'value' => encryptID($produk->kode_produk))) !!}
        
        {!! form_input(array('type' => 'hidden','name' => 'checkbox_tipe_mesin', 'class' => 'form-control m-input', 'placeholder' => '')) !!}
        {!! form_input(array('type' => 'hidden','name' => 'checkbox_tipe_kopling', 'class' => 'form-control m-input', 'placeholder' => '')) !!}
        {!! form_input(array('type' => 'hidden','name' => 'checkbox_tipe_rangka', 'class' => 'form-control m-input', 'placeholder' => '')) !!}
        {!! form_close() !!}
        </div>
    </div>
@stop

@section('scripts')
<script type="text/javascript">
   var getUrutanProduk = '{{$getUrutanProduk}}';
</script>
<script src="{{ base_url() }}assets/backend/js/bootzard/jquery.backstretch.min.js"></script>
<script src="{{ base_url() }}assets/backend/js/bootzard/retina-1.1.0.min.js"></script>
<script src="{{ base_url() }}assets/backend/js/bootzard/scripts-edit.js"></script>
<script src="{{ base_url() }}assets/backend/js/produk_rekomendasi/produk/js/produk-edit.js" type="text/javascript"></script>
@stop