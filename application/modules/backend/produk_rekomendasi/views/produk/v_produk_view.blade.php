@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form View{{ get_menu_name() }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{$back}}" class="m-portlet__nav-link m-portlet__nav-link--icon">
                            <i class="la la-undo"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open('', array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-4 form-group">
                    <label>
                        *
                        Kategori Motor
                    </label>
                    <h5>{{ $produk->nama_kategori_motor }}</h5>
                </div>
                <div class="col-lg-4 form-group">
                    <label>
                        *
                        Nama Produk:
                    </label>
                    <h5>{{ $produk->nama_produk }}</h5>
                </div>
                <div class="col-lg-4 form-group">
                    <label>
                        *
                        Harga Produk:
                    </label>
                    <h5>{{$produk->harga_produk}}</h5>
                    <!--<h5>{{ 'Rp. ' . formattedNumber($produk->harga_produk) }}</h5>-->
                </div>
            </div>

            <div class="form-group m-form__group row">
                <div class="col-lg-4 form-group">
                    <label>
                        *
                        Gambar Produk
                    </label>
                    <div>
                        <img src="{{base_url()}}assets/upload/produk/gambar/{{ $produk->gambar_produk }}">
                    </div>
                </div>
                <div class="col-lg-4 form-group">
                    <label>
                        *
                        Deskripsi Produk
                    </label>
                    <h5>{{ $produk->deskripsi_produk }}</h5>
                </div>
                <div class="col-lg-4 form-group">
                    <label>
                        *
                        Brosur Produk
                    </label>
                    <div>
                        <a href="{{base_url()}}assets/upload/produk/brosur/{{ $produk->brosur_produk }}" target="_blank">{{ $produk->brosur_produk }}</a>
                    </div>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <div class="col-lg-4 form-group">
                    <label>
                        *
                        Produk Terbaik
                    </label>
                    @if($produk->produk_terbaik == "T")
                    <h5>Ya</h5>
                    @else
                    <h5>Tidak</h5>
                    @endif
                </div>
                <div class="col-lg-4 form-group">
                    <label>
                        *
                        Urutan Produk Terbaik
                    </label>
                    @if($produk->produk_terbaik == "T")
                    <h5>{{ $produk->urutan_produk_terbaik }}</h5>
                    @else
                    <h5>-</h5>
                    @endif
                </div>
                <div class="col-lg-4 form-group">
                    <label>
                        *
                        Status Tampil
                    </label>
                    @if($produk->status_tampil == "T")
                    <h5>Ya</h5>
                    @else
                    <h5>Tidak</h5>
                    @endif
                </div>
            </div>

            <div class="form-group m-form__group">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a href="#tab_5_1" class="nav-link active" data-toggle="tab" aria-expanded="false"> Produk Varian </a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab_5_2" class="nav-link" data-toggle="tab" aria-expanded="false"> Mesin </a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab_5_3" class="nav-link" data-toggle="tab" aria-expanded="false"> Kelistrikan </a>
                    </li>
                    <li>
                        <a href="#tab_5_4" class="nav-link" data-toggle="tab" aria-expanded="false"> Dimensi dan Berat </a>
                    </li>
                    <li>
                        <a href="#tab_5_5" class="nav-link" data-toggle="tab" aria-expanded="false"> Rangka dan Kaki </a>
                    </li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="tab_5_1" role="tabpanel">
                        <div class="form-group m-form__group row">
                           <div class="col-lg-6 form-group">
                                <label>* Nama Varian </label>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>* Harga OTR </label>
                            </div>
                            @if($harga_otr != "[]")
                            @foreach($harga_otr as $vharga_otr)
                            <div class="col-lg-6 form-group">
                                <h5>{{ $vharga_otr->nama_produk_otr }}</h5>
                            </div>
                            <div class="col-lg-6 form-group">
                                <h5>{{ 'Rp. ' . formattedNumber($vharga_otr->harga_produk_otr) }}</h5>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="tab-pane" id="tab_5_2" role="tabpanel">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-12 form-group">
                                <label>
                                    *
                                    Tipe Mesin
                                </label>
                                    <h5>{{ $produk->tipe_mesin }}</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 form-group">
                                <label>
                                    *
                                    Tipe Transmisi
                                </label>
                                    <h5>{{ $produk->tipe_transmisi }}</h5>
                            </div>
                            <div class="col-lg-4 form-group" id="oper_gigi">
                                <label>
                                    Pola Pengoperan Gigi
                                </label>
                                    <h5>{{ $produk->pola_pengoperan_gigi }}</h5>
                            </div>
                            <div class="col-lg-4 form-group" id="reduksi_gigi">
                                <label>
                                    Rasio Reduksi Gigi
                                </label>
                                    <h5>{{ $produk->rasio_reduksi_gigi }}</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-12 form-group" id="kopling">
                                <label>
                                    *
                                    Tipe Kopling
                                </label>
                                    <h5>{{ $produk->tipe_kopling }}</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 form-group ">
                                <label>
                                    *
                                    Tipe Starter
                                </label>
                                    <h5>{{ $produk->tipe_starter }}</h5>
                            </div>
                            <div class="col-lg-4 form-group ">
                                <label>
                                    *
                                    Tipe Busi
                                </label>
                                    <h5>{{ $produk->tipe_busi }}</h5>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label>
                                    *
                                    Diameter
                                </label>
                                    <h5>{{ $produk->diameter }} mm</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 form-group">
                                <label>
                                    *
                                    Langkah
                                </label>
                                    <h5>{{ $produk->langkah }} mm</h5>
                            </div>
                            <div class="col-lg-4 form-group ">
                                <label>
                                    *
                                    Volume Langkah
                                </label>
                                    <h5>{{ $produk->volume_langkah }} cc</h5>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label>
                                    *
                                    Sistem Pendingin Mesin
                                </label>
                                    <h5>{{ $produk->sistem_pendingin_mesin }}</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 form-group ">
                                <label>
                                    *
                                    Sistem Bahan Bakar
                                </label>
                                    <h5>{{ $produk->sistem_suplai_bb }}</h5>
                            </div>
                            <div class="col-lg-4 form-group ">
                                <label>
                                    Perbandingan Kompresi
                                </label>
                                    <h5>{{ $produk->perbandingan_kompresi }}</h5>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label>
                                    *
                                    Daya Maksimum
                                </label>
                                    <h5>{{ $produk->daya_maksimum }}</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 form-group ">
                                <label>
                                    *
                                    Torsi Maksimum
                                </label>
                                    <h5>{{ $produk->torsi_maksimum }}</h5>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label>
                                    *
                                    Sistem Pelumasan
                                </label>
                                    <h5>{{ $produk->sistem_pelumasan }}</h5>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label>
                                    *
                                    Sistem Pengereman
                                </label>
                                    <h5>{{ $produk->sistem_pengereman }}</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 form-group">
                                <label>
                                    *
                                    Sistem Idling Stop
                                </label>
                                    <h5>{{ $produk->sistem_idling_stop }}</h5>
                            </div>
                            <div class="col-lg-4 form-group ">
                                <label>
                                    *
                                    Kapasitas Tangki Bahan Bakar
                                </label>
                                    <h5>{{ $produk->kapasitas_tangki_bb }} L</h5>
                            </div>
                            <div class="col-lg-4 form-group ">
                                <label>
                                    *
                                    Kapasitas Minyak Pelumas
                                </label>
                                    <h5>{{ $produk->kapasitas_minyak_pelumas }} L</h5>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab_5_3" role="tabpanel">
                        <div class="form-group m-form__group row">
                           <div class="col-lg-6 form-group">
                                <label>
                                    *
                                    Tipe Baterai
                                </label>
                                    <h5>{{ $produk->tipe_baterai }}</h5>
                            </div>
                            <div class="col-lg-6 form-group ">
                                <label>
                                    Sistem Pengapian
                                </label>
                                    <h5>{{ $produk->sistem_pengapian }}</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                           <div class="col-lg-6 form-group">
                                <label>
                                    *
                                    Lampu Depan
                                </label>
                                    <h5>{{ $produk->lampu_depan }}</h5>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>
                                    *
                                    Lampu Senja
                                </label>
                                    <h5>{{ $produk->lampu_senja }}</h5>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab_5_4" role="tabpanel">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 form-group">
                                <label>
                                    *
                                    Panjang
                                </label>
                                    <h5>{{ $produk->panjang }} mm</h5>
                            </div>
                            <div class="col-lg-4 form-group ">
                                <label>
                                    *
                                    Lebar
                                </label>
                                    <h5>{{ $produk->lebar }} mm</h5>
                            </div>
                            <div class="col-lg-4 form-group ">
                                <label>
                                    *
                                    Tinggi
                                </label>
                                    <h5>{{ $produk->tinggi }} mm</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 form-group">
                                <label>
                                    *
                                    Jarak Sumbu Roda
                                </label>
                                    <h5>{{ $produk->jarak_sumbu_roda }} mm</h5>
                            </div>
                            <div class="col-lg-4 form-group ">
                                <label>
                                    *
                                    Jarak Terendah Ke Tanah
                                </label>
                                    <h5>{{ $produk->jarak_terendah_ke_tanah }} mm</h5>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label>
                                    *
                                    Ketinggian Tempat Duduk
                                </label>
                                    <h5>{{ $produk->ketinggian_tempat_duduk }} mm</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 form-group ">
                                <label>
                                    *
                                    Radius Putar
                                </label>
                                    <h5>{{ $produk->radius_putar }} mm</h5>
                            </div>
                            <div class="col-lg-4 form-group ">
                                <label>
                                    Berat Kosong
                                </label>
                                    <h5>{{ $produk->berat_kosong }} Kg</h5>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab_5_5" role="tabpanel">
                        <div class="form-group m-form__group row">
                           <div class="col-lg-12 form-group">
                                <label>
                                    *
                                    Tipe Rangka
                                </label>
                                    <h5>{{ $produk->tipe_rangka }}</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                           <div class="col-lg-6 form-group">
                                <label>
                                    *
                                    Tipe Suspensi Depan
                                </label>
                                    <h5>{{ $produk->tipe_suspensi_depan }}</h5>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>
                                    *
                                    Tipe Suspensi Belakang
                                </label>
                                    <h5>{{ $produk->tipe_suspensi_belakang }}</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                           <div class="col-lg-6 form-group">
                                <label>
                                    *
                                    Ukuran Ban Depan
                                </label>
                                    <h5>{{ $produk->ukuran_ban_depan }}</h5>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>
                                    *
                                    Ukuran Ban Belakang
                                </label>
                                    <h5>{{ $produk->ukuran_ban_belakang }}</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                           <div class="col-lg-6 form-group">
                                <label>
                                    *
                                    Rem Depan
                                </label>
                                    <h5>{{ $produk->rem_depan }}</h5>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>
                                    *
                                    Rem Belakang
                                </label>
                                    <h5>{{ $produk->rem_belakang }}</h5>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                           <div class="col-lg-6 form-group">
                                <label>
                                    *
                                    Jenis Velg
                                </label>
                                    <h5>{{ $produk->jenis_velg }}</h5>
                            </div>
                        </div>
                    </div>

                </div> 

            </div>
        </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop
