@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Produk:
                        </label>
                        <select id="m_select2_1" name="kode_produk" class="form-control m-select2">
                            <option value="{{encryptID($produk->kode_produk)}}" selected>{{$produk->nama_produk}}</option>
                        </select>
                    </div>
                </div>
                <div class="m-form__group row">
                    <div class="col-lg-3 form-group ">
                        <label>
                            *
                            Rekomendasi 1:
                        </label>
                        <select id="m_select2_2" name="kode_rekomendasi_1" class="form-control m-select2">
                            <option value="{{encryptID($rekomendasi_1->kode_produk)}}" selected>{{$rekomendasi_1->nama_produk}}</option>
                        </select>
                    </div>
                    <div class="col-lg-3 form-group ">
                        <label>
                            *
                            Rekomendasi 2:
                        </label>
                        <select id="m_select2_3" name="kode_rekomendasi_2" class="form-control m-select2">
                            <option value="{{encryptID($rekomendasi_2->kode_produk)}}" selected>{{$rekomendasi_2->nama_produk}}</option>
                        </select>
                    </div>
                    <div class="col-lg-3 form-group ">
                        <label>
                            *
                            Rekomendasi 3:
                        </label>
                        <select id="m_select2_4" name="kode_rekomendasi_3" class="form-control m-select2">
                            <option value="{{encryptID($rekomendasi_3->kode_produk)}}" selected>{{$rekomendasi_3->nama_produk}}</option>
                        </select>
                    </div>
                    <div class="col-lg-3 form-group ">
                        <label>
                            *
                            Rekomendasi 4:
                        </label>
                        <select id="m_select2_5" name="kode_rekomendasi_4" class="form-control m-select2">
                            <option value="{{encryptID($rekomendasi_4->kode_produk)}}" selected>{{$rekomendasi_4->nama_produk}}</option>
                        </select>
                    </div>
                </div>
                <div class="m-form__group row">
                    
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_save"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($ms_rekomendasi_produk->kode_rekomendasi_produk) )) !!}
        
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var urlRekomendasi  = "{{base_url()}}produk_rekomendasi/rekomendasi_produk/selectRekomendasi";
</script>
<script src="{{ base_url() }}assets/backend/js/produk_rekomendasi/rekomendasi_produk/js/rekomendasi-produk.js" type="text/javascript"></script>
@stop