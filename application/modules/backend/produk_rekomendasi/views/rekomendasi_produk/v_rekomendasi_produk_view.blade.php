@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Detail {!! get_menu_name() !!}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ $back }}" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-undo"></i>
                            </a>
                        </li>
                    </ul>
                </div>
        </div>
        <!--begin::Form-->
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Kategori Produk:
                        </label>
                        <h5>{!! $ms_rekomendasi_produk->nama_kategori_motor !!}</h5>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            Nama Produk:
                        </label>
                        <h5>{!! $ms_rekomendasi_produk->nama_produk !!}</h5>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-3 form-group">
                        <label>
                            Nama Rekomendasi 1:
                        </label>
                        <h5>{!! $rekomendasi_1->nama_produk !!}</h5>
                        <h6>{!! $rekomendasi_1->harga_produk !!}</h6>
                        <!-- <h6>Rp {!! number_format( $rekomendasi_1->harga_produk , 2 , ',' , '.' ) !!}</h6> -->
                    </div>
                    <div class="col-lg-3 form-group">
                        <label>
                            Nama Rekomendasi 2:
                        </label>
                        <h5>{!! $rekomendasi_2->nama_produk !!}</h5>
                        <h6>{!! $rekomendasi_2->harga_produk !!}</h6>
                        <!-- <h6>Rp {!! number_format( $rekomendasi_2->harga_produk , 2 , ',' , '.' ) !!}</h6> -->
                    </div>
                    <div class="col-lg-3 form-group">
                        <label>
                            Nama Rekomendasi 3:
                        </label>
                        <h5>{!! $rekomendasi_3->nama_produk !!}</h5>
                        <h6>{!! $rekomendasi_3->harga_produk !!}</h6>
                        <!-- <h6>Rp {!! number_format( $rekomendasi_3->harga_produk , 2 , ',' , '.' ) !!}</h6> -->
                    </div>
                    <div class="col-lg-3 form-group">
                        <label>
                            Nama Rekomendasi 4:
                        </label>
                        <h5>{!! $rekomendasi_4->nama_produk !!}</h5>
                        <h6>{!! $rekomendasi_4->harga_produk !!}</h6>
                        <!-- <h6>Rp {!! number_format( $rekomendasi_4->harga_produk , 2 , ',' , '.' ) !!}</h6> -->
                    </div>
                </div>
            </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop