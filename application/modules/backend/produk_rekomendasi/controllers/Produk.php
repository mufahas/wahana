<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        /* Load Model */
        $this->load->model('select_global_model');
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $data['message_success'] = $this->session->flashdata('message_success');
        $data['message_error']   = $this->session->flashdata('messages_error');

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_produk";
        $condition    = "ms_produk.dihapus = 'F'";
        $row          = array('ms_produk.kode_produk','ms_kategori_motor.nama_kategori_motor','ms_produk.nama_produk');
        $row_search   = array('ms_produk.kode_produk','ms_kategori_motor.nama_kategori_motor','ms_produk.nama_produk');
        $join         = array('ms_kategori_motor' => 'ms_kategori_motor.kode_kategori_motor = ms_produk.kode_kategori_motor');
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['url_succees']     = site_url() . $this->site;
        $data['action']          = site_url() . $this->site . '/save';
        
        /* Get Dropdown */
        $data['kategori_motor']        = $this->select_global_model->selectKategoriMotor();
        $data['produk_terbaik']        = $this->select_global_model->selectProductTerbaik();
        $data['urutan_produk_terbaik'] = $this->select_global_model->selectUrutanProductTerbaik();
        $data['tipe_starter']          = $this->select_global_model->selectTipeStarter();
        $data['tipe_transmisi']        = $this->select_global_model->selectTipeTransmisi();
        $data['pengoperan_gigi']       = $this->select_global_model->selectPolaPengoperanGigi();
        $data['urutan_produk_terbaru']    = '- Pilih Urutan Produk -';

        /* Get Model for Checkbox */
        $data['tipe_mesin']      = ms_tipe_mesin::where('ms_tipe_mesin.dihapus','F')->get();
        $data['tipe_kopling']    = ms_tipe_kopling::where('ms_tipe_kopling.dihapus','F')->get();
        $data['tipe_rangka']     = ms_tipe_rangka::where('ms_tipe_rangka.dihapus','F')->get();
        
        /* Get Ajax Function */
        $data['getUrutanProduk']          = site_url() . $this->site . '/getUrutanProduk';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }
    
    /**
    * Ajax Function Get Urutan Produk
    **/
    function getUrutanProduk()
    {
        $produk_terbaik         = $this->input->get('produk_terbaik');

        $urutan_produk_terbaru  = $this->select_global_model->selectUrutanProdukTerbaru($produk_terbaik);

        $result                 = form_dropdown('urutan_produk_terbaru', $urutan_produk_terbaru, '', 'class="form-control  m-select2" id="m_urutan_produk_terbaik"');
        echo json_encode($result);
        
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {    
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            /* Url */
            $url_succees              = site_url() . $this->site . '/detail' . '/';
            $url_error                = site_url() . $this->site . '/add';
            
            /* Get User Login */
            $user                     = $this->ion_auth->user()->row();
            
            /* Get Data Post Tab Produk */
            $kode_kategori_motor      = $this->input->post('kategori_motor');
            $nama_produk              = ucwords($this->input->post('nama_produk'));
            $harga_produk             = $this->input->post('harga_produk');
            $deskripsi_produk         = $this->input->post('deskripsi_produk');
            $nama_varian              = $this->input->post('nama_varian[]');
            $harga_otr                = $this->input->post('harga_otr[]');
            $lokasi_otr               = $this->input->post('lokasi_otr[]');
        
            if ($tipe_transmisi = "4 Kecepatan") {
                $pola_pengoperan_gigi = "1-N-2-3-4";
            }else if($tipe_transmisi = "5 Kecepatan"){
                $pola_pengoperan_gigi = "1-N-2-3-4-5";
            }else if($tipe_transmisi = "6 Kecepatan"){
                $pola_pengoperan_gigi = "1-N-2-3-4-5-6";
            }else{
                $pola_pengoperan_gigi = "-";
            }

            $rasio_reduksi_gigi       = ucwords($this->input->post('rasio_reduksi_gigi'));
            $tipe_mesin               = ucwords($this->input->post('tipe_mesin'));
            $tipe_kopling             = ucwords($this->input->post('tipe_kopling'));
            $tipe_starter             = $this->input->post('tipe_starter');
            $tipe_busi                = ucwords($this->input->post('tipe_busi'));
            $diameter                 = $this->input->post('diameter');
            $langkah                  = $this->input->post('langkah');
            $volume_langkah           = ucwords($this->input->post('volume_langkah'));
            $sistem_pendingin_mesin   = ucwords($this->input->post('sistem_pendingin_mesin'));
            $sistem_bahan_bakar       = ucwords($this->input->post('sistem_bahan_bakar'));
            $perbandingan_kompresi    = ucwords($this->input->post('perbandingan_kompresi'));
            $daya_maksimum            = ucwords($this->input->post('daya_maksimum'));
            $torsi_maksimum           = ucwords($this->input->post('torsi_maksimum'));
            $sistem_pelumasan         = ucwords($this->input->post('sistem_pelumasan'));
            $sistem_pengereman        = ucwords($this->input->post('sistem_pengereman'));
            $sistem_idling_stop       = ucwords($this->input->post('sistem_idling_stop'));
            $kapasitas_tangki_bb      = ucwords($this->input->post('kapasitas_tangki_bb'));
            $kapasitas_minyak_pelumas = ucwords($this->input->post('kapasitas_minyak_pelumas'));
            
            /* Get Data Post Tab Kelistrikan */
            $tipe_baterai             = ucwords($this->input->post('tipe_baterai'));
            $sistem_pengapian         = ucwords($this->input->post('sistem_pengapian'));
            $lampu_depan              = ucwords($this->input->post('lampu_depan'));
            $lampu_senja              = ucwords($this->input->post('lampu_senja'));

            /* Get Data Post Tab Dimensi dan Berat */
            $panjang                  = $this->input->post('panjang');
            $lebar                    = $this->input->post('lebar');
            $tinggi                   = $this->input->post('tinggi');
            $jarak_sumbu_roda         = $this->input->post('jarak_sumbu_roda');
            $jarak_terendah_ke_tanah  = $this->input->post('jarak_terendah_ke_tanah');
            $ketinggian_tempat_duduk  = $this->input->post('ketinggian_tempat_duduk');
            $radius_putar             = $this->input->post('radius_putar');
            $berat_kosong             = $this->input->post('berat_kosong');
            
            /* Get Data Post Tab Rangka dan Kaki */
            $tipe_rangka              = ucwords($this->input->post('tipe_rangka'));
            $tipe_suspensi_depan      = ucwords($this->input->post('tipe_suspensi_depan'));
            $tipe_suspensi_belakang   = ucwords($this->input->post('tipe_suspensi_belakang'));
            $ukuran_ban_depan         = ucwords($this->input->post('ukuran_ban_depan'));
            $ukuran_ban_belakang      = ucwords($this->input->post('ukuran_ban_belakang'));
            $rem_depan                = ucwords($this->input->post('rem_depan'));
            $rem_belakang             = ucwords($this->input->post('rem_belakang'));
            $jenis_velg               = ucwords($this->input->post('jenis_velg'));

            /* Get Data Post Tab Tahap Akhir */
            $produk_terbaik           = $this->input->post('produk_terbaik');
            $urutan_produk_terbaik    = $this->input->post('urutan_produk_terbaru');
            // $brosur_produk            = $this->input->post('brosur_produk');
            $status_tampil            = "F";
            // $status_t                 = $this->input->post('status_tampil');
            // if(!empty($status_t)){
            //     $status_tampil = "T";
            // }else{
            //     $status_tampil = "F";
            // }
            
            /* Get Data Post Upload Gambar Produk */
            $userfile                 = $_FILES["userfile"]["name"];
            $userfile_size            = $_FILES["userfile"]["size"];
            $userfile_tmp_name        = $_FILES["userfile"]["tmp_name"];

            /* Get Data Post Upload Brosur Produk */
            $userfile_                 = $_FILES["brosur_produk"]["name"];
            $userfile_size_            = $_FILES["brosur_produk"]["size"];
            $userfile_tmp_name_        = $_FILES["brosur_produk"]["tmp_name"];

            /* Start Save Data ms_produk */
        $model_produk = new ms_produk;

        $model_produk->kode_kategori_motor      = $kode_kategori_motor;
        $model_produk->nama_produk              = $nama_produk;
        $model_produk->nama_produk_url          = clean_url(strtolower($nama_produk));
        $model_produk->harga_produk             = $harga_produk;
        $model_produk->deskripsi_produk         = $deskripsi_produk;
        // harga otr

        // tipe mesin
        $model_produk->tipe_mesin               = $tipe_mesin;
        $model_produk->tipe_transmisi           = $tipe_transmisi;
        $model_produk->pola_pengoperan_gigi     = ($tipe_transmisi == 'Matic' ? '-' : $pola_pengoperan_gigi); 
        $model_produk->rasio_reduksi_gigi       = empty($rasio_reduksi_gigi) ?  '-' : $rasio_reduksi_gigi; 
        // tipe kopling
        $model_produk->tipe_kopling             = $tipe_kopling;
        $model_produk->tipe_starter             = $tipe_starter; 
        $model_produk->tipe_busi                = $tipe_busi; 
        $model_produk->diameter                 = $diameter;
        $model_produk->langkah                  = $langkah;
        $model_produk->volume_langkah           = $volume_langkah;
        $model_produk->sistem_pendingin_mesin   = $sistem_pendingin_mesin;
        $model_produk->sistem_suplai_bb         = $sistem_bahan_bakar;
        $model_produk->perbandingan_kompresi    = $perbandingan_kompresi;
        $model_produk->daya_maksimum            = $daya_maksimum;
        $model_produk->torsi_maksimum           = $torsi_maksimum;
        $model_produk->sistem_pelumasan         = $sistem_pelumasan; 
        $model_produk->sistem_pengereman        = $sistem_pengereman; 
        $model_produk->sistem_idling_stop       = $sistem_idling_stop; 
        $model_produk->kapasitas_tangki_bb      = $kapasitas_tangki_bb;
        $model_produk->kapasitas_minyak_pelumas = $kapasitas_minyak_pelumas; 

        $model_produk->tipe_baterai             = $tipe_baterai; 
        $model_produk->sistem_pengapian         = $sistem_pengapian;
        $model_produk->lampu_depan              = $lampu_depan;
        $model_produk->lampu_senja              = $lampu_senja;

        $model_produk->panjang                  = $panjang; 
        $model_produk->lebar                    = $lebar; 
        $model_produk->tinggi                   = $tinggi; 
        $model_produk->jarak_sumbu_roda         = $jarak_sumbu_roda; 
        $model_produk->jarak_terendah_ke_tanah  = $jarak_terendah_ke_tanah; 
        $model_produk->ketinggian_tempat_duduk  = $ketinggian_tempat_duduk; 
        $model_produk->radius_putar             = $radius_putar;
        $model_produk->berat_kosong             = $berat_kosong;

        // tipe rangka
        $model_produk->tipe_rangka              = $tipe_rangka;
        $model_produk->tipe_suspensi_depan      = $tipe_suspensi_depan;
        $model_produk->tipe_suspensi_belakang   = $tipe_suspensi_belakang;
        $model_produk->ukuran_ban_depan         = $ukuran_ban_depan;
        $model_produk->ukuran_ban_belakang      = $ukuran_ban_belakang;
        $model_produk->rem_depan                = $rem_depan;
        $model_produk->rem_belakang             = $rem_belakang;
        $model_produk->jenis_velg               = $jenis_velg;

        $model_produk->produk_terbaik           = $produk_terbaik;
        $model_produk->urutan_produk_terbaik    = $urutan_produk_terbaik;
        $model_produk->status_tampil            = $status_tampil;


        $save_produk = $model_produk->save();

        if($save_produk)
            {
                
                /* Start Save Data ms_harga_otr */
                    foreach ($nama_varian as $key => $value) {
                        $ms_harga_otr                                 = new ms_harga_otr;

                        $ms_harga_otr->kode_produk                    = ms_produk::max('kode_produk');
                        $ms_harga_otr->nama_produk_otr                = $value;
                        $ms_harga_otr->harga_produk_otr               = $harga_otr[$key];
                        $ms_harga_otr->lokasi_otr                     = ucwords($lokasi_otr[$key]);

                        $save_ms_harga_otr                            = $ms_harga_otr->save();
                    }
                /* End Save Data ms_harga_otr */

                /* Start Save Data file */
                if(!empty($userfile) && !empty($userfile_))
                {
                    /*  */
                    $filename            = $userfile;
                    $file_basename       = substr($filename, 0, strripos($filename, '.')); // get file extention
                    $file_ext            = substr($filename, strripos($filename, '.')); // get file name extension
                    $filesize            = $userfile_size; //get size
                    $allowed_file_types  = array('.png','.jpg','.jpeg');
                    
                    /* EXCEL or PDF Daftar Harga */
                    $filename_           = $userfile_;
                    $file_basename_      = substr($filename_, 0, strripos($filename_, '.')); // get file extention
                    $file_ext_           = substr($filename_, strripos($filename_, '.')); // get file name extension
                    $filesize_           = $userfile_size_; //get size
                    $allowed_file_types_ = array('.pdf');

                    if(in_array(strtolower($file_ext),$allowed_file_types) && in_array(strtolower($file_ext_),$allowed_file_types_) && $filesize <= 5000000 && $filesize_ <= 15000000) {

                        //Set new filename
                        $file_img   = 'PRODUK_GAMBAR_'. ms_produk::max('kode_produk') . '_' . date('Y-m-d') . $file_ext;
                        $file_pdf   = 'PRODUK_BROSUR_'. ms_produk::max('kode_produk') . '_' . date('Y-m-d') . $file_ext_;


                        //Condition Error If File Exist
                        if(file_exists("assets/upload/produk/gambar/" . $file_img) && file_exists("assets/upload/produk/brosur/" . $file_pdf) ) {
                            $this->session->set_flashdata('message', lang('message_save_failed'));
                            redirect(site_url() . $this->site);
                        } else {
                            if(move_uploaded_file($userfile_tmp_name,  "assets/upload/produk/gambar/" . $file_img) && move_uploaded_file($userfile_tmp_name_,  "assets/upload/produk/brosur/" . $file_pdf)) {

                                $kode_produk         = ms_produk::max('kode_produk');
                                $model_file_produk   = ms_produk::where('kode_produk',$kode_produk)->first();
                                
                                $model_file_produk->gambar_produk = $file_img;
                                $model_file_produk->brosur_produk = $file_pdf;
                                
                                $save_file_produk                 = $model_file_produk->save();
                            }
                            $this->session->set_flashdata('message', lang('message_save_success'));
                            redirect($url_succees . encryptID(ms_produk::max('kode_produk')));
                        } 
                    } elseif(empty($file_basename) && empty($file_basename_)) {
                        $this->session->set_flashdata('message', lang('message_save_failed'));
                        redirect(site_url() . $this->site);
                    } elseif ($filesize >= 5000000 && $filesize_ >= 15000000) {
                        $this->session->set_flashdata('message', lang('message_save_failed'));
                        redirect(site_url() . $this->site);
                    } else {
                        $this->session->set_flashdata('message', lang('message_save_failed'));
                        unlink($userfile_tmp_name);
                        redirect(site_url() . $this->site);
                    }
                }
                /* END Save data file*/ 


                /* Write Log */
                $data_notif = array(
                                     "Kode Produk"        => ms_produk::max('kode_produk'),
                                     "Nama Produk "       => $nama_produk,
                                     );

                $message = "Berhasil menambahkan master produk" . $nama_produk;
                $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                /* End Write Log */

                //$status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);

            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
            }
        }
    }

    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {   
        $kode_produk = decryptID($id);

        $ms_produk  = ms_produk::where('kode_produk',$kode_produk)->first();

        if(!empty($ms_produk))
        {   
            /* Path Gambar Produk  */
            $data['path_gambar_produk']     = site_url() . 'assets/upload/produk/gambar/';

            /* Path Brosur Produk  */
            $data['path_brosur_produk']     = site_url() . 'assets/upload/produk/brosur/';

            /* Get Produk */
            $data['produk']                 = $ms_produk;

            /* Get Kategori Motor */
            $data['kategori_motor']         = $this->select_global_model->selectKategoriMotor();

            /* Get Tipe Starter */
            $data['tipe_starter']           = $this->select_global_model->selectTipeStarter();

            /* Get Tipe Transmisi */
            $data['tipe_transmisi']         = $this->select_global_model->selectTipeTransmisi();
            
            /* Get Pengoperan Gigi */
            $data['pengoperan_gigi']        = $this->select_global_model->selectPolaPengoperanGigi();

            /* Get Harga OTR */
            $data['harga_otr_']             = ms_harga_otr::where('ms_harga_otr.kode_produk',$kode_produk)->get();
            
            /* Get Produk Terbaik */
            $data['produk_terbaik']         = $this->select_global_model->selectProductTerbaik();

            /* Get Urutan Produk Terbaik */
            $data['urutan_produk_terbaru']  = $this->select_global_model->selectUrutanProductTerbaik($kode_produk);
            
            /* Ajax Get Urutan */
            $data['getUrutanProduk']        = site_url() . $this->site . '/getUrutanProdukEdit';

            /* Button Action */
            $data['action']                 = site_url() . $this->site . '/update';

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        } else {
            redirect(site_url() . $this->site);
        }
    }
    
    /**
    * Ajax Function Delete Harga Otr
    **/
    function deleteHargaOtr()
    {
        $kode_harga_otr = $this->input->post('kode_harga_otr');
        $delete         = ms_harga_otr::where('ms_harga_otr.kode_harga_otr',$kode_harga_otr)->delete();
        if ($delete) {
            echo json_encode([ 'status' => 'success',
                    'kode_harga_otr' => $kode_harga_otr
                    ]);
        }
    }
    
    /**
    * Ajax Function Get Urutan Produk
    **/
    function getUrutanProdukEdit()
    {
        $produk_terbaik         = $this->input->get('produk_terbaik');
        $id                     = $this->input->get('kode_produk'); 
        $kode_produk            = decryptID($id);

        $ms_produk              = ms_produk::where('ms_produk.kode_produk',$kode_produk)->first();

        $urutan_produk_terbaru  = $this->select_global_model->selectUrutanProdukTerbaru($produk_terbaik,$kode_produk);

        
        $result                 = form_dropdown('urutan_produk_terbaru', $urutan_produk_terbaru, $ms_produk == 'T' ? $ms_produk->urutan_produk_terbaik : '', 'class="form-control  m-select2" id="m_urutan_produk_terbaik"');

        echo json_encode($result);
        
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") 
        {   
            /* Set URL */
            $url_succees              = site_url() . $this->site . '/detail/';
            $url_error                = site_url() . $this->site . '/add';
            
            /* Get Post Produk */
            $kategori_motor           = $this->input->post('kategori_motor');
            $nama_produk              = ucfirst($this->input->post('nama_produk'));
            $harga_produk             = $this->input->post('harga_produk');
            $deskripsi_produk         = ucfirst($this->input->post('deskripsi_produk'));
            $kode_harga_otr_edit      = $this->input->post('kode_harga_otr_edit');
            $nama_varian_edit         = $this->input->post('nama_varian_edit');
            $harga_otr_edit           = $this->input->post('harga_otr_edit');
            $lokasi_otr_edit          = $this->input->post('lokasi_otr_edit');
            $nama_varian              = $this->input->post('nama_varian');            
            $harga_otr                = $this->input->post('harga_otr');
            $lokasi_otr               = $this->input->post('lokasi_otr');
            
            /* Get Post Mesin */
            $tipe_mesin               = ucwords($this->input->post('tipe_mesin'));
            $tipe_kopling             = ucwords($this->input->post('tipe_kopling'));
            $tipe_transmisi           = $this->input->post('tipe_transmisi');
            $pola_pengoperan_gigi     = $this->input->post('pengoperan_gigi');
            $rasio_reduksi_gigi       = $this->input->post('rasio_reduksi_gigi');
            $tipe_starter             = $this->input->post('tipe_starter');
            $tipe_busi                = $this->input->post('tipe_busi');
            $diameter                 = $this->input->post('diameter');
            $langkah                  = $this->input->post('langkah');
            $volume_langkah           = $this->input->post('volume_langkah');
            $sistem_pendingin_mesin   = $this->input->post('sistem_pendingin_mesin');
            $sistem_bahan_bakar       = $this->input->post('sistem_bahan_bakar');
            $perbandingan_kompresi    = $this->input->post('perbandingan_kompresi');
            $daya_maksimum            = $this->input->post('daya_maksimum');
            $torsi_maksimum           = $this->input->post('torsi_maksimum');
            $sistem_pelumasan         = $this->input->post('sistem_pelumasan');
            $sistem_pengereman        = $this->input->post('sistem_pengereman');
            $sistem_idling_stop       = $this->input->post('sistem_idling_stop');
            $kapasitas_tangki_bb      = $this->input->post('kapasitas_tangki_bb');
            $kapasitas_minyak_pelumas = $this->input->post('kapasitas_minyak_pelumas');
            
            /* Get Post Kelistrikan */
            $tipe_baterai             = $this->input->post('tipe_baterai');
            $sistem_pengapian         = $this->input->post('sistem_pengapian');
            $lampu_depan              = $this->input->post('lampu_depan');
            $lampu_senja              = $this->input->post('lampu_senja');
            
            /* Get Post Dimensi dan Berat */
            $panjang                  = $this->input->post('panjang');
            $lebar                    = $this->input->post('lebar');
            $tinggi                   = $this->input->post('tinggi');
            $jarak_sumbu_roda         = $this->input->post('jarak_sumbu_roda');
            $jarak_terendah_ke_tanah  = $this->input->post('jarak_terendah_ke_tanah');
            $ketinggian_tempat_duduk  = $this->input->post('ketinggian_tempat_duduk');
            $radius_putar             = $this->input->post('radius_putar');
            $berat_kosong             = $this->input->post('berat_kosong');
            
            /* Get Post Dimensi dan Berat */
            $tipe_rangka              = ucwords($this->input->post('tipe_rangka'));
            $tipe_suspensi_depan      = $this->input->post('tipe_suspensi_depan');
            $tipe_suspensi_belakang   = $this->input->post('tipe_suspensi_belakang');
            $ukuran_ban_depan         = $this->input->post('ukuran_ban_depan');
            $ukuran_ban_belakang      = $this->input->post('ukuran_ban_belakang');
            $rem_depan                = $this->input->post('rem_depan');
            $rem_belakang             = $this->input->post('rem_belakang');
            $jenis_velg               = $this->input->post('jenis_velg');

            /* Get Post Tahap Baru */
            $produk_terbaik           = $this->input->post('produk_terbaik');
            $urutan_produk_terbaik    = $this->input->post('urutan_produk_terbaru');

            /* Get Post Upload Gambar Produk */
            $userfile                 = $_FILES["userfile"]["name"];
            $userfile_size            = $_FILES["userfile"]["size"];
            $userfile_tmp_name        = $_FILES["userfile"]["tmp_name"];

            /* Get Post Upload Brosur Produk */
            $userfile_                = $_FILES["brosur_produk"]["name"];
            $userfile_size_           = $_FILES["brosur_produk"]["size"];
            $userfile_tmp_name_       = $_FILES["brosur_produk"]["tmp_name"];   

            //Get Post Kode Produk
            $id                       = $this->input->post('id_');
            $kode_produk              = decryptID($id);    

            $model_produk             = ms_produk::where('ms_produk.kode_produk',$kode_produk)->first();
            $old_gambar_produk        = "assets/upload/produk/gambar/" . $model_produk->gambar_produk;
            $old_gambar_brosur        = "assets/upload/produk/brosur/" . $model_produk->brosur_produk;

            $cek_urutan               = ms_produk::where('urutan_produk_terbaik',$urutan_produk_terbaik)->first();
           
            // if(empty($cek_urutan))
            // {
                /* Begin Save Data Produk */
                $model_produk->kode_kategori_motor      = $kategori_motor;
                $model_produk->nama_produk              = $nama_produk;
                $model_produk->nama_produk_url          = clean_url(strtolower($nama_produk));
                $model_produk->harga_produk             = $harga_produk;
                $model_produk->tipe_mesin               = $tipe_mesin;
                $model_produk->tipe_kopling             = $tipe_kopling;
                $model_produk->volume_langkah           = $volume_langkah;
                $model_produk->sistem_pendingin_mesin   = $sistem_pendingin_mesin;    
                $model_produk->sistem_suplai_bb         = $sistem_bahan_bakar;
                $model_produk->diameter                 = $diameter;
                $model_produk->langkah                  = $langkah;
                $model_produk->perbandingan_kompresi    = $perbandingan_kompresi;
                $model_produk->daya_maksimum            = $daya_maksimum;
                $model_produk->torsi_maksimum           = $torsi_maksimum;
                $model_produk->tipe_transmisi           = $tipe_transmisi;
                $model_produk->pola_pengoperan_gigi     = $pola_pengoperan_gigi;
                $model_produk->rasio_reduksi_gigi       = $rasio_reduksi_gigi;
                $model_produk->tipe_starter             = $tipe_starter;
                $model_produk->sistem_pelumasan         = $sistem_pelumasan;
                $model_produk->sistem_pengapian         = $sistem_pengapian;
                $model_produk->lampu_depan              = $lampu_depan;
                $model_produk->lampu_senja              = $lampu_senja;
                $model_produk->tipe_baterai             = $tipe_baterai;
                $model_produk->tipe_busi                = $tipe_busi;
                $model_produk->sistem_pengereman        = $sistem_pengereman;
                $model_produk->sistem_idling_stop       = $sistem_idling_stop;
                $model_produk->tipe_rangka              = $tipe_rangka;
                $model_produk->panjang                  = $panjang;
                $model_produk->lebar                    = $lebar;
                $model_produk->tinggi                   = $tinggi;
                $model_produk->jarak_sumbu_roda         = $jarak_sumbu_roda;
                $model_produk->jarak_terendah_ke_tanah  = $jarak_terendah_ke_tanah;
                $model_produk->ketinggian_tempat_duduk  = $ketinggian_tempat_duduk;
                $model_produk->berat_kosong             = $berat_kosong;
                $model_produk->radius_putar             = $radius_putar;
                $model_produk->jenis_velg               = $jenis_velg;
                $model_produk->kapasitas_tangki_bb      = $kapasitas_tangki_bb;
                $model_produk->kapasitas_minyak_pelumas = $kapasitas_minyak_pelumas;
                $model_produk->tipe_suspensi_depan      = $tipe_suspensi_depan;
                $model_produk->tipe_suspensi_belakang   = $tipe_suspensi_belakang;
                $model_produk->ukuran_ban_depan         = $ukuran_ban_depan;
                $model_produk->ukuran_ban_belakang      = $ukuran_ban_belakang;
                $model_produk->rem_depan                = $rem_depan;
                $model_produk->rem_belakang             = $rem_belakang;
                $model_produk->produk_terbaik           = $produk_terbaik;
                $model_produk->urutan_produk_terbaik    = $urutan_produk_terbaik;
                // $model_produk->status_tampil            = 'F';

                $save_model_produk = $model_produk->save(); //Update table ms_produk 

                if($save_model_produk)
                {   
                    
                    /* Save Harga OTR Vaian Produk */
                    if(!empty($nama_varian))
                    {   
                        // ADD NEW 
                        foreach ($nama_varian as $key => $value) {
                            $ms_harga_otr                                 = new ms_harga_otr;

                            $ms_harga_otr->kode_produk                    = $kode_produk;
                            $ms_harga_otr->nama_produk_otr                = $value;
                            $ms_harga_otr->harga_produk_otr               = $harga_otr[$key];
                            $ms_harga_otr->lokasi_otr                     = ucwords($lokasi_otr[$key]);

                            $ms_harga_otr->save();
                        }

                        // Update Exist Data
                        foreach ($kode_harga_otr_edit as $key2 => $val2) {
                            
                            $ms_harga_otr_                   = ms_harga_otr::where('kode_harga_otr', decryptID($val2))->first();
                            
                            $ms_harga_otr_->nama_produk_otr  = $nama_varian_edit[$key2];
                            $ms_harga_otr_->harga_produk_otr = $harga_otr_edit[$key2];
                            $ms_harga_otr_->lokasi_otr       = ucwords($lokasi_otr_edit[$key2]);
                            
                            $ms_harga_otr_->save();
                        }
                    }
                    else
                    {
                        foreach ($kode_harga_otr_edit as $keyotr => $valotr) {
                            
                            $model_harga_otr                   = ms_harga_otr::where('kode_harga_otr', decryptID($valotr))->first();
                            
                            $model_harga_otr->nama_produk_otr  = $nama_varian_edit[$keyotr];
                            $model_harga_otr->harga_produk_otr = $harga_otr_edit[$keyotr];
                            $model_harga_otr->lokasi_otr       = ucwords($lokasi_otr_edit[$keyotr]);

                            $model_harga_otr->save();
                        }
                    }


                    //kondisi apabila hanya gambar produk yang diubah
                    if(!empty($userfile) && empty($userfile_))
                    {
                        /* Gambar Produk */
                        $filename            = $userfile;
                        $file_basename       = substr($filename, 0, strripos($filename, '.')); // get file extention
                        $file_ext            = substr($filename, strripos($filename, '.')); // get file name extension
                        $filesize            = $userfile_size; //get size
                        $allowed_file_types  = array('.png','.jpg','.jpeg');

                        /* Begin Upload Gambar Produk  */
                        if(in_array(strtolower($file_ext),$allowed_file_types))
                        {
                            //Set new filename
                            $file_img   = 'PRODUK_GAMBAR_'. $model_produk->kode_produk . '_' . date('Y-m-d') . $file_ext;

                            if(file_exists($old_gambar_produk))
                            {
                                unlink('assets/upload/produk/gambar/' . $model_produk->gambar_produk);

                                if(move_uploaded_file($userfile_tmp_name,  "assets/upload/produk/gambar/" . $file_img))
                                {
                                    $model_produk->gambar_produk = $file_img;

                                    $save_file_produk            = $model_produk->save();
                                    
                                    $this->session->set_flashdata('messages', lang('message_update_success'));
                                    redirect($url_succees . encryptID($kode_produk));
                                }
                            }
                            else
                            {
                                if(move_uploaded_file($userfile_tmp_name,  "assets/upload/produk/gambar/" . $file_img))
                                {
                                    $model_produk->gambar_produk = $file_img;

                                    $save_file_produk            = $model_produk->save();
                                    
                                    $this->session->set_flashdata('messages', lang('message_update_success'));
                                    redirect($url_succees . encryptID($kode_produk));
                                }
                            } 
                        } 
                        elseif(empty($file_basename))
                        {
                            $this->session->set_flashdata('messages', lang('message_update_failed'));
                            redirect($url_error);
                        }
                        elseif($filesize >= 5000000)
                        {
                            $this->session->set_flashdata('messages', lang('message_update_failed'));
                            redirect($url_error);
                        }
                        else
                        {
                            $this->session->set_flashdata('messages', lang('message_update_failed'));
                            unlink($userfile_tmp_name);
                            redirect($url_error);
                        }
                        /* End Upload Gambar Produk  */
                        
                    } 

                    //kondisi apabila hanya brosur yang diubah
                    elseif(!empty($userfile_) && empty($userfile))
                    {
                        /* EXCEL or PDF Daftar Harga */
                        $filename_           = $userfile_;
                        $file_basename_      = substr($filename_, 0, strripos($filename_, '.')); // get file extention
                        $file_ext_           = substr($filename_, strripos($filename_, '.')); // get file name extension
                        $filesize_           = $userfile_size_; //get size
                        $allowed_file_types_ = array('.pdf');

                        /* Begin Upload Brosur Produk  */
                        if(in_array(strtolower($file_ext_),$allowed_file_types_))
                        {   
                            //Set new filename
                            $file_pdf   = 'PRODUK_BROSUR_'. $model_produk->kode_produk . '_' . date('Y-m-d') . $file_ext_;

                            if(file_exists($old_gambar_brosur))
                            {
                                unlink('assets/upload/produk/brosur/' . $model_produk->brosur_produk);

                                if(move_uploaded_file($userfile_tmp_name_,  "assets/upload/produk/brosur/" . $file_pdf))
                                {
                                    $model_produk->brosur_produk = $file_pdf;

                                    $save_file_produk            = $model_produk->save();
                                    
                                    $this->session->set_flashdata('messages', lang('message_update_success'));
                                    redirect($url_succees . encryptID($kode_produk));
                                }
                            } 
                            else 
                            {
                                if(move_uploaded_file($userfile_tmp_name_,  "assets/upload/produk/brosur/" . $file_pdf))
                                {
                                    $model_produk->brosur_produk = $file_pdf;
                                    
                                    $save_file_produk            = $model_produk->save();

                                    $this->session->set_flashdata('messages', lang('message_update_success'));
                                    redirect($url_succees . encryptID($kode_produk));
                                }
                            }

                        }
                        elseif(empty($file_basename_)) 
                        {
                            $this->session->set_flashdata('messages', lang('message_update_failed'));
                            redirect($url_error);
                        }
                        elseif($filesize_ >= 15000000)
                        {
                            $this->session->set_flashdata('messages', lang('message_update_failed'));
                            redirect($url_error);
                        }
                        else
                        {
                            $this->session->set_flashdata('messages', lang('message_update_failed'));
                            unlink($userfile_tmp_name_);
                            redirect($url_error);
                        }
                        /* End Upload Brosur Produk  */
                    }

                    //kondisi apabila gambar produk dan brosur produk yang diubah
                    elseif(!empty($userfile) && !empty($userfile_))
                    {
                         /* Gambar Produk */
                        $filename            = $userfile;
                        $file_basename       = substr($filename, 0, strripos($filename, '.')); // get file extention
                        $file_ext            = substr($filename, strripos($filename, '.')); // get file name extension
                        $filesize            = $userfile_size; //get size
                        $allowed_file_types  = array('.png','.jpg','.jpeg');
                        
                        /* EXCEL or PDF Daftar Harga */
                        $filename_           = $userfile_;
                        $file_basename_      = substr($filename_, 0, strripos($filename_, '.')); // get file extention
                        $file_ext_           = substr($filename_, strripos($filename_, '.')); // get file name extension
                        $filesize_           = $userfile_size_; //get size
                        $allowed_file_types_ = array('.pdf');

                        if(in_array(strtolower($file_ext),$allowed_file_types) && in_array(strtolower($file_ext_),$allowed_file_types_) && $filesize <= 5000000 && $filesize_ <= 15000000) 
                        {

                            //Set new filename
                            $file_img   = 'PRODUK_GAMBAR_'. ms_produk::max('kode_produk') . '_' . date('Y-m-d') . $file_ext;
                            $file_pdf   = 'PRODUK_BROSUR_'. ms_produk::max('kode_produk') . '_' . date('Y-m-d') . $file_ext_;


                            //Condition Error If File Exist
                            if(file_exists("assets/upload/produk/gambar/" . $file_img) && file_exists("assets/upload/produk/brosur/" . $file_pdf) ) 
                            {
                                $this->session->set_flashdata('messages', lang('message_save_failed'));
                                redirect($url_error);
                            } 
                            else 
                            {
                                if(move_uploaded_file($userfile_tmp_name,  "assets/upload/produk/gambar/" . $file_img) && move_uploaded_file($userfile_tmp_name_,  "assets/upload/produk/brosur/" . $file_pdf)) 
                                {
                                    
                                    $model_produk->gambar_produk = $file_img;
                                    $model_produk->brosur_produk = $file_pdf;
                                    
                                    $save_file_produk            = $model_produk->save();
                                }
                                
                                $this->session->set_flashdata('messages', lang('message_save_success'));
                                redirect($url_succees . encryptID($kode_produk));
                            } 
                        } 
                        elseif(empty($file_basename) && empty($file_basename_)) 
                        {
                            $this->session->set_flashdata('messages', lang('message_save_success'));
                            redirect($url_error);
                        } 
                        elseif ($filesize >= 5000000 && $filesize_ >= 15000000) 
                        {
                            $this->session->set_flashdata('messages', lang('message_save_failed'));
                            redirect($url_error);
                        } else 
                        {
                            $this->session->set_flashdata('messages', lang('message_save_failed'));
                            unlink($userfile_tmp_name);
                            redirect($url_error);
                        }
                    }
                    /* END Save data file*/ 
                    else
                    {
                        $this->session->set_flashdata('messages', lang('message_save_success'));
                        if(!empty($userfile_tmp_name_))
                        {
                            unlink($userfile_tmp_name_);
                        }
                        // unlink($userfile_tmp_name_);
                        redirect($url_succees . encryptID($kode_produk));
                    } 
                }
            // } 
            // else 
            // {
            //     $this->session->set_flashdata('message_error', lang('message_data_exist'));
            //     redirect($url_succees . encryptID(ms_produk::max('kode_produk')));
            // }
        }
    }
    
    /*
    * Validate urutan berita
    * @param urutan berita
    * @return boolean
    **/
    function ajax_check_urutan()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") 
        {
            $id                    = decryptID($this->input->post('id_'));
            $urutan_produk_terbaik = $this->input->post('urutan_produk_terbaik');
            $result                = ms_produk::where('urutan_produk_terbaik',$urutan_produk_terbaik)->first();

            if($urutan_produk_terbaik)
            {
                if ($result) 
                {
                    if ($id) 
                    {
                        if ($id == $result->kode_produk) 
                        {
                            echo 'true';
                        } 
                        else 
                        {
                            echo 'false';
                        }
                    } 
                    else 
                    {
                        echo 'false';
                    }
                } 
                else 
                {
                    echo 'true';
                }
            }
            else
            {
                echo 'false';
            }
        
        }    
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
         if ($this->input->is_ajax_request()) 
        {  
            $url                = site_url() . $this->site;
            $id                 = $this->input->get("id");
            $kode_produk        = decryptID($id);
            
            $ms_produk          = ms_produk::where("kode_produk",$kode_produk)->first();

            if(!empty($ms_produk))
            {
                $user                     = $this->ion_auth->user()->row();
                
                $ms_produk->dihapus               = 'T';
                $ms_produk->dihapus_oleh          = $user->first_name . ' ' . $user->last_name;
                $ms_produk->tanggal_hapus         = date('Y-m-d H:i:s');
                $ms_produk->produk_terbaik        = 'F';
                $ms_produk->urutan_produk_terbaik = null;
                $ms_produk->status_tampil         = 'F';
                $delete                           = $ms_produk->save();

                if($delete)
                {
                    /* Array for write log */
                    $produk_old             = ms_produk::where('kode_produk',$ms_produk->kode_produk)->first();

                    /* Write Log */
                    $data_notif = array(
                                        "Kode Produk"          => $ms_produk->kode_produk,
                                        "Nama Produk"          => $ms_produk->nama_produk,
                                        );

                    $message = "Menghapus produk " . $ms_produk->nama_produk;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * View data from table:
    * @param Id
    * @return page index
    **/
    function view($id)
    {
        $data['back'] = site_url() . $this->site;

        $kode_produk = decryptID($id);
        $ms_produk   = ms_produk::where('kode_produk',$kode_produk)->first();

        if(!empty($ms_produk)) {    
            $data['getImage']   = site_url() . $this->site . '/getImage/';
            $data['path_image'] = site_url() . 'assets/upload/prouk/';

            $data['produk']     = ms_produk::join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')
                                ->where('ms_produk.kode_produk',$kode_produk)
                                ->where('ms_produk.dihapus','F')
                                ->first();

            $data['harga_otr']  = ms_harga_otr::where('ms_harga_otr.kode_produk',$kode_produk)->get();
            
        } else {
            redirect(site_url() . $this->site);
        }

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    function getImage($kode_produk)
    {
       $ms_gambar_produk = ms_gambar_produk::where('kode_produk', decryptID($kode_produk))->get();

       echo json_encode($ms_gambar_produk);
    }


    /* -------------- Detail ----------------------*/


    /**
    * Direct to page fitur data
    * @return page
    **/
    function detail($id)
    {
        $kode_produk        = decryptID($id);

        $data['messages'] = $this->session->flashdata('messages');

        /* Button Action */
        $data['url_finish']     = site_url() . $this->site;
        $data['url']            = site_url() . $this->site . '/detail' . '/' . $id;
        $data['header_url']     = site_url() . $this->site . '/header' . '/' . $id;
        $data['varian_url']     = site_url() . $this->site . '/varian' . '/' . $id;
        $data['delete_varian']  = site_url() . $this->site . '/delete_varian' . '/' . $id;
        $data['back_fitur_url'] = site_url() . $this->site . '/back_fitur' . '/' . $id;
        $data['fitur_url']      = site_url() . $this->site . '/fitur' . '/' . $id;
        $data['delete_fitur']   = site_url() . $this->site . '/delete_fitur' . '/' . $id;
        $data['video_url']      = site_url() . $this->site . '/video' . '/' . $id;
        $data['delete_video']   = site_url() . $this->site . '/delete_video' . '/' . $id;
        $data['performa_url']   = site_url() . $this->site . '/performa' . '/' . $id;
        $data['delete_performa']= site_url() . $this->site . '/delete_performa' . '/' . $id;
        $data['highlight_url']  = site_url() . $this->site . '/highlight' . '/' . $id;
        $data['delete_highlight'] = site_url() . $this->site . '/delete_highlight' . '/' . $id;
        $data['posisi_url']     = site_url() . $this->site . '/posisi' . '/' . $id;
        $data['status_url']     = site_url() . $this->site . '/selesai' . '/' . $id;

        $data['header_produk']     = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '1')->first();

        $data['varian_produk']     = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '2')->get();

        $data['back_fitur_produk'] = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '3')->first();
        $data['fitur_produk']      = ms_produk_fitur::where('ms_produk_fitur.kode_produk',$kode_produk)->get();

        $data['video_produk']      = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '4')->first();

        $data['performa_produk']   = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '5')->first();

        $data['highlight_produk']  = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '6')->first();

        $data['detail']       = ms_produk::where('ms_produk.dihapus', 'F')->where('ms_produk.kode_produk', $kode_produk)->first();

        $data['harga_otr']  = ms_harga_otr::where('ms_harga_otr.kode_produk',$kode_produk)->get();

        $data['header_urutan']     = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '1')->first();
        $data['varian_urutan']     = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '2')->groupBy('tbl_konten_produk.kode_produk')->first();
        $data['fitur_urutan']      = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '3')->first();
        $data['video_urutan']      = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '4')->first();
        $data['performa_urutan']   = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '5')->first();
        $data['highlight_urutan']  = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '6')->first();

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save header
    * @return page
    **/
    function header($id)
    {
        $kode_produk        = decryptID($id);
        $today              = date('d-m-Y');

        $file_name          = $_FILES["gambar_header"]["name"];
        $file_basename      = substr($file_name, 0, strripos($file_name, '.')); // get file extention
        $file_ext           = substr($file_name, strripos($file_name, '.')); // get file name
        $file_size          = $_FILES["gambar_header"]["size"];
        $file_tmp_name      = $_FILES["gambar_header"]["tmp_name"];
        $allowed_file_types = array('.jpg','.jpeg','.png');

        if (in_array(strtolower($file_ext),$allowed_file_types) && $file_size <= 5000000)
        {
            $urutan_konten = "1";
            $cek = tbl_konten_produk::whereRaw("tbl_konten_produk.kode_produk = '".$kode_produk."' AND tbl_konten_produk.kode_konten = '1'")->first();
            if (!empty($cek)) {
                $urutan_konten = $cek->urutan_konten;
                if (file_exists('assets/upload/produk/header/'.$cek->isi_konten)) {
                     unlink('assets/upload/produk/header/'.$cek->isi_konten);
                }
                $cek->delete();
             } 

            // Rename file
            $newfilename  = 'PRODUK_HEADER_'. $kode_produk . '_'. $today . $file_ext;
            $locationfile = 'assets/upload/produk/header/' . $newfilename;
            if(move_uploaded_file($file_tmp_name, $locationfile)){
                    
                $model = new tbl_konten_produk;

                $model->isi_konten    = $newfilename;
                $model->urutan_konten = $urutan_konten;
                $model->kode_produk   = $kode_produk;
                $model->kode_konten   = "1";

                $save = $model->save();

                if($save){
                    //echo "File uploaded successfully.";  
                    // echo '<img src="../../../'.$locationfile.'" width="100%" height="100%">'; 
                    $this->session->set_flashdata('message_success', lang('message_save_success')); 
                }
            }        
        }
        elseif (empty($file_basename))
        {   
            // file selection error
            // echo "Please select a file to upload.";
            $this->session->set_flashdata('message_error', lang('message_save_error'));
        } 
        elseif ($file_size >= 5000000)
        {   
            // file size error
            // echo "The file you are trying to upload is too large.";
            $this->session->set_flashdata('message_error', lang('message_save_error'));
        }
        else
        {
            // file type error
            //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
            $this->session->set_flashdata('message_error', lang('message_save_error'));
            unlink($_FILES["file"]["tmp_name"]);
        }

    }

    /**
    * Save varian
    * @return page
    **/
    function varian($id)
    {
        $kode_produk        = decryptID($id);
        $today              = date('d-m-Y');
        $url                = site_url() . $this->site . '/detail' . '/' . $id;

        $file_name          = $_FILES["userFile"]["name"];
        $file_basename      = substr($file_name, 0, strripos($file_name, '.')); // get file extention
        $file_ext           = substr($file_name, strripos($file_name, '.')); // get file name
        $file_size          = $_FILES["userFile"]["size"];
        $file_tmp_name      = $_FILES["userFile"]["tmp_name"];
        $allowed_file_types = array('.jpg','.jpeg','.png');


        if (in_array(strtolower($file_ext),$allowed_file_types) && $file_size <= 5000000)
        {
            $urutan_konten = "2";
            $cek = tbl_konten_produk::whereRaw("tbl_konten_produk.kode_produk = '".$kode_produk."' AND tbl_konten_produk.kode_konten = '2'")->groupBy('tbl_konten_produk.kode_produk')->first();
            if (!empty($cek)) {
                $urutan_konten = $cek->urutan_konten;
            }

            /* Checking folder IDUSER in folder content */
            $folder_content_user = './assets/upload/produk/varian_warna/'.$kode_produk;
            if (!file_exists($folder_content_user)) 
            {
                mkdir($folder_content_user, 0777);
            }
            /* End Checking */

            // Rename file
            $newfilename  = 'PRODUK_VARIAN-WARNA_'. $kode_produk . '_'. $today . '_' . uniqid() . $file_ext;
            $locationfile = 'assets/upload/produk/varian_warna/'. $kode_produk .'/'. $newfilename;
            if(move_uploaded_file($file_tmp_name, $locationfile)){
                    
                $model = new tbl_konten_produk;

                $model->isi_konten    = $newfilename;
                $model->urutan_konten = $urutan_konten;
                $model->kode_produk   = $kode_produk;
                $model->kode_konten   = "2";

                $save = $model->save();

                if($save){
                    //echo "File uploaded successfully.";  
                    $status = array('status' => 'success','message' => lang('message_save_success') . ' ' . $file_name, 'url' => $url);
                }
            }        
        }
        elseif (empty($file_basename))
        {   
            // file selection error
            // echo "Please select a file to upload.";
            $status = array('status' => 'error', 'message' => lang('message_save_failed'));
        } 
        elseif ($file_size >= 5000000)
        {   
            // file size error
            // echo "The file you are trying to upload is too large.";
            $status = array('status' => 'error', 'message' => lang('message_save_failed'));
        }
        else
        {
            // file type error
            //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
            $status = array('status' => 'error', 'message' => lang('message_save_failed'));
            unlink($_FILES["file"]["tmp_name"]);
        }
        
        $data         = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete_varian($id_produk)
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Get Data Post */
            $id        = $this->input->post('id'); 

            /* check in table available or not */
            $model   = tbl_konten_produk::where('tbl_konten_produk.kode_konten_produk',$id)->first();
            
            if(!empty($model))
            {
                if(file_exists("assets/upload/produk/varian_warna/". $model->kode_produk ."/" . $model->isi_konten))
                {
                    unlink("assets/upload/produk/varian_warna/". $model->kode_produk ."/" . $model->isi_konten);
                }

                $delete = tbl_konten_produk::where('tbl_konten_produk.kode_konten_produk',$id)->delete();

                if($delete)
                {
                    $status = array('status' => 'success','message' => lang('message_delete_success'));
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'));
                }          
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'));
            }

            $data         = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Save highlight
    * @return page
    **/
    function highlight($id)
    {
        $kode_produk        = decryptID($id);
        $today              = date('d-m-Y');

        $file_name          = $_FILES["gambar_highlight"]["name"];
        $file_basename      = substr($file_name, 0, strripos($file_name, '.')); // get file extention
        $file_ext           = substr($file_name, strripos($file_name, '.')); // get file name
        $file_size          = $_FILES["gambar_highlight"]["size"];
        $file_tmp_name      = $_FILES["gambar_highlight"]["tmp_name"];
        $allowed_file_types = array('.jpg','.jpeg','.png');

        if (in_array(strtolower($file_ext),$allowed_file_types) && $file_size <= 5000000)
        {
            $urutan_konten = "3";
            $cek = tbl_konten_produk::whereRaw("tbl_konten_produk.kode_produk = '".$kode_produk."' AND tbl_konten_produk.kode_konten = '6'")->first();
            if (!empty($cek)) {
                $urutan_konten = $cek->urutan_konten;
                if (file_exists('assets/upload/produk/highlight/'.$cek->isi_konten)) {
                     unlink('assets/upload/produk/highlight/'.$cek->isi_konten);
                }
                $cek->delete();
             } 

            // Rename file
            $newfilename  = 'PRODUK_HIGHLIGHT_'. $kode_produk . '_'. $today . $file_ext;
            $locationfile = 'assets/upload/produk/highlight/' . $newfilename;
            if(move_uploaded_file($file_tmp_name, $locationfile)){
                    
                $model = new tbl_konten_produk;

                $model->isi_konten    = $newfilename;
                $model->urutan_konten = $urutan_konten;
                $model->kode_produk   = $kode_produk;
                $model->kode_konten   = "6";

                $save = $model->save();

                if($save){
                    //echo "File uploaded successfully.";  
                    // echo '<img src="../../../'.$locationfile.'" width="100%" height="100%">'; 
                    $this->session->set_flashdata('message_success', lang('message_save_success')); 
                }
            }        
        }
        elseif (empty($file_basename))
        {   
            // file selection error
            // echo "Please select a file to upload.";
            $this->session->set_flashdata('message_error', lang('message_save_error'));
        } 
        elseif ($file_size >= 5000000)
        {   
            // file size error
            // echo "The file you are trying to upload is too large.";
            $this->session->set_flashdata('message_error', lang('message_save_error'));
        }
        else
        {
            // file type error
            //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
            $this->session->set_flashdata('message_error', lang('message_save_error'));
            unlink($_FILES["file"]["tmp_name"]);
        }

    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete_highlight($id_produk)
    {
        if ($this->input->is_ajax_request()) 
        {   

            /* Get Data Post */
            $id        = $this->input->post('id'); 

            /* check in table available or not */
            $model   = tbl_konten_produk::where('kode_konten_produk',$id)->first();

            if(!empty($model))
            {
                if(file_exists("assets/upload/produk/highlight/" . $model->isi_konten))
                {
                    unlink("assets/upload/produk/highlight/" . $model->isi_konten);
                }
                $delete = tbl_konten_produk::where('kode_konten_produk',$id)->delete();
                
                if($delete)
                {
                    $status = array('status' => 'success','message' => lang('message_delete_success'));
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'));
                }          
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'));
            }

            $data         = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Save back fitur
    * @return page
    **/
    function back_fitur($id)
    {
        
        $kode_produk        = decryptID($id);
        $today              = date('d-m-Y');

        $file_name          = $_FILES["back_fitur"]["name"];
        $file_basename      = substr($file_name, 0, strripos($file_name, '.')); // get file extention
        $file_ext           = substr($file_name, strripos($file_name, '.')); // get file name
        $file_size          = $_FILES["back_fitur"]["size"];
        $file_tmp_name      = $_FILES["back_fitur"]["tmp_name"];
        $allowed_file_types = array('.jpg','.jpeg','.png');

        if (in_array(strtolower($file_ext),$allowed_file_types) && $file_size <= 5000000)
        {
            $urutan_konten = "4";
            $cek = tbl_konten_produk::whereRaw("tbl_konten_produk.kode_produk = '".$kode_produk."' AND tbl_konten_produk.kode_konten = '3'")->first();
            if (!empty($cek)) {
                $urutan_konten = $cek->urutan_konten;
                if (file_exists('assets/upload/produk/fitur/'.$cek->isi_konten)) {
                     unlink('assets/upload/produk/fitur/'.$cek->isi_konten);
                }
                $cek->delete();
             } 

            // Rename file
            $newfilename  = 'PRODUK_FITUR_'. $kode_produk . '_'. $today . $file_ext;
            $locationfile = 'assets/upload/produk/fitur/' . $newfilename;
            if(move_uploaded_file($file_tmp_name, $locationfile)){
                    
                $model = new tbl_konten_produk;

                $model->isi_konten    = $newfilename;
                $model->urutan_konten = $urutan_konten;
                $model->kode_produk   = $kode_produk;
                $model->kode_konten   = "3";

                $save = $model->save();

                if($save){
                    //echo "File uploaded successfully.";  
                    $this->session->set_flashdata('message_success', lang('message_save_success'));
                }
            }        
        }
        elseif (empty($file_basename))
        {   
            // file selection error
            // echo "Please select a file to upload.";
            $this->session->set_flashdata('message_error', lang('message_save_error'));
        } 
        elseif ($file_size >= 5000000)
        {   
            // file size error
            // echo "The file you are trying to upload is too large.";
            $this->session->set_flashdata('message_error', lang('message_save_error'));
        }
        else
        {
            // file type error
            //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
            $this->session->set_flashdata('message_error', lang('message_save_error'));
            unlink($_FILES["file"]["tmp_name"]);
        }

    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function fitur($id_produk)
    {
        $url           = site_url() . $this->site . '/detail' . '/' . $id_produk;
        $kode_produk   = decryptID($id_produk);

        /* Get Data Post */
        $id        = $this->input->post('id'); 
        $coorX     = $this->input->post('cX');
        $coorY     = $this->input->post('cY');
        $nama      = ucwords($this->input->post('nama_fitur'));
        // $deskripsi = $this->input->post('deskripsi_fitur');
        $posisi    = $coorX .','. $coorY;

        /* Save Fitur */
        if (empty($id)) {

            $filename           = $_FILES["gambar_fitur"]["name"];
            $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext           = substr($filename, strripos($filename, '.')); // get file name
            $filesize           = $_FILES["gambar_fitur"]["size"];
            $allowed_file_types = array('.jpg','.jpeg','.png','.gif');

            if (in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 5000000)
            {
                // Rename file
                $newfilename = 'FITUR_'. time() . '_'. md5($filename) . $file_ext;
                if (file_exists("assets/upload/fitur/" . $newfilename))
                {
                    // file already exists error
                    // echo "You have already uploaded this file.";
                    $status = array('status' => 'error', 'message' => lang('message_save_failed') . ' ' . $filename);
                }
                else
                {       
                    if(move_uploaded_file($_FILES["gambar_fitur"]["tmp_name"], "assets/upload/fitur/" . $newfilename)){
                        
                        $model = new ms_produk_fitur;

                        $model->kode_produk     = $kode_produk;
                        $model->posisi          = $posisi;
                        $model->nama_fitur      = $nama;
                        // $model->deskripsi_fitur = $deskripsi;
                        $model->gambar_fitur    = $newfilename;

                        $save = $model->save();

                        if($save){
                            //echo "File uploaded successfully.";  
                            $this->session->set_flashdata('message_success', lang('message_save_success'));
                        }
                    }
                }        
            }
            elseif (empty($file_basename))
            {   
                // file selection error
                // echo "Please select a file to upload.";
                $this->session->set_flashdata('message_error', lang('message_save_error'));
            } 
            elseif ($filesize >= 5000000)
            {   
                // file size error
                // echo "The file you are trying to upload is too large.";
                $this->session->set_flashdata('message_error', lang('message_save_error'));
            }
            else
            {
                // file type error
                //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
                unlink($_FILES["gambar_fitur"]["tmp_name"]);
                $this->session->set_flashdata('message_error', lang('message_save_error'));
            }

        }

        /* Update Fitur */
        else{
            $model = ms_produk_fitur::where('kode_produk_fitur',$id)->first();

            if(!empty($_FILES))
            {
                $filename           = $_FILES["gambar_fitur"]["name"];
                $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
                $file_ext           = substr($filename, strripos($filename, '.')); // get file name
                $filesize           = $_FILES["gambar_fitur"]["size"];
                $allowed_file_types = array('.jpg','.jpeg','.png','.gif');  

                if (in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 5000000)
                {   
                    // Rename file
                    $newfilename = 'FITUR_'. time() . '_'. md5($filename) . $file_ext;
                    if (file_exists("assets/upload/fitur/" . $newfilename))
                    {
                        // file already exists error
                        // echo "You have already uploaded this file.";
                        $status = array('status' => 'error', 'message' => lang('message_update_failed') . ' ' . $filename);
                    }
                    else
                    {       
                        if(move_uploaded_file($_FILES["gambar_fitur"]["tmp_name"], "assets/upload/fitur/" . $newfilename)){
                            
                            /* Delete file old */
                            if($model->gambar_fitur != null){
                                if(file_exists("assets/upload/fitur/" . $model->gambar_fitur))
                                {
                                    unlink("assets/upload/fitur/" . $model->gambar_fitur);
                                }
                            }

                            /* Initialize Data */
                            $model->nama_fitur      = $nama;
                            // $model->deskripsi_fitur = $deskripsi;
                            $model->gambar_fitur    = $newfilename;
                            
                            $save = $model->save();

                            if($save){
                                $this->session->set_flashdata('message_success', lang('message_save_success'));
                            }
                        }
                    }
                }
                elseif (empty($file_basename))
                {   
                    // file selection error
                    // echo "Please select a file to upload.";
                    $this->session->set_flashdata('message_error', lang('message_save_error'));
                } 
                elseif ($filesize >= 5000000)
                {   
                    // file size error
                    // echo "The file you are trying to upload is too large.";
                    $this->session->set_flashdata('message_error', lang('message_save_error'));
                }
                else
                {
                    // file type error
                    //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
                    $this->session->set_flashdata('message_error', lang('message_save_error'));
                    unlink($_FILES["gambar_fitur"]["tmp_name"]);
                }
            }
            else
            {
                /* Initialize Data */
                $model->nama_fitur      = $nama;
                // $model->deskripsi_fitur = $deskripsi;

                $save = $model->save();

                if($save){
                    $this->session->set_flashdata('message_success', lang('message_save_success'));          
                }
            }
        }
        redirect($url."#m_portlet_tools_3");
        // $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete_fitur($id_produk)
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Get Data Post */
            $id        = $this->input->post('id'); 

            /* check in table available or not */
            $model   = ms_produk_fitur::where('kode_produk_fitur',$id)->first();

            if(!empty($model))
            {
                if(file_exists("assets/upload/fitur/" . $model->gambar_fitur))
                {
                    unlink("assets/upload/fitur/" . $model->gambar_fitur);
                }
                $delete = ms_produk_fitur::where('kode_produk_fitur',$id)->delete();
                
                if($delete)
                {
                    $status = array('status' => 'success','message' => lang('message_delete_success'));
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'));
                }          
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'));
            }

            $data         = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Save header
    * @return page
    **/
    function video($id)
    {
        $kode_produk = decryptID($id);
        $isi_konten  = $this->input->post('video');
        $video       = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '4')->first();
        if(empty($video)){
                
            $model = new tbl_konten_produk;

            $model->isi_konten    = $isi_konten;
            $model->urutan_konten = "5";
            $model->kode_produk   = $kode_produk;
            $model->kode_konten   = "4";

            $save = $model->save();

            if($save){
                //echo "File uploaded successfully.";  
                // $embed = str_replace("/watch?v=", "/embed/", $isi_konten);
                // echo '<iframe width="100%" height="500" src="'.$embed.'" frameborder="0" allowfullscreen></iframe>'; 
                // echo '<button type="button" class="btn btn-danger" onclick="deleteVideo('.tbl_konten_produk::max('kode_konten_produk').')">Hapus</button>'; 
                $this->session->set_flashdata('message_success', lang('message_save_success'));
            }
        }else{

            $video->isi_konten    = $isi_konten;
            $save = $video->save();

            if($save){
                //echo "File uploaded successfully.";  
                // $embed = str_replace("/watch?v=", "/embed/", $isi_konten);
                // echo '<iframe width="100%" height="500" src="'.$embed.'" frameborder="0" allowfullscreen></iframe>'; 
                // echo '<button type="button" class="btn btn-danger" onclick="deleteVideo('.$video->kode_konten_produk.')">Hapus</button>'; 
                $this->session->set_flashdata('message_success', lang('message_save_success'));
            }
        }

    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete_video($id_produk)
    {
        if ($this->input->is_ajax_request()) 
        {   

            /* Get Data Post */
            $id        = $this->input->post('id'); 

            /* check in table available or not */
            $model   = tbl_konten_produk::where('kode_konten_produk',$id)->first();

            if(!empty($model))
            {

                $delete = tbl_konten_produk::where('kode_konten_produk',$id)->delete();
                
                if($delete)
                {
                    $status = array('status' => 'success','message' => lang('message_delete_success'));
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'));
                }          
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'));
            }

            $data         = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Save performa
    * @return page
    **/
    function performa($id)
    {
        $kode_produk        = decryptID($id);
        $today              = date('d-m-Y');

        $file_name          = $_FILES["gambar_performa"]["name"];
        $file_basename      = substr($file_name, 0, strripos($file_name, '.')); // get file extention
        $file_ext           = substr($file_name, strripos($file_name, '.')); // get file name
        $file_size          = $_FILES["gambar_performa"]["size"];
        $file_tmp_name      = $_FILES["gambar_performa"]["tmp_name"];
        $allowed_file_types = array('.jpg','.jpeg','.png');

        if (in_array(strtolower($file_ext),$allowed_file_types) && $file_size <= 5000000)
        {
            $urutan_konten = "6";
            $cek = tbl_konten_produk::whereRaw("tbl_konten_produk.kode_produk = '".$kode_produk."' AND tbl_konten_produk.kode_konten = '5'")->first();
            if (!empty($cek)) {
                $urutan_konten = $cek->urutan_konten;
                if (file_exists('assets/upload/produk/performa/'.$cek->isi_konten)) {
                     unlink('assets/upload/produk/performa/'.$cek->isi_konten);
                }
                $cek->delete();
             } 

            // Rename file
            $newfilename  = 'PRODUK_PERFORMA_'. $kode_produk . '_'. $today . $file_ext;
            $locationfile = 'assets/upload/produk/performa/' . $newfilename;
            if(move_uploaded_file($file_tmp_name, $locationfile)){
                    
                $model = new tbl_konten_produk;

                $model->isi_konten    = $newfilename;
                $model->urutan_konten = $urutan_konten;
                $model->kode_produk   = $kode_produk;
                $model->kode_konten   = "5";

                $save = $model->save();

                if($save){
                    //echo "File uploaded successfully.";  
                    // echo '<img src="../../../'.$locationfile.'" width="100%" height="100%">'; 
                    $this->session->set_flashdata('message_success', lang('message_save_success')); 
                }
            }        
        }
        elseif (empty($file_basename))
        {   
            // file selection error
            // echo "Please select a file to upload.";
            $this->session->set_flashdata('message_error', lang('message_save_error'));
        } 
        elseif ($file_size >= 5000000)
        {   
            // file size error
            // echo "The file you are trying to upload is too large.";
            $this->session->set_flashdata('message_error', lang('message_save_error'));
        }
        else
        {
            // file type error
            //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
            $this->session->set_flashdata('message_error', lang('message_save_error'));
            unlink($_FILES["file"]["tmp_name"]);
        }

    }
    
    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete_performa($id_produk)
    {
        if ($this->input->is_ajax_request()) 
        {   

            /* Get Data Post */
            $id        = $this->input->post('id'); 

            /* check in table available or not */
            $model   = tbl_konten_produk::where('kode_konten_produk',$id)->first();

            if(!empty($model))
            {
                if(file_exists("assets/upload/produk/performa/" . $model->isi_konten))
                {
                    unlink("assets/upload/produk/performa/" . $model->isi_konten);
                }
                $delete = tbl_konten_produk::where('kode_konten_produk',$id)->delete();
                
                if($delete)
                {
                    $status = array('status' => 'success','message' => lang('message_delete_success'));
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'));
                }          
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'));
            }

            $data         = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Save posisi
    * @return page
    **/
    function posisi($id)
    {
        $url                = site_url() . $this->site . '/detail' . '/' . $id;
        $kode_produk        = decryptID($id);
        $header_position    = $this->input->post('header_position');
        $varian_position    = $this->input->post('varian_position');
        $highlight_position = $this->input->post('highlight_position');
        $fitur_position     = $this->input->post('fitur_position');
        $video_position     = $this->input->post('video_position');
        $performa_position  = $this->input->post('performa_position');
        $produk            = ms_produk::where('ms_produk.kode_produk', $kode_produk)->first();
        
        if(!empty($produk)){

            if (!empty($header_position)) {
                $header                = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '1')->first();
                $header->urutan_konten = $header_position;
                $save_header = $header->save();
            }

            if (!empty($varian_position)) {
                $varian                = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '2')->get();
                foreach ($varian as $rvarian) {
                    $varian                = tbl_konten_produk::where('tbl_konten_produk.kode_konten_produk', $rvarian->kode_konten_produk)->first();
                    $varian->urutan_konten = $varian_position;
                    $save_varian = $varian->save();
                }
            }

            if (!empty($highlight_position)) {
                $highlight                = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '6')->first();
                $highlight->urutan_konten = $highlight_position;
                $save_highlight = $highlight->save();
            }

            if (!empty($fitur_position)) {
                $fitur                = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '3')->first();
                $fitur->urutan_konten = $fitur_position;
                $save_fitur = $fitur->save();
            }

            if (!empty($video_position)) {
                $video                = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '4')->first();
                $video->urutan_konten = $video_position;
                $save_video = $video->save();
            }

            if (!empty($performa_position)) {
                $performa                = tbl_konten_produk::where('tbl_konten_produk.kode_produk', $kode_produk)->where('tbl_konten_produk.kode_konten', '5')->first();
                $performa->urutan_konten = $performa_position;
                $save_performa = $performa->save();
            }

            //echo "File uploaded successfully."; 
            $this->session->set_flashdata('message_success', lang('message_save_success')); 
            redirect($url."#m_portlet_tools_8");
        }else{
            $this->session->set_flashdata('message_error', lang('message_save_error'));
            redirect($url."#m_portlet_tools_8");
        }

    }

    /**
    * Save selesai
    * @return page
    **/
    function selesai($id)
    {
        $url_succees   = site_url() . $this->site;
        $url_error     = site_url() . $this->site . '/detail' . '/' . $id;
        $kode_produk   = decryptID($id);
        $status_tampil = $this->input->post('status_tampil');
        $produk          = ms_produk::where('ms_produk.kode_produk', $kode_produk)->first();
        
        if(!empty($produk)){

            $produk->status_tampil    = $status_tampil;
            $save = $produk->save();

            if($save){
                //echo "File uploaded successfully."; 
                $this->session->set_flashdata('message_success', lang('message_save_success')); 
                redirect($url_succees);
            }
        }else{
            $this->session->set_flashdata('message_error', lang('message_save_error'));
            redirect($url_error);
        }

    }


}