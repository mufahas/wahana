<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rekomendasi_produk extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        /* Load Model */
        $this->load->model('select_global_model');
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_rekomendasi_produk";
        $condition    = "ms_rekomendasi_produk.dihapus = 'F'";
        $row          = array('ms_rekomendasi_produk.kode_rekomendasi_produk','ms_rekomendasi_produk.kode_produk', 'ms_kategori_motor.nama_kategori_motor', 'ms_produk.nama_produk');
        $row_search   = array('ms_rekomendasi_produk.kode_rekomendasi_produk','ms_rekomendasi_produk.kode_produk', 'ms_kategori_motor.nama_kategori_motor', 'ms_produk.nama_produk');
        $join         = array('ms_produk' => 'ms_produk.kode_produk = ms_rekomendasi_produk.kode_produk','ms_kategori_motor' => 'ms_kategori_motor.kode_kategori_motor = ms_produk.kode_kategori_motor');
        $order        = "";
        $groupby      = array('ms_rekomendasi_produk.kode_produk');
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * View data from table:
    * @param Id
    * @return page index
    **/
    function view($id)
    {
        $kode_rekomendasi_produk = decryptID($id);
        $ms_rekomendasi_produk  = ms_rekomendasi_produk::join('ms_produk','ms_produk.kode_produk','=','ms_rekomendasi_produk.kode_produk')->join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')->where('ms_rekomendasi_produk.kode_rekomendasi_produk',$kode_rekomendasi_produk)->first();

        if(!empty($ms_rekomendasi_produk))
        {
            $data['back']            = site_url() . $this->site;
            $data['ms_rekomendasi_produk']    = $ms_rekomendasi_produk;
            $data['rekomendasi_1']  = ms_produk::where('ms_produk.kode_produk',$ms_rekomendasi_produk->kode_rekomendasi_1)->first();
            $data['rekomendasi_2']  = ms_produk::where('ms_produk.kode_produk',$ms_rekomendasi_produk->kode_rekomendasi_2)->first();
            $data['rekomendasi_3']  = ms_produk::where('ms_produk.kode_produk',$ms_rekomendasi_produk->kode_rekomendasi_3)->first();
            $data['rekomendasi_4']  = ms_produk::where('ms_produk.kode_produk',$ms_rekomendasi_produk->kode_rekomendasi_4)->first();
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
        
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']               = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($this->input->is_ajax_request()) 
        {   
            /* Url */
            $url_succees         = site_url() . $this->site;
            $url_error           = site_url() . $this->site . '/add';
            
            /* Get Data Post */
            $kode_produk        = decryptID($this->input->post('kode_produk'));
            $kode_rekomendasi_1 = decryptID($this->input->post('kode_rekomendasi_1'));
            $kode_rekomendasi_2 = decryptID($this->input->post('kode_rekomendasi_2'));
            $kode_rekomendasi_3 = decryptID($this->input->post('kode_rekomendasi_3'));
            $kode_rekomendasi_4 = decryptID($this->input->post('kode_rekomendasi_4'));

            /* check in table available or not */
            $ms_rekomendasi_produk  = ms_rekomendasi_produk::where('kode_produk',$kode_produk)->first();

            if(empty($ms_rekomendasi_produk))
            {
                $model = new ms_rekomendasi_produk;

                $model->kode_produk        = $kode_produk;
                $model->kode_rekomendasi_1 = $kode_rekomendasi_1;
                $model->kode_rekomendasi_2 = $kode_rekomendasi_2;
                $model->kode_rekomendasi_3 = $kode_rekomendasi_3;
                $model->kode_rekomendasi_4 = $kode_rekomendasi_4;

                $save = $model->save();

                if($save)
                {
                    /* Write Log */
                    $data_notif = array(
                                        "Kode Produk"        => $kode_produk,
                                        "Kode Rekomendasi 1" => $kode_rekomendasi_1,
                                        "Kode Rekomendasi 2" => $kode_rekomendasi_2,
                                        "Kode Rekomendasi 3" => $kode_rekomendasi_3,
                                        "Kode Rekomendasi 4" => $kode_rekomendasi_4,
                                        );

                    $message = "Berhasil menambahkan master produk rekomendasi " . $kode_produk;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);

                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
                }          
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $kode_rekomendasi_produk = decryptID($id);
        $ms_rekomendasi_produk   = ms_rekomendasi_produk::where('kode_rekomendasi_produk',$kode_rekomendasi_produk)->first();

        if(!empty($ms_rekomendasi_produk))
        {
            /* Button Action */
            $data['action']            = site_url() . $this->site . '/update';

            $data['ms_rekomendasi_produk'] = $ms_rekomendasi_produk;
            $data['produk']         = ms_produk::where('ms_produk.kode_produk',$ms_rekomendasi_produk->kode_produk)->first();
            $data['rekomendasi_1']  = ms_produk::where('ms_produk.kode_produk',$ms_rekomendasi_produk->kode_rekomendasi_1)->first();
            $data['rekomendasi_2']  = ms_produk::where('ms_produk.kode_produk',$ms_rekomendasi_produk->kode_rekomendasi_2)->first();
            $data['rekomendasi_3']  = ms_produk::where('ms_produk.kode_produk',$ms_rekomendasi_produk->kode_rekomendasi_3)->first();
            $data['rekomendasi_4']  = ms_produk::where('ms_produk.kode_produk',$ms_rekomendasi_produk->kode_rekomendasi_4)->first();

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($this->input->is_ajax_request()) 
        {   
            $id            = $this->input->post("id");
            $kode_rekomendasi_produk = decryptID($id);

            /* Url */
            $url_succees = site_url() . $this->site;
            $url_error   = site_url() . $this->site . '/edit/' . $id;

            /* Get Data Post */
            $kode_produk        = decryptID($this->input->post('kode_produk'));
            $kode_rekomendasi_1 = decryptID($this->input->post('kode_rekomendasi_1'));
            $kode_rekomendasi_2 = decryptID($this->input->post('kode_rekomendasi_2'));
            $kode_rekomendasi_3 = decryptID($this->input->post('kode_rekomendasi_3'));
            $kode_rekomendasi_4 = decryptID($this->input->post('kode_rekomendasi_4'));

            /* check in table available or not */
            $ms_rekomendasi_produk = ms_rekomendasi_produk::where('kode_produk',$kode_produk)->whereRaw('kode_rekomendasi_produk != '.$kode_rekomendasi_produk.'')->first();

            if(empty($ms_rekomendasi_produk))
            {
                $model = ms_rekomendasi_produk::where('kode_rekomendasi_produk',$kode_rekomendasi_produk)->first();

                $ms_kota_kabupaten   = ms_kota_kabupaten::where('kode_kota_kabupaten',$model->kode_kota_kabupaten)->first();

                /* Array for write log */
                $data_old = array(
                            "Kode Produk"        => $model->kode_produk,
                            "Kode Rekomendasi 1" => $model->kode_rekomendasi_1,
                            "Kode Rekomendasi 2" => $model->kode_rekomendasi_2,
                            "Kode Rekomendasi 3" => $model->kode_rekomendasi_3,
                            "Kode Rekomendasi 4" => $model->kode_rekomendasi_4,
                            );

                $data_new = array(
                            "Kode Produk"        => $kode_produk,
                            "Kode Rekomendasi 1" => $kode_rekomendasi_1,
                            "Kode Rekomendasi 2" => $kode_rekomendasi_2,
                            "Kode Rekomendasi 3" => $kode_rekomendasi_3,
                            "Kode Rekomendasi 4" => $kode_rekomendasi_4,
                            );
                /* End array for write log */

                /* Initialize Data */
                $model->kode_produk        = $kode_produk;
                $model->kode_rekomendasi_1 = $kode_rekomendasi_1;
                $model->kode_rekomendasi_2 = $kode_rekomendasi_2;
                $model->kode_rekomendasi_3 = $kode_rekomendasi_3;
                $model->kode_rekomendasi_4 = $kode_rekomendasi_4;

                /* Save */
                $save = $model->save();

                if($save)
                {
                    /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message     = 'Memperbarui master rekomendasi produk ' . $kode_produk;
                    $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                    /* End Write Log*/

                    $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_error);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url                     = site_url() . $this->site;
            $id                      = $this->input->get("id");
            $kode_rekomendasi_produk = decryptID($id);
            
            $ms_rekomendasi_produk   = ms_rekomendasi_produk::where("kode_rekomendasi_produk",$kode_rekomendasi_produk)->first();

            if(!empty($ms_rekomendasi_produk))
            {
                $user      = $this->ion_auth->user()->row();

                $ms_rekomendasi_produk->dihapus       = 'T';
                $ms_rekomendasi_produk->tanggal_hapus = date('Y-m-d H:i:s');
                $ms_rekomendasi_produk->dihapus_oleh  = $user->first_name . ' ' . $user->last_name;
                $delete                            = $ms_rekomendasi_produk->save();

                if($delete)
                {
                    /* Write log */
                    $data_notif = array(
                        "Kode Produk"        => $ms_rekomendasi_produk->kode_produk,
                        "Kode Rekomendasi 1" => $ms_rekomendasi_produk->kode_rekomendasi_1,
                        "Kode Rekomendasi 2" => $ms_rekomendasi_produk->kode_rekomendasi_2,
                        "Kode Rekomendasi 3" => $ms_rekomendasi_produk->kode_rekomendasi_3,
                        "Kode Rekomendasi 4" => $ms_rekomendasi_produk->kode_rekomendasi_4,
                    );

                    $message = "Menghapus master rekomendasi produk " . $ms_rekomendasi_produk->kode_produk;
                    $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                    /* End Write Log */

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * select data list:
    * @param Id
    * @return page index
    **/
    function selectRekomendasi(){
        if ($this->input->is_ajax_request()) {
            $kode_produk         = decryptID($this->input->post("kode_produk"));
            $kode_rekomendasi_1  = decryptID($this->input->post("kode_rekomendasi_1"));
            $kode_rekomendasi_2  = decryptID($this->input->post("kode_rekomendasi_2"));
            $kode_rekomendasi_3  = decryptID($this->input->post("kode_rekomendasi_3"));
            $kode_rekomendasi_4  = decryptID($this->input->post("kode_rekomendasi_4"));

            $where_produk = "ms_produk.dihapus = 'F' AND ms_produk.status_tampil = 'T'";
            $where_rekom1 = "ms_produk.dihapus = 'F' AND ms_produk.status_tampil = 'T'";
            $where_rekom2 = "ms_produk.dihapus = 'F' AND ms_produk.status_tampil = 'T'";
            $where_rekom3 = "ms_produk.dihapus = 'F' AND ms_produk.status_tampil = 'T'";
            $where_rekom4 = "ms_produk.dihapus = 'F' AND ms_produk.status_tampil = 'T'";
            // if (!empty($kode_produk) OR !empty($kode_rekomendasi_1) OR !empty($kode_rekomendasi_2) OR !empty($kode_rekomendasi_3) OR !empty($kode_rekomendasi_4) ) {
            //     $where .= " AND ";
            // }
            if (!empty($kode_produk)) {
                $where_rekom1 .= " AND ms_produk.kode_produk != '".$kode_produk."'";
                $where_rekom2 .= " AND ms_produk.kode_produk != '".$kode_produk."'";
                $where_rekom3 .= " AND ms_produk.kode_produk != '".$kode_produk."'";
                $where_rekom4 .= " AND ms_produk.kode_produk != '".$kode_produk."'";
            }            
            if (!empty($kode_rekomendasi_1)) {
                $where_produk .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_1."'";
                $where_rekom2 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_1."'";
                $where_rekom3 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_1."'";
                $where_rekom4 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_1."'";
            }
            if (!empty($kode_rekomendasi_2)) {
                $where_produk .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_2."'";
                $where_rekom1 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_2."'";
                $where_rekom3 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_2."'";
                $where_rekom4 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_2."'";
            }
            if (!empty($kode_rekomendasi_3)) {
                $where_produk .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_3."'";
                $where_rekom1 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_3."'";
                $where_rekom2 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_3."'";
                $where_rekom4 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_3."'";
            }
            if (!empty($kode_rekomendasi_4)) {
                $where_produk .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_4."'";
                $where_rekom1 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_4."'";
                $where_rekom2 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_4."'";
                $where_rekom3 .= " AND ms_produk.kode_produk != '".$kode_rekomendasi_4."'";
            }

            $produk     = ms_produk::whereRaw($where_produk)->get();
            $rekom1     = ms_produk::whereRaw($where_rekom1)->get();
            $rekom2     = ms_produk::whereRaw($where_rekom2)->get();
            $rekom3     = ms_produk::whereRaw($where_rekom3)->get();
            $rekom4     = ms_produk::whereRaw($where_rekom4)->get();
            
            // select produk
            $produk_rekomendasi = ms_rekomendasi_produk::where('ms_rekomendasi_produk.dihapus','F')->get();
            foreach ($produk_rekomendasi as $rekom){
                if ($rekom->kode_produk != $kode_produk) {
                    $produk_rekom[] = $rekom->kode_produk;
                }
            }
            $option_produk       = '<select id="m_select2_1" name="kode_produk" class="form-control m-select2" data-live-search="true">';
            $option_produk       .= '<option value="">-- Pilih Produk --</option>';
            foreach ($produk as $key => $value) {
                if (!empty($produk_rekom)) {
                    if(!in_array($value->kode_produk, $produk_rekom)){
                        if ($value->kode_produk == $kode_produk) {
                            $option_produk   .= '<option value="'.encryptID($value->kode_produk).'" selected>'. $value->nama_produk .'</option>';
                        }else{
                            $option_produk   .= '<option value="'.encryptID($value->kode_produk).'">'. $value->nama_produk .'</option>';
                        }
                    }
                }else{
                    if ($value->kode_produk == $kode_produk) {
                        $option_produk   .= '<option value="'.encryptID($value->kode_produk).'" selected>'. $value->nama_produk .'</option>';
                    }else{
                        $option_produk   .= '<option value="'.encryptID($value->kode_produk).'">'. $value->nama_produk .'</option>';
                    }
                }
            }
            $option_produk        .= '</select>';

            // select rekomendasi 1 
            $option_rekom1       = '<select id="m_select2_2" name="kode_rekomendasi_1" class="form-control m-select2" data-live-search="true">';
            $option_rekom1       .= '<option value="">-- Pilih Rekomendasi 1 --</option>';
            foreach ($rekom1 as $key => $value) {
                if ($value->kode_produk == $kode_rekomendasi_1) {
                    $option_rekom1   .= '<option value="'.encryptID($value->kode_produk).'" selected>'. $value->nama_produk .'</option>';
                }else{
                    $option_rekom1   .= '<option value="'.encryptID($value->kode_produk).'">'. $value->nama_produk .'</option>';
                }
            }
            $option_rekom1        .= '</select>';

            // select rekomendasi 2 
            $option_rekom2       = '<select id="m_select2_3" name="kode_rekomendasi_2" class="form-control m-select2" data-live-search="true">';
            $option_rekom2       .= '<option value="">-- Pilih Rekomendasi 2 --</option>';
            foreach ($rekom2 as $key => $value) {
                if ($value->kode_produk == $kode_rekomendasi_2) {
                    $option_rekom2   .= '<option value="'.encryptID($value->kode_produk).'" selected>'. $value->nama_produk .'</option>';
                }else{
                    $option_rekom2   .= '<option value="'.encryptID($value->kode_produk).'">'. $value->nama_produk .'</option>';
                }
            }
            $option_rekom2        .= '</select>';

            // select rekomendasi 3 
            $option_rekom3       = '<select id="m_select2_4" name="kode_rekomendasi_3" class="form-control m-select2" data-live-search="true">';
            $option_rekom3       .= '<option value="">-- Pilih Rekomendasi 3 --</option>';
            foreach ($rekom3 as $key => $value) {
                if ($value->kode_produk == $kode_rekomendasi_3) {
                    $option_rekom3   .= '<option value="'.encryptID($value->kode_produk).'" selected>'. $value->nama_produk .'</option>';
                }else{
                    $option_rekom3   .= '<option value="'.encryptID($value->kode_produk).'">'. $value->nama_produk .'</option>';
                }
            }
            $option_rekom3        .= '</select>';

            // select rekomendasi 4 
            $option_rekom4       = '<select id="m_select2_5" name="kode_rekomendasi_4" class="form-control m-select2" data-live-search="true">';
            $option_rekom4       .= '<option value="">-- Pilih Rekomendasi 4 --</option>';
            foreach ($rekom4 as $key => $value) {
                if ($value->kode_produk == $kode_rekomendasi_4) {
                    $option_rekom4   .= '<option value="'.encryptID($value->kode_produk).'" selected>'. $value->nama_produk .'</option>';
                }else{
                    $option_rekom4   .= '<option value="'.encryptID($value->kode_produk).'">'. $value->nama_produk .'</option>';
                }
            }
            $option_rekom4        .= '</select>';

            $data['produk'] = $option_produk;
            $data['rekom1'] = $option_rekom1;
            $data['rekom2'] = $option_rekom2;
            $data['rekom3'] = $option_rekom3;
            $data['rekom4'] = $option_rekom4;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * select data list:
    * @param Id
    * @return page index
    **/
    function selectRekomendasi1(){
        if ($this->input->is_ajax_request()) {
            $kode_produk        = decryptID($this->input->post("kode_produk"));
            $kode_rekomendasi_1 = "";

            // saat Edit
            if ($this->input->post("kode_rekomendasi_1") != "" AND $this->input->post("kode_rekomendasi_2") != "" AND $this->input->post("kode_rekomendasi_3") != "" AND $this->input->post("kode_rekomendasi_4") != "") {
                $kode_rekomendasi_1 = decryptID($this->input->post("kode_rekomendasi_1"));
                $kode_rekomendasi_2 = decryptID($this->input->post("kode_rekomendasi_2"));
                $kode_rekomendasi_3 = decryptID($this->input->post("kode_rekomendasi_3"));
                $kode_rekomendasi_4 = decryptID($this->input->post("kode_rekomendasi_4"));
                $produk       = ms_produk::whereRaw("ms_produk.kode_produk != ".$kode_produk." AND ms_produk.kode_produk != ".$kode_rekomendasi_2." AND ms_produk.kode_produk != ".$kode_rekomendasi_3." AND ms_produk.kode_produk != ".$kode_rekomendasi_4)->get();
            // saat Tambah
            }else{
                $produk       = ms_produk::whereRaw("ms_produk.kode_produk != ".$kode_produk)->get();    
            }
            
            $option       = '<select id="m_select2_2" name="kode_rekomendasi_1" class="form-control select2" data-live-search="true">';
            $option       .= '<option>-- Pilih Rekomendasi 1 --</option>';
            foreach ($produk as $key => $value) {
                if ($value->kode_produk == $kode_rekomendasi_1) {
                    $option   .= '<option value="'.encryptID($value->kode_produk).'" selected>'. $value->nama_produk .'</option>';
                }else{
                    $option   .= '<option value="'.encryptID($value->kode_produk).'">'. $value->nama_produk .'</option>';
                }
            }
            $option        .= '</select>';

            echo $option;
            //$this->output->set_content_type('application/json')->set_output(json_encode($option));
        }
    }

    /**
    * select data list:
    * @param Id
    * @return page index
    **/
    function selectRekomendasi2(){
        if ($this->input->is_ajax_request()) {
            $kode_produk         = decryptID($this->input->post("kode_produk"));
            $kode_rekomendasi_1  = decryptID($this->input->post("kode_rekomendasi_1"));
            $kode_rekomendasi_2 = "";

            // saat Edit
            if ($this->input->post("kode_rekomendasi_2") != "" AND $this->input->post("kode_rekomendasi_3") != "" AND $this->input->post("kode_rekomendasi_4") != "") {
                $kode_rekomendasi_2 = decryptID($this->input->post("kode_rekomendasi_2"));
                $kode_rekomendasi_3 = decryptID($this->input->post("kode_rekomendasi_3"));
                $kode_rekomendasi_4 = decryptID($this->input->post("kode_rekomendasi_4"));
                $produk       = ms_produk::whereRaw("ms_produk.kode_produk != ".$kode_produk." AND ms_produk.kode_produk != ".$kode_rekomendasi_1." AND ms_produk.kode_produk != ".$kode_rekomendasi_3." AND ms_produk.kode_produk != ".$kode_rekomendasi_4)->get();
            // saat Tambah
            }else{
                $produk       = ms_produk::whereRaw("ms_produk.kode_produk != ".$kode_produk." AND ms_produk.kode_produk != ".$kode_rekomendasi_1)->get();   
            }
            

            $option       = '<select id="m_select2_3" name="kode_rekomendasi_2" class="form-control select2" data-live-search="true">';
            $option       .= '<option>-- Pilih Rekomendasi 2 --</option>';
            foreach ($produk as $key => $value) {
                if ($value->kode_produk == $kode_rekomendasi_2) {
                    $option   .= '<option value="'.encryptID($value->kode_produk).'" selected>'. $value->nama_produk .'</option>';
                }else{
                    $option   .= '<option value="'.encryptID($value->kode_produk).'">'. $value->nama_produk .'</option>';
                }
            }
            $option        .= '</select>';

            echo $option;
            //$this->output->set_content_type('application/json')->set_output(json_encode($option));
        }
    }

    /**
    * select data list:
    * @param Id
    * @return page index
    **/
    function selectRekomendasi3(){
        if ($this->input->is_ajax_request()) {
            $kode_produk         = decryptID($this->input->post("kode_produk"));
            $kode_rekomendasi_1  = decryptID($this->input->post("kode_rekomendasi_1"));
            $kode_rekomendasi_2  = decryptID($this->input->post("kode_rekomendasi_2"));
            $kode_rekomendasi_3  = "";


            // saat Edit
            if ($this->input->post("kode_rekomendasi_3") != "" AND $this->input->post("kode_rekomendasi_4") != "") {
                $kode_rekomendasi_3 = decryptID($this->input->post("kode_rekomendasi_3"));
                $kode_rekomendasi_4 = decryptID($this->input->post("kode_rekomendasi_4"));
                $produk       = ms_produk::whereRaw("ms_produk.kode_produk != ".$kode_produk." AND ms_produk.kode_produk != ".$kode_rekomendasi_1." AND ms_produk.kode_produk != ".$kode_rekomendasi_2." AND ms_produk.kode_produk != ".$kode_rekomendasi_4)->get();
            // saat Tambah
            }else{
                $produk       = ms_produk::whereRaw("ms_produk.kode_produk != ".$kode_produk." AND ms_produk.kode_produk != ".$kode_rekomendasi_1." AND ms_produk.kode_produk != ".$kode_rekomendasi_2)->get(); 
            }

            $option       = '<select id="m_select2_4" name="kode_rekomendasi_3" class="form-control select2" data-live-search="true">';
            $option       .= '<option>-- Pilih Rekomendasi 3 --</option>';
            foreach ($produk as $key => $value) {
                if ($value->kode_produk == $kode_rekomendasi_3) {
                    $option   .= '<option value="'.encryptID($value->kode_produk).'" selected>'. $value->nama_produk .'</option>';
                }else{
                    $option   .= '<option value="'.encryptID($value->kode_produk).'">'. $value->nama_produk .'</option>';
                }
            }
            $option        .= '</select>';

            echo $option;
            //$this->output->set_content_type('application/json')->set_output(json_encode($option));
        }
    }

    /**
    * select data list:
    * @param Id
    * @return page index
    **/
    function selectRekomendasi4(){
        if ($this->input->is_ajax_request()) {
            $kode_produk         = decryptID($this->input->post("kode_produk"));
            $kode_rekomendasi_1  = decryptID($this->input->post("kode_rekomendasi_1"));
            $kode_rekomendasi_2  = decryptID($this->input->post("kode_rekomendasi_2"));
            $kode_rekomendasi_3  = decryptID($this->input->post("kode_rekomendasi_3"));
            $kode_rekomendasi_4  = "";

            // saat Edit
            if ($this->input->post("kode_rekomendasi_4") != "") {
                $kode_rekomendasi_4 = decryptID($this->input->post("kode_rekomendasi_4"));
                $produk       = ms_produk::whereRaw("ms_produk.kode_produk != ".$kode_produk." AND ms_produk.kode_produk != ".$kode_rekomendasi_1." AND ms_produk.kode_produk != ".$kode_rekomendasi_2." AND ms_produk.kode_produk != ".$kode_rekomendasi_3)->get();
            // saat Tambah
            }else{
                $produk       = ms_produk::whereRaw("ms_produk.kode_produk != ".$kode_produk." AND ms_produk.kode_produk != ".$kode_rekomendasi_1." AND ms_produk.kode_produk != ".$kode_rekomendasi_2." AND ms_produk.kode_produk != ".$kode_rekomendasi_3)->get();
            }

            $option       = '<select id="m_select2_5" name="kode_rekomendasi_4" class="form-control select2" data-live-search="true">';
            $option       .= '<option>-- Pilih Rekomendasi 4 --</option>';
            foreach ($produk as $key => $value) {
                if ($value->kode_produk == $kode_rekomendasi_4) {
                    $option   .= '<option value="'.encryptID($value->kode_produk).'" selected>'. $value->nama_produk .'</option>';
                }else{
                    $option   .= '<option value="'.encryptID($value->kode_produk).'">'. $value->nama_produk .'</option>';
                }
            }
            $option        .= '</select>';

            echo $option;
            //$this->output->set_content_type('application/json')->set_output(json_encode($option));
        }
    }

}