<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_rekomendasi_produk extends Eloquent {

	public $table      = 'ms_rekomendasi_produk';
	public $primaryKey = 'kode_rekomendasi_produk';
	public $timestamps = false;

}