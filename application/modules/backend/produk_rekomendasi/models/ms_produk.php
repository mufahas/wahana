<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_produk extends Eloquent {

	public $table      = 'ms_produk';
	public $primaryKey = 'kode_produk';
	public $timestamps = false;

}