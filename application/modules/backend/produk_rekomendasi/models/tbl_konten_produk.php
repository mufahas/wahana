<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_konten_produk extends Eloquent {

	public $table      = 'tbl_konten_produk';
	public $primaryKey = 'kode_konten_produk';
	public $timestamps = false;

}