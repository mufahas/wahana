<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_produk_fitur extends Eloquent {

	public $table      = 'ms_produk_fitur';
	public $primaryKey = 'kode_produk_fitur';
	public $timestamps = false;

}