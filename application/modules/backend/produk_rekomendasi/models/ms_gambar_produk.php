<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_gambar_produk extends Eloquent {

	public $table      = 'ms_gambar_produk';
	public $primaryKey = 'kode_gambar_produk';
	public $timestamps = false;

}