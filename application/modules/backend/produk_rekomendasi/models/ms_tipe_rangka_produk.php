<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_tipe_rangka_produk extends Eloquent {

	public $table      = 'ms_tipe_rangka_produk';
	public $primaryKey = 'kode_tipe_rangka_produk';
	public $timestamps = false;

}