@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            {!! form_input(array('type' => 'hidden','name' => 'id')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group ">
                        <label>
                            *
                            Judul Kegiatan:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'title', 'class' => 'form-control m-input', 'placeholder' => 'Judul Kegiatan' )) !!}
                    </div>
                    <div class="col-lg-12 form-group" style="padding-top: 20px ">
                        <label>
                            *
                            Lokasi:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'location', 'class' => 'form-control m-input', 'placeholder' => 'Lokasi' )) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        <label>
                            *
                            Photo:
                        </label> 
                        <div class="m-dropzone dropzone m-dropzone--primary dropzone-previews" id="my-awesome-dropzone">
                            <div class="m-dropzone__msg dz-message needsclick">
                                <h3 class="m-dropzone__msg-title">
                                    Jatuhkan berkas di sini atau klik untuk diunggah.
                                </h3>
                                <span class="m-dropzone__msg-desc">
                                    Upload hingga 5 berkas. Berkas maksimal 2 MB
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 form-group" style="padding-top: 20px">
                        <label>
                            *
                            Deskripsi Kegiatan:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'description', 'class' => 'summernote form-control m-input', 'placeholder' => 'Deskripsi' )) !!}
                    </div>
                    <div class="col-lg-12 form-group" style="padding-top: 10px ">
                        <label class="col-form-label">
                            *Waktu Pelaksanaan
                        </label>
                        <div class='input-group date' id='date'>
                            <span class="input-group-addon">
                                <i class="la la-calendar"></i>
                            </span>
                            <input type='text' class="form-control m-input" name="date" readonly placeholder="Pilih Waktu Pelaksanaan Kegiatan" />
                        </div>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>
                            Status Event
                        </label>
                        <div class="m-checkbox-list"> 
                            <label class="m-checkbox">
                                <input type="checkbox" name="dibuka" id="dibuka" value="1"> Dibuka Untuk umum
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_save"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var checkTitle       = '{{$checkTitle}}';
    var url              = "{{$action}}";
    var url_succees      = "{{$url_succees}}";
    var uploadMultiple   = false; 
    var autoProcessQueue = false;
    var maxFilesize      = 2;
    var paramName        = "file";
    var addRemoveLinks   = true;
    var maxFiles         = 5;
    var parallelUploads  = 5;
    var acceptedFiles    = 'image/jpeg,image/png';

    $('.save').attr("disabled",true);
</script>
<script src="{{ base_url() }}assets/backend/js/kegiatan/js/kegiatan.js" type="text/javascript"></script>
@stop