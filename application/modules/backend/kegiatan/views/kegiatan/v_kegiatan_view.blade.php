@extends('backend.default.views.layout.v_layout')

@section('body')

<style type="text/css">
    .m-portlet.m-portlet--creative .m-portlet__head .m-portlet__head-caption .m-portlet__head-title .m-portlet__head-text {
        font-size: 1.2rem;
        font-weight: 600
     }
     .m-portlet .m-portlet__head .m-portlet__head-tools .m-portlet__nav .m-portlet__nav-item .m-portlet__nav-link.m-portlet__nav-link--icon [class^="la-"], .m-portlet .m-portlet__head .m-portlet__head-tools .m-portlet__nav .m-portlet__nav-item .m-portlet__nav-link.m-portlet__nav-link--icon [class*=" la-"] {
        font-size: 2em;
    }
    .m-portlet .m-portlet__head .m-portlet__head-tools .m-portlet__nav .m-portlet__nav-item .m-portlet__nav-link.m-portlet__nav-link--icon i {
        color: #36a3f7;
    }
    .m-portlet.m-portlet--creative .m-portlet__head .m-portlet__head-caption {
        background: #f4f5f8;
        padding-left: 10px;
        padding-top: 10px;
    }
    .m-portlet .m-portlet__head .m-portlet__head-tools{
        background: #f4f5f8
    }
</style>
    <!--begin::Portlet-->
    <div class="m-portlet">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="flaticon-statistics"></i>
                        </span>
                        <h1 class="m-portlet__head-text">
                            <h2>{{$event->judul_event }}</h2>
                        </h1>
                        <h2 class="m-portlet__head-label m-portlet__head-label--danger">
                            <span>
                                Detail {{ get_menu_name()}}
                            </span>
                        </h2>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{$back}}" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-undo"></i>
                            </a>
                            @if($event->status_event == 'A')
                                <span class="m-badge  m-badge--info m-badge--wide">Aktif</span>
                            @else
                                <span class="m-badge  m-badge--danger m-badge--wide">Tidak Aktif</span>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                
                <table class=" table table-bordered" border="1">
                    <tr>
                        <td colspan="2">{!!$event->deskripsi_event!!}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: 500;vertical-align: middle;width: 20%" rowspan="2" align="center" valign="middle"><b>Lokasi dan Waktu Kegiatan</b></td>
                        <td>{{$event->lokasi_event}} </td>
                    </tr>
                    <tr>
                        <td>
                            {{date('d F Y', strtotime($event->tanggal_mulai_event))}} s/d {{date('d F Y', strtotime($event->tanggal_akhir_event))}}, Pukul : {{date('h:i A', strtotime($event->waktu_mulai_event))}} - {{date('h:i A', strtotime($event->waktu_akhir_event))}}
                        </td>
                    </tr>
                </table>
                    
            </div>
        </div>
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var checkTitle = '{{$checkTitle}}';
</script>
<script src="{{ base_url() }}assets/backend/js/kegiatan/js/kegiatan.js" type="text/javascript"></script>
@stop