<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table: tbl_event
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "tbl_event";
        $condition    = "";
        $row          = array('tbl_event.kode_event','tbl_event.judul_event','tbl_event.lokasi_event','tbl_event.tanggal_mulai_event','tbl_event.tanggal_akhir_event','tbl_event.status_event','tbl_event.dibuka_untuk_umum');
        $row_search   = array('tbl_event.kode_event','tbl_event.judul_event','tbl_event.lokasi_event','tbl_event.tanggal_mulai_event','tbl_event.tanggal_akhir_event','tbl_event.status_event','tbl_event.dibuka_untuk_umum');
        $join         = "";
        $order        = array('status_event'=>'ASC');
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['checkTitle']     = site_url() . $this->site . '/ajax_check_title';
        $data['checkEventCode'] = site_url() . $this->site . '/ajax_event_code';
        $data['action']         = site_url() . $this->site . '/save';
        $data['url_succees']    = site_url() . $this->site;

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table: tbl_event
    * @param Post Data
    * @return page index
    **/
    function save()
    {
          
        /* Url */
        $url_succees        = site_url() . $this->site;
        $url_error          = site_url() . $this->site . '/add';
        
        /* Get Data Post */
        $event_date         = explode(' - ', $this->input->post('date'));
        $start              = explode(', ', $event_date[0]); 
        $end                = explode(', ', $event_date[1]);
        $start_date         = date('Y-m-d',strtotime($start[0]));
        $end_date           = date('Y-m-d',strtotime($end[0]));
        $start_time         = date('H:i:s',strtotime($start[1]));
        $end_time           = date('H:i:s',strtotime($end[1]));
        $user               = $this->ion_auth->user()->row();
        $title              = ucwords($this->input->post('title'));
        $location           = $this->input->post('location');
        $dibuka             = $this->input->post('dibuka');
        $description        = $this->input->post('description');
        // $event_code         = strtoupper($this->input->post('event_code'));
        $short_desc         = $this->input->post('short_desc');

        /* check in table available or not */
        // $tbl_event          = tbl_event::where('judul_event',$title)->orWhere('event_code',$event_code)->first();
        $tbl_event          = tbl_event::where('judul_event',$title)->first();
    
        if(empty($tbl_event))
        {
            /* Post Upload File */
            $filename           = $_FILES["file"]["name"];
            $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext           = substr($filename, strripos($filename, '.')); // get file name
            $filesize           = $_FILES["file"]["size"];
            $allowed_file_types = array('.jpg','.jpeg','.png');

            if (in_array($file_ext,$allowed_file_types) && $filesize <= 2000000)
            {
                // Rename file
                $newfilename = 'EVENT_'. time() . '_'. md5($filename) . $file_ext;

                if(file_exists("assets/upload/kegiatan/" . $newfilename))
                {
                    // file already exists error
                    // echo "You have already uploaded this file.";
                    $status = array('status' => 'error', 'message' => lang('message_save_failed') . ' ' . $filename);
                } else {
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], "assets/upload/kegiatan/" . $newfilename))
                    {

                        $model = new tbl_event;

                        if (!empty($dibuka)) {
                            $model->dibuka_untuk_umum     = $dibuka;
                        }

                        $model->judul_event         = $title;
                        $model->judul_event_url     = clean_url(strtolower($title));
                        $model->lokasi_event        = $location;
                        $model->tanggal_mulai_event = $start_date;
                        $model->deskripsi_event     = $description;
                        $model->gambar_event        = $newfilename;
                        $model->tanggal_akhir_event = $end_date;
                        $model->waktu_mulai_event   = $start_time;
                        $model->waktu_akhir_event   = $end_time;
                        $model->dipublikasi_oleh    = $user->first_name.' '.$user->last_name;
                        $model->dipublikasi_oleh    = $user->first_name.' '.$user->last_name;
                        $model->status_event        = 'A';
                        // $model->event_code          = !empty($event_code) ? $event_code : '';
                        $model->short_desc          = $short_desc;

                        $save = $model->save();

                        /* Write Log */
                        $data_notif = array(
                                    "Judul Event"         => $title,
                                    "Judul Event Url"     => clean_url(strtolower($title)),
                                    "Lokasi Event"        => $location,
                                    "Tanggal Mulai Event" => $start_date,
                                    "Tanggal Akhir Event" => $end_date,
                                    "Waktu Mulai Event"   => $start_time,
                                    "Waktu Akhir Event"   => $end_time,
                                    "Deskripsi Event"     => $description,
                                    "Dipublikasikan Oleh" => $user->first_name.' '.$user->last_name
                                            );

                        $message = "Berhasil menambahkan event " . $title;
                        $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                        /* End Write Log */

                        $status = array('status' => 'success','message' => lang('message_save_success'), 'url' => $url_succees);
                    }
                }
            } elseif(empty($file_basename)) {
                // file selection error
                // echo "Please select a file to upload.";
                $status = array('status' => 'error', 'message' => lang('message_save_failed'));
            } elseif($filesize >= 2000000) {
                // file size error
                // echo "The file you are trying to upload is too large.";
                $status = array('status' => 'error', 'message' => lang('message_save_failed'));
            } else {
                // file type error
                //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
                $status = array('status' => 'error', 'message' => lang('message_save_failed'));
                unlink($_FILES["file"]["tmp_name"]);
            }
        } else
        {
            $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));   
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {

        $event_code = decryptID($id);
        $tbl_event  = tbl_event::where('kode_event',$event_code)->first();

        if(!empty($tbl_event))
        {
            /* Button Action */
            $data['checkTitle']     = site_url() . $this->site . '/ajax_check_title';
            $data['checkEventCode'] = site_url() . $this->site . '/ajax_event_code';
            $data['action']         = site_url() . $this->site . '/update';
            $data['event']          = $tbl_event;
            $data['date']           = date('d F Y', strtotime($tbl_event->tanggal_mulai_event)).', '.date('h:i A',strtotime($tbl_event->waktu_mulai_event)).' - '.date('d F Y', strtotime($tbl_event->tanggal_akhir_event)).', '.date('h:i A',strtotime($tbl_event->waktu_akhir_event));
            
            $data['getImage']       = site_url() . $this->site . '/getImage/';
            $data['remove_image']   = site_url() . $this->site . '/remove_image/';
            $data['path_image']     = site_url() . 'assets/upload/kegiatan/';
            $data['url_succees']    = site_url() . $this->site;

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table: tbl_event
    * @param Post Data
    * @return page index
    **/
    function update()
    { 
        $id          = $this->input->post("id");
        $kode_event  = decryptID($id);
        
        /* Url */
        $url_succees = site_url() . $this->site;
        $url_error   = site_url() . $this->site . '/edit/' . $id;
        
        /* Get Data Post */
        $event_date  = explode(' - ', $this->input->post('date'));
        $start       = explode(', ', $event_date[0]); 
        $end         = explode(', ', $event_date[1]);
        $start_date  = date('Y-m-d',strtotime($start[0]));
        $end_date    = date('Y-m-d',strtotime($end[0]));
        $start_time  = date('H:i:s',strtotime($start[1]));
        $dibuka      = $this->input->post('dibuka');
        $end_time    = date('H:i:s',strtotime($end[1]));
        $user        = $this->ion_auth->user()->row();
        $title       = ucwords($this->input->post('title'));
        $location    = $this->input->post('location');
        $description = $this->input->post('description');
        // $event_code  = strtoupper($this->input->post('event_code'));
        $short_desc  = $this->input->post('short_desc');
        $status      = $this->input->post('status_event');
        if (empty($status)) 
        {
            $status_event = 'TA';
        }
        else
        {
            $status_event = 'A';
        }

        /* check in table available or not */
        // $event = tbl_event::where('judul_event',$title)->where('event_code',$event_code)->whereRaw('kode_event != '.$kode_event.'')->first();
        $event = tbl_event::where('judul_event',$title)->whereRaw('kode_event != '.$kode_event.'')->first();
        // echo "<pre>";
        // print_r($event);
        // echo "</pre>";
        // die();
        $model = tbl_event::where('kode_event',$kode_event)->first();

        /* Array for write log */
        $data_old = array
                    (
                        "Judul Event"         => $model->judul_event,
                        "Judul Event Url"     => $model->judul_event_url,
                        "Lokasi Event"        => $model->lokasi_event,
                        "Tanggal Mulai Event" => $model->tanggal_mulai_event,
                        "Tanggal Akhir Event" => $model->tanggal_akhir_event,
                        "Waktu Mulai Event"   => $model->waktu_mulai_event,
                        "Waktu Akhir Event"   => $model->tanggal_akhir_event,
                        "Deskripsi Event"     => $model->deskripsi_event,
                        "Gambar Event"        => $model->gambar_event,
                        "Dipublikasikan Oleh" => $model->dipublikasi_oleh
                    );
        /* End array for write log */

        if(empty($event) && !empty($_FILES))
        {   
            /* Post Upload File */
            $filename           = $_FILES["file"]["name"];
            $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext           = substr($filename, strripos($filename, '.')); // get file name
            $filesize           = $_FILES["file"]["size"];
            $allowed_file_types = array('.jpg','.jpeg','.png');

            if(in_array($file_ext,$allowed_file_types) && $filesize <= 2000000)
            {
                // Rename file
                $newfilename = 'EVENT_'. time() . '_'. md5($filename) . $file_ext;

                // Kondisi Jika File Sudah Ada, Maka Gagal Simpan. Jika Belum Ada, Maka Berhasil Simpan
                if(file_exists("assets/upload/kegiatan/" . $newfilename))
                {
                    // file already exists error
                    // echo "You have already uploaded this file.";
                    $status = array('status' => 'error', 'message' => lang('message_save_failed') . ' ' . $filename);
                } 
                else
                {
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], "assets/upload/kegiatan/" . $newfilename))
                    {

                        if(file_exists("assets/upload/kegiatan/" . $newfilename))
                        {
                            unlink("assets/upload/kegiatan/" . $model->gambar_event);
                        }

                        /* Array for write log */
                        $data_new = array(
                                        "Judul Event"         => $title,
                                        "Judul Event Url"     => clean_url(strtolower($title)),
                                        "Lokasi Event"        => $location,
                                        "Tanggal Mulai Event" => $start_date,
                                        "Tanggal Akhir Event" => $end_date,
                                        "Waktu Mulai Event"   => $start_time,
                                        "Waktu Akhir Event"   => $end_time,
                                        "Gambar Event"        => $newfilename,
                                        "Deskripsi Event"     => $description,
                                        "Dipublikasikan Oleh" => $user->first_name.' '.$user->last_name
                                    );
                        /* End array for write log */

                        $model->judul_event             = $title;
                        $model->judul_event_url         = clean_url(strtolower($title));
                        $model->lokasi_event            = $location;
                        $model->tanggal_mulai_event     = $start_date;
                        $model->tanggal_akhir_event     = $end_date;
                        $model->deskripsi_event         = $description;
                        $model->gambar_event            = $newfilename;
                        $model->waktu_mulai_event       = $start_time;
                        $model->waktu_akhir_event       = $end_time;
                        $model->dipublikasi_oleh        = $user->first_name.' '.$user->last_name;
                        $model->dipublikasi_oleh        = $user->first_name.' '.$user->last_name;
                        $model->status_event            = $status_event;
                        $model->tanggal_terakhir_diubah = date('Y-m-d');
                        $model->dibuka_untuk_umum       = !empty($dibuka) ? $dibuka : 0;
                        // $model->event_code              = $event_code;
                        $model->short_desc              = $short_desc;

                        /* Save */
                        $save = $model->save();

                        /* Write Log */
                        $data_change = array_diff_assoc($data_new, $data_old);
                        $message     = 'Memperbarui kegiatan ' . $title;
                        $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                        /* End Write Log*/

                        $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
                    }
                }
            } 
            elseif(empty($file_basename)) //Jika File Tidak Ada
            {
                $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);  
            }
            elseif ($filesize >= 2000000) 
            {
                $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_save_failed'), 'url' => $url_error);
                unlink($_FILES["file"]["tmp_name"]);
            }
        }
        else if(empty($event) && empty($_FILES))
        {

            $model->judul_event             = $title;
            $model->judul_event_url         = clean_url(strtolower($title));
            $model->lokasi_event            = $location;
            $model->tanggal_mulai_event     = $start_date;
            $model->tanggal_akhir_event     = $end_date;
            $model->deskripsi_event         = $description;
            $model->waktu_mulai_event       = $start_time;
            $model->waktu_akhir_event       = $end_time;
            $model->dipublikasi_oleh        = $user->first_name.' '.$user->last_name;
            $model->dipublikasi_oleh        = $user->first_name.' '.$user->last_name;
            $model->status_event            = $status_event;
            $model->tanggal_terakhir_diubah = date('Y-m-d');
            $model->dibuka_untuk_umum       = !empty($dibuka) ? $dibuka : 0;
            // $model->event_code              = $event_code;
            $model->short_desc              = $short_desc;

            $save                           = $model->save();

            if($save)
            {
                /* Array for write log */
                $data_new = array(
                                "Judul Event"         => $title,
                                "Judul Event Url"     => clean_url(strtolower($title)),
                                "Lokasi Event"        => $location,
                                "Tanggal Mulai Event" => $start_date,
                                "Tanggal Akhir Event" => $end_date,
                                "Waktu Mulai Event"   => $start_time,
                                "Waktu Akhir Event"   => $end_time,
                                "Gambar Event"        => $model->gambar_event,
                                "Deskripsi Event"     => $description,
                                "Dipublikasikan Oleh" => $user->first_name.' '.$user->last_name
                            );
                /* End array for write log */

                $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
            }
        }
        else
        {
            $status = array('status' => 'error', 'message' => lang('message_data_exist'), 'url' => $url_error);
        }
     
        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
        
    }

    /**
    * Direct to page detail
    * @return page
    **/
    function view($id)
    {

        $event_code = decryptID($id);
        $tbl_event  = tbl_event::where('kode_event',$event_code)->first();

        if(!empty($tbl_event))
        {
            /* Button Action */
            $data['checkTitle'] = site_url() . $this->site . '/ajax_check_title';
            $data['action']     = site_url() . $this->site . '/update';
            $data['back']       = site_url() . $this->site;
            $data['event']      = $tbl_event;

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }


    /**
    * Validate title
    * @param title
    * @return boolean
    **/
    function ajax_check_title()
    {
        if ($this->input->is_ajax_request()) 
        { 
            $id     = decryptID($this->input->post('id'));
            $title  = $this->input->post('title');
            $result = tbl_event::where('judul_event',$title)->first();

            if($title)
            {
                if ($result) 
                {
                    if ($id) 
                    {
                        if ($id == $result->kode_event) 
                        {
                            echo 'true';
                        } 
                        else 
                        {
                            echo 'false';
                        }
                    } 
                    else 
                    {
                        echo 'false';
                    }
                } 
                else 
                {
                    echo 'true';
                }
            }
            else
            {
                echo 'false';
            }
        }
    }

    function getImage($kode_event)
    {
       $tbl_event = tbl_event::where('kode_event', decryptID($kode_event))->get();
       echo json_encode($tbl_event);
    }

     function remove_image($kode_event, $gambar_event)
    {
        if (file_exists("assets/upload/kegiatan/" . $gambar_event))
        {
            if(unlink("assets/upload/kegiatan/" . $gambar_event))
            {
                $tbl_event = tbl_event::where('kode_event',decryptID($kode_event))
                                                                ->where('gambar_event',$gambar_event)
                                                                ->first();
                $tbl_event->gambar_event    = null;

                $save_ = $tbl_event->save();

                if(!empty($tbl_event)){
                    $save_;
                }

                $status = array('status' => 'success', 'message' => 'Gambar berhasil dihapus.');
            }else{
                $status = array('status' => 'error', 'message' => 'Gambar gagal dihapus.');
            }
        }
        else
        {
                $tbl_event = tbl_event::where('kode_event',decryptID($kode_event))
                                                                ->where('gambar_event',$gambar_event)
                                                                ->first();
                $tbl_event->gambar_event    = null;

                $save_ = $tbl_event->save();

                if(!empty($tbl_event)){
                    $save_;
                }

            $status = array('status' => 'success', 'message' => 'Gambar berhasil dihapus.');
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    /**
    * Delete data from table: tbl_event
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request())
        {
            $url                = site_url() . $this->site;
            $id                 = $this->input->get("id");
            $kode_event         = decryptID($id);
            
            $event              = tbl_event::where('kode_event', $kode_event)->first();

            if(!empty($event))
            {
                $user   = $this->ion_auth->user()->row();
                
                $delete = $event->delete();

                if($delete)
                {   
                    unlink("assets/upload/kegiatan/" . $event->gambar_event);
                    //Delete Success
                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * Validate event_code
    * @param event_code
    * @return boolean
    **/
    // function ajax_event_code()
    // {
    //     if ($this->input->is_ajax_request()) 
    //     { 
    //         $id         = decryptID($this->input->post('id'));
    //         $event_code = strtoupper($this->input->post('event_code'));
    //         // $result     = tbl_event::whereRaw('event_code LIKE "%'.$event_code.'%"')->first();
            
    //         if(empty($id) && !empty($event_code))
    //         {
    //             $result     = tbl_event::where('event_code',$event_code)->first();
    //         }
    //         else if(!empty($id) && !empty($event_code))
    //         {
    //             $result     = tbl_event::where('event_code','=',$event_code)->whereRaw('kode_event != "'.$id.'"')->first();
    //         }
    //         else
    //         {
    //             $result = '';
    //         }

    //         if(empty($result))
    //         {
    //             $status = array('status' => 'true', 'message' => 'Kode Event Tersedia');
    //         }
    //         else
    //         {
    //             $status = array('status' => 'false', 'message' => 'Kode Event Sudah Digunakan');
    //         }

    //         $data  = $status;
    //         $this->output->set_content_type('application/json')->set_output(json_encode($data));  
    //     }
    // }
}