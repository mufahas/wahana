<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_peserta_event extends Eloquent {

	public $table      = 'tbl_peserta_event';
	public $primaryKey = 'id_peserta_event';
	public $timestamps = false;

}