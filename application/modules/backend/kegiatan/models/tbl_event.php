<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_event extends Eloquent {

	public $table      = 'tbl_event';
	public $primaryKey = 'kode_event';
	public $timestamps = false;

}