@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 ">
                        <label>
                            *
                            Diskripsi Tentang Wahana:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'deskripsi_tentang_wahana', 'class' => 'form-control m-input summernote', 'placeholder' => 'Diskripsi Tentang Wahana', 'value' => $profile->deskripsi_tentang_wahana )) !!}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 ">
                        <label>
                            *
                            Core Value:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'core_value', 'class' => 'form-control m-input summernote', 'placeholder' => 'Core Value' , 'value' => $profile->core_value )) !!}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            *
                            Jumlah Dealer:
                        </label>
                        {!! form_input(array('type' => 'number','name' => 'jumlah_dealer', 'class' => 'form-control m-input', 'placeholder' => 'Jumlah Dealer' , 'value' => $profile->jumlah_dealer )) !!}
                    </div>
                    <div class="col-lg-6 ">
                        <label>
                            *
                            Jumlah Ahass:
                        </label>
                        {!! form_input(array('type' => 'number','name' => 'jumlah_ahass', 'class' => 'form-control m-input', 'placeholder' => 'Jumlah Ahass', 'value' => $profile->jumlah_ahass )) !!}
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_update"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($profile->kode_tentang_kami) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/tentang_kami/profile/js/profile.js" type="text/javascript"></script>
@stop