@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 ">
                        <label>
                            *
                            Visi:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'visi', 'class' => 'form-control m-input summernote', 'placeholder' => 'Visi', 'value' => $profile->visi )) !!}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 ">
                        <label>
                            *
                            Misi:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'misi', 'class' => 'form-control m-input summernote', 'placeholder' => 'Misi' , 'value' => $profile->misi )) !!}
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_update"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($profile->kode_tentang_kami) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/tentang_kami/visi_misi/js/visi_misi.js" type="text/javascript"></script>
@stop