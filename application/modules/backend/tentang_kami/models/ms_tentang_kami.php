<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_tentang_kami extends Eloquent {

	public $table      = 'ms_tentang_kami';
	public $primaryKey = 'kode_tentang_kami';
	public $timestamps = false;

}