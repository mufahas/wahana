<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        /* Button Action */
        $data['action']  = site_url() . $this->site . '/update';
        $data['profile'] = ms_tentang_kami::where('kode_tentang_kami',1)->first();

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Save data to table:ms_tentang_kami
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($this->input->is_ajax_request()) 
        {   
            $user              = $this->ion_auth->user()->row();
            $id                = $this->input->post("id");
            $kode_tentang_kami = decryptID($id);
            
            /* Url */
            $url_succees       = site_url() . $this->site;

            /* Get Data Post */
            $deskripsi_tentang_wahana = $this->input->post("deskripsi_tentang_wahana");
            $core_value               = $this->input->post("core_value");
            $jumlah_dealer            = $this->input->post("jumlah_dealer");
            $jumlah_ahass             = $this->input->post("jumlah_ahass");


            $model = ms_tentang_kami::where('kode_tentang_kami',$kode_tentang_kami)->first();

            /* Array for write log */
            $data_old = array(
                        "Deskripsi Tentang Wahana" => $model->deskripsi_tentang_wahana,
                        "Core Value"               => $model->core_value,
                        "Jumlah Dealer"            => $model->jumlah_dealer,
                        "Jumlah Ahass"             => $model->jumlah_ahass,
                        );

            $data_new = array(
                        "Deskripsi Tentang Wahana" => $deskripsi_tentang_wahana,
                        "Core Value"               => $core_value,
                        "Jumlah Dealer"            => $jumlah_dealer,
                        "Jumlah Ahass"             => $jumlah_ahass,
                        );
            /* End array for write log */

            /* Initialize Data */
            $model->deskripsi_tentang_wahana = $deskripsi_tentang_wahana;
            $model->core_value               = $core_value;
            $model->jumlah_dealer            = $jumlah_dealer;
            $model->jumlah_ahass             = $jumlah_ahass;
            $model->tanggal_terakhir_ubah    = date('Y-m-d H:i:s');
            $model->diubah_oleh              = $user->first_name . ' ' . $user->last_name;

            /* Save */
            $save = $model->save();

            if($save)
            {
                /* Write Log */
                $data_change = array_diff_assoc($data_new, $data_old);
                $message     = 'Memperbarui Profile Wahana';
                $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                /* End Write Log*/

                $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_update_failed'), 'url' => $url_succees);
            }
           

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
}