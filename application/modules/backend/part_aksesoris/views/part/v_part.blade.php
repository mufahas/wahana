@extends('backend.default.views.layout.v_layout')

@section('body')

<link href="{{ base_url() }}assets/default/css/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ base_url() }}assets/default/css/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    {{ get_menu_name() }}
                </h3>
            </div>
        </div>
    </div>

    <div class="m-portlet__body">
        @if(!empty($messages) && $messages == TRUE) 
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show success" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                {{$messages}}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
        @endif
        <!--begin: button  -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-12 order-1 order-xl-2 m--align-right">
                    <!-- {!! $btn["btn_add"] !!} -->
                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>
            </div>
        </div>
        <!--end: button-->

        <!--begin: Datatable -->
        <table class="table table-striped table-bordered table-hover" id="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Berkas Part</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    var loadTable  = '{{$loadTable}}';
    var path_file  = '{{$path_file}}';
    var view_      = '{!! $btn["btn_view"] !!}';
    var edit_      = '{!! $btn["btn_edit"] !!}';
    var delete_    = '{!! $btn["btn_delete"] !!}';
</script>

<script src="{{ base_url() }}assets/default/js/datatables.min.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/default/js/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/backend/js/handler-datatable.js" type="text/javascript"></script>

<script src="{{ base_url() }}assets/backend/js/partaksesoris/table/table-part.js" type="text/javascript"></script>
@stop