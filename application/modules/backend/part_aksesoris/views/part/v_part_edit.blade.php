@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form', 'enctype' => 'multipart/form-data')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            *
                            Upload Berkas Part:
                        </label> 
                        @if(!empty($part_file->nama_part_file))
                        {!! form_input(array('type' => 'file','name' => 'userfile', 'id' => 'userfile_' , 'value' => $part_file->nama_part_file, 'class' => 'form-control m-input', 'accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/pdf', 'style' => 'display: none;')) !!}
                        <a href="{{ $path_file . $part_file->nama_part_file }}">{{$part_file->nama_part_file}}</a>
                        @endif
                        <div>    
                            <button id="btn-image" type="button" class="btn blue btn-sm" onClick="$('#userfile_').click()">
                            <i class="fa fa-upload"></i> Choose File
                            </button>
                        </div>
                        <br>
                        <span>
                            <ul>Keterangan:
                                <li>Jenis file: .xlsx dan .pdf</li>
                                <li>Maksimal file 25mb</li>
                            </ul>
                        </span>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_update_"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($part_file->kode_part_file) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script src="{{ base_url() }}assets/backend/js/partaksesoris/js/part-edit.js" type="text/javascript"></script>
@stop