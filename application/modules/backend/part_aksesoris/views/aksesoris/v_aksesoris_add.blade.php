@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
        <div class="m-portlet__body">
             <div class="form-group m-form__group row">
                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Nama Aksesoris
                    </label>
                    {!! form_input(array('type' => 'text','name' => 'nama_part_aksesoris','id' => 'nama_part_aksesoris', 'class' => 'form-control m-input', 'placeholder' => 'Nama Aksesoris' )) !!}
                </div>

                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Nama Produk:
                    </label>
                    {!! form_dropdown('kode_produk', $produk, '', 'class="form-control m-select2" id="m_select2_3" multiple="multiple"') !!}
                </div>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-12 form-group">
                    <label>
                        *
                        Deskripsi Aksesoris
                    </label>
                    {!! form_textarea(array('type' => 'text','name' => 'deskripsi_part_aksesoris', 'class' => 'form-control m-input summernote', 'placeholder' => 'Deskripsi Aksesoris' )) !!}
                </div>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Harga Aksesoris:
                    </label>
                    {!! form_input(array('type' => 'text','name' => 'harga_part_aksesoris', 'class' => 'form-control m-input numbers', 'placeholder' => 'Harga Aksesoris', 'value' => '0')) !!}
                </div>
                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Kategori Aksesoris
                    </label>
                    {!! form_dropdown('kode_kategori_part_aksesoris', $kategori_part_aksesoris, '', 'class="form-control m-select2" id="m_select2_2"') !!}
                </div>
            </div>
            <!-- <div class="form-group m-form__group row">
                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Jenis Aksesoris
                    </label>
                    {!! form_dropdown('jenis_part_aksesoris', $jenis_part_aksesoris, '', 'class="form-control m-select2" id="m_select2_4"') !!}
                </div>
            </div> -->
            <div class="form-group m-form__group row">
                <div class="col-lg-12 form-group">
                    <label>
                        *
                        Photo
                    </label>
                    <div class="m-dropzone dropzone m-dropzone--primary dropzone-previews" id="my-awesome-dropzone">
                        <div class="m-dropzone__msg dz-message needsclick">
                            <h3 class="m-dropzone__msg-title">
                                Jatuhkan berkas di sini atau klik untuk diunggah.
                            </h3>
                            <span class="m-dropzone__msg-desc">
                                Upload maksimal 1 berkas. Berkas maksimal 2 MB
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group row">
                 <div class="col-lg-6 form-group">
                    <label>
                        *
                        Status Promo:
                    </label>
                    {!! form_dropdown('status_promo', $status_promo, '', 'class="form-control m-select2" id="m_select2_5"') !!}
                </div>
                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Status Ketersediaan Stok
                    </label>
                    {!! form_dropdown('status_ketersediaan_stok', $status_ketersediaan_stok, '', 'class="form-control m-select2" id="m_select2_6"') !!}
                </div>
            </div>
        </div>

            
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions--solid">
                <div class="row">
                    <div class="col-lg-12 m--align-right">
                        {!! $btn["btn_save"] !!}
                        {!! $btn["btn_cancel"] !!}
                    </div>
                </div>
            </div>
        </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">

    var url                    = "{{$action}}";
    var url_succees            = "{{$url_succees}}";
    var checkNamaPartAksesoris = "{{$checkNamaPartAksesoris}}";
    
    var uploadMultiple         = true; 
    var autoProcessQueue       = false;
    var maxFilesize            = 2;
    var paramName              = "file";
    var addRemoveLinks         = true;
    var maxFiles               = 5;
    var parallelUploads        = 5;
    var acceptedFiles          = 'image/jpeg,image/png';

    $('.save').attr("disabled",true);
</script>
<script src="{{ base_url() }}assets/backend/js/partaksesoris/js/part-aksesoris-add.js" type="text/javascript"></script>
@stop