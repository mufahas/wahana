@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open(null, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 ">
                        <label>
                            Jenis part_aksesoris:
                        </label>
                        <h5>{{ $part_aksesoris->nama_kategori_part_aksesoris }}</h5>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Nama Produk:
                        </label>
                        @if(!empty($produk))
                        <h5>{{ $produk }}</h5>
                        @endif
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <div class="col-lg-12">
                        <label>
                            Nama part_aksesoris:
                        </label>
                        <h5>{{ $part_aksesoris->nama_part_aksesoris }}</h5>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                     <div class="col-lg-12">
                        <label>
                            Photo:
                        </label>
                        <h5><img src="{{ $path_foto .  $part_aksesoris->gambar_part_aksesoris }}" style="width: 50%; display: block;margin-left: auto; margin-right: auto;"></h5>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                     <div class="col-lg-12">
                        <label>
                            Deskripsi:
                        </label>
                        <h5>{!!  $part_aksesoris->deskripsi_part_aksesoris !!}</h5>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label>
                            Harga:
                        </label>
                        <h5>Rp {{ number_format($part_aksesoris->harga_part_aksesoris,'0',',','.') }}</h5>
                    </div>
                    <div class="col-lg-6">
                        <label>
                            Status Kesediaan Stok:
                        </label>
                        <h5>{{ $part_aksesoris->status_ketersediaan_stok }}</h5>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label>
                            Status promo:
                        </label>
                        <h5>{{ $part_aksesoris->status_promo == 'T' ? 'Ya' : 'Tidak' }}</h5>
                    </div>
                    <div class="col-lg-6 ">
                        <label>
                            Status Publikasi:
                        </label>
                        <h5>{{ $part_aksesoris->publikasi == 'T' ? 'Ya' : 'Tidak' }}</h5>
                    </div>
                </div>

            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop