@extends('backend.default.views.layout.v_layout')

@section('body')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Nama Part Aksesoris
                    </label>
                    {!! form_input(array('type' => 'text','name' => 'nama_part_aksesoris','id' => 'nama_part_aksesoris', 'class' => 'form-control m-input', 'placeholder' => 'Nama Part Aksesoris', 'value' => $ms_part_aksesoris->nama_part_aksesoris )) !!}
                </div>
                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Nama Produk:
                    </label>
                    <!-- {!! form_dropdown('kode_produk', $produk, encryptID($ms_part_aksesoris->kode_produk), 'class="form-control m-select2" id="m_select2_3"') !!} -->
                    <select name="kode_produk[]" class="form-control m-select2" id="m_select2_4" multiple="multiple">
                        <?php 
                            foreach ($produk as $key) { ?>
                                <option value="<?= encryptID($key->kode_produk) ?>" <?php 
                                if (in_array($key->kode_produk, $array_produk)) {
                                    echo "selected";
                                    } ?>><?= $key->nama_produk; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-12 form-group">
                    <label>
                        *
                        Deskripsi Part Aksesoris
                    </label>
                    {!! form_textarea(array('type' => 'text','name' => 'deskripsi_part_aksesoris', 'class' => 'form-control m-input summernote', 'placeholder' => 'Deskripsi Part Aksesoris' , 'value' => $ms_part_aksesoris->deskripsi_part_aksesoris )) !!}
                </div>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Harga Part Aksesoris:
                    </label>
                    {!! form_input(array('type' => 'text','name' => 'harga_part_aksesoris', 'class' => 'form-control m-input numbers', 'placeholder' => 'Harga Part Aksesoris', 'value' => formattedNumber($ms_part_aksesoris->harga_part_aksesoris))) !!}
                </div>
                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Kategori Part Aksesoris
                    </label>
                    {!! form_dropdown('kode_kategori_part_aksesoris', $kategori_part_aksesoris, encryptID($ms_part_aksesoris->kode_kategori_part_aksesoris), 'class="form-control m-select2" id="m_select2_2"') !!}
                </div>
            </div>
            <!-- <div class="form-group m-form__group row">
                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Jenis Part Aksesoris
                    </label>
                    {!! form_dropdown('jenis_part_aksesoris', $jenis_part_aksesoris, $ms_part_aksesoris->jenis_part_aksesoris , 'class="form-control m-select2" id="m_select2_4"') !!}
                </div>
            </div> -->
            <div class="form-group m-form__group row">
                <div class="col-lg-12 form-group">
                    <label>
                        *
                        Photo
                    </label>
                    <div class="m-dropzone dropzone m-dropzone--primary dropzone-previews" id="my-awesome-dropzone">
                        <div class="m-dropzone__msg dz-message needsclick">
                            <h3 class="m-dropzone__msg-title">
                                Jatuhkan berkas di sini atau klik untuk diunggah.
                            </h3>
                            <span class="m-dropzone__msg-desc">
                                Upload maksimal 1 berkas. Berkas maksimal 2 MB
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group row">
                 <div class="col-lg-6 form-group">
                    <label>
                        *
                        Status Promo:
                    </label>
                    {!! form_dropdown('status_promo', $status_promo, $ms_part_aksesoris->status_promo , 'class="form-control m-select2" id="m_select2_5"') !!}
                </div>
                <div class="col-lg-6 form-group">
                    <label>
                        *
                        Status Ketersediaan Stok
                    </label>
                    {!! form_dropdown('status_ketersediaan_stok', $status_ketersediaan_stok, $ms_part_aksesoris->status_ketersediaan_stok , 'class="form-control m-select2" id="m_select2_6"') !!}
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions--solid">
                <div class="row">
                    <div class="col-lg-12 m--align-right">
                        {!! $btn["btn_update"] !!}
                        {!! $btn["btn_cancel"] !!}
                    </div>
                </div>
            </div>
        </div>

        {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($ms_part_aksesoris->id_part_aksesoris) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">

    var url                    = "{{$action}}";
    var url_succees            = "{{$url_succees}}";
    var checkNamaPartAksesoris = "{{$checkNamaPartAksesoris}}";
    var getImage               = "{{$getImage}}{{encryptID($ms_part_aksesoris->id_part_aksesoris)}}";
    var path_image             = "{{$path_image}}";
    var remove_image           = "{{$remove_image}}";

    var uploadMultiple         = true; 
    var autoProcessQueue       = false;
    var maxFilesize            = 2;
    var paramName              = "file";
    var addRemoveLinks         = true;
    var maxFiles               = 5;
    var parallelUploads        = 5;
    var acceptedFiles          = 'image/jpeg,image/png';
</script>
<script src="{{ base_url() }}assets/backend/js/partaksesoris/js/part-aksesoris-edit.js" type="text/javascript"></script>
@stop