@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Jenis Apparel:
                        </label>
                        {!! form_dropdown('jenis_apparel', $jenis_apparel, $apparel->jenis_apparel, 'class="form-control m-select2" id="m_select2_2"') !!}
                    </div> 
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Nama Produk:
                        </label>
                        
                        <select name="kode_produk[]" class="form-control m-select2" id="m_select2_4" multiple="multiple">
                            <?php 
                                foreach ($produk as $key) { ?>
                                    <option value="<?= encryptID($key->kode_produk) ?>" <?php 
                                    if (in_array($key->kode_produk, $arr_produk_apparel)) {
                                        echo "selected";
                                        } ?>><?= $key->nama_produk; ?></option>
                            <?php } ?>
                        </select>

                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-12">
                        <label>
                            *
                            Nama Apparel:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'nama_apparel', 'value' => $apparel->nama_apparel, 'class' => 'form-control m-input', 'placeholder' => 'Nama Apparel' )) !!}
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <div class="col-lg-12">
                        <label>
                            *
                            Harga Apparel:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'harga_apparel', 'value' => $apparel->harga_apparel, 'class' => 'form-control m-input', 'placeholder' => 'Harga Apparel' )) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-12">
                        <label>
                            *
                            Deskripsi:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'deskripsi_apparel', 'value' => $apparel->deskripsi_apparel,'class' => 'form-control m-input summernote', 'placeholder' => 'Deskripsi Part Aksesoris' )) !!}
                    </div>
                </div>

                <div class="m-portlet__body photo-edit">
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6">
                            <label>
                                *
                                Photo:
                            </label>
                            <div class="input-group date" id="start_date">
                                <img src="{{site_url()}}assets/upload/part_aksesoris/apparel/{{$apparel->gambar_apparel}}" width="auto" height="200">
                                <span class="input-group-addon">
                                    <button type="button" class="remove m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Hapus"><i class="la la-trash"></i> </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__body dropzone-edit" style="display: none;">
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6">
                            <label>
                                *
                                Photo:
                            </label> 
                            <div class="m-dropzone dropzone m-dropzone--primary dropzone-previews" id="my-awesome-dropzone">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 class="m-dropzone__msg-title">
                                        Jatuhkan berkas di sini atau klik untuk diunggah.
                                    </h3>
                                    <span class="m-dropzone__msg-desc">
                                        Upload hingga 1 berkas. Berkas maksimal 2 MB
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label>
                            *
                            Varian Warna:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'varian_warna', 'value' => $apparel->varian_warna,'class' => 'form-control m-input', 'placeholder' => 'Varian Warna' )) !!}
                    </div>

                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Ukuran:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'ukuran', 'value' => $apparel->ukuran,'class' => 'form-control m-input', 'placeholder' => 'Ukuran' )) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Publish:
                        </label>
                        {!! form_dropdown('publish', $publish, $apparel->publikasi, 'class="form-control m-select2" id="m_select2_3"') !!}
                    </div>
                </div>
            </div>

            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_update"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! form_input(array('type' => 'hidden','name' => 'id', 'value' => encryptID($apparel->kode_apparel) )) !!}

        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var url              = "{{$action}}";
    var url_succees      = "{{$url_succees}}";
    var uploadMultiple   = false; 
    var autoProcessQueue = false;
    var maxFilesize      = 2;
    var paramName        = "file";
    var addRemoveLinks   = true;
    var maxFiles         = 1;
    var parallelUploads  = 1;
    var acceptedFiles    = 'image/jpeg,image/png';
</script>
<script src="{{ base_url() }}assets/backend/js/partaksesoris/js/apparel-edit.js" type="text/javascript"></script>
@stop