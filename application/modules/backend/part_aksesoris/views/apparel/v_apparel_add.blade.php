@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form Apparel
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open($action, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Jenis Apparel:
                        </label>
                        {!! form_dropdown('jenis_apparel', $jenis_apparel, '', 'class="form-control m-select2" id="m_select2_2"') !!}
                    </div> 
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Nama Produk:
                        </label>
                        {!! form_dropdown('kode_produk', $produk, '', 'class="form-control m-select2" id="m_select2_4" multiple="multiple"') !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            *
                            Nama Apparel:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'nama_apparel', 'class' => 'form-control m-input', 'placeholder' => 'Nama Apparel' )) !!}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            *
                            Harga Apparel:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'harga_apparel', 'class' => 'form-control m-input', 'placeholder' => 'Harga Apparel' )) !!}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            *
                            Deskripsi:
                        </label>
                        {!! form_textarea(array('type' => 'text','name' => 'deskripsi_apparel', 'class' => 'form-control m-input summernote', 'placeholder' => 'Deskripsi Part Aksesoris' )) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-12 form-group">
                        <label>
                            *
                            Photo
                        </label>
                        <div class="m-dropzone dropzone m-dropzone--primary dropzone-previews" id="my-awesome-dropzone">
                            <div class="m-dropzone__msg dz-message needsclick">
                                <h3 class="m-dropzone__msg-title">
                                    Jatuhkan berkas di sini atau klik untuk diunggah.
                                </h3>
                                <span class="m-dropzone__msg-desc">
                                    Upload maksimal 1 berkas. Berkas maksimal 2 MB
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group ">
                        <label>
                            *
                            Varian Warna:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'varian_warna', 'class' => 'form-control m-input', 'placeholder' => 'Varian Warna' )) !!}
                    </div>

                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Ukuran:
                        </label>
                        {!! form_input(array('type' => 'text','name' => 'ukuran', 'class' => 'form-control m-input', 'placeholder' => 'Ukuran' )) !!}
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            *
                            Publish:
                        </label>
                        {!! form_dropdown('publish', $publish, '', 'class="form-control m-select2" id="m_select2_3"') !!}
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-12 m--align-right">
                            {!! $btn["btn_save"] !!}
                            {!! $btn["btn_cancel"] !!}
                        </div>
                    </div>
                </div>
            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop

@section('scripts')
<script type="text/javascript">
    var url              = "{{$action}}";
    var url_succees      = "{{$url_succees}}";
    var checkNamaApparel = "{{$checkNamaApparel}}";
    
    var uploadMultiple   = false; 
    var autoProcessQueue = false;
    var maxFilesize      = 2;
    var paramName        = "file";
    var addRemoveLinks   = true;
    var maxFiles         = 1;
    var parallelUploads  = 1;
    var acceptedFiles    = 'image/jpeg,image/png';

    $('.save').attr("disabled",true);
</script>
<script src="{{ base_url() }}assets/backend/js/partaksesoris/js/apparel-add.js" type="text/javascript"></script>
@stop