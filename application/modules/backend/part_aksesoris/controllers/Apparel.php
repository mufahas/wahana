<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Apparel extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        /* Load Model */
        $this->load->model('select_global_model');
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table: ms_apparel
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_apparel";
        $condition    = "ms_apparel.dihapus = 'F'";
        $row          = array('ms_apparel.kode_apparel','ms_apparel.nama_apparel','ms_apparel.jenis_apparel','ms_apparel.publikasi','ms_apparel.tanggal_publikasi');
        $row_search   = array('ms_apparel.kode_apparel','ms_apparel.nama_apparel','ms_apparel.jenis_apparel','ms_apparel.publikasi','ms_apparel.tanggal_publikasi');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {   

        $data['jenis_apparel']    = $this->select_global_model->selectJenisApparel();
        $data['publish']          = $this->select_global_model->selectPublish();
        $data['produk']           = $this->select_global_model->selectProduct();
        
        $data['checkNamaApparel'] = site_url() . $this->site . '/checkNamaApparel';

        $data['url_succees']      = site_url() . $this->site;
        
        /* Button Action */
        $data['action']           = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table: ms_apparel
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        /* Url */
        $url_succees       = site_url() . $this->site;
        $url_error         = site_url() . $this->site . '/add';
        
        $user              = $this->ion_auth->user()->row();
        
        $jenis_apparel     = $this->input->post('jenis_apparel');
        $produk            = $this->input->post('kode_produk');
        $kode_produk       = explode(',', $produk);
        $publikasi         = $this->input->post('publish');
        $nama_apparel      = ucfirst($this->input->post('nama_apparel'));
        $harga_apparel     = $this->input->post('harga_apparel');
        $deskripsi_apparel = $this->input->post('deskripsi_apparel');
        $varian_warna      = ucfirst($this->input->post('varian_warna'));
        $ukuran            = ucfirst($this->input->post('ukuran'));
        
        /* Post Upload Foto */
        $userfile          = $_FILES["file"]["name"];
        $userfile_size     = $_FILES["file"]["size"];
        $userfile_tmp_name = $_FILES["file"]["tmp_name"];
        
        $result            = ms_apparel::where('nama_apparel', $nama_apparel)->first();

        if(empty($result) && !empty($userfile))
        {
            //Get Nama Produk
            // $produk             = ms_produk::where('kode_produk', decryptID($kode_produk))->first();

            //Set Upload File 
            $filename           = $userfile;
            $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext           = substr($filename, strripos($filename, '.')); // get file name
            $filesize           = $userfile_size;
            $allowed_file_types = array('.jpg','.jpeg','.png');  

            // Rename file
            $newfilename = 'APPAREL_'. time() . '_' . $file_basename . $file_ext;

            if (in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 2000000)
            {
                if (file_exists("assets/upload/part_aksesoris/apparel/" . $newfilename))
                {
                    // file already exists error
                    // echo "You have already uploaded this file.";
                    $status = array('status' => 'error', 'message' => lang('message_save_failed') . ' ' . $filename);
                } 
                else 
                {
                    if(move_uploaded_file($userfile_tmp_name, "assets/upload/part_aksesoris/apparel/" . $newfilename))
                    {    
                        $model                    = new ms_apparel;
    
                        $model->jenis_apparel     = $jenis_apparel;
                        $model->nama_apparel      = $nama_apparel;
                        $model->harga_apparel     = $harga_apparel;
                        $model->nama_apparel_url  = clean_url(strtolower($nama_apparel));
                        $model->gambar_apparel    = $newfilename;
                        $model->deskripsi_apparel = $deskripsi_apparel;
                        $model->varian_warna      = $varian_warna;
                        $model->ukuran            = $ukuran;
                        $model->publikasi         = $publikasi;
                        $model->tanggal_publikasi = $publikasi == 'T' ? date('Y-m-d H:i:s') : 'null';
                        $model->dipublikasi_oleh  = $publikasi == 'T' ? $user->username : '';
                        
                        $save                     = $model->save();

                        if($save)
                        {

                            /* Begin Save Detail Apparel */
                            foreach ($kode_produk as $key) 
                            {
                                $ms_apparel_detail               = new ms_apparel_detail;
                                
                                $ms_apparel_detail->kode_apparel = ms_apparel::max('kode_apparel');
                                $ms_apparel_detail->kode_produk  = decryptID($key); 

                                $save_apparel_detail = $ms_apparel_detail->save();  
                            }  
                            /* End Save Detail Apparel */

                            $data_notif = array(
                                "Kode Apparel"  => ms_apparel::max('kode_apparel'),
                                "Jenis Apparel" => ($jenis_apparel == 'K' ? 'Kemeja' : ($jenis_apparel == 'T' ? 'Topi' : 'Kaos')),
                                "Nama Apparel"  => $nama_apparel,
                                "Ukuran"        => $ukuran,
                                "Warna"         => $varian_warna,
                                );

                            $message = "Berhasil menambahkan master Apparel " . $nama_apparel;
                            $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                            /* End Write Log */

                            //echo "File uploaded successfully.";     
                            $status = array('status' => 'success','message' => lang('message_save_success') . ' ' . $filename);
                        }
                    } 
                    else 
                    {
                        $status = array('status' => 'error', 'message' => lang('message_save_failed'));
                    }
                }
            }
            else if(empty($file_basename))
            {
                // file selection error
                // echo "Please select a file to upload.";
                $status = array('status' => 'error', 'message' => lang('message_save_failed'));
            } 
            else if($filesize >= 200000)
            {
                // file size error
                // echo "The file you are trying to upload is too large.";
                $status = array('status' => 'error', 'message' => lang('message_save_failed'));
            } else {
                // file type error
                //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
                $status = array('status' => 'error', 'message' => lang('message_save_failed'));
                unlink($userfile_tmp_name);
            }
        } else {
            $status = array('status' => 'error', 'message' => 'Nama Apparel Sudah Ada.');
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
        
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {   
        $kode_apparel = decryptID($id);
        
        $ms_apparel   = ms_apparel::selectRaw(' ms_apparel.kode_apparel,
                                                ms_apparel.nama_apparel,
                                                ms_apparel.nama_apparel_url,
                                                ms_apparel.harga_apparel,
                                                ms_apparel.gambar_apparel,
                                                ms_apparel.deskripsi_apparel,
                                                ms_apparel.varian_warna,
                                                ms_apparel.ukuran,
                                                ms_apparel.jenis_apparel,
                                                ms_apparel.dihapus,
                                                ms_apparel.dihapus_oleh,
                                                ms_apparel.tanggal_hapus,
                                                ms_apparel.tanggal_terakhir_ubah,
                                                ms_apparel.diubah_oleh,
                                                ms_apparel.publikasi,
                                                ms_apparel.tanggal_publikasi,
                                                ms_apparel.dipublikasi_oleh'
                                            )
                                    ->where('kode_apparel',$kode_apparel)
                                    ->first();
            // echo '<pre>';
            // print_r($ms_apparel);
            // echo '</pre>';
            // die();

        if(!empty($ms_apparel))
        {   

            $data['apparel']            = $ms_apparel;
            $data['jenis_apparel']      = $this->select_global_model->selectJenisApparel();
            $data['publish']            = $this->select_global_model->selectPublish();
            $data['produk']             = ms_produk::where('dihapus','F')->get();
   
            $apparel_detail_produk      = ms_apparel_detail::leftJoin('ms_produk','ms_produk.kode_produk','=','ms_apparel_detail.kode_produk')
                                                            ->where('ms_apparel_detail.kode_apparel', $ms_apparel->kode_apparel)
                                                            ->get();
            $arr_produk_apparel         = array();

            foreach ($apparel_detail_produk as $detail) 
            {
                array_push($arr_produk_apparel, $detail->kode_produk);
            }

            $data['arr_produk_apparel']       = $arr_produk_apparel;
            
            /* Button Action */
            $data['action']                   = site_url() . $this->site . '/update';
            $data['getImage']                 = site_url() . $this->site . '/getImage/';
            $data['remove_image']             = site_url() . $this->site . '/remove_image/';
            $data['url_succees']              = site_url() . $this->site;
            $data['path_image']               = site_url() . 'assets/upload/part_aksesoris/apparel/';
            $data['checkNamaApparel']         = site_url() . $this->site . '/checkNamaApparel';

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        } else {
            redirect(site_url() . $this->site);
        }

    }

    /**
    * Save data to table: ms_apparel
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        
        $id           = $this->input->post("id");
        $kode_apparel = decryptID($id);

        /* Url */
        $url_succees = site_url() . $this->site;
        $url_error   = site_url() . $this->site . '/edit/' . $id;

        /* Get User */
        $user              = $this->ion_auth->user()->row();

        $jenis_apparel     = $this->input->post('jenis_apparel');
        $kode_produk       = $this->input->post('kode_produk[]');
        // print_r($kode_produk);
        // die();
        $publikasi         = $this->input->post('publish');
        $nama_apparel      = ucfirst($this->input->post('nama_apparel'));
        $harga_apparel     = $this->input->post('harga_apparel');
        $deskripsi_apparel = $this->input->post('deskripsi_apparel');
        $varian_warna      = ucfirst($this->input->post('varian_warna'));
        $ukuran            = ucfirst($this->input->post('ukuran'));

        /* Post Upload Foto */
        $userfile          = $_FILES;
        
        $model     = ms_apparel::where('kode_apparel',$kode_apparel)->first(); //get apparel
        
        /* Write Log Data Old*/
        $data_old   = array(
                            "Kode Apparel"  => $model->kode_apparel,
                            "Nama Apparel"  => $model->nama_apparel,
                            "Jenis Apparel" => ($model->jenis_apparel == 'K' ? 'Kemeja' : ($model->jenis_apparel == 'T' ? 'Topi' : 'Kaos')),
                            "Ukuran"        => $model->ukuran,
                            "Warna"         => $model->varian_warna,
                        );    

        if(!empty($userfile))
        {
            $filename           = $_FILES["file"]["name"];
            $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext           = substr($filename, strripos($filename, '.')); // get file name
            $filesize           = $_FILES["file"]["size"];
            $allowed_file_types = array('.jpg','.jpeg','.png');  

            if (in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 2000000)
            {
                // Rename file
                $newfilename = 'APPAREL_'. time() . '_' . $file_basename . $file_ext;

                if(file_exists("assets/upload/part_aksesoris/apparel/" . $newfilename))
                {
                    $status = array('status' => 'error', 'message' => lang('message_save_failed') . ' ' . $newfilename);
                }
                else
                {
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], "assets/upload/part_aksesoris/apparel/" . $newfilename))
                    {
                        if(file_exists("assets/upload/part_aksesoris/apparel/" . $model->gambar_apparel))
                        {
                            unlink("assets/upload/part_aksesoris/apparel/" . $model->gambar_apparel);
                        }

                        $model->jenis_apparel         = $jenis_apparel;
                        $model->nama_apparel          = $nama_apparel;
                        $model->harga_apparel         = $harga_apparel;
                        $model->nama_apparel_url      = clean_url(strtolower($nama_apparel));
                        $model->gambar_apparel        = $newfilename;
                        $model->deskripsi_apparel     = $deskripsi_apparel;
                        $model->varian_warna          = $varian_warna;
                        $model->ukuran                = $ukuran;
                        $model->publikasi             = $publikasi;
                        $model->tanggal_publikasi     = $publikasi == 'T' ? date('Y-m-d H:i:s') : 'null';
                        $model->dipublikasi_oleh      = $publikasi == 'T' ? $user->username : '';
                        $model->diubah_oleh           = $user->username;
                        $model->tanggal_terakhir_ubah = date('Y-m-d H:i:s');

                        $save                         = $model->save();

                        if($save)
                        {
                           if(!empty($kode_produk))
                           {
                                $delete_apparel_detail = ms_apparel_detail::where('ms_apparel_detail.kode_apparel',$kode_apparel)->delete();

                                if($delete_apparel_detail)
                                {
                                    foreach($kode_produk as $pro_duk)
                                    {
                                        $ms_apparel_detail = new ms_apparel_detail;

                                        $ms_apparel_detail->kode_apparel = $kode_apparel;
                                        $ms_apparel_detail->kode_produk  = decryptID($pro_duk);

                                        $save_ms_apparel_detail = $ms_apparel_detail->save();
                                    }
                                }
                           }
                           else
                           {
                                $status = array('status' => 'error', 'message' => 'Nama Produk Masih Kosong !!');
                           }

                            /* Write Log */
                            $data_new   = array(
                                                "Kode Apparel"  => $kode_apparel,
                                                "Nama Apparel"  => $nama_apparel,
                                                "Jenis Apparel" => ($jenis_apparel == 'K' ? 'Kemeja' : ($jenis_apparel == 'T' ? 'Topi' : 'Kaos')),
                                                "Ukuran"        => $ukuran,
                                                "Warna"         => $varian_warna,
                                                );
                            /* Write Log */
                            $data_change = array_diff_assoc($data_new, $data_old);
                            $message     = 'Memperbarui Apparel ' . $nama_apparel;
                            $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                            /* End Write Log*/

                            $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
                        }
                    }
                }
            }
        } 
        else
        {
            $model->jenis_apparel         = $jenis_apparel;
            $model->nama_apparel          = $nama_apparel;
            $model->harga_apparel         = $harga_apparel;
            $model->nama_apparel_url      = clean_url(strtolower($nama_apparel));
            $model->deskripsi_apparel     = $deskripsi_apparel;
            $model->varian_warna          = $varian_warna;
            $model->ukuran                = $ukuran;
            $model->publikasi             = $publikasi;
            $model->tanggal_publikasi     = $publikasi == 'T' ? date('Y-m-d H:i:s') : 'null';
            $model->dipublikasi_oleh      = $publikasi == 'T' ? $user->username : '';
            $model->diubah_oleh           = $user->username;
            $model->tanggal_terakhir_ubah = date('Y-m-d H:i:s');

            $save                         = $model->save();

            if($save)
            {

                if(!empty($kode_produk))
                {
                    $delete_apparel_detail = ms_apparel_detail::where('ms_apparel_detail.kode_apparel',$kode_apparel)->delete();

                    if($delete_apparel_detail)
                    {
                        foreach($kode_produk as $pro_duk)
                        {
                            $ms_apparel_detail = new ms_apparel_detail;

                            $ms_apparel_detail->kode_apparel = $kode_apparel;
                            $ms_apparel_detail->kode_produk  = decryptID($pro_duk);

                            $save_ms_apparel_detail = $ms_apparel_detail->save();
                        }
                    }
                }    
                else
                {
                    $status = array('status' => 'error', 'message' => 'Nama Produk Masih Kosong !!');
                }

                /* Write Log */
                $data_new   = array(
                                    "Kode Apparel"  => $kode_apparel,
                                    "Nama Apparel"  => $nama_apparel,
                                    "Jenis Apparel" => ($jenis_apparel == 'K' ? 'Kemeja' : ($jenis_apparel == 'T' ? 'Topi' : 'Kaos')),
                                    "Ukuran"        => $ukuran,
                                    "Warna"         => $varian_warna,
                                    );
                /* Write Log */
                $data_change = array_diff_assoc($data_new, $data_old);
                $message     = 'Memperbarui Apparel ' . $nama_apparel;
                $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                /* End Write Log*/

                $status = array('status' => 'success','message' => lang('message_update_success'), 'url' => $url_succees);
            }  
            
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    /**
    * Delete data from table: ms_apparel
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request())
        {
            $url                = site_url() . $this->site;
            $id                 = $this->input->get("id");
            $kode_apparel       = decryptID($id);

            $ms_apparel         = ms_apparel::where('kode_apparel',$kode_apparel)->first();
            $ms_apparel_detail  = ms_apparel_detail::where('ms_apparel_detail.kode_apparel',$kode_apparel)->get();

            if(!empty($ms_apparel))
            {
                
                $delete        = ms_apparel::where("kode_apparel", $kode_apparel)->delete();

                if($delete)
                {
                    $delete_detail = ms_apparel_detail::where("kode_apparel", $kode_apparel)->delete();
                    if(!empty($ms_apparel)){
                        if (file_exists('assets/upload/part_aksesoris/apparel/' . $ms_apparel->gambar_apparel))
                        {
                            unlink('assets/upload/part_aksesoris/apparel/' . $ms_apparel->gambar_apparel);
                        }
                    }
                    
                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                } 
                else 
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            } 
            else 
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
    * Direct to page detail
    * @return page
    **/
    function view($id)
    {
        $kode_apparel = decryptID($id);
        $ms_apparel   = ms_apparel::join('ms_apparel_detail','ms_apparel_detail.kode_apparel','=','ms_apparel.kode_apparel')->join('ms_produk','ms_produk.kode_produk','=','ms_apparel_detail.kode_produk')->where('ms_apparel.kode_apparel',$kode_apparel)->first();

        if(!empty($ms_apparel))
        {
            $data['apparel']   = $ms_apparel;
            $data['path_foto'] = base_url() . "assets/upload/part_aksesoris/apparel/";

            $ms_apparel_detail   = ms_apparel_detail::join('ms_produk','ms_produk.kode_produk','=','ms_apparel_detail.kode_produk')->where('ms_apparel_detail.kode_apparel', $kode_apparel)->get();
            foreach ($ms_apparel_detail as $key => $value) {
                $produk[] = $value->nama_produk;
            }
            $data['produk'] = implode( ", ", $produk );

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        }  else {
            redirect(site_url() . $this->site);
        }
    }

    function checkNamaApparel()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $id                  = decryptID($this->input->post('id'));
            $nama_apparel        = $this->input->post('nama_apparel');
            $result              = ms_apparel::where('nama_apparel',$nama_apparel)->first();

            if (!empty($result)) 
            {
                if (!empty($id)) 
                {
                    if ($id == $result->kode_apparel) 
                    {
                        echo 'true';
                    } 
                    else 
                    {
                        echo 'false';
                    }
                } 
                else 
                {
                    echo 'false';
                }
            } 
            else 
            {
                echo 'true';
            }
        }
    }

    function getImage($kode_apparel)
    {
       $ms_apparel = ms_apparel::selectRaw('ms_apparel.gambar_apparel')->where('kode_apparel', decryptID($kode_apparel))->get();

       echo json_encode($ms_apparel);
    }

    function remove_image($kode_apparel, $gambar_apparel)
    {
        if (file_exists("assets/upload/part_aksesoris/apparel/" . $gambar_apparel))
        {
            if(unlink("assets/upload/part_aksesoris/apparel/" . $gambar_apparel))
            {
                $ms_apparel = ms_apparel::where('kode_apparel',decryptID($kode_apparel))
                                                                ->where('gambar_apparel',$gambar_apparel)
                                                                ->first();
                if(!empty($ms_apparel)){
                    $ms_apparel->gambar_apparel = null;

                    $save_aparel = $ms_apparel->save();
                }

                $status = array('status' => 'success', 'message' => 'Gambar berhasil dihapus.');
            }else{
                $status = array('status' => 'error', 'message' => 'Gambar gagal dihapus.');
            }
        }
        else
        {
            $ms_apparel = ms_apparel::where('kode_apparel',decryptID($kode_apparel))
                                                                ->where('gambar_apparel',$gambar_apparel)
                                                                ->first();
            if(!empty($ms_apparel)){
                $ms_apparel->gambar_apparel = $gambar_apparel;

                $save_aparel = $ms_apparel->save();
            }


            $status = array('status' => 'success', 'message' => 'Gambar berhasil dihapus.');
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
}