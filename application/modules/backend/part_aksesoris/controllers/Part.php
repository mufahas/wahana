<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Part extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable']       = site_url() . $this->site . '/loadTable';
        $data['add']             = site_url() . $this->site . '/add';
        $data['edit']            = site_url() . $this->site . '/edit';
        $data['delete']          = site_url() . $this->site . '/delete';

        $data['messages']       = $this->session->flashdata('messages');

        /* Set Path File */
        $data['path_file']       = base_url() . "assets/upload/part_aksesoris/part_file/";


        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

     /**
    * Serverside load table: ms_part_file
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_part_file";
        $condition    = "";
        $row          = array('ms_part_file.kode_part_file','ms_part_file.nama_part_file');
        $row_search   = array('ms_part_file.kode_part_file','ms_part_file.nama_part_file');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $data['message_success'] = $this->session->flashdata('message_success');
        $data['message_error']   = $this->session->flashdata('messages_error');

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table: ms_part_file
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            /* User */
            $user = $this->ion_auth->user()->row();

            /* Set Post File */
            $userfile            = $_FILES["userfile"]["name"];
            $userfile_size       = $_FILES["userfile"]["size"];
            $userfile_tmp_name   = $_FILES["userfile"]["tmp_name"];

            if(!empty($userfile)) {

                // Set File Name, Extension File, File size, and Allowed Type
                $filename            = $userfile;
                $file_basename       = substr($filename, 0, strripos($filename, '.')); // get file extention
                $file_ext            = substr($filename, strripos($filename, '.')); // get file name extension
                $filesize            = $userfile_size; //get size
                $allowed_file_types  = array('.xls','.xlsx','.pdf');

                if(in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 25000000)
                {
                    //Set New File Name
                    $file_part = 'PART_FILE_' . date('d-m-Y') . $file_ext;
                   
                    if(file_exists("assets/upload/part_aksesoris/part_file/" . $file_part)) {
                        $this->session->set_flashdata('message_error', lang('message_save_failed'));
                        redirect(site_url() . $this->site);
                    } else {
                        if(move_uploaded_file($userfile_tmp_name,  "assets/upload/part_aksesoris/part_file/" . $file_part))
                        {
                            $ms_part_file = new ms_part_file;

                            $ms_part_file->nama_part_file = $file_part;

                            $save_ms_part_file = $ms_part_file->save();

                            if($save_ms_part_file) {
                                $this->session->set_flashdata('message_success', lang('message_save_success') . ' ' . $file_part);
                                redirect(site_url() . $this->site);
                            } else {
                                $this->session->set_flashdata('message_error', lang('message_save_failed'));
                                redirect(site_url() . $this->site . '/add');
                            }
                        }
                    }
                } else if(empty($file_basename)) {
                    $this->session->set_flashdata('message_error', lang('message_save_failed'));
                    redirect(site_url() . $this->site . '/add');
                } else if($filesize >= 25000000) {
                    $this->session->set_flashdata('message_error', lang('message_save_failed'));
                    redirect(site_url() . $this->site . '/add');
                } else {
                    $this->session->set_flashdata('message_error', lang('message_save_failed'));
                    unlink($userfile_tmp_name);
                    redirect(site_url() . $this->site . '/add');
                }
            }
        } else {
            $this->session->set_flashdata('message_error', lang('message_save_failed'));
            redirect(site_url() . $this->site);
        }
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $kode_part_file = decryptID($id);
        
        $ms_part_file   = ms_part_file::where('kode_part_file',$kode_part_file)->first();

        if(!empty($ms_part_file))
        {   
            /* Set Path Foto */
            $data['path_file']          = site_url() . 'assets/upload/part_aksesoris/part_file/';
            $data['part_file']          = $ms_part_file;   
            
            /* Button Action */
            $data['messages']           = $this->session->flashdata('messages');
            $data['action']             = site_url() . $this->site . '/update';
            
            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        } else {
            redirect(site_url() . $this->site);
        }


    }

    /**
    * Save data to table: ms_part_file
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            /* Get User */
            $user                = $this->ion_auth->user()->row();

            $id                  = $this->input->post('id');
            $kode_part_file      = decryptID($id);

            $userfile            = $_FILES["userfile"]["name"];
            $userfile_size       = $_FILES["userfile"]["size"];
            $userfile_tmp_name   = $_FILES["userfile"]["tmp_name"];

            $ms_part_file        = ms_part_file::where('kode_part_file',$kode_part_file)->first();

            if(!empty($userfile))
            {
                // Set File Name, Extension File, File size, and Allowed Type
                $filename            = $userfile;
                $file_basename       = substr($filename, 0, strripos($filename, '.')); // get file extention
                $file_ext            = substr($filename, strripos($filename, '.')); // get file name extension
                $filesize            = $userfile_size; //get size
                $allowed_file_types  = array('.xls','.xlsx','.pdf');

                if(in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 25000000) 
                {
                    $file_part = 'PART_FILE_' . $file_basename . '_' .date('d-m-Y') . $file_ext;

                    if(file_exists("assets/upload/part_aksesoris/part_file/" . $file_part))
                    {
                        $unlink_foto = unlink("assets/upload/part_aksesoris/part_file/" . $ms_part_file->nama_part_file);

                        if($unlink_foto)
                        {
                            if(move_uploaded_file($userfile_tmp_name, "assets/upload/part_aksesoris/part_file/" . $file_part))
                            {
                                $ms_part_file->nama_part_file = $file_part;
                                $ms_part_file->diubah_oleh    = $user->username;
                                $ms_part_file->tanggal_diubah = date('Y-m-d H:i:s');

                                $save_ms_part_file            = $ms_part_file->save();

                                $this->session->set_flashdata('messages', 'Data Berhasil Diubah');
                                redirect(site_url() . $this->site);
                            }
                            else
                            {
                                $this->session->set_flashdata('messages', 'Data Gagal Diubah');
                                redirect(site_url() . $this->site);
                            }
                        }
                        else 
                        {
                            $this->session->set_flashdata('messages', 'Data Gagal Diubah');
                            redirect(site_url() . $this->site);
                        }
                    }
                    else
                    {
                        if(move_uploaded_file($userfile_tmp_name, "assets/upload/part_aksesoris/part_file/" . $file_part))
                        {
                            $ms_part_file->nama_part_file = $file_part;
                            $ms_part_file->diubah_oleh    = $user->username;
                            $ms_part_file->tanggal_diubah = date('Y-m-d H:i:s');

                            $save_ms_part_file            = $ms_part_file->save();

                            $this->session->set_flashdata('messages', 'Data Berhasil Diubah');
                            redirect(site_url() . $this->site);
                        }
                        else
                        {
                            $this->session->set_flashdata('messages', 'Data Gagal Diubah');
                            redirect(site_url() . $this->site);
                        }
                    }
                }
            } 
            else 
            {
                $this->session->set_flashdata('messages', 'Data Gagal Diubah');
                redirect(site_url() . $this->site);
            }
        }
        else
        {
            redirect(site_url() . $this->site);  
        }
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        
    }
}