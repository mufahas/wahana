<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Aksesoris extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();

        /* Load Model */
        $this->load->model('select_global_model');
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:ms_part_aksesoris
    * @return ajax
    **/
    function loadTable()
    {
        $model        = "ms_part_aksesoris";
        $condition    = "ms_part_aksesoris.dihapus = 'F'";
        $row          = array('ms_part_aksesoris.id_part_aksesoris', 'ms_part_aksesoris.nama_part_aksesoris','ms_part_aksesoris.harga_part_aksesoris','ms_part_aksesoris.status_ketersediaan_stok');
        $row_search   = array('ms_part_aksesoris.id_part_aksesoris', 'ms_part_aksesoris.nama_part_aksesoris','ms_part_aksesoris.harga_part_aksesoris','ms_part_aksesoris.status_ketersediaan_stok');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add()
    {
        /* Button Action */
        $data['action']                   = site_url() . $this->site . '/save';
        $data['checkNamaPartAksesoris']   = site_url() . $this->site . '/checkNamaPartAksesoris';
        $data['url_succees']              = site_url() . $this->site;
        
        $data['jenis_part_aksesoris']     = $this->select_global_model->selectJenisPartAksesoris();
        $data['status_promo']             = $this->select_global_model->selectStatusPromo();
        $data['status_ketersediaan_stok'] = $this->select_global_model->selectStatusKetersediaanStok();
        $data['kategori_part_aksesoris']  = $this->select_global_model->selectKategoriPartAksesoris();
        $data['produk']                   = $this->select_global_model->selectProduct();

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Direct to page detail
    * @return page
    **/
    function view($id)
    {
        $id_part_aksesoris = decryptID($id);
        $ms_part_aksesoris   = ms_part_aksesoris::join('ms_part_aksesoris_detail','ms_part_aksesoris_detail.id_part_aksesoris','=','ms_part_aksesoris.id_part_aksesoris')->join('ms_produk','ms_produk.kode_produk','=','ms_part_aksesoris_detail.kode_produk')->join('ms_kategori_part_aksesoris','ms_kategori_part_aksesoris.kode_kategori_part_aksesoris','=','ms_part_aksesoris.kode_kategori_part_aksesoris')->join('ms_gambar_part_aksesoris','ms_gambar_part_aksesoris.id_part_aksesoris','=','ms_part_aksesoris.id_part_aksesoris')->where('ms_part_aksesoris.id_part_aksesoris',$id_part_aksesoris)->first();

        if(!empty($ms_part_aksesoris))
        {
            $data['part_aksesoris']   = $ms_part_aksesoris;
            $data['path_foto'] = base_url() . "assets/upload/part_aksesoris/aksesoris/";

            $ms_part_aksesoris_detail   = ms_part_aksesoris_detail::join('ms_produk','ms_produk.kode_produk','=','ms_part_aksesoris_detail.kode_produk')->where('ms_part_aksesoris_detail.id_part_aksesoris', $id_part_aksesoris)->get();
            foreach ($ms_part_aksesoris_detail as $key => $value) {
                $produk[] = $value->nama_produk;
            }
            $data['produk'] = implode( ", ", $produk );

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
        }  else {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:ms_part_aksesoris
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        $kode_kategori_part_aksesoris = $this->input->post('kode_kategori_part_aksesoris');
        $produk                       = $this->input->post('kode_produk');
        $kode_produk                  = explode(',', $produk);
        $status_promo                 = $this->input->post('status_promo'); 
        $nama_part_aksesoris          = $this->input->post('nama_part_aksesoris');
        $harga_part_aksesoris         = $this->input->post('harga_part_aksesoris'); 
        $clean_harga_part_aksesoris   = str_replace(',', '', $harga_part_aksesoris);
        $deskripsi_part_aksesoris     = $this->input->post('deskripsi_part_aksesoris');
        $status_ketersediaan_stok     = $this->input->post('status_ketersediaan_stok');
        $userfile                     = $_FILES["file"]["name"];
        $userfile_size                = $_FILES["file"]["size"];
        $userfile_tmp_name            = $_FILES["file"]["tmp_name"];

        $result              = ms_part_aksesoris::where('nama_part_aksesoris',$nama_part_aksesoris)->first();

        if(empty($result))
        {

            $model = new ms_part_aksesoris;

            $model->kode_kategori_part_aksesoris = decryptID($kode_kategori_part_aksesoris);
            // $model->kode_produk                  = decryptID($kode_produk);
            $model->status_promo                 = $status_promo; 
            $model->nama_part_aksesoris          = $nama_part_aksesoris;
            $model->nama_part_aksesoris_url      = clean_url(strtolower($nama_part_aksesoris));
            $model->harga_part_aksesoris         = $clean_harga_part_aksesoris;
            $model->deskripsi_part_aksesoris     = $deskripsi_part_aksesoris;
            $model->status_ketersediaan_stok     = $status_ketersediaan_stok;

            $save = $model->save();

            if($save){

                /* Begin Save Detail Aksesoris */
                foreach ($kode_produk as $key) 
                {
                    $ms_part_aksesoris_detail                    = new ms_part_aksesoris_detail;
                    
                    $ms_part_aksesoris_detail->id_part_aksesoris = ms_part_aksesoris::max('id_part_aksesoris');
                    $ms_part_aksesoris_detail->kode_produk       = decryptID($key); 

                    $save_part_aksesoris_detail = $ms_part_aksesoris_detail->save();  
                }  
                /* End Save Detail Aksesoris */

                $kategori_part_aksesoris = ms_kategori_part_aksesoris::where('kode_kategori_part_aksesoris',decryptID($kode_kategori_part_aksesoris))->first();
                /* Write Log */
                $data_notif = array(
                                    "Nama Part Aksesoris"          => $nama_part_aksesoris,
                                    "Harga Part Aksesoris"         => $harga_part_aksesoris,
                                    "Deskripsi Part Aksesoris"     => $deskripsi_part_aksesoris,
                                    "Kode Kategori Part Aksesoris" => $kategori_part_aksesoris->nama_kategori_part_aksesoris,
                                    "Status Promo"                 => ($status_promo == 'T') ? 'Tidak Promo' : 'Promo',
                                    "Status Ketersediaan Stok"     => $status_ketersediaan_stok,
                                    );

                $message = "Berhasil menambahkan part aksesoris " . $nama_part_aksesoris;
                $this->activity_log->create(NULL, NULL, json_encode($data_notif), $message, $this->router->fetch_method());
                /* End Write Log */

                foreach ($userfile as $key => $file) 
                {
                    $filename           = $file;
                    $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
                    $file_ext           = substr($filename, strripos($filename, '.')); // get file name
                    $filesize           = $userfile_size[$key];
                    $allowed_file_types = array('.jpg','.jpeg','.png');  

                    // Rename file
                    $newfilename = 'PART_AKSESORIS_'. time() . '_' . md5($file_basename) .$file_ext;

                    if (in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 2000000)
                    {   
                        if (file_exists("assets/upload/part_aksesoris/aksesoris/" . $newfilename))
                        {
                            // file already exists error
                            // echo "You have already uploaded this file.";
                            $status = array('status' => 'error', 'message' => lang('message_save_failed') . ' ' . $filename);
                        }
                        else
                        {       
                            if(move_uploaded_file($userfile_tmp_name[$key], "assets/upload/part_aksesoris/aksesoris/" . $newfilename))
                            {
                                /* Write Log */
                                $model_image = new ms_gambar_part_aksesoris;

                                $model_image->id_part_aksesoris = ms_part_aksesoris::max('id_part_aksesoris');
                                $model_image->gambar_part_aksesoris = $newfilename;

                                $save_image = $model_image->save();

                                //echo "File uploaded successfully.";     
                                $status = array('status' => 'success','message' => lang('message_save_success') . ' ' . $filename);
                            }
                        }
                    }
                    elseif (empty($file_basename))
                    {   
                        // file selection error
                        // echo "Please select a file to upload.";
                        $status = array('status' => 'error', 'message' => lang('message_save_failed'));
                    } 
                    elseif ($filesize >= 2000000)
                    {   
                        // file size error
                        // echo "The file you are trying to upload is too large.";
                        $status = array('status' => 'error', 'message' => lang('message_save_failed'));
                    }
                    else
                    {
                        // file type error
                        //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
                        $status = array('status' => 'error', 'message' => lang('message_save_failed'));
                        unlink($userfile_tmp_name[$key]);
                    }
                }
            }else{
                $status = array('status' => 'error', 'message' => lang('message_save_failed'));
            }
        }else{
            $status = array('status' => 'error', 'message' => 'Nama part aksesoris sudah digunakan.');
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        $id_part_aksesoris = decryptID($id);
        $ms_part_aksesoris = ms_part_aksesoris::where('id_part_aksesoris',$id_part_aksesoris)->first();

        if(!empty($ms_part_aksesoris))
        {
            /* Button Action */
            $data['action']                   = site_url() . $this->site . '/update';
            $data['checkNamaPartAksesoris']   = site_url() . $this->site . '/checkNamaPartAksesoris';
            $data['getImage']                 = site_url() . $this->site . '/getImage/';
            $data['remove_image']             = site_url() . $this->site . '/remove_image/';
            $data['url_succees']              = site_url() . $this->site;
            $data['path_image']               = site_url() . 'assets/upload/part_aksesoris/aksesoris/';
            
            // $data['jenis_part_aksesoris']     = $this->select_global_model->selectJenisPartAksesoris();
            $data['status_promo']             = $this->select_global_model->selectStatusPromo();
            $data['status_ketersediaan_stok'] = $this->select_global_model->selectStatusKetersediaanStok();
            $data['kategori_part_aksesoris'] = $this->select_global_model->selectKategoriPartAksesoris();
            // $data['produk']               = $this->select_global_model->selectProduct();
            
            $data['produk'] = ms_produk::select('ms_produk.kode_produk','ms_produk.nama_produk')->where('ms_produk.dihapus','F')->get();
            $arr_produk = ms_part_aksesoris_detail::where('id_part_aksesoris', $id_part_aksesoris)->get();
            foreach ($arr_produk as $varr_produk) {
                $array_produk[] = $varr_produk->kode_produk;
            }
            $data['array_produk']            = $array_produk;
            $data['ms_part_aksesoris']       = $ms_part_aksesoris;

            $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);

        }
        else
        {
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        $id                = $this->input->post("id");
        $id_part_aksesoris = decryptID($id);
        
        /* Url */
        $url_succees       = site_url() . $this->site;
        $url_error         = site_url() . $this->site . '/edit/' . $id;

        $kode_kategori_part_aksesoris = $this->input->post('kode_kategori_part_aksesoris');
        $kode_produk                  = $this->input->post('kode_produk[]');
        $status_promo                 = $this->input->post('status_promo'); 
        $nama_part_aksesoris          = $this->input->post('nama_part_aksesoris');
        $harga_part_aksesoris         = $this->input->post('harga_part_aksesoris'); 
        $clean_harga_part_aksesoris   = str_replace(',', '', $harga_part_aksesoris);
        $deskripsi_part_aksesoris     = $this->input->post('deskripsi_part_aksesoris');
        $status_ketersediaan_stok     = $this->input->post('status_ketersediaan_stok');
        $check_file                   = $_FILES;
        
        $result                       = ms_part_aksesoris::where('nama_part_aksesoris',$nama_part_aksesoris)
                                                         ->where('id_part_aksesoris', '!=' , $id_part_aksesoris)
                                                         ->first();

        if(empty($result))
        {
            $model = ms_part_aksesoris::where('id_part_aksesoris',$id_part_aksesoris)->first();

            /* Array for write log */
            $kategori_part_aksesoris_old = ms_kategori_part_aksesoris::where('kode_kategori_part_aksesoris',$model->kode_kategori_part_aksesoris)->first();

            /* Write Log */
            $data_old = array(
                                "Nama Part Aksesoris"          => $model->nama_part_aksesoris,
                                "Harga Part Aksesoris"         => formattedNumber($model->harga_part_aksesoris),
                                "Deskripsi Part Aksesoris"     => $model->deskripsi_part_aksesoris,
                                "Kode Kategori Part Aksesoris" => $kategori_part_aksesoris_old->nama_kategori_part_aksesoris,
                                "Status Promo"                 => ($model->status_promo == 'T') ? 'Tidak Promo' : 'Promo',
                                "Status Ketersediaan Stok"     => $model->status_ketersediaan_stok,
                                );

            $model->kode_kategori_part_aksesoris = decryptID($kode_kategori_part_aksesoris);
            // $model->kode_produk                  = decryptID($kode_produk);
            $model->status_promo                 = $status_promo; 
            $model->nama_part_aksesoris          = $nama_part_aksesoris;
            $model->nama_part_aksesoris_url      = clean_url(strtolower($nama_part_aksesoris));
            $model->harga_part_aksesoris         = $clean_harga_part_aksesoris;
            $model->deskripsi_part_aksesoris     = $deskripsi_part_aksesoris;
            $model->status_ketersediaan_stok     = $status_ketersediaan_stok;
            $model->tanggal_terakhir_diubah      = date('Y-m-d');

            $save = $model->save();

            if($save){

                if (!empty($kode_produk)) {
                    /* Begin Save Detail Aksesoris */
                    $delete_aksesoris_produk = ms_part_aksesoris_detail::where('ms_part_aksesoris_detail.id_part_aksesoris', $id_part_aksesoris)->delete();
                    foreach ($kode_produk as $key) 
                    {
                        $ms_part_aksesoris_detail                    = new ms_part_aksesoris_detail;
                        
                        $ms_part_aksesoris_detail->id_part_aksesoris = $id_part_aksesoris;
                        $ms_part_aksesoris_detail->kode_produk       = decryptID($key); 

                        $save_part_aksesoris_detail = $ms_part_aksesoris_detail->save();  
                    }  
                    /* End Save Detail Aksesoris */
                }

                $kategori_part_aksesoris = ms_kategori_part_aksesoris::where('kode_kategori_part_aksesoris',decryptID($kode_kategori_part_aksesoris))->first();
                /* Write Log */
                $data_new = array(
                                    "Nama Part Aksesoris"          => $nama_part_aksesoris,
                                    "Harga Part Aksesoris"         => $harga_part_aksesoris,
                                    "Deskripsi Part Aksesoris"     => $deskripsi_part_aksesoris,
                                    "Kode Kategori Part Aksesoris" => $kategori_part_aksesoris->nama_kategori_part_aksesoris,
                                    "Status Promo"                 => ($status_promo == 'T') ? 'Tidak Promo' : 'Promo',
                                    "Status Ketersediaan Stok"     => $status_ketersediaan_stok,
                                    );

                /* Write Log */
                $data_change = array_diff_assoc($data_new, $data_old);
                $message     = 'Memperbarui part aksesoris ' . $nama_part_aksesoris;
                $this->activity_log->create(json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, $this->router->fetch_method());
                /* End Write Log*/

                if(!empty($check_file))
                {
                    $userfile                     = $_FILES["file"]["name"];
                    $userfile_size                = $_FILES["file"]["size"];
                    $userfile_tmp_name            = $_FILES["file"]["tmp_name"];

                    foreach ($userfile as $key => $file) 
                    {
                        $filename           = $file;
                        $file_basename      = substr($filename, 0, strripos($filename, '.')); // get file extention
                        $file_ext           = substr($filename, strripos($filename, '.')); // get file name
                        $filesize           = $userfile_size[$key];
                        $allowed_file_types = array('.jpg','.jpeg','.png');  

                        // Rename file
                        $newfilename = 'PART_AKSESORIS_'. time() . '_' . md5($file_basename) .$file_ext;

                        if (in_array(strtolower($file_ext),$allowed_file_types) && $filesize <= 2000000)
                        {   
                            if (file_exists("assets/upload/part_aksesoris/aksesoris/" . $newfilename))
                            {
                                // file already exists error
                                // echo "You have already uploaded this file.";
                                $status = array('status' => 'error', 'message' => lang('message_update_failed') . ' ' . $filename);
                            }
                            else
                            {       
                                if(move_uploaded_file($userfile_tmp_name[$key], "assets/upload/part_aksesoris/aksesoris/" . $newfilename))
                                {
                                    /* Write Log */
                                    $model_image = new ms_gambar_part_aksesoris;

                                    $model_image->id_part_aksesoris     = $id_part_aksesoris;
                                    $model_image->gambar_part_aksesoris = $newfilename;

                                    $save_image = $model_image->save();

                                    //echo "File uploaded successfully.";     
                                    $status = array('status' => 'success','message' => lang('message_update_success') . ' ' . $filename);
                                }
                            }
                        }
                        elseif (empty($file_basename))
                        {   
                            // file selection error
                            // echo "Please select a file to upload.";
                            $status = array('status' => 'error', 'message' => lang('message_update_failed'));
                        } 
                        elseif ($filesize >= 2000000)
                        {   
                            // file size error
                            // echo "The file you are trying to upload is too large.";
                            $status = array('status' => 'error', 'message' => lang('message_update_failed'));
                        }
                        else
                        {
                            // file type error
                            //echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
                            $status = array('status' => 'error', 'message' => lang('message_update_failed'));
                            unlink($userfile_tmp_name[$key]);
                        }
                    }
                }else{
                    $status = array('status' => 'success','message' => lang('message_update_success'));
                }
            }
        }
        else
        {
            $status = array('status' => 'error', 'message' => 'Nama part aksesoris sudah digunakan.');
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

   /**
    * Delete data from table:ms_kategori_part_aksesoris
    * @param Id
    * @return page index
    **/
    function delete()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $url                = site_url() . $this->site;
            $id                 = $this->input->get("id");
            $id_part_aksesoris  = decryptID($id);
            
            $ms_part_aksesoris  = ms_part_aksesoris::where("id_part_aksesoris",$id_part_aksesoris)->first();

            if(!empty($ms_part_aksesoris))
            {
                
                $delete        = ms_part_aksesoris::where("id_part_aksesoris", $id_part_aksesoris)->delete();

                if($delete)
                {
                    $delete_detail = ms_part_aksesoris_detail::where("id_part_aksesoris", $id_part_aksesoris)->delete();
                    
                    $ms_gambar_part_aksesoris = ms_gambar_part_aksesoris::where('id_part_aksesoris', $id_part_aksesoris)->first();
                    if(!empty($ms_gambar_part_aksesoris)){
                        if (file_exists('assets/upload/part_aksesoris/aksesoris/' . $ms_gambar_part_aksesoris->gambar_part_aksesoris))
                        {
                            if(unlink('assets/upload/part_aksesoris/aksesoris/' . $ms_gambar_part_aksesoris->gambar_part_aksesoris))
                            {
                                $delete_data_gambar = ms_gambar_part_aksesoris::where('id_part_aksesoris',$id_part_aksesoris)->delete();
                            }
                        }
                    }

                    $status = array('status' => 'success', 'message' => lang('message_delete_success'), 'url' => $url);
                }
                else
                {
                    $status = array('status' => 'error', 'message' => lang('message_delete_failed'), 'url' => $url);
                }
            }
            else
            {
                $status = array('status' => 'error', 'message' => lang('message_data_not_found'), 'url' => $url);
            }

            $data  = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    function remove_image($id_part_aksesoris, $gambar_part_aksesoris)
    {
        if (file_exists('assets/upload/part_aksesoris/aksesoris/' . $gambar_part_aksesoris))
        {
            if(unlink('assets/upload/part_aksesoris/aksesoris/' . $gambar_part_aksesoris))
            {
                $ms_gambar_part_aksesoris = ms_gambar_part_aksesoris::where('id_part_aksesoris',decryptID($id_part_aksesoris))
                                                                ->where('gambar_part_aksesoris',$gambar_part_aksesoris)
                                                                ->first();
                if(!empty($ms_gambar_part_aksesoris)){
                    ms_gambar_part_aksesoris::where('id_part_aksesoris',decryptID($id_part_aksesoris))->where('gambar_part_aksesoris',$gambar_part_aksesoris)->delete();
                }

                $status = array('status' => 'success', 'message' => 'Gambar berhasil dihapus.');
            }else{
                $status = array('status' => 'error', 'message' => 'Gambar gagal dihapus.');
            }
        }
        else
        {
            $ms_gambar_part_aksesoris = ms_gambar_part_aksesoris::where('id_part_aksesoris',decryptID($id_part_aksesoris))
                                                                ->where('gambar_part_aksesoris',$gambar_part_aksesoris)
                                                                ->first();
            if(!empty($ms_gambar_part_aksesoris)){
                $ms_gambar_part_aksesoris->delete();
            }

            $status = array('status' => 'success', 'message' => 'Gambar berhasil dihapus.');
        }

        $data  = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function getImage($id_part_aksesoris)
    {
       $ms_gambar_part_aksesoris = ms_gambar_part_aksesoris::where('id_part_aksesoris', decryptID($id_part_aksesoris))->get();

       echo json_encode($ms_gambar_part_aksesoris);
    }

    function checkNamaPartAksesoris()
    {
        if ($this->input->is_ajax_request()) 
        {  
            $id                  = decryptID($this->input->post('id'));
            $nama_part_aksesoris = $this->input->post('nama_part_aksesoris');
            $result              = ms_part_aksesoris::where('nama_part_aksesoris',$nama_part_aksesoris)->first();

            if (!empty($result)) 
            {
                if (!empty($id)) 
                {
                    if ($id == $result->id_part_aksesoris) 
                    {
                        echo 'true';
                    } 
                    else 
                    {
                        echo 'false';
                    }
                } 
                else 
                {
                    echo 'false';
                }
            } 
            else 
            {
                echo 'true';
            }
        }
    }
}