<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_part_file extends Eloquent {

	public $table      = 'ms_part_file';
	public $primaryKey = 'kode_part_file';
	public $timestamps = false;

}