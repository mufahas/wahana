<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_apparel_detail extends Eloquent {

	public $table      = 'ms_apparel_detail';
	public $primaryKey = 'kode_apparel_detail';
	public $timestamps = false;

}