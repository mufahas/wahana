<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_pembeli_aksesoris extends Eloquent {

	public $table      = 'tbl_pembeli_aksesoris';
	public $primaryKey = 'id_pembeli_aksesoris';
	public $timestamps = false;

}