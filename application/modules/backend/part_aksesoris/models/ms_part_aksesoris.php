<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_part_aksesoris extends Eloquent {

	public $table      = 'ms_part_aksesoris';
	public $primaryKey = 'id_part_aksesoris';
	public $timestamps = false;

}