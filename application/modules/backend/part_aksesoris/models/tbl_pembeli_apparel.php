<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class tbl_pembeli_apparel extends Eloquent {

	public $table      = 'tbl_pembeli_apparel';
	public $primaryKey = 'id_pembeli_apparel';
	public $timestamps = false;

}