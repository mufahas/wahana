<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_apparel extends Eloquent {

	public $table      = 'ms_apparel';
	public $primaryKey = 'kode_apparel';
	public $timestamps = false;

}