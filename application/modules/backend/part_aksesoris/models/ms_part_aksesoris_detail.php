<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class ms_part_aksesoris_detail extends Eloquent {

	public $table      = 'ms_part_aksesoris_detail';
	public $primaryKey = 'kode_part_aksesoris_detail';
	public $timestamps = false;

}