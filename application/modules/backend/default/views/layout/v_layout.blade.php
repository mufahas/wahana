<!DOCTYPE html>
<html lang="en" >
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>
            Wahana | {{ get_menu_name() }}
        </title>
        <meta name="description" content="Multi column form examples">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!--end::Web font -->

        <!--begin::Base Styles -->
        <link href="{{ base_url() }}assets/default/css/vendors.bundle.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/default/css/style.bundle.css" rel="stylesheet" type="text/css" />

        <link href="{{ base_url() }}assets/backend/css/custom.css" rel="stylesheet" type="text/css" /> 
        <!--end::Base Styles -->

        <link rel="stylesheet" href="{{ base_url() }}assets/backend/css/bootzard/style.css">

        <!--begin::Base Scripts -->
        <script src="{{ base_url() }}assets/default/js/vendors.bundle.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/default/js/scripts.bundle.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/default/js/sweetalert.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/default/js/date-format.js" type="text/javascript"></script>
        <!--end::Base Scripts -->

        <link rel="shortcut icon" href="{{ base_url() }}assets/default/media/img/logo/favicon.ico" />
    </head>
    <!-- end::Head -->

    <!-- end::Body -->
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <!-- BEGIN: Header -->
            <header class="m-grid__item m-header"  data-minimize-offset="200" data-minimize-mobile-offset="200" >
                <div class="m-container m-container--fluid m-container--full-height">
                    <div class="m-stack m-stack--ver m-stack--desktop">
                        <!-- BEGIN: Brand -->
                        <div class="m-stack__item m-brand m-brand--skin-dark ">
                            <div class="m-stack m-stack--ver m-stack--general">
                                <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                    <a href="{{ base_url() }}dashboard" class="m-brand__logo-wrapper">
                                        <img alt="" src="{{ base_url() }}assets/default/media/img/users/logo-1.png" width="160" />
                                    </a>
                                </div>
                                <div class="m-stack__item m-stack__item--middle m-brand__tools">
                                    <!-- BEGIN: Left Aside Minimize Toggle -->
                                    <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
                                        <span></span>
                                    </a>
                                    <!-- END -->
                                </div>
                            </div>
                        </div>
                        <!-- END: Brand -->
                        <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                            <!-- BEGIN: Topbar -->
                            <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                                <div class="m-stack__item m-topbar__nav-wrapper">
                                    <ul class="m-topbar__nav m-nav m-nav--inline">
                                        <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
                                            <a href="#" class="m-nav__link m-dropdown__toggle">
                                                <span class="m-topbar__userpic">
                                                    <img src="{{ base_url() }}assets/default/media/img/users/user4.png" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                                                </span>
                                                <span class="m-topbar__username m--hide">
                                                    Nick
                                                </span>
                                            </a>
                                            <div class="m-dropdown__wrapper">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__header m--align-center" style="background: url({{ base_url() }}assets/default/media/img/users/user_profile_bg.jpg); background-size: cover;">
                                                        <div style="background: rgba(0,0,0,.5);z-index: 9999;height: 100%;width: 100%;">
                                                            
                                                        <div class="m-card-user m-card-user--skin-dark">
                                                            <div class="m-card-user__pic">
                                                                <img src="{{ base_url() }}assets/default/media/img/users/user4.png" class="m--img-rounded m--marginless" alt=""/>
                                                            </div>
                                                            <div class="m-card-user__details">
                                                                <span class="m-card-user__name m--font-weight-500">
                                                                    {{$sess_user->first_name}}
                                                                </span>
                                                                <a href="javascript:;" class="m-card-user__email m--font-weight-300 m-link">
                                                                   {{$sess_user->email}}
                                                                </a>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__content">
                                                            <ul class="m-nav m-nav--skin-light">
                                                                <li class="m-nav__item">
                                                                </li>
                                                                <li class="m-nav__separator m-nav__separator--fit"></li>
                                                                <li class="m-nav__item">
                                                                    <a href="{{ base_url() }}logout" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                                                        Logout
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- END: Topbar -->
                        </div>
                    </div>
                </div>
            </header>
            <!-- END: Header -->    

            <!-- begin::Body -->
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
                <!-- BEGIN: Left Aside -->
                <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
                    <!-- BEGIN: Aside Menu -->
                    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500" >
                        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

                            @include('backend.default.views.layout.v_layout_menu')

                        </ul>
                    </div>
                    <!-- END: Aside Menu -->
                </div>

                <!-- END: Left Aside -->
                <div class="m-grid__item m-grid__item--fluid m-wrapper">
                    <!-- BEGIN: Subheader -->
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title m-subheader__title--separator">
                                    {{ get_menu_name() }}
                                </h3>

                                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                    {{ get_breadcrumb() }}
                                </ul>

                            </div>
                        </div>
                    </div>

                    <!-- END: Subheader -->
                    <div class="m-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- BEGIN CONTENT BODY -->
                                @yield('body')
                                <!-- END CONTENT BODY -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end:: Body -->

            <!-- begin::Footer -->
            <footer class="m-grid__item m-footer">
                <div class="m-container m-container--fluid m-container--full-height m-page__container">
                    <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                        <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                            <span class="m-footer__copyright">
                                2018 &copy; Wahana Honda Corporate
                                <a href="#" class="m-link">
                                    <!-- Keenthemes -->
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end::Footer -->
        </div>
        <!-- end:: Page -->
        
        <!-- begin::Scroll Top -->
        <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
            <i class="la la-arrow-up"></i>
        </div>
        <!-- end::Scroll Top -->            

        <!--begin::Base Scripts -->
        <script src="{{ base_url() }}assets/default/js/jquery.number.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/default/js/input-mask.js" type="text/javascript"></script>
        <script type="text/javascript">
            var redirect_index = '{{ $redirect_index }}';
            var locale         = '{!! json_encode(getLocalformatNumber()) !!}';
        </script>
        <script src="{{ base_url() }}assets/backend/js/general-plugins.js" type="text/javascript"></script>
        <!--end::Base Scripts -->

        @section('scripts') @show
        
    </body>
    <!-- end::Body -->
</html>
