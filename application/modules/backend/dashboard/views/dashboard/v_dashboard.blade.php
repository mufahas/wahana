@extends('backend.default.views.layout.v_layout')

@section('body')

<link href="{{ base_url() }}assets/default/css/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ base_url() }}assets/default/css/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    {{ get_menu_name() }}
                </h3>
            </div>
        </div>
    </div>

    <div class="m-portlet__body">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#m_tabs_1_1" id="tab_1" aria-expanded="true">
                    Berita
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#m_tabs_1_2" id="tab_2" aria-expanded="false">
                    Promo
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#m_tabs_1_3" id="tab_3" aria-expanded="false">
                    Event
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#m_tabs_1_4" id="tab_4" aria-expanded="false">
                    Pesan
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#m_tabs_1_5" id="tab_5" aria-expanded="false">
                    Komentar
                </a>
            </li>
        </ul>
        
        <div class="tab-content">
            <div class="tab-pane active" id="m_tabs_1_1" role="tabpanel" aria-expanded="true">
                <table class="table table-striped table-bordered table-hover" id="table-berita" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul Berita</th>
                            <th>Status Publikasi</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div class="tab-pane" id="m_tabs_1_2" role="tabpanel" aria-expanded="false">
                <table class="table table-striped table-bordered table-hover" id="table-promo" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Promo</th>
                            <th>Status Promo</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div class="tab-pane" id="m_tabs_1_3" role="tabpanel" aria-expanded="false">
                <table class="table table-striped table-bordered table-hover" id="table-event" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Kegiatan</th>
                            <th>Status Publikasi</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div class="tab-pane" id="m_tabs_1_4" role="tabpanel" aria-expanded="false">
                <table class="table table-striped table-bordered table-hover" id="table-pesan" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>No. Telepon</th>
                            <th>Subyek Pesan</th>
                            <th>Isi Pesan</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div class="tab-pane" id="m_tabs_1_5" role="tabpanel" aria-expanded="false">
                <table class="table table-striped table-bordered table-hover" id="table-komentar" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Judul Berita</th>
                            <th>Isi Komentar</th>
                            <th>Status Komentar</th>
                            <th>Tanggal Komentar</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
       
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    var loadTableBerita   = '{{$loadTableBerita}}';
    var loadTablePromo    = '{{$loadTablePromo}}';
    var loadTableEvent    = '{{$loadTableEvent}}';
    var loadTablePesan    = '{{$loadTablePesan}}';
    var loadTableKomentar = '{{$loadTableKomentar}}';
    var view_             = '{!! $btn["btn_view"] !!}';
</script>

<script src="{{ base_url() }}assets/default/js/datatables.min.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/default/js/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{ base_url() }}assets/backend/js/handler-datatable.js" type="text/javascript"></script>

<script src="{{ base_url() }}assets/backend/js/dashboard/table/table-dashboard.js" type="text/javascript"></script>
@stop