<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index()
    {
        $data['loadTableBerita']   = site_url() . $this->site . '/loadTableBerita';
        $data['loadTablePromo']    = site_url() . $this->site . '/loadTablePromo';
        $data['loadTableEvent']    = site_url() . $this->site . '/loadTableEvent';
        $data['loadTablePesan']    = site_url() . $this->site . '/loadTablePesan';
        $data['loadTableKomentar'] = site_url() . $this->site . '/loadTableKomentar';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTableBerita()
    {
        $model        = "tbl_berita";
        $condition    = "tbl_berita.kategori_berita = 'B'";
        $row          = array('tbl_berita.kode_berita','tbl_berita.judul_berita','tbl_berita.status_publikasi');
        $row_search   = array('tbl_berita.kode_berita','tbl_berita.judul_berita','tbl_berita.status_publikasi');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTablePromo()
    {
        $model        = "tbl_promo";
        $condition    = "";
        $row          = array('tbl_promo.kode_promo','tbl_promo.judul_promo','tbl_promo.status_promo');
        $row_search   = array('tbl_promo.kode_promo','tbl_promo.judul_promo','tbl_promo.status_promo');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTableEvent()
    {
        $model        = "tbl_berita";
        $condition    = "tbl_berita.kategori_berita = 'E'";
        $row          = array('tbl_berita.kode_berita','tbl_berita.judul_berita','tbl_berita.status_publikasi');
        $row_search   = array('tbl_berita.kode_berita','tbl_berita.judul_berita','tbl_berita.status_publikasi');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTablePesan()
    {
        $model        = "tbl_pesan";
        $condition    = "";
        $row          = array('tbl_pesan.kode_pesan','tbl_pesan.nama_lengkap','tbl_pesan.email','tbl_pesan.nomor_telepon','tbl_pesan.subyek_pesan','tbl_pesan.tipe_pesan');
        $row_search   = array('tbl_pesan.kode_pesan','tbl_pesan.nama_lengkap','tbl_pesan.email','tbl_pesan.nomor_telepon','tbl_pesan.subyek_pesan','tbl_pesan.tipe_pesan');
        $join         = "";
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTableKomentar()
    {
        $model        = "tbl_komentar";
        $condition    = "";
        $row          = array('tbl_komentar.kode_komentar','tbl_komentar.nama','tbl_komentar.email','tbl_berita.judul_berita','tbl_komentar.isi_komentar','tbl_komentar.status_komentar','tbl_komentar.tanggal_komentar');
        $row_search   = array('tbl_komentar.kode_komentar','tbl_komentar.nama','tbl_komentar.email','tbl_berita.judul_berita','tbl_komentar.isi_komentar','tbl_komentar.status_komentar','tbl_komentar.tanggal_komentar');
        $join         = array('tbl_berita'  => 'tbl_berita.kode_berita = tbl_komentar.kode_berita');
        $order        = "";
        $groupby      = "";
        $limit        = "";
        $offset       = "";
        $distinct     = "";

        /* Get Data */
        $q            = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add()
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Direct to page update data
    * @return page
    **/
    function view($id)
    {
        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

}