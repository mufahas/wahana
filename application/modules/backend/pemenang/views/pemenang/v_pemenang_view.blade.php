@extends('backend.default.views.layout.v_layout')

@section('body')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Form {{ get_menu_name() }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! form_open(null, array('id' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'role' => 'form')) !!}
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Kuis Yang Dimenangkan:
                        </label>
                        <h5>{{ $detail->kuis_yang_dimenangkan }}</h5>
                    </div>
                    <div class="col-lg-6 ">
                        <label>
                            Nama Lengkap Pemenang:
                        </label>
                        <h5>{{ $detail->nama_lengkap_pemenang }}</h5>
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Nomor HP:
                        </label>
                        <h5>{{ $detail->no_hp_pemenang }}</h5>
                    </div>

                    <div class="col-lg-6 form-group">
                        <label>
                            Alamat:
                        </label>
                        <h5>{{ $detail->alamat_pemenang }}</h5>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Kelurahan:
                        </label>
                        <h5>{{ $detail->kelurahan }}</h5>
                    </div>

                    <div class="col-lg-6 form-group">
                        <label>
                            Kecamatan:
                        </label>
                        <h5>{{ $detail->kecamatan }}</h5>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Kota:
                        </label>
                        <h5>{{ $detail->kota }}</h5>
                    </div>

                    <div class="col-lg-6 form-group">
                        <label>
                            Kecamatan:
                        </label>
                        <h5>{{ $detail->kode_pos }}</h5>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <div class="col-lg-6 form-group">
                        <label>
                            Akun Instagram:
                        </label>
                        <h5>{{ !empty($detail->instagram_acc) ? $detail->instagram_acc : '' }}</h5>
                    </div>

                    <div class="col-lg-6 form-group">
                        <label>
                            Akun Twitter:
                        </label>
                        <h5>{{ !empty($detail->twitter_acc) ? $detail->twitter_acc : '' }}</h5>
                    </div>
                </div>
            </div>
        {!! form_close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@stop