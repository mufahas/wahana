<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pemenang extends MY_Controller
{

    public $site       = "";
    public $module     = "";
    public $folder     = "";
    public $class      = "";
    public $method     = "";

    public function __construct() 
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }

        /* Dynamical controller */
        $this->module = $this->router->fetch_module();
        $this->folder = $this->uri->segment(1);
        $this->class  = $this->router->fetch_class();
        $this->site   = $this->folder . '/' . $this->class;
        $this->method = $this->router->fetch_method();
    }

    function index() 
    {
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['view']    = site_url() . $this->site . '/view';

        $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class, $data);
    }

    /**
    * Serverside load table:
    * @return ajax
    **/
    function loadTable()
    {   
        $date                 = $this->input->post("start_date");
        $explode              = explode(" / ", $date);
        $start_date           = !empty($explode['0']) ? date('Y-m-d', strtotime($explode['0'])) : date('Y-m-d');
        $end_date             = !empty($explode['1']) ? date('Y-m-d', strtotime($explode['1'])) : date('Y-m-d');
        $condition            = "";

        if(!empty($start_date) && !empty($end_date))
        {
            $condition .= "tbl_pemenang.submit_date >= '".$start_date."' AND tbl_pemenang.submit_date <= '".$end_date."'";
        }

        $model                = "tbl_pemenang";
        $condition            = $condition;
        $row                  = array(
                                        'tbl_pemenang.id_pemenang',
                                        'tbl_pemenang.nama_lengkap_pemenang',
                                        'tbl_pemenang.kuis_yang_dimenangkan',
                                        'tbl_pemenang.no_hp_pemenang',
                                        'tbl_pemenang.alamat_pemenang',
                                        'tbl_pemenang.kecamatan',
                                        'tbl_pemenang.kelurahan',
                                        'tbl_pemenang.kota',
                                        'tbl_pemenang.kode_pos',
                                        'tbl_pemenang.twitter_acc',
                                        'tbl_pemenang.instagram_acc',
                                        'tbl_pemenang.submit_date'
                                    );
        $row_search           = array('tbl_pemenang.nama_lengkap_pemenang','tbl_pemenang.kuis_yang_dimenangkan','tbl_pemenang.no_hp_pemenang','tbl_pemenang.submit_date');
        $join                 = "";
        $order                = "";
        $groupby              = "";
        $limit                = "";
        $offset               = "";
        $distinct             = "";
        
        /* Get Data */
        $q                    = $this->datatable_model->loadTableServerSide($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct);
        return $q;
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function add() 
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/save';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function save()
    {

    }


    /**
    * Direct to page update data
    * @return page
    **/
    function edit($id)
    {
        /* Button Action */
        $data['action']    = site_url() . $this->site . '/update';

        $this->load_view("backend", $this->folder , $this->class , "v_" . $this->class . "_" . $this->method, $data);
    }

    /**
    * Save data to table:
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        
    }

    /**
    * Delete data from table:
    * @param Id
    * @return page index
    **/
    function delete()
    {
        
    }

     /**
    * View data from table: tbl_pemenang
    * @param Id
    * @return page index
    **/
    function view($id)
    {   

        $id_pemenang           = decryptID($id);
        $tbl_pemenang          = tbl_pemenang::where('tbl_pemenang.id_pemenang', $id_pemenang)->first();
      
        $data['foto_pemenang'] = base_url() . "assets/upload/pemenang/";

        if(!empty($tbl_pemenang))
        {
            $data['detail']    = $tbl_pemenang;
            
            $this->load_view("backend", $this->folder , $this->class ,"v_" . $this->class . "_" . $this->method, $data);
        }
        else
        {
            redirect(site_url() . $this->site);
        }
        
    }
}