<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Country_api extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
    }

    function save_post() 
    {
    	$data = array('status' => 'success','message' => 'yes');
    	$this->response($data);
    }
}