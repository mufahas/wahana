<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @abstract Class Model Select Function
**/

class Select_global_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    /**
     *  @abstract Function select Role for User
     *  @return Drop down
    **/
    public function selectRoleUser()
    {
        $role_options        = array();
        $role_options['']    = '- Select Role User -';
        $role                = M_group::where('groups.is_delete','f')->get();
        
        if (!empty($role)) 
        {
            foreach ($role as $role) 
            {
                $role_options[encryptID($role->id)] = $role->name;
            }
        }

        return $role_options;
    }
    
    /**
     *  @abstract Function select Role for Role Access
     *  @return Drop down
    **/
    public function selectAddRole()
    {
        $group_options        = array();
        $group_options['']    = '- Select User Level -';
        $additional_condition = 'id NOT IN (SELECT id_group FROM role_access WHERE role_access.id_group = groups.id)';
        $group                = M_group::whereRaw($additional_condition)->where('groups.is_delete','f')->get();

        if (!empty($group)) 
        {
            foreach ($group as $group) 
            {
                $group_options[$group->id] = $group->name;
            }
        }

        return $group_options;
    }

    /**
     *  @abstract Function select Kategori Jaringan 
     *  @return Drop down
    **/
    public function selectJenisBanner()
    {
        $select_jenis_banner       = array();
        $select_jenis_banner['']   = '-- Pilih Jenis Banner --';
        $select_jenis_banner['H']  = 'Home';
        $select_jenis_banner['A']  = 'Aksesoris';

        return $select_jenis_banner;
    }

   /**
     *  @abstract Function select Jenis Apparel
     *  @return Drop down
    **/
    public function selectJenisApparel()
    {
        $select_jenis_apparel       = array();
        $select_jenis_apparel['']   = '-- Pilih Jenis Apparel --';
        $select_jenis_apparel['K']  = 'Kemeja';
        $select_jenis_apparel['T']  = 'Topi';
        $select_jenis_apparel['KS'] = 'Kaos';
        $select_jenis_apparel['H']  = 'Helm';
        $select_jenis_apparel['J']  = 'Jacket';
        $select_jenis_apparel['ST'] = 'Sarung Tangan';
        $select_jenis_apparel['P']  = 'Protector';

        return $select_jenis_apparel;
    }

    /**
     *  @abstract Function select Perhitungan Jenis Bunga from table ms_kalkulator_kredit
     *  @return Drop down
    **/
    public function selectPerhitunganJenisBunga()
    {
        $status_perhitungan            = array();
        $status_perhitungan['']        = '- Pilih Perhitungan Jenis Bunga -';
        $status_perhitungan['Flat']    = 'Flat';
        $status_perhitungan['Efektif'] = 'Efektif';
        $status_perhitungan['Anuitas'] = 'Anuitas';

        return $status_perhitungan;
    }

    /**
     *  @abstract Function select Jenis Kontak from table ms_kontak_wahana
     *  @return Drop down
    **/
    public function selectJenisKontak()
    {
        $select_jenis_kontak        = array();
        $select_jenis_kontak['']    = '- Pilih Jenis Kontak -';
        $select_jenis_kontak['P']   = 'Pusat';
        $select_jenis_kontak['C']   = 'Cabang';
        $select_jenis_kontak['LK']  = 'Layanan Konsumen';
        $select_jenis_kontak['GSO'] = 'Group Sales Operation';

        return $select_jenis_kontak;
    }

    /**
     *  @abstract Function select Kategori Berita 
     *  @return Drop down
    **/
    public function selectKategoriBerita()
    {
        $select_kategori_berita       = array();
        $select_kategori_berita['']   = '-- Pilih Kategori Berita --';
        $select_kategori_berita['B']  = 'Berita';
        $select_kategori_berita['T']  = 'Tips';
        $select_kategori_berita['E']  = 'Event';
        $select_kategori_berita['PR'] = 'Press Release';

        return $select_kategori_berita;
    }

    /**
     *  @abstract Function select Berita Utama
     *  @return Drop down
    **/
    public function selectBeritaUtama()
    {
        $select_berita_utama       = array();
        $select_berita_utama['']   = '-- Pilih Berita Utama --';
        $select_berita_utama['T']  = 'Ya';
        $select_berita_utama['F']  = 'Tidak';

        return $select_berita_utama;
    }

    /**
     *  @abstract Function selectPublish
     *  @return Drop down
    **/
    public function selectPublish()
    {
        $status_publikasi       = array();
        $status_publikasi['']   = '-- Pilih Status Publikasi --';
        $status_publikasi['T']  = 'Ya';
        $status_publikasi['F']  = 'Tidak';

        return $status_publikasi;
    }

    /**
     *  @abstract Function selectTenor
     *  @return Drop down
    **/
    public function selectTenor()
    {
        $select_tenor       = array();
        $select_tenor['11'] = '11';
        $select_tenor['17'] = '17';
        $select_tenor['23'] = '23';
        $select_tenor['27'] = '27';
        $select_tenor['29'] = '29';
        $select_tenor['33'] = '33';
        $select_tenor['35'] = '35';

        return $select_tenor;
    }

     /**
     *  @abstract Function select Bulan
     *  @return Drop down
    **/
    public function selectBulan()
    {
        $select_bulan       = array();
        $select_bulan['']   = '-- Pilih Bulan --';
        $select_bulan['1']  = 'Januari';
        $select_bulan['2']  = 'Februari';
        $select_bulan['3']  = 'Maret';
        $select_bulan['4']  = 'April';
        $select_bulan['5']  = 'Mei';
        $select_bulan['6']  = 'Juni';
        $select_bulan['7']  = 'Juli';
        $select_bulan['8']  = 'Agustus';
        $select_bulan['9']  = 'September';
        $select_bulan['10'] = 'Oktober';
        $select_bulan['11'] = 'November';
        $select_bulan['12'] = 'Desember';

        return $select_bulan;
    }
    
    /**
     *  @abstract Function Select Lokasi Test Ride
     *  @return Drop down
    **/
    public function selectLokasiTestride()
    {
        $select_lokasi_testride = array();

        $select_lokasi_testride['']               = '-- Pilih Lokasi Test Ride --';
        $select_lokasi_testride[encryptID('JKT')] = 'Wahana Makmur Sejati - Jakarta';
        $select_lokasi_testride[encryptID('TGR')] = 'Wahana Jetake - Tangerang';

        return $select_lokasi_testride;
    }

    /**
     *  @abstract Function select Label
     *  @return Drop down
    **/
    public function selectLabel() 
    {
        $select_label     = array();
        $label            = ms_label::where('ms_label.dihapus','F')->get();
        
        if (!empty($label)) 
        {
            foreach ($label as $label) 
            {
                $select_label[encryptID($label->kode_label)] = $label->nama_label;
            }
        }

        return $select_label;
    }

    /**
     *  @abstract Function select Kategori Part Aksesoris from table ms_kategori_part_aksesoris
     *  @return Drop down
    **/
    public function selectKategoriPartAksesoris() 
    {
        $select_kategori_part_aksesoris     = array();
        $select_kategori_part_aksesoris[''] = '- Pilih Kategori Aksesoris -';
        $kategori_part_aksesoris            = ms_kategori_part_aksesoris::where('ms_kategori_part_aksesoris.dihapus','F')->get();
        
        if (!empty($kategori_part_aksesoris)) 
        {
            foreach ($kategori_part_aksesoris as $kategori_part_aksesoris) 
            {
                $select_kategori_part_aksesoris[encryptID($kategori_part_aksesoris->kode_kategori_part_aksesoris)] = $kategori_part_aksesoris->nama_kategori_part_aksesoris;
            }
        }

        return $select_kategori_part_aksesoris;
    }


    /**
     *  @abstract Function select Kategori Motor from table ms_kategori_motor
     *  @return Drop down
    **/
    public function selectKategoriMotor() 
    {
        $select_kategori_motor     = array();
        $select_kategori_motor[''] = '- Pilih Kategori Motor -';
        $kategori_motor            = ms_kategori_motor::where('ms_kategori_motor.dihapus','F')->get();
        
        if (!empty($kategori_motor)) 
        {
            foreach ($kategori_motor as $kategori_motor) 
            {
                $select_kategori_motor[$kategori_motor->kode_kategori_motor] = $kategori_motor->nama_kategori_motor;
            }
        }

        return $select_kategori_motor;
    }

    /**
     *  @abstract Function select Provinsi from table ms_kota_kabupaten
     *  @return Drop down
    **/
    public function selectKotaKabupaten() 
    {
        $select_kota_kabupaten     = array();
        $select_kota_kabupaten[''] = '- Pilih Kota Kabupaten -';
        $kota_kabupaten            = ms_kota_kabupaten::where('ms_kota_kabupaten.dihapus','F')->get();
        
        if (!empty($kota_kabupaten)) 
        {
            foreach ($kota_kabupaten as $kota_kabupaten) 
            {
                $select_kota_kabupaten[encryptID($kota_kabupaten->kode_kota_kabupaten)] = $kota_kabupaten->nama_kota_kabupaten;
            }
        }

        return $select_kota_kabupaten;
    }

    /**
     *  @abstract Function select Provinsi from table ms_provinsi
     *  @return Drop down
    **/
    public function selectProvinsi() 
    {
        $select_provinsi     = array();
        $select_provinsi[''] = '- Pilih Provinsi -';
        $provinsi            = ms_provinsi::where('ms_provinsi.dihapus','F')->get();
        
        if (!empty($provinsi)) 
        {
            foreach ($provinsi as $provinsi) 
            {
                $select_provinsi[encryptID($provinsi->kode_provinsi)] = $provinsi->nama_provinsi;
            }
        }

        return $select_provinsi;
    }

    /**
     *  @abstract Function select Status Uji Coba
     *  @return Drop down
    **/
    // public function selectStatusUjiCoba()
    // {
    //     $select_status_uji_coba        = array();
    //     $select_status_uji_coba['']    = '- Pilih Status Uji Coba -';
    //     $select_status_uji_coba['Y']   = 'Ya';
    //     $select_status_uji_coba['T']   = 'Tidak';

    //     return $select_status_uji_coba;
    // }

    /**
     *  @abstract Function select Tipe Mesin from table ms_tipe_mesin
     *  @return Drop down
    **/
    public function selectTipeMesin()
    {
        $select_tipe_mesin     = array();
        $select_tipe_mesin[''] = '- Pilih Provinsi -';
        $tipe_mesin            = ms_tipe_mesin::where('ms_tipe_mesin.dihapus','F')->get();
        
        if (!empty($tipe_mesin)) 
        {
            foreach ($tipe_mesin as $tipe_mesin) 
            {
                $select_tipe_mesin[encryptID($tipe_mesin->kode_tipe_mesin)] = $tipe_mesin->nama_tipe_mesin;
            }
        }

        return $select_tipe_mesin;
    }

    /**
     *  @abstract Function select Kategori Jaringan 
     *  @return Drop down
    **/
    public function selectKategoriJaringan()
    {
        $select_kategori_jaringan         = array();
        $select_kategori_jaringan['']     = '-- Pilih Kategori Jaringan --';
        $select_kategori_jaringan['H1']   = 'Penjualan';
        $select_kategori_jaringan['H2']   = 'Pemeliharaan';
        $select_kategori_jaringan['H3']   = 'Suku Cadang';

        return $select_kategori_jaringan;
    }

    /**
     *  @abstract Function select Tipe Kopling from table ms_tipe_kopling
     *  @return Drop down
    **/
    public function selectTipeKopling()
    {
        $select_tipe_kopling     = array();
        $select_tipe_kopling[''] = '- Pilih Tipe Kopling -';
        $tipe_kopling            = ms_tipe_kopling::where('ms_tipe_kopling.dihapus','F')->get();
        
        if (!empty($tipe_kopling)) 
        {
            foreach ($tipe_kopling as $tipe_kopling) 
            {
                $select_tipe_kopling[encryptID($tipe_kopling->kode_tipe_kopling)] = $tipe_kopling->nama_tipe_kopling;
            }
        }

        return $select_tipe_kopling;
    }

    /**
     *  @abstract Function select Tipe Rangka from table ms_tipe_rangka
     *  @return Drop down
    **/
    public function selectTipeRangka()
    {
        $select_tipe_rangka     = array();
        $select_tipe_rangka[''] = '- Pilih Tipe Rangka -';
        $tipe_rangka            = ms_tipe_rangka::where('ms_tipe_rangka.dihapus','F')->get();
        
        if (!empty($tipe_rangka)) 
        {
            foreach ($tipe_rangka as $tipe_rangka) 
            {
                $select_tipe_rangka[encryptID($tipe_rangka->kode_tipe_rangka)] = $tipe_rangka->nama_tipe_rangka;
            }
        }

        return $select_tipe_rangka;
    }

    /**
     *  @abstract Function select Tipe Starter
     *  @return Drop down
    **/
    public function selectTipeStarter()
    {
        $select_tipe_starter                      = array();
        $select_tipe_starter['']                  = '-- Pilih Tipe Starter --';
        $select_tipe_starter['Kaki']              = 'Starter Kaki';
        $select_tipe_starter['Elektrik']          = 'Starter Elektrik';
        $select_tipe_starter['Kaki dan Elektrik'] = 'Starker Kaki dan Elektrik';

        return $select_tipe_starter;
    }

    /**
     *  @abstract Function select Pola Pengoperan Gigi
     *  @return Drop down
    **/
    public function selectPolaPengoperanGigi()
    {
        $select_pola_pengoperan_gigi                  = array();
        $select_pola_pengoperan_gigi['']              = '-- Pilih Pola Pengoperan Gigi --';
        $select_pola_pengoperan_gigi['1-N-2-3-4']     = '1-N-2-3-4';
        $select_pola_pengoperan_gigi['1-N-2-3-4-5']   = '1-N-2-3-4-5';
        $select_pola_pengoperan_gigi['1-N-2-3-4-5-6'] = '1-N-2-3-4-5-6';

        return $select_pola_pengoperan_gigi;
    }

    /**
     *  @abstract Function select Tipe Transmisi
     *  @return Drop down
    **/
    public function selectTipeTransmisi()
    {
        $select_tipe_transmisi                  = array();
        $select_tipe_transmisi['']              = '-- Pilih Tipe Transmisi --';   
        $select_tipe_transmisi['Matic']         = 'Automatic';
        $select_tipe_transmisi['4 Kecepatan']   = '4 Kecepatan';
        $select_tipe_transmisi['5 Kecepatan']   = '5 Kecepatan';
        $select_tipe_transmisi['6 Kecepatan']   = '6 Kecepatan';
    
        return $select_tipe_transmisi;
    }

    /**
     *  @abstract Function select produk from table ms_produk
     *  @return Drop down
    **/
    public function selectProduct()
    {
        $select_produk     = array();
        $produk            = ms_produk::where('ms_produk.dihapus','F')->get();
        
        if (!empty($produk)) 
        {
            foreach ($produk as $produk) 
            {
                $select_produk[encryptID($produk->kode_produk)] = $produk->nama_produk;
            }
        }

        return $select_produk;
    }

    /**
     *  @abstract Function select produk terbaik
     *  @return Drop down
    **/
    public function selectProductTerbaik()
    {
        $select_produk_terbaik        = array();
        $select_produk_terbaik['']    = '- Pilih Produk Terbaik -';

        $produk = ms_produk::selectRaw('count(*) as jumlah_produk_terbaik')->where('ms_produk.dihapus','F')->where('ms_produk.produk_terbaik','T')->first();

        if ($produk->jumlah_produk_terbaik >= "9") {
            $select_produk_terbaik['F']   = 'Tidak';
        }else{
            $select_produk_terbaik['T']   = 'Ya';
            $select_produk_terbaik['F']   = 'Tidak';
        }

        return $select_produk_terbaik;
    }

    /**
     *  @abstract Function select urutan produk terbaik
     *  @return Drop down
    **/
    public function selectUrutanProductTerbaik($kode_produk=null)
    {
        $select_urutan_produk     = array();
        $select_urutan_produk[''] = '- Pilih Urutan Produk Terbaik -';
        
        if(!empty($kode_produk))
        {
            $produk            = ms_produk::where('ms_produk.dihapus','F')->whereRaw('ms_produk.kode_produk != "'.$kode_produk.'"')->get(); 
        }
        else
        {
            $produk            = ms_produk::where('ms_produk.dihapus','F')->get();  
        }
        foreach ($produk as $produk) 
        {
            $urutan_terpakai[] = $produk->urutan_produk_terbaik;
        }
        
        for ($i=1; $i <= 9; $i++) { 
            if (!empty($urutan_terpakai)) {
                if (!in_array($i, $urutan_terpakai)) {
                    $select_urutan_produk[$i] = $i;
                }
            }else{
                $select_urutan_produk[$i] = $i;
            }
        }

        return $select_urutan_produk;
    }

    /**
     *  @abstract Function select Jenis Part Aksesoris
     *  @return Drop down
    **/
    public function selectJenisPartAksesoris()
    {
        $select_jenis_part_aksesoris      = array();
        $select_jenis_part_aksesoris['']  = '-- Pilih Jenis Part Aksesoris --';
        $select_jenis_part_aksesoris['P'] = 'Part';
        $select_jenis_part_aksesoris['A'] = 'Aksesoris';

        return $select_jenis_part_aksesoris;
    }


    /**
     *  @abstract Function select Status Promo
     *  @return Drop down
    **/
    public function selectStatusPromo()
    {
        $select_status_promo      = array();
        $select_status_promo['']  = '-- Pilih Status Promo --';
        $select_status_promo['Y'] = 'Promo';
        $select_status_promo['T'] = 'Tidak Promo';

        return $select_status_promo;
    }

    /**
     *  @abstract Function select Status Ketersediaan Stok
     *  @return Drop down
    **/
    public function selectStatusKetersediaanStok()
    {
        $select_ketersediaan_stok                   = array();
        $select_ketersediaan_stok['']               = '-- Pilih Status Ketersediaan Stok --';
        $select_ketersediaan_stok['Tersedia']       = 'Tersedia';
        $select_ketersediaan_stok['Tidak Tersedia'] = 'Tidak Tersedia';

        return $select_ketersediaan_stok;
    }

    /**
     *  @abstract Function select produk from table ms_produk
     *  @return Drop down
    **/
    // public function selectRekomendasiProduk($kode = null)
    // {
    //     $select_produk      = array();
    //     $select_produk['']  = '-- Pilih Produk --';
    //     $produk             = ms_produk::where('ms_produk.dihapus','F')->get();
    //     $produk_rekomendasi = ms_rekomendasi_produk::where('ms_rekomendasi_produk.dihapus','F')->get();
    //     foreach ($produk_rekomendasi as $rekom){
    //         if ($kode != null) {
    //             if ($rekom->kode_produk != $kode) {
    //                 $produk_rekom[] = $rekom->kode_produk;
    //             }
    //         }else{
    //             $produk_rekom[] = $rekom->kode_produk;
    //         }
    //     }
        
    //     if (!empty($produk)) 
    //     {
    //         foreach ($produk as $produk) 
    //         {
    //             if(!empty($produk_rekom)){
    //                 if(!in_array($produk->kode_produk, $produk_rekom)){
    //                     $select_produk[encryptID($produk->kode_produk)] = $produk->nama_produk;
    //                 }
    //             }else{
    //                 $select_produk[encryptID($produk->kode_produk)] = $produk->nama_produk;
    //             }
    //         }
    //     }

    //     return $select_produk;
    // }
   
    /**
     *  @abstract Function select produk from table ms_harga_otr
     *  @return Drop down
    **/
    public function selectProdukHargaOtr()
    {
        // $where = "ms_harga_otr.list_harga_otr = ''";
        $select_produk     = array();
        $select_produk[''] = '- Pilih Produk -';
        $produk            = ms_harga_otr::select('ms_harga_otr.kode_harga_otr','ms_harga_otr.nama_produk_otr','ms_harga_otr.list_harga_otr')
                                          ->join('ms_produk','ms_produk.kode_produk','=','ms_harga_otr.kode_produk')
                                          ->leftjoin('ms_kalkulator_cicilan','ms_kalkulator_cicilan.kode_harga_otr','=','ms_harga_otr.kode_harga_otr')
                                          ->where('ms_produk.dihapus','F')
                                          ->where('ms_harga_otr.list_harga_otr','')
                                          // ->whereRaw($where)
                                          // ->groupBy('ms_kalkulator_cicilan.kode_harga_otr')
                                          ->get();
        
        if (!empty($produk)) 
        {
            foreach ($produk as $produk) 
            {
                $select_produk[encryptID($produk->kode_harga_otr)] = $produk->nama_produk_otr;
            }
        }

        return $select_produk;
    }

    /**
     *  @abstract Function select dp from table ms_kalkulator_cicilan
     *  @return Drop down
    **/ 
    public function selectDP()
    {
        $select_dp     = array();
        $select_dp[''] = '- Pilih DP -';
        $dp            = ms_kalkulator_cicilan::selectRaw('dp')->get();

        if(!empty($dp))
        {
            foreach ($dp as $dp) 
            {
                $select_dp[$dp->dp] = 'Rp. ' . number_format($dp->dp,2,',','.');
            }
        }

        return $select_dp;
    }

    /**
     *  @abstract Function select dp from table ms_kalkulator_cicilan berdsarkan kode_produk dari ms_harga_otr
     *  @return Drop down
    **/ 
    public function selectDPProduk($kode_harga_otr)
    {
        $select_dp     = array();
        $select_dp[''] = '- Pilih DP -';
        $uang_dp       = ms_kalkulator_cicilan::join('ms_harga_otr','ms_harga_otr.kode_harga_otr','=','ms_kalkulator_cicilan.kode_harga_otr')
                                                ->where('ms_harga_otr.kode_harga_otr',$kode_harga_otr)
                                                ->where('ms_kalkulator_cicilan.dp','!=',null)
                                                ->get();
        if(!empty($uang_dp))
        {
            foreach ($uang_dp as $dp) {
                $select_dp[$dp->dp] = 'Rp. ' . number_format($dp->dp,2,',','.');
            }
        }

        return $select_dp;
    }

    /**
     *  @abstract Function select varian produk from table ms_harga_otr 
     *  @return Drop down
    **/
    public function selectVarianProduk($kategori_motor=null)
    {
        $select_varian      = array('');
        $select_varian['']  = '- Pilih Produk -';
        $varians            = ms_harga_otr::join('ms_produk','ms_produk.kode_produk','=','ms_harga_otr.kode_produk')
                                     ->join('ms_kategori_motor','ms_kategori_motor.kode_kategori_motor','=','ms_produk.kode_kategori_motor')
                                     ->where('ms_harga_otr.list_harga_otr','!=','')
                                     ->where('ms_produk.kode_kategori_motor',$kategori_motor)
                                     ->where('ms_produk.status_tampil','T')
                                     ->where('ms_produk.dihapus','F')
                                     ->get();

        if(!empty($varians))
        {
            foreach ($varians as $varian) {
                $select_varian[encryptID($varian->kode_harga_otr)] = $varian->nama_produk_otr;
            }
        }

        return $select_varian;
    }
    
    public function selectUrutanProdukTerbaru($produk_terbaik,$kode_produk=null)
    {
        $select_urutan_produk = array();
        $select_urutan_produk['-'] = '- Pilih Urutan Produk -';
    
        if($produk_terbaik == 'T')
        {   
            if(!empty($kode_produk))
            {
                $produk            = ms_produk::where('ms_produk.dihapus','F')->whereRaw('ms_produk.kode_produk != "'.$kode_produk.'"')->get();
            }
            else
            {
                $produk            = ms_produk::where('ms_produk.dihapus','F')->get();
            }
            foreach ($produk as $produk) 
            {
                $urutan_terpakai[] = $produk->urutan_produk_terbaik;
            }
            
            for ($i=1; $i <= 9; $i++) { 
                if (!empty($urutan_terpakai)) {
                    if (!in_array($i, $urutan_terpakai)) {
                        $select_urutan_produk[$i] = $i;
                    }
                }else{
                    $select_urutan_produk[$i] = $i;
                }
            } 
        }
        else
        {
            $select_urutan_produk['-'] = '- Pilih Urutan Produk -';
        }

        return $select_urutan_produk;
    }
    
    /*----------------------------------------Select Untuk Bagian Front End Jaringan----------------------------------------*/
    public function getJaringanBasedOnFilter($area,$penjualan,$pemeliharaan,$sukucadang)
    {   
        // print_r($area);
        // print_r($penjualan);
        // print_r($pemeliharaan);
        // print_r($sukucadang);
        // die();
        $list_jaringan = array();

        // All Field Filter Empty
        if(empty($area) && empty($penjualan) && empty($pemeliharaan) && empty($sukucadang))
        {
            $list_jaringan  = ms_jaringan_wahana::select('ms_jaringan_wahana.latitude','ms_jaringan_wahana.longitude', 'ms_jaringan_wahana.nama_dealer','ms_jaringan_wahana.alamat_jaringan', 'ms_jaringan_wahana.nomor_telepon', 'ms_kota_kabupaten.nama_kota_kabupaten', 'ms_jaringan_wahana.kategori_jaringan', 'ms_jaringan_wahana.link_peta')
                              ->join('ms_kota_kabupaten', 'ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')
                              ->where('ms_jaringan_wahana.dihapus', 'F')
                              ->get();
        } 

        // Only Area Filter Selected
        if(!empty($area) && empty($penjualan) && empty($pemeliharaan) && empty($sukucadang))
        {
            $list_jaringan  = ms_jaringan_wahana::select('ms_jaringan_wahana.latitude','ms_jaringan_wahana.longitude', 'ms_jaringan_wahana.nama_dealer','ms_jaringan_wahana.alamat_jaringan', 'ms_jaringan_wahana.nomor_telepon', 'ms_kota_kabupaten.nama_kota_kabupaten', 'ms_jaringan_wahana.kategori_jaringan', 'ms_jaringan_wahana.link_peta')
                              ->join('ms_kota_kabupaten', 'ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')
                              ->where('ms_jaringan_wahana.dihapus', 'F')
                              ->where('ms_jaringan_wahana.kode_kota_kabupaten', $area)
                              ->get();
        }

        // Area and H1 Penjualan Selected
        if(!empty($area) && !empty($penjualan) && empty($pemeliharaan) && empty($sukucadang))
        {
            $list_jaringan  = ms_jaringan_wahana::select('ms_jaringan_wahana.latitude','ms_jaringan_wahana.longitude', 'ms_jaringan_wahana.nama_dealer','ms_jaringan_wahana.alamat_jaringan', 'ms_jaringan_wahana.nomor_telepon', 'ms_kota_kabupaten.nama_kota_kabupaten', 'ms_jaringan_wahana.kategori_jaringan', 'ms_jaringan_wahana.link_peta')
                              ->join('ms_kota_kabupaten', 'ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')
                              ->where('ms_jaringan_wahana.dihapus', 'F')
                              ->where('ms_jaringan_wahana.kode_kota_kabupaten', $area)
                              ->where('ms_jaringan_wahana.kategori_jaringan', $penjualan)
                              ->get();
        }

        // Area and H2 Pemeliharaan Selected
        if(!empty($area) && empty($penjualan) && !empty($pemeliharaan) && empty($sukucadang))
        {
            $list_jaringan  = ms_jaringan_wahana::select('ms_jaringan_wahana.latitude','ms_jaringan_wahana.longitude', 'ms_jaringan_wahana.nama_dealer','ms_jaringan_wahana.alamat_jaringan', 'ms_jaringan_wahana.nomor_telepon', 'ms_kota_kabupaten.nama_kota_kabupaten', 'ms_jaringan_wahana.kategori_jaringan', 'ms_jaringan_wahana.link_peta')
                              ->join('ms_kota_kabupaten', 'ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')
                              ->where('ms_jaringan_wahana.dihapus', 'F')
                              ->where('ms_jaringan_wahana.kode_kota_kabupaten', $area)
                              ->where('ms_jaringan_wahana.kategori_jaringan', $pemeliharaan)
                              ->get();
        }

        // Area and H3 Suku Cadang Selected
        if(!empty($area) && empty($penjualan) && empty($pemeliharaan) && !empty($sukucadang))
        {
            $list_jaringan  = ms_jaringan_wahana::select('ms_jaringan_wahana.latitude','ms_jaringan_wahana.longitude', 'ms_jaringan_wahana.nama_dealer','ms_jaringan_wahana.alamat_jaringan', 'ms_jaringan_wahana.nomor_telepon', 'ms_kota_kabupaten.nama_kota_kabupaten', 'ms_jaringan_wahana.kategori_jaringan', 'ms_jaringan_wahana.link_peta')
                              ->join('ms_kota_kabupaten', 'ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')
                              ->where('ms_jaringan_wahana.dihapus', 'F')
                              ->where('ms_jaringan_wahana.kode_kota_kabupaten', $area)
                              ->where('ms_jaringan_wahana.kategori_jaringan', $sukucadang)
                              ->get();
        }

        // Area, H1, And H2 Selected
        if(!empty($area) && !empty($penjualan) && !empty($pemeliharaan) && empty($sukucadang))
        {
            $list_jaringan  = ms_jaringan_wahana::select('ms_jaringan_wahana.latitude','ms_jaringan_wahana.longitude', 'ms_jaringan_wahana.nama_dealer','ms_jaringan_wahana.alamat_jaringan', 'ms_jaringan_wahana.nomor_telepon', 'ms_kota_kabupaten.nama_kota_kabupaten', 'ms_jaringan_wahana.kategori_jaringan', 'ms_jaringan_wahana.link_peta')
                              ->join('ms_kota_kabupaten', 'ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')
                              ->where('ms_jaringan_wahana.dihapus', 'F')
                              ->where('ms_jaringan_wahana.kode_kota_kabupaten', $area)
                              ->whereRaw('(ms_jaringan_wahana.kategori_jaringan = "'.$penjualan.'" OR ms_jaringan_wahana.kategori_jaringan = "'.$pemeliharaan.'")')
                              ->get();
        } 

        // Area, H1, and H3 Selected
        if(!empty($area) && !empty($penjualan) && empty($pemeliharaan) && !empty($sukucadang))
        {
            $list_jaringan  = ms_jaringan_wahana::select('ms_jaringan_wahana.latitude','ms_jaringan_wahana.longitude', 'ms_jaringan_wahana.nama_dealer','ms_jaringan_wahana.alamat_jaringan', 'ms_jaringan_wahana.nomor_telepon', 'ms_kota_kabupaten.nama_kota_kabupaten', 'ms_jaringan_wahana.kategori_jaringan', 'ms_jaringan_wahana.link_peta')
                              ->join('ms_kota_kabupaten', 'ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')
                              ->where('ms_jaringan_wahana.dihapus', 'F')
                              ->where('ms_jaringan_wahana.kode_kota_kabupaten', $area)
                              ->whereRaw('(ms_jaringan_wahana.kategori_jaringan = "'.$penjualan.'" OR ms_jaringan_wahana.kategori_jaringan = "'.$sukucadang.'")')
                              ->get();
        } 

        // Area, H2, and H3 Selected
        if(!empty($area) && empty($penjualan) && !empty($pemeliharaan) && !empty($sukucadang))
        {
            $list_jaringan  = ms_jaringan_wahana::select('ms_jaringan_wahana.latitude','ms_jaringan_wahana.longitude', 'ms_jaringan_wahana.nama_dealer','ms_jaringan_wahana.alamat_jaringan', 'ms_jaringan_wahana.nomor_telepon', 'ms_kota_kabupaten.nama_kota_kabupaten', 'ms_jaringan_wahana.kategori_jaringan', 'ms_jaringan_wahana.link_peta')
                              ->join('ms_kota_kabupaten', 'ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')
                              ->where('ms_jaringan_wahana.dihapus', 'F')
                              ->where('ms_jaringan_wahana.kode_kota_kabupaten', $area)
                              ->whereRaw('(ms_jaringan_wahana.kategori_jaringan = "'.$pemeliharaan.'" OR ms_jaringan_wahana.kategori_jaringan = "'.$sukucadang.'")')
                              ->get();
        } 

        // All Filter Selected
        if(!empty($area) && !empty($penjualan) && !empty($pemeliharaan) && !empty($sukucadang))
        {
            $list_jaringan  = ms_jaringan_wahana::select('ms_jaringan_wahana.latitude','ms_jaringan_wahana.longitude', 'ms_jaringan_wahana.nama_dealer','ms_jaringan_wahana.alamat_jaringan', 'ms_jaringan_wahana.nomor_telepon', 'ms_kota_kabupaten.nama_kota_kabupaten', 'ms_jaringan_wahana.kategori_jaringan', 'ms_jaringan_wahana.link_peta')
                              ->join('ms_kota_kabupaten', 'ms_kota_kabupaten.kode_kota_kabupaten','=','ms_jaringan_wahana.kode_kota_kabupaten')
                              ->where('ms_jaringan_wahana.dihapus', 'F')
                              ->where('ms_jaringan_wahana.kode_kota_kabupaten', $area)
                              ->whereRaw('(ms_jaringan_wahana.kategori_jaringan = "'.$penjualan.'" OR ms_jaringan_wahana.kategori_jaringan = "'.$pemeliharaan.'" OR ms_jaringan_wahana.kategori_jaringan = "'.$sukucadang.'")')
                              ->get();
        }
        
        return $list_jaringan;
    }

    public function selectKendaraanTestride($lokasi)
    {
        $select_kendaraan_testride          = array();
        $select_kendaraan_testride['']     = '-- Pilih Tipe Motor --';

        if(!empty($lokasi))
        {
            if($lokasi == 'JKT')
            {
                $select_kendaraan_testride[encryptID('PCX150')]    = 'Honda PCX150'; 
                $select_kendaraan_testride[encryptID('CBR250')]    = 'Honda CBR250'; 
                $select_kendaraan_testride[encryptID('CRF250')]    = 'Honda CRF250'; 
                $select_kendaraan_testride[encryptID('ADV')]       = 'Honda ADV'; 
                $select_kendaraan_testride[encryptID('GENIO')]     = 'Honda Genio'; 
            } 
            else
            {
                $select_kendaraan_testride[encryptID('PCX150')]    = 'Honda PCX150'; 
                $select_kendaraan_testride[encryptID('CBR250')]    = 'Honda CBR250'; 
                $select_kendaraan_testride[encryptID('CRF250')]    = 'Honda CRF250'; 
                $select_kendaraan_testride[encryptID('ADV')]       = 'Honda ADV'; 
                $select_kendaraan_testride[encryptID('GENIO')]     = 'Honda Genio';
            }
            
        }
        
        return $select_kendaraan_testride;
    }

    /*----------------------------------------Select Untuk Bagian Front End Jaringan----------------------------------------*/
    
    /*----------  FRONTEND 2019  ----------*/

    /*
    * @abstract select dropdown harga otr produk parameter kode_produk
    **/
    public function selectProdukOtr($kode_produk)
    {
        
        $select_produk_otr  = array('');
        $select_produk_otr[''] = '- Pilih Varian Produk -';

        $produk_otr = ms_harga_otr::select(
                                            'kode_harga_otr',
                                            'nama_produk_otr',
                                            'harga_produk_otr'
                                        )
                                    ->join('ms_produk','ms_produk.kode_produk','=','ms_harga_otr.kode_produk')
                                    ->where('ms_harga_otr.list_harga_otr','!=',null)
                                    ->where('ms_produk.status_tampil','T')
                                    ->where('ms_produk.dihapus','F')
                                    ->where('ms_produk.kode_produk',$kode_produk)
                                    ->get();
        if(!empty($produk_otr))
        {
            foreach ($produk_otr as $protr) {
                $select_produk_otr[encryptID($protr->kode_harga_otr)] = $protr->nama_produk_otr;
            }
        }

        return $select_produk_otr;
    }

    /*----------  FRONTEND 2019  ----------*/

}
