<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  @abstract Class Model All Setting Menu
 *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
**/

class Setting_menu_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_list_menu()
    {   
        $q = M_menu::selectRaw('menu.*')
                   ->where('menu.menu_status','1')
                   ->where('menu.menu_parent','0')
                   ->orderBy('menu.menu_order','ASC')
                   ->get();

        return $q;
    }

    function get_list_child_($parent="")
    {
        $q = M_menu::selectRaw('menu.*')
                   ->where('menu.menu_status','1')
                   ->where('menu.menu_parent',$parent)
                   ->orderBy('menu.menu_order','ASC')
                   ->get();

        return $q;


    }

    function get_list_child($id_menu="",$sub="")
    {
        $data           = array();
        $get_list_child = $this->get_list_child_($id_menu);
        $space          = "";
        for ($i=0; $i < $sub; $i++)
        { 
            $space .= "&nbsp; &nbsp;";
        }
        $sub            = $sub + 1;

        if (count($get_list_child) > 0)
        {
            foreach ($get_list_child as $row)
            {

                $data[$row->id_menu] = $space . "- " . $row->menu_name;

                if (empty($row->menu_class))
                {
                    $sub_menu = $this->get_list_child($row->id_menu, $sub);
                    foreach ($sub_menu['data'] as $key => $value)
                    {
                        $data[$key] = $value;
                    }
                }

            }
        }

        return array('data' => $data);
    }

    function get_list_button()
    {
        $q = M_button::selectRaw('id_button, button_name, button_title')
                     ->where('is_privilege','1')
                     ->orderBy('id_button','ASC')
                     ->get();

        return $q;
    }

    function get_list_menubutton($id_menu="",$id_button="")
    {
        $q = M_menu_button::selectRaw('id_menu_button')
                          ->where('id_menu',$id_menu)
                          ->where('id_button', $id_button)
                          ->first();

        return $q;
    }

    function get_select_parent()
    {
        $result    = $this->get_list_menu();

        $data['']  = "-- Select Menu Parent --";
        $data['0'] = "-- Select As Menu Parent --";

        if(count($result) > 0)
        {
            $sub = 1;
            foreach ($result as $row)
            {
                $data[$row->id_menu] = $row->menu_name;

                if (empty($row->menu_class))
                {
                    $sub_menu = $this->get_list_child($row->id_menu, $sub);
                    foreach ($sub_menu['data'] as $key => $value)
                    {
                        $data[$key] = $value;
                    }
                }
            }
        }

        return $data;
    }

    function get_select_icon()
    {

        $icon = M_icon::orderBy('icon_name')->get();

        $data['']  = "-- Select Icon --";

        if(!empty($icon))
        {
            foreach ($icon as $row)
            {
                $data[$row->id_icon] = '<i class="'.$row->icon_name.'"></i>' . $row->icon_name;
            }

            return $data;
        }
    }

}