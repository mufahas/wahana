<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @abstract Class Model Datatable Serverside
**/

class Datatable_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Function Datatable Serverside
     * @return ajax
    **/
    private function _get_datatables_query($model="", $condition="", $row="", $row_search="", $join="", $order="", $groupby="", $limit="", $offset="", $distinct="", $globals=true)
    {        
        /* Model Name */
        $q = $model::query(); //Eloquent Model
 
        /* Search */
        // if datatable send POST for search
        if($this->input->post("search")['value'])
        {
            $cond   = array();
            $i      = 0;
            $cond[] = '(';
            foreach ($row_search as $item)
            {  
                if ($i===0)
                {
                    $cond[] = 'CAST('.$item.' as CHAR) LIKE "%' . $this->input->post("search")['value'] . '%"';
                }
                else
                {
                    $cond[] = 'OR CAST('.$item.' as CHAR) LIKE "%' . $this->input->post("search")['value'] . '%"';
                }
                $i++;
            }
            $cond[]                = ')';
            $real_search_condition = implode(" ",$cond);

            $q->whereRaw($real_search_condition);
        }

        /* Distinct */
        if (!empty($distinct) && $distinct == true)
            $q->distinct();

        /* Set Field To Select */
        if (is_array($row) && !empty($row))
        {
            $field = implode(",", $row);
            $q->selectRaw($field);
        }

        /* Set Condition */
        if ($condition)
        {
            $q->whereRaw($condition);
        }

        /* Set Join */
        if (is_array($join) && !empty($join))
        {
            foreach ($join as $key => $value)
            {   
                $val = preg_split("/[\=,]+/", $value);
  
                $join_1 = trim($val[0]);
                $join_2 = trim($val[1]);

                if(!empty($val[2])){
                    $type   = trim($val[2]);
                    $q->$type($key,$join_1,'=',$join_2);
                }else{
                    $q->join($key,$join_1,'=',$join_2);
                }
            }
        }

        /* Set Limit And Offset */
        if ($limit != 0 || $offset != 0)
            $q->limit($limit,$offset);

        /* Set Group By */
        if ($groupby)
            $q->groupBy($groupby);
        
        
        /* Set Ordering */
        if (is_array($order) && !empty($order))
        {
            foreach ($order as $key => $value)
            {
                if (!empty($value))
                {
                    $q->orderBy($key,$value);
                }
                else
                {
                    $q->orderBy($key);
                }
            }
        }
        else
        {
            $q->orderBy($row[$this->input->post("order")['0']['column']],$this->input->post("order")['0']['dir']);
        }
        
        return $q;
    }

    private function count_filtered($model="", $condition="", $row="", $row_search="", $join="", $order="", $groupby="", $limit="", $offset="", $distinct="", $globals=true)
    {
        $q = $this->_get_datatables_query($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct, $globals);
        $query = $q->count();
        return $query;
    }
 
    private function count_all($model)
    {
        $q = $model::count();
        return $q;
    }
 
    private function get_datatables($model="", $condition="", $row="", $row_search="", $join="", $order="", $groupby="", $limit="", $offset="", $distinct="", $globals=true)
    {
        $q = $this->_get_datatables_query($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct, $globals);
        if ($this->input->post('length') != -1) $q->offset($this->input->post('start'))->limit($this->input->post('length'));
        $query = $q->get();
        return $query;
    }
 
    public function loadTableServerSide($model="", $condition="", $row="", $row_search="", $join="", $order="", $groupby="", $limit="", $offset="", $distinct="", $globals=true)
    {
        $list = $this->get_datatables($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct, $globals);
        $data = array();
        $no   = $this->input->post('start');//$_POST['start'];
        foreach ($list as $column) {
            $no++;
            $columns = array();
            $columns[] = $no;
            for ($i=0; $i < count($row); $i++) {
                $field = explode('.', $row[$i]);
                if (strpos(end($field), 'id_') !== false) {
                    $columns[] = encryptID($column[end($field)]);
                }
                else if (strpos(end($field), '_id') !== false) {
                    $columns[] = encryptID($column[end($field)]);
                } else {
                    $columns[] = $column[end($field)];
                }
            }
            $data[] = $columns;
        }
 
        $output = array(
                        "draw"            => $this->input->post('draw'),//$_POST['draw'],
                        "recordsTotal"    => $this->count_all($model),
                        "recordsFiltered" => $this->count_filtered($model, $condition, $row, $row_search, $join, $order, $groupby, $limit, $offset, $distinct, $globals),
                        "data"            => $data,
                );
        echo json_encode($output);
    }

}
