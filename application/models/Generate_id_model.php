<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @abstract Class Generate_id_model
**/

class generate_id_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    /**
     *  @abstract Function get max id
     *  @return String row data
     *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
    **/
    public function getMaxId($model="", $col_ID="", $condition="")
    {
        /* Select Max */
        $q = $model::selectRaw('max('.$col_ID.') as id');

        /* Set Condition */
        if(!empty($condition))
        {
            $q->whereRaw($condition);
        }

        /* Get Data */
        $query = $q->first();
        $key   = 0;

        if (count($query) > 0)
        {
            $res = $query;
            $key = $res->id + 1;
        }
        else
        {
            $key = 1;
        }
        return $key;
    }


    /**
     *  @abstract Function code prefix
     *  @return String code prefix
     *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
    **/
    function code_prefix($model, $field, $data)
    {
        if (!empty($data) && is_array($data))
        {

            $length   = $data['length'];
            $numbers  = str_repeat('0',$length);
            $prefix   = $data['prefix'];
            $position = $data['position'];

            if ($position == 'right') {
                $prefix     = $data['prefix'] . $numbers;
                $len_prefix = strlen(str_replace($numbers,'',$prefix));
                $fieldset   = 'right('.$field.', '.$length.')';
                $condition  = 'substring('.$field.' from 1 for '.$len_prefix.') = "' . str_replace($numbers,'',$prefix) . '"';
            } else {
                $prefix     = $numbers . $data['prefix'];
                $len_prefix = strlen(str_replace($numbers, '', $prefix));
                $fieldset   = 'substring('.$field.'from 1 for '.$length.')';
                $condition  = 'right('.$field.', '.$len_prefix.') = "' . str_replace($numbers,'',$prefix) . '"';
            }

            /* Get Max */
            if (!empty($data['prefix'])) {
                $max = $this->getMaxId($model,$fieldset,$condition);
            } else {
                $max = $this->getMaxId($model,$fieldset);
            }
        }
        else
        {
            /* Default Setting Prefix (001/I/2017) */
            $length     = 3;
            $numbers    = str_repeat('0', $length);
            $prefix     = $numbers . '/' . NumbertoRomawi(date('m')) . '/' . date('Y');
            $len_prefix = strlen(str_replace($numbers, '', $prefix));
            $fieldset   = 'substring('.$field.' from 1 for '.$length.')';
            $condition  = array('right('.$field.', '.$len_prefix.') =' => str_replace($numbers,'',$prefix));

            /* Get Max */
            $max = $this->getMaxId($model,$fieldset,$condition);
        }

        /* Generate New Code */
        $new_code     = substr(str_repeat('0',$length) . $max, -$length);
        $pf           = str_replace($numbers,'',$prefix);
        $generate_id  = $pf . $new_code;

        return $generate_id;
    }

}