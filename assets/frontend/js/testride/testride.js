function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function hitungMundur(seconds, timer) {
    setInterval(function() {
        seconds--;

        if(seconds >= 0)
        {   
            $('.resend').attr('disabled',true);
            document.getElementById("timer").innerHTML = seconds + " Detik";
        }

        if(seconds === 0)
        {
            $('.resend').attr('disabled',false);
            clearInterval(seconds);
        }
    }, 1000);
}

jQuery(document).ready(function() {
    /* Show Hide Notif */
    $('.success').fadeOut(5000);
    $('.error').fadeOut(5000);

    /* Get Lisst Kendaraan Untuk Test Ride */
    $('select[name="lokasi_testride"]').change(function() {
        var lokasi = $(this).val();
        var data = {
            lokasi: lokasi
        };
        $.ajax({
            type: 'GET',
            url: getAjaxTestRide,
            data: data,
            cache: false,
            success: function(result) {
                var testride = JSON.parse(result);
                $('select[name="kendaraan_testride"]').html(testride);
            }
        });
        return false;
    });
    
    /* Begin Handling OTP */
        $('input[name="nomor_hp"],input[name="nomor_verifikasi"]').on('keyup', function() {
            var no_hp = $(this).val();
            if (no_hp.length < 10) {
                $('.button-verifikasi').attr('disabled', true);
            } else {
                $('.button-verifikasi').attr('disabled', false);
            }
        });

        $('.button-verifikasi').click(function() {
            
            /* Begin Validate For Input Field Nomor HP */
            var no_hp = $('input[name="nomor_hp"]').val();
             
            $.ajax({
                type: 'POST',
                url: getVerifikasiKode,
                data: {
                    no_hp: no_hp
                },
                dataType: 'json',
                cache: false,
                success: function(result) {
                    var resP = JSON.parse(result.resp);
                    var password = result.password;
                    $.each(resP, function(i, v) {
                        var respons_sms = v;
                        $.each(respons_sms, function(j, k) {
                            var status = k.status;
                            var nomor_tujuan = k.destination;
                            if (status == '0') {
                                
                                $('.nomor-hp').prop('readonly','true');
                                $('.nomor_verifikasi').show();
                                var seconds = 60;
                                var timer;
                                if (!timer) {
                                    hitungMundur(seconds, timer);
                                }

                                $('.sent-message-success').show();
                                $('.sent-message-success').fadeOut(5000);
                                $('input[name="confirm_code"]').val(password);
                            } else {
                                $('.sent-message-fail').show();
                                $('.sent-message-fail').fadeOut(5000);
                            }
                        });
                    });
                }
            });
            /* End Validate For Input Field Nomor HP */
        });

        $('.resend').click(function() {
            var no_hp = $('input[name="nomor_hp"]').val();
            
            $.ajax({
                type: 'POST',
                url: getVerifikasiKode,
                data: {
                    no_hp: no_hp
                },
                dataType: 'json',
                cache: false,
                success: function(result) {
                    var resP = JSON.parse(result.resp);
                    var password = result.password;
                    $.each(resP, function(i, v) {
                        var respons_sms = v;
                        $.each(respons_sms, function(j, k) {
                            var status = k.status;
                            var nomor_tujuan = k.destination;
                            if (status == '0') {
                                $('.nomor_verifikasi').show();
                                var seconds = 60;
                                var timer;
                                if (!timer) {
                                    hitungMundur(seconds, timer);
                                }
                                $('.sent-message-success').show();
                                $('.sent-message-success').fadeOut(5000);
                                $('input[name="confirm_code"1]').val(password);
                            } else {
                                $('.sent-message-fail').show();
                                $('.sent-message-fail').fadeOut(5000);
                            }
                        });
                    });
                }
            });
        });

        $('input[name="nomor_verifikasi"]').on('keyup', function() {
            var nomor_verifikasi = $(this).val();
            var nomor_konfirmasi = $('input[name="confirm_code"]').val();
            if (nomor_verifikasi.length == 6) {
                if (nomor_verifikasi !== nomor_konfirmasi) {
                    $('.status-message-wrong').show();
                    $('.status-message-wrong').fadeOut(3000);
                    $('.save').addClass('disabled');
                } else {
                    $('.status-message-right').show();
                    $('.status-message-right').fadeOut(3000);
                    $('.save').removeClass('disabled');
                }
            }
        });
    /* End Handling OTP */

    /* Save */
    $('.save').click(function() {
        var form = $('#form');

        form.validate({
            errorElement: 'div',
            errorClass: "ui-state-error-text",
            ignore: [],
            rules: {
                lokasi_testride: {
                    required: true
                },
                kendaraan_testride: {
                    required: true
                },
                nama_testrider: {
                    required: true,
                    maxlength: 255
                },
                email_testrider: {
                    email: true,
                    required: true
                },
                nomor_hp: {
                    number: true,
                    required: true,
                    maxlength: 15
                },
                nomor_verifikasi: {
                    required: true,
                    minlength: 6
                },
                hiddenRecaptcha: {
                    required: function() {
                        if (grecaptcha.getResponse() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
            },
            messages: {
                hiddenRecaptcha: {
                    required: 'Captcha belum dicheck'
                }
            },
            invalidHandler: function(event, validator) { // display error alert on form submit              
            },
            errorPlacement: function(error, element) { // render error placement for each input type
                var cont = $(element);
                var div = $(element).closest('div');
                if (cont) {
                    if (div[0].className == 'form-group' || div[0].className == 'g-recaptcha' || div[0].className == 'input-group') {
                        div = div;
                    } else {
                        div = cont;
                    }
                    div.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
});