function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
} 

function hitungMundur(seconds, timer) {
    setInterval(function() {
        seconds--;

        if(seconds >= 0)
        {   
            $('.resend').attr('disabled',true);
            document.getElementById("timer").innerHTML = seconds + " Detik";
        }

        if(seconds === 0)
        {
            $('.resend').attr('disabled',false);
            clearInterval(seconds);
        }
    }, 1000);
}

jQuery(document).ready(function() {
    $('.success').fadeOut(8000);
    $('.error').fadeOut(8000);

    $('#aLayananVip').click(function(){
        event.preventDefault();
        $('#layananVIP').show();
        $('#fasilitasKelebihan').hide();
    });
    $('#aFasilitasKelebihan').click(function(){
        event.preventDefault();
        $('#fasilitasKelebihan').show();
        $('#layananVIP').hide();
    });

    // $('#form').validate({
    //         errorElement : 'div',
    //         errorClass: "ui-state-error-text",
    //         ignore: [],
    //         rules: {
    //             nama_lengkap: {
    //                 required: true,
    //                 maxlength : 100,
    //             },
    //             email: {
    //                 required: true,
    //                 email: true
    //             },
    //             perusahaan: {
    //                 required: true,
    //                 maxlength: 100
    //             },
    //             nomor_hp: {
    //                 required: true
    //             },
    //             lokasi: {
    //                 required: true,
    //                 maxlength: 100
    //             },
    //             subyek_pesan: {
    //                 required: true
    //             },
    //             'check[]': {
    //                 required: true
    //             },
    //             isi_pesan: {
    //                 required: true
    //             },
    //             nomor_verifikasi: {
    //                 required: true,
    //                 minglength: 6
    //             },
    //             hiddenRecaptcha: {
    //                 required: function() {
    //                     if(grecaptcha.getResponse() == '') {
    //                          return true;
    //                     } else {
    //                         return false;
    //                     }
    //                 }
    //             }
    //         },

    //         invalidHandler: function (event, validator) { // display error alert on form submit              
    //         },

    //         errorPlacement: function (error, element) { // render error placement for each input type
    //            var cont = $(element);
    //            var div  = $(element).closest('div');
               
    //             if (cont) {
    //                 if (div[0].className == 'form-group' || div[0].className == 'custom-control-label' || div[0].className == 'input-group') {
    //                     div = div;
    //                 } else {
    //                     div = cont;
    //                 }
    //                 div.after(error);
    //             } else {
    //                 element.after(error);
    //             }
    //         },

    //         highlight: function (element) { // hightlight error inputs

    //             $(element)
    //                 .closest('.form-group').addClass('has-error'); // set error class to the control group
    //         },

    //         unhighlight: function (element) { // revert the change done by hightlight
    //             $(element)
    //                 .closest('.form-group').removeClass('has-error'); // set error class to the control group
    //         },

    //         success: function (label) {
    //             label
    //                 .closest('.form-group').removeClass('has-error'); // set success class to the control group
    //         },
    //         submitHandler: function (form) {
    //             form.submit();
    //         }
    //     });
    
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            errorElement : 'div',
            errorClass: "ui-state-error-text",
            ignore: [],
            rules: {
                nama_lengkap: {
                    required: true,
                    maxlength : 100,
                },
                email: {
                    required: true,
                    email: true
                },
                perusahaan: {
                    required: true,
                    maxlength: 100
                },
                nomor_hp: {
                    required: true
                },
                lokasi: {
                    required: true,
                    maxlength: 100
                },
                subyek_pesan: {
                    required: true
                },
                'check[]': {
                    required: true
                },
                isi_pesan: {
                    required: true
                },
                hiddenRecaptcha: {
                    required: function() {
                        if(grecaptcha.getResponse() == '') {
                             return true;
                        } else {
                            return false;
                        }
                    }
                }
            },

            invalidHandler: function (event, validator) { // display error alert on form submit              
            },

            errorPlacement: function (error, element) { // render error placement for each input type
               var cont = $(element);
               var div  = $(element).closest('div');
               
                if (cont) {
                    if (div[0].className == 'form-group' || div[0].className == 'custom-control-label' || div[0].className == 'input-group') {
                        div = div;
                    } else {
                        div = cont;
                    }
                    div.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        if (form.valid()) {

            mApp.block('#frmregister', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                message: 'Processing...'
            });

            $('.save').addClass('m-loader m-loader--light m-loader--right disabled');
        }
    });

    /* Remove Attr Disabled Button Verifikasi */
    $('input[name="nomor_hp"],input[name="nomor_verifikasi"]').on('keyup', function() {
        var no_hp = $(this).val();
        if (no_hp.length < 12) {
            $('.button-verifikasi').attr('disabled', true);
        } else {
            $('.button-verifikasi').attr('disabled', false);
        }
    });

    $('.button-verifikasi').click(function() {

        // $('#form').valid();

        var no_hp = $('input[name="nomor_hp"]').val();
        

        $.ajax({
            type: 'POST',
            url: getVerifikasiKode,
            data: {no_hp:no_hp,_token: $('input[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success: function(result) {
                var resP     = JSON.parse(result.resp);
                var password = result.password;

                $.each(resP, function(i,v) {
                
                    var respons_sms = v;
                    
                    $.each(respons_sms, function(j,k) {
                      var status       = k.status;
                      var nomor_tujuan = k.destination;  
                      
                      if(status == '0')
                      {
                       
                        $('.nomor_verifikasi').show();

                        var seconds=60;
                        var timer;
                      
                        if(!timer)
                        {
                            hitungMundur(seconds,timer);
                        } 

                        $('.sent-message-success').show();
                        $('.sent-message-success').fadeOut(5000);

                        $('input[name="confirm_code"]').val(password);
                      }
                      else
                      {
                        $('.sent-message-fail').show();
                        $('.sent-message-fail').fadeOut(5000);
                      }
                    });
                });
            }
        });
    });

    /* Resend Code Button */
    $('.resend').click(function() {

        var no_hp = $('input[name="nomor_hp"]').val();
        
        $.ajax({
          type: 'POST',
          url: getVerifikasiKode,
          data: {no_hp:no_hp},
          dataType: 'json',
          cache: false,
          success: function(result) {
            
            var resP     = JSON.parse(result.resp);
            var password = result.password;

            $.each(resP, function(i,v) {
              
              var respons_sms = v;
              
              $.each(respons_sms, function(j,k) {
                var status       = k.status;
                var nomor_tujuan = k.destination;  
                
                if(status == '0')
                {
                  
                    $('.nomor_verifikasi').show();
                    var seconds = 60;
                    var timer;
                  
                    if(!timer)
                    {     
                        hitungMundur(seconds,timer);
                    }
                    
                    $('.sent-message-success').show();
                    $('.sent-message-success').fadeOut(5000);

                    $('input[name="confirm_code"]').val(password);
                }
                else
                {
                  $('.sent-message-fail').show();
                  $('.sent-message-fail').fadeOut(5000);
                }
              });
            });

          }
        });
    });
    /* Resend Code Button */
});