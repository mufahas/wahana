jQuery(document).ready(function() {

    $('.success').fadeOut(5000);
    $('.error').fadeOut(5000);
    $('.exist').fadeOut(5000);


    /* Validasi Untuk Pemesanan Motor */
    $('.save').click(function() {
        var form = $(this).closest('form');
        form.validate({
            errorElement : 'div',
            errorClass: "ui-state-error-text",
            ignore: [],
            rules: {
                kuis_yang_dimenangkan: {
                    required: true,
                    maxlength: 255
                },
                nama_lengkap: {
                    required: true
                }, 
                nomor_hp: {
                    required: true,
                    number: true,
                    maxlength: 14
                },
                alamat: {
                    required: true
                },
                kelurahan: {
                    required: true,
                    maxlength: 255
                },
                kecamatan: {
                    required: true,
                    maxlength: 255
                },
                kota: {
                    required: true,
                    maxlength: 255
                },
                kode_pos: {
                    required: true,
                    maxlength: 10,
                    number: true
                },

                hiddenRecaptcha: {
                    required: function() {
                        if(grecaptcha.getResponse() == '') {
                             
                             return true;
                        } else {
                            return false;
                        }
                    }
                }
            },

            invalidHandler: function (event, validator) { // display error alert on form submit              
            },

            errorPlacement: function (error, element) { // render error placement for each input type
             var cont = $(element);
             var div  = $(element).closest('div');
             
              if (cont) {
                  if (div[0].className == 'form-group' || div[0].className == 'g-recaptcha') {
                      div = div;
                  } else {
                      div = cont;
                  }
                  div.after(error);
              } else {
                  element.after(error);
              }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            
            submitHandler: function (form) {
                form.submit();
            }
        });

        if(form.valid())
        {
          $('.save').removeClass('disabled');
        }
    });

    listNewsAjax.init();
            
            /* Search For Dekstop */ 
            $(document).on('click', '#button_search', function() {
                var searchData = $('input[name="input_search"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Dekstop */ 

            /* Search For Mobile */ 
            $(document).on('click', '#button_search_nav', function() {
                var searchData = $('input[name="search_nav"]').val();
                $('#paginationAll').twbsPagination('destroy');

                $('#for_container').hide();
                $('#contentSearch').show();

                $.ajax({
                    type: 'POST',
                    url: count_news_page,
                    data: {search: searchData},
                    dataType: 'json',
                    success: function(result) {
                        var totalPagesAll = (result.total_page_all != '0') ? result.total_page_all : '1';
                        
                        listNewsAjax.init(searchData, totalPagesAll);
                    }
                });
            });
            /* End Search For Mobile */
});