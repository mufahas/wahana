function initMap() {
    var map;
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 17,
      center: new google.maps.LatLng(-6.1551102,106.8365053),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    
    var infowindow = new google.maps.InfoWindow();
    var marker;

    
    marker = new google.maps.Marker({
        position : new google.maps.LatLng(-6.1551102,106.8365053),
        map      : map,
        icon     : xx,
    });

    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
          infowindow.setContent('<h4>Wahana Makmur Sejati, PT</h4> <br> Gedung Wahanaarta Sahari No.32<br>Jakarta 10720, indonesia<br>Phone : +6221 601 2070');
          infowindow.open(map, marker);
        }
    })(marker));
}

function hitungMundur(seconds, timer) {
    setInterval(function() {
        seconds--;

        if(seconds >= 0)
        {   
            $('.resend').attr('disabled',true);
            document.getElementById("timer").innerHTML = seconds + " Detik";
        }

        if(seconds === 0)
        {
            $('.resend').attr('disabled',false);
            clearInterval(seconds);
        }
    }, 1000);
}

jQuery(document).ready(function() {
    $('.success').fadeOut(8000);
    $('.error').fadeOut(8000);

    /* Enable Button Verifikasi No Hp */
    $('input[name="nomor_hp"],input[name="nomor_verifikasi"]').on('keyup', function() {
        var no_hp = $(this).val();
        if (no_hp.length < 10) {
            $('.button-verifikasi').attr('disabled', true);
        } else {
            $('.button-verifikasi').attr('disabled', false);
        }
    });

    /* Begin Verify Phone Number */
    $('.button-verifikasi').click(function() {
        $('.no-hp-otp').attr('readonly','true');
        /* Begin Validate For Input Field Nomor HP */
        var no_hp = $('input[name="nomor_hp"]').val();
        
        $.ajax({
            type: 'POST',
            url: getVerifikasiKode,
            data: {
                no_hp: no_hp
            },
            dataType: 'json',
            cache: false,
            success: function(result) {
                var resP = JSON.parse(result.resp);
                var password = result.password;
                $.each(resP, function(i, v) {
                    var respons_sms = v;
                    $.each(respons_sms, function(j, k) {
                        var status = k.status;
                        var nomor_tujuan = k.destination;
                        if (status == '0') {
                            
                            $('.nomor_verifikasi').show();
                            var seconds = 60;
                            var timer;
                            if (!timer) {
                                hitungMundur(seconds, timer);
                            }

                            $('.sent-message-success').show();
                            $('.sent-message-success').fadeOut(5000);
                            $('input[name="confirm_code"]').val(password);
                        } else {
                            $('.sent-message-fail').show();
                            $('.sent-message-fail').fadeOut(5000);
                        }
                    });
                });
            }
        });
        /* End Validate For Input Field Nomor HP */
    });

    $('.resend').click(function() {
        var no_hp = $('input[name="nomor_hp"]').val();
        
        $.ajax({
            type: 'POST',
            url: getVerifikasiKode,
            data: {
                no_hp: no_hp
            },
            dataType: 'json',
            cache: false,
            success: function(result) {
                var resP = JSON.parse(result.resp);
                var password = result.password;
                $.each(resP, function(i, v) {
                    var respons_sms = v;
                    $.each(respons_sms, function(j, k) {
                        var status = k.status;
                        var nomor_tujuan = k.destination;
                        if (status == '0') {
                            $('.nomor_verifikasi').show();
                            var seconds = 60;
                            var timer;
                            if (!timer) {
                                hitungMundur(seconds, timer);
                            }
                            $('.sent-message-success').show();
                            $('.sent-message-success').fadeOut(5000);
                            $('input[name="confirm_code"1]').val(password);
                        } else {
                            $('.sent-message-fail').show();
                            $('.sent-message-fail').fadeOut(5000);
                        }
                    });
                });
            }
        });
    });

    $('input[name="nomor_verifikasi"]').on('keyup', function() {
        var nomor_verifikasi = $(this).val();
        var nomor_konfirmasi = $('input[name="confirm_code"]').val();
        if (nomor_verifikasi.length == 6) {
            if (nomor_verifikasi !== nomor_konfirmasi) {
                $('.status-message-wrong').show();
                $('.status-message-wrong').fadeOut(3000);
                $('.save').addClass('disabled');
            } else {
                $('.status-message-right').show();
                $('.status-message-right').fadeOut(3000);
                $('.save').removeClass('disabled');
            }
        }
    });
    /* End Verify Phone Number */
    
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            errorElement : 'div',
            errorClass: "ui-state-error-text",
            ignore: [],
            rules: {
                nama_lengkap: {
                    required: true,
                    maxlength : 100,
                },
                email: {
                    required: true,
                    email: true
                },
                nomor_hp: {
                    required: true
                },
                subyek_pesan: {
                    required: true
                },
                isi_pesan: {
                    required: true
                },
                nomor_verifikasi: {
                    required: function() {
                        var noHp = $('input[name="nomor_hp"]').val();

                        if(noHp != '' || noHp != undefined)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }, 
                hiddenRecaptcha: {
                    required: function() {
                        if(grecaptcha.getResponse() == '') {
                             return true;
                        } else {
                            return false;
                        }
                    }
                }
            },

            invalidHandler: function (event, validator) { // display error alert on form submit              
            },

            errorPlacement: function (error, element) { // render error placement for each input type
               var cont = $(element);
               var div  = $(element).closest('div');
               
                if (cont) {
                    if (div[0].className == 'form-group' || div[0].className == 'custom-control-label' || div[0].className == 'input-group') {
                        div = div;
                    } else {
                        div = cont;
                    }
                    div.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        if (form.valid()) {

            mApp.block('#frmregister', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                message: 'Processing...'
            });

            $('.save').addClass('m-loader m-loader--light m-loader--right disabled');
        }
    });
});