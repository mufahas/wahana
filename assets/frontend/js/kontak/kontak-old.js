function initMap() {
    var map;
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 17,
      center: new google.maps.LatLng(-6.1551102,106.8365053),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    
    var infowindow = new google.maps.InfoWindow();
    var marker;

    
    marker = new google.maps.Marker({
        position : new google.maps.LatLng(-6.1551102,106.8365053),
        map      : map,
        icon     : xx,
    });

    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
          infowindow.setContent('<h4>Wahana Makmur Sejati, PT</h4> <br> Gedung Wahanaarta Sahari No.32<br>Jakarta 10720, indonesia<br>Phone : +6221 601 2070');
          infowindow.open(map, marker);
        }
    })(marker));
}

jQuery(document).ready(function() {
  $('.success').fadeOut(8000);
  $('.error').fadeOut(8000);

  $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            errorElement : 'div',
            errorClass: "ui-state-error-text",
            ignore: [],
            rules: {
                nama_lengkap: {
                    required: true,
                    maxlength : 100,
                },
                email: {
                    required: true,
                    email: true
                },
                nomor_telepon: {
                    required: true
                },
                subyek_pesan: {
                    required: true
                },
                isi_pesan: {
                    required: true
                },
                hiddenRecaptcha: {
                    required: function() {
                        if(grecaptcha.getResponse() == '') {
                             return true;
                        } else {
                            return false;
                        }
                    }
                }
            },

            invalidHandler: function (event, validator) { // display error alert on form submit              
            },

            errorPlacement: function (error, element) { // render error placement for each input type
               var cont = $(element);
               var div  = $(element).closest('div');
               
                if (cont) {
                    if (div[0].className == 'form-group' || div[0].className == 'custom-control-label') {
                        div = div;
                    } else {
                        div = cont;
                    }
                    div.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        if (form.valid()) {

            mApp.block('#frmregister', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                message: 'Processing...'
            });

            $('.save').addClass('m-loader m-loader--light m-loader--right disabled');
        }
    });
});