jQuery(document).ready(function() {
  $('.varian').owlCarousel({
      loop:true,
      margin:10,
      dots: true,
      responsive:{
          0:{
              items:2
          },
          600:{
              items:4
          },
          1000:{
              items:1
          }
      }
  })

  $('select[name="kategori_motor"]').change(function() {
    var kategori_motor = $(this).val();
    var data           = {kategori_motor: kategori_motor};

    $.ajax({
      type: 'GET',
      url: getVarianProduk,
      data: data,
      cache: false,
      success: function(result) {
        var varian_produk = JSON.parse(result);

        $('select[name="produk"]').html(varian_produk);
      }
    });
  });


  /* Get Harga OTR */
  $('select[name="produk"]').change(function() {
    var produk_otr = $(this).val();
    var data       = {kode_harga_otr: produk_otr};

    $.ajax({
      type: 'GET',
      url: getOtr,
      data: data,
      cache: false,
      success: function(result) {
        var harga_otr = JSON.parse(result);

        $('input[name="harga_otr"]').val(harga_otr);
      }
    });
  });

  /* Get Harga DP Bersdarkan Varian Produk */
  $('select[name="produk"]').change(function() {
    var kode_harga_otr = $(this).val();
    var data           = {kode_harga_otr: kode_harga_otr};

    $.ajax({
      type: 'GET',
      url: getDP,
      data: data,
      cache: false,
      success: function(result) {
        var uang_dp = JSON.parse(result);

        $('select[name="dp"]').html(uang_dp);
      }
    });
  });

  /* Get Angsuran Cicilan */
  $('select[name="dp"], select[name="tenor"]').change(function() {
    var dp    = $('select[name="dp"]').val();
    var tenor = $('select[name="tenor"]').val();
    var data  = {dp: dp, tenor: tenor};

    $.ajax({
      type: 'GET',
      url: getHargaCicilan,
      data: data,
      cache: false,
      success: function(result) {
        var angsuran = JSON.parse(result);

        $('input[name="angsuran"]').val(angsuran);
      }
    });
  });

  $('.save').click(function() {
    var form = $(this).closest('form');

    form.validate({
            rules: {
                dp: {
                    required: true
                },
                tenor: {
                    required: true
                },
                nama_lengkap: {
                    required: true
                },
                userfile: {
                    required: true
                }, 
            },

            invalidHandler: function (event, validator) { // display error alert on form submit              
            },

            errorPlacement: function (error, element) { // render error placement for each input type
             var cont = $(element);
             var div  = $(element).closest('div');
             
              if (cont) {
                  if (div[0].className == 'form-group') {
                      div = div;
                  } else {
                      div = cont;
                  }
                  div.after(error);
              } else {
                  element.after(error);
              }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            
            submitHandler: function (form) {
                form.submit();
            }
        });

      if(form.valid())
      {
        mApp.block('#frmregister', {
                  overlayColor: '#000000',
                  type: 'loader',
                  state: 'success',
                  message: 'Processing...'
              });

        $('.save').addClass('m-loader m-loader--light m-loader--right disabled');
      }

  });
});