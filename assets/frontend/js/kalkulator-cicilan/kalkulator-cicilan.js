function clear_input() {
    $('input[name="nama_lengkap"]').val('');
    $('input[name="email"]').val('');
    $('input[name="nomor_hp"]').val('');
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function hitungMundur(seconds, timer) {
    setInterval(function() {
        seconds--;

        if(seconds >= 0)
        {   
            $('.resend').attr('disabled',true);
            document.getElementById("timer").innerHTML = seconds + " Detik";
        }

        if(seconds === 0)
        {
            $('.resend').attr('disabled',false);
            clearInterval(seconds);
        }
    }, 1000);
}

jQuery(document).ready(function() {
    /* Ready Document To Validate After Verification Button Click */
    $('#form').validate({
        errorElement: 'div',
        errorClass: "ui-state-error-text",
        ignore: [],
        rules: {
            kategori_motor: {
                required: true
            },
            produk: {
                required: true
            },
            dp: {
                required: true
            },
            tenor: {
                required: true
            },
            nama_lengkap: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            nomor_hp: {
                required: true,
                number: true,
                maxlength: 14
            },
            // userfile: {
            //     required: true
            // },
            lokasi_testride: {
                required: true
            },
            kendaraan_testride: {
                required: true,
                maxlength: 30
            },
            nama_testrider: {
                required: true,
                maxlength: 255
            },
            email: {
                email: true,
                required: true
            },
            // no_ktp: {
            //     required: true,
            //     maxlength: 20
            // },
            domisili: {
                required: true
            },
            nomor_verifikasi: {
                required: true,
                minlength: 6
            },
            // kode_verifikasi: {
            //   required: function(){
            //     if($('.kode_verifikasi') == '')
            //     {
            //       return true;
            //     }
            //     else
            //     {
            //       return false;
            //     }
            //   }
            // },
            hiddenRecaptcha: {
                required: function() {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
        messages: {
            hiddenRecaptcha: {
                required: 'Captcha Belum Dicentang'
            }
        },
        invalidHandler: function(event, validator) { // display error alert on form submit              
        },
        errorPlacement: function(error, element) { // render error placement for each input type
            var cont = $(element);
            var div = $(element).closest('div');
            if (cont) {
                if (div[0].className == 'form-group' || div[0].className == 'g-recaptcha' || div[0].className == 'input-group' ) {
                    div = div;
                } else {
                    div = cont;
                }
                div.after(error);
            } else {
                element.after(error);
            }
        },
        highlight: function(element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function(element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    /* Ready Document To Validate After Verification Button Click */
    $('.success').fadeOut(5000);
    $('.error').fadeOut(5000);
    $('.varian').owlCarousel({
        loop: true,
        margin: 10,
        dots: true,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items: 1
            }
        }
    });
    /* Get Kategori Motor */
    $('select[name="kategori_motor"]').change(function() {
        var kategori_motor = $(this).val();
        var data = {
            kategori_motor: kategori_motor
        };
        $.ajax({
            type: 'GET',
            url: getVarianProduk,
            data: data,
            cache: false,
            success: function(result) {
                var varian_produk = JSON.parse(result);
                $('select[name="produk"]').html(varian_produk);
            }
        });
    });
    /* Get Harga OTR */
    $('select[name="produk"]').change(function() {
        var produk_otr = $(this).val();
        var data = {
            kode_harga_otr: produk_otr
        };
        $.ajax({
            type: 'GET',
            url: getOtr,
            data: data,
            cache: false,
            success: function(result) {
                var harga_otr = JSON.parse(result);
                $('input[name="harga_otr"]').val(harga_otr);
            }
        });
    });
    /* Get Harga DP Bersdarkan Varian Produk */
    $('select[name="produk"]').change(function() {
        var kode_harga_otr = $(this).val();
        var data = {
            kode_harga_otr: kode_harga_otr
        };
        $.ajax({
            type: 'GET',
            url: getDP,
            data: data,
            cache: false,
            success: function(result) {
                var uang_dp = JSON.parse(result);
                $('select[name="dp"]').html(uang_dp);
            }
        });
    });
    /* Get Angsuran Cicilan */
    $('select[name="dp"], select[name="tenor"]').change(function() {
        var dp = $('select[name="dp"]').val();
        var tenor = $('select[name="tenor"]').val();
        var kode_harga_otr = $('select[name="produk"]').val();
        
        var data = {
            dp: dp,
            tenor: tenor,
            kode_harga_otr: kode_harga_otr
        };
        $.ajax({
            type: 'GET',
            url: getHargaCicilan,
            data: data,
            cache: false,
            success: function(result) {
                var angsuran = JSON.parse(result);
                $('input[name="angsuran"]').val(angsuran);
            }
        });
    });
    /* Get Lisst Kendaraan Untuk Test Ride */
    $('select[name="lokasi_testride"]').change(function() {
        var lokasi = $(this).val();
        var data = {
            lokasi: lokasi
        };
        $.ajax({
            type: 'GET',
            url: getAjaxTestRide,
            data: data,
            cache: false,
            success: function(result) {
                var testride = JSON.parse(result);
                $('select[name="kendaraan_testride"]').html(testride);
            }
        });
        return false;
    });
    /* Display Form Pendaftaran Pemesanan Motor */
    $('.pesan_motor').click(function() {
        $('.identitas').show();
        // clear_input();
    });
    
    /* Begin Save Form Pemesanan */
    $('.save').click(function() {
        var form = $(this).closest('form');
        form.validate({
            errorElement: 'div',
            errorClass: "ui-state-error-text",
            ignore: [],
            rules: {
                kategori_motor: {
                    required: true
                },
                produk: {
                    required: true
                },
                dp: {
                    required: true
                },
                tenor: {
                    required: true
                },
                nama_lengkap: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                nomor_hp: {
                    required: true,
                    number: true,
                    maxlength: 14
                },
                // userfile: {
                //     required: true
                // },
                lokasi_testride: {
                    required: true
                },
                kendaraan_testride: {
                    required: true,
                    maxlength: 30
                },
                nama_testrider: {
                    required: true,
                    maxlength: 255
                },
                email: {
                    email: true,
                    required: true
                },
                // no_ktp: {
                //     required: true,
                //     maxlength: 20
                // },
                nomor_verifikasi: {
                    required: true,
                    maxlength: 6
                },
                // kode_verifikasi: {
                //   required: function(){
                //     if($('.kode_verifikasi') == '')
                //     {
                //       return true;
                //     }
                //     else
                //     {
                //       return false;
                //     }
                //   }
                // },
                hiddenRecaptcha: {
                    required: function() {
                        if (grecaptcha.getResponse() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            invalidHandler: function(event, validator) { // display error alert on form submit              
            },
            errorPlacement: function(error, element) { // render error placement for each input type
                var cont = $(element);
                var div = $(element).closest('div');
                if (cont) {
                    if (div[0].className == 'form-group' || div[0].className == 'input-group') {
                        div = div;
                    } else {
                        div = cont;
                    }
                    div.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
    /* End Save Form Pemesanan */
    /* Begin Save Test Ride */
    $('.save_modal').click(function() {
        var form = $('#form-modal');
        form.validate({
            errorElement: 'div',
            errorClass: "ui-state-error-text",
            ignore: [],
            rules: {
                lokasi_testride: {
                    required: true
                },
                kendaraan_testride: {
                    required: true
                },
                nama_testrider: {
                    required: true,
                    maxlength: 255
                },
                email_testrider: {
                    email: true,
                    required: true
                },
                no_telepon: {
                    number: true,
                    required: true,
                    maxlength: 15
                },
            },
            invalidHandler: function(event, validator) { // display error alert on form submit              
            },
            errorPlacement: function(error, element) { // render error placement for each input type
                var cont = $(element);
                var div = $(element).closest('div');
                if (cont) {
                    if (div[0].className == 'form-group') {
                        div = div;
                    } else {
                        div = cont;
                    }
                    div.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
    /* End Save Test Ride */

    /* Remove Attr Disabled Button Verifikasi */
    $('input[name="nomor_hp"],input[name="nomor_verifikasi"]').on('keyup', function() {
        var no_hp = $(this).val();
        if (no_hp.length < 10) {
            $('.button-verifikasi').attr('disabled', true);
        } else {
            $('.button-verifikasi').attr('disabled', false);
        }
    });

    /* Button Verification */
    $('.button-verifikasi').click(function() {
        /* Begin Validate For Input Field Nomor HP */
        var no_hp = $('input[name="nomor_hp"]').val();
         
        $.ajax({
            type: 'POST',
            url: getVerifikasiKode,
            data: {
                no_hp: no_hp
            },
            dataType: 'json',
            cache: false,
            success: function(result) {
                var resP = JSON.parse(result.resp);
                var password = result.password;
                $.each(resP, function(i, v) {
                    var respons_sms = v;
                    $.each(respons_sms, function(j, k) {
                        var status = k.status;
                        var nomor_tujuan = k.destination;
                        if (status == '0') {
                            
                            $('.nomor-hp').prop('readonly','true');
                            $('.nomor_verifikasi').show();
                            var seconds = 60;
                            var timer;
                            if (!timer) {
                                hitungMundur(seconds, timer);
                            }

                            $('.sent-message-success').show();
                            $('.sent-message-success').fadeOut(5000);
                            $('input[name="confirm_code"]').val(password);
                        } else {
                            $('.sent-message-fail').show();
                            $('.sent-message-fail').fadeOut(5000);
                        }
                    });
                });
            }
        });
        /* End Validate For Input Field Nomor HP */
    });
    /* Button Verification */

    /* Resend Code Button */
    $('.resend').click(function() {
        var no_hp = $('input[name="nomor_hp"]').val();
        
        $.ajax({
            type: 'POST',
            url: getVerifikasiKode,
            data: {
                no_hp: no_hp
            },
            dataType: 'json',
            cache: false,
            success: function(result) {
                var resP = JSON.parse(result.resp);
                var password = result.password;
                $.each(resP, function(i, v) {
                    var respons_sms = v;
                    $.each(respons_sms, function(j, k) {
                        var status = k.status;
                        var nomor_tujuan = k.destination;
                        if (status == '0') {
                            $('.nomor_verifikasi').show();
                            var seconds = 60;
                            var timer;
                            if (!timer) {
                                hitungMundur(seconds, timer);
                            }
                            $('.sent-message-success').show();
                            $('.sent-message-success').fadeOut(5000);
                            $('input[name="confirm_code"1]').val(password);
                        } else {
                            $('.sent-message-fail').show();
                            $('.sent-message-fail').fadeOut(5000);
                        }
                    });
                });
            }
        });
    });
    /* Resend Code Button */
    $('input[name="nomor_verifikasi"]').on('keyup', function() {
        var nomor_verifikasi = $(this).val();
        var nomor_konfirmasi = $('input[name="confirm_code"]').val();
        if (nomor_verifikasi.length == 6) {
            if (nomor_verifikasi !== nomor_konfirmasi) {
                $('.status-message-wrong').show();
                $('.status-message-wrong').fadeOut(3000);
                $('.save').addClass('disabled');
            } else {
                $('.status-message-right').show();
                $('.status-message-right').fadeOut(3000);
                $('.save').removeClass('disabled');
            }
        }
    });
});