function clear_input() {
    $('input[name="nama_lengkap"]').val('');
    $('input[name="email"]').val('');
    $('input[name="nomor_hp"]').val('');
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function hitungMundur(seconds, timer) {
    setInterval(function() {
        seconds--;

        if(seconds >= 0)
        {   
            $('.resend').attr('disabled',true);
            document.getElementById("timer").innerHTML = seconds + " Detik";
        }

        if(seconds === 0)
        {
            $('.resend').attr('disabled',false);
            clearInterval(seconds);
        }
    }, 1000);
}

jQuery(document).ready(function() {
    /* Ready Document To Validate After Verification Button Click */
    $('#form').validate({
        errorElement: 'div',
        errorClass: "ui-state-error-text",
        ignore: [],
        rules: {
            no_ktp: {
                required: true
            },
            nama_pendaftar: {
                required: true
            },
            email: {
                email: true,
                required: true
            },
            nomor_hp: {
                required: true
            },
            nomor_verifikasi: {
              required: true,
              minlength: 6
            },
            domisili: {
                required: true
            },
            hiddenRecaptcha: {
                required: function() {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
        invalidHandler: function(event, validator) { // display error alert on form submit              
        },
        errorPlacement: function(error, element) { // render error placement for each input type
            var cont = $(element);
            var div = $(element).closest('div');
            if (cont) {
                if (div[0].className == 'form-group' ||  div[0].className == 'input-group') {
                    div = div;
                } else {
                    div = cont;
                }
                div.after(error);
            } else {
                element.after(error);
            }
        },
        highlight: function(element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function(element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    /* Ready Document To Validate After Verification Button Click */
    $('.success').fadeOut(5000);
    $('.error').fadeOut(5000);
    $('.varian').owlCarousel({
        loop: true,
        margin: 10,
        dots: true,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items: 1
            }
        }
    });

    
    /* Begin Save Form Pemesanan */
    $('.save').click(function() {
        var form = $(this).closest('form');
        form.validate({
            errorElement: 'div',
            errorClass: "ui-state-error-text",
            ignore: [],
            rules: {
                hiddenRecaptcha: {
                    required: function() {
                        if (grecaptcha.getResponse() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            invalidHandler: function(event, validator) { // display error alert on form submit              
            },
            errorPlacement: function(error, element) { // render error placement for each input type
                var cont = $(element);
                var div = $(element).closest('div');
                if (cont) {
                    if (div[0].className == 'form-group' || div[0].className == 'g-recaptcha') {
                        div = div;
                    } else {
                        div = cont;
                    }
                    div.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
    /* End Save Form Pemesanan */

    /* Remove Attr Disabled Button Verifikasi */
    $('input[name="nomor_hp"],input[name="nomor_verifikasi"]').on('keyup', function() {
        var no_hp = $(this).val();
        if (no_hp.length < 10) {
            $('.button-verifikasi').attr('disabled', true);
        } else {
            $('.button-verifikasi').attr('disabled', false);
        }
    });

    /* Button Verification */
    $('.button-verifikasi').click(function() {
        /* Begin Validate For Input Field Nomor HP */
        var no_hp = $('input[name="nomor_hp"]').val();
         
        $.ajax({
            type: 'POST',
            url: getVerifikasiKode,
            data: {
                no_hp: no_hp
            },
            dataType: 'json',
            cache: false,
            success: function(result) {
                var resP = JSON.parse(result.resp);
                var password = result.password;
                $.each(resP, function(i, v) {
                    var respons_sms = v;
                    $.each(respons_sms, function(j, k) {
                        var status = k.status;
                        var nomor_tujuan = k.destination;
                        if (status == '0') {
                            
                            $('.nomor_verifikasi').show();
                            var seconds = 60;
                            var timer;
                            if (!timer) {
                                hitungMundur(seconds, timer);
                            }

                            $('.sent-message-success').show();
                            $('.sent-message-success').fadeOut(5000);
                            $('input[name="confirm_code"]').val(password);
                        } else {
                            $('.sent-message-fail').show();
                            $('.sent-message-fail').fadeOut(5000);
                        }
                    });
                });
            }
        });
        /* End Validate For Input Field Nomor HP */
    });
    /* Button Verification */

    /* Resend Code Button */
    $('.resend').click(function() {
        var no_hp = $('input[name="nomor_hp"]').val();
        
        $.ajax({
            type: 'POST',
            url: getVerifikasiKode,
            data: {
                no_hp: no_hp
            },
            dataType: 'json',
            cache: false,
            success: function(result) {
                var resP = JSON.parse(result.resp);
                var password = result.password;
                $.each(resP, function(i, v) {
                    var respons_sms = v;
                    $.each(respons_sms, function(j, k) {
                        var status = k.status;
                        var nomor_tujuan = k.destination;
                        if (status == '0') {
                            $('.nomor_verifikasi').show();
                            var seconds = 60;
                            var timer;
                            if (!timer) {
                                hitungMundur(seconds, timer);
                            }
                            $('.sent-message-success').show();
                            $('.sent-message-success').fadeOut(5000);
                            $('input[name="confirm_code"1]').val(password);
                        } else {
                            $('.sent-message-fail').show();
                            $('.sent-message-fail').fadeOut(5000);
                        }
                    });
                });
            }
        });
    });
    /* Resend Code Button */
    $('input[name="nomor_verifikasi"]').on('keyup', function() {
        var nomor_verifikasi = $(this).val();
        var nomor_konfirmasi = $('input[name="confirm_code"]').val();
        if (nomor_verifikasi.length == 6) {
            if (nomor_verifikasi !== nomor_konfirmasi) {
                $('.status-message-wrong').show();
                $('.status-message-wrong').fadeOut(3000);
                $('.save').addClass('disabled');
            } else {
                $('.status-message-right').show();
                $('.status-message-right').fadeOut(3000);
                $('.save').removeClass('disabled');
            }
        }
    });
});