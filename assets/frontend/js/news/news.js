jQuery(document).ready(function() {
    
    $('.success').fadeOut(8000);
    $('.error').fadeOut(8000);

    $("#img-thumn a").on('click', function(e) {
            e.preventDefault();

            $('#preview img').attr('src', $(this).attr('href'));
        });

    $('.aksesoris').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        stageOuterClass: 'owl-stage-outer outer-stage',
        navText: [ '<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>' ],
        navContainerClass: 'nav-container',
        navClass: [ 'prev', 'next' ],
        responsive:{
            0:{
                items:2
            },
            600:{
                items:4
            },
            1000:{
                items:6
            }
        }
    })

    $('.kirim').click(function() {
        
        $('#m_modal_1').modal('show');

        var form = $(this).closest('form');

        form.validate({
            errorElement : 'div',
            errorClass: "ui-state-error-text",
            rules: {
                komentar: {
                        required: true
                    },
                nama: {
                    required: true,
                    maxlength: 255
                },
                email: {
                    email: true,
                    required: true,
                    maxlength: 100
                }
            },
            
            invalidHandler: function (event, validator) { // display error alert on form submit              
            },    

            errorPlacement: function (error, element) { // render error placement for each input type
               var cont = $(element);
               var div  = $(element).closest('div');
               
                if (cont) {
                    if (div[0].className == 'form-group' || div[0].className == 'custom-control-label') {
                        div = div;
                    } else {
                        div = cont;
                    }
                    div.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                form.submit();
            }

        });

    });
            
});