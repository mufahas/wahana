jQuery(document).ready(function() {

    chkall();

    $('.chk_all').click(function() {
        chkall_click();
    });

    $('.chk').click(function() {
        var id = $(this).val();
        chkall();
        chk_click(id);
    });

    function chkall() {
        var all       = $('.chk').length;
        var checked   = $('.chk:checked').length;

        if (all == checked) {
            $('.chk_all').prop('checked', true);
        } else {
            $('.chk_all').prop('checked', false);
        }
    }

    function chkall_click() {
        var chkall    = $('.chk_all');
        var chk       = $('.chk');
        var chkbutton = $('.chkbutton');

        if (chkall.is(':checked')) {
            chk.prop('checked',true);
            chkbutton.prop('checked', true);

        } else {
            chk.prop('checked', false);
            chkbutton.prop('checked', false);
        }
    }

    function chk_click(id) {
        var chk            = $('.chk[value="' + id + '"');
        var chkbutton      = chk.closest('tr').find('.chkbutton').parent().parent();

        if (chk.is(':checked')) {
            chkbutton.find('label').find('input').prop('checked', true);
        } else {
            chkbutton.find('label').find('input').prop('checked', false);
        }

        chkchild_click(id);
    }

    function chkchild_click(id) {
        var chk            = $('.chk[value="' + id + '"');
        var chkchild       = $('.chkchild' + id);

        var chkchildbutton = chkchild.closest('tr').find('.chkbutton').parent().parent();

        if (chk.is(':checked')) {
            chkchild.prop('checked', true);
            chkchildbutton.find('label').find('input').prop('checked', true);
        } else {
            chkchild.prop('checked', false);
            chkchildbutton.find('label').find('input').prop('checked', false);
        }

        for (var i = 0; i < chkchild.length; i++) {
            chkchild_click(chkchild[i].value);
        }
    }

    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                role: {
                    required: true,
                },
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $.ajax({
                type    : 'POST',
                url     : form.attr('action'),
                data    : form.serialize(),
                cache   : false,
                success : function(data) {

                    if(data.status == 'success')
                    {   
                        toastr.success(data.message); 
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);   
                    }
                    else
                    {
                        toastr.error(data.message);
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);
                    }
                }
            });
        }
    });
    
});