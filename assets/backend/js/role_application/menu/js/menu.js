jQuery(document).ready(function() {

    $('[data-switch=true]').bootstrapSwitch();

    $('#m_select2_2').select2({
        placeholder: "-- Select icon --",
    });

    /* get preview icon */
    $("select[name='menu_icon']").change(function(){
        var icon_name = $('select[name="menu_icon"] option:selected').html();
        $('.icons').html('<i class="'+ icon_name +'"></i>');
    });

    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                menu_name: {
                    required: true,
                    maxlength : 49
                },
                menu_class: {
                    maxlength : 49,
                    remote: {
                        type : 'POST',
                        url  : checkClass,
                        data : {
                            id_menu: function() {
                                return $('input[name="id"]').val();
                            },
                            menu_class: function() {
                                return $('input[name="menu_class"]').val();
                            }
                        }
                    }
                },
                menu_folder: {
                    maxlength : 49
                },
                menu_parent: {
                    required: true
                },
            },
            messages: {
                menu_class: {
                                remote: jQuery.validator.format("This alias class is already taken.")
                            }
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $.ajax({
                type    : 'POST',
                url     : form.attr('action'),
                data    : form.serialize(),
                cache   : false,
                success : function(data) {

                    if(data.status == 'success')
                    {   
                        toastr.success(data.message); 
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);   
                    }
                    else
                    {
                        toastr.error(data.message);
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);
                    }
                }
            });
        }
    });
    
});