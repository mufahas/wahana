var DatatableRemoteAjax = function() {

    var demo = function() {

        var oTable = $('#table').dataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url"  : loadTable,
                "type" : "POST",
            },

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            "buttons": [],

            "responsive": true,

            "paging": true,

            "order": [
                [1, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "bProcessing": true,

            "oLanguage": {
                "sProcessing": "Loading, please wait..."
            },

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            "columns": [
                {
                    "render": function(data, type, row) {
                        return row[0];
                    },
                    "visible": true,
                    "class": 'text-center',
                    "orderable": false,
                    "width": '50px'
                },
                {
                    "render": function(data, type, row) {
                        return row[2];
                    },
                    "visible": true
                },
                {
                    "render": function(data, type, row) {
                        return row[3];
                    },
                    "visible": true
                },
                {
                    "render": function(data, type, row) {
                        return row[4];
                    },
                    "visible": true
                },
                {
                    "render": function(data, type, row) {
                        if(row[5] == '1')
                        {
                            var status = '<span class="m-badge  m-badge--info m-badge--wide">Active</span>';
                        }
                        else
                        {
                            var status = '<span class="m-badge  m-badge--danger m-badge--wide">Not Active</span>';
                        }
                        return status;
                    },
                    "visible": true
                },
                {
                    "render": function(data, type, row) {
                        var id     = '<input type="hidden" id="id" value="' + row[6] + '">';
                        var btnE   = '<a href="'+ edit_ +'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill edit" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Edit" data-original-title="Edit"><i class="la la-edit"></i></a>';
                        var btnD   = '<a href="'+ delete_ +'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Delete" data-original-title="Delete"><i class="la la-trash"></i></a>';
                        var button = id + '' + btnE + '' + btnD;
                        return button;
                    },
                    "visible": true,
                    "class": 'text-center',
                    "orderable": false,
                    "width": '100px'
                },
            ]

        });

    };

    return {
        // public functions
        init: function() {
            demo();
        },
    };
}();

jQuery(document).ready(function() {
    DatatableRemoteAjax.init();
});