jQuery(document).ready(function() {

    $('[data-switch=true]').bootstrapSwitch();

    $('#m_select2_2').select2({
        placeholder: "-- Select Role --",
    });

    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                username: {
                    required: true,
                    maxlength : 100
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                role_user: {
                    required: true
                },
                phone_number: {
                    required: true
                }
            },
            messages: {
                email:  {
                                remote: jQuery.validator.format("This Email is Already Exist")
                            }
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $.ajax({
                type    : 'POST',
                url     : form.attr('action'),
                data    : form.serialize(),
                cache   : false,
                success : function(data) {

                    if(data.status == 'success')
                    {   
                        toastr.success(data.message); 
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);   
                    }
                    else
                    {
                        toastr.error(data.message);
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);
                    }
                }
            });
        }
    });
    
});