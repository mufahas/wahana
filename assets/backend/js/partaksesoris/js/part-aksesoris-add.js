/* Plugin dropzone upload file */
var myDropzone = Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element
    url              : url,
    uploadMultiple   : uploadMultiple, 
    autoProcessQueue : autoProcessQueue,
    maxFilesize      : maxFilesize,
    paramName        : paramName,
    addRemoveLinks   : addRemoveLinks,
    maxFiles         : maxFiles,
    parallelUploads  : parallelUploads,
    acceptedFiles    : acceptedFiles,
    init: function() {
        myDropzone = this;
            
            this.on("processing", function(file) {
            this.options.url = url;
        });
        this.on("addedfile", function(file) { 
            $('.save').attr("disabled",false); 
        });
        this.on("sending", function(file, xhr, data) { 

            /* additional data */     
            data.append("kode_part_aksesoris", $('input[name="kode_part_aksesoris"]').val());
            data.append("kode_kategori_part_aksesoris", $('select[name="kode_kategori_part_aksesoris"]').val());
            data.append("nama_part_aksesoris", $('input[name="nama_part_aksesoris"]').val());
            data.append("deskripsi_part_aksesoris", $('textarea[name="deskripsi_part_aksesoris"]').val());
            data.append("harga_part_aksesoris", $('input[name="harga_part_aksesoris"]').val());
            data.append("kategori_part_aksesoris", $('select[name="kategori_part_aksesoris"]').val());
            data.append("kode_produk", $('select[name="kode_produk"]').val());
            data.append("status_promo", $('select[name="status_promo"]').val());
            data.append("status_ketersediaan_stok", $('select[name="status_ketersediaan_stok"]').val());

            data.append("id", $('input[name="id"]').val());
        });
        this.on("removedfile", function(file) {
            var count_file = this.files.length;
            if(count_file == 0)
            {
                $('.save').attr("disabled",true);
            }
        });
        this.on("success", function(file, response) {

            //myDropzone.options.autoProcessQueue = false; 

            if(response.status == 'success')
            {   
                toastr.success(response.message);  
            }
            else
            {
                toastr.error(response.message);
            }

        });
        this.on("queuecomplete", function(file, response) {
            setTimeout(function() {
                window.location.href = url_succees;
            }, 4000);   
        });
    }
} 

function return_post_dropzone(){
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
}

jQuery(document).ready(function() {

    $('[data-switch=true]').bootstrapSwitch();

    /* Start Set Select Dropdown */
    $('#m_select2_2').select2({
        placeholder: "-- Pilih Kategori Aksesoris --",
    });

    $('#m_select2_3').select2({
        placeholder: "-- Pilih Produk --",
    });

    $('#m_select2_4').select2({
        placeholder: "-- Pilih Jenis Aksesoris --",
    });

    $('#m_select2_5').select2({
        placeholder: "-- Pilih Status Promo --",
    });

    $('#m_select2_6').select2({
        placeholder: "-- Pilih Status Ketersediaan Stok --",
    });


    $('.summernote').summernote({
        height: 200, 
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
          ]
    });

    $('.remove').click(function(){
        $('.save').attr("disabled",true);
        $('.photo-edit').hide();
        $('.dropzone-edit').show();
    });

    /* Save Data*/
    $('.save').click(function() {
    
        var form = $(this).closest('form');

        form.validate({
            rules: {
                kode_kategori_part_aksesoris: {
                    required: true
                },
                kode_produk: {
                    required: true
                },
                jenis_part_aksesoris: {
                    required: true
                },
                status_promo: {
                    required: true
                },
                nama_part_aksesoris: {
                    required: true,
                    maxlength : 100,
                    remote: {
                        type : 'POST',
                        url  : checkNamaPartAksesoris,
                        data : {
                            id: function() {
                                return $('input[name="id"]').val();
                            },
                            nama_part_aksesoris: function() {
                                return $('input[name="nama_part_aksesoris"]').val();
                            }
                        }
                    }
                },
                harga_part_aksesoris: {
                    required: true
                },
                deskripsi_part_aksesoris: {
                    required: true
                },
                status_ketersediaan_stok: {
                    required: true
                },
            },
            messages: {
                nama_part_aksesoris: {
                                remote: jQuery.validator.format("Nama Part Aksesoris sudah digunakan.")
                            }
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            if(myDropzone.files.length == 0){
               $.ajax({
                    type    : 'POST',
                    url     : form.attr('action'),
                    data    : form.serialize(),
                    cache   : false,
                    success : function(data) {
                        
                        if(data.status == 'success')
                        {   
                            toastr.success(data.message); 
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);   
                        }
                        else
                        {
                            toastr.error(data.message);
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);
                        }
                    }
                });
            }else{
                return_post_dropzone();
            }
        }
    });
    
});