/* Plugin dropzone upload file */
jQuery(document).ready(function() {

    /* Save Data*/
    $('.save').click(function() {
    
        var form = $(this).closest('form');

        form.validate({
            rules: {
                userfile: {
                    required: true
                }
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $('.save').addClass('m-loader m-loader--light m-loader--right disabled');
        }
    });
    
});