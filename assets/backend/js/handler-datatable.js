jQuery(document).ready(function() {

    $('#table').on('click', '.edit', function() {
        var x    = $(this).closest('td').find('#id').val();
        var href = $(this).attr('href');
        $(this).attr('href', href + '/' + x);
    });

    $('#table').on('click', '.view', function() {
        var x    = $(this).closest('td').find("#id").val();
        var href = $(this).attr("href");
        $(this).attr("href", href + '/' + x);
    });

    $('#table_kontak').on('click', '.view', function() {
        var x    = $(this).closest('td').find("#id").val();
        var href = $(this).attr("href");
        $(this).attr("href", href + '/' + x);
    }); 

    $('#table_gso').on('click', '.view', function() {
        var x    = $(this).closest('td').find("#id").val();
        var href = $(this).attr("href");
        $(this).attr("href", href + '/' + x);
    });  

    $('#table').on('click', '.delete', function(e) {
        var x    = $(this).closest('td').find('#id').val();
        var href = $(this).attr('href');

        e.preventDefault();  

        swal({
          title      : "Yakin hapus data ?",
          text       : "Setelah dihapus, Anda tidak akan dapat memulihkan data ini!",
          icon       : "warning",
          buttons    : true,
          dangerMode : true,
          allowOutsideClick: false
        })

        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    type    : 'GET',
                    url     : href,
                    data    : {id : x},
                    cache   : false,
                    success : function(result) {
                
                        if(result.status == 'success')
                        {
                            swal(result.message, {
                                icon    : "success",
                                buttons : false,
                            });

                            setTimeout(function() {
                                window.location.href = result.url;
                            }, 2000);
                        }
                        else
                        {
                            swal(result.message, {
                                icon     : "warning",
                                 buttons : false,
                            });

                            setTimeout(function() {
                                window.location.href = result.url;
                            }, 2000);
                        }
                    },
                });

            } else {
                swal("Batal hapus!");
            }
        });
    });
});