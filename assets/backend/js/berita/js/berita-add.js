var myDropzone = Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element
    url              : url,
    uploadMultiple   : uploadMultiple, 
    autoProcessQueue : autoProcessQueue,
    maxFilesize      : maxFilesize,
    paramName        : paramName,
    addRemoveLinks   : addRemoveLinks,
    maxFiles         : maxFiles,
    parallelUploads  : parallelUploads,
    acceptedFiles    : acceptedFiles,
    init: function() {
        myDropzone = this;
            this.on("processing", function(file) {
            this.options.url = url;
        });
        this.on("addedfile", function(file) { 
            $('.save').attr("disabled",false); 
        });
        this.on("sending", function(file, xhr, data) { 

            /* additional data */     
            data.append("id", $('input[name="id"]').val());
            data.append("kategori_berita"/*name*/, $('select[name="kategori_berita"]').val()/*value*/);
            data.append("title"/*name*/, $('input[name="title"]').val()/*value*/);
            data.append("content"/*name*/, $('textarea[name="content"]').val()/*value*/);
            data.append("deskripsi"/*name*/, $('textarea[name="deskripsi"]').val()/*value*/);
            data.append("berita_utama"/*name*/, $('select[name="berita_utama"]').val()/*value*/);
            data.append("label_berita"/*name*/, $('select[name="label_berita"]').val()/*value*/);
            data.append("urutan_berita"/*name*/, $('input[name="urutan_berita"]').val()/*value*/);
            data.append("status_publikasi"/*name*/, $('select[name="status_publikasi"]').val()/*value*/);
            data.append("datePub"/*name*/, $('input[name="datePub"]').val()/*value*/);
        });
        this.on("removedfile", function(file) {
            var count_file = this.files.length;
            if(count_file == 0)
            {
                $('.save').attr("disabled",true);
            }
        });
        this.on("success", function(file, response) {

            myDropzone.options.autoProcessQueue = false; 

            if(response.status == 'success')
            {   
                toastr.success(response.message);  
            }
            else
            {
                toastr.error(response.message);
            }

        });
        this.on("queuecomplete", function(file, response) {
            setTimeout(function() {
                window.location.href = url_succees;
            }, 4000);   
        });
    }
} 

function return_post_dropzone(){
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
}

jQuery(document).ready(function() {
    $('[data-switch=true]').bootstrapSwitch();
    
    $('#m_select2_2').select2({
        placeholder: "-- Pilih Kategori Berita --",
    });

    $('#m_select2_3').select2({
        placeholder: "-- Pilih Berita Utama --",
    }); 

    $('#m_select2_4').select2({
        placeholder: "-- Pilih Label Berita --",
    }); 

    $('#m_select2_5').select2({
        placeholder: "-- Pilih Status Publikasi --",
    }); 

    $('#m_datetimepicker_1').datetimepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd MM yyyy hh:ii'
    });


    // $('.summernote').summernote({
    //     height: 150,
    //     toolbar: [
    //     // [groupName, [list of button]]
    //     ['style', ['bold', 'italic', 'underline', 'clear']],
    //     ['font', ['strikethrough', 'superscript', 'subscript']],
    //     ['fontsize', ['fontsize']],
    //     ['color', ['color']],
    //     ['para', ['ul', 'ol', 'paragraph']],
    //     ['height', ['height']]
    //   ]
    // });
    
    $('.summernote').summernote({
      height: 150,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['view', ['fullscreen', 'codeview']]
      ]
    });
        
    // enable clear button 
    $('#start_date, #end_date').datepicker({
        todayBtn: "linked",
        format :'dd MM yyyy',
        clearBtn: true,
        todayHighlight: true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });

    $('select[name="berita_utama"]').change(function() {
        var berita_utama = $(this).val();

        if(berita_utama == 'T') {
            $('#urutan_berita').show();
        } else {
            $('#urutan_berita').hide();
            $('.save').attr("disabled",false);
        }

    });

    $('input[name="urutan_berita"]').keyup(function() {
        var urutan = $(this).val();
        var foto   = $('.dropzone').html();

        $.ajax({
            type    : 'POST',
            url     : checkUrutan,
            data    : {urutan_berita : urutan},
            cache   : false,
            success : function(data) {
                var oObj = JSON.parse(data);

                if(oObj == true) {
                   $('.save').attr("disabled",false);
                } else {
                    $('.save').attr("disabled",true);
                }
            }
        }); 

    });

    $("select[name='status_publikasi']").change(function(){
        var status_publikasi = $(this).val();
       $('.save').attr("disabled",false);
        
        if(status_publikasi == 'F')
        {
            $('.date-public').hide();
            $('input[name="date"]').val('');
            $('.save').attr("disabled",false);
        }
        else
        {
            $('.date-public').show();
            $('.save').attr("disabled",false);
        }
    });

    /* get preview icon */
    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                title: {
                    required: true,
                    maxlength: 100,
                    remote: {
                        type : 'POST',
                        url  : checkTitle,
                        data : {
                            id: function() {
                                return $('input[name="id"]').val();
                            },
                            title: function() {
                                return $('input[name="title"]').val();
                            }
                        }
                    }
                },
                kategori_berita: {
                    required: true
                },
                berita_utama: {
                    required: true
                },
                content : {
                    required : true
                },
                status_publikasi: {
                    required: true
                }
            },
            messages: {
                title: {
                            remote: jQuery.validator.format("Judul ini telah digunakan.")
                        },
                urutan_berita: {
                            remote: jQuery.validator.format("Urutan berita ini sudah digunakan.")
                        },
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            if(myDropzone.files.length == 0) {
                $.ajax({
                    type    : 'POST',
                    url     : form.attr('action'),
                    data    : form.serialize(),
                    cache   : false,
                    success : function(data) {

                        if(data.status == 'success')
                        {   
                            toastr.success(data.message); 
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);   
                        }
                        else
                        {
                            toastr.error(data.message);
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);
                        }
                    }
                }); 
            } else {
                return_post_dropzone();
            }
        }
    });
    
});