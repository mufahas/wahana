jQuery(document).ready(function() {
    $('[data-switch=true]').bootstrapSwitch();

    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                status_komentar: {
                    required: false
                }
            }
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $.ajax({
                type    : 'POST',
                url     : form.attr('action'),
                data    : form.serialize(),
                cache   : false,
                success : function(data) {
                    
                    if(data.status == 'success')
                    {   
                        toastr.success(data.message); 
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);   
                    }
                    else
                    {
                        toastr.error(data.message);
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);
                    }
                }
            });
        }
    });
});