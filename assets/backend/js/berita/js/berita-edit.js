var myDropzone = Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element
    url              : url,
    uploadMultiple   : uploadMultiple, 
    autoProcessQueue : autoProcessQueue,
    maxFilesize      : maxFilesize,
    paramName        : paramName,
    addRemoveLinks   : addRemoveLinks,
    maxFiles         : maxFiles,
    parallelUploads  : parallelUploads,
    acceptedFiles    : acceptedFiles,
    init: function() {
        myDropzone = this;

        $.get(getImage, function(data) {
            
            var oObj = JSON.parse(data);

            $.each(oObj, function(key,value){

                // var mockFile = { name: value.gambar_part_aksesoris, size: 0 };
                
                var mockFile = { 
                    name: value.gambar_berita, 
                    url: path_image + value.gambar_berita
                };
                 
                myDropzone.options.addedfile.call(myDropzone, mockFile);
 
                myDropzone.options.thumbnail.call(myDropzone, mockFile, path_image + value.gambar_berita);

                myDropzone.files.push(mockFile);
                
            });
        });


        this.on("processing", function(file) {
            this.options.url = url;
        });
        this.on("addedfile", function(file) { 
            //$('.save').attr("disabled",false); 
        });
        this.on("sending", function(file, xhr, data) { 

            /* additional data */     
            data.append("id", $('input[name="id"]').val());
            data.append("kategori_berita"/*name*/, $('select[name="kategori_berita"]').val()/*value*/);
            data.append("title"/*name*/, $('input[name="title"]').val()/*value*/);
            data.append("title_url"/*name*/, $('input[name="title_url"]').val()/*value*/);
            data.append("content"/*name*/, $('textarea[name="content"]').val()/*value*/);
            data.append("deskripsi"/*name*/, $('textarea[name="deskripsi"]').val()/*value*/);
            data.append("berita_utama"/*name*/, $('select[name="berita_utama"]').val()/*value*/);
            data.append("label[]"/*name*/, $('select[name="label[]"]').val()/*value*/);
            data.append("date"/*name*/, $('input[name="date"]').val()/*value*/);
            data.append("urutan_berita"/*name*/, $('input[name="urutan_berita"]').val()/*value*/);
        });
        this.on("removedfile", function(file) {
            if (file.url && file.url.trim().length > 0) {
                var kode_berita = $('input[name="id"]').val();
                $.get(remove_image + kode_berita + '/' + file.name , function(data) {

                    if(data.status == 'success')
                    {   
                        toastr.success(data.message);  
                    }
                    else
                    {
                        toastr.error(data.message);
                    }
                });
            }
        });
        this.on("success", function(file, response) {

            // myDropzone.options.autoProcessQueue = false; 

            if(response.status == 'success')
            {   
                toastr.success(response.message);  
            }
            else
            {
                toastr.error(response.message);
            }

        });
        this.on("queuecomplete", function(file, response) {
            setTimeout(function() {
                window.location.href = url_succees;
            }, 4000);   
        });
    }
} 

function return_post_dropzone(){
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
}

jQuery(document).ready(function() {

    var status_publikasi = $("select[name='status_publikasi']").val();
    console.log(status_publikasi);

    if(status_publikasi == 'F')
    {
        $('.date-public').hide();
        $('input[name="date"]').val('');
    }
    else
    {
        $('.date-public').show();
    }

    var bu = $('select[name="berita_utama"]').val();

    if(bu == 'T') {
        $('#urutan_berita').show();
    } else {
        $('#urutan_berita').hide();
    }
    
    $('[data-switch=true]').bootstrapSwitch();

    $('#m_select2_2').select2({
        placeholder: "-- Pilih Kategori Berita --",
    });

    $('#m_select2_3').select2({
        placeholder: "-- Pilih Berita Utama --",
    }); 

    $('#m_select2_4').select2({
        placeholder: "-- Pilih Label Berita --",
    });  
    
    $('#m_select2_5').select2({
        placeholder: "-- Pilih Status Publikasi --",
    });  

    $('select[name="berita_utama"]').change(function() {
        var berita_utama = $(this).val();

        if(berita_utama == 'T') {
            $('#urutan_berita').show();
        } else {
            $('#urutan_berita').hide();
        }

    });

    $('#m_datetimepicker_1').datetimepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd MM yyyy hh:ii'
    });

    // $('.summernote').summernote({
    //     height: 150,
    //     toolbar: [
    //     // [groupName, [list of button]]
    //     ['style', ['bold', 'italic', 'underline', 'clear']],
    //     ['font', ['strikethrough', 'superscript', 'subscript']],
    //     ['fontsize', ['fontsize']],
    //     ['color', ['color']],
    //     ['para', ['ul', 'ol', 'paragraph']],
    //     ['height', ['height']]
    //   ]
    // });
    
    $('.summernote').summernote({
      height: 150,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['view', ['fullscreen', 'codeview']]
      ]
    });
        
    // enable clear button 
    $('#start_date, #end_date').datepicker({
        todayBtn: "linked",
        format :'dd MM yyyy',
        clearBtn: true,
        todayHighlight: true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });

    $("select[name='status_publikasi']").change(function(){
        var status_publikasi = $(this).val();

        if(status_publikasi == 'F')
        {
            $('.date-public').hide();
            $('input[name="date"]').val('');
        }
        else
        {
            $('.date-public').show();
        }
    });

    /* get preview icon */
    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                title: {
                    required: true,
                    maxlength: 100,
                    remote: {
                        type : 'POST',
                        url  : checkTitle,
                        data : {
                            id: function() {
                                return $('input[name="id"]').val();
                            },
                            title: function() {
                                return $('input[name="title"]').val();
                            }
                        }
                    }
                },
                title_url: {
                    required: true,
                    maxlength: 100,
                    remote: {
                        type : 'POST',
                        url  : checkTitleUrl,
                        data : {
                            id: function() {
                                return $('input[name="id"]').val();
                            },
                            title_url: function() {
                                return $('input[name="title_url"]').val();
                            }
                        }
                    }
                },
                urutan_berita: {
                    remote: {
                        type : 'POST',
                        url  : checkUrutan,
                        data : {
                            id: function() {
                                return $('input[name="id"]').val();
                            },
                            urutan_berita: function() {
                                return $('input[name="urutan_berita"]').val();
                            }
                        }
                    }
                },
                kategori_berita: {
                    required: true
                },
                berita_utama: {
                    required: true
                },
                content : {
                    required : true
                }
            },
            messages: {
                title: {
                            remote: jQuery.validator.format("Judul ini telah digunakan.")
                        },
                title_url: {
                            remote: jQuery.validator.format("URL Judul ini telah digunakan.")
                        },
                urutan_berita: {
                            remote: jQuery.validator.format("Urutan berita ini sudah digunakan.")
                        },
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            if(myDropzone.getQueuedFiles().length == 0) {
                $.ajax({
                    type    : 'POST',
                    url     : form.attr('action'),
                    data    : form.serialize(),
                    cache   : false,
                    success : function(data) {

                        if(data.status == 'success')
                        {   
                            toastr.success(data.message); 
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);   
                        }
                        else
                        {
                            toastr.error(data.message);
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);
                        }
                    }
                }); 
            } else {
                return_post_dropzone();
            }
        }
    });
    
});