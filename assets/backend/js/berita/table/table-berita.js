var DatatableRemoteAjax = function() {

    var demo = function(start_date="") {

        var oTable = $('#table').dataTable({

            "processing": true,
            "serverSide": true,
            "pagingType": "full_numbers",
            "ajax": {
                "url"  : loadTable,
                "type" : "POST",
                "data" : {start_date : start_date}
            },

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            "buttons": [],

            "responsive": true,

            "paging": true,

            "order": [
                [1, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "bProcessing": true,

            "oLanguage": {
                "sProcessing": "Loading, please wait..."
            },

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            "columns": [
                {
                    "render": function(data, type, row) {
                        return row[0];
                    },
                    "visible": true,
                    "class": 'text-center',
                    "orderable": false,
                    "width": '50px'
                },
                {
                    "render": function(data, type, row) {
                        return row[2];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        var berita_utama = row[3];

                        if(berita_utama == 'T') {
                            var bu = 'Ya';
                        } else {
                            var bu = 'Tidak';
                        }
                        return bu;
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        if(row[4] != null)
                        {
                            var dateB = new Date(row[4]);
                            var ex2   = dateB.format("dd mmmm yyyy");
                            return  ex2;
                        }
                        else
                        {
                            return '-';
                        }
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        var id     = '<input type="hidden" id="id" value="' + encryptID(row[1]) + '">';
                        var btnE   = edit_;
                        var btnV   = view_;
                        var button = id + '' + btnE + '' + btnV;
                        return button;
                    },
                    "visible": true,
                    "class": 'text-center',
                    "orderable": false,
                    "width": '100px'
                },
            ]

        });

    };

    return {
        // public functions
        init: function(start_date="") {
            demo(start_date);
        },
    };
}();

jQuery(document).ready(function() {
    
    DatatableRemoteAjax.init();

    $(document).on('change','input[name="start_date"]', function(){
        var start_date = $('input[name="start_date"]').val();

        $("#table").dataTable().fnDestroy();

        DatatableRemoteAjax.init(start_date);
    });
});