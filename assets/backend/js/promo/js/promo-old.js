/* Plugin dropzone upload file */
var myDropzone = Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element
    url              : url,
    uploadMultiple   : uploadMultiple, 
    autoProcessQueue : autoProcessQueue,
    maxFilesize      : maxFilesize,
    paramName        : paramName,
    addRemoveLinks   : addRemoveLinks,
    maxFiles         : maxFiles,
    parallelUploads  : parallelUploads,
    acceptedFiles    : acceptedFiles,
    init: function() {
        myDropzone = this;
            this.on("processing", function(file) {
            this.options.url = url;
        });
        this.on("addedfile", function(file) { 
            $('.save').attr("disabled",false); 
        });
        this.on("sending", function(file, xhr, data) { 

            /* additional data */     
            data.append("judul_promo"/*name*/, $('input[name="judul_promo"]').val()/*value*/);
            data.append("tanggal_promo"/*name*/, $('input[name="tanggal_promo"]').val()/*value*/);
            data.append("deskripsi"/*name*/, $('textarea[name="deskripsi"]').val()/*value*/);
            data.append("status_promo"/*name*/, $('input[name="status_promo"]').prop('checked')/*value*/);
            data.append("status_publikasi"/*name*/, $('select[name="status_publikasi"]').val()/*value*/);
            data.append("id", $('input[name="id"]').val());
        });
        this.on("removedfile", function(file) {
            var count_file = this.files.length;
            if(count_file == 0)
            {
                $('.save').attr("disabled",true);
            }
        });
        this.on("success", function(file, response) {

            myDropzone.options.autoProcessQueue = false; 

            if(response.status == 'success')
            {   
                toastr.success(response.message);  
            }
            else
            {
                toastr.error(response.message);
            }

        });
        this.on("queuecomplete", function(file, response) {
            setTimeout(function() {
                window.location.href = url_succees;
            }, 4000);   
        });
    }
} 

function return_post_dropzone(){
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
}


function check(){
    var judpro = $('input[name="judul_promo"]').val();
    var idpro = $('input[name="id"]').val();
    $.ajax({
       type: "POST",
       url: checkJudul,
       data: {'judul_promo' : judpro, 'id' : idpro},
       dataType: "text",
       success: function(msg){
                if (msg == 'false') {
                    $('.ValidasiJudul').show();
                    $('.save').attr("disabled",true);
                }else{
                    $('.ValidasiJudul').hide();
                    $('.save').attr("disabled",false);
                }
       }
    });
}

jQuery(document).ready(function() {

    // input group and left alignment setup
    // $('#tanggal_promo').daterangepicker({
    //     buttonClasses: 'm-btn btn',
    //     applyClass: 'btn-primary',
    //     cancelClass: 'btn-secondary'
    // }, function(start, end, label) {
    //     $('#tanggal_promo .form-control').val( start.format('DD MMMM YYYY') + ' - ' + end.format('DD MMMM YYYY'));
    // });
    
    $('#m_select2_1').select2({
        placeholder: "-- Pilih Status Publikasi --",
    });
    
    $('.summernote').summernote({
        height: 150, 
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
          ]
    });

    // enable clear button 
    $('#mulai_promo').datepicker({
        format: "dd MM yyyy",
        todayBtn: "linked",
        clearBtn: true,
        todayHighlight: true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });

    $('#akhir_promo').datepicker({
        format: "dd MM yyyy",
        todayBtn: "linked",
        clearBtn: true,
        todayHighlight: true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });

    $('[data-switch=true]').bootstrapSwitch();

    var valueBanner = $('input[name="value_banner"]').val();
    if(valueBanner == ""){
        $('.banner-edit').hide();
        $('.dropzone-edit').show();
    }else{
        $('.banner-edit').show();
        $('.dropzone-edit').hide();
    }

    $('.remove').click(function(){
        $('.save').attr("disabled",true);
        $('.banner-edit').hide();
        $('.dropzone-edit').show();
    });

    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                judul_promo: {
                    required: true,
                    maxlength : 99,
                    remote: {
                        type : 'POST',
                        url  : checkJudul,
                        data : {
                            id: function() {
                                return $('input[name="id"]').val();
                            },
                            judul_promo: function() {
                                return $('input[name="judul_promo"]').val();
                            }
                        }
                    }
                },
                tanggal_promo: {
                    required: true,
                },
                deskripsi: {
                    required: true,
                }
            },
            messages: {
                judul_promo: {
                                remote: jQuery.validator.format("Judul promo sudah digunakan")
                            }
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            if(myDropzone.files.length == 0){
                $.ajax({
                    type    : 'POST',
                    url     : form.attr('action'),
                    data    : form.serialize(),
                    cache   : false,
                    success : function(data) {
                        
                        if(data.status == 'success')
                        {   
                            toastr.success(data.message); 
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);   
                        }
                        else
                        {
                            toastr.error(data.message);
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);
                        }
                    }
                });
            }else{
                return_post_dropzone();
            }
        }
    });
});