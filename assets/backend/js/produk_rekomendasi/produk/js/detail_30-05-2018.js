/* Plugin dropzone upload file */
var myDropzone = Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element
    url              : url,
    uploadMultiple   : uploadMultiple,
    autoProcessQueue : autoProcessQueue,
    maxFilesize      : maxFilesize,
    paramName        : paramName,
    addRemoveLinks   : addRemoveLinks,
    maxFiles         : maxFiles,
    parallelUploads  : parallelUploads,
    acceptedFiles    : acceptedFiles,

    init: function() {
        myDropzone = this;
            this.on("processing", function(file) {
            this.options.url = url;
        });
        this.on("addedfile", function(file) { 
            $('.save').attr("disabled",false); 
        });
        this.on("sending", function(file, xhr, data) { 
            /* additional data */     
            // data.append("nama_fitur", $('input[name="nama_fitur"]').val());
        });
        this.on("removedfile", function(file) {
            var count_file = this.files.length;
            if(count_file == 0)
            {
                $('.save').attr("disabled",true);
            }
        });
        this.on("success", function(file, response) {

            myDropzone.options.autoProcessQueue = false; 

            if(response.status == 'success')
            {   
                toastr.success(response.message);  
            }
            else
            {
                toastr.error(response.message);
            }

        });
        this.on("queuecomplete", function(file, response) {
            setTimeout(function() {
                window.location.reload();
                window.location.href = url_succees+'#m_portlet_tools_2';
            }, 4000);   
        });
    }
} 

function return_post_dropzone(){
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
}




function showcoords() {
    var cX = event.offsetX;
    var cY = event.offsetY;
    $('#cX').val(cX);
    $('#cY').val(cY);
    $('input[name="nama_fitur"]').val('');
    $('textarea[name="deskripsi_fitur"]').val('');
    $('input[name="id"]').val('');
    $('.delete').hide();
    $('.gambar-edit').hide();
    $('.dropzone-edit').show();
}

function editfitur($id, $coorX, $coorY, $nama, $deskripsi, $gambar){
    // # berhenti submit
    event.preventDefault();
    var gambar = '../../../assets/upload/fitur/'+$gambar;
    $('input[name="cX"]').val($coorX);
    $('input[name="cY"]').val($coorY);
    $('input[name="nama_fitur"]').val($nama);
    $('textarea[name="deskripsi_fitur"]').val($deskripsi);
    $('input[name="id"]').val($id);
    $("#tampilGambarFitur").attr("src", gambar);
    $('.delete-fitur').show();
    $('.gambar-edit').show();
    $('.gambar-add').hide();
    $('#m_modal').modal('show');
}

$('.remove').click(function(){
    $('.gambar-edit').hide();
    $('.gambar-add').show();
});

$("select[name='header_position']").on("change",function(){  
    var header = $("select[name='header_position']").val();
    var varian = $("select[name='varian_position']").val();
    var fitur  = $("select[name='fitur_position']").val();
    var video  = $("select[name='video_position']").val();
    var performa = $("select[name='performa_position']").val();
    var highlight = $("select[name='highlight_position']").val();
    if(header == varian){
        $("select[name='varian_position']").val("");
    }else if(header == fitur){
        $("select[name='fitur_position']").val("");
    }else if(header == video){
        $("select[name='video_position']").val("");
    }else if(header == performa){
        $("select[name='performa_position']").val("");
    }else if(header == highlight){
        $("select[name='highlight_position']").val("");
    }
});
$("select[name='varian_position']").on("change",function(){  
    var header   = $("select[name='header_position']").val();
    var varian   = $("select[name='varian_position']").val();
    var fitur    = $("select[name='fitur_position']").val();
    var video    = $("select[name='video_position']").val();
    var performa = $("select[name='performa_position']").val();
    var highlight = $("select[name='highlight_position']").val();
    if(varian == header){
        $("select[name='header_position']").val("");
    }else if(varian == fitur){
        $("select[name='fitur_position']").val("");
    }else if(varian == video){
        $("select[name='video_position']").val("");
    }else if(varian == performa){
        $("select[name='performa_position']").val("");
    }else if(varian == highlight){
        $("select[name='highlight_position']").val("");
    }
});
$("select[name='fitur_position']").on("change",function(){  
    var header   = $("select[name='header_position']").val();
    var varian   = $("select[name='varian_position']").val();
    var fitur    = $("select[name='fitur_position']").val();
    var video    = $("select[name='video_position']").val();
    var performa = $("select[name='performa_position']").val();
    var highlight = $("select[name='highlight_position']").val();
    if(fitur == header){
        $("select[name='header_position']").val("");
    }else if(fitur == varian){
        $("select[name='varian_position']").val("");
    }else if(fitur == video){
        $("select[name='video_position']").val("");
    }else if(fitur == performa){
        $("select[name='performa_position']").val("");
    }else if(fitur == highlight){
        $("select[name='highlight_position']").val("");
    }
});
$("select[name='video_position']").on("change",function(){  
    var header   = $("select[name='header_position']").val();
    var varian   = $("select[name='varian_position']").val();
    var fitur    = $("select[name='fitur_position']").val();
    var video    = $("select[name='video_position']").val();
    var performa = $("select[name='performa_position']").val();
    var highlight = $("select[name='highlight_position']").val();
    if(video == header){
        $("select[name='header_position']").val("");
    }else if(video == varian){
        $("select[name='varian_position']").val("");
    }else if(video == fitur){
        $("select[name='fitur_position']").val("");
    }else if(video == performa){
        $("select[name='performa_position']").val("");
    }else if(video == highlight){
        $("select[name='highlight_position']").val("");
    }
});
$("select[name='performa_position']").on("change",function(){  
    var header   = $("select[name='header_position']").val();
    var varian   = $("select[name='varian_position']").val();
    var fitur    = $("select[name='fitur_position']").val();
    var video    = $("select[name='video_position']").val();
    var performa = $("select[name='performa_position']").val();
    var highlight = $("select[name='highlight_position']").val();
    if(performa == header){
        $("select[name='header_position']").val("");
    }else if(performa == varian){
        $("select[name='varian_position']").val("");
    }else if(performa == fitur){
        $("select[name='fitur_position']").val("");
    }else if(performa == video){
        $("select[name='video_position']").val("");
    }else if(performa == highlight){
        $("select[name='highlight_position']").val("");
    }
});
$("select[name='highlight_position']").on("change",function(){  
    var header    = $("select[name='header_position']").val();
    var varian    = $("select[name='varian_position']").val();
    var fitur     = $("select[name='fitur_position']").val();
    var video     = $("select[name='video_position']").val();
    var performa  = $("select[name='performa_position']").val();
    var highlight = $("select[name='highlight_position']").val();
    if(highlight == header){
        $("select[name='header_position']").val("");
    }else if(highlight == varian){
        $("select[name='varian_position']").val("");
    }else if(highlight == fitur){
        $("select[name='fitur_position']").val("");
    }else if(highlight == video){
        $("select[name='video_position']").val("");
    }else if(highlight == performa){
        $("select[name='performa_position']").val("");
    }
});



function deleteVarian(id) {
    
    event.preventDefault();  
    swal({
      title      : "Are you sure ?",
      text       : "Once deleted, you will not be able to recover this data!",
      icon       : "warning",
      buttons    : true,
      dangerMode : true,
      allowOutsideClick: false
    })

    .then((willDelete) => {
        if (willDelete) {

            $.ajax({
                type    : 'POST',
                url     : delete_varian,
                data    : {id : id},
                cache   : false,
                success : function(result) {
                    if(result.status == 'success')
                    {
                        swal(result.message, {
                            icon    : "success",
                            buttons : false,
                        });

                        setTimeout(function() {
                            window.location.reload();
                            window.location.href = url_succees+'#m_portlet_tools_2';
                        }, 2000);
                    }
                    else
                    {
                        swal(result.message, {
                            icon     : "warning",
                             buttons : false,
                        });

                        setTimeout(function() {
                            window.location.reload();
                            window.location.href = url_succees+'#m_portlet_tools_2';
                        }, 2000);
                    }
                },
            });

        } else {
            swal("Your data is safe!");
        }
    });
};

function deleteVideo(id) {
    
    event.preventDefault();  
    swal({
      title      : "Are you sure ?",
      text       : "Once deleted, you will not be able to recover this data!",
      icon       : "warning",
      buttons    : true,
      dangerMode : true,
      allowOutsideClick: false
    })

    .then((willDelete) => {
        if (willDelete) {

            $.ajax({
                type    : 'POST',
                url     : delete_video,
                data    : {id : id},
                cache   : false,
                success : function(result) {
                    if(result.status == 'success')
                    {
                        swal(result.message, {
                            icon    : "success",
                            buttons : false,
                        });

                        setTimeout(function() {
                            window.location.reload();
                            window.location.href = url_succees+'#m_portlet_tools_4';
                        }, 2000);
                    }
                    else
                    {
                        swal(result.message, {
                            icon     : "warning",
                             buttons : false,
                        });

                        setTimeout(function() {
                            window.location.reload();
                            window.location.href = url_succees+'#m_portlet_tools_4';
                        }, 2000);
                    }
                },
            });

        } else {
            swal("Your data is safe!");
        }
    });
};



jQuery(document).ready(function() {

    /* Save Data*/
    $('.save').click(function() {

        var form  = $(this).closest('form');
        form.validate({
            rules: {
            }
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            if(myDropzone.files.length == 0){
               $.ajax({
                    type    : 'POST',
                    url     : form.attr('action'),
                    data    : form.serialize(),
                    cache   : false,
                    success : function(data) {
                        
                        if(data.status == 'success')
                        {   
                            toastr.success(data.message); 
                            setTimeout(function() {
                                window.location.reload();
                                window.location.href = url_succees+'#m_portlet_tools_2';
                            }, 2000);   
                        }
                        else
                        {
                            toastr.error(data.message);
                            setTimeout(function() {
                                window.location.reload();
                                window.location.href = url_succees+'#m_portlet_tools_2';
                            }, 2000);
                        }
                    }
                });
            }else{
             return_post_dropzone();
            }
        }
    });


    /* Delete Data*/
    $('.delete-fitur').click(function(e) {

        var id   = $('input[name="id"]').val();
        
        e.preventDefault();  
        swal({
          title      : "Are you sure ?",
          text       : "Once deleted, you will not be able to recover this data!",
          icon       : "warning",
          buttons    : true,
          dangerMode : true,
          allowOutsideClick: false
        })

        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    type    : 'POST',
                    url     : delete_fitur,
                    data    : {id : id},
                    cache   : false,
                    success : function(result) {
                        if(result.status == 'success')
                        {
                            swal(result.message, {
                                icon    : "success",
                                buttons : false,
                            });

                            setTimeout(function() {
                                window.location.reload();
                                window.location.href = url_succees+'#m_portlet_tools_3';
                            }, 2000);
                        }
                        else
                        {
                            swal(result.message, {
                                icon     : "warning",
                                 buttons : false,
                            });

                            setTimeout(function() {
                                window.location.reload();
                                window.location.href = url_succees+'#m_portlet_tools_3';
                            }, 2000);
                        }
                    },
                });

            } else {
                swal("Your data is safe!");
            }
        });
    });

});