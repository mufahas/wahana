var DatatableRemoteAjax = function() {

    var demo = function() {

        var beritaTable = $('#table-berita').dataTable({

            "processing": true,
            "serverSide": true,
            "pagingType": "full_numbers",
            "ajax": {
                "url"  : loadTableBerita,
                "type" : "POST",
            },

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            "buttons": [],

            "responsive": true,

            "paging": true,

            "order": [
                [1, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "bProcessing": true,

            "oLanguage": {
                "sProcessing": "Loading, please wait..."
            },

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            "columns": [
                {
                    "render": function(data, type, row) {
                        return row[0];
                    },
                    "visible": true,
                    "class": 'text-center',
                    "orderable": false,
                    "width": '50px'
                },
                {
                    "render": function(data, type, row) {
                        return row[2];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        var status_pub = row[3];

                        if(status_publikasi == 'T')
                        {
                            var status_publikasi = 'Ya';
                        }
                        else
                        {
                            var status_publikasi = 'Tidak';
                        }

                        return status_publikasi;
                    },
                    "visible": true,
                },
            ]

        });

        var promoTable = $('#table-promo').dataTable({

            "processing": true,
            "serverSide": true,
            "pagingType": "full_numbers",
            "ajax": {
                "url"  : loadTablePromo,
                "type" : "POST",
            },

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            "buttons": [],

            "responsive": true,

            "paging": true,

            "order": [
                [1, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "bProcessing": true,

            "oLanguage": {
                "sProcessing": "Loading, please wait..."
            },

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            "columns": [
                {
                    "render": function(data, type, row) {
                        return row[0];
                    },
                    "visible": true,
                    "class": 'text-center',
                    "orderable": false,
                    "width": '50px'
                },
                {
                    "render": function(data, type, row) {
                        return row[2];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        var stat_pro = row[3];

                        if(stat_pro = 'A')
                        {
                            var status_promo = 'Aktif';
                        }
                        else
                        {
                            var status_promo = 'Tidak Aktif';
                        }

                        return status_promo;
                    },
                    "visible": true,
                },
            ]

        });

        var eventTable = $('#table-event').dataTable({

            "processing": true,
            "serverSide": true,
            "pagingType": "full_numbers",
            "ajax": {
                "url"  : loadTableEvent,
                "type" : "POST",
            },

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            "buttons": [],

            "responsive": true,

            "paging": true,

            "order": [
                [1, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "bProcessing": true,

            "oLanguage": {
                "sProcessing": "Loading, please wait..."
            },

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            "columns": [
                {
                    "render": function(data, type, row) {
                        return row[0];
                    },
                    "visible": true,
                    "class": 'text-center',
                    "orderable": false,
                    "width": '50px'
                },
                {
                    "render": function(data, type, row) {
                        return row[2];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        var status_pub = row[3];

                        if(status_publikasi == 'T')
                        {
                            var status_publikasi = 'Ya';
                        }
                        else
                        {
                            var status_publikasi = 'Tidak';
                        }

                        return status_publikasi;
                    },
                    "visible": true,
                },
            ]

        });

        var pesanTable = $('#table-pesan').dataTable({

            "processing": true,
            "serverSide": true,
            "pagingType": "full_numbers",
            "ajax": {
                "url"  : loadTablePesan,
                "type" : "POST",
            },

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            "buttons": [],

            "responsive": true,

            "paging": true,

            "order": [
                [1, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "bProcessing": true,

            "oLanguage": {
                "sProcessing": "Loading, please wait..."
            },

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            "columns": [
                {
                    "render": function(data, type, row) {
                        return row[0];
                    },
                    "visible": true,
                    "class": 'text-center',
                    "orderable": false,
                    "width": '50px'
                },
                {
                    "render": function(data, type, row) {
                        return row[2];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        return row[3];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        return row[4];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        return row[5];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        var tip_pes = row[6];

                        if(tip_pes = 'K')
                        {
                            var tipe_pesan = 'Kontak'; 
                        }
                        else
                        {
                            var tipe_pesan = 'GSO'; 
                        }

                        return tipe_pesan;
                    },
                    "visible": true,
                },

            ]

        });

        var komentarTable = $('#table-komentar').dataTable({

            "processing": true,
            "serverSide": true,
            "pagingType": "full_numbers",
            "ajax": {
                "url"  : loadTableKomentar,
                "type" : "POST",
            },

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            "buttons": [],

            "responsive": true,

            "paging": true,

            "order": [
                [1, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "bProcessing": true,

            "oLanguage": {
                "sProcessing": "Loading, please wait..."
            },

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            "columns": [
                {
                    "render": function(data, type, row) {
                        return row[0];
                    },
                    "visible": true,
                    "class": 'text-center',
                    "orderable": false,
                    "width": '50px'
                },
                {
                    "render": function(data, type, row) {
                        return row[2];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        return row[3];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        return row[4];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        return row[5];
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        var stat_kom = row[6];

                        if(stat_kom == 'T')
                        {
                            var status_komentar = 'Disetujui';
                        }
                        else
                        {
                            var status_komentar = 'Tidak Disetujui';
                        }

                        return status_komentar;
                    },
                    "visible": true,
                },
                {
                    "render": function(data, type, row) {
                        return formattedDateddmmyyyy(row[7])
                    },
                    "visible": true,
                },

            ]

        });

    };

    return {
        // public functions
        init: function() {
            demo();
        },
    };
}();

jQuery(document).ready(function() {
    DatatableRemoteAjax.init();

    $('#tab_1').click(function(){
        $('#m_tabs_1_1').show();
        $('#m_tabs_1_2').hide();
        $('#m_tabs_1_3').hide();
        $('#m_tabs_1_4').hide();
        $('#m_tabs_1_5').hide();
    });
    $('#tab_2').click(function(){
        $('#m_tabs_1_1').hide();
        $('#m_tabs_1_2').show();
        $('#m_tabs_1_3').hide();
        $('#m_tabs_1_4').hide();
        $('#m_tabs_1_5').hide();
    });
    $('#tab_3').click(function(){
        $('#m_tabs_1_1').hide();
        $('#m_tabs_1_2').hide();
        $('#m_tabs_1_3').show();
        $('#m_tabs_1_4').hide();
        $('#m_tabs_1_5').hide();
    });
    $('#tab_4').click(function(){
        $('#m_tabs_1_1').hide();
        $('#m_tabs_1_2').hide();
        $('#m_tabs_1_3').hide();
        $('#m_tabs_1_4').show();
        $('#m_tabs_1_5').hide();
    });
    $('#tab_5').click(function(){
        $('#m_tabs_1_1').hide();
        $('#m_tabs_1_2').hide();
        $('#m_tabs_1_3').hide();
        $('#m_tabs_1_4').hide();
        $('#m_tabs_1_5').show();
    });
});