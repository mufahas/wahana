jQuery(document).ready(function() {

    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
            <span></span>\
        </div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        alert.animateClass('fadeIn animated');
        alert.find('span').html(msg);
    }

    $('#m_reset_password_signin_submit').click(function(e) {

        e.preventDefault();

        var btn         = $(this);
        var form        = $(this).closest('form');

        form.validate({
            rules: {
                new_password: {
                    required : true
                },
                confirm_password: {
                    equalTo: "#new_password"
                }
            }
        });

        if (!form.valid()) {
            return;
        }

        btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
        form.ajaxSubmit({
            type : 'POST',
            url  : processResetPassword,
            success: function(response, status, xhr, $form) {
                var oObj = JSON.parse(response);
                /* similate 2s delay */
                if(oObj.status == 'error'){
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', oObj.message);
                        setTimeout(function() {
                            window.location.href = reset_password;
                        }, 2000);
                    }, 2000);
                }

                if(oObj.status == 'success'){
                    showErrorMsg(form, 'success', oObj.message);
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        window.location.href = auth;
                    }, 2000);
                }
            }
        });
    });
});