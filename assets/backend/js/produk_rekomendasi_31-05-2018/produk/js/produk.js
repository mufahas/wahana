jQuery(document).ready(function() {

    $('[data-switch=true]').bootstrapSwitch();

    /* Start Set Select Dropdown */
    $('#m_kategori_motor').select2({
        placeholder: "-- Pilih Ketegori Motor --",
    });

    $('#m_tipe_transmisi').select2({
        placeholder: "-- Pilih Tipe Transmisi --",
    });

    $('#m_pengoperan_gigi').select2({
        placeholder: "-- Pilih Tipe Pengoperan Gigi --",
    });

    $('#m_tipe_kopling').select2({
        placeholder: "-- Pilih Tipe Kopling --",
    });

    $('#m_tipe_starter').select2({
        placeholder: "-- Pilih Tipe Starter --",
    }); 

    // $('#m_produk_terbaik').select2({
    //     placeholder: "-- Pilih Produk Terbaik --",
    // });

    // $('#m_urutan_produk_terbaik').select2({
    //     placeholder: "-- Pilih Urutan Produk Terbaik --",
    // }); 
    /* End Set Select Dropdown */
    
    /* Function Read URL */
    function readURL(input)
    {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#gambar_produk').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    /* On Change Gambar Produk */
    $('#input_gambar_produk').change(function(){
        readURL(this);
    });

    /* Decimal Format*/
    $("input[name='harga_produk']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    $("input[name='harga_otr[]']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    $("input[name='diameter']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    $("input[name='panjang']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    $("input[name='lebar']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    $("input[name='tinggi']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    $("input[name='jarak_sumbu_roda']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    $("input[name='jarak_terendah_ke_tanah']").inputmask('decimal', {
        rightAlignNumerics: false
    });
    $("input[name='ketinggian_tempat_duduk']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    $("input[name='berat_kosong']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    $("input[name='radius_putar']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    $("input[name='kapasitas_tangki_bb']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    $("input[name='kapasitas_minyak_pelumas']").inputmask('decimal', {
        rightAlignNumerics: false
    }); 
    /* End Decimal format*/ 

    /* get preview kategori motor */
    $('select[name="kategori_motor"]').change(function(){
        var kategori_motor = $('select[name="kategori_motor"] option:selected').html();
    });

    /* set on change kategori motor */
    $('select[name="kategori_motor"]').change(function(){
        var kategori_motor = $(this).val();

        if(kategori_motor != '') 
        {
            $('input[name="nama_produk"]').prop('disabled',false);
            $('input[name="harga_produk"]').prop('disabled',false);
            $('select[name="status_uji_coba"]').prop('disabled',false);
            $('textarea[name="deskripsi_produk"]').prop('disabled',false);
        }
    });

    /* set on change produk terbaik */
    $('select[name="tipe_transmisi"]').change(function(){
        var tipe_transmisi = $(this).val();

        if(tipe_transmisi == '4 Kecepatan'){
            $('select[name="pengoperan_gigi"]').val("1-N-2-3-4").trigger('change');
            $('input[name="rasio_reduksi_gigi"]').prop('disabled',false);
            $('select[name="pengoperan_gigi"]').removeClass('notvalidate');
            $('input[name="rasio_reduksi_gigi"]').removeClass('notvalidate');
        }else if(tipe_transmisi == '5 Kecepatan'){
            $('select[name="pengoperan_gigi"]').val("1-N-2-3-4-5").trigger('change');
            $('input[name="rasio_reduksi_gigi"]').prop('disabled',false);
            $('select[name="pengoperan_gigi"]').removeClass('notvalidate');
            $('input[name="rasio_reduksi_gigi"]').removeClass('notvalidate');
        }else if(tipe_transmisi == '6 Kecepatan'){
            $('select[name="pengoperan_gigi"]').val("1-N-2-3-4-5-6").trigger('change');
            $('input[name="rasio_reduksi_gigi"]').prop('disabled',false);
            $('select[name="pengoperan_gigi"]').removeClass('notvalidate');
            $('input[name="rasio_reduksi_gigi"]').removeClass('notvalidate');
        }else if(tipe_transmisi == 'Matic'){
            $('select[name="pengoperan_gigi"]').val("-").trigger('change');
            $('select[name="pengoperan_gigi"]').prop('disabled',false);
            $('input[name="rasio_reduksi_gigi"]').val("-");
            $('input[name="rasio_reduksi_gigi"]').prop('disabled',false);
            $('input[name="rasio_reduksi_gigi"]').removeClass('notvalidate');
            $('input[name="rasio_reduksi_gigi"]').removeClass('input-error');
        }
    });

    /* set on change produk terbaik */
    // $('select[name="produk_terbaik"]').change(function(){
    //     var produk_terbaik = $(this).val();

    //     if(produk_terbaik != 'F'){
    //         $('select[name="urutan_produk_terbaik"]').prop('disabled',false);
    //         $('select[name="urutan_produk_terbaik"]').addClass('notvalidate');
    //     }else{
    //         $('select[name="urutan_produk_terbaik"]').val("-").trigger('change');
    //         $('select[name="urutan_produk_terbaik"]').prop('disabled',true);
    //         $('select[name="urutan_produk_terbaik"]').removeClass('notvalidate');
    //     }
    // });

    /* Begin Set Table Study */
    $(".addmore_otr").on('click',function(){
        var i=$('#row_otr').val();
        $('#row_otr').val(++i);
        
        var data='<div class="row col-lg-12" id="listOtr_'+i+'">';
            data+='<div class="col-lg-5 form-group">';
            data+='<input type="text" name="nama_varian[]" class="form-control m-input" placeholder="Nama Varian">';
            data+='</div>';
            data+='<div class="col-lg-5 form-group">';
            data+='<input type="text" name="harga_otr[]" class="form-control m-input" placeholder="Harga Otr">';
            data+='</div>';
            data+='<div class="col-lg-2 form-group">';
            data+='<button type="button" class="btn btn-sm btn-danger delete_otr" title="Delete"><i class="fa fa-close "></i> &nbsp; Delete</button>';
            data+='</div>';
            data+='</div>';
            
        $('#formOtr').append(data);
    
        $('.delete_otr').click(function() {
         var list = $(this).parent().parent();
         list.remove();
        });
    });

    /* Save Data*/
    $('#save').click(function() {

        var form                    = $(this).closest('form');
       
        form.validate({
            rules: {
                kategori_motor: {
                    required: true
                },
                nama_produk: {
                    required: true,
                    maxlength: 255
                },
                harga_produk: {
                    required: true
                },
                tipe_mesin: {
                    required: true
                },
                tipe_transmisi: {
                    required: true
                },
                tipe_kopling: {
                    required: false
                },
                tipe_starter: {
                    required: true
                },
                tipe_busi: {
                    required: true,
                    maxlength: 100
                },
                diameter: {
                    required: true
                },
                langkah: {
                    required: true
                },
                volume_langkah: {
                    required: true,
                    maxlength: 12
                },
                sistem_pendingin_mesin: {
                    required: true,
                    maxlength: 100
                },
                sistem_bahan_bakar: {
                    required: true,
                    maxlength: 100
                },
                perbandingan_kompresi: {
                    maxlength: 20
                },
                daya_maksimum: {
                    required: true,
                    maxlength: 100
                }, 
                torsi_maksimum: {
                    required: true,
                    maxlength: 100
                },
                sistem_pengapian: {
                    required: true
                },
                sistem_pelumasan: {
                    required: true,
                    maxlength: 100
                },
                kapasitas_minyak_pelumas: {
                    required: true
                },
                tipe_rangka: {
                  required: true
                },
                radius_putar: {
                    required: true
                },
                panjang: {
                    required: true
                },
                lebar: {
                    required: true
                },
                tinggi: {
                    required: true
                },
                jarak_sumbu_roda: {
                    required: true
                },
                jarak_terendah_ke_tanah: {
                    required: true
                },
                berat_kosong: {
                    required: true
                },
                sistem_pelumasan: {
                    required: true
                },
                sistem_pengapian: {
                    required: true
                },
                kapasitas_tangki_bb: {
                    required: true
                },
                tipe_baterai: {
                    required: true,
                    maxlength: 100
                },
                tipe_rangka: {
                    required: true
                },
                tipe_suspensi_depan: {
                    required: true,
                    maxlength: 100
                },
                tipe_suspensi_belakang: {
                    required: true,
                    maxlength: 100
                },
                ukuran_ban_depan: {
                    required: true,
                    maxlength: 100
                },
                ukuran_ban_belakang: {
                    required: true,
                    maxlength: 100
                },
                rem_depan: {
                    required: true,
                    maxlength: 100
                },
                rem_belakang: {
                    required: true,
                    maxlength: 100
                },
                deskripsi_produk: {
                    required: true
                },
            }
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });
        }
    });
    
});