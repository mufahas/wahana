jQuery(document).ready(function() {

    $('#m_select2_1').select2({
        placeholder: "-- Pilih Produk --",
    });
    $('#m_select2_2').select2({
        placeholder: "-- Pilih Rekomendasi 1 --",
    });
    $('#m_select2_3').select2({
        placeholder: "-- Pilih Rekomendasi 2 --",
    });
    $('#m_select2_4').select2({
        placeholder: "-- Pilih Rekomendasi 3 --",
    });
    $('#m_select2_5').select2({
        placeholder: "-- Pilih Rekomendasi 4 --",
    });

    var kodeProduk   = $("select[name='kode_produk']").val();
    var rekomendasi1 = $("select[name='kode_rekomendasi_1']").val();
    var rekomendasi2 = $("select[name='kode_rekomendasi_2']").val();
    var rekomendasi3 = $("select[name='kode_rekomendasi_3']").val();
    var rekomendasi4 = $("select[name='kode_rekomendasi_4']").val();

    $.ajax({
        type    : 'POST',
        url     : urlRekomendasi,
        data    : {kode_produk : kodeProduk, kode_rekomendasi_1 : rekomendasi1, kode_rekomendasi_2 : rekomendasi2, kode_rekomendasi_3 : rekomendasi3, kode_rekomendasi_4 : rekomendasi4},
        dataType: "json",
        cache   : false,
        success : function(response){
            $("select[name='kode_produk']").html(response['produk']);
            $("select[name='kode_rekomendasi_1']").html(response['rekom1']);
            $("select[name='kode_rekomendasi_2']").html(response['rekom2']);
            $("select[name='kode_rekomendasi_3']").html(response['rekom3']);
            $("select[name='kode_rekomendasi_4']").html(response['rekom4']);
        }
    });

    $(".m-select2").on("change",function(){  
        var kodeProduk   = $("select[name='kode_produk']").val();
        var rekomendasi1 = $("select[name='kode_rekomendasi_1']").val();
        var rekomendasi2 = $("select[name='kode_rekomendasi_2']").val();
        var rekomendasi3 = $("select[name='kode_rekomendasi_3']").val();
        var rekomendasi4 = $("select[name='kode_rekomendasi_4']").val();
        $.ajax({
            type    : 'POST',
            url     : urlRekomendasi,
            data    : {kode_produk : kodeProduk, kode_rekomendasi_1 : rekomendasi1, kode_rekomendasi_2 : rekomendasi2, kode_rekomendasi_3 : rekomendasi3, kode_rekomendasi_4 : rekomendasi4},
            dataType: "json",
            cache   : false,
            success : function(response){
                $("select[name='kode_produk']").html(response['produk']);
                $("select[name='kode_rekomendasi_1']").html(response['rekom1']);
                $("select[name='kode_rekomendasi_2']").html(response['rekom2']);
                $("select[name='kode_rekomendasi_3']").html(response['rekom3']);
                $("select[name='kode_rekomendasi_4']").html(response['rekom4']);
            }
        });
    });

    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                kode_produk: {
                    required: true,
                },
                kode_rekomendasi_1: {
                    required: true,
                },
                kode_rekomendasi_2: {
                    required: true
                },
                kode_rekomendasi_3: {
                    required: true
                },
                kode_rekomendasi_4: {
                    required: true,
                }
            }
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $.ajax({
                type    : 'POST',
                url     : form.attr('action'),
                data    : form.serialize(),
                cache   : false,
                success : function(data) {
                    
                    if(data.status == 'success')
                    {   
                        toastr.success(data.message); 
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);   
                    }
                    else
                    {
                        toastr.error(data.message);
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);
                    }
                }
            });
        }
    });
});