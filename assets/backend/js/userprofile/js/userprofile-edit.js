function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


jQuery(document).ready(function() {

    /* Begin Change Password */
    $('.change-password').click(function(e) {
        e.preventDefault();  

        swal({
          title      : "Yakin Ubah Password ?",
          text       : "Perubahan password akan dikiriman ke email yang telah didaftarkan. Silahkan cek kembali email anda !",
          icon       : "warning",
          buttons    : true,
          dangerMode : true,
          allowOutsideClick: false
        })

        .then((changePassword) => {
            if (changePassword) {

                $.ajax({
                    type    : 'POST',
                    url     : change_password,
                    cache   : false,
                    success : function(result) {
                        mApp.blockPage();

                        if(result.status == 'success')
                        {   
                            swal(result.message, {
                                icon    : "success",
                                buttons : false,
                            });

                            setTimeout(function() {
                                window.location.reload();
                            }, 2000);
                        }
                        else
                        {
                            swal(result.message, {
                                icon    : "warning",
                                buttons : false,
                            });

                            setTimeout(function() {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                });
               

            } else {
                swal("Batal ubah password!");
            }
        });
    });
    /* End Change Password */

    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
              username: {
                required: true
              },
              email: {
                required: true,
                email: true
              },
              first_name: {
                required: true
              },
              last_name: {
                required: true
              },
              phone_number: {
                required: true,
                minlength: 10
              }
            }
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $.ajax({
                type    : 'POST',
                url     : form.attr('action'),
                data    : form.serialize(),
                cache   : false,
                success : function(data) {
                    
                    if(data.status == 'success')
                    {   
                        toastr.success(data.message); 
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);   
                    }
                    else
                    {
                        toastr.error(data.message);
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);
                    }
                }
            });
        }
    });
});