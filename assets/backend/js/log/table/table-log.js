var DatatableRemoteAjax = function() {

    var demo = function() {

        var oTable = $('#table').dataTable()

    };

    return {
        // public functions
        init: function() {
            demo();
        },
    };
}();

jQuery(document).ready(function() {
    DatatableRemoteAjax.init();
});