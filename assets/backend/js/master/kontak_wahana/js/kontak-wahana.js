jQuery(document).ready(function() {

    $('[data-switch=true]').bootstrapSwitch();

    $('#m_select2_2').select2({
        placeholder: "-- Pilih Jenis Kontak --",
    });

    /* get preview icon */
    $("select[name='jenis_kontak']").change(function(){
        var jenis_kontak = $('select[name="jenis_kontak"] option:selected').html();
    });

    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                nama_kontak: {
                    required: true,
                    maxlength: 100
                },
                jenis_kontak: {
                    required: true
                },
                email: {
                    required: true,
                    maxlength: 100
                },
                nomor_telepon1: {
                    required: true,
                    maxlength: 15
                },
                nomor_telepon2: {
                    required: true,
                    maxlength: 15
                },
                alamat_kontak: {
                    required: true
                },
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $.ajax({
                type    : 'POST',
                url     : form.attr('action'),
                data    : form.serialize(),
                cache   : false,
                success : function(data) {

                    if(data.status == 'success')
                    {   
                        toastr.success(data.message); 
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);   
                    }
                    else
                    {
                        toastr.error(data.message);
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);
                    }
                }
            });
        }
    });
    
});