jQuery(document).ready(function() {
    
    $('.success').fadeOut(3000);
    $('.error').fadeOut(3000);

    $('.summernote').summernote({
            height: 150,
            toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
          ]
        });

    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                layanan_vip: {
                    required: true
                },
                fasilitas_kelebihan: {
                    required: true
                },
                userfile: {
                    required: true
                },
                userfile_: {
                    required: true
                } 
            },
        });
    });
});