jQuery(document).ready(function() {

    $('[data-switch=true]').bootstrapSwitch();
    $('#m_select2_2').select2({
        placeholder: "-- Pilih Provinsi --",
    });

    $("select[name='kode_provinsi']").change(function(){
        var provinsi = $('select[name="kode_provinsi"] option:selected').html();
    });

    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                kode_provinsi: {
                    required: true,
                    maxlength: 100
                },
                koka: {
                    required: true,
                    maxlength: 100
                }
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $.ajax({
                type    : 'POST',
                url     : form.attr('action'),
                data    : form.serialize(),
                cache   : false,
                success : function(data) {

                    if(data.status == 'success')
                    {   
                        toastr.success(data.message); 
                            window.location.href = data.url;
                        setTimeout(function() {
                        }, 2000);   
                    }
                    else
                    {
                        toastr.error(data.message);
                            window.location.href = data.url;
                        setTimeout(function() {
                        }, 2000);
                    }
                }
            });
        }
    });
    
});