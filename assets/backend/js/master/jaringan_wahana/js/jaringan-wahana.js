jQuery(document).ready(function() {

    $("#no_tel").inputmask("mask", {
        "mask": "02199999999"
    });    

    /*  MAP */
    $('input[name="link_peta"]').on('change paste keyup keypress onclick onload', function() {

        var linkPeta = $(this).val();
        var data     = {link_peta: linkPeta};

        $.ajax({
            type: 'POST',
            url: getAjaxLatLng,
            data: data,
            dataType: 'json',
            cache: false,
            success: function(result) {
                var oObj     = result;
                
                var url      = oObj.toString();
                var urlSplit = url.split('/');
                
                if(urlSplit[6] != '' && urlSplit[6] != undefined)
                {
                    var getLatLng = urlSplit[6].replace('@','');
                    getLatLng     = getLatLng.split( ',' );
                    
                    var lat = parseFloat(getLatLng[0]);
                    var lng = parseFloat(getLatLng[1]);

                    $('input[name="latitude"]').val(lat);
                    $('input[name="longitude"]').val(lng);

                }
            }
        });
    });
    /*  MAP */



    $('#m_select2_1').select2({
        placeholder: "-- Pilih Kota Kabupaten --",
    });
    $('#m_select2_2').select2({
        placeholder: "-- Pilih Kategori Jaringan --",
    });

    var view_peta = $('textarea[name="link_embed_peta"]').val();
    $('#previewPeta').html(view_peta);

    $('textarea[name="link_embed_peta"]').keyup(function(){
        var view_peta = $('textarea[name="link_embed_peta"]').val();
        $('#previewPeta').html(view_peta);
    });

    /* get preview icon */
    // $("select[name='kategori_jaringan']").change(function(){
    //     var kategori_jaringan = $('select[name="kategori_jaringan"] option:selected').html();
    // });

    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                kode_kota_kabupaten: {
                    required: true,
                },
                'kategori_jaringan[]': {
                    required: true,
                },
                nama_dealer: {
                    required: true,
                    maxlength : 99,
                },
                nomor_telepon: {
                    required: false,
                    maxlength : 15,
                },
                alamat_jaringan: {
                    required: true,
                },
                link_peta: {
                    required: true,
                },
                latitude: {
                    required: true
                },
                longitude: {
                    required: true
                }
                // link_embed_peta: {
                //     required: true,
                // }
            }
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $.ajax({
                type    : 'POST',
                url     : form.attr('action'),
                data    : form.serialize(),
                cache   : false,
                success : function(data) {
                    
                    if(data.status == 'success')
                    {   
                        toastr.success(data.message); 
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);   
                    }
                    else
                    {
                        toastr.error(data.message);
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);
                    }
                }
            });
        }
    });
});