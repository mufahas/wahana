/* Plugin dropzone upload file */
var myDropzone = Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element
    url              : url,
    uploadMultiple   : uploadMultiple, 
    autoProcessQueue : autoProcessQueue,
    maxFilesize      : maxFilesize,
    paramName        : paramName,
    addRemoveLinks   : addRemoveLinks,
    maxFiles         : maxFiles,
    parallelUploads  : parallelUploads,
    acceptedFiles    : acceptedFiles,
    accept: function(file, done) {
        if (file.name.length > 200) {
            this.removeFile(file);
            toastr.error('Filename exceeds 200 characters!');
            // done("Filename exceeds 200 characters!");
        }
        else { done(); }
    },
    init: function() {
        myDropzone = this;
            this.on("processing", function(file) {
            this.options.url = url;
        });
        this.on("addedfile", function(file) { 
            $('.save').attr("disabled",false); 
        });
        this.on("sending", function(file, xhr, data) { 

            /* additional data */     
            data.append("jenis"/*name*/, $('select[name="jenis"]').val()/*value*/);
            data.append("url_banner"/*name*/, $('input[name="url_banner"]').val()/*value*/);
            data.append("new_page"/*name*/, $('input[name="new_page"]').val()/*value*/);
            data.append("publish_status"/*name*/, $('input[name="publish_status"]:checked').val()/*value*/);
            data.append("id", $('input[name="id"]').val());
        });
        this.on("removedfile", function(file) {
            var count_file = this.files.length;
            if(count_file == 0)
            {
                $('.save').attr("disabled",true);
            }
        });
        this.on("success", function(file, response) {

            myDropzone.options.autoProcessQueue = false; 

            if(response.status == 'success')
            {   
                toastr.success(response.message);  
            }
            else
            {
                toastr.error(response.message);
            }

        });
        this.on("queuecomplete", function(file, response) {
            setTimeout(function() {
                window.location.href = url_succees;
            }, 4000);   
        });
    }
} 

function return_post_dropzone(){
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
}

jQuery(document).ready(function() {

    $('[data-switch=true]').bootstrapSwitch();
    $('#m_select2_2').select2({
        placeholder: "-- Pilih Jenis Banner --",
    });

    $('.remove').click(function(){
        $('.save').attr("disabled",true);
        $('.photo-edit').hide();
        $('.dropzone-edit').show();
    });

    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                jenis: {
                    required: true
                },
            }
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            if(myDropzone.files.length == 0){
               $.ajax({
                    type    : 'POST',
                    url     : form.attr('action'),
                    data    : form.serialize(),
                    cache   : false,
                    success : function(data) {
                        
                        if(data.status == 'success')
                        {   
                            toastr.success(data.message); 
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);   
                        }
                        else
                        {
                            toastr.error(data.message);
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);
                        }
                    }
                });
            }else{
                return_post_dropzone();
            }
        }
    });
});