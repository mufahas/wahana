jQuery(document).ready(function() {

    $('[data-switch=true]').bootstrapSwitch();

    $('#m_select2_2').select2({
        placeholder: "-- Pilih Perhitungan Jenis Bunga --",
    });

    /* get preview icon */
    $("select[name='perhitungan_jenis_bunga']").change(function(){
        var perhitungan_jenis_bunga = $('select[name="perhitungan_jenis_bunga"] option:selected').html();
    });

    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                persentase_bunga: {
                    required: true
                },
                perhitungan_jenis_bunga: {
                    required: true
                },
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $.ajax({
                type    : 'POST',
                url     : form.attr('action'),
                data    : form.serialize(),
                cache   : false,
                success : function(data) {

                    if(data.status == 'success')
                    {   
                        toastr.success(data.message); 
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);   
                    }
                    else
                    {
                        toastr.error(data.message);
                        setTimeout(function() {
                            window.location.href = data.url;
                        }, 2000);
                    }
                }
            });
        }
    });
    
});