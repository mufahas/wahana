jQuery(document).ready(function() {
    $('#m_select2_2').select2({
        placeholder: "-- Pilih Produk --",
    });

    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                produk: {
                    required: true
                },
                userfile: {
                    required: true
                },
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            $('.save').addClass('m-loader m-loader--light m-loader--right disabled');
        }
    });
    
});