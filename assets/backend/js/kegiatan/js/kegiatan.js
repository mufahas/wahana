/* Plugin dropzone upload file */
var myDropzone = Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element
    url              : url,
    uploadMultiple   : uploadMultiple, 
    autoProcessQueue : autoProcessQueue,
    maxFilesize      : maxFilesize,
    paramName        : paramName,
    addRemoveLinks   : addRemoveLinks,
    maxFiles         : maxFiles,
    parallelUploads  : parallelUploads,
    acceptedFiles    : acceptedFiles,
    init: function() {
        myDropzone = this;
            this.on("processing", function(file) {
            this.options.url = url;
        });
        this.on("addedfile", function(file) { 
            $('.save').attr("disabled",false); 
        });
        this.on("sending", function(file, xhr, data) { 

            /* additional data */     
            data.append("title"/*name*/, $('input[name="title"]').val()/*value*/);
            // data.append("event_code"/*name*/, $('input[name="event_code"]').val()/*value*/);
            data.append("description"/*name*/, $('textarea[name="description"]').val()/*value*/);
            data.append("short_desc"/*name*/, $('textarea[name="short_desc"]').val()/*value*/);
            data.append("location"/*name*/, $('textarea[name="location"]').val()/*value*/);
            data.append("date"/*name*/, $('input[name="date"]').val()/*value*/);
            data.append("dibuka"/*name*/, $('input[name="dibuka"]').val()/*value*/);
            data.append("id", $('input[name="id"]').val());
        });
        this.on("removedfile", function(file) {
            var count_file = this.files.length;
            if(count_file == 0)
            {
                $('.save').attr("disabled",true);
            }
        });
        this.on("success", function(file, response) {

            myDropzone.options.autoProcessQueue = false; 

            if(response.status == 'success')
            {   
                toastr.success(response.message);  
            }
            else
            {
                toastr.error(response.message);
            }

        });
        this.on("queuecomplete", function(file, response) {
            setTimeout(function() {
                window.location.href = url_succees;
            }, 4000);   
        });
    }
} 

function return_post_dropzone(){
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
}

// function enableClick()
// {
//     $('input[name="event_code"]').on('keyup keypress bind', function() {
//         $('.check_event_code').attr('disabled',false);
//     });
// }


jQuery(document).ready(function() {

    // $('.check_event_code').attr('disabled',true);

    // enableClick();

    // $('.check_event_code').click(function() {
    //     var event_code = $('input[name="event_code"]').val();
    //     var id         = $('input[name="id"]').val(); 

    //     $.ajax({
    //         type    : 'POST',
    //         url     : checkEventCode,
    //         data    : {id: id, event_code: event_code},
    //         cache   : false,
    //         success : function(data) {

    //             if(data.status == 'true')
    //             {   
    //                 toastr.success(data.message); 
    //             }
    //             else
    //             {
    //                 toastr.error(data.message);
    //             }
    //         }
    //     });
    // });

    $('.summernote').summernote({
        height: 150,
        toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
      ]
    });
    // date & time
    $('#date').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'DD MMMM YYYY, hh:mm a'
        }
    }, function(start, end, label) {
        $('#date .form-control').val( start.format('DD MMMM YYYY, hh:mm a') + ' - ' + end.format('DD MMMM YYYY, hh:mm a'));
    });
    // enable clear button 
    $('#start_date, #end_date').datepicker({
        todayBtn: "linked",
        format :'dd MM yyyy',
        clearBtn: true,
        todayHighlight: true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });
    // minimum setup
    $('#start_time, #end_time').timepicker({showMeridian: false,showSeconds: true});

    $('[data-switch=true]').bootstrapSwitch();

    $('.remove').click(function(){
        $('.save').attr("disabled",true);
        $('.photo-edit').hide();
        $('.dropzone-edit').show();
    });

    /* get preview icon */
    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                title: {
                    required: true,
                    maxlength: 100,
                    remote: {
                        type : 'POST',
                        url  : checkTitle,
                        data : {
                            id: function() {
                                return $('input[name="id"]').val();
                            },
                            title: function() {
                                return $('input[name="title"]').val();
                            }
                        }
                    }
                },
                location : {
                    required : true
                },
                date : {
                    required : true
                },
                description : {
                    required : true
                },
                short_desc:{
                    required: true,
                    minlength: 120,
                    maxlength: 320
                }
            },
            messages: {
                title: {
                            remote: jQuery.validator.format("Judul ini telah digunakan.")
                        }
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            if(myDropzone.files.length == 0)
            {
                $.ajax({
                    type    : 'POST',
                    url     : form.attr('action'),
                    data    : form.serialize(),
                    cache   : false,
                    success : function(data) {

                        if(data.status == 'success')
                        {   
                            toastr.success(data.message); 
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);   
                        }
                        else
                        {
                            toastr.error(data.message);
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);
                        }
                    }
                });
            } else 
            {
                return_post_dropzone();
            }
        }
    });
    
});