var myDropzone = Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element
    url              : url,
    uploadMultiple   : uploadMultiple, 
    autoProcessQueue : autoProcessQueue,
    maxFilesize      : maxFilesize,
    paramName        : paramName,
    addRemoveLinks   : addRemoveLinks,
    maxFiles         : maxFiles,
    parallelUploads  : parallelUploads,
    acceptedFiles    : acceptedFiles,
    init: function() {
        myDropzone = this;

        $.get(getImage, function(data) {
            
            var oObj = JSON.parse(data);

            $.each(oObj, function(key,value){

                // var mockFile = { name: value.gambar_part_aksesoris, size: 0 };
                
                var mockFile = { 
                    name: value.gambar_event, 
                    url: path_image + value.gambar_event
                };
                 
                myDropzone.options.addedfile.call(myDropzone, mockFile);
 
                myDropzone.options.thumbnail.call(myDropzone, mockFile, path_image + value.gambar_event);

                myDropzone.files.push(mockFile);
                
            });
        });


        this.on("processing", function(file) {
            this.options.url = url;
        });
        this.on("addedfile", function(file) { 
            //$('.save').attr("disabled",false); 
        });
        this.on("sending", function(file, xhr, data) { 

             /* additional data */     
            data.append("title"/*name*/, $('input[name="title"]').val()/*value*/);
            data.append("description"/*name*/, $('textarea[name="description"]').val()/*value*/);
            data.append("location"/*name*/, $('textarea[name="location"]').val()/*value*/);
            data.append("date"/*name*/, $('input[name="date"]').val()/*value*/);
            data.append("dibuka"/*name*/, $('input[name="dibuka"]').val()/*value*/);
            data.append("id", $('input[name="id"]').val());
        });

        this.on("removedfile", function(file) {
            if (file.url && file.url.trim().length > 0) {
                var kode_event = $('input[name="id"]').val();
                $.get(remove_image + kode_event + '/' + file.name , function(data) {

                    if(data.status == 'success')
                    {   
                        toastr.success(data.message);  
                    }
                    else
                    {
                        toastr.error(data.message);
                    }
                });
            }
        });
        this.on("success", function(file, response) {

            // myDropzone.options.autoProcessQueue = false; 

            if(response.status == 'success')
            {   
                toastr.success(response.message);  
            }
            else
            {
                toastr.error(response.message);
            }

        });
        this.on("queuecomplete", function(file, response) {
            setTimeout(function() {
                window.location.href = url_succees;
            }, 4000);   
        });
    }
} 

function return_post_dropzone(){
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
}

jQuery(document).ready(function() {
   
    $('.summernote').summernote({
        height: 150,
        toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
      ]
    });

    // date & time
    $('#date').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'DD MMMM YYYY, hh:mm a'
        }
    }, 

    function(start, end, label) {
        $('#date .form-control').val( start.format('DD MMMM YYYY, hh:mm a') + ' - ' + end.format('DD MMMM YYYY, hh:mm a'));
    });
    
    // enable clear button 
    $('#start_date, #end_date').datepicker({
        todayBtn: "linked",
        format :'dd MM yyyy',
        clearBtn: true,
        todayHighlight: true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });
    // minimum setup
    $('#start_time, #end_time').timepicker({showMeridian: false,showSeconds: true});

    $('[data-switch=true]').bootstrapSwitch();

    /* get preview icon */
    /* Save Data*/
    /* Save Data*/
    $('.save').click(function() {

        var form = $(this).closest('form');

        form.validate({
            rules: {
                title: {
                    required: true,
                    maxlength: 100,
                    remote: {
                        type : 'POST',
                        url  : checkTitle,
                        data : {
                            id: function() {
                                return $('input[name="id"]').val();
                            },
                            title: function() {
                                return $('input[name="title"]').val();
                            }
                        }
                    }
                },
                location : {
                    required : true
                },
                date : {
                    required : true
                },
                description : {
                    required : true
                },
            },
            messages: {
                title: {
                            remote: jQuery.validator.format("Judul ini telah digunakan.")
                        }
            },
        });

        if (form.valid()) {

            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'primary',
                message: 'Processing...'
            });

            if(myDropzone.getQueuedFiles().length == 0)
            {
                $.ajax({
                    type    : 'POST',
                    url     : form.attr('action'),
                    data    : form.serialize(),
                    cache   : false,
                    success : function(data) {

                        if(data.status == 'success')
                        {   
                            toastr.success(data.message); 
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);   
                        }
                        else
                        {
                            toastr.error(data.message);
                            setTimeout(function() {
                                window.location.href = data.url;
                            }, 2000);
                        }
                    }
                });
            } else 
            {
                return_post_dropzone();
            }
        }
    });
    
});